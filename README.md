# Oposiciones
Repositorio de estudiuo de oposiciones

## Estructura
Se comenzó realizando apuntes en md, pero por su tamaño se migró a Tex, para poder manejarlos de forma independiente, tanto por la cantidad de temas distintos, como de subniveles.

La carpeta **Tex** contiene lo necesario para generar un pdf con todos los apuntes (ignorado en el repo y agregado como elemento en sucesivas versiones). Dentro, la carpeta **meta** contiene los ficheros ajenos al contenido general y que arropan al documento, como el glosario o el temario. La carpeta **Apuntes** contiene un fichero por cada tema de las oposiciones, dividido de la misma manera que los temas de la oposición.

La carpeta **Apuntes** Contiene archivos de preparatec con resúmenes en pdf, audios y material de estudio. Existe mas material que no se ha subido por su peso y por no haberlo usado aún, si se usa se añadirá.

## Compilación
Mira, yo que se, le he instalado a debian un pool de paquetes de latex, nada raro. Intenta crearlo y si no mira que le falta. Ire subiendo versiones del pdf cada tema que acabe.