<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1377087037039" ID="ID_562185076" MODIFIED="1377182091674" TEXT="Wimax">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368723832211" ID="ID_846338265" MODIFIED="1383413599552" POSITION="right" TEXT="WiMAX">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368723853199" ID="ID_1828428774" MODIFIED="1382617559226" TEXT="IEEE 802.16">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376559198635" ID="ID_942571644" MODIFIED="1383413647185" TEXT="802.16e: Wimax m&#xf3;vil">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376559214058" ID="ID_1723097792" MODIFIED="1377182319632" TEXT="802.16m: Wimax advanced">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376559227998" ID="ID_200281530" MODIFIED="1377182323296" TEXT="1 Gbps fijo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376559244090" ID="ID_1933202137" MODIFIED="1377182326370" TEXT="100 Mbps m&#xf3;vil">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1368723909601" ID="ID_874460585" MODIFIED="1377182288519" TEXT="Acceso al medio mediante TDMA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368723972464" ID="ID_932499559" MODIFIED="1381332242053" TEXT="Bandas de trabajo">
<node CREATED="1368723981586" ID="ID_1610346170" MODIFIED="1377182294572" TEXT="10-66 GHz (licencia y LOS)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368724002179" ID="ID_554885321" MODIFIED="1377182298901" TEXT="2-11 GHz (LOS y NLOS)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368724058391" ID="ID_30547894" MODIFIED="1368724068660" TEXT="Compatible con 802.11a"/>
<node CREATED="1368724079052" ID="ID_1135726786" MODIFIED="1381332135286" TEXT="Soporta canales de distinto ancho de banda">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368724109461" ID="ID_1160501838" MODIFIED="1381332141386" TEXT="Canales de hasta 75 Mbps compartidos en 20 MHz">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368724129100" ID="ID_297922334" MODIFIED="1377182189444" TEXT="Modulaci&#xf3;n OFDM con 256 subportadoras">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368724214063" ID="ID_1171885768" MODIFIED="1382617957161" TEXT="Definici&#xf3;n de canales duplex ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368724232901" ID="ID_751029726" MODIFIED="1377182279125" TEXT="Divisi&#xf3;n en frecuencia (FDD)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368724248956" ID="ID_753652450" MODIFIED="1377182284120" TEXT="Divisi&#xf3;n en el tiempo (TDD)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368724345611" ID="ID_1956007353" MODIFIED="1377182209249" TEXT="Alcance de hasta 50 Km NLOS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368724370343" ID="ID_1712919574" MODIFIED="1377182310817" TEXT="Punto a punto y malla">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369054201148" ID="ID_1962919288" MODIFIED="1383413624537" TEXT="Ofrece QoS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368724427904" ID="ID_1872659727" MODIFIED="1377182308259" TEXT="Soporta antenas MIMO">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368724450971" ID="ID_1637400971" MODIFIED="1383413629231" TEXT="Seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368724455694" ID="ID_1947705449" MODIFIED="1377187889737" TEXT="Cifrado T-DES, AES y RSA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368724477142" ID="ID_830529063" MODIFIED="1382618530984" TEXT="Certificados x509">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</map>
