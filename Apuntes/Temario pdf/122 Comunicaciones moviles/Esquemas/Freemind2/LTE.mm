<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1378656317126" ID="ID_1855422657" MODIFIED="1381340964290" TEXT="LTE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1373623981493" ID="ID_1277650186" MODIFIED="1384776404363" POSITION="right" TEXT="LTE (Long Term Evolution)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      A diferencia de GSM, UTMS y HSPx, tanto LTE como IMT utilizan canales de ancho de banda adaptativo y OFDM.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1373624011791" ID="ID_1064780547" MODIFIED="1384773792316" TEXT="OFDMA en bajada (se abandona WCDMA)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1373624062337" ID="ID_1554587875" MODIFIED="1377190249423" TEXT="DFTS-OFDM (portadora simple FDMA) en subida">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1373624078378" ID="ID_1823407159" MODIFIED="1384773857067" TEXT="Ancho de banda adaptativo hasta 20 MHz (Igual que Wimax, en UMTS eran 5 MHz)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1373624096731" ID="ID_994876659" MODIFIED="1377181830185" TEXT="Hasta 500 Usuarios por celda">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1373624036512" ID="ID_307164734" MODIFIED="1377181855317" TEXT="MIMO">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1373624127116" ID="ID_714055080" MODIFIED="1377181859466" TEXT="Bajada: 326,5 Mbps para 4x4 antenas, 172,28 Mbps para 2x2 antenas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1373624151197" ID="ID_989863458" MODIFIED="1377181862461" TEXT="Subida: 86,5 Mbps">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1373624203919" ID="ID_634580939" MODIFIED="1383417971919" TEXT="Mayor eficiencia espectral">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1373624234040" ID="ID_1710087871" MODIFIED="1383417973474" TEXT="Modulaci&#xf3;n QPSK, 16QAM, 64QAM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1373625634388" ID="ID_1169273328" MODIFIED="1377181867625" TEXT="Ultima versi&#xf3;n 8">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378653608819" ID="ID_181368156" MODIFIED="1378654131050" TEXT="Arquitectura (SAE)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378653637376" ID="ID_1137037209" MODIFIED="1378654179357" TEXT="Evolved UTRAN (E-UTRAN)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378653645371" ID="ID_1488325427" MODIFIED="1378653808392" TEXT="Se elimina RNC de UMTS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378653656310" ID="ID_1386693127" MODIFIED="1378653804748" TEXT="Los nodos B pasan a ser evolved nodos-B (eNB)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378655927904" ID="ID_193382883" MODIFIED="1383419777380" TEXT="La se&#xf1;alizaci&#xf3;n se comunican con MME">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378655940057" ID="ID_1239802588" MODIFIED="1383419779041" TEXT="El tr&#xe1;fico se comunica con  SGW">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378655485904" ID="ID_264342535" MODIFIED="1385159646962" TEXT="Evolved Packet Core (EPC)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378656056357" ID="ID_1242761421" MODIFIED="1381340944872" TEXT="HSS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378656063655" ID="ID_1212350230" MODIFIED="1383419780615" TEXT="se corresponde con HLR">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378655517945" ID="ID_1004486422" MODIFIED="1378655790583" TEXT="MME (Mobility Management Entity)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378656075266" ID="ID_836076485" MODIFIED="1383419781912" TEXT="Se corresponde con VLR">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378656187037" ID="ID_1470913949" MODIFIED="1383419783467" TEXT="Controla las operaciones de alto nivel">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378655523688" ID="ID_1071937964" MODIFIED="1378655788372" TEXT="SAE-G">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378655541035" ID="ID_1548159760" MODIFIED="1378655796678" TEXT="SGW">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378655640827" ID="ID_1805879924" MODIFIED="1383419788893" TEXT="Conexiones con MME y eUTRAN ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378656122309" ID="ID_46741854" MODIFIED="1383419789618" TEXT="Pasa los datos entre eNB y PGW">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378655555906" ID="ID_655945003" MODIFIED="1378655971305" TEXT="PGW">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378655659750" ID="ID_323394770" MODIFIED="1383419790327" TEXT="Conexi&#xf3;n con redes externas de paquetes (IP)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1378655857630" ID="ID_177092140" MODIFIED="1378655867962">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="lte_epc.jpg" />
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1378656292494" ID="ID_726049080" MODIFIED="1378656304033">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="lte_architecture.jpg" />
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</map>
