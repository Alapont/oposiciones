<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1377169732752" ID="ID_1787367002" MODIFIED="1377173237922" TEXT="GSM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368781268974" ID="ID_1311895871" MODIFIED="1384874254049" POSITION="right" TEXT="GSM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368892776767" ID="ID_1867717017" MODIFIED="1391208215416" TEXT="Caracter&#xed;sticas (ETSI)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368892783297" ID="ID_845944153" MODIFIED="1377185107923" TEXT="Reutilizaci&#xf3;n de frecuencias sectorizadas a 120&#xba;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368892849238" ID="ID_1552956107" MODIFIED="1377172863899" TEXT="Bandas (25MHz)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368892870313" ID="ID_789491089" MODIFIED="1377172866498" TEXT="890-915 MHz ascendente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368892889719" ID="ID_1067885527" MODIFIED="1377172868882" TEXT="935-960 MHz descendente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382634915840" ID="ID_2873336" MODIFIED="1382635100793" TEXT="Espaciado duplex entre canales de 45 MHz (desde 890 a 935)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368892912808" ID="ID_1257782879" MODIFIED="1377354801462" TEXT="125 radiocanales FDMA de 200 KHz en cada banda (uno es de guarda)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Esto es, acceso m&#250;ltiple por divisi&#243;n en la frecuencia y en el tiempo (FDMA + TDMA).
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369070625836" ID="ID_1838308128" MODIFIED="1378382502245" TEXT="De cada canal, una &#xfa;nica portadora de 270,833 Kbps (GMSK)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368892948047" ID="ID_449005620" MODIFIED="1377354817293" TEXT="8 slots de tiempo (usuario) en cada portadora de 270,833Kbps (TDMA)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377172900123" ID="ID_1156950284" MODIFIED="1384771116785" TEXT="RLE-LPT 13 Kbps de voz o 9,6 Kbps de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368892995823" ID="ID_252539666" MODIFIED="1378651114175" TEXT="Canales para voz y datos (2400 a 9600 Kbps CSD)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369070789892" ID="ID_761801495" MODIFIED="1377173152310" TEXT="n&#xba; de canales disponibles = 124 x 8 = 992 usuarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368893015933" ID="ID_1116751885" MODIFIED="1378651106048" TEXT="Protecci&#xf3;n mediante salto en RF">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375604462148" ID="ID_242900030" MODIFIED="1378651109495" TEXT="Transmisi&#xf3;n discontin&#xfa;a: s&#xf3;lo se transmite cuando se habla">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368893033080" ID="ID_109403484" MODIFIED="1384771027015" TEXT="Supresi&#xf3;n de silencios (DTX)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1368893045310" ID="ID_1800653902" MODIFIED="1377173131965" TEXT="Localizaci&#xf3;n mediante &quot;paging&quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368893073574" ID="ID_520550780" MODIFIED="1384771033352" TEXT="Circuitos conmutados extremo a extremo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375606786544" ID="ID_1263681284" MODIFIED="1377173139526" TEXT="Encriptado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375606791619" ID="ID_1207947615" MODIFIED="1384771045063" TEXT="A5/1: El m&#xe1;s antiguo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375606805993" ID="ID_108239086" MODIFIED="1380128012890" TEXT="A5/2. El m&#xe1;s d&#xe9;bil, roto con ataques de tabla de arcoiris.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375606859795" ID="ID_104389594" MODIFIED="1377173141813" TEXT="A5/3 (KASUMI)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1368892743815" ID="ID_383705134" MODIFIED="1384874656436" TEXT="Arquitectura">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368781339046" ID="ID_70371882" MODIFIED="1368781342478" TEXT="MS"/>
<node CREATED="1368781291742" ID="ID_465332101" MODIFIED="1368781295014" TEXT="BSS">
<node CREATED="1368781348685" ID="ID_276699924" MODIFIED="1368781352190" TEXT="BTS"/>
<node CREATED="1368781363605" ID="ID_1658188648" MODIFIED="1368781388661" TEXT="BSC"/>
</node>
<node CREATED="1368781295669" ID="ID_1015203579" MODIFIED="1391185672387" TEXT="NSS">
<node CREATED="1368781399349" ID="ID_762247407" MODIFIED="1368781409191" TEXT="MSC">
<node CREATED="1368781441990" ID="ID_1066023155" MODIFIED="1377173180001" TEXT="HLR: Registro de abonado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375463827421" ID="ID_395344356" MODIFIED="1375607005705" STYLE="bubble" TEXT="Aqu&#xed; se centralizan los servicios suplementarios"/>
<node CREATED="1378571499812" ID="ID_81352452" MODIFIED="1381397687083" TEXT="MSISDN: N&#xfa;mero que identifica al abonado en la red   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368781469973" ID="ID_499215100" MODIFIED="1377173181423" TEXT="VLR: Registro de abonado visitante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368781508605" ID="ID_1784731446" MODIFIED="1377173183103" TEXT="AUC: Centro de Autenticaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368781538037" ID="ID_529618812" MODIFIED="1377173184744" TEXT="EIR: Registro de Identificaci&#xf3;n de Equipo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368781573395" ID="ID_851425070" MODIFIED="1377173186568" TEXT="IMEI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368781587542" ID="ID_590391429" MODIFIED="1377173188408" TEXT="lista gris">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368781598493" ID="ID_479447903" MODIFIED="1377173190680" TEXT="lista negra">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1368781614565" ID="ID_347865126" MODIFIED="1368781617552" TEXT="OMC"/>
</node>
<node CREATED="1368781642621" ID="ID_36312644" MODIFIED="1377173176119" TEXT="SIM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368893322474" ID="ID_976223880" MODIFIED="1377173363295" TEXT="N&#xfa;mero de serie">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368893328366" ID="ID_1510383489" MODIFIED="1384771537036" TEXT="Estado de SIM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368893333500" ID="ID_1749310074" MODIFIED="1384771539957" TEXT="Clave de algoritmo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368893345034" ID="ID_263809850" MODIFIED="1384771541548" TEXT="Algoritmo de autenticaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368893366505" ID="ID_53921917" MODIFIED="1377173358447" TEXT="Identificaci&#xf3;n internacional (IMSI)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368893376710" ID="ID_1889735569" MODIFIED="1377173360663" TEXT="PIN">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368811483775" ID="ID_294974205" MODIFIED="1384874936863" TEXT="USSD (Unstructured Supplementary Service Data)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375607816019" ID="ID_950388134" MODIFIED="1375607830928" TEXT="Servicio de mensajes cortos parecido a SMS"/>
<node CREATED="1375607831367" ID="ID_873963218" MODIFIED="1375607847518" TEXT="A diferencia de SMS no dispone de un centro de almacenamiento intermedio"/>
</node>
</node>
<node CREATED="1368893103202" ID="ID_1711368800" MODIFIED="1384874942198" TEXT="Se&#xf1;alizaci&#xf3;n CCS7">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368893145823" ID="ID_1023529294" MODIFIED="1377173216809" TEXT="MAP (Mobile Application Part)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368893169190" ID="ID_416645859" MODIFIED="1377173392200" TEXT="Registro y cancelaci&#xf3;n de localizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368893205954" ID="ID_1434502322" MODIFIED="1384772079435" TEXT="Gesti&#xf3;n de los servicios de abonado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368893217364" ID="ID_1011337858" MODIFIED="1384772082025" TEXT="Par&#xe1;metros de abonado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368893224364" ID="ID_1358934725" MODIFIED="1377173225914" TEXT="Handover">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375475343175" ID="ID_1784904807" MODIFIED="1377173230129" STYLE="bubble" TEXT="CAMEL: tecnolog&#xed;a para el roaming internacional">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375349719949" ID="ID_358138575" MODIFIED="1381397657578" TEXT="Implantado en MSC, HLR, VLR y EIR   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368893284006" ID="ID_595873026" MODIFIED="1368893303680" TEXT="Telefon&#xed;a b&#xe1;sica: TUP"/>
<node CREATED="1368893304293" ID="ID_1429693996" MODIFIED="1368893314215" TEXT="RDSI: ISUP"/>
</node>
</node>
</node>
</map>
