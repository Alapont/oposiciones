<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1368780543270" ID="ID_748185104" MODIFIED="1377172741436" TEXT="M&#xf3;viles">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368780595156" FOLDED="true" ID="ID_1582485635" MODIFIED="1391639145420" POSITION="right" TEXT="DECT">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369055573509" ID="ID_1310042932" MODIFIED="1369055581729" TEXT="TDMA/TDD"/>
<node CREATED="1369055582292" ID="ID_965027092" MODIFIED="1382634171310" TEXT="alta calidad de voz por modulaci&#xf3;n ADPCM (G.726)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375349286988" ID="ID_664467395" MODIFIED="1382634163932" TEXT="Altas densidades de tr&#xe1;fico (m&#xe1;s de 10000 erlang/km2 frente a los 2000 del DCS)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375349339824" ID="ID_1067719659" MODIFIED="1375349364261" TEXT="10 frecuencias entre 1880 y 1900 MHz"/>
<node CREATED="1375349411010" ID="ID_1978865588" MODIFIED="1375349424503" TEXT="Alcance inferior a 100 m en entornos cerrados"/>
<node CREATED="1375349364376" ID="ID_1336862010" MODIFIED="1375349374949" TEXT="24 timeslots por portadora"/>
</node>
<node CREATED="1368780623246" FOLDED="true" ID="ID_508158795" MODIFIED="1391619385407" POSITION="left" TEXT="Transmision duplex">
<node CREATED="1368780675662" ID="ID_137460670" MODIFIED="1368780679510" TEXT="FDD">
<node CREATED="1377189493512" ID="ID_819306435" MODIFIED="1377189499562" TEXT="UMTS"/>
</node>
<node CREATED="1368780679870" ID="ID_786069513" MODIFIED="1368780683414" TEXT="TDD">
<node CREATED="1377189488380" ID="ID_1223796072" MODIFIED="1377189492156" TEXT="UMTS"/>
<node CREATED="1377189585542" ID="ID_1020426223" MODIFIED="1377189590137" TEXT="DECT"/>
</node>
</node>
<node CREATED="1368780687717" FOLDED="true" ID="ID_1213503121" MODIFIED="1391619401834" POSITION="left" TEXT="T&#xe9;cnicas de Acceso M&#xfa;ltiple o Multiplexaci&#xf3;n">
<node CREATED="1368780722274" ID="ID_1314252907" MODIFIED="1368780729337" TEXT="FDMA">
<node CREATED="1377189480671" ID="ID_1624715472" MODIFIED="1377189483317" TEXT="GSM"/>
<node CREATED="1377190178354" ID="ID_20418120" MODIFIED="1377190188013" TEXT="LTE para la subida"/>
</node>
<node CREATED="1368780739509" ID="ID_486331403" MODIFIED="1368780743206" TEXT="TDMA">
<node CREATED="1377189476003" ID="ID_394330257" MODIFIED="1377189478498" TEXT="GSM"/>
<node CREATED="1377189543393" ID="ID_639816616" MODIFIED="1377189550566" TEXT="Wimax"/>
<node CREATED="1377189568068" ID="ID_528226610" MODIFIED="1377189576706" TEXT="DECT"/>
</node>
<node CREATED="1368780743646" ID="ID_1271443297" MODIFIED="1368780747038" TEXT="CDMA">
<node CREATED="1377189470639" ID="ID_933509675" MODIFIED="1377189474973" TEXT="UMTS"/>
</node>
</node>
<node CREATED="1368781241654" ID="ID_211476020" MODIFIED="1377172709979" POSITION="right" TEXT="TACS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375357235053" ID="ID_1411809131" MODIFIED="1380127462059" TEXT="NMT (en los 80)"/>
</node>
<node CREATED="1377169775802" ID="ID_885772097" LINK="GSM.mm" MODIFIED="1377172718852" POSITION="right" TEXT="GSM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375357934893" FOLDED="true" ID="ID_1366524592" MODIFIED="1391615714617" POSITION="right" TEXT="DCS 1800">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375357941352" ID="ID_1366238624" MODIFIED="1383416138579" TEXT="Duplica la frecuencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375357951536" ID="ID_612864302" MODIFIED="1383416140832" TEXT="Triplica el n&#xfa;mero de radiocanales (375) de GSM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375477210997" ID="ID_841462446" MODIFIED="1383416141639" TEXT="Triplica el ancho de banda (75 Mhz)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375477259094" ID="ID_769565812" MODIFIED="1383416142516" TEXT="200 Khz por radiocanal, al igual que GSM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368781756326" FOLDED="true" ID="ID_1129305999" MODIFIED="1391615713248" POSITION="right" TEXT="WAP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368781900374" ID="ID_464859612" MODIFIED="1377180243147" TEXT="Lenguaje WML">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368781922949" ID="ID_1604743013" MODIFIED="1369069111651" TEXT="Protocolo">
<node CREATED="1368781796076" ID="ID_568853344" MODIFIED="1377180245862" TEXT="Aplicaci&#xf3;n (WAE)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368781808934" ID="ID_1781198381" MODIFIED="1377180248529" TEXT="Sesi&#xf3;n (WSP)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368781824069" ID="ID_1027995467" MODIFIED="1377180251915" TEXT="Transacciones (WTP)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368781837557" ID="ID_1742519543" MODIFIED="1377180254489" TEXT="Seguridad (WTLS)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368781847254" ID="ID_1589956702" MODIFIED="1377180257297" TEXT="Transporte (WDP)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1368781990870" FOLDED="true" ID="ID_516973798" MODIFIED="1391640269393" POSITION="right" TEXT="GPRS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368782016029" ID="ID_760826888" MODIFIED="1391187338485" TEXT="Arquitectura">
<node CREATED="1368782025246" ID="ID_1999170878" MODIFIED="1368809506097" TEXT="GGSN: Pasarela con redes de datos externa"/>
<node CREATED="1368782033709" ID="ID_892991173" MODIFIED="1368809476579" TEXT="SGSN: Serving Gateway"/>
<node CREATED="1375044893866" ID="ID_1026154963" MODIFIED="1375044937765" TEXT="Gn: interfaz entre los anteriones si est&#xe1;n situados en la misma red m&#xf3;vil"/>
<node CREATED="1375045127358" ID="ID_1373788179" MODIFIED="1375045146972" TEXT="Gi: entre GGSN y otra red"/>
<node CREATED="1375045073522" ID="ID_1986295475" MODIFIED="1375045105203">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="300px-GPRS_core_structure.png" />
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1368809421064" ID="ID_956514746" MODIFIED="1384875124587" TEXT="Funcionamiento">
<node CREATED="1368809432186" ID="ID_1720719278" MODIFIED="1384772490134" TEXT="Modo datagrama (paquetes)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369071102287" ID="ID_1539671998" MODIFIED="1384772488124" TEXT="Conmutaci&#xf3;n a nivel de paquete">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368809441381" ID="ID_1075855460" MODIFIED="1380128514518" TEXT="Transmisi&#xf3;n a intervalos de tiempo IRREGULARES">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368809587879" ID="ID_514241949" MODIFIED="1384772485783" TEXT="Aprovecha acceso radio de GSM, si bien requiere infraestructura de conmutaci&#xf3;n de paquetes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368809602770" ID="ID_321325023" MODIFIED="1382635641720" TEXT="Multiplexaci&#xf3;n de voz y datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369071112968" ID="ID_698040545" MODIFIED="1380128510395" TEXT="Velocidad m&#xe1;xima 172 Kbps, t&#xed;pica de 115 kbps">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1368782688574" FOLDED="true" ID="ID_75463704" MODIFIED="1391640277256" POSITION="right" TEXT="EDGE (Enhanced Data for GSM Evolution)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375290527485" ID="ID_1182793253" MODIFIED="1375290535628" TEXT="Uso en EEUU"/>
<node CREATED="1375358044252" ID="ID_1634461484" MODIFIED="1375358049506" TEXT="2.75 G"/>
<node CREATED="1375290536147" ID="ID_1837102309" MODIFIED="1375290542524" TEXT="Hasta 384 Kbps"/>
<node CREATED="1375358051626" ID="ID_845769011" MODIFIED="1375358060625" TEXT="Puede usar GPRS"/>
</node>
<node CREATED="1378570341898" ID="ID_724324487" MODIFIED="1391613823496" POSITION="right" TEXT="IMT-2000 (International Mobile Telecommunications)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377169837729" ID="ID_517822315" LINK="UMTS.mm" MODIFIED="1391188219806" TEXT="UMTS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375636336734" ID="ID_950139792" MODIFIED="1377172736093" TEXT="VHE (Virtual Home Environment)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375636352669" ID="ID_1792494590" MODIFIED="1375636379231" TEXT="Entorno personalizado para el usuario (aplicaciones y servicios)"/>
<node CREATED="1375636382186" ID="ID_545128621" MODIFIED="1375636401360" TEXT="Independiente de la plataforma (Bluetooth, GSM, UMTS)"/>
<node CREATED="1375636402614" ID="ID_1432274121" MODIFIED="1375636412772" TEXT="Independencia geogr&#xe1;fica"/>
</node>
</node>
</node>
<node CREATED="1377169987315" ID="ID_1236391323" LINK="HSx.mm" MODIFIED="1377172723796" POSITION="right" TEXT="HSx">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378656390671" ID="ID_156815913" LINK="LTE.mm" MODIFIED="1378656421226" POSITION="right" TEXT="LTE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1373625309262" ID="ID_1385784946" MODIFIED="1391619352600" POSITION="right" TEXT="IMT Advanced">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      En un principio, la UIT especific&#243; las tecnolog&#237;as que pod&#237;an considerarse 4G, incluyendo LTE, WiMAX y HSPA+, para no entrar en pol&#233;micas, ya no habla de 4G, sino de IMT-2000 e IMT-Advanced. Esto es, aunque para ellos esto es lo &#250;nico que es 4G, admiten que otras tecnolog&#237;as 3G evolucionadas tambi&#233;n puedan llarse as&#237; por motivos de marketing.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1373625319029" ID="ID_1494300097" MODIFIED="1377181884972" TEXT="1 Gbps sin movimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1373625435675" ID="ID_863906865" MODIFIED="1377181889184" TEXT="100 Mbps en movimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1373625411547" ID="ID_99590094" MODIFIED="1377182013549" TEXT="Ancho de banda escalable hasta 40 MHz">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1373625473261" ID="ID_703648285" MODIFIED="1384776852078" TEXT="Tecnolog&#xed;as &quot;True 4G&quot;">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Para diferenciarlas de 4G, que son
    </p>
    <p>
      LTE, WIMAX y HSPA+.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1373625477668" ID="ID_1083467635" MODIFIED="1377181881961" TEXT="LTE Advanced (versi&#xf3;n 10 LTE)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1373625495790" ID="ID_1943771936" MODIFIED="1378571930305" TEXT="Wimax Advanced">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375434641055" ID="ID_1806867242" MODIFIED="1391619400536" POSITION="left" TEXT="Tecnicas de localizaci&#xf3;n seg&#xfa;n m&#xf3;vil">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375434699575" ID="ID_1386475883" MODIFIED="1377181921805" TEXT="CGI (Identidad Global Celular): Por la c&#xe9;lula donde se encuentra">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375434727672" ID="ID_1086155478" MODIFIED="1382638687411" TEXT="AOA: por el &#xe1;ngulo de llegada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375434753233" ID="ID_492598313" MODIFIED="1377181924785" TEXT="TOA: por el tiempo de llegada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375434775823" ID="ID_1990778566" MODIFIED="1377181927639" TEXT="TDOA: diferencia en el tiempo de llegada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375434792556" ID="ID_1849575042" MODIFIED="1377181930338" TEXT="MF. Huella multitrayecto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375434832502" ID="ID_951897194" MODIFIED="1377181933006" TEXT="TDOA o E-OTD. Diferencia en el tiempo de llegada modificado.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375434866847" ID="ID_1495095525" MODIFIED="1377181936063" TEXT="A-FLT. Triangulaci&#xf3;n avanzada de enlace">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375434886922" ID="ID_1369804666" MODIFIED="1375434888837" TEXT="GPS"/>
<node CREATED="1375434889333" ID="ID_741590067" MODIFIED="1378648591111" TEXT="GPS-A. GPS Avanzado.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375555773403" ID="ID_915986294" MODIFIED="1377172734197" POSITION="left" TEXT="SIP: Se&#xf1;alizaci&#xf3;n en Voz sobre IP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375555797517" ID="ID_242201003" MODIFIED="1375555800718" TEXT="GPRS"/>
<node CREATED="1375555801171" ID="ID_969266837" MODIFIED="1375555804289" TEXT="UMTS"/>
</node>
</node>
</map>
