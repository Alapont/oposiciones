<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1441827653546" ID="ID_1715109443" MODIFIED="1441827668233">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T108 COMS
    </p>
    <p style="text-align: center">
      M&#211;VILES
    </p>
  </body>
</html></richcontent>
<node CREATED="1441827672020" ID="ID_1433079961" MODIFIED="1441872924358" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Radiocoms
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441871576345" ID="ID_1067605571" MODIFIED="1441873073768">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Coms entre 2 ptos fijos/m&#243;v por medio de ondas hertzianas/RF a trav&#233;s del espacio radioel&#233;ctr (aire)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
</node>
<node CREATED="1441871789829" ID="ID_1126407012" MODIFIED="1441871794503" TEXT="Trunking">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441871794513" ID="ID_673593575" MODIFIED="1446020037884">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Grupo <u>cerrado</u>&#160;(priv)&#8594;&#160;servs emerg/corp. Colas sin p&#233;rdidas, mux freq y estruct celular
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1446019859947" ID="ID_1605294305" MODIFIED="1446020002466">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Modos:</i>&#160;normal, directo o repetidor
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441871901180" ID="ID_1766103284" MODIFIED="1446020011315">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>TETRA:&#160;</i>TransEU Trunked Radio
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1441827680223" ID="ID_610844061" MODIFIED="1441827682310" TEXT="Ppios">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441799319289" ID="ID_412097410" MODIFIED="1446020938143">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>RF&#8594; Radiaci&#243;n:</i>&#160;long onda&lt;elem radiante / <i>Att:</i>&#160;prop a dist, freq, ags meteo e indust
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1446020402425" ID="ID_1599190237" MODIFIED="1446020960641">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      +Freq&#8594; -alcance/+att y disps consumen+
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1441872120936" ID="ID_134935158" MODIFIED="1446020763552">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Espectro RE</i>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441872180667" ID="ID_1561445293" MODIFIED="1441881718042">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>3kHz-300GHz</u>&#160;freqs se tx por el aire, div en bandas asociadas a servs (planif en<u>CNAF </u>)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1446021061435" ID="ID_367119004" MODIFIED="1446021076769">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Microondas:</i>&#160;X, Ku, K, Ka
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441872234391" ID="ID_1321833777" MODIFIED="1446020112815">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>VLF (3-30kHz): </i>onda guiada Tierra-Ionosf (naveg)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441872354181" ID="ID_1135540571" MODIFIED="1446027805419">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>L (30-300kHz):&#160;</i>onda Tierra/Superf
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1441872527217" ID="ID_1621866360" MODIFIED="1446020209446">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>M/HF (300kHz-30MHz):</i>&#160;onda Ionosf (radio y pto a pto)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1441872605991" ID="ID_1978574615" MODIFIED="1446020387569">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>V/<u>UHF</u>&#160;(30MHz-3GHz, <u>tv-mvl</u>):</i>&#160;onda Espacio (<u>multitray</u>&#8594;&#160;se&#241;al rx=dir+reflej+difract) + Refr Troposf
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<node CREATED="1446021521420" ID="ID_1634638537" MODIFIED="1446021694197" TEXT="-interfs y terminales peq">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1441873744159" ID="ID_1375114312" MODIFIED="1441874286458" TEXT="T&#xe9;cnicas">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441873868632" ID="ID_472585419" MODIFIED="1446022132311">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Duplexaci&#243;n:&#160;</i>(canal bidir)<i>&#160;</i>FDD (2 freqs&#8594;&#160;tx/rx) o TDD (1 freq, 2 ranuras temp tx/rx-semiduplex)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1446026805701" ID="ID_1766458399" MODIFIED="1446026825291" TEXT="Freq asc &lt;= freq desc">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1441873950310" ID="ID_940556980" MODIFIED="1446027215451">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Acceso m&#250;ltiple:</i>&#160;FDMA, TDMA o SSMA (espectr ensanch&#8594; DSS=CDMA-div de c&#243;d o FHSS-salto freq)&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1441827694737" ID="ID_521987472" MODIFIED="1441875326636" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Coms inal&#225;mb
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441875211726" ID="ID_996381384" MODIFIED="1446021020824">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Uso <u>dom&#233;stico</u>&#8594; tx datos/tfn y dom&#243;tica (redes W/PAN, &lt;100m)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441875257090" ID="ID_619426984" MODIFIED="1446020988428">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i><u>DECT</u>: </i>(Dig Enh Cordless Telco) est&#225;ndar de acceso&#160;a redes fijas, 1800-1900MHz, TDMA/TDD/CDCS
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1441881005667" ID="ID_779592803" MODIFIED="1441881007675" POSITION="right" TEXT="WAP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441881007680" ID="ID_613685653" MODIFIED="1446025041292">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Wireless App Prot) <u>acceso a Inet</u>&#160;dsd disps m&#243;v por pasarela o serv WTA (Wireles Tfn App)&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1441881063973" ID="ID_1626332303" MODIFIED="1446021137323">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>WML: </i>(Wireless, similar a HTML) <u>present</u>&#160;en terminales y proceso reducido, naveg por <u>cards</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1441827807456" ID="ID_330909891" MODIFIED="1441881141790" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sat&#233;lites
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441881150300" ID="ID_946832126" MODIFIED="1446027316859">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>&#211;rbitas: </i>LEO (Low Earth &lt;3000km, pe Iridium, GlobalStar), MEO (Medium) y GEO (Geoestat)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1446021148713" ID="ID_1756795756" MODIFIED="1446021190759">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      A -&#243;rbita, +sats (van +r&#225;pido para aguantarse&#8594; cambian antes de cobertura)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1441827701493" ID="ID_1874966092" MODIFIED="1446022307870" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      TMA
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1446021761342" ID="ID_1053722993" MODIFIED="1446022065463" TEXT="Intro">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441875363849" ID="ID_823086524" MODIFIED="1446025037709">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Tfn M&#243;vil Autom) s&#170;&#160; <u>concentraci&#243;n</u>&#160;de tfn p&#250;b&#8594; <u>interfaz</u>&#160;entre terminales y red tfn p&#250;b conmut (RTC)
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1446021773663" ID="ID_1521763832" MODIFIED="1446022009550">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>C&#233;lulas hexag</u>&#8594;3 antenas (120&#186;). Muchos users, ctrl por P y alcance
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441875727929" ID="ID_1129946386" MODIFIED="1441875789742">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Reut de freqs</u>&#160;en celdas no adyacs. <i>Cl&#250;ster:</i>&#160;celdas dnd no se repiten freqs
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1446021927625" ID="ID_190257597" MODIFIED="1446026503480">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Handover:&#160;</i>paso entre celdas (por mov, baja cal o congest)&#8594; Hard (cambio freq) o Soft (=freq)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1441827708229" ID="ID_198407118" MODIFIED="1441882066207">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1G
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441882066213" ID="ID_343850305" MODIFIED="1441882066232">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      TACS
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441875937821" ID="ID_937547002" MODIFIED="1446027459535">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Total Acc Comms Systs, UK 1985) tfn&#160; <u>anal&#243;g</u>&#160;obsoleta, 900MHz (25kHz/ch), FM
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1441827719937" ID="ID_1102361481" MODIFIED="1441881952578">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      2G
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441881952583" ID="ID_684832242" MODIFIED="1446026208209">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>GSM</b>
    </p>
    <p>
      (ETSI)
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441876095855" ID="ID_1598075321" MODIFIED="1446027834448">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Global Sys Mob)&#160;conmut &#160;<b>circs</b>&#160;(voz, reserv recs), <u>900MHz</u>&#160;(200kHz/ch), <u>TD/FDMA</u>,&#160;SS7, <u>GMSK</u>&#160;(Gauss)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1441876376441" ID="ID_135181217" MODIFIED="1446027888943">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>DTX</u>&#160;(tx si habla), IMEI/IMSI/SIM, auten, pago-dur/dest&#160;&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1446022546514" ID="ID_289146029" MODIFIED="1446022548566" TEXT="Canales">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441876198175" ID="ID_653755571" MODIFIED="1446027408261">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Voz (TCH-Traffic): </i><u>13 kbps</u>/8 users (Full-rate) o 6,5 kbps/16 users (Half)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1446022567408" ID="ID_929306187" MODIFIED="1446022581351">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tb permite datos:<i>&#160;</i><u>9,6 kbps</u>&#160;(poco &#250;til)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441876268955" ID="ID_1540091535" MODIFIED="1446022632755">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ctrl Com&#250;n:&#160;</i>BC (broadc), P (paging, tx a un disp), RA (random acc, pide conex), AG (granted)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441876311806" ID="ID_1105043460" MODIFIED="1446027856795">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Dedic:&#160;</i>SDC (llamada), S/FAC (slow/fast assoc, cambios)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1441876914783" ID="ID_1111621999" MODIFIED="1446024016747">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Arq</i>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1446023107027" ID="ID_447928919" MODIFIED="1446023602305">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Base Station Subsys (BSS):</i>&#160;&#160;Base Transceiver St (BTS), Base St Ctrl (BSC)
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1446023294506" ID="ID_399410347" MODIFIED="1446023649732">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Netw Switch </i>
    </p>
    <p>
      <i>Subsys (NSS)</i>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1446023487677" ID="ID_463043373" MODIFIED="1446027532500">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mobile Sw Center (MSC, <u>centralita</u>), Home Loc Reg (HLR, info <u>est&#225;t</u>&#160;abonado),
    </p>
    <p>
      Visitor Loc (VLR, info&#160; <u>temp</u>), Autent Center (AuC), EIR (Equip Id (EIR, IMEIs)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1446023150943" ID="ID_225479187" MODIFIED="1446023640504">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Op &amp; Maintenance Center (OMC):&#160; </i><u>adm</u>, seg, monit&#160;y config de toda la red
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1441877056219" ID="ID_857498424" MODIFIED="1441877100547">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>DCS 1800: </i>variante, resuelve escasez de freqs usando banda de<u>1800MHz</u>&#160;(celdas +peq)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441827724413" ID="ID_1170184375" MODIFIED="1446024472441">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      2.5G
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441875879061" ID="ID_1821127947" MODIFIED="1441877199659">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      HSCSD
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441877137206" ID="ID_1801767769" MODIFIED="1446023756821">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (High-Speed Circ Sw Data) =infr, terminal puede reservar 4 slots de t&#8594; <u>4 canales GSM</u>&#160;(57,6 kbps)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441827729254" ID="ID_1002126978" MODIFIED="1441827731508" TEXT="GPRS">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441877264673" ID="ID_407085216" MODIFIED="1446027608607">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Gen Packet Radio Sys) incluye <u>datos</u>&#8594; conmut <b>paqs </b>IP. <u>170&#160;kbps</u>&#160;(115 reales). Pago por vol y QoS
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1441877408090" ID="ID_1300772578" MODIFIED="1446024258321">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Arq</i>
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1446024044392" ID="ID_1715647996" MODIFIED="1446024383640">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Divide infr de red (voz/datos)&#8594; SGSN (Serv GPRS Supp Node, conmut&#160;paqs) y GGSN (GW&#160;a redes ext)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1441877474266" ID="ID_1376231955" MODIFIED="1446025417295">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b><i>EDGE</i></b><i>: </i>(Enhanced Datarates for GSM Evol) mod +eficiente (<u>384 kbps</u>), no implant en EU
    </p>
  </body>
</html></richcontent>
<node CREATED="1446024435915" ID="ID_1356279103" MODIFIED="1446024458536">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b><i>CAMEL</i></b>: (Customized Apps for Mobiles Netw) similar a GPRS
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1441827737220" ID="ID_956733120" MODIFIED="1441882079084">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      3G
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441882079089" ID="ID_1887185031" MODIFIED="1446026224256">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>UMTS</b>
    </p>
    <p>
      (3GPP)
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441878098887" ID="ID_1092197688" MODIFIED="1446027339219">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Univ Mob Telco Sys)&#160; <u>2000MHz</u>&#160;y <u>WCDMA</u><i>&#160;</i>(=DSS&#8594;5MHz/ch)&#8594;sim/FDD o asim/TDD (+vel) seg&#250;n celda
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1446025170600" ID="ID_431685019" MODIFIED="1446025300252">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Convive con GSM (=arq jer&#225;rquica)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441878361004" ID="ID_1921201207" MODIFIED="1446024824640">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Celdas:</i>&#160;<u>superpuestas</u>&#160;(evita handover), tama&#241;o seg&#250;n movilidad y entorno
    </p>
  </body>
</html></richcontent>
<node CREATED="1441878378840" ID="ID_1815239988" MODIFIED="1446025390076">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Abierto-mov alta (megaceldas-sat, <u>144kbps</u>), Urbano-mov media (microceldas,
    </p>
    <p>
      FDD-CDMA,&#160; <u>384kbps</u>) y Urb-mov baja (picoceldas, TDD-CDMA, <u>2Mbps</u>)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441877631043" ID="ID_1499929821" MODIFIED="1446025291055">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Arq:</i>&#160;modif infr de&#160; <u>cliente</u>&#160;(banda ancha)&#8594; NodoB y RNC (Radio Netw Ctrl)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441827765871" ID="ID_1356319000" MODIFIED="1441882097905">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      3.5G
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441882097911" ID="ID_383082909" MODIFIED="1446025574773">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      HSDPA
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441879618742" ID="ID_107704455" MODIFIED="1446025632705">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (High-Speed Downlink Pack Acc) nuevo canal&#160; <u>compartido</u>&#160;(HSD-SC), <u>14Mbps</u>
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1446025587428" ID="ID_949019954" MODIFIED="1446025609962">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      HSPA+ (28 Mbps), HSPA+ MIMO (=HSOPA, 42 Mbps, OFDM)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1446025574753" ID="ID_1554259541" MODIFIED="1446025576620" TEXT="Mecs">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441879754269" ID="ID_1980770267" MODIFIED="1441880588406">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Link adapt: </i>codif/mod&#160; <u>adaptativos</u>&#160;al canal
    </p>
  </body>
</html></richcontent>
<node CREATED="1441879787371" ID="ID_53785537" MODIFIED="1446025558364">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Fast Pack Schedul: </i>Est Base pgm tx paqs a users <u>seg&#250;n calidad</u>&#160;link
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441879985684" ID="ID_1601632375" MODIFIED="1446026366855">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>HARQ: </i>(Hybrid Auto Repeat Req) est retx paqs err&#243;neos (<u>redund increm</u>) con codifics dif para recup
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1441827770027" ID="ID_786967881" MODIFIED="1441882201093">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      4G
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441882201098" ID="ID_9132201" MODIFIED="1446026268834">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>LTE</b>
    </p>
    <p>
      (3GPP)
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441880101039" ID="ID_1972675234" MODIFIED="1446026012914">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Long Term Evol) <u>&#250;nica red</u>&#160;IPv6&#8594; VoIP+datos por conmut&#160;<b>paqs</b>, latencia m&#237;n (stream) y <u>QoS</u>
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
</node>
<node CREATED="1441880463927" ID="ID_1740367445" MODIFIED="1446025890026">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>800-1800-2600MHz</u>, <u>OFDM</u>&#160;(1,25-20MHz/ch)&#8594; sim/FDD o asim/TDD, =mecs que HSDPA
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1441880624338" ID="ID_671939262" MODIFIED="1446026098127">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>MIMO: </i>4x4 antenas&#8594;<i>&#160;</i>86,5 (UL) / <u>326,5 Mbps</u>&#160;(DL) o 2x2&#8594;<i>&#160;</i><u>172,8 Mbps</u>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1441880167994" ID="ID_1252433076" MODIFIED="1446027741312">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Arq:</i>&#160;simple (<u>no jer&#225;rq</u>)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441880187970" ID="ID_1971319352" MODIFIED="1446026002754">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      eNodeB (evol NodoB+RNC), Mov Manag Entity (MME, se&#241;aliz y GW) y Pack Data Netw (GW)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
