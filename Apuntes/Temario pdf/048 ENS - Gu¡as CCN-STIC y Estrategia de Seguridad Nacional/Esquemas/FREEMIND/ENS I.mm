<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1368467165026" ID="ID_1647302942" MODIFIED="1377597139281" TEXT="ENS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425199381076" ID="ID_1608864439" MODIFIED="1425199414227" POSITION="right" TEXT="Articulaci&#xf3;n atendiendo a:">
<node CREATED="1425199416081" ID="ID_1183875637" MODIFIED="1425199443815" TEXT="Ley 11/2007, de 22 de junio, LAECSP"/>
<node CREATED="1425199446694" ID="ID_1468596798" MODIFIED="1425199479234" TEXT="Ley 15/1999, de 13 de diciembre, LOPD"/>
<node CREATED="1425199482474" ID="ID_96066867" MODIFIED="1425199506512" TEXT="Ley 30/1992, de 26 de noviembre, LRJAPAC"/>
<node CREATED="1425199509070" ID="ID_195753616" MODIFIED="1425199530240" TEXT="Ley 37/2007, de 16 de noviembre, Reutilizaci&#xf3;n de info en SP"/>
<node CREATED="1425199540539" ID="ID_1702042593" MODIFIED="1425199576347" TEXT="Otros documentos">
<node CREATED="1425199545563" ID="ID_1299263829" MODIFIED="1425199567053" TEXT="Criterios de Seguridad, Normalizaci&#xf3;n y Conservaci&#xf3;n"/>
<node CREATED="1425199569040" ID="ID_1347582818" MODIFIED="1425199635727">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Gu&#237;as CCN-STIC de Seguridad de los Sistemas de Info. y Comunicaciones
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1425199576348" ID="ID_1822944508" MODIFIED="1425199598675" TEXT="Esquema Nacional de Interoperabilidad"/>
<node CREATED="1425199600552" ID="ID_1638378659" MODIFIED="1425199615366" TEXT="Metodolog&#xed;a y herramientas de an&#xe1;lisis y gesti&#xf3;n de riesgos"/>
</node>
</node>
<node CREATED="1376048503482" ID="ID_1151893080" MODIFIED="1425144302323" POSITION="right" TEXT="Participaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425199680734" ID="ID_1524393685" MODIFIED="1425199687273" TEXT="Todas las AAPP donde es de aplicaci&#xf3;n"/>
<node CREATED="1376048509155" ID="ID_1937815796" MODIFIED="1425199804354">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Organos colegiados
    </p>
    <p>
      (informado favorablemente)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376048514837" ID="ID_1900913779" MODIFIED="1425199749479">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comisi&#243;n Permanente del Consejo Superior de Administraci&#243;n Electr&#243;nica
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376048584334" ID="ID_1230763969" MODIFIED="1425199759575">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comit&#233; Sectorial de Administraci&#243;n Electr&#243;nica (AGE-CCAA)&#160;&#160;
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376048608211" ID="ID_323965856" MODIFIED="1380624815586" TEXT="Comisi&#xf3;n Nacional de Administraci&#xf3;n Local (CNAL)  ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1376048634945" ID="ID_1205123421" MODIFIED="1391796864163" TEXT="Informan">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376048668479" ID="ID_506250282" MODIFIED="1425199886375">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Agencia Espa&#241;ola de Protecci&#243;n de Datos (Sometido al previo informe)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376048644877" ID="ID_182019217" MODIFIED="1383827298964" TEXT="Ministerio de AAPP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376048658545" ID="ID_1536770984" MODIFIED="1383823427364" TEXT="Ministerio de la Presidencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376048679430" ID="ID_35815783" MODIFIED="1383823428894" TEXT="Consejo de Estado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1425199898865" ID="ID_748656135" MODIFIED="1425199974995" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Definici&#243;n seg&#250;n LAECSP (art. 42)
    </p>
  </body>
</html></richcontent>
<node CREATED="1425199959875" ID="ID_313189799" MODIFIED="1425200585419">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Establecer la pol&#237;tica de seguridad en la utilizaci&#243;n de medios electr&#243;nicos &#225;mbito Ley 11/2007<br />
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1425200054802" ID="ID_1360996280" MODIFIED="1425200054802" TEXT="y constituido por:">
<node CREATED="1425200057349" ID="ID_1846839223" MODIFIED="1425200082596">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1. Los principios b&#225;sicos
    </p>
    <p>
      2. Requisitos m&#237;nimos
    </p>
    <p>
      que permitan una protecci&#243;n adecuada de la informaci&#243;n.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1425200266366" ID="ID_232973516" MODIFIED="1425200360582" POSITION="right" TEXT="Cap&#xed;tulo I. Disposiciones Generales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425200364727" ID="ID_1852345787" MODIFIED="1425200366786" TEXT="Objeto">
<node CREATED="1425200371997" ID="ID_298228205" MODIFIED="1425200403966" TEXT="Regular ENS establecido en Ley 11/2007"/>
<node CREATED="1425200408360" ID="ID_335499723" MODIFIED="1425200477596">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Principios b&#225;sicos y requisitos m&#237;nimos para adecuada protecci&#243;n de la info.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1376048454843" ID="ID_1965024894" MODIFIED="1425200360582" TEXT="Ambito">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425200501381" ID="ID_124609702" MODIFIED="1425200518987" TEXT="El establecido por Ley 11/2007"/>
<node CREATED="1376048459095" ID="ID_383459473" MODIFIED="1425200360582" TEXT="Excluidos los sistemas que tratan informaci&#xf3;n clasificada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1368467176874" ID="ID_651782261" MODIFIED="1425204040003" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cap&#237;tulo II. Principios b&#225;sicos
    </p>
  </body>
</html></richcontent>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_651782261" ENDARROW="Default" ENDINCLINATION="-20;173;" ID="Arrow_ID_363605736" SOURCE="ID_779270143" STARTARROW="None" STARTINCLINATION="-186;4;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368467186165" ID="ID_292303425" MODIFIED="1425212798338">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Seguridad integral
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="6_web.png-32x32"/>
<node CREATED="1425200649984" ID="ID_1558651804" MODIFIED="1425200978693">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      La seguridad como un proceso integral
    </p>
    <p>
      constituido por todos los elementos
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368467256397" ID="ID_197727933" MODIFIED="1425200978694" TEXT="t&#xe9;cnicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368467272285" ID="ID_1487358338" MODIFIED="1368467274471" TEXT="humanos"/>
<node CREATED="1368467274836" ID="ID_1098034389" MODIFIED="1368467281421" TEXT="materiales "/>
<node CREATED="1368467282160" ID="ID_1117613769" MODIFIED="1368467285594" TEXT="organizativos"/>
</node>
<node CREATED="1368467301606" ID="ID_1979081554" MODIFIED="1425200978690">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      M&#225;xima atenci&#243;n a concienciaci&#243;n de las personas que intervienen y a sus responsables jer&#225;rquicos
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368467191974" ID="ID_570066612" MODIFIED="1425212798331" TEXT="Gesti&#xf3;n de riesgos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="clanbomber"/>
<node CREATED="1425200990628" ID="ID_1506942542" MODIFIED="1425201024728" TEXT="An&#xe1;lisis y gesti&#xf3;n de riesgos para mantener entorno controlado">
<node CREATED="1425201032550" ID="ID_1342988810" MODIFIED="1425201050205" TEXT="Minimizando los riesgos"/>
</node>
</node>
<node CREATED="1368467197426" ID="ID_1599023273" MODIFIED="1425212798330" TEXT="Prevenci&#xf3;n, reacci&#xf3;n y recuperaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1425201090463" ID="ID_1722298276" MODIFIED="1425201096519" TEXT="Medidas de prevenci&#xf3;n">
<node CREATED="1425201099023" ID="ID_550220171" MODIFIED="1425201121195" TEXT="Deben eliminar o reducir posibilidad de materializaci&#xf3;n de amenazas"/>
<node CREATED="1425201123755" ID="ID_1951424315" MODIFIED="1425201138453" TEXT="Entre otras: disuasi&#xf3;n y reducci&#xf3;n de la exposici&#xf3;n"/>
</node>
<node CREATED="1425201141469" ID="ID_878500888" MODIFIED="1425201150448" TEXT="Medidas de detecci&#xf3;n/reacci&#xf3;n">
<node CREATED="1425201152038" ID="ID_1631757180" MODIFIED="1425201171095" TEXT="Atajar los incidentes a tiempo"/>
</node>
<node CREATED="1425201175070" ID="ID_1016627734" MODIFIED="1425201179089" TEXT="Medidas de recuperaci&#xf3;n">
<node CREATED="1425201183122" ID="ID_199286725" MODIFIED="1425201198830" TEXT="Restauraci&#xf3;n de info. y servicios"/>
</node>
</node>
<node CREATED="1368467210538" ID="ID_477947181" MODIFIED="1425212798325" TEXT="L&#xed;neas de defensa">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Coincide con la ISO 27002, excepto la parte legal.
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="stop-sign"/>
<node CREATED="1384186553116" ID="ID_939847754" MODIFIED="1425201272615">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estrategia de protecci&#243;n por m&#250;tliples capas de defensa,
    </p>
    <p>
      de forma que cuando una falle, permita:
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384186557837" ID="ID_434504329" MODIFIED="1384186615948" TEXT="a) Ganar tiempo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384186564114" ID="ID_1290851023" MODIFIED="1385058572744" TEXT="b) Reducir la probabilidad que el sistema sea comprometido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384186584870" ID="ID_462330411" MODIFIED="1385058568049" TEXT="c) Minimizar el impacto final sobre el mismo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368467349884" ID="ID_92127180" MODIFIED="1377444605264" TEXT="medidas organizativas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368467360248" ID="ID_658503362" MODIFIED="1377444607665" TEXT="medidas f&#xed;sicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368467365966" ID="ID_548563675" MODIFIED="1377444611025" TEXT="medidas l&#xf3;gicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368467218158" ID="ID_400589" MODIFIED="1425212798321" TEXT="Reevaluaci&#xf3;n peri&#xf3;dica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="calendar"/>
<node CREATED="1425201315566" ID="ID_1492672149" MODIFIED="1425201382386" TEXT="Reevaluaci&#xf3;n y actualizaci&#xf3;n peri&#xf3;dicas de las medidas de seguridad">
<node CREATED="1425201384698" ID="ID_410608022" MODIFIED="1425201401789">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Incluso replanteamiento de la seguridad, si necesario
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1368467228805" ID="ID_1694211674" MODIFIED="1397815095711" TEXT="Funci&#xf3;n diferenciada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="group"/>
<node CREATED="1368467431275" ID="ID_1060581324" MODIFIED="1425201434909" TEXT="Responsable de informaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425201452129" ID="ID_1268561478" MODIFIED="1425201457006" TEXT="Req. de la info. tratada"/>
</node>
<node CREATED="1368467440534" ID="ID_661167890" MODIFIED="1425201441621" TEXT="Responsable del servicio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425201459175" ID="ID_1643721727" MODIFIED="1425201472662" TEXT="Requisitos de los servicios prestados"/>
</node>
<node CREATED="1368467448061" ID="ID_1435331690" MODIFIED="1425201445813" TEXT="Responsable de la seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425201476642" ID="ID_1650475009" MODIFIED="1425201486743" TEXT="Satisfacer requisitos de seguridad"/>
</node>
<node CREATED="1425201521601" ID="ID_909489726" MODIFIED="1425201584238" TEXT="Responsabilidad de la seguridad diferenciada de la responsabilidad sobre prestaci&#xf3;n de los servicios "/>
</node>
</node>
<node CREATED="1368467483964" ID="ID_1952801930" MODIFIED="1425203716858" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cap&#237;tulo III. Requisitos m&#237;nimos
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425203716858" ID="ID_150649475" MODIFIED="1425204049342">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pol&#237;tica de Seguridad
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425203740709" ID="ID_1700314033" MODIFIED="1425203889922" TEXT="Deber&#xe1;n disponer de ella los &#xf3;rganos superiores de las AAPP"/>
<node CREATED="1425203762245" ID="ID_133812767" MODIFIED="1425203914588" TEXT="Aprobada por el titular del &#xf3;rgano superior"/>
<node CREATED="1425203969979" ID="ID_779270143" MODIFIED="1425204045275" TEXT="Principios b&#xe1;sicos y requisitos m&#xed;nimos">
<arrowlink DESTINATION="ID_651782261" ENDARROW="Default" ENDINCLINATION="-20;173;" ID="Arrow_ID_363605736" STARTARROW="None" STARTINCLINATION="-186;4;"/>
<arrowlink DESTINATION="ID_1382334710" ENDARROW="Default" ENDINCLINATION="-20;-83;" ID="Arrow_ID_729531401" STARTARROW="None" STARTINCLINATION="-174;0;"/>
</node>
</node>
<node CREATED="1425203922879" ID="ID_1382334710" MODIFIED="1425204045275" TEXT="Requisitos m&#xed;nimos">
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1382334710" ENDARROW="Default" ENDINCLINATION="-20;-83;" ID="Arrow_ID_729531401" SOURCE="ID_779270143" STARTARROW="None" STARTINCLINATION="-174;0;"/>
<node CREATED="1368467495859" ID="ID_55402860" MODIFIED="1425204187208">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Organizaci&#243;n e implantaci&#243;n del proceso de seguridad
    </p>
  </body>
</html></richcontent>
<node CREATED="1425204209126" ID="ID_1424000790" MODIFIED="1425204225035" TEXT="Comprometer a todos los miembros de la organizaci&#xf3;n"/>
</node>
<node CREATED="1368467505461" ID="ID_515268243" MODIFIED="1377597057221" TEXT="An&#xe1;lisis y gesti&#xf3;n de riesgos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425204274813" ID="ID_1709235767" MODIFIED="1425204298810" TEXT="Utilizar metodolog&#xed;a reconocida internacionalmente"/>
<node CREATED="1425204307964" ID="ID_1210724642" MODIFIED="1425204349183" TEXT="Proporcionalidad entre medidas adoptadas para mitigaci&#xf3;n y los propios riesgos"/>
</node>
<node CREATED="1368467524704" ID="ID_580798946" MODIFIED="1383825052207" TEXT="Gesti&#xf3;n del personal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425204353672" ID="ID_1204463950" MODIFIED="1425204422499" TEXT="Personal relacionado con info y sistemas">
<node CREATED="1425204424620" ID="ID_616028128" MODIFIED="1425204426115" TEXT="Formado"/>
<node CREATED="1425204429703" ID="ID_1844371283" MODIFIED="1425204447197" TEXT="Informado de sus deberes en materia de seguridad"/>
<node CREATED="1425204519885" ID="ID_1334119372" MODIFIED="1425204528113" TEXT="Actuaciones supervisadas"/>
</node>
<node CREATED="1425204466130" ID="ID_775232329" MODIFIED="1425204537075" TEXT="Todo usuario debe estar identificado de forma &#xfa;nica"/>
</node>
<node CREATED="1368467561777" ID="ID_1880784985" MODIFIED="1383825054457" TEXT="Profesionalidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425204554957" ID="ID_1439913704" MODIFIED="1425204597122" TEXT="Seguridad atendida, revisada y auditada por personal cualificado"/>
<node CREATED="1425204599978" ID="ID_694537607" MODIFIED="1425204616359" TEXT="Personal de las AAPP recibir&#xe1; formaci&#xf3;n necesaria"/>
</node>
<node CREATED="1368467585357" ID="ID_1694201644" MODIFIED="1377597062026" TEXT="Autorizaci&#xf3;n y control de acceso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425204629931" ID="ID_1755667089" MODIFIED="1425204719297">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Acceso a los sistemas controlado y limitado a usuarios, procesos,
    </p>
    <p>
      dispositivos y otros sistemas debidamente autorizados
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1368467600403" ID="ID_1453906949" MODIFIED="1425144302347" TEXT="Protecci&#xf3;n de las instalaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368467688052" ID="ID_1635265641" MODIFIED="1425204730263" TEXT="Salas cerradas y control de llaves"/>
</node>
<node CREATED="1368467614498" ID="ID_1080590103" MODIFIED="1425212988590">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Adquisici&#243;n de productos
    </p>
    <p>
      de seguridad
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1425204743664" ID="ID_1850361597" MODIFIED="1425205619604">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Valorar positivamente productos con certificaci&#243;n de la funcionalidad de seguridad
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1425204794817" ID="ID_612974215" MODIFIED="1425213008583">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Organismo de Certificaci&#243;n del Esquema
    </p>
    <p>
      Nacional de Evaluaci&#243;n y Certificaci&#243;n
    </p>
    <p>
      de Seguridad de la TI (ENECSTI)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1425204884802" ID="ID_1152587824" MODIFIED="1425205452361">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Determinar&#225; el criterio a cumplir seg&#250;n uso previsto del producto
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1425205049046" ID="ID_1560281851" MODIFIED="1425206233325">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      El CCN es el OC del ENECSTIC
    </p>
    <p>
      (Acreditado por la ENAC)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1425205205760" ID="ID_1897230323" MODIFIED="1425212952060">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tipo de
    </p>
    <p>
      certificaciones
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1425205235676" ID="ID_1643029422" MODIFIED="1425205238981" TEXT="Funcional">
<node CREATED="1425205485963" ID="ID_1022163150" MODIFIED="1425212958860">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Criterios reconocidos
    </p>
    <p>
      como est&#225;ndar internacional
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1425205246509" ID="ID_580504917" MODIFIED="1425205249472" TEXT="Criptol&#xf3;gica">
<node CREATED="1425205539527" ID="ID_580081436" MODIFIED="1425212965364">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Capacidad de proteger
    </p>
    <p>
      la informaci&#243;n nacional clasificada
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1425205251802" ID="ID_1812628722" MODIFIED="1425205254996" TEXT="TEMPEST">
<node CREATED="1425205415653" ID="ID_1164904405" MODIFIED="1425212971541">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Protecci&#243;n frente a emanaciones
    </p>
    <p>
      electromagn&#233;ticas&#160;compremetedoras de la info.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1368467630137" ID="ID_1253588295" MODIFIED="1425144302348" TEXT="Seguridad por defecto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368467717481" ID="ID_116205643" MODIFIED="1425206291777">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      M&#237;nima funcionalidad requerida
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368467728721" ID="ID_776177587" MODIFIED="1425206377767">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      M&#237;nima funcionalidad&#160;de operaci&#243;n, administraci&#243;n y registro de actividad
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368467751817" ID="ID_1592485826" MODIFIED="1425206402008">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Eliminar funcionalidades innecesarias mediante control de la configuraci&#243;n
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1425206410006" ID="ID_1375854598" MODIFIED="1425206457968" TEXT="Uso ordinario del sistema sencillo y seguro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425206431334" ID="ID_1702999036" MODIFIED="1425206450132" TEXT="Utilizaci&#xf3;n insegura requiera acto consciente del usuario"/>
</node>
</node>
<node CREATED="1368467781387" ID="ID_1650704987" MODIFIED="1368467794196" TEXT="Integridad y actualizaci&#xf3;n">
<node CREATED="1425206514121" ID="ID_1244293386" MODIFIED="1425206534902" TEXT="Todo elemento f&#xed;sico o l&#xf3;gico requerir&#xe1; autorizaci&#xf3;n formal previa a su instalaci&#xf3;n"/>
<node CREATED="1425206549543" ID="ID_361314060" MODIFIED="1425206635004" TEXT="Conocer en todo momento estado de seguridad en relaci&#xf3;n a &#xa;las vulnerabilidades y a las actualizaciones que les afecten"/>
</node>
<node CREATED="1368467856182" ID="ID_103523334" MODIFIED="1425206648272">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Protecci&#243;n de informaci&#243;n almacenada y en tr&#225;nsito
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383825385227" ID="ID_1421837537" MODIFIED="1383825402165" TEXT="Consideraci&#xf3;n de inseguros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368467885861" ID="ID_702533864" MODIFIED="1383825438066" TEXT="Port&#xe1;tiles, PDA, perif&#xe9;ricos, soporte">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368467890065" ID="ID_1239662361" MODIFIED="1377597042619" TEXT="Redes inal&#xe1;mbricas con cifrado d&#xe9;bil">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1425206703945" ID="ID_1634699580" MODIFIED="1425206710999" TEXT="Info en soporte no electr&#xf3;nico">
<node CREATED="1425206712568" ID="ID_1944612941" MODIFIED="1425206732188" TEXT="Protegida con el mismo grado de seguridad"/>
</node>
</node>
<node CREATED="1368467913333" ID="ID_135642893" MODIFIED="1368467923646" TEXT="Sistemas de informaci&#xf3;n conectados">
<node CREATED="1425206766546" ID="ID_1637650503" MODIFIED="1425206800855" TEXT="Proteger el per&#xed;metro en particular si se conecta a redes disponibles al p&#xfa;blico"/>
<node CREATED="1425206813927" ID="ID_1890362979" MODIFIED="1425206853880" TEXT="Analizar riesgos de la interconexi&#xf3;n y controlar punto de uni&#xf3;n"/>
</node>
<node CREATED="1368467923886" ID="ID_1859424003" MODIFIED="1377597052244" TEXT="Registro de actividad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425206876722" ID="ID_5701896" MODIFIED="1425206963582" TEXT="Con plenas garant&#xed;as de dcho al honor, intimidad y conforme a normativa protecci&#xf3;n de datos"/>
<node CREATED="1425206901696" ID="ID_1490644510" MODIFIED="1425206958802" TEXT="Se registar&#xe1; la actividad de los usuarios de cara a gestionar actividades no autorizadas"/>
</node>
<node CREATED="1368467953254" ID="ID_1303881705" MODIFIED="1425144302350" TEXT="Incidentes de seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368468160923" ID="ID_1334601125" MODIFIED="1425207042102">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Establecer sistema de detecci&#243;n y reacci&#243;n frente a c&#243;digo da&#241;ino
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368468174347" ID="ID_1644426168" MODIFIED="1425207023325" TEXT="Registro de incidentes y acciones de tratamiento seguidas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368467960656" ID="ID_1459642859" MODIFIED="1368467968178" TEXT="Continuidad de la actividad">
<node CREATED="1425207058379" ID="ID_113955454" MODIFIED="1425207083591" TEXT="Copias de seguridad y mecanismos para garantizar continuidad de las operaciones"/>
</node>
<node CREATED="1368467968885" ID="ID_492518244" MODIFIED="1425207115006" TEXT="Mejora continua">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425207091389" ID="ID_1297774212" MODIFIED="1425207109776" TEXT="Proceso integral de seguridad actualizado y mejorado de forma continua"/>
</node>
</node>
<node CREATED="1425204087688" ID="ID_1718443435" MODIFIED="1425204167140" TEXT="Proporcionalidad: Requisitos en proporci&#xf3;n a los riesgos del sistema (algunos pueden no exigirse en sistemas sin riesgos significativos)"/>
<node CREATED="1425207159194" ID="ID_719858474" MODIFIED="1425207289316" TEXT="CCN elaborar&#xe1; y difundir&#xe1; las Gu&#xed;as de Seguridad de las TIC"/>
<node CREATED="1425207300675" ID="ID_148963577" MODIFIED="1425207469973">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sistemas de informaci&#243;n no afectados
    </p>
  </body>
</html></richcontent>
<node CREATED="1425207359103" ID="ID_339240344" MODIFIED="1425213017105" TEXT="No relacionados con ejercicio de derechos ni cumplimiento de deberes por mmee ni acceso electr&#xf3;nico de ciudadanos a info y procedimiento administrativo"/>
<node CREATED="1425207318884" ID="ID_356078380" MODIFIED="1425207461757">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Las AAPP podr&#225;n determinar cu&#225;les
    </p>
    <p>
      son esos sistemas
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</map>
