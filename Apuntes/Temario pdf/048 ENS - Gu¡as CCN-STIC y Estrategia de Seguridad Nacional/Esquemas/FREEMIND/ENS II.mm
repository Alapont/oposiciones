<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1368467165026" ID="ID_1647302942" MODIFIED="1377597139281" TEXT="ENS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425207653620" ID="ID_1858941040" MODIFIED="1425212831286" POSITION="right" TEXT="Cap&#xed;tulo IV. Comunicaciones electr&#xf3;nicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425207701832" ID="ID_1134598745" MODIFIED="1425207816241">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Requerimientos t&#233;cnicos de notificaciones
    </p>
    <p>
      y publicaciones electr&#243;nicas
    </p>
  </body>
</html></richcontent>
<node CREATED="1425207744535" ID="ID_1585678297" MODIFIED="1425207755242" TEXT="Autenticidad del organismo que publique"/>
<node CREATED="1425207757758" ID="ID_23749641" MODIFIED="1425207764654" TEXT="Integridad de la info publicada"/>
<node CREATED="1425207767028" ID="ID_918449499" MODIFIED="1425207799392">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Constancia de fecha y hora de puesta a disposici&#243;n y acceso a contenido
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1425207787686" ID="ID_987768826" MODIFIED="1425207808025" TEXT="Autenticidad del destinatario"/>
</node>
</node>
<node CREATED="1368468299187" ID="ID_1180206396" MODIFIED="1425212831281" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cap&#237;tulo V. Auditor&#237;a de seguridad
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425207932101" ID="ID_525890227" MODIFIED="1425207948053" TEXT="Para verificar cumplimiento de ENS"/>
<node CREATED="1425207979066" ID="ID_304258832" MODIFIED="1425208025194" TEXT="Emplear criterios y m&#xe9;todos generalmente reconocidos y normalizaci&#xf3;n nacional e internacional aplicable"/>
<node CREATED="1425208055064" ID="ID_1881020085" MODIFIED="1425208066950" TEXT="Se realizar&#xe1; seg&#xfa;n la Categor&#xed;a del sistema"/>
<node CREATED="1368468312176" ID="ID_1861512319" MODIFIED="1383825724321" TEXT="Ordinaria, al menos cada dos a&#xf1;os">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368468328749" ID="ID_1903935020" MODIFIED="1397815190852" TEXT="Auditor&#xed;a extraordinaria si se producen modificaciones sustanciales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383825764553" ID="ID_228051012" MODIFIED="1383825800156" TEXT="Determinar&#xe1; la fecha para el c&#xf3;mputo de los dos a&#xf1;os">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368468364496" ID="ID_768172277" MODIFIED="1384186809045" TEXT="Profundizar&#xe1; hasta el nivel que considere proporciona evidencia suficiente y relevante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1380626281674" ID="ID_1318763547" MODIFIED="1397815185410" TEXT="Sistemas de categor&#xed;a b&#xe1;sica o inferior">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1380626301551" ID="ID_1044789719" MODIFIED="1383822855870" TEXT="Autoevaluaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1425208095571" ID="ID_1574766167" MODIFIED="1425208108160" TEXT="Informes de auditor&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425208141344" ID="ID_1802808683" MODIFIED="1425208190507" TEXT="Dictaminar grado cumplimiento ENS, identificar deficiencias y sugerir medidas y recomendaciones"/>
<node CREATED="1383825958572" ID="ID_1498705426" MODIFIED="1425208245345">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Presentados a:
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425208224474" ID="ID_772086975" MODIFIED="1425208267049" TEXT="Responsable de Seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384186708682" ID="ID_1786172861" MODIFIED="1425208343398" TEXT="Analizar&#xe1; los informes y presentar&#xe1; conclusiones al responsable del sistema"/>
</node>
<node CREATED="1383825987674" ID="ID_500384899" MODIFIED="1425208549050" TEXT="Responsable del Sistema">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384186724709" ID="ID_600114974" MODIFIED="1425208359822" TEXT="Implantar&#xe1; las medidas correctoras"/>
<node CREATED="1425208566533" ID="ID_1053917401" MODIFIED="1425208665583" TEXT="Podr&#xe1; acordar retirada de servicio o sistema en su totalidad hasta aplicar medidas"/>
</node>
</node>
</node>
</node>
<node CREATED="1368468417482" ID="ID_836838974" MODIFIED="1425212831271" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cap&#237;tulo VI. Estado de seguridad de los sistemas
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425208717125" ID="ID_482530992" MODIFIED="1425208822786">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comit&#233; Sectorial de Administraci&#243;n Electr&#243;nica
    </p>
  </body>
</html></richcontent>
<node CREATED="1425208825041" ID="ID_942776809" MODIFIED="1425208841658" TEXT="Articular&#xe1; procedimientos para conocer regularmente estado de seguridad de los sistemas"/>
<node CREATED="1425208736168" ID="ID_1802345839" MODIFIED="1425208751579" TEXT="Elaborar perfil general del estado de seguridad en las AAPP"/>
</node>
</node>
<node CREATED="1368468440626" ID="ID_1390073494" MODIFIED="1425212831268" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cap&#237;tulo VII. Respuesta a incidentes
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368468453941" ID="ID_810743002" MODIFIED="1425208900455">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CCN-CERT
    </p>
    <p>
      Centro Criptol&#243;gico Nacional
    </p>
    <p>
      Computer Emergency Reaction Team
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368468465087" ID="ID_1739650916" MODIFIED="1425209014598">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Soporte y coordinaci&#243;n para el tratamiento
    </p>
    <p>
      de vulnerabilidades y resoluci&#243;n de incidentes
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383826064122" ID="ID_1931681476" MODIFIED="1383826078170" TEXT="A todas las AAPP y entidades dependientes de las mismas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1425209024133" ID="ID_1024805338" MODIFIED="1425209034398" TEXT="Puede recabar los informes de auditor&#xed;a"/>
</node>
<node CREATED="1368468490651" ID="ID_1374760631" MODIFIED="1377597090684" TEXT="Investigaci&#xf3;n y divulgaci&#xf3;n de las mejores pr&#xe1;cticas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425209044618" ID="ID_1630097044" MODIFIED="1425209089490">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Documentos CCN-STIC
    </p>
    <p>
      (Seguridad de las TIC)
    </p>
  </body>
</html></richcontent>
<node CREATED="1425209072089" ID="ID_1358584203" MODIFIED="1425209077994" TEXT="Normas, instrucciones y gu&#xed;as"/>
</node>
</node>
<node CREATED="1368468517850" ID="ID_1834089020" MODIFIED="1425209117309" TEXT="Formaci&#xf3;n al personal de las AAPP especialista en seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368468528684" ID="ID_1801512110" MODIFIED="1425209134478" TEXT="Informaci&#xf3;n sobre vulnerabilidades, alertas y avisos de amenazas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384182590519" ID="ID_969668171" MODIFIED="1384183316193" TEXT="Servicio de Alerta Temprana (SAT)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384183316194" ID="ID_1997353464" MODIFIED="1384183325029" TEXT="De la red SARA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384183333467" ID="ID_472681165" MODIFIED="1384183344623" TEXT="De Internet">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1425209241683" ID="ID_258370807" MODIFIED="1425212831258" POSITION="right" TEXT="Cap&#xed;tulo VIII. Normas de conformidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425209266675" ID="ID_1003724226" MODIFIED="1425209465115" TEXT="Sedes y registros electr&#xf3;nicos conforme a ANS"/>
<node CREATED="1425209476348" ID="ID_1581623703" MODIFIED="1425209496614" TEXT="Incluir especificaciones de seguridad en el ciclo de vida de servicios y sistemas"/>
<node CREATED="1425209564969" ID="ID_605779607" MODIFIED="1425209629235">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cada &#243;rgano de la AP
    </p>
  </body>
</html></richcontent>
<node CREATED="1425209503524" ID="ID_1836485544" MODIFIED="1425209613799" TEXT="Establecer&#xe1; sus mecanismos de control para garantizar cumplimiento ENS"/>
<node CREATED="1425209638384" ID="ID_1404214469" MODIFIED="1425209679601" TEXT="Dar&#xe1;n publicidad en sus sedes a declaraciones de conformidad y distintivos de seguridad"/>
</node>
</node>
<node CREATED="1425209699889" ID="ID_1695992117" MODIFIED="1425209710981" POSITION="right" TEXT="Cap&#xed;tulo IX. Actualizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425209713119" ID="ID_102270805" MODIFIED="1425209787454" TEXT="ENS actualizado permanentemente (avances en eAdmon, tecnolog&#xed;a...)"/>
</node>
<node CREATED="1425209798393" ID="ID_1223600727" MODIFIED="1425209818425" POSITION="right" TEXT="Cap&#xed;tulo X. Categorizaci&#xf3;n de los SSII">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425209820361" ID="ID_1588314441" MODIFIED="1425209907220" TEXT="Proporcionalidad: equilibrio entre importancia de info y servicios y esfuerzo de seguridad requerido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1425209913808" ID="ID_1066876006" MODIFIED="1425209989782" TEXT="Seg&#xfa;n Anexo I"/>
</node>
<node CREATED="1376049732795" ID="ID_1299136328" MODIFIED="1425210012298" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Disposici&#243;n adicional primera. Formaci&#243;n del personal de las AAPP
    </p>
  </body>
</html></richcontent>
<node CREATED="1425210030691" ID="ID_1016190389" MODIFIED="1425210036312" TEXT="Garantizar conocimiento del ENS"/>
</node>
<node CREATED="1376049742529" ID="ID_1865129574" MODIFIED="1425212831253" POSITION="right" TEXT="Disposici&#xf3;n adicional segunda. INTECO - Instituto Nacional de Tecnolog&#xed;as de la Comunicaci&#xf3;n">
<node CREATED="1425210085211" ID="ID_1292983757" MODIFIED="1425210101501" TEXT="Centro de excelencia promovido por el M&#xba; Industria"/>
<node CREATED="1425210109092" ID="ID_965380149" MODIFIED="1425210204582" TEXT="Podr&#xe1; apoyar proyectos de innovaci&#xf3;n y programas de investigaci&#xf3;n enfocados a la mejor implantaci&#xf3;n de medidas de seguridad del ENS"/>
</node>
<node CREATED="1383826321349" ID="ID_1773908988" MODIFIED="1425212831251" POSITION="right" TEXT="Disposici&#xf3;n adicional tercera. Comit&#xe9; de Seguridad de la Informaci&#xf3;n de las AAPP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425210240487" ID="ID_1011095730" MODIFIED="1425210257739" TEXT="Dependiente del Comit&#xe9; Sectorial de Administraci&#xf3;n Electr&#xf3;nica"/>
<node CREATED="1383826357491" ID="ID_971223486" MODIFIED="1425210275169" TEXT="Un representante de cada una de las entidades del CSAE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383826383937" ID="ID_92788732" MODIFIED="1425210361668" TEXT="Funciones de cooperaci&#xf3;n en materias comunes relacionadas con implantaci&#xf3;n ENS ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368469437717" ID="ID_1861171135" MODIFIED="1425212831249" POSITION="right" TEXT="Disposici&#xf3;n transitoria. Adecuaci&#xf3;n de sistemas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384183687774" ID="ID_363252322" MODIFIED="1391886204866" TEXT="El primer paso consistir&#xe1; en redactar una POLITICA DE SEGURIDAD">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384183938161" ID="ID_401098752" MODIFIED="1384183984078" TEXT="a) Objetivos y misi&#xf3;n de la organizaci&#xf3;n"/>
<node CREATED="1384183949591" ID="ID_1125225575" MODIFIED="1391798778779" TEXT="b) Marco LEGAL y regulatorio donde se definir&#xe1;n las actividades"/>
<node CREATED="1384184002774" ID="ID_260588819" MODIFIED="1391798770342" TEXT="c) ROLES y funciones de seguridad"/>
<node CREATED="1384184038845" ID="ID_1059250134" MODIFIED="1391798760724" TEXT="d) Estructura del COMITE para la gesti&#xf3;n y coordinaci&#xf3;n de la seguridad"/>
<node CREATED="1384184062145" ID="ID_1385598166" MODIFIED="1391798752109" TEXT="e) Directrices para la estructuraci&#xf3;n de la DOCUMENTACION de seguridad del sistema"/>
</node>
<node CREATED="1425210465601" ID="ID_66118340" MODIFIED="1425210473448" TEXT="Sistemas existentes">
<node CREATED="1368469458584" ID="ID_738917125" MODIFIED="1384183620227" TEXT="12 meses desde la entrada en vigor del ENS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384183582386" ID="ID_416215639" MODIFIED="1425144302329" TEXT="Si no, se deber&#xe1; disponer de un PLAN DE ADECUACION">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384183776766" ID="ID_604022639" MODIFIED="1384183790537" TEXT="No es aconsejable publicarlo"/>
</node>
<node CREATED="1384183579287" ID="ID_1254474622" MODIFIED="1384183634447" TEXT=" En todo caso no superior a 48 meses DESDE LA ENTRADA EN VIGOR">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1368468566132" ID="ID_1409252517" MODIFIED="1425212831235" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Anexo I. Categor&#237;as de los sistemas
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383826591555" ID="ID_1680533885" MODIFIED="1425210921116">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Fundamentos de categorizaci&#243;n:
    </p>
    <p>
      Valoraci&#243;n de impacto de un incidente
    </p>
    <p>
      con repercusi&#243;n en la organizaci&#243;n para:
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383826619077" ID="ID_1459520966" MODIFIED="1383826684347" TEXT="Alcanzar sus objetivos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383826625309" ID="ID_208986258" MODIFIED="1383826693927" TEXT="Proteger los activos a su cargo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383826638826" ID="ID_704523870" MODIFIED="1383826689950" TEXT="Cumplir con sus obligaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383826648980" ID="ID_1395886512" MODIFIED="1383826690951" TEXT="Respetar la legalidad vigente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383826667141" ID="ID_404578900" MODIFIED="1383826691671" TEXT="Respetar los derechos de las personas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368468699982" ID="ID_991902089" MODIFIED="1425212831237" TEXT="Dimensiones">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Coinciden con las de MAGERIT v.3
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368468731401" ID="ID_259285860" MODIFIED="1377444439872" TEXT="Disponibilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368468736773" ID="ID_484514559" MODIFIED="1377444442052" TEXT="Autenticidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368468742740" ID="ID_1281240707" MODIFIED="1377444444528" TEXT="Integridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368468747552" ID="ID_1936079902" MODIFIED="1377444449194" TEXT="Confidencialidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368468753738" ID="ID_700203880" MODIFIED="1377444451630" TEXT="Trazabilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368468783542" ID="ID_1500867876" MODIFIED="1425212831244" TEXT="Niveles usados en cada dimensi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368468789557" ID="ID_789780756" MODIFIED="1381767793469" TEXT="Nivel BAJO (perjuicio limitado)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368468875036" ID="ID_1684124863" MODIFIED="1425210816224" TEXT="Reducci&#xf3;n apreciable de la capacidad de la organizaci&#xf3;n"/>
<node CREATED="1368468883920" ID="ID_1113484929" MODIFIED="1368468887806" TEXT="Da&#xf1;o menor"/>
<node CREATED="1368468888093" ID="ID_918364953" MODIFIED="1425210770268" TEXT="Incumplimiento formal de Ley con car&#xe1;cter subsanable"/>
<node CREATED="1425210788316" ID="ID_637446690" MODIFIED="1425210794031" TEXT="Perjuicio menor a un individuo"/>
</node>
<node CREATED="1368468928770" ID="ID_1631122637" MODIFIED="1381767795997" TEXT="Nivel MEDIO (perjuicio grave)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368468948623" ID="ID_1359251668" MODIFIED="1425210821662" TEXT="Reducci&#xf3;n significativa de la capacidad de la organizaci&#xf3;n"/>
<node CREATED="1368468962092" ID="ID_812329195" MODIFIED="1384184982569" TEXT="Da&#xf1;o SIGNIFICATIVO"/>
<node CREATED="1368468971272" ID="ID_591908171" MODIFIED="1425210831642" TEXT="Incumplimiento material o incumplimiento formal de Ley que no tenga el car&#xe1;cter de subsanable"/>
<node CREATED="1368469012776" ID="ID_453243813" MODIFIED="1368469033417" TEXT="Perjuicio significativo a un individuo"/>
</node>
<node CREATED="1368469038009" ID="ID_264349564" MODIFIED="1381767797245" TEXT="Nivel ALTO (perjuicio muy grave)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368469049390" ID="ID_1808461623" MODIFIED="1368469065772" TEXT="Anulaci&#xf3;n de la capacidad de la organizaci&#xf3;n"/>
<node CREATED="1368469066199" ID="ID_950112184" MODIFIED="1368469076138" TEXT="Da&#xf1;o muy grave o irreparable"/>
<node CREATED="1368469079420" ID="ID_1405617797" MODIFIED="1384184995423" TEXT="Incumplimiento GRAVE"/>
<node CREATED="1368469086213" ID="ID_945761527" MODIFIED="1368469102736" TEXT="Perjuicio grave a un individuo"/>
</node>
</node>
<node CREATED="1368469134363" ID="ID_1779397972" MODIFIED="1385061928404" TEXT="Categor&#xed;as en las que se engloba el sistema">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368469141939" ID="ID_1559973249" MODIFIED="1377444517771" TEXT="ALTA: alguna de sus dimensiones alcanza nivel ALTO">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368469202652" ID="ID_464618512" MODIFIED="1377444519650" TEXT="MEDIA: alguna de sus dimensiones alcanza nivel MEDIO y ninguna uno superior">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368469245622" ID="ID_315473074" MODIFIED="1377444521024" TEXT="BASICA: si alguna de sus dimensiones alcanza el nivel BAJO, y ninguna un nivel superior">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1368532790444" ID="ID_508790418" MODIFIED="1425212831225" POSITION="right" TEXT="Anexo II. Medidas de seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425210975972" ID="ID_488783180" MODIFIED="1425212831227" TEXT="Ser&#xe1;n proporcionales a">
<node CREATED="1425210986615" ID="ID_1203563098" MODIFIED="1425210993426" TEXT="Dimensiones de seguridad relevantes"/>
<node CREATED="1425210995581" ID="ID_491789565" MODIFIED="1425211003728" TEXT="Categor&#xed;a del sistema de informaci&#xf3;n"/>
</node>
<node CREATED="1368532869133" ID="ID_1277620423" MODIFIED="1425211280747" TEXT="Grupos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368532797653" ID="ID_778066128" MODIFIED="1377444526888" TEXT="Marco organizativo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425211043265" ID="ID_435338106" MODIFIED="1425211052761" TEXT="Organizaci&#xf3;n global de la seguridad"/>
</node>
<node CREATED="1368532811277" ID="ID_1653837368" MODIFIED="1377444528483" TEXT="Marco operacional">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425211054827" ID="ID_674777236" MODIFIED="1425211097184" TEXT="Protecci&#xf3;n de la operaci&#xf3;n del sistema como conjunto integral"/>
</node>
<node CREATED="1368532818933" ID="ID_75157360" MODIFIED="1377444529548" TEXT="Medidas de protecci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425211074483" ID="ID_1397725986" MODIFIED="1425211086030" TEXT="Proteger activos concretos"/>
</node>
</node>
<node CREATED="1368532881221" ID="ID_1709629591" MODIFIED="1425211110072" TEXT="Pasos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368532896005" ID="ID_159487422" MODIFIED="1383823202718" TEXT="Identificaci&#xf3;n de los tipos de activos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1368532905405" ID="ID_9827433" MODIFIED="1383823206431" TEXT="Determinaci&#xf3;n de las dimensiones de seguridad seg&#xfa;n los activos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1377598403148" ID="ID_1240629647" MODIFIED="1383823208463" TEXT="Disponibilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377598460636" ID="ID_334596977" MODIFIED="1383823210047" TEXT="Autenticidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377598469130" ID="ID_906287443" MODIFIED="1383823210822" TEXT="Integridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377598483381" ID="ID_1470861364" MODIFIED="1383823211523" TEXT="Confidencialidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377598495293" ID="ID_556328256" MODIFIED="1383823212203" TEXT="Trazabilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368532933101" ID="ID_1849970321" MODIFIED="1425144223818" TEXT="Determinaci&#xf3;n del nivel correspondiente a cada dimensi&#xf3;n de seguridad ">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <table style="border-right-width: 0; border-bottom-style: solid; border-top-style: solid; border-top-width: 0; border-right-style: solid; border-left-width: 0; border-left-style: solid; width: 80%; border-bottom-width: 0" border="0">
      <tr>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            <b>BAJO </b>
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            <b>MEDIO </b>
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            <b>ALTO</b>
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            <b>DISPONIBILIDAD </b>
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            <b>AUTENTICIDAD </b>
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            <b>INTEGRIDAD </b>
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            <b>CONFIDENCIALIDAD </b>
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            <b>TRAZABILIDAD</b>
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            
          </p>
        </td>
        <td valign="top" style="border-right-width: 1; border-bottom-style: solid; border-top-style: solid; border-top-width: 1; border-right-style: solid; border-left-width: 1; border-left-style: solid; width: 33%; border-bottom-width: 1">
          <p style="margin-left: 1; margin-top: 1; margin-right: 1; margin-bottom: 1">
            
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1377598388811" ID="ID_248894274" MODIFIED="1383823219495" TEXT="BAJO">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377598396002" ID="ID_324162705" MODIFIED="1383823220755" TEXT="MEDIO">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377598398116" ID="ID_1954807815" MODIFIED="1383823221665" TEXT="ALTO">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368532955685" ID="ID_918036294" MODIFIED="1383823225706" TEXT="Determinaci&#xf3;n de la categor&#xed;a del sistema">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
<node CREATED="1377598444138" ID="ID_1309571300" MODIFIED="1383823228546" TEXT="BASICA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377598442165" ID="ID_807141862" MODIFIED="1383823229796" TEXT="MEDIA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377598437680" ID="ID_1546613198" MODIFIED="1383823230640" TEXT="ALTA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368532973013" ID="ID_1642009614" MODIFIED="1383823232551" TEXT="Selecci&#xf3;n de las medidas de seguridad adecuadas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-5"/>
</node>
</node>
</node>
<node CREATED="1425211675268" ID="ID_890890664" MODIFIED="1425212018779" POSITION="right" TEXT="Anexo III. Auditor&#xed;a de la seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425211914451" ID="ID_1452348333" MODIFIED="1425212831213" TEXT="Seguridad auditada en base a:">
<node CREATED="1425211698095" ID="ID_1864885830" MODIFIED="1425211807930" TEXT="La pol&#xed;tica de seguridad define roles y funciones de los responsables de la informaci&#xf3;n, los servicios, los activos y la seguridad del sistema de informaci&#xf3;n&#xa;"/>
<node CREATED="1425211807933" ID="ID_1457942650" MODIFIED="1425211826523" TEXT="Procedimientos para resoluci&#xf3;n de conflictos entre responsables&#xa;"/>
<node CREATED="1425211826523" ID="ID_1840322715" MODIFIED="1425211831211" TEXT="Personas para dichos roles seg&#xfa;n principio de &#xab;separaci&#xf3;n de funciones&#xbb;&#xa;"/>
<node CREATED="1425211831212" ID="ID_1450507239" MODIFIED="1425211844603" TEXT="Se ha realizado un an&#xe1;lisis de riesgos, con revisi&#xf3;n y aprobaci&#xf3;n anual&#xa;"/>
<node CREATED="1425211844604" ID="ID_1837506964" MODIFIED="1425211884557" TEXT="Se cumplen las recomendaciones de protecci&#xf3;n descritas en el anexo II"/>
<node CREATED="1425211861815" ID="ID_1528025118" MODIFIED="1425211903086" TEXT="Existe SGSI documentado y con proceso regular de aprobaci&#xf3;n por la direcci&#xf3;n"/>
</node>
<node CREATED="1425211947773" ID="ID_1108567410" MODIFIED="1425212831208" TEXT="Se basa en la existencia de evidencias">
<node CREATED="1425211948033" ID="ID_469308014" MODIFIED="1425211970392" TEXT="Documentaci&#xf3;n de los procedimientos&#xa;"/>
<node CREATED="1425211970393" ID="ID_1958911204" MODIFIED="1425211978948">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Registro de incidencias
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1425211978953" ID="ID_855011048" MODIFIED="1425211978956">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Examen del personal afectado
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1425212020980" ID="ID_772008618" MODIFIED="1425212831198" TEXT="Sistemas de categor&#xed;a B&#xc1;SICA">
<node CREATED="1425212033526" ID="ID_21846908" MODIFIED="1425212137862" TEXT="No necesitan auditor&#xed;a --&gt; Informe de Autoevaluaci&#xf3;n">
<node CREATED="1425212065633" ID="ID_1009441312" MODIFIED="1425212084876" TEXT="Por el personal que administra el sistema"/>
<node CREATED="1425212095731" ID="ID_1932865033" MODIFIED="1425212108272" TEXT="Documentados y analizados por responsable de seguridad"/>
</node>
</node>
<node CREATED="1425212141671" ID="ID_1853154406" MODIFIED="1425212149614" TEXT="Sistemas de categor&#xed;a MEDIA y ALTA">
<node CREATED="1425208095571" ID="ID_427994440" MODIFIED="1425208108160" TEXT="Informes de auditor&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425208141344" ID="ID_779056930" MODIFIED="1425208190507" TEXT="Dictaminar grado cumplimiento ENS, identificar deficiencias y sugerir medidas y recomendaciones"/>
<node CREATED="1383825958572" ID="ID_271574850" MODIFIED="1425208245345">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Presentados a:
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1425208224474" ID="ID_1342610671" MODIFIED="1425208267049" TEXT="Responsable de Seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384186708682" ID="ID_1454727343" MODIFIED="1425208343398" TEXT="Analizar&#xe1; los informes y presentar&#xe1; conclusiones al responsable del sistema"/>
</node>
<node CREATED="1383825987674" ID="ID_1578263990" MODIFIED="1425208549050" TEXT="Responsable del Sistema">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384186724709" ID="ID_308359496" MODIFIED="1425208359822" TEXT="Implantar&#xe1; las medidas correctoras"/>
<node CREATED="1425208566533" ID="ID_387689852" MODIFIED="1425208665583" TEXT="Podr&#xe1; acordar retirada de servicio o sistema en su totalidad hasta aplicar medidas"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1425212226153" ID="ID_514421809" MODIFIED="1425212234284" POSITION="right" TEXT="Anexo IV. Glosario de t&#xe9;rminos"/>
<node CREATED="1425212240597" ID="ID_1956211510" MODIFIED="1425212253650" POSITION="right" TEXT="Anexo V. Modelo de cl&#xe1;usula administrativa particular"/>
<node CREATED="1377444858023" ID="ID_1225834248" LINK="https://www.ccn-cert.cni.es/index.php?option=com_content&amp;view=article&amp;id=3053" MODIFIED="1377444893047" POSITION="right" TEXT="Comparativa entre ENS y 27002">
<node CREATED="1425212303575" ID="ID_478774994" MODIFIED="1425212303575" TEXT=""/>
</node>
</node>
</map>
