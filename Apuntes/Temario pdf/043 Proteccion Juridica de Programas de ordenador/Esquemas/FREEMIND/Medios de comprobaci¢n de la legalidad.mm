<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1366635904427" ID="ID_1038509435" MODIFIED="1378740392080" TEXT="Legalidad software">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366636184845" ID="ID_57156187" MODIFIED="1384280084726" POSITION="right" TEXT="DRM (Digital Rights Management)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366636296438" ID="ID_733150937" MODIFIED="1366636301292" TEXT="Aplicaciones">
<node CREATED="1366636279988" ID="ID_1259255373" MODIFIED="1366636286167" TEXT="Reproducci&#xf3;n por pa&#xed;ses"/>
<node CREATED="1366636273974" ID="ID_1638549462" MODIFIED="1366636279576" TEXT="Pago por visi&#xf3;n"/>
<node CREATED="1366636266868" ID="ID_226836068" MODIFIED="1375877632179" TEXT="Servicios online"/>
<node CREATED="1366636240030" ID="ID_498045775" MODIFIED="1366636264881" TEXT="CD o DVD con protecci&#xf3;n"/>
</node>
<node CREATED="1366636314058" ID="ID_440031968" MODIFIED="1366636317071" TEXT="Esquema">
<node CREATED="1366636318512" ID="ID_1936750732" MODIFIED="1366636325113" TEXT="Servidor de empaquetado"/>
<node CREATED="1366636325867" ID="ID_1222311726" MODIFIED="1366636333560" TEXT="Servidor de contenidos"/>
<node CREATED="1366636334018" ID="ID_1924598684" MODIFIED="1366636339574" TEXT="Servidor de licencias"/>
<node CREATED="1366636340079" ID="ID_1469316616" MODIFIED="1366636344199" TEXT="Reproductor"/>
</node>
<node CREATED="1366636367654" ID="ID_1192708823" MODIFIED="1366636381632" TEXT="Control de licencias software">
<node CREATED="1366636383184" ID="ID_481793033" MODIFIED="1366636391312" TEXT="Llaves USB"/>
<node CREATED="1366636403596" ID="ID_1206281697" MODIFIED="1366636413753" TEXT="Llaves software"/>
<node CREATED="1366636419983" ID="ID_1525959776" MODIFIED="1366636438471" TEXT="Software de evaluaci&#xf3;n"/>
<node CREATED="1366636449428" ID="ID_892539483" MODIFIED="1366636455187" TEXT="Super distribuci&#xf3;n"/>
<node CREATED="1366636456035" ID="ID_379348244" MODIFIED="1366636463119" TEXT="Registro del usuario final"/>
<node CREATED="1366636469693" ID="ID_1044544731" MODIFIED="1366636474765" TEXT="Licencias flotantes"/>
</node>
<node CREATED="1375877693846" ID="ID_449212503" MODIFIED="1377525713523" TEXT="XrML(eXtensible Rights Markup Language)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375877795917" ID="ID_1557790127" MODIFIED="1378740266448" TEXT="REL (Rights Expression Language) Compatible con XML">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375897612558" ID="ID_1255587718" MODIFIED="1384341782636" TEXT="TPM (Technical Protection Measures)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384281031791" ID="ID_1199782374" MODIFIED="1384622544005" TEXT="Impide">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384281038809" ID="ID_1871702188" MODIFIED="1384281106371" TEXT="Reproducir un contenido en otro dispositivo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384281066939" ID="ID_1092284218" MODIFIED="1384281107603" TEXT="Copiar un contenido y distribuirlo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375877960789" ID="ID_690533651" MODIFIED="1385568266887" TEXT="Sistemas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375877971765" ID="ID_1952821772" MODIFIED="1377525616829" TEXT="SCMS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375877991828" ID="ID_1414088209" MODIFIED="1377525618677" TEXT="Protege la m&#xfa;sica en los CD">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375878008332" ID="ID_1689094475" MODIFIED="1377525620533" TEXT="Macrovisi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375878015689" ID="ID_980277644" MODIFIED="1377525622990" TEXT="Protege las copias de v&#xed;deo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375878039155" ID="ID_783698249" MODIFIED="1377525625005" TEXT="CSS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375878043864" ID="ID_1913330345" MODIFIED="1377525626574" TEXT="Protege los DVD">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375897701793" ID="ID_383582508" MODIFIED="1385568268932" TEXT="Medidas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375897705252" ID="ID_804239178" MODIFIED="1391357440530" TEXT="Dongle: hardware con un n&#xfa;mero de serie que debe insertarse en el PC  ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375897740626" ID="ID_1655791324" MODIFIED="1382810303048" TEXT="Registration Key">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1391357963294" ID="ID_541484717" MODIFIED="1391357992727" TEXT="Para ejecutar el producto, es necesario disponer de un n&#xfa;mero de ser&#xed;e que suministra el fabricante"/>
</node>
<node CREATED="1375897757828" ID="ID_728020921" MODIFIED="1382810304343" TEXT="Internet  Product Activation">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1391357933226" ID="ID_1035420194" MODIFIED="1391358003640" TEXT="Una vez el producto es activado, no permite su instalaci&#xf3;n en otro equipo">
<node CREATED="1384280308562" ID="ID_433570629" MODIFIED="1384280371170" TEXT="MPA (Microsoft Product Activation)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384280325576" ID="ID_593774591" MODIFIED="1384280376130" TEXT="WPA (Windows)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384280330417" ID="ID_1489517135" MODIFIED="1384280374570" TEXT="OWA (Officce)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1375897791192" ID="ID_1280834385" MODIFIED="1391358552209" TEXT="Encriptado de la informaci&#xf3;n, de forma que no se puede acceder a ella">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1391358026697" ID="ID_685575419" MODIFIED="1391358067247" TEXT="Software tamper&#xed;ng">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1391358035130" ID="ID_1670936185" MODIFIED="1391358061086" TEXT="Si el softawre detecta  que ha sido copiado, activa modificaciones en el mismo que previenen su correcta ejecuci&#xf3;n"/>
</node>
<node CREATED="1375877338842" ID="ID_1825903921" MODIFIED="1384349049666" TEXT="T&#xe9;cnicas esteganogr&#xe1;ficas (Tambi&#xe9;n se pueden considerar DRM)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366635931881" ID="ID_1357920878" MODIFIED="1384349173285" TEXT="Marca de agua digital o Watermarking">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contiene informaci&#243;n acerca del copyright
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366635959627" ID="ID_1352882572" MODIFIED="1384342285852" TEXT="Caracter&#xed;sticas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366635965688" ID="ID_668190912" MODIFIED="1377525661034" TEXT="Imperceptible">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366635972106" ID="ID_870040107" MODIFIED="1377525663051" TEXT="No degrade">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366635976497" ID="ID_695751953" MODIFIED="1377525665051" TEXT="Robusta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366635982682" ID="ID_965004595" MODIFIED="1377525667532" TEXT="No ambigua">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1366635999647" ID="ID_1880130552" MODIFIED="1384342289700" TEXT="T&#xe9;cnicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366636005258" ID="ID_778171356" MODIFIED="1391358717568" TEXT="Espaciales">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modificaci&#243;n de componente en el dominio espacial
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366636011004" ID="ID_1382527661" MODIFIED="1391358736430" TEXT="Espectrales">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modificaci&#243;n de componentes en el dominio de la frecuencia
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1366636030013" ID="ID_1944101404" MODIFIED="1384342288412" TEXT="Ataques">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366636046276" ID="ID_1854932590" MODIFIED="1377525679141" TEXT="Robustez">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366636053600" ID="ID_1013723408" MODIFIED="1377525680573" TEXT="Presentaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366636063576" ID="ID_1267638925" MODIFIED="1377525681686" TEXT="Interpretaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1366636086969" ID="ID_649941959" MODIFIED="1391358682024" TEXT="Huella digital">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Incluyen, adem&#225;s del copyright, informaci&#243;n acerca del usuario leg&#237;timo del producto para detectar a posteriori si se ha hecho una copia.
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366636116618" ID="ID_561543236" MODIFIED="1384349170349" TEXT="Tipos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366636121306" ID="ID_229895694" MODIFIED="1377525643224" TEXT="Sim&#xe9;trica. S&#xf3;lo interviene el vendedor">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366636126460" ID="ID_1307552631" MODIFIED="1377525645976" TEXT="Asim&#xe9;trica. Intervienen tanto el comprador como el vendedor">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366636130321" ID="ID_1535771216" MODIFIED="1377525650905" TEXT="An&#xf3;nima. Interviene una tercera parte de confianza">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1384280467429" ID="ID_1949376172" MODIFIED="1384349121949" TEXT="Formatos de fichero">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384280476783" ID="ID_733490659" MODIFIED="1384280527506" TEXT=".wma o .asf">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384280495292" ID="ID_447891472" MODIFIED="1384280528584" TEXT="DRM de Microsoft">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1384280509712" ID="ID_111898594" MODIFIED="1384280529691" TEXT="ACC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384280518443" ID="ID_436121846" MODIFIED="1384280534449" TEXT="Fairplay de Apple">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
