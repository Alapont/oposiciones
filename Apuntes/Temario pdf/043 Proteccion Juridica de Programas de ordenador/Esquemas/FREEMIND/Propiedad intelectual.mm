<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1366631436660" ID="ID_1516436325" MODIFIED="1384349409792" TEXT="Propiedad intelectual">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384349418050" ID="ID_353800101" MODIFIED="1384349438185" POSITION="right" TEXT="Ley de Propiedad intelectual 1/1996">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366631448022" ID="ID_16882248" MODIFIED="1384349183010" POSITION="right" TEXT="Tipos de derechos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366631486750" ID="ID_523227216" MODIFIED="1391355243245" TEXT="Derechos morales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366631497961" ID="ID_49410895" MODIFIED="1377525876747" TEXT="Divulgaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366631503240" ID="ID_1373775079" MODIFIED="1377525878554" TEXT="Autor&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366631516726" ID="ID_1786947926" MODIFIED="1377525880290" TEXT="Integridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1366631533958" ID="ID_139714012" MODIFIED="1391355246178" TEXT="Patrimoniales (TRLPI)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366631542445" ID="ID_98822722" MODIFIED="1377525886684" TEXT="Reproducci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366631547071" ID="ID_490311028" MODIFIED="1377525888723" TEXT="Distribuci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366631552741" ID="ID_696926383" MODIFIED="1377525890419" TEXT="Comunicaci&#xf3;n P&#xfa;blica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366631566774" ID="ID_1054638096" MODIFIED="1377525892444" TEXT="Tranformaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1366631611101" ID="ID_785867502" MODIFIED="1366631639979" POSITION="left" TEXT="Copyleft o GNU GPL"/>
<node CREATED="1366631749882" ID="ID_884193528" MODIFIED="1366631766654" POSITION="left" TEXT="Medidas tecnol&#xf3;gicas de protecci&#xf3;n no aplicables a los programas"/>
<node CREATED="1366632349820" ID="ID_1282863630" MODIFIED="1384349291083" POSITION="right" TEXT="Propiedad industrial">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366632368940" ID="ID_856804810" MODIFIED="1377525895108" TEXT="Plazos (20 a&#xf1;os)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366632375794" ID="ID_1151004054" MODIFIED="1518435885240" TEXT="Requisitos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366632384655" ID="ID_1478135423" MODIFIED="1384284662000" TEXT="NOVEDAD">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366632387830" ID="ID_1634035123" MODIFIED="1377525902350" TEXT="Aplicaci&#xf3;n industrial">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366632394171" ID="ID_1397987702" MODIFIED="1377525903469" TEXT="Actividad inventiva">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1366632381746" ID="ID_728019312" MODIFIED="1382812452050" TEXT="Excepci&#xf3;n: programas de ordenador">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384284600512" ID="ID_1242736659" MODIFIED="1518435886252" TEXT="Ventajas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384284607912" ID="ID_1733641307" MODIFIED="1384284621870" TEXT="Es un activo contable">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384284628576" ID="ID_1973652005" MODIFIED="1384284800191" TEXT="Puede negociarse con &#xe9;l">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1384284638509" ID="ID_1237116582" MODIFIED="1384284655102" TEXT="Protecci&#xf3;n frente a copias o imitaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1384284683895" ID="ID_1329516418" MODIFIED="1518435887409" TEXT="Inconvenientes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384284689815" ID="ID_227121089" MODIFIED="1384284717345" TEXT="No protege bienes intangibles">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384284704352" ID="ID_1309028541" MODIFIED="1384284715754" TEXT="Divulgaci&#xf3;n obligatoria">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384284731736" ID="ID_1206006611" MODIFIED="1384284771051" TEXT="Alto coste">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384284751341" ID="ID_383695880" MODIFIED="1384284774654" TEXT="Proceso lento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384284739186" ID="ID_270173293" MODIFIED="1384284776048" TEXT="Hay que demostrar la novedad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1366631820841" ID="ID_564123844" MODIFIED="1384349354852" POSITION="right" TEXT="Propiedad intelectual (Art. 95 a 104 TRLPI)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366631793601" ID="ID_455107560" MODIFIED="1382812459554" TEXT="No est&#xe1; protegidos las ideas o principios en que se basan (art. 96)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ff6666" CREATED="1366631844044" ID="ID_1917675197" MODIFIED="1377525922190" STYLE="bubble" TEXT="Plazos (70 A&#xf1;os)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366631860728" ID="ID_1841943852" MODIFIED="1377525927223" TEXT="Autor&#xed;a colectiva">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366631879019" ID="ID_863218174" MODIFIED="1377525930975" TEXT="Documentaci&#xf3;n y manuales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366631888309" ID="ID_1981250663" MODIFIED="1377525932447" TEXT="Trabajador asalariado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375876735395" ID="ID_198367005" MODIFIED="1377525933807" TEXT="Podr&#xe1;n inscribirse en el Registro de la Propiedad Intelectual">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366632126981" ID="ID_1858213897" MODIFIED="1391355707440" TEXT="Derechos exclusivos de explotaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366632145337" ID="ID_722443018" MODIFIED="1377525939759" TEXT="Reproducci&#xf3;n total o parcial">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366632160085" ID="ID_1409537398" MODIFIED="1377525941599" TEXT="Cualquier transformaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366632174273" ID="ID_1222844749" MODIFIED="1377525942783" TEXT="Distribuci&#xf3;n p&#xfa;blica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1381571298197" ID="ID_211800514" MODIFIED="1384283679299" TEXT="Incluye el alquiler">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1366632186901" ID="ID_1484834388" MODIFIED="1384342449300" TEXT="L&#xed;mites a los derechos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366632194181" ID="ID_644278775" MODIFIED="1377525945935" TEXT="Transformaci&#xf3;n para la utilizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366632214747" ID="ID_1427302384" MODIFIED="1384283501489" TEXT="Una copia de seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366632234505" ID="ID_230340224" MODIFIED="1377525949720" TEXT="Determinar ideas y principios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366632245214" ID="ID_1544260416" MODIFIED="1382812356469" TEXT="El autor no podr&#xe1;  negarse a versiones sucesivas por el titular de los derechos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366632258092" ID="ID_1002290594" MODIFIED="1391355800323" TEXT="Cambios para conseguir la INTEROPERABILIDAD   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1366632279191" ID="ID_655020051" MODIFIED="1384283558795" TEXT="Infracciones (102)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366632298317" ID="ID_1251632603" MODIFIED="1384283743796" TEXT="Poner en circulaci&#xf3;n copias, presumiendo naturaleza il&#xed;cita">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366632303831" ID="ID_183931291" MODIFIED="1384283735629" TEXT="Tener con fines comerciales copias, presumiendo su naturaleza il&#xed;cita">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366632327879" ID="ID_404193019" MODIFIED="1391355744023" TEXT="Poner en circulaci&#xf3;n instrumentos con el &#xfa;nico fin de eliminar las medidas de seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1381571108976" ID="ID_425130242" MODIFIED="1384283694877" TEXT="Siempre que tenga ese &#xfa;nico fin ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1368388998727" ID="ID_1219399107" MODIFIED="1377525967265" TEXT="Obra compuesta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1381571091171" ID="ID_1976286516" MODIFIED="1384283569654" TEXT="Sin colaboraci&#xf3;n, pero con autorizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1384284258745" ID="ID_1831546944" MODIFIED="1384622837761" TEXT="Ventajas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384284263443" ID="ID_1905568045" MODIFIED="1391355859713" TEXT="No requiere novedad, s&#xf3;lo que sea ORIGINAL   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384284281329" ID="ID_682015291" MODIFIED="1384284324444" TEXT="Nace en el mismo momento de su creaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384284308298" ID="ID_756551876" MODIFIED="1384284324941" TEXT="Proporciona protecci&#xf3;n con respecto al plagio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1384284556799" ID="ID_903122823" MODIFIED="1384622845409" TEXT="Inconvenientes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384284561815" ID="ID_1621670411" MODIFIED="1384284587892" TEXT="No protege las ideas o principios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384284572896" ID="ID_593523261" MODIFIED="1384284586637" TEXT="Duraci&#xf3;n excesiva (70 a&#xf1;os)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</map>
