<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1426523590362" ID="ID_567669205" MODIFIED="1441291918104">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T36-37 LPI
    </p>
    <p style="text-align: center">
      RD 1/1996
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426523609800" ID="ID_1422315681" MODIFIED="1426525205223" POSITION="right" TEXT="Protecci&#xf3;n de dchos de autor de obras por el mero hecho de crearlas">
<icon BUILTIN="info"/>
<node CREATED="1441291744572" ID="ID_568534230" MODIFIED="1441291791340">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ley 23/2006: </i>modif RD 1/1996 (dchos autor)&#8594; trasp Dir 2001/28/CE
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426526531930" ID="ID_315831170" MODIFIED="1441291809295">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reg en <u>MinCult</u>&#160;&#160;(p&#250;b)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1426523725358" ID="ID_1148440972" MODIFIED="1426524591150" POSITION="right" TEXT="Obras protegidas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426523731391" ID="ID_130237649" MODIFIED="1441291174929">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Originales: </i>plasmaci&#243;n material de la idea por cualquier medio
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426523754936" ID="ID_16827157" MODIFIED="1441291186256">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Derivadas: </i>actualizaciones, arreglos (con consent de autor)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426523775012" ID="ID_662222514" MODIFIED="1441291190866">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Dchos afines o conexos:</i>&#160;por haber participado en su proceso productivo
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426523801476" ID="ID_1654767277" MODIFIED="1441291216071" TEXT="Disps legales, resols jur&#xed;dicas y dict&#xe1;menes de orgs">
<icon BUILTIN="stop-sign"/>
</node>
</node>
</node>
<node CREATED="1426524188999" ID="ID_549765698" MODIFIED="1426524590486" POSITION="right" TEXT="Autor&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426524190913" ID="ID_658488762" MODIFIED="1441291233820">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Individual:</i>&#160;persona nat/jur que crea la obra
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426524215703" ID="ID_1608127515" MODIFIED="1441291236007" TEXT="Plural">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426524218424" ID="ID_564464771" MODIFIED="1441291241148">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Obra unitaria o en colaboraci&#243;n: </i>partes separables
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426524305650" ID="ID_663261868" MODIFIED="1441291299590">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Dchos: en proporci&#243;n determinada y req <u>consent de todos</u>
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node CREATED="1426524332449" ID="ID_1824474941" MODIFIED="1441291249946">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Obra colectiva:</i>&#160;partes inseparables
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426524346264" ID="ID_1361242695" MODIFIED="1441291311271">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Dchos: persona nat-jur que <u>coord&#160;y divulgue</u>
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1441291311278" ID="ID_488596254" MODIFIED="1441291315493">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Salvo pacto contrario
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1426524575861" ID="ID_132103024" MODIFIED="1426524589686" POSITION="right" TEXT="Derechos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426524577942" ID="ID_1810128597" MODIFIED="1441291508024" TEXT="Morales">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426524592660" ID="ID_374214527" MODIFIED="1441291898185">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      De personalidad. Irrenunciable, inalienable (no se ceden), perpetuo (no siempre), no explotable eco
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1426524700956" ID="ID_1893529134" MODIFIED="1441291333248">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Dchos:</i>&#160;decidir divulg, reconocim, integr y retirada
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426524737358" ID="ID_298585349" MODIFIED="1441291339215">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Duraci&#243;n:</i>&#160;vida autor
    </p>
  </body>
</html></richcontent>
<node CREATED="1426524760361" ID="ID_1194334076" MODIFIED="1441291413544">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Divulg de <u>obras in&#233;ditas</u>: +70 a&#241;os por heredero y seg&#250;n inter&#233;s social
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="stop-sign"/>
</node>
</node>
</node>
<node CREATED="1426524795920" ID="ID_1576122441" MODIFIED="1441291508680" TEXT="Patrimoniales">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426524802876" ID="ID_1546407490" MODIFIED="1441292770717">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      De explotaci&#243;n. Renunciable, se puede ceder (no exclusiva e intransf) y al final es p&#250;b
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1426524909479" ID="ID_24364436" MODIFIED="1441291450099">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Dchos:</i>&#160;reprod, distrib, transform, colecci&#243;n y com p&#250;b
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426524999309" ID="ID_1160037070" MODIFIED="1441291455592" TEXT="Duraci&#xf3;n">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426525037247" ID="ID_1825921897" MODIFIED="1441291869462">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Indiv: </i>vida autor+70 a&#241;os (si es pers f&#237;s)
    </p>
  </body>
</html></richcontent>
<node CREATED="1426525051838" ID="ID_1241880994" MODIFIED="1441291471041">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Obra unitaria: </i>vida &#250;ltimo coautor+70 a&#241;os
    </p>
  </body>
</html></richcontent>
<node CREATED="1426525068077" ID="ID_1980183156" MODIFIED="1441291475638">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Obra colectiva:</i>&#160;70 a&#241;os desde divulgaci&#243;n
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1426525107379" ID="ID_145791006" MODIFIED="1441291499247">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Computaci&#243;n: </i>1 enero a&#241;o <u>siguiente</u>&#160;&#160;a muerte o divulg
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="clock"/>
</node>
</node>
</node>
</node>
<node CREATED="1426525207187" ID="ID_288234335" MODIFIED="1441291520462" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pgms de ord
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1426525212875" ID="ID_327885575" MODIFIED="1441291532073" TEXT="Se protegen con LPI, aunque si forman parte de una patente tb pueden protegerse por Ley Patentes">
<icon BUILTIN="yes"/>
</node>
<node CREATED="1426525331952" ID="ID_668562400" MODIFIED="1441291545630">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Protege:</i>&#160;pgms + derivs, docum y manuales
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426525660425" ID="ID_1157783356" MODIFIED="1441291552999" TEXT="Ideas y ppios en los que se basa o pgm con efectos nocivos">
<icon BUILTIN="stop-sign"/>
</node>
<node CREATED="1426525402995" ID="ID_69197750" MODIFIED="1441291568787">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Desde elab de c&#243;digo fuente. Req que sea original (no novedad)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1426525544022" ID="ID_1293403585" MODIFIED="1441291855166">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Obra colectiva:&#160; </i>dchos de <u>empresa</u>&#160;si trabajador lo realiza en sus funcs (<u>salvo pacto</u>&#160;contrario)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441291840323" ID="ID_1712190668" MODIFIED="1441291845788" TEXT="Podr&#xed;a ser obra indiv">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1426525754963" ID="ID_167564851" MODIFIED="1441291823970" TEXT="Dchos patrimoniales">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426525782822" ID="ID_600827759" MODIFIED="1441291620241">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reprod, transform y distrib (1&#170; venta agota dcho&#8594; licencias)
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426525896725" ID="ID_1011226766" MODIFIED="1441291625969">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No req autorizaci&#243;n de autor
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426525904367" ID="ID_1774435854" MODIFIED="1426526184323" TEXT="Reproducir/transformar">
<node CREATED="1426526184761" ID="ID_192516403" MODIFIED="1426526185619" TEXT="si es necesario para el uso del pgm (salvo pacto)"/>
<node CREATED="1426526186112" ID="ID_1248958175" MODIFIED="1426526235732" TEXT="para obtener info de su interoperabilidad no dispuesta"/>
</node>
<node CREATED="1426526105755" ID="ID_826211312" MODIFIED="1441291997488" TEXT="Hacer versiones (salvo pacto), observ de su funcionamiento y copia de seg"/>
</node>
<node CREATED="1426526795889" ID="ID_869723163" MODIFIED="1441291695235">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Infracci&#243;n:</i>&#160;poseer o circular copias con fines <u>comerciales</u>, o instrum q facilite&#160; <u>supresi&#243;n</u>&#160;de protec
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="smiley-angry"/>
</node>
</node>
</node>
<node CREATED="1441291921791" ID="ID_1917836092" MODIFIED="1441291929928" POSITION="right" TEXT="Gesti&#xf3;n Dchos Dig (DRM)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426527188699" ID="ID_1522589373" MODIFIED="1441292101378">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tecns, herrams y procesos q protegen la PI en ops <u>comerciales</u>&#160;de contenidos dig
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1426527018813" ID="ID_840611810" MODIFIED="1441292713603">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Reglas de uso</u>&#160;para distrib controlada y seg&#160;de contenidos
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1441292511450" ID="ID_704341637" MODIFIED="1441292696603">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Permite <u>descarga</u>&#160;del contenido protegido, garantiza el proceso de&#160; <u>pago</u>&#160;e <u>impide la copia</u>&#160;o modif
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1426527350724" ID="ID_59395800" MODIFIED="1441292315673">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CV de dchos
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426527359270" ID="ID_1043226648" MODIFIED="1441292175051">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Empaq: </i>crear dchos, condics y reqs seguim
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426527410511" ID="ID_920633218" MODIFIED="1426527418065" TEXT="Autores y proveedores"/>
</node>
<node CREATED="1426527419638" ID="ID_951401032" MODIFIED="1441292186156">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Prot y venta:</i>&#160;fijar precio y modelo neg
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426527433127" ID="ID_1642704277" MODIFIED="1426527450780" TEXT="Proveedores y operadoras"/>
</node>
<node CREATED="1426527452193" ID="ID_1213830750" MODIFIED="1441292194440">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Consumo:</i>&#160;personalizar contenido y seguim
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426527469411" ID="ID_1703883976" MODIFIED="1426527585015" TEXT="Usuarios"/>
</node>
</node>
<node CREATED="1426527529121" ID="ID_1042799660" MODIFIED="1441292324090">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Soluciones
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426527533257" ID="ID_181553762" MODIFIED="1441292211838">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>C/S:&#160;</i>est&#225;ndar y propietarias
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426527549259" ID="ID_1103232116" MODIFIED="1441292222557">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cliente (agente DRM): </i>hw, sw, app
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426527565053" ID="ID_859448283" MODIFIED="1441292227355">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Servidor (plataforma DRM):</i>&#160;protege, empaqueta y gestiona dchos
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1426527588820" ID="ID_16357889" MODIFIED="1441292232491">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Servidor:</i>&#160;propietarias
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426527598261" ID="ID_985089705" MODIFIED="1441292266069">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Watermarking</i>
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1441292266075" ID="ID_648252759" MODIFIED="1441292266078">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Inclusi&#243;n de marcas en contenido para&#160; <u>seguim</u>&#160;de un objeto dig
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1441292269331" ID="ID_75617121" MODIFIED="1441292299479">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contiene&#160; <u>info de autor</u>&#160;o propietario del objeto (copyright)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1426527618546" ID="ID_38758163" MODIFIED="1441292295075">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Mixto: </i>plataforma DRM compleja
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1426527728115" ID="ID_1527102209" MODIFIED="1441292328046">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tecnolog&#237;as
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426527732276" ID="ID_1677700514" MODIFIED="1441292311339" TEXT="Sintaxis y sem&#xe1;ntica de las reglas de uso (lengs)">
<icon BUILTIN="info"/>
<node CREATED="1441292433095" ID="ID_1550158898" MODIFIED="1441292733817">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Utilizan&#160; <u>licencias</u>&#160;con permisos&#160;(ops posibles) y restriccs (ctrl consumo)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1426527761448" ID="ID_1186030019" MODIFIED="1441292397778">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Rights Expression (REL):</i>&#160;basado en XrML. Est&#225;ndar de<u>ISO</u>&#160;para MPEG21
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1426527973800" ID="ID_894940805" MODIFIED="1441292415792">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Open Dig&#160;Rights (ODRL): </i>est&#225;ndar de <u>W3C</u>, tiene versi&#243;n m&#243;vil
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1426528110618" ID="ID_251743193" MODIFIED="1441292430732">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Extensive Media Commerce (XMLC): </i>est&#225;ndar de <u>Real</u>&#160;Networks (RealPlayer)
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1441292789402" ID="ID_477363255" MODIFIED="1441292834145" TEXT="BSA">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441292793362" ID="ID_927968104" MODIFIED="1441292824983">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Business SW Alliance) org&#160; <u>antipirater&#237;a</u>, evita violaci&#243;n de dchos de copyright
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</map>
