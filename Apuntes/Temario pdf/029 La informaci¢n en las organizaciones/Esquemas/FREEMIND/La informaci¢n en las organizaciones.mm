<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1366309279229" ID="ID_231101798" MODIFIED="1381483175001" TEXT="La informaci&#xf3;n en las organizaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366309294548" FOLDED="true" ID="ID_1466376253" MODIFIED="1384274017589" POSITION="right" TEXT="LAURENCE PRUSAK: el conocimiento en las organizaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366824821276" ID="ID_399446544" MODIFIED="1384273715148" TEXT="Ventaja competitiva">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1366309478333" FOLDED="true" ID="ID_1652522635" MODIFIED="1384273974705" POSITION="right" TEXT="WENDERFEL (1984): Teor&#xed;a de los recursos y capacidades">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366309513138" ID="ID_410491823" MODIFIED="1377503186337" TEXT="Activos intangibles">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366309527568" ID="ID_1852844262" MODIFIED="1366309533827" TEXT="Estructura externa"/>
<node CREATED="1366309538150" ID="ID_330144000" MODIFIED="1366309544936" TEXT="Estructura interna"/>
<node CREATED="1366309570832" ID="ID_980455529" MODIFIED="1366309581892" TEXT="Competencia individual"/>
</node>
</node>
<node CREATED="1366309654874" ID="ID_1846437569" MODIFIED="1384273726848" POSITION="right" TEXT="BERENGUER: Caracter&#xed;sticas de las organizaciones basadas en el conocimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366309685015" ID="ID_638748595" MODIFIED="1384273729546" POSITION="right" TEXT="DAVENPORT Y PRUSAK: Definici&#xf3;n de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366309735374" FOLDED="true" ID="ID_1067536352" MODIFIED="1384273972802" POSITION="right" TEXT="POLANVI: Taxonomia del conocimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366309776168" ID="ID_1586152254" MODIFIED="1377503151640" TEXT="T&#xe1;cito">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366309788242" ID="ID_1044028146" MODIFIED="1377503161336" TEXT="Expl&#xed;cito">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1366309842811" ID="ID_1176562023" MODIFIED="1368360278405" POSITION="right" TEXT="BUENO (1993): Tipos de informaci&#xf3;n interna.">
<node CREATED="1366309864183" ID="ID_1032127068" MODIFIED="1366309882638" TEXT="Toma de decisiones"/>
<node CREATED="1366309885524" ID="ID_1635542715" MODIFIED="1366309919610" TEXT="Comunicaci&#xf3;n y coordinaci&#xf3;n de los individuos"/>
</node>
<node CREATED="1366309986877" ID="ID_1664943856" MODIFIED="1377503163176" POSITION="right" TEXT="PETER DRUCKER: Organizaci&#xf3;n aprendiente o adhocracia. Caracter&#xed;sticas.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1373879403857" ID="ID_894758185" MODIFIED="1377503172513" TEXT="Trabajador del conocimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1366310992305" ID="ID_501567660" MODIFIED="1366311001072" POSITION="right" TEXT="Un nuevo tipo de directivo"/>
<node CREATED="1366310533584" ID="ID_378304907" MODIFIED="1377503144906" POSITION="right" TEXT="ANTHONY: Piramide de niveles directivos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366310742250" ID="ID_252705873" MODIFIED="1377503147360" POSITION="right" TEXT="MINTZBERG. Estructura de las organizaciones. Organizaciones matriciales.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366310796820" FOLDED="true" ID="ID_795850891" MODIFIED="1384273967607" POSITION="right" TEXT="LAUDON Y LAUDON (1991): Nueva piramide de niveles directivos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366310857348" ID="ID_1172737313" MODIFIED="1377503196666" TEXT="Alta direcci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366310880531" ID="ID_1159976249" MODIFIED="1377503199538" TEXT="Direcci&#xf3;n t&#xe1;ctica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366310901014" ID="ID_747760073" MODIFIED="1377503201658" TEXT="Trabajadores del conocimiento y trabajadores de los datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366310929812" ID="ID_1545851878" MODIFIED="1377503207810" TEXT="Trabajadores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1366824767763" ID="ID_1230023747" MODIFIED="1384273740888" POSITION="right" TEXT="TAPSCOTT: inteligencia interconectada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</map>
