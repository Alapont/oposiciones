<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1441636010478" ID="ID_1216931956" MODIFIED="1441642671742">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T90/91
    </p>
    <p style="text-align: center">
      POSTDESARROLLO
    </p>
    <p style="text-align: center">
      DEL SW
    </p>
  </body>
</html></richcontent>
<node CREATED="1441636059454" ID="ID_1724703525" MODIFIED="1441642676504" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Matenimiento
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441637012294" ID="ID_1938897798" MODIFIED="1443772238999">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modifs de sw&#160;(<u>tras IAS</u>) para corregir errores, mejorar rendim/cal o adaptarlo a cambios del entorno
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
</node>
<node CREATED="1441636065291" ID="ID_449643559" MODIFIED="1441636067223" TEXT="Tipos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441637124303" ID="ID_1470816193" MODIFIED="1443772429589">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Seg&#250;n M&#233;trica&#160;v3
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441637171077" ID="ID_11847133" MODIFIED="1443773274395">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Correctivo:</i>&#160;de errores (puede introducir nuevos probls&#8594; pruebas de regresi&#243;n)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441637210758" ID="ID_158590026" MODIFIED="1443772276613">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Evolutivo:</i>&#160;cambio de reqs, nuevas necesidades
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1443772295420" ID="ID_875504478" MODIFIED="1443772515832">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No se tratan&#8594;
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1443772309850" ID="ID_947982897" MODIFIED="1443772331127">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Adaptativo: </i>a nuevo entorno f&#237;sico (migraci&#243;n)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1443772331849" ID="ID_1716740716" MODIFIED="1443772566431">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Perfectivo:</i>&#160;mejora cal (reing&#8594; aporta valor y previene errores, volunt)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1441637350191" ID="ID_1219222184" MODIFIED="1443772434603">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Seg&#250;n Bennet
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441637352324" ID="ID_617601174" MODIFIED="1443772415938">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Correctivo, perfectivo (=evolutivo Mv3), adaptativo y preventivo (=perfectivo Mv3)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1441636089233" ID="ID_980153364" MODIFIED="1443772600465" TEXT="Mantenibilidad">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441637821107" ID="ID_630629723" MODIFIED="1441637843694" TEXT="Facilidad de mantenim y posib de comprender y mejorar el sw">
<node CREATED="1443772610969" ID="ID_771408418" MODIFIED="1443772628618" TEXT="Es un factor de cal (atrib ext) con criterios (int, durante DSI)"/>
</node>
<node CREATED="1441637844977" ID="ID_1918198882" MODIFIED="1443772670218" TEXT="Influyen">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441637847038" ID="ID_370331721" MODIFIED="1443772972172" TEXT="Personal cualif, estruct comprensible, depuraci&#xf3;n, docum est&#xe1;ndar, dispo desarrolladores"/>
<node CREATED="1441638093361" ID="ID_1726674655" MODIFIED="1443772690397">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      En DSI (int)
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441637890823" ID="ID_1168710569" MODIFIED="1443772953161">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Revisar reqs, dise&#241;o (datos, acople m&#243;ds, cohesi&#243;n), c&#243;digo (estilo, leng pgm) y pruebas (previo)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1441636093092" ID="ID_1189020501" MODIFIED="1443772991245">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Costes
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441638117324" ID="ID_1533840898" MODIFIED="1443772786067">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Prop a costes DSI, variable (RT mucho +caro), aprox el 70% del coste total
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441638225307" ID="ID_1689941121" MODIFIED="1441638244123">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Intangibles:&#160;</i>insatisf cliente, -cal, trastornos en otros esfuerzos
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441638273652" ID="ID_1723985940" MODIFIED="1443773193882">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      COCOMO cl&#225;sico: EMA=E x TCA x t (1 a&#241;o)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1441638295231" ID="ID_1004036033" MODIFIED="1443773173315">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      EMA (Esf Manten Anual, pers-mes), E (Esf DSI inicial), TCA (Tr&#225;f Cambio Anual, %instruccs modif)&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1441636100455" ID="ID_446745851" MODIFIED="1441636185934" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reingenier&#237;a
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441636895567" ID="ID_404414809" MODIFIED="1441636972704">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Transform sistem del SI para <u>mejorar&#160;su calidad</u>&#160;a bajo coste, plan de desarrollo corto y bajo riesgo
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1441639531331" ID="ID_818501534" MODIFIED="1441639588678">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Examen y modif de un SI para ser reconstruido de forma nueva
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441639589423" ID="ID_1980504993" MODIFIED="1441641798226">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mejorar capacidades y <u>mantenibilidad</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1441641718668" ID="ID_512031990" MODIFIED="1441641744028" TEXT="-costes y errores, SI +f&#xe1;ciles, +vida, reutiliz, migraci&#xf3;n, cal, poder de reacci&#xf3;n">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1441641869291" ID="ID_1754993839" MODIFIED="1441641895641">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Causas: </i>fallos freq, rendim, tecn obsoleta, integr, mantenim caro, especs incompletas, migraci&#243;n
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441636123116" ID="ID_101806516" MODIFIED="1441639763237">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modelo IEEE
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441639491076" ID="ID_146030072" MODIFIED="1443773635289">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Niveles de abstracc:</i>&#160;Reqs, Dise&#241;o e Implem
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1441639763222" ID="ID_1892761969" MODIFIED="1441641913493">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reing incluye
    </p>
  </body>
</html></richcontent>
<node CREATED="1441639658899" ID="ID_1824260422" MODIFIED="1441641006610">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ing inversa:&#160;</i>analiza SI en orden inv para <u>id&#160;comps y sus rels</u>&#8594; nueva repres o <u>nivel sup</u>&#160;de abstrac
    </p>
  </body>
</html></richcontent>
<node CREATED="1441639732334" ID="ID_885993533" MODIFIED="1441659363035" TEXT="No implica modif ni generar otro SI">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1441639751871" ID="ID_1556171335" MODIFIED="1441641041230">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Reestructuraci&#243;n:</i>&#160;transforma <u>forma de repres</u>&#160;un SI en otra del&#160; <u>=nivel</u>&#160;de abstrac
    </p>
  </body>
</html></richcontent>
<node CREATED="1441639848941" ID="ID_606706404" MODIFIED="1441639865602" TEXT="Cambia forma del sw, pero no su funcionalidad">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1441639928484" ID="ID_373891758" MODIFIED="1443773481272">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ing directa:&#160;</i>desde alto nivel de abstrac <u>hasta implem f&#237;sica</u>&#160;del SI (=ing sw)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1441636162746" ID="ID_1604969119" MODIFIED="1441641114958">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Modelos de Procesos</b>
    </p>
    <p>
      (Pressman)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441640813406" ID="ID_1458880092" MODIFIED="1441641122508">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      C&#237;clico
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441640853618" ID="ID_1089929392" MODIFIED="1443773522061">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      An&#225;lisis inventario (apps candidatas)&#8594; Reestruct&#160;docs&#8594; <u>Ing Inv</u>&#8594; <u>Reestruct</u>&#160;c&#243;digo y datos&#8594; <u>Ing directa</u>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441641122930" ID="ID_1046735687" MODIFIED="1441641126889" TEXT="Herradura">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441641632236" ID="ID_1961811431" MODIFIED="1443773625319">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Niveles de abstracc:&#160;</i>Estruct de c&#243;digo, nivel Funcional y Conceptual
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1441641126893" ID="ID_235670318" MODIFIED="1443773596245">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      An&#225;lisis SI (recup arq=Ing Inv)&#8594; Transf l&#243;g (arq deseable=Restruct)&#8594; Desarr nuevo SI (constr=Ing Dir)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1441636127891" ID="ID_1192775092" MODIFIED="1443773709830">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Enfoque moderno
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441640035013" ID="ID_1597098674" MODIFIED="1443773726971">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Caja blanca (Ing Inv):</i>&#160;req conocer <u>bajo nivel</u>&#160;de abstracci&#243;n (c&#243;d fuente)
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1441640063674" ID="ID_1375477736" MODIFIED="1443774030601" TEXT="Caja negra">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441640107505" ID="ID_1298286792" MODIFIED="1443773914652">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Solo req conocer <u>interfaces E/S</u>&#160;del SI
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441636052759" ID="ID_586110263" MODIFIED="1443773964766">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Act&#250;a sobre <u>Legacy Systems</u><i>&#160;</i>(s&#170; heredado) que req adecuaci&#243;n&#160;a nuevas necesidades
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1441636156435" ID="ID_1921706455" MODIFIED="1443773741285">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Wrapping
    </p>
    <p>
      (Reing OO)
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<node CREATED="1441640260506" ID="ID_1333033133" MODIFIED="1441640781197">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>De UI:&#160;</i>parte +visible y f&#225;cil, pero no resuelve problema del LS
    </p>
  </body>
</html></richcontent>
<node CREATED="1441640295537" ID="ID_216817368" MODIFIED="1441640344165">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Screen scaping:</i>&#160;envuelve UI txt en entorno gr&#225;f (GUI)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441640764305" ID="ID_1752899323" MODIFIED="1443773880254">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>De datos:</i>&#160;accede a datos del LS con una interfaz diferente de la inicial
    </p>
  </body>
</html></richcontent>
<node CREATED="1443773768075" ID="ID_1332172382" MODIFIED="1443773784477">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      GW de BD, integr XML o replic de BD
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441640489845" ID="ID_331640825" MODIFIED="1443773888133">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Funcional:&#160; </i>encapsula datos y funcs del LS
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1443773813912" ID="ID_97961209" MODIFIED="1443773859736" TEXT="Integr con Common GW Interf (WS), CORBA o comps (Enterprise JavaBeans)"/>
</node>
</node>
</node>
</node>
<node CREATED="1441641961770" ID="ID_794003240" MODIFIED="1441641966524" TEXT="Modelo Harry Seed">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441641966528" ID="ID_984850687" MODIFIED="1443774072069" TEXT="Etapas para facilitar la toma de decisiones">
<node CREATED="1441641992073" ID="ID_1826413478" MODIFIED="1441642192959">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Justif del proy (an&#225;lisis cal sw actual)&#8594; An&#225;lisis cartera de apps (prioridad m&#225;x: <u>baja cal y alto valor</u>)&#8594; Estim costes (pondera comps sw)&#8594; Costes/Benefs (si es neg, desarrollo nuevo o adquirir)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1441636240244" ID="ID_751437366" MODIFIED="1441636243756" POSITION="right" TEXT="Abandono del SI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441642442562" ID="ID_1236154861" MODIFIED="1441642460028" TEXT="Si no es capaz de seguir ritmo de necesidades y su reing no es posible o es cara">
<icon BUILTIN="info"/>
<node CREATED="1441642461599" ID="ID_78103753" MODIFIED="1443774556674">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Consume recs y no significa empezar de 0 un desarrollo&#8594; se sustituye y se aprovecha
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1441642488831" ID="ID_903564445" MODIFIED="1443774131842">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Mining LS assets</i>
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1443774126125" ID="ID_859084227" MODIFIED="1443774186405">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      T&#233;cns de b&#250;sq de<u>recs valiosos</u>&#160;del LS para aprovechar
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1441642523124" ID="ID_164850262" MODIFIED="1441642570682">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Recogida info&#8594; Decisiones de recs a minar (OAR)&#8594; An&#225;lisis de comps&#8594; Rehab de recs elegidos
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441642269331" ID="ID_828642452" MODIFIED="1443774156737">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      M&#233;todo OAR (An&#225;lisis de Opcs para Reing)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<node CREATED="1441636222904" ID="ID_1522901474" MODIFIED="1441642295686">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Id y extrae <u>comps sw relevantes</u>&#160;de SI complejos y analiza <u>cambios</u>&#160;req para usarlos en nuevas arq
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1441642313095" ID="ID_369094227" MODIFIED="1441642430791">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contexto (an&#225;lisis LS)&#8594; Inventario comps&#8594; An&#225;lisis candidatos&#8594; Selecc de opcs de extracci&#243;n (estims)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1441636267904" ID="ID_1816559549" MODIFIED="1441660748454" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Migraci&#243;n de apps
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441636277247" ID="ID_1208232755" MODIFIED="1443774750603" TEXT="Ajuste dimensional">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441660192763" ID="ID_1379747453" MODIFIED="1441660233485">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mejor respuesta, -coste, no dep de un proveedor, oportunidad de mejora al reescribir, info accesible
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1441660260272" ID="ID_95172196" MODIFIED="1443774741660">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Criterios: </i>criticidad (en ord central, por seg), t de resp, control, interconectiv (mejor alta), exp usuario
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1443774750585" ID="ID_1302121991" MODIFIED="1443774751674" TEXT="Tipos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441659899725" ID="ID_910895228" MODIFIED="1441660678363">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Downsizing:</i>&#160;&#160;trasladar recs de proceso dsd gran ord ppal a ords de bajo coste&#8594;<u>distrib</u>&#160;en LAN
    </p>
  </body>
</html></richcontent>
<node CREATED="1441660083603" ID="ID_1572424914" MODIFIED="1443774768093">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Req reestruct la org y reescribir apps
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1441659966693" ID="ID_1561462451" MODIFIED="1443774648392">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Upsizing:</i>&#160;&#160;migrar apps personales/departam con info relevante a uso <u>global</u>&#160;(de BD local a corporativa)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1441659962796" ID="ID_379811457" MODIFIED="1441660685073">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Rightsizing:</i>&#160;&#160;migrar apps al <u>ord +apropiado</u>, complementando el ord central con ords especializados
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</map>
