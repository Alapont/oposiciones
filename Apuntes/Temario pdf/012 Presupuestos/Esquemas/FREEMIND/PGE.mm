<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1462179544784" ID="ID_1753076880" MODIFIED="1462179747151" TEXT="T11 PGE">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1462179765184" ID="ID_207720751" MODIFIED="1463053763084" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Intro</b>
    </p>
    <p>
      (LGP)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1462266902967" ID="ID_1482801465" MODIFIED="1463053677159">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Estado moderno</u>&#160;q interviene en aspectos socio-eco req una <u>ord</u>&#160;de gastos e ingresos
    </p>
  </body>
</html></richcontent>
<node CREATED="1463053897519" ID="ID_1388002575" MODIFIED="1463054035208">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i><b>Ley 48/2015</b>:&#160;</i>LPGE 2016
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1463053763054" ID="ID_212681875" MODIFIED="1463053771505">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>LGP</b>
    </p>
    <p>
      47/2013
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462180721678" ID="ID_1342891828" MODIFIED="1462390250451">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Expresi&#243;n cifrada, conj y sistem de <u>dchos/obligs a liquidar</u>&#160; durante ejerc <u>anual</u>&#160;de cada &#243;rg SPE
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1462267038531" ID="ID_939412584" MODIFIED="1462390843788">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Sost fin</u>&#160;&#160;AP, <u>confi</u>&#160;en estab ESP y comprom con <u>UE</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1462180770271" ID="ID_829092560" MODIFIED="1463053784043">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>&#193;mbito subj: </i><u>SPE</u>&#8594; Adm (AGE, OOAA, SS), Empr (EPE, soc merc, consorcs) y Fundac
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1462181280133" ID="ID_866774267" MODIFIED="1463053814814">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cont:</i>&#160;n&#250;cleo fund (gastos/ingrs) y&#160; <u>normas</u>&#160;sobre activ eco SP
    </p>
  </body>
</html></richcontent>
<node CREATED="1462181166639" ID="ID_812254140" MODIFIED="1463053892199">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Const:&#160;<b>art 134</b></i>&#8594; G-I&#160;del SPE y benef fisc (<u>tribs</u>)/ LPGE no puede crear tribs (solo ley trib)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1462262750698" ID="ID_551327376" MODIFIED="1463122387700">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Ley 2/2012</b>&#160;(art 135 CE)
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462262752629" ID="ID_1994859207" MODIFIED="1463122410515">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ppios: </i><u>estab</u>&#160;presu&#160;y <u>sost</u>&#160;fin, <u>plurian</u>,&#160;transp, respons, efici de recs p&#250;b y lealtad instit
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1463053935047" ID="ID_227434770" MODIFIED="1463123758202">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>L&#237;ms'20: </i>d&#233;f&lt;0,5%, deuda&lt;60% (ingr suman),<u>regla gasto</u>&#160;(increm gasto&lt;prev crec)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1463123710226" ID="ID_868725506" MODIFIED="1463123740028">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Medidas</u>&#160;prev, correct y coerc para asegurar cumplim
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1462182666654" ID="ID_91259684" MODIFIED="1462267423990" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1. Elab
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462267423971" ID="ID_1434002905" MODIFIED="1462267427036" TEXT="Previo">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462263386462" ID="ID_1658123228" MODIFIED="1462390630591">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Semestre EU: </i>ciclo de <u>coord</u>&#160;de pol&#237;ts eco-presup los primeros 6 meses (Pacto fiscal)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1462273438924" ID="ID_1658366979" MODIFIED="1463122595142">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CE recibe borrador presup con <u>Pgm Estab y Crec</u>&#160;(3 a&#241;os) y da recos
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1462263152460" ID="ID_1846458060" MODIFIED="1463123071774">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pgm Estab (esc plurian)&#8594;&#160; <u>Obj estab presup</u>&#160;y deuda (CM, 3 a&#241;os)&#8594; aprob <u>CG</u>&#8594; Elab
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1462390613272" ID="ID_1756483426" MODIFIED="1463123555145">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>AIReF</u>&#160;informa de grado cumplim (no ctrola)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1462390638119" ID="ID_1961025189" MODIFIED="1463122891926">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Elab proy
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462182936699" ID="ID_1661083800" MODIFIED="1462392109500">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Gob</b>&#8594; <u>3 meses</u>&#160;antes de expir a&#241;o anterior&#8594; Remite a CG
    </p>
  </body>
</html></richcontent>
<node CREATED="1462181776881" ID="ID_1085030988" MODIFIED="1462390758086">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Objs, cr&#233;ditos, proys de invers, liquid anterior, balance SS, informe eco-fin, benefs fisc
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1462179767325" ID="ID_1158088056" MODIFIED="1463054158518">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estruct
    </p>
    <p>
      (MinHAP)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462183005445" ID="ID_69876262" MODIFIED="1462390805578">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Gastos (cr&#233;ditos)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462183026308" ID="ID_1908144936" MODIFIED="1462390811522">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Org&#225;nica: </i>asign por <u>secciones</u>&#160;(centrs gestores)&#8594; AGE, OOAA, SS...
    </p>
  </body>
</html></richcontent>
<node CREATED="1462183105032" ID="ID_783640541" MODIFIED="1462183133028">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Por pgms:</i>&#160;seg&#250;n <u>objs</u>&#160;a conseguir
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1462183135362" ID="ID_1870529726" MODIFIED="1463054425152">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Eco: </i><u>cap&#237;tulos</u>&#8594; ops corrientes, capital (invers), financ y Fondo&#160; <u>Conting</u>&#160;(imprev)&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1462263016872" ID="ID_293513350" MODIFIED="1462263029132">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Territ:&#160;</i>CCAA y provs
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1462183369586" ID="ID_1791753313" MODIFIED="1463054277685">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ingresos:</i>&#160;Org&#225;nica y Eco (impuestos, tasas, patrim...)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1462182360815" ID="ID_1699467298" MODIFIED="1462390669164" TEXT="Pr&#xf3;rroga">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462182365396" ID="ID_964143225" MODIFIED="1462390770992">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si no se aprueba en plazo, se prorroga <u>autom&#225;t</u>&#160;PGE anterior&#8594; pero req <u>ajustes</u>&#160;a sit actual&#8594; <u>RD-Ley</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1462182704798" ID="ID_1764491727" MODIFIED="1462264176082" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      2. Aprob
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462263136229" ID="ID_915068506" MODIFIED="1462392115581">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>CG</b>
    </p>
  </body>
</html></richcontent>
<node CREATED="1462182135351" ID="ID_746442843" MODIFIED="1463123133402">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Debate parlam</u>&#160;en Congreso&#8594; <u>Senado</u>&#160;(an&#225;logo)&#8594; aprob def Congr&#8594; Rey&#8594; BOE
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1462181571409" ID="ID_444397178" MODIFIED="1462264185419" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      3. Ejec
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462263146138" ID="ID_105388853" MODIFIED="1462391026125">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Gob</b>&#160;a trav&#233;s de <u>centros gestores</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1462186215844" ID="ID_888090932" MODIFIED="1462391000053">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Varios proced seg&#250;n tipo de gasto&#8594; se permite <u>acumul</u>&#160;de fases (=efectos, &#250;nico doc 'ADOK')
    </p>
  </body>
</html></richcontent>
<node CREATED="1463123191351" ID="ID_895007486" MODIFIED="1463123215811">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>SIC: </i>contab de centros gest
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="kaddressbook"/>
</node>
</node>
</node>
<node CREATED="1462186277203" ID="ID_1557069200" MODIFIED="1463055126552">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>1. Autoriz</b>&#160;(int)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462186333044" ID="ID_1838605056" MODIFIED="1463054997329">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Autoridad compet para gestionar gasto&#160;(Min, Presi, Dir)&#160; <u>aprueba</u>&#160;su realiz determinando cuant&#237;a aprox
    </p>
  </body>
</html></richcontent>
<node CREATED="1462186538325" ID="ID_943413340" MODIFIED="1462264015282">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Doc contable 'A'
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1462186412546" ID="ID_528172191" MODIFIED="1463060336440">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Tr&#225;mites previos:</i>&#160;propuesta&#8594; certif <u>exist</u>&#160;cr&#233;dito (centro gestor)&#8594;&#160;<u>fiscaliz</u>&#160;(IGAE)
    </p>
  </body>
</html></richcontent>
<node CREATED="1462187141952" ID="ID_1708609054" MODIFIED="1462187161965">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si gasto&gt; <u>12M</u>&#8364;&#8594; req autoriz <u>CM</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1462186283865" ID="ID_741843697" MODIFIED="1463055143899">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>2. Dispo/comprom</b>&#160;(ext)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462186635819" ID="ID_1045759938" MODIFIED="1463055276207">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Aut compet <u>acuerda</u>&#160;con un 3&#186; lo que se ha autorizado (contrat, subvs)
    </p>
  </body>
</html></richcontent>
<node CREATED="1462186727027" ID="ID_957926877" MODIFIED="1462264043721">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Doc contable 'D'
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1462186301152" ID="ID_629425160" MODIFIED="1463055164998">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>3. Reconoc de Oblig</b>&#160;(jur)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462186776918" ID="ID_1340158698" MODIFIED="1463055288144">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Aut compet <u>acepta deuda</u>&#160;a favor de un 3&#186; tras su cumplim&#8594; Orden a Tesorer&#237;a del Estado y pago
    </p>
  </body>
</html></richcontent>
<node CREATED="1462186845341" ID="ID_386759966" MODIFIED="1462264073198">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Doc 'OK'
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1462179774167" ID="ID_617904012" MODIFIED="1462392207662">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Modifs</b>
    </p>
    <p>
      (adapt)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462267592837" ID="ID_882678059" MODIFIED="1462267600197">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      L&#237;mite
    </p>
    <p>
      cuantit
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462183739858" ID="ID_158324753" MODIFIED="1463054695407">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cr&#233;ds extraords y supls: </i>gastos <u>inaplazables</u>&#160;y no hay cr&#233;dito (extraord) o no ampliable (supl)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1462184123647" ID="ID_1746229351" MODIFIED="1463054851045">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ampliacs: </i>cr&#233;ds deriv de Deuda, pensiones o incluidos en la LPGE actual (pe cuotas SS)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1462184374822" ID="ID_1807835694" MODIFIED="1462391218656">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Gener de cr&#233;ditos por ingresos: </i><u>no previstos</u>&#8594; aport del Estado, ventas, reembolsos
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1462267670462" ID="ID_1496185348" MODIFIED="1462267732220">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      L&#237;m cualit
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462184365045" ID="ID_676883403" MODIFIED="1462267698817">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Tranfers:</i>&#160;de dotaciones entre cr&#233;ditos
    </p>
  </body>
</html></richcontent>
<node CREATED="1462184450413" ID="ID_1761954379" MODIFIED="1462391235348">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Dsd cr&#233;ditos de op <u>financ o capital</u>, entre cr&#233;ds de <u>dif seccs</u>, y no minoran extraords/ampliads
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="stop-sign"/>
</node>
</node>
</node>
<node CREATED="1462267711436" ID="ID_1855520993" MODIFIED="1462267715165" TEXT="L&#xed;mite temp">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462184393549" ID="ID_1465343984" MODIFIED="1462391274788">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Incorp de cr&#233;ditos:</i>&#160;del ejerc <u>anterior</u>&#8594; por ley, pagos anticip de retencs, cr&#233;ds extraord en &#250;ltimo mes
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1462179800177" ID="ID_1772914629" MODIFIED="1462392245734" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ctrl eco-fin
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462179803176" ID="ID_1029765531" MODIFIED="1463060491445">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>IGAE</b>&#160;
    </p>
    <p>
      (int)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462187963604" ID="ID_1249908574" MODIFIED="1463123292402">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Subsecret <u>indep</u>&#160;adscrita a <u>SEPG</u>&#160;(Poder Ejec, MinHAP)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1462187928620" ID="ID_1351350123" MODIFIED="1463123369111">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ctrl legal, contable, efici (buena gesti&#243;n)&#160;y presup (objs LPGE)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1463060491428" ID="ID_1977164488" MODIFIED="1463060494049" TEXT="Funci&#xf3;n">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462187613999" ID="ID_1989163059" MODIFIED="1463060501293">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Interv</u>&#160;(gastos antes de ser aprob), ctrl <u>financ</u>&#160;perm&#160;(estab presup) y <u>audit</u>&#160;p&#250;b (rev post)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1462188129347" ID="ID_859814904" MODIFIED="1462264601952">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Org
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="group"/>
<node CREATED="1462188146304" ID="ID_13974893" MODIFIED="1463060537499">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Central: </i>Gabinete T&#233;cn, Ofi Nac Audit y otras Subdirs
    </p>
  </body>
</html></richcontent>
<node CREATED="1462188179042" ID="ID_1203536028" MODIFIED="1462391447994">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Descentr:</i>&#160;IG Def y SS + IDelegs (Minists, &#211;rg P&#250;b, Reg/Territ, fuera de ESP)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1462179805163" ID="ID_1497988273" MODIFIED="1462190054718">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Trib </b>
    </p>
    <p>
      <b>Cuentas</b>
    </p>
    <p>
      (ext)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462189458960" ID="ID_1205171072" MODIFIED="1462189715934">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#211;rg supremo de <u>fiscaliz</u>&#160;ext perm de activ eco-fin del SP y <u>enjuiciam</u>&#160;de respons contable
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1462189592759" ID="ID_1180145994" MODIFIED="1462189720937">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Dep de <u>CG</u>&#160;pero act&#250;a con plena <u>indep</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1462189663685" ID="ID_993745417" MODIFIED="1462190585866">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Funci&#243;n
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462189667855" ID="ID_417668199" MODIFIED="1462190684056">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Fiscaliz
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462189938380" ID="ID_277007855" MODIFIED="1463060085224">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ex&#225;menes de CGE y parc, contratos, patrim y modifs presup
    </p>
  </body>
</html></richcontent>
<node CREATED="1462391762516" ID="ID_256311757" MODIFIED="1462391787810">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Informes</u>&#160;a CG y Memo anual con <u>props</u>&#160;mejora
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1462189828369" ID="ID_65452215" MODIFIED="1462268061040">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cuenta Gen del Estado:&#160;</i><u>balances</u>&#160;consolidados del <u>SPE</u>. Elab IGAE&#8594; examina TCuentas&#8594; aprob CG
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1462189672312" ID="ID_68247164" MODIFIED="1462190589018">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Jurisdicc
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1462190427800" ID="ID_384114002" MODIFIED="1462391672049">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Rendic de cuentas</u>&#160;de quienes intervengan&#8594;&#160;manejo recs p&#250;b, deuda injustif o cancel fianzas
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1462189008462" ID="ID_67311178" MODIFIED="1462264604874" TEXT="Org">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="group"/>
<node CREATED="1462189096658" ID="ID_142304090" MODIFIED="1463060575178">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Presi+Pleno&#160;(6C-6S), Comisi&#243;n Gob (r&#233;g personal), Fiscal&#237;a (dicts) y Secre Gen (soporte)
    </p>
  </body>
</html></richcontent>
<node CREATED="1462189216198" ID="ID_442973158" MODIFIED="1463060596500">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Seccs:</i>&#160;de Fiscaliz y de Enjuic (en Salas)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1462190633622" ID="ID_1904629617" MODIFIED="1463055791841">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>CG </b>(ext)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1463055780259" ID="ID_108848958" MODIFIED="1463055795858">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ctrl&#160; <u>pol&#237;tico</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1462190745196" ID="ID_1127376482" MODIFIED="1462391966477">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>A priori:&#160;</i>aprob <u>leyes</u>&#8594; LGP, ops cr&#233;dito y Deuda P&#250;b, y tribs
    </p>
  </body>
</html></richcontent>
<node CREATED="1462190898349" ID="ID_1322612170" MODIFIED="1462391970692">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>A posteriori:</i>&#160;aprob de la <u>CGE</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1462190961982" ID="ID_1362739934" MODIFIED="1462392017103">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Durante: </i><u>sesiones</u>&#160;ctrl (investig, interpels, mociones y proposics) y&#160;&#160; <u>Ofi Presup</u><i>&#160;</i>(seguim&#160;y ctrl de ejec)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
</map>
