<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1445439437225" ID="ID_633266407" MODIFIED="1448013428259">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T118 GESTI&#211;N
    </p>
    <p style="text-align: center">
      IDS Y ACCESOS
    </p>
    <p style="text-align: center">
      (IAM)
    </p>
  </body>
</html></richcontent>
<node CREATED="1445440146484" ID="ID_1536581535" MODIFIED="1448013458860" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      S&#170;integrado de pol&#237;ticas y procs org para&#160; <u>ctrl de acceso</u>&#160;a los SI e instals
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1445440794572" ID="ID_1047679126" MODIFIED="1448014554023">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Serv personaliz, reqs audit y ctrl, integr datos (cambios en user se propagan), escalab (autom)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1445440213081" ID="ID_249454207" MODIFIED="1448014583819">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Protec BD/apps, <u>AAAA</u>&#160;(Autent, Autoriz, Account-trazab, Audit) y normas (LOPD, LAECSP)&#160;
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1448013053657" ID="ID_658446478" MODIFIED="1448014994382" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Id dig 2.0
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448013277134" ID="ID_1983401058" MODIFIED="1448013334250">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>&#218;nica ID</u>&#160;transp y flex en vez de m&#250;ltiples nombres y contrase&#241;as para registrarse en sitios web
    </p>
  </body>
</html></richcontent>
<node CREATED="1448013338675" ID="ID_1867274148" MODIFIED="1448013344049" TEXT="pe OpenID, MS Cardspace"/>
</node>
<node CREATED="1448013129850" ID="ID_553767623" MODIFIED="1448015023223">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>ORM:</i>&#160;(Online Reput Managment) b&#250;sq y an&#225;lisis de reput pers/prof a trav&#233;s de Online Media
    </p>
  </body>
</html></richcontent>
<node CREATED="1448013076502" ID="ID_1718371417" MODIFIED="1448015086135">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Reput: </i><u>rastro</u>&#160;q deja usuario en <u>Inet</u>&#160;x interrel con otros o contenidos&#160;
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1448014663898" ID="ID_490795238" MODIFIED="1448014666932" POSITION="right" TEXT="Comps">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448014724227" ID="ID_99541502" MODIFIED="1448015689399">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Gesti&#243;n Ids (CV), Ctrl Acceso (SSO), Directorio (LDAP, ids), BDs (SO,mail,ERP/CRM), Prov Users (reglas)&#160;&#160;
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1448014362921" ID="ID_1161957131" MODIFIED="1448015103475">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>CV de Id:</i>&#160;Creaci&#243;n users&#8594; Gesti&#243;n cambio y mant (permisos)&#8594; Fin (user deja org, baja de perfil)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1448015266373" ID="ID_1003914023" MODIFIED="1448015525725" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Login
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445441295790" ID="ID_996059018" MODIFIED="1445447102165">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Single Sign On</i>
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445444422278" ID="ID_1483285043" MODIFIED="1448018420024">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      User solo&#160; <u>se autentica una vez</u>&#160;para acceder a todos los servs de la org mediante un <u>token</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1445441517711" ID="ID_1692411969" MODIFIED="1448015420115">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Req +protec de credenciales (+impacto si se pierden)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1448015434056" ID="ID_1013358993" MODIFIED="1448015482015">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      R&#225;pido acceso, usab y seg (-riesgo x passwords)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1448018316253" ID="ID_1591838446" MODIFIED="1448018338945">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Simple SO: </i>guarda en el Token el user+pass (PYMES)
    </p>
  </body>
</html></richcontent>
<node CREATED="1448018339502" ID="ID_1834510310" MODIFIED="1448018376806">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Reduce SO:</i>&#160;dif pol&#237;ticas de login seg&#250;n entorno y apps
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1445447102133" ID="ID_1257749444" MODIFIED="1448015531264">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1 dominio
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445439619345" ID="ID_1686774603" MODIFIED="1448019030177">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Kerberos 5.0
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1448015740569" ID="ID_499561567" MODIFIED="1448016767173">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Autentic mutua C/S, cript&#160; <u>sim&#233;trica</u>-DES y 3&#186; de confianza (KDC)
    </p>
  </body>
</html></richcontent>
<node CREATED="1445441856060" ID="ID_1465455695" MODIFIED="1448016774997">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>BD Claves Secretas: </i>solo la conocen C/S y Kerberos
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1448015800123" ID="ID_780243572" MODIFIED="1448016006268">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Key Distrib Center</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1448016006273" ID="ID_650856467" MODIFIED="1448016271764">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Directorio con&#160; <u>Authent</u>&#160;Serv (AS, conoce todas las claves) y Ticket Grant Serv (TGS, emite tickets)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1445441730674" ID="ID_1998083479" MODIFIED="1445442561568">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Func</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1448016373293" ID="ID_126621429" MODIFIED="1448016744916">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      C se autentica en AS y le devuelve un Ticket Grant (TGT, cifrado con su clave priv) para acceder al TGS
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1448016408904" ID="ID_1873150203" MODIFIED="1448016832017">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      C le pide con su TGT al TGS un <u>Ticket</u>&#160;para poder usar un S durante cierto t sin volver a autenticarse
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1448016690226" ID="ID_602148919" MODIFIED="1448016900708">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      C se autentica con su Ticket en S,&#160;comprueba q C est&#225; autorizado para usar su servicio y accede
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1448016945991" ID="ID_1995951519" MODIFIED="1448017234215">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      E-SSO
    </p>
    <p>
      (Enterprise)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448017167933" ID="ID_488492626" MODIFIED="1448017198488">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      S&#170; heredados no pueden usar Kerberos (almacenar&#160;credenciales fuera)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="help"/>
<icon BUILTIN="yes"/>
<node CREATED="1445442775281" ID="ID_494062237" MODIFIED="1448017249178">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Sol&#8594; </i>sw propietario, se integra con otros s&#170; (web, ERPs..)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1448017225383" ID="ID_683422601" MODIFIED="1448017225386">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Rellena autom</u>&#160;info de autentic de user
    </p>
  </body>
</html></richcontent>
<node CREATED="1445442936012" ID="ID_313725683" MODIFIED="1448017091267">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Implant: </i><u>scripts</u>&#160;para cada app (cogen credenciales) o<u>App</u>&#160;escritorio (monitoriza user)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1445439643558" ID="ID_333883970" MODIFIED="1448017326457">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Web-SSO
    </p>
    <p>
      (Cookies)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445443072498" ID="ID_1589644926" MODIFIED="1448018120930">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Id en <u>apps web</u>&#160;a trav&#233;s de naveg
    </p>
  </body>
</html></richcontent>
<node CREATED="1445443424429" ID="ID_1603033467" MODIFIED="1448018165541">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Info de autentic va en stream HTTP (visible)&#8594; Posible robo de cookies&#8594; Sol: <u>HTTPS</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1445443122610" ID="ID_1651610904" MODIFIED="1448017862485">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Func
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445443128287" ID="ID_511824085" MODIFIED="1448018002669">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      C (naveg) accede a p&#225;g web por HTTP y el Serv Web (WS) le redirige al AS
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1448017963833" ID="ID_235746937" MODIFIED="1448018103902">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      C se autentica en el AS y le devuelve una&#160; <u>cookie</u>&#160;de sesi&#243;n&#160;para acceder a la p&#225;g web
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1445443380111" ID="ID_1820840688" MODIFIED="1448018087535">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      C accede con la cookie a la p&#225;g web, WS comprueba q es v&#225;lida y no ha expirado y le presenta info
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1445439650738" ID="ID_1927117162" MODIFIED="1448018482722">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Multidom
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445444086419" ID="ID_1793362343" MODIFIED="1448018994331">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Federaci&#243;n </i>
    </p>
    <p>
      <i>de Entidades</i>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="info"/>
<node CREATED="1447952545556" ID="ID_1654913205" MODIFIED="1448018948301">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Intercambio de <u>id</u>&#160;de usuarios en diferentes <u>dominios</u>&#160;de seg
    </p>
  </body>
</html></richcontent>
<node CREATED="1448018614015" ID="ID_185489968" MODIFIED="1448018655906">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Eficiencia (sin redunds), sync ids, agrupaci&#243;n, audit e informes
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1445444189114" ID="ID_219048963" MODIFIED="1448018768379">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Proveedor de Id:</i>&#160;(IdP) infraestr para autenticar users (1/dominio)
    </p>
  </body>
</html></richcontent>
<node CREATED="1445444213150" ID="ID_1792294188" MODIFIED="1448018783059">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Prov de Servs:</i>&#160;(SP) da acceso al usuario a un recurso (&gt;1/dominio)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1445444323339" ID="ID_1553437000" MODIFIED="1448019165038">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>C&#237;rc de Conf:</i>&#160;(CoT) entidads cn <u>SLA</u>&#160;para suministrar servs a users comunes en entorno seg multidom
    </p>
  </body>
</html></richcontent>
<node CREATED="1448019067878" ID="ID_377056964" MODIFIED="1448019122530">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>FIDELITY: </i>proy EU para id entre CoTs
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1445439698212" ID="ID_1547537720" MODIFIED="1448019025916">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      SAML 2.0
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1445444660260" ID="ID_1448188938" MODIFIED="1448019266061">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Sec Assertion, OASIS) especs de&#160; <u>intercambio</u>&#160;de credenciales entre dif dominios
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1445444751014" ID="ID_1418748131" MODIFIED="1445445249264">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Assertions: </i>paqs XML con <u>info de id</u>&#160;de un sujeto&#8594;
    </p>
  </body>
</html></richcontent>
<node CREATED="1445444774963" ID="ID_1290274100" MODIFIED="1448019443940">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Authent (qui&#233;n y a qui&#233;n), Atrib (info de user), Authoriz (permisos)&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1445444876838" ID="ID_843324548" MODIFIED="1448019565578">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Prots:</i>&#160;<u>petics</u>&#160;y resps de aserciones, con su propio Esq XML
    </p>
  </body>
</html></richcontent>
<node CREATED="1445444969884" ID="ID_156930486" MODIFIED="1448019534734">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Auth Req (msj), Assertion Query&amp;Req (ya existentes), Artifact (refs)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1445445077482" ID="ID_1120880593" MODIFIED="1445445479687">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Bindings:</i>&#160;tx de msjs con <u>HTTP o SOAP</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1445445510981" ID="ID_1900068270" MODIFIED="1445445562296">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Seg: </i>usa XML Encrypt (confid) y XML Signature (autent e integr)&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1445445340489" ID="ID_1445193591" MODIFIED="1445445503383">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Profiles: </i>Assert, Prots y Bindings + interaccs para satisfacer un caso de uso
    </p>
  </body>
</html></richcontent>
<node CREATED="1445445343383" ID="ID_1109789761" MODIFIED="1445445873375">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Web Browser SSO, Assert Query&amp;Req, Artifact Resol, IdP Discovery
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1448019700704" ID="ID_1223909549" MODIFIED="1448019791312">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Implems (libres):</i>&#160;OpenSAML (libs C-Java), SimpleSAML PHP, Shibboleth (entorno universitario)&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1445445585954" ID="ID_1406652205" MODIFIED="1448022112366">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>WS-Federation:</i>&#160;negoc info de ids entre doms
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1445445630275" ID="ID_1524463262" MODIFIED="1445447483044">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>WS-Sec/Trust/SecPolicy:</i>&#160;&#160;gesti&#243;n de assertions y tokens de seg (Kerberos, SAML) para WS
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1445445673419" ID="ID_409260076" MODIFIED="1448022119207">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Windows CardSpace:</i>&#160;ids como tarjetas de <u>info visual</u>&#160;&#160;(UI)&#8594; user selecciona y app solicita token a IdP
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1445445751608" ID="ID_1968355052" MODIFIED="1448020303674">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se basa en prots WS-*&#8594; compatibles
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1448017379051" ID="ID_396955790" MODIFIED="1448020404107">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>OpenID: </i>SSO distrib y descentraliz, id=<u>URL</u>&#160;q cualquier app/serv puede verif
    </p>
  </body>
</html></richcontent>
<node CREATED="1445445997267" ID="ID_297297511" MODIFIED="1445446030804">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      User solo req id creado en serv compat con OpenID. No especifica mec de autentic
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1445446033343" ID="ID_1178776016" MODIFIED="1445446068883">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Facebook Connect:&#160;</i>permite usar id de FB en otras web. Integra info de su perfil
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1448020514418" ID="ID_329290110" MODIFIED="1448021450369" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      AAPP EU
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448020516471" ID="ID_718698652" MODIFIED="1448021430501">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ppios de interop
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448020527218" ID="ID_1409268884" MODIFIED="1448020679758">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Usab (seg), Id offline y online (mecs req), apoderams y autorizs, valid online, consenso de terminolog&#237;a
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1448020692600" ID="ID_1567620468" MODIFIED="1448021430882">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Criterios de dise&#241;o
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448020699021" ID="ID_1035847925" MODIFIED="1448020822442">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Federado, seg multinivel, confianza en fuentes, aproxim por sectores o contextos, mercado priv&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1448020830469" ID="ID_852263851" MODIFIED="1448021431213" TEXT="STORK">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448020833341" ID="ID_872722536" MODIFIED="1448020921862">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Sec Id across borders linked) plataf de interop EU de ids-e&#8594; acept de certifs dig (DNIe)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1445439758096" ID="ID_563087592" MODIFIED="1445439760879" POSITION="right" TEXT="Teletrabajo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445439760884" ID="ID_1799813670" MODIFIED="1445439765565" TEXT="Desktop Sharing">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445446170546" ID="ID_843158942" MODIFIED="1445446354039">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Acceso remoto al ord del trabajo mediante <u>emulador de terminal gr&#225;fico</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1445446421639" ID="ID_956784550" MODIFIED="1445446441964">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No permite trabajar fuera de un solo ambiente NAT
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1445446354849" ID="ID_429149392" MODIFIED="1447952581477">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      X Window Sys (Unix), Remote Desk Prot (RDP, Windows) o Virtual Netw Computing (VNC, libre)&#160;&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1445439766404" ID="ID_351189544" MODIFIED="1445439769614" TEXT="VPN">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445446452252" ID="ID_1212315790" MODIFIED="1445446523795">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Acceso <u>cifrado</u>&#160;a LAN de la empresa mediante<u>t&#250;nel</u>&#160;por red pub (Internet)
    </p>
  </body>
</html></richcontent>
<node CREATED="1445446528768" ID="ID_1438043675" MODIFIED="1447952603831">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      IPSec o OpenVPN (libre=multiplat, SSL, pto a pto, WiFi, no interop con IPSec)&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
</map>
