<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1427617532922" ID="ID_1127508678" MODIFIED="1480701407859">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      M&#201;TRICA
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1427618029425" ID="ID_558473214" MODIFIED="1447330894922" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Metodolog&#237;a </b>
    </p>
    <p>
      <b>de desarrollo</b>
    </p>
  </body>
</html></richcontent>
<node CREATED="1447330861504" ID="ID_459054482" MODIFIED="1447330861507">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Gu&#237;a con comprobs sistem&#225;ticas, coherente y dirigido a objetivos
    </p>
  </body>
</html></richcontent>
<node CREATED="1427618189565" ID="ID_823665689" MODIFIED="1447330963350">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>MCV:</i>&#160;activs y orden&#8594; <i>Metodolog&#237;a: </i>c&#243;mo y qui&#233;n, <u>neutro</u>&#160;respecto a MCV
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1427618252332" ID="ID_1982059308" MODIFIED="1447330922579" TEXT="SI para consegiur fines, satisfacc de reqs, +productiv, com entre particips, f&#xe1;cil mantenim y uso">
<icon BUILTIN="xmag"/>
<node CREATED="1447331035328" ID="ID_485346181" MODIFIED="1447331048821" TEXT="No es de uso privativo para las AP">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1427618687488" ID="ID_1039844460" MODIFIED="1447330893263" POSITION="right" TEXT="Versiones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427618387694" ID="ID_346949029" MODIFIED="1447330996904">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>V2</b>: no mantenim, estructurado, no contempla calidad (PGGC) ni seguridad (Magerit), pocas t&#233;cnicas
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1427618414229" ID="ID_865003997" MODIFIED="1447331457294">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>V3</b>: <u>mantenim</u>, estructurado y&#160; <u>OO</u>, interfaces de <u>cal y seg</u>, gu&#237;a de t&#233;cnicas <u>amplia</u>&#160;y herrams
    </p>
  </body>
</html></richcontent>
<node CREATED="1427618474675" ID="ID_594087578" MODIFIED="1447331021677">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>CSAE (2001): </i>unifica Metodolog&#237;a+PGGC+Magerit
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1427619385379" ID="ID_1637118414" MODIFIED="1427619488458" POSITION="right" TEXT="Procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427619388843" ID="ID_1950756403" MODIFIED="1447331468177">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Basados en
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1447331147613" ID="ID_268736371" MODIFIED="1447333138995">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>ISO 12207: </i>(excepto PSI) procesos del <u>CV</u>&#160;de desarrollo sw&#8594; adquis, sumin, desarr, op y mantenim
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1447331158636" ID="ID_1996249055" MODIFIED="1447331242178">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>ISO 15504: </i>SPICE&#8594; determ de capacidad y<u>mejora</u>&#160;de procesos
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1427619503062" ID="ID_1805491366" MODIFIED="1427619510687" TEXT="Planificaci&#xf3;n (PSI)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427620756460" ID="ID_133542697" MODIFIED="1447331662842">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Marco <u>estrat&#233;gico</u>: sit actual, modelos de&#160; <u>arq de info</u>&#160;y plan de acci&#243;n (proys, seg y eval )
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1447331372005" ID="ID_1067968887" MODIFIED="1447331392352">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No incluye Riesgo&#8594;Interf SEG
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1427626944249" ID="ID_1161296710" MODIFIED="1447331416697">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Niveles:</i>&#160;Estrat&#233;gico (largo plazo, rel con ext), T&#225;ctico (funcionalidad del s&#170;) y Operativo (ejec)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1447331420535" ID="ID_339737874" MODIFIED="1447333701100">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reqs de info&#8594; Consultores y UE
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="fema"/>
<node CREATED="1447331488271" ID="ID_1893504614" MODIFIED="1447332304763">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Aprob&#8594; Direcci&#243;n (Comit&#233;)&#160;y Jefe Proy
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1427619511045" ID="ID_1889259047" MODIFIED="1427619522694" TEXT="Desarrollo">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427619516126" ID="ID_599967606" MODIFIED="1427619524046" TEXT="Estudio Viabilidad (EVS)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427621522398" ID="ID_1526783809" MODIFIED="1447331643285">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Soluci&#243;n a&#160; <u>corto plazo</u>&#160;a partir de an&#225;lisis de necesidades&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1447331548254" ID="ID_722874700" MODIFIED="1447331578007">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reqs del SI (generales)&#8594; JP, Analistas y UE
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="group"/>
</node>
</node>
</node>
<node CREATED="1427619524349" ID="ID_1756737415" MODIFIED="1427619530102" TEXT="An&#xe1;lisis (ASI)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427621791846" ID="ID_414998043" MODIFIED="1447331784646">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Especif detallada del SI: <u>cat&#225;log de reqs</u>, modelos (Estruct/OO) y&#160; <u>plan de pruebas</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1447331712593" ID="ID_593115820" MODIFIED="1447331765071">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reqs del SI (func y no)&#8594; Analistas y UE
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="fema"/>
</node>
</node>
</node>
<node CREATED="1427619530732" ID="ID_416161810" MODIFIED="1427619534134" TEXT="Dise&#xf1;o (DSI)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427622271316" ID="ID_94296506" MODIFIED="1447331888913">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Arq del s&#170;</u>&#160;(Estruct/OO) y del entorno tecn soporte, y&#160;especs de construc de&#160; <u>comps</u>&#160;del SI
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1427619534517" ID="ID_1149375343" MODIFIED="1447331981988">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Construc (CSI)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427622929918" ID="ID_917427795" MODIFIED="1447334252918">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>C&#243;digo</u>&#160;&#160;comps, pruebas&#160;(<u>unitarias, de integr y del s&#170; </u>), proceds de op y seg y <u>manuales</u>&#160;de user
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1447331943263" ID="ID_1294601037" MODIFIED="1447333970133">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pruebas&#8594; Programads (unit) y Analistas (eq proy, de integr y de s&#170;)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="fema"/>
</node>
</node>
<node CREATED="1427619540940" ID="ID_771988469" MODIFIED="1447332011407">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Implant y Acept (IAS)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427623696873" ID="ID_1930261223" MODIFIED="1447334236189">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Entrega</u>&#160;y paso a produc, <u>SLA</u>, formaci&#243;n y&#160;pruebas&#160;(de <u>impl y acept</u>)
    </p>
  </body>
</html></richcontent>
<node CREATED="1427623974797" ID="ID_1280451066" MODIFIED="1427623979673" TEXT="Proceso iterativo">
<icon BUILTIN="yes"/>
</node>
<node CREATED="1447332042835" ID="ID_1171615807" MODIFIED="1447333975486">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pruebas&#8594; Analistas (de impl) y UE (de acept)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="fema"/>
</node>
</node>
</node>
</node>
<node CREATED="1427619558572" ID="ID_1208896560" MODIFIED="1447334337254">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mantenim (MSI)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427624371955" ID="ID_1047125963" MODIFIED="1447334506198">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Nueva versi&#243;n</u>&#160;dl SI por petici&#243;n/mejora y pruebas&#160;(de <u>regresi&#243;n</u>, qcambios no afecten)
    </p>
  </body>
</html></richcontent>
<node CREATED="1447332042835" ID="ID_430567444" MODIFIED="1447334496493">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pruebas&#8594; JP (JP+resp mant) y Analistas (eq mant)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="fema"/>
</node>
</node>
<node CREATED="1427624393065" ID="ID_1013790437" MODIFIED="1447332522306">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Correctivo</u>&#160;(errores detectados) o <u>Evolutivo </u>&#160;(nuevos reqs)
    </p>
  </body>
</html></richcontent>
<node CREATED="1447332191295" ID="ID_683965062" MODIFIED="1447332236135">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Solo se definen&#8594;&#160;Perfectivo (evita errores, reing) y Adaptativo (nuevo entorno)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
<node CREATED="1427619258039" ID="ID_1409475425" MODIFIED="1427619377589" POSITION="right" TEXT="Interfaces">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427619263221" ID="ID_905233101" MODIFIED="1447333183992">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Agrupaciones que interrelacionan&#160;activs&#160;de&#160; <u>org o soporte</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
</node>
<node CREATED="1427619293728" ID="ID_838565926" MODIFIED="1427619379277" TEXT="Gesti&#xf3;n de Proy (GP)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427624605024" ID="ID_1026578345" MODIFIED="1447333631232">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Inicio (GPI):&#160; </i><u>estimaci&#243;n</u>&#160;de esfuerzo y <u>planif</u>&#160;del proyecto (tras EVS)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1427624628823" ID="ID_1250328108" MODIFIED="1447333283711">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Seg y ctrl (GPS):</i>&#160;&#160;vigilar correcto desarrollo y gesti&#243;n&#160; <u>incidencias</u>&#160;(seg&#250;n Eurom&#233;todo)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1427624676731" ID="ID_1002796869" MODIFIED="1447333270484">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Fin (GPF): </i><u>cierre</u>&#160;del proy y <u>registro</u>&#160;de docum de gesti&#243;n (tras MSI)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1427619352379" ID="ID_476389268" MODIFIED="1427619381116" TEXT="Gesti&#xf3;n de Config (GC)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427625031672" ID="ID_1326822941" MODIFIED="1447333302161">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Control y registro de&#160; <u>cambios</u>&#160;(reqs, prods) para mantener integridad
    </p>
  </body>
</html></richcontent>
<node CREATED="1427625116504" ID="ID_1152530373" MODIFIED="1447333311742" TEXT="Facilita mantenim (valora impacto cambios)">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1427619363108" ID="ID_1621656087" MODIFIED="1427619382341" TEXT="Aseguram Calidad (CAL)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427625230691" ID="ID_1221939505" MODIFIED="1447333347579">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Eval de proceso (Garant&#237;a de Cal) y producto (Ctrl de Cal) para evitar y corregir desviaciones
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1427625337215" ID="ID_360714469" MODIFIED="1447333418002">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Marco com&#250;n coherente con la org
    </p>
  </body>
</html></richcontent>
<node CREATED="1447333418006" ID="ID_1412939231" MODIFIED="1447333425429">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Grupo de Aseg de Cal:</i>&#160;(GAC) equipo <u>indep</u>&#160;del de desarrollo, creado en EVS
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1427619370972" ID="ID_1567583367" MODIFIED="1427619383789" TEXT="Seguridad (SEG)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427626369392" ID="ID_1202419590" MODIFIED="1427626843247" TEXT="Mecanismos adicionales para asegurar consistencia y seguridad">
<node CREATED="1427626437403" ID="ID_470896700" MODIFIED="1447333462807">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      An&#225;lisis de <u>riesgos</u>&#8594; Planes de Contingencia
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1427626847035" ID="ID_608920675" MODIFIED="1427626871086" TEXT="Actividades de seg del desarrollo (SPD) y del SI (SSI)"/>
</node>
</node>
</node>
<node CREATED="1427619564555" ID="ID_1850821293" MODIFIED="1427619566677" POSITION="right" TEXT="Productos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427619567708" ID="ID_486945108" MODIFIED="1447333527240">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>E/S</u>&#160;de tareas (documentaci&#243;n) mediante t&#233;cnicas/pr&#225;cticas, dependencias entre tareas
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1427619629102" ID="ID_997399126" MODIFIED="1427619685907" POSITION="right" TEXT="T&#xe9;cnicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427619632879" ID="ID_1305978696" MODIFIED="1427620633854" TEXT="Generales y formalizadas, est&#xe1;ndares, notaci&#xf3;n y criterios de calidad">
<node CREATED="1427619657957" ID="ID_513600914" MODIFIED="1447333605276">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estim, planif, C/B, diagr, modelado, normaliz, optimiz, reglas y matriciales
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1427619686121" ID="ID_549564262" MODIFIED="1427620173307" POSITION="right" TEXT="Pr&#xe1;cticas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427619743254" ID="ID_1175578637" MODIFIED="1427620589273" TEXT="Particulares y no formalizadas, r&#xe1;pido y seguro, sin criterios">
<node CREATED="1427619773773" ID="ID_836463012" MODIFIED="1427620110435" TEXT="Impacto, catalogaci&#xf3;n, representaci&#xf3;n, FCE, prototipado, pruebas y revisiones"/>
</node>
</node>
<node CREATED="1427620171696" ID="ID_315907200" MODIFIED="1427620204792" POSITION="right" TEXT="Herramientas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427620179064" ID="ID_1780975433" MODIFIED="1447333643643">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sw de soporte de t&#233;cnicas, <u>ajenas</u>&#160;a la metodolog&#237;a
    </p>
  </body>
</html></richcontent>
<node CREATED="1447333647116" ID="ID_100674420" MODIFIED="1447333669970">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Matriz de trazab de Reqs:</i>seguim de cumplim de reqs en todo el CV
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1427620206054" ID="ID_1774293159" MODIFIED="1427620209456" POSITION="right" TEXT="Participantes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427620248805" ID="ID_616783728" MODIFIED="1447333727914">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Directivo:</i>&#160;&#160;objs y validaciones
    </p>
  </body>
</html></richcontent>
<node CREATED="1427620263373" ID="ID_9840519" MODIFIED="1447333736243">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comit&#233;s (de direc y seg), Director de Usuarios y&#160; <u>Usuarios Expertos</u>&#160;(UE)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1427620286285" ID="ID_1951649494" MODIFIED="1447333754224">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Jefe de Proyecto: </i>estrategia, estructura, planif, control y coord
    </p>
  </body>
</html></richcontent>
<node CREATED="1427620317243" ID="ID_889226761" MODIFIED="1427621351139">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Jefe y <u>responsables</u>&#160;(de implant, mantenim, op, sistemas, seg y cal)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1427620357084" ID="ID_1516771486" MODIFIED="1447333774136">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Consultor:&#160; </i>asesoramiento
    </p>
  </body>
</html></richcontent>
<node CREATED="1427620361397" ID="ID_1906265056" MODIFIED="1447333785247">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Consultor (de neg, inf, TI, SI), especialista en comunics, t&#233;cnico de s&#170; y t&#233;cnico de comunics&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1427620413804" ID="ID_1609440456" MODIFIED="1447333797311">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Analista:&#160; </i>cat&#225;logo de reqs y modelos
    </p>
  </body>
</html></richcontent>
<node CREATED="1427620437465" ID="ID_1140369486" MODIFIED="1447333815881">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Analista, DBA, aseg de cal (GAC) y <u>equipos</u>&#160;(de proy, arq, form, seg, implant, op, soporte t&#233;cn)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1427620491867" ID="ID_617290155" MODIFIED="1447333832757">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Programador: </i>c&#243;digo y pruebas unitarias
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</map>
