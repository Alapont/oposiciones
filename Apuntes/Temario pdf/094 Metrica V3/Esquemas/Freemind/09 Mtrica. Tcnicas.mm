<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1377853104293" ID="ID_424125342" MODIFIED="1380988639327" TEXT="M&#xe9;trica. T&#xe9;cnicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370341167897" ID="ID_1846700051" MODIFIED="1376672700453" POSITION="right" TEXT="T&#xe9;cnicas">
<node CREATED="1370342700141" ID="ID_1153335944" MODIFIED="1376672529477" TEXT="Desarrollo">
<node CREATED="1370342707871" ID="ID_227799472" MODIFIED="1391361286116" TEXT="An&#xe1;lisis Coste/beneficio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372846008053" ID="ID_1958097411" MODIFIED="1378223153490" TEXT="Punto de amortizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372846019047" ID="ID_1106988658" MODIFIED="1378223155097" TEXT="Periodo de amortizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372846039344" ID="ID_1498497944" MODIFIED="1383568645611" TEXT="Retorno de la Inversi&#xf3;n (ROI)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377853155607" ID="ID_682569897" MODIFIED="1378223122227" TEXT="Beneficio Neto Actual">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377853168974" ID="ID_493326984" MODIFIED="1378223123818" TEXT="Coste de desarrollo Anualizado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377853178565" ID="ID_1480857675" MODIFIED="1378223125472" TEXT="Inversi&#xf3;n promedio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378632799540" ID="ID_377418752" MODIFIED="1378632805344" TEXT="Pasos">
<node CREATED="1378632805345" ID="ID_167455192" MODIFIED="1379612094919" TEXT="Producir estimaciones de coste/beneficio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1378632898461" ID="ID_921636273" MODIFIED="1384951613381" TEXT="Aspectos tangibles e INTANGIBLES">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378632910261" ID="ID_55464099" MODIFIED="1383568822827" TEXT="Clasificaci&#xf3;n de costes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378632723468" ID="ID_800072529" MODIFIED="1379612099272" TEXT="Determinar la viabilidad del proyecto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1378632741594" ID="ID_558381571" MODIFIED="1383584844440" TEXT="ROI">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378632943578" ID="ID_1244930544" MODIFIED="1383584836671" TEXT="C&#xe1;lculo del coste y beneficio anual">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378632744485" ID="ID_1511730131" MODIFIED="1383584829417" TEXT="Valor actual">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Determinar el dinero que es viable invertir inicialmente para que se recupere la inversi&#243;n en un periodo de tiempo definido previamente (a&#241;o n).
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378633086738" ID="ID_1258874067" MODIFIED="1378633109945" TEXT="C&#xe1;lculo del beneficio neto cada a&#xf1;o"/>
<node CREATED="1378633128833" ID="ID_255982489" MODIFIED="1378633144432" TEXT="Se calcula el valor en el a&#xf1;o n">
<node CREATED="1378633034284" ID="ID_88293248" MODIFIED="1378633052386" TEXT="Depende del tipo de inter&#xe9;s (r)"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1370342724960" FOLDED="true" ID="ID_1393385171" LINK="../Ingenier&#xed;a%20del%20software/Dise&#xf1;o%20orientado%20a%20objetos.mm" MODIFIED="1384951641914" TEXT="Casos de Uso">
<arrowlink DESTINATION="ID_883937946" ENDARROW="Default" ENDINCLINATION="257;0;" ID="Arrow_ID_169058217" STARTARROW="None" STARTINCLINATION="257;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372843445197" ID="ID_1394689658" MODIFIED="1382195408649" TEXT="Actores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372843452742" ID="ID_1743907362" MODIFIED="1382195410138" TEXT="Casos de uso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372843478974" ID="ID_1361781703" MODIFIED="1382195410987" TEXT="Relaciones">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      En los apuntes hay dos formas m&#225;s de relaciones en los diagramas de Casos de uso, que no intervienen en METRICA: la generalizaci&#243;n y la inclusi&#243;n.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372852944598" ID="ID_115355875" MODIFIED="1372852949821" TEXT="&quot;Usa&quot;"/>
<node CREATED="1372852950283" ID="ID_1236401272" MODIFIED="1372852958926" TEXT="&quot;Extiende&quot;"/>
</node>
</node>
<node CREATED="1370342847866" FOLDED="true" ID="ID_1221469059" LINK="../Ingenier&#xed;a%20del%20software/Dise&#xf1;o%20orientado%20a%20objetos.mm" MODIFIED="1384951683161" TEXT="Diagrama de Clases. Clases y asociaciones">
<arrowlink DESTINATION="ID_883937946" ENDARROW="Default" ENDINCLINATION="258;0;" ID="Arrow_ID_1173614227" STARTARROW="None" STARTINCLINATION="258;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372852770137" ID="ID_1878746855" MODIFIED="1382195412711" TEXT="Clases">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372852774558" ID="ID_1274284904" MODIFIED="1372852778409" TEXT="Atributos"/>
<node CREATED="1372852779522" ID="ID_1810445923" MODIFIED="1372852782969" TEXT="Operaciones"/>
</node>
<node CREATED="1372852795588" FOLDED="true" ID="ID_1002204888" MODIFIED="1384951682350" TEXT="Relaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372852800476" ID="ID_824692250" MODIFIED="1372852803910" TEXT="Asociaci&#xf3;n"/>
<node CREATED="1372852804380" ID="ID_70628503" MODIFIED="1372852807186" TEXT="Herencia"/>
<node CREATED="1372852807838" ID="ID_1128671232" MODIFIED="1372852811600" TEXT="Agregaci&#xf3;n"/>
<node CREATED="1372852812045" ID="ID_1215357485" MODIFIED="1372852816887" TEXT="Composici&#xf3;n"/>
<node CREATED="1372852817341" ID="ID_783135534" MODIFIED="1372852820115" TEXT="Dependencia"/>
</node>
<node CREATED="1372852834644" ID="ID_1464016087" MODIFIED="1382195414486" TEXT="Interfaces">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372852838492" ID="ID_535888713" MODIFIED="1382195415423" TEXT="Paquetes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370342854457" ID="ID_1509405865" MODIFIED="1378223850460" TEXT="Diagrama de Componentes. Software, interfaces y dependencias.">
<arrowlink DESTINATION="ID_883937946" ENDARROW="Default" ENDINCLINATION="270;0;" ID="Arrow_ID_1899508080" STARTARROW="None" STARTINCLINATION="270;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370342861485" FOLDED="true" ID="ID_1747814761" MODIFIED="1384952245060" TEXT="Diagrama de Descomposici&#xf3;n. Expresan la Jerarqu&#xed;a de componentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372843691538" ID="ID_1042994939" MODIFIED="1372843704794" TEXT="Organizativo"/>
<node CREATED="1372843705204" ID="ID_881691603" MODIFIED="1372843709106" TEXT="Funcional"/>
<node CREATED="1372843709486" ID="ID_884730353" MODIFIED="1372843719701" TEXT="Di&#xe1;logos"/>
</node>
<node CREATED="1370342911273" FOLDED="true" ID="ID_579679276" LINK="../Ingenier&#xed;a%20del%20software/Dise&#xf1;o%20orientado%20a%20objetos.mm" MODIFIED="1384952256136" TEXT="Diagrama de Despliegue. Se introduce el HW">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370342954838" ID="ID_1878121709" MODIFIED="1370342969517" TEXT="Relaciones entre software y hardware"/>
<node CREATED="1372844034253" ID="ID_686879524" MODIFIED="1378223242053" TEXT="Disposici&#xf3;n de particiones f&#xed;sicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372843958592" ID="ID_904717328" MODIFIED="1378223245813" TEXT="En qu&#xe9; dispositivos est&#xe1; el SW">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370342980825" ID="ID_811794836" MODIFIED="1378223247857" TEXT="Nodos y conexiones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370342976637" FOLDED="true" ID="ID_1294077997" MODIFIED="1384952365151" TEXT="Diagrama de Estructura">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372851164065" ID="ID_1581597769" MODIFIED="1382195459991" TEXT="Forma de &#xe1;rbol">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372851195359" ID="ID_458949500" MODIFIED="1382195462862" TEXT="M&#xf3;dulo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372851202742" ID="ID_1647352206" MODIFIED="1382195463479" TEXT="Conexi&#xf3;n ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372851207043" ID="ID_245205477" MODIFIED="1382195463988" TEXT="Par&#xe1;metro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372851217959" ID="ID_877154221" MODIFIED="1382195464566" TEXT="M&#xf3;dulo predefinido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372851223801" ID="ID_877662614" MODIFIED="1382195465155" TEXT="Almac&#xe9;n de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372851228473" ID="ID_1235212606" MODIFIED="1382195465911" TEXT="Dispositivo f&#xed;sico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370343024123" ID="ID_1835110816" MODIFIED="1382195469879" TEXT="Estructura modular">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370349633275" ID="ID_301431917" MODIFIED="1382195466850" TEXT="M&#xf3;dulos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370349643474" ID="ID_665895202" MODIFIED="1382195467845" TEXT="Conexiones entre los m&#xf3;dulos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370349653626" ID="ID_1571658834" MODIFIED="1382195468833" TEXT="Comunicaciones entre los m&#xf3;dulos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372851128400" ID="ID_962672707" MODIFIED="1382195470839" TEXT="Estructuras">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372851139743" ID="ID_442571040" MODIFIED="1382195472184" TEXT="Secuencial">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372851144434" ID="ID_289599747" MODIFIED="1382195472960" TEXT="Repetitiva">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372851148549" ID="ID_1600280723" MODIFIED="1382195473570" TEXT="Alternativa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1370343038928" ID="ID_1660306204" LINK="../Ingenier&#xed;a%20del%20software/Diagrama%20de%20Flujo%20de%20Datos.mm" MODIFIED="1378223853596" TEXT="DFD">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370343051470" FOLDED="true" ID="ID_736105855" LINK="../Ingenier&#xed;a%20del%20software/Dise&#xf1;o%20orientado%20a%20objetos.mm" MODIFIED="1384951743456" TEXT="Diagramas de Interacci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370343083473" ID="ID_762254693" MODIFIED="1383568508869" TEXT="Diagrama de secuencia. Secuencia temporal de los mensajes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370343090376" ID="ID_554219925" MODIFIED="1383568511389" TEXT="Diagrama de colaboraci&#xf3;n. Objetos que se pasan mensajes">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Este diagrama ya no existe en UML 2.2
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370343106842" ID="ID_883937946" MODIFIED="1378223855265" TEXT="Diagrama de paquetes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370343184140" FOLDED="true" ID="ID_1233597691" MODIFIED="1384952426101" TEXT="Diagrama de Transici&#xf3;n de Estados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372844212184" ID="ID_679512734" MODIFIED="1383570081973" TEXT="Estados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372844214975" ID="ID_1254554504" MODIFIED="1383570083413" TEXT="Transiciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372844240220" ID="ID_786074665" MODIFIED="1383570084469" TEXT="S&#xf3;lo puede haber un estado inicial y ninguna transici&#xf3;n puede dirigirse a &#xe9;l">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372844265378" ID="ID_1237292582" MODIFIED="1383570085285" TEXT="Pueden existir varios estados finales (mutuamente excluyentes)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372844347804" ID="ID_1743343252" MODIFIED="1383570085973" TEXT="No pueden partir transiciones desde los estados finales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370343377526" FOLDED="true" ID="ID_1517980802" MODIFIED="1384952428582" TEXT="Modelado de procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370343611417" ID="ID_683940726" MODIFIED="1383569467986" TEXT="Proceso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370343624438" ID="ID_538803362" MODIFIED="1370343634047" TEXT="actividades + procedimientos"/>
<node CREATED="1376654881959" ID="ID_966260475" MODIFIED="1376654886052" TEXT="Tipos">
<node CREATED="1376654886053" ID="ID_83335189" MODIFIED="1376654890374" TEXT="Principales"/>
<node CREATED="1376654890663" ID="ID_1571792599" MODIFIED="1376654894671" TEXT="De soporte"/>
</node>
</node>
<node CREATED="1370343386194" ID="ID_915244901" MODIFIED="1383569464234" TEXT="SADT">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370343678326" ID="ID_806291881" MODIFIED="1370343695270" TEXT="Diagramas por nivel"/>
<node CREATED="1370343720345" ID="ID_1354406970" MODIFIED="1370343728591" TEXT="Flujos">
<node CREATED="1370343728591" ID="ID_1545364160" MODIFIED="1370343743489" TEXT="Entrada"/>
<node CREATED="1370343743807" ID="ID_1742145958" MODIFIED="1370343770438" TEXT="Salida"/>
<node CREATED="1370343750211" ID="ID_102380272" MODIFIED="1370343766694" TEXT="Control"/>
<node CREATED="1370343757473" ID="ID_742454970" MODIFIED="1370343760345" TEXT="Mecanismo"/>
</node>
</node>
</node>
<node CREATED="1370343814686" ID="ID_976303469" LINK="../Ingenier&#xed;a%20del%20software/Modelo%20ER.mm" MODIFIED="1378223857715" TEXT="Modelo E/R extendido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370343827002" FOLDED="true" ID="ID_1723651996" MODIFIED="1384952433465" TEXT="Normalizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376672533556" ID="ID_1353370142" MODIFIED="1382287104154" TEXT="La desnormalizaci&#xf3;n">
<node CREATED="1376672709661" ID="ID_454981021" MODIFIED="1376672736020" TEXT="Reduce y simplifica el acceso a la base de datos"/>
<node CREATED="1376672738743" ID="ID_1497151067" MODIFIED="1376672750555" TEXT="Optimiza el desempe&#xf1;o de la BD"/>
</node>
</node>
<node CREATED="1370343857555" ID="ID_1993837810" MODIFIED="1378223859634" TEXT="Optimizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370343863272" FOLDED="true" ID="ID_1673153720" MODIFIED="1384952442294" TEXT="Reglas de Obtenci&#xf3;n del Modelo F&#xed;sico a partir del l&#xf3;gico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376654999900" ID="ID_1719161842" MODIFIED="1383569704230" TEXT="Conservar la sem&#xe1;ntica del modelo l&#xf3;gico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370343885416" FOLDED="true" ID="ID_1305072586" MODIFIED="1384952443418" TEXT="Reglas de transformaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376655019925" ID="ID_553605349" MODIFIED="1379623579052" TEXT="Obtener un modelo f&#xed;sico de datos a partir del modelo de clases">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376655045551" ID="ID_508131668" MODIFIED="1379612602997" TEXT="Conservar la sem&#xe1;ntica del modelo de clases">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370343895299" ID="ID_1388940865" MODIFIED="1384952075110" TEXT="T&#xe9;cnicas Matriciales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376655112249" ID="ID_484493147" MODIFIED="1383569757962" TEXT="Representan las relaciones existentes entre las entidades">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376655132827" ID="ID_1481675487" MODIFIED="1383569760577" TEXT="Se utilizan para analizar la consistencia de los modelos ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370343921718" ID="ID_614017384" MODIFIED="1383997836646" TEXT="T&#xe9;cnicas de gesti&#xf3;n de Proyectos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372679865305" ID="ID_875050443" MODIFIED="1378223465312" TEXT="Staffing Size">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372850900056" ID="ID_673984962" MODIFIED="1372850928532" TEXT="N&#xfa;mero de clases clave"/>
<node CREATED="1372850911412" ID="ID_1155193263" MODIFIED="1372850923351" TEXT="N&#xfa;mero de clases secundarias"/>
<node CREATED="1372850930943" ID="ID_1910983342" MODIFIED="1372850951539" TEXT="Promedio entre clases clave y clases secundarias"/>
</node>
<node CREATED="1370343931288" ID="ID_1329963258" MODIFIED="1383569771929" TEXT="T&#xe9;cnicas de Estimaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370343979734" ID="ID_1605139744" MODIFIED="1378223876732" TEXT="Puntos de Funci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370343985843" ID="ID_53593363" MODIFIED="1382287118428" TEXT="M&#xe9;todo de Albrecht">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370343992791" ID="ID_148478035" MODIFIED="1378223463190" TEXT="M&#xe9;todo MARK II">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1370344027447" ID="ID_210677272" MODIFIED="1383569775737" TEXT="Planificaci&#xf3;n ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370344043025" ID="ID_270925295" MODIFIED="1378223467153" TEXT="PERT">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370344045262" ID="ID_39562748" MODIFIED="1378223469415" TEXT="Gantt">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372850809429" ID="ID_94043229" MODIFIED="1378223470367" TEXT="WBS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372850814591" ID="ID_1742036901" MODIFIED="1378223472114" TEXT="Diagrama de extrapolaci&#xf3;n: previsi&#xf3;n de las desviaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
