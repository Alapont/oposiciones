<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1370209640148" ID="ID_1379736155" MODIFIED="1378222106489" TEXT="M&#xe9;trica. DSI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370328481769" ID="ID_1388065786" MODIFIED="1372774795281" POSITION="right" TEXT="Arquitectura">
<cloud/>
<node BACKGROUND_COLOR="#66ff66" CREATED="1370209940517" FOLDED="true" ID="ID_873026721" MODIFIED="1372834716004" STYLE="bubble" TEXT="Definici&#xf3;n de la Arquitectura del Sistema">
<node CREATED="1370210271510" ID="ID_1402566839" MODIFIED="1370210282776" TEXT="Definici&#xf3;n de Niveles de Arquitectura"/>
<node CREATED="1370210283343" ID="ID_1370721777" MODIFIED="1370210304062" TEXT="Identificaci&#xf3;n de Requisitos de Dise&#xf1;o y Construcci&#xf3;n"/>
<node CREATED="1370210305347" ID="ID_1533244702" MODIFIED="1370210314163" TEXT="Especificaci&#xf3;n de Excepciones"/>
<node CREATED="1370210315151" ID="ID_486341406" MODIFIED="1370210344169" TEXT="Especificaci&#xf3;n de Est&#xe1;ndares y Normas de Dise&#xf1;o y Construcci&#xf3;n"/>
<node CREATED="1370210344456" ID="ID_1889134799" MODIFIED="1370210356283" TEXT="Identificaci&#xf3;n de Subsistemas de Dise&#xf1;o"/>
<node CREATED="1370210358239" ID="ID_320550930" MODIFIED="1370210372499" TEXT="Especificaci&#xf3;n del Entorno Tecnol&#xf3;gico"/>
<node CREATED="1370210373191" ID="ID_401917647" MODIFIED="1372775174510" TEXT="Especificaci&#xf3;n de Requisitos de Operaci&#xf3;n y Seguridad"/>
</node>
<node BACKGROUND_COLOR="#66ff66" CREATED="1370209981397" FOLDED="true" ID="ID_863659439" MODIFIED="1376648286006" STYLE="bubble" TEXT="Dise&#xf1;o de la Arquitectura de Soporte">
<node CREATED="1370210414992" ID="ID_1779893824" MODIFIED="1372774724361" TEXT="Dise&#xf1;o de Subsistemas de Soporte"/>
<node CREATED="1370210431036" ID="ID_562996356" MODIFIED="1372774724361" TEXT="Identificaci&#xf3;n de Mecanismos Gen&#xe9;ricos de Dise&#xf1;o"/>
</node>
<node BACKGROUND_COLOR="#9999ff" CREATED="1370210064529" ID="ID_1160782540" MODIFIED="1372775693731" STYLE="bubble" TEXT="Dise&#xf1;o de Clases"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1370210000936" ID="ID_1239046664" MODIFIED="1372775709534" STYLE="bubble" TEXT="Dise&#xf1;o de la Arquitectura de M&#xf3;dulos"/>
<node BACKGROUND_COLOR="#9999ff" CREATED="1370210045193" ID="ID_1332488142" MODIFIED="1372775693731" STYLE="bubble" TEXT="Dise&#xf1;o de Casos de Uso Reales"/>
<node BACKGROUND_COLOR="#66ff66" CREATED="1370210076426" ID="ID_846700591" MODIFIED="1372774763050" STYLE="bubble" TEXT="Dise&#xf1;o F&#xed;sico de Datos"/>
</node>
<node CREATED="1370210093842" ID="ID_1466694211" MODIFIED="1372834815263" POSITION="right" TEXT="Verificaci&#xf3;n y Aceptaci&#xf3;n de la Arquitectura del Sistema">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1370328511619" ID="ID_482718132" MODIFIED="1372774903673" POSITION="right" TEXT="Construcci&#xf3;n">
<cloud/>
<node CREATED="1370210113849" ID="ID_1863743032" MODIFIED="1372835478024" TEXT="Generaci&#xf3;n de Especificaciones de Construcci&#xf3;n">
<icon BUILTIN="gohome"/>
</node>
<node CREATED="1370210134698" ID="ID_351128852" MODIFIED="1372835502188" TEXT="Dise&#xf1;o de Migraci&#xf3;n y Carga Inicial de Datos">
<icon BUILTIN="launch"/>
</node>
<node CREATED="1370210149838" ID="ID_929256120" MODIFIED="1372835506109" TEXT="Especificaci&#xf3;n T&#xe9;cnica del Plan de Pruebas">
<icon BUILTIN="list"/>
</node>
<node CREATED="1370210161608" ID="ID_1356885476" MODIFIED="1372835386816" TEXT="Establecimiento de Requisitos de Implantaci&#xf3;n">
<icon BUILTIN="help"/>
</node>
</node>
<node CREATED="1370210175094" ID="ID_675824615" MODIFIED="1372834818074" POSITION="right" TEXT="Aprobaci&#xf3;n del Dise&#xf1;o de Sistema de Informaci&#xf3;n">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1370332544327" ID="ID_1914588130" MODIFIED="1370332548120" POSITION="left" TEXT="Productos">
<node CREATED="1370332690869" ID="ID_1673358288" MODIFIED="1380614569494" TEXT="Arquitectura del sistema">
<cloud/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370332712397" ID="ID_973374631" MODIFIED="1370332737827" TEXT="Estructurado">
<node BACKGROUND_COLOR="#ff9999" CREATED="1370332738831" ID="ID_726162822" MODIFIED="1370336597478" STYLE="bubble" TEXT="Arquitectura modular"/>
</node>
<node CREATED="1370332762746" ID="ID_159659450" MODIFIED="1370332769207" TEXT="Orientaci&#xf3;n a objetos">
<node BACKGROUND_COLOR="#9999ff" CREATED="1370332782878" ID="ID_1509374364" MODIFIED="1372775015526" STYLE="bubble" TEXT="Casos de uso"/>
<node BACKGROUND_COLOR="#9999ff" CREATED="1370332797940" ID="ID_1942305666" MODIFIED="1372775015526" STYLE="bubble" TEXT="Modelo de clases"/>
<node BACKGROUND_COLOR="#9999ff" CREATED="1370332806387" ID="ID_1182210802" MODIFIED="1372775015526" STYLE="bubble" TEXT="Comportamiento de clases"/>
</node>
<node BACKGROUND_COLOR="#66ff66" CREATED="1370332752146" ID="ID_1102794574" MODIFIED="1375261767232" STYLE="bubble" TEXT="Dise&#xf1;o de la Interfaz de usuario"/>
<node CREATED="1370332864373" ID="ID_1167323467" MODIFIED="1370339223494" TEXT="Cat&#xe1;logos">
<icon BUILTIN="list"/>
<node CREATED="1370332695372" ID="ID_296465274" MODIFIED="1372839574146" STYLE="bubble" TEXT="Especificaciones del Cat&#xe1;logo de requisitos">
<icon BUILTIN="edit"/>
</node>
<node CREATED="1370332876814" ID="ID_51747506" MODIFIED="1372834859226" STYLE="bubble" TEXT="Cat&#xe1;logo de excepciones"/>
<node CREATED="1370332885838" ID="ID_365747607" MODIFIED="1372834859226" STYLE="bubble" TEXT="Cat&#xe1;logo de normas"/>
</node>
<node CREATED="1370333035710" ID="ID_343634804" MODIFIED="1372834859226" STYLE="bubble" TEXT="Operaci&#xf3;n y administraci&#xf3;n del sistema (Especificaciones)">
<icon BUILTIN="edit"/>
</node>
<node CREATED="1370333052283" ID="ID_976398143" MODIFIED="1372834859226" STYLE="bubble" TEXT="Seguridad y control de acceso (Especificaciones)">
<icon BUILTIN="edit"/>
</node>
<node CREATED="1372840353676" ID="ID_1792941764" MODIFIED="1372840388596" TEXT="Los MANUALES DE USUARIO SE ELABORAN EN CSI">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1370333195388" ID="ID_1745238723" MODIFIED="1370335195974" TEXT="Para CSI">
<node CREATED="1370333393579" ID="ID_1913679321" MODIFIED="1372841682738" STYLE="bubble" TEXT="Especificaciones de construcci&#xf3;n">
<icon BUILTIN="gohome"/>
<icon BUILTIN="down"/>
</node>
<node CREATED="1372841647737" ID="ID_1039722417" MODIFIED="1372841775378" TEXT="La PREPARACI&#xd3;N DEL ENTORNO DE CONSTRUCCION SE HACE EN CSI">
<icon BUILTIN="yes"/>
</node>
<node CREATED="1370334966612" ID="ID_197086597" MODIFIED="1372839539266" STYLE="bubble" TEXT="Especificaci&#xf3;n de detalle de Plan de Pruebas">
<icon BUILTIN="edit"/>
<icon BUILTIN="list"/>
</node>
<node CREATED="1370334769217" ID="ID_1419878938" MODIFIED="1370335202511" TEXT="Para IAS">
<node CREATED="1370334919461" ID="ID_190215564" MODIFIED="1372841780940" STYLE="bubble" TEXT="Especificaciones  de Migraci&#xf3;n y Carga Inicial de Datos">
<icon BUILTIN="help"/>
<icon BUILTIN="edit"/>
<icon BUILTIN="down"/>
</node>
<node CREATED="1372841748004" ID="ID_1462648438" MODIFIED="1372841777870" TEXT="Los PROCEDIMIENTOS se hacen en CSI">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
<node CREATED="1372774525595" ID="ID_457768187" MODIFIED="1372840452613" POSITION="left" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="DSI.png" />
  </body>
</html></richcontent>
</node>
</node>
</map>
