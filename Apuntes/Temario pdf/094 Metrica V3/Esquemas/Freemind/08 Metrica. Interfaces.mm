<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1377853072392" ID="ID_335978542" MODIFIED="1378222949549" TEXT="Metrica. Interfaces">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370341339380" ID="ID_1136568676" MODIFIED="1378222994930" POSITION="right" TEXT="Interfaces">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370341343446" ID="ID_685963169" MODIFIED="1384950947198" TEXT="Gesti&#xf3;n de Proyectos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372842502127" ID="ID_864368045" MODIFIED="1398079505695" TEXT="Acitividades de inicio de proyectos (GPI)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372842598974" ID="ID_641595124" MODIFIED="1383566687447" TEXT="Estimar el esfuerzo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372842606313" ID="ID_365622220" MODIFIED="1383566685400" TEXT="Realizar la planificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372842695359" ID="ID_1803523994" MODIFIED="1378223033120" TEXT="Entre EVS y ASI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372842516489" ID="ID_1505272100" MODIFIED="1398079510287" TEXT="Actividades de seguimiento y control (GPS)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372842627858" ID="ID_1909910019" MODIFIED="1383566696864" TEXT="Asignaci&#xf3;n de tareas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372842642425" ID="ID_1983487786" MODIFIED="1383566698816" TEXT="Gesti&#xf3;n de incidencias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372842669294" ID="ID_1221900515" MODIFIED="1383566700656" TEXT="Cambios en los requisitos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372842679370" ID="ID_548435374" MODIFIED="1378223030811" TEXT="Desde ASI a MSI (inclusive)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372842532503" ID="ID_76839321" MODIFIED="1378222980843" TEXT="Actividades de finalizaci&#xf3;n de proyectos (GPF)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376653614669" ID="ID_1585439528" MODIFIED="1376653675355">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="GP.png" />
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1370341353374" ID="ID_644447228" MODIFIED="1378222953168" TEXT="Gesti&#xf3;n de la Configuraci&#xf3;n (GC)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370342102490" ID="ID_1182569169" MODIFIED="1378222966350" TEXT="Todas las fases">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372842791961" ID="ID_1853083627" MODIFIED="1382194988298" TEXT="Control de los cambios, versiones, etc.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372842819230" ID="ID_1543548379" MODIFIED="1382194989389" TEXT="Garantiza que no hay cambios incontrolados y que la versi&#xf3;n es la correcta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370341362055" ID="ID_1130268694" MODIFIED="1378222955711" TEXT="Aseguramiento de la Calidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370342204073" ID="ID_875053711" MODIFIED="1382194990595" TEXT="Grupo de Aseguramiento de la Calidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370342298825" ID="ID_267605719" MODIFIED="1378222962716" TEXT="Todas las fases">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377769360583" ID="ID_420209695" MODIFIED="1377864638709" TEXT="Comienza en el estudio de Viabilidad del Sistema">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370341392686" ID="ID_862948542" MODIFIED="1378222957817" TEXT="Seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370342075441" ID="ID_495293893" MODIFIED="1378222960063" TEXT="Todas las fases">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372842853557" ID="ID_546002778" MODIFIED="1372842856554" TEXT="MAGERIT"/>
</node>
</node>
</node>
</map>
