<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1370240954356" ID="ID_1269718588" MODIFIED="1378222218908" TEXT="Metrica. IAS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372841346836" FOLDED="true" ID="ID_1400991186" MODIFIED="1376650653539" POSITION="right" TEXT="Implantaci&#xf3;n">
<icon BUILTIN="launch"/>
<node CREATED="1370240991257" ID="ID_1694085962" MODIFIED="1372841499315" TEXT="Establecimiento del Plan de Implantaci&#xf3;n">
<node CREATED="1370241023221" ID="ID_1195487292" MODIFIED="1370241039447" TEXT="Definici&#xf3;n del Plan de Implantaci&#xf3;n"/>
<node CREATED="1370241039827" ID="ID_892744323" MODIFIED="1370241052730" TEXT="Especificaci&#xf3;n del Equipo de Implantaci&#xf3;n"/>
</node>
<node CREATED="1370241018018" ID="ID_1667076203" MODIFIED="1372840956934" TEXT="Formaci&#xf3;n necesaria para la implantaci&#xf3;n">
<node CREATED="1370241103624" ID="ID_1917414044" MODIFIED="1370241121831" TEXT="Preparaci&#xf3;n de la Formaci&#xf3;n del Equipo de Implantaci&#xf3;n"/>
<node CREATED="1370241122071" ID="ID_660985366" MODIFIED="1370241130855" TEXT="Formaci&#xf3;n del Equipo de Implantaci&#xf3;n"/>
<node CREATED="1370241131111" ID="ID_967714263" MODIFIED="1370241151228" TEXT="Preparaci&#xf3;n de la Formaci&#xf3;n a Usuarios Finales"/>
<node CREATED="1370241151998" ID="ID_431868277" MODIFIED="1370241167647" TEXT="Seguimiento de la Formaci&#xf3;n a Usuarios Finales"/>
</node>
</node>
<node CREATED="1372841370732" FOLDED="true" ID="ID_706197235" MODIFIED="1376650652691" POSITION="right" TEXT="Operaci&#xf3;n">
<icon BUILTIN="group"/>
<node CREATED="1370241172317" ID="ID_1118747241" MODIFIED="1372841491333" TEXT="Incorporaci&#xf3;n del sistema al entorno de operaci&#xf3;n">
<node CREATED="1370241236199" ID="ID_461357612" MODIFIED="1370241243018" TEXT="Preparaci&#xf3;n de la instalaci&#xf3;n"/>
<node CREATED="1370241243898" ID="ID_1076301966" MODIFIED="1370241258236" TEXT="Realizaci&#xf3;n de la instalaci&#xf3;n"/>
</node>
<node CREATED="1370241187862" ID="ID_1675551478" MODIFIED="1372840795194" TEXT="Carga de datos al entorno de operaci&#xf3;n">
<icon BUILTIN="info"/>
<node CREATED="1370241262766" ID="ID_1846556979" MODIFIED="1370241277916" TEXT="Migraci&#xf3;n y Carga Inicial de Datos"/>
<node CREATED="1372840742057" ID="ID_531048413" MODIFIED="1372841603877" TEXT="Se hizo el Plan de Migraci&#xf3;n y Carga inicial de Datos en ASI">
<icon BUILTIN="yes"/>
<icon BUILTIN="down"/>
</node>
<node CREATED="1372840814482" ID="ID_1516566654" MODIFIED="1372841607068" TEXT="Las especificaciones se hicieron en DSI">
<icon BUILTIN="yes"/>
<icon BUILTIN="down"/>
</node>
<node CREATED="1372841574989" ID="ID_553367764" MODIFIED="1372841599175" TEXT="Los componentes y procedimientos se hicieron en CSI">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1372841388357" FOLDED="true" ID="ID_1270997032" MODIFIED="1376650650979" POSITION="right" TEXT="Pruebas">
<icon BUILTIN="button_ok"/>
<node CREATED="1370241197854" FOLDED="true" ID="ID_682592348" MODIFIED="1372840724605" TEXT="Pruebas de implantaci&#xf3;n del sistema">
<icon BUILTIN="button_ok"/>
<node CREATED="1370241282087" ID="ID_954248514" MODIFIED="1370241298047" TEXT="Preparaci&#xf3;n de las Pruebas de Implantaci&#xf3;n"/>
<node CREATED="1370241298412" ID="ID_485969095" MODIFIED="1370241313905" TEXT="Realizaci&#xf3;n de las Pruebas de Implantaci&#xf3;n"/>
<node CREATED="1370241314191" ID="ID_1953126956" MODIFIED="1370241336751" TEXT="Evaluaci&#xf3;n del resultado de las Pruebas de Implantaci&#xf3;n"/>
</node>
<node CREATED="1370241197854" FOLDED="true" ID="ID_170824427" MODIFIED="1372801038673" TEXT="Pruebas de aceptaci&#xf3;n del sistema">
<icon BUILTIN="button_ok"/>
<node CREATED="1370241282087" ID="ID_227396579" MODIFIED="1370241403621" TEXT="Preparaci&#xf3;n de las Pruebas de Aceptaci&#xf3;n"/>
<node CREATED="1370241298412" ID="ID_257980686" MODIFIED="1370241411389" TEXT="Realizaci&#xf3;n de las Pruebas de Aceptaci&#xf3;n"/>
<node CREATED="1370241314191" ID="ID_1704077104" MODIFIED="1370241419954" TEXT="Evaluaci&#xf3;n del resultado de las Pruebas de Aceptaci&#xf3;n"/>
</node>
</node>
<node CREATED="1372841430251" FOLDED="true" ID="ID_201086245" MODIFIED="1376650651546" POSITION="right" TEXT="Mantenimiento">
<icon BUILTIN="kaddressbook"/>
<node CREATED="1370241425825" FOLDED="true" ID="ID_906396126" MODIFIED="1372776032926" TEXT="Preparaci&#xf3;n del mantenimiento del sistema">
<node CREATED="1370241492999" ID="ID_1252832584" MODIFIED="1370241514451" TEXT="Establecimiento de la Infraestructura para el Mantenimiento"/>
<node CREATED="1370241514722" ID="ID_1336159271" MODIFIED="1370241536814" TEXT="Formalizaci&#xf3;n del Plan de Mantenimiento"/>
</node>
<node CREATED="1370241444038" FOLDED="true" ID="ID_1077935913" MODIFIED="1370338448279" TEXT="Establecimiento del acuerdo de nivel de servicio">
<node CREATED="1370241541047" ID="ID_1711077099" MODIFIED="1370241550581" TEXT="Identificaci&#xf3;n de los Servicios"/>
<node CREATED="1370241550961" ID="ID_1336881182" MODIFIED="1370241569496" TEXT="Descripci&#xf3;n de las Propiedades de cada Servicio"/>
<node CREATED="1370241569938" ID="ID_498441298" MODIFIED="1370241590095" TEXT="Determinaci&#xf3;n del SLA"/>
</node>
</node>
<node CREATED="1370241454685" FOLDED="true" ID="ID_385394464" MODIFIED="1372840778999" POSITION="right" TEXT="Presentaci&#xf3;n y aprobaci&#xf3;n del sistema">
<icon BUILTIN="ksmiletris"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1370241594220" ID="ID_214361172" MODIFIED="1370241611429" TEXT="Convocatoria de la presentaci&#xf3;n del Sistema"/>
<node CREATED="1370241611731" ID="ID_1522724284" MODIFIED="1370241617754" TEXT="Aprobaci&#xf3;n del Sistema"/>
</node>
<node CREATED="1370241468093" FOLDED="true" ID="ID_501952642" MODIFIED="1372801106562" POSITION="right" TEXT="Paso a producci&#xf3;n">
<icon BUILTIN="launch"/>
<node CREATED="1370241660647" ID="ID_1424959449" MODIFIED="1370241676514" TEXT="Preparaci&#xf3;n del Entorno de Producci&#xf3;n"/>
<node CREATED="1370241676769" ID="ID_1287711711" MODIFIED="1370241688206" TEXT="Activaci&#xf3;n del Sistema en Producci&#xf3;n"/>
</node>
<node CREATED="1370337996647" ID="ID_1504530872" MODIFIED="1370338404279" POSITION="left" TEXT="Productos">
<node CREATED="1370338987509" ID="ID_1521077214" MODIFIED="1370339020255" TEXT="De IAS">
<cloud/>
<node CREATED="1370338010899" ID="ID_1917396984" MODIFIED="1372840609949" STYLE="bubble" TEXT="Plan de Implantaci&#xf3;n">
<icon BUILTIN="list"/>
</node>
<node CREATED="1370338068641" ID="ID_985350492" MODIFIED="1372840613828" STYLE="bubble" TEXT="Plan de Formaci&#xf3;n">
<icon BUILTIN="list"/>
<node CREATED="1370338084795" ID="ID_861217156" MODIFIED="1372840613828" TEXT="Implantaci&#xf3;n"/>
<node CREATED="1370338107391" ID="ID_844731327" MODIFIED="1372840613828" TEXT="Usuarios Finales">
<icon BUILTIN="male2"/>
</node>
</node>
<node CREATED="1370338135729" ID="ID_1971984114" MODIFIED="1370338905017" TEXT="Instalaci&#xf3;n">
<node CREATED="1370338140996" ID="ID_1840361768" MODIFIED="1372840644889" STYLE="bubble" TEXT="Producto Software"/>
<node CREATED="1370338153528" ID="ID_427521139" MODIFIED="1372840622442" STYLE="bubble" TEXT="Componentes de Migraci&#xf3;n y Carga Inicial de Datos"/>
<node CREATED="1370338188278" ID="ID_1496671787" MODIFIED="1372840649121" STYLE="bubble" TEXT="Base de datos/ficheros cargados"/>
</node>
<node CREATED="1370338926911" ID="ID_558715998" MODIFIED="1370338929814" TEXT="Pruebas">
<node CREATED="1370338235725" ID="ID_1038565224" MODIFIED="1372840656121" STYLE="bubble" TEXT="Resultados de Pruebas de implantaci&#xf3;n (entorno de operaci&#xf3;n)">
<icon BUILTIN="full-4"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1370338292158" ID="ID_1565751622" MODIFIED="1372840661564" STYLE="bubble" TEXT="Resultados de Pruebas de aceptaci&#xf3;n">
<icon BUILTIN="full-5"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1370338409792" ID="ID_1326971150" MODIFIED="1370338413460" TEXT="Para MSI">
<node CREATED="1370338314162" ID="ID_651018128" MODIFIED="1372840679909" STYLE="bubble" TEXT="Plan de Mantenimiento">
<icon BUILTIN="list"/>
</node>
<node CREATED="1370338342889" ID="ID_1782820464" MODIFIED="1372840686604" STYLE="bubble" TEXT="SLAs"/>
</node>
</node>
<node CREATED="1372776107005" ID="ID_977987366" MODIFIED="1372840668548" POSITION="left" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="IAS.png" />
  </body>
</html></richcontent>
</node>
</node>
</map>
