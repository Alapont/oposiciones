<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1370162915253" ID="ID_1573355009" MODIFIED="1381052283613" TEXT="Metrica. PSI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370162936976" ID="ID_1316564511" MODIFIED="1374735357879" POSITION="right" TEXT="Inicio del PSI">
<icon BUILTIN="full-1"/>
<icon BUILTIN="idea"/>
<node CREATED="1370163000580" ID="ID_194467712" MODIFIED="1370163015640" TEXT="An&#xe1;lisis de la necesidad del PSI"/>
<node CREATED="1370163016034" ID="ID_420538152" MODIFIED="1370163034318" TEXT="Identificaci&#xf3;n del alcance del PSI"/>
<node CREATED="1370163034920" ID="ID_1854831624" MODIFIED="1370163051254" TEXT="Determinaci&#xf3;n de responsables"/>
</node>
<node CREATED="1370163055375" ID="ID_1890872956" MODIFIED="1374735361186" POSITION="right" TEXT="Definici&#xf3;n y organizaci&#xf3;n del PSI">
<icon BUILTIN="full-2"/>
<icon BUILTIN="group"/>
<node CREATED="1370163098622" ID="ID_21815573" MODIFIED="1370163123364" TEXT="Especificaci&#xf3;n del Ambito y Alcance"/>
<node CREATED="1370163126281" ID="ID_1316684812" MODIFIED="1370163138286" TEXT="Organizaci&#xf3;n del PSI"/>
<node CREATED="1370163138726" ID="ID_823273915" MODIFIED="1370163150488" TEXT="Definici&#xf3;n del Plan de Trabajo"/>
<node CREATED="1370163150870" ID="ID_1056770449" MODIFIED="1370163164521" TEXT="Comunicaci&#xf3;n del Plan de Trabajo"/>
</node>
<node CREATED="1370163068582" ID="ID_1109499350" MODIFIED="1374735366085" POSITION="right" TEXT="Estudio de la informaci&#xf3;n relevante">
<icon BUILTIN="full-3"/>
<icon BUILTIN="yes"/>
<node CREATED="1370163170616" ID="ID_43909247" MODIFIED="1370163186415" TEXT="Selecci&#xf3;n y An&#xe1;lisis de antecedentes"/>
<node CREATED="1370163186706" ID="ID_1993163556" MODIFIED="1372766022581" TEXT="Valoraci&#xf3;n de antecedentes"/>
</node>
<node CREATED="1370163087440" ID="ID_1037725731" MODIFIED="1375111246688" POSITION="right" TEXT="IDENTIFICACION de requisitos">
<icon BUILTIN="full-4"/>
<icon BUILTIN="help"/>
<node CREATED="1370163203269" ID="ID_425687559" MODIFIED="1370163211305" TEXT="Estudio de los Procesos del PSI"/>
<node CREATED="1370163211638" ID="ID_1533656557" MODIFIED="1370163230226" TEXT="An&#xe1;lisis de las Necesidades de la Informaci&#xf3;n"/>
<node CREATED="1370163230598" ID="ID_196829432" MODIFIED="1370163238664" TEXT="Catalogaci&#xf3;n de Requisitos"/>
</node>
<node CREATED="1370163248211" ID="ID_1561510997" MODIFIED="1374735376381" POSITION="right" TEXT="Estudio de los Sistemas de Informacion Actuales">
<icon BUILTIN="full-5"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1370163271390" ID="ID_687720559" MODIFIED="1370163309700" TEXT="Alcance y Objetivos del Estudio de SI"/>
<node CREATED="1370206733599" ID="ID_695381892" MODIFIED="1370206745533" TEXT="An&#xe1;lisis de los Sistemas de Informaci&#xf3;n Actuales"/>
<node CREATED="1370206747458" ID="ID_1333617468" MODIFIED="1370206761983" TEXT="Valoraci&#xf3;n de los Sistemas de Informaci&#xf3;n Actuales"/>
</node>
<node CREATED="1370206767870" ID="ID_1267125163" MODIFIED="1375111264116" POSITION="right" TEXT="DISE&#xd1;O del Modelo de Sistemas de Informaci&#xf3;n">
<icon BUILTIN="full-6"/>
<icon BUILTIN="edit"/>
<node CREATED="1370206789830" ID="ID_53251627" MODIFIED="1370206803652" TEXT="Diagn&#xf3;stico de la Situaci&#xf3;n Actual"/>
<node CREATED="1370206804017" ID="ID_485914654" MODIFIED="1370206819526" TEXT="Definici&#xf3;n del Modelo de Sistemas de Informaci&#xf3;n"/>
</node>
<node CREATED="1370206823963" ID="ID_859291768" MODIFIED="1375111279864" POSITION="right" TEXT="DEFINICION de la Arquitectura Tecnol&#xf3;gica">
<icon BUILTIN="full-7"/>
<icon BUILTIN="gohome"/>
<icon BUILTIN="info"/>
<node CREATED="1370206843520" ID="ID_233633869" MODIFIED="1370206873693" TEXT="Identificaci&#xf3;n de las necesidades de Infraestructura tecnol&#xf3;gica"/>
<node CREATED="1370206873980" ID="ID_604056598" MODIFIED="1370206892764" TEXT="Selecci&#xf3;n de la Arquitectura Tecnol&#xf3;gica"/>
<node CREATED="1372862562363" ID="ID_1577258822" MODIFIED="1372862588791" TEXT="Ojo: No es la arquitectura del sistema, sino el entorno."/>
</node>
<node CREATED="1370206895079" ID="ID_74480176" MODIFIED="1374735555797" POSITION="right" TEXT="Definici&#xf3;n del Plan de Acci&#xf3;n">
<icon BUILTIN="full-8"/>
<icon BUILTIN="list"/>
<node CREATED="1370206920484" ID="ID_681441223" MODIFIED="1370206937068" TEXT="Definici&#xf3;n de Proyectos a Realizar"/>
<node CREATED="1370206937636" ID="ID_947669538" MODIFIED="1370206958261" TEXT="Elaboraci&#xf3;n del Plan de Mantenimiento del PSI"/>
</node>
<node CREATED="1370206972120" ID="ID_39363577" MODIFIED="1374735562302" POSITION="right" TEXT="Revisi&#xf3;n y aprobaci&#xf3;n del PSI">
<icon BUILTIN="full-9"/>
<icon BUILTIN="group"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1370207003220" ID="ID_1123873909" MODIFIED="1370207013423" TEXT="Convocatoria de la Presentaci&#xf3;n"/>
<node CREATED="1370207014084" ID="ID_1511706671" MODIFIED="1370207022900" TEXT="Evaluaci&#xf3;n y Mejora de la Propuesta"/>
<node CREATED="1370207023186" ID="ID_1622117099" MODIFIED="1370207028820" TEXT="Aprobaci&#xf3;n del PSI"/>
</node>
<node CREATED="1372832881992" ID="ID_848505830" MODIFIED="1372832924628" POSITION="left">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="PSI.png" />
  </body>
</html></richcontent>
</node>
</node>
</map>
