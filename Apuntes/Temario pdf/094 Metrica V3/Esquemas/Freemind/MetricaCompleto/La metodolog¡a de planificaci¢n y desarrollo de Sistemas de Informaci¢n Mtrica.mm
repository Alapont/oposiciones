<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1425593611669" ID="ID_590997041" MODIFIED="1524173768085" STYLE="bubble" TEXT="Tema 86. La metodolog&#xed;a de planificaci&#xf3;n y desarrollo de Sistemas de Informaci&#xf3;n M&#xe9;trica">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1425594005524" ID="ID_604916479" MODIFIED="1524173776488" POSITION="right" TEXT="Introducci&#xf3;n">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1425594096294" FOLDED="true" ID="ID_1983355530" MODIFIED="1524173767996" TEXT="Definicion de Metodologia">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425594128406" ID="ID_1785069430" MODIFIED="1425594129656" TEXT="el conjunto de m&#xe9;todos y reglas que sirven de gu&#xed;a para realizar los trabajos propios de desarrollo y que obligan a los miembros del equipo de proyecto a realizar ciertas comprobaciones sistem&#xe1;ticas de manera que el resultado final no presente incoherencias y est&#xe9; dirigido a un objetivo claro y prefijado">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1425594146808" ID="ID_1163978713" MODIFIED="1524173793016" TEXT="Diferencias entre MCV y Metodolog&#xed;a">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425594169962" ID="ID_874434433" MODIFIED="1524173832254" TEXT="Se define MCV de un SI como el conjunto de fases por las que pasa el sistema desde que se concibe hasta que se retira del servicio">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425594185901" ID="ID_1266711785" MODIFIED="1425594187171" TEXT="El MCV indica cu&#xe1;les son las actividades a realizar y el orden en que se van a realizar. "/>
<node COLOR="#111111" CREATED="1425594201203" ID="ID_490119305" MODIFIED="1425594202293" TEXT="El MCV no indica c&#xf3;mo se realizan las actividades y mucho menos qui&#xe9;n las tiene que realizar."/>
</node>
<node COLOR="#990000" CREATED="1425594230167" ID="ID_1780072599" MODIFIED="1524173853661" TEXT="La metodolog&#xed;a indica c&#xf3;mo se realizan las actividades y qui&#xe9;n las tiene que realizar.">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425594287254" ID="ID_875128319" MODIFIED="1425594289564" TEXT="Indica el procedimiento de trabajo para avanzar en la construcci&#xf3;n del sistema pero el orden en que se realizan las actividades lo indica el MCV"/>
</node>
<node COLOR="#990000" CREATED="1425594313987" ID="ID_1844478458" MODIFIED="1425594315187" TEXT="Frecuentemente, las metodolog&#xed;as presentan un MCV de desarrollo del sistema">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425594329079" FOLDED="true" ID="ID_1643046549" MODIFIED="1524173768000" TEXT="La metodolog&#xed;a es neutra con respecto al MCV">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425594346603" ID="ID_1961294399" MODIFIED="1425594347893" TEXT="cualquier metodolog&#xed;a vale para cualquier ciclo de vida dado que ese ciclo de vida proponga actividades que tenga sentido resolverse con la metodolog&#xed;a dada"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1425649706334" ID="ID_652250777" MODIFIED="1524173783619" TEXT="Niveles de planificaci&#xf3;n de los sistemas">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425649736643" ID="ID_1955058075" MODIFIED="1524173859532" TEXT="1. Estrat&#xe9;gico: ">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425649742218" ID="ID_496960988" MODIFIED="1425649780304" TEXT="Es la parte del sistema que fija los objetivos y metas a largo plazo. ">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1425649774834" ID="ID_155292167" MODIFIED="1524173868140" TEXT=" Tambi&#xe9;n se define la planificaci&#xf3;n estrat&#xe9;gica como ">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1425649786016" ID="ID_679533731" MODIFIED="1425649786019" TEXT="el conjunto de actividades mediante las cuales una organizaci&#xf3;n define los par&#xe1;metros b&#xe1;sicos a utilizar para conseguir los objetivos fijados dentro de un marco de relaci&#xf3;n con el exterior.">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1425649804232" ID="ID_1301864898" MODIFIED="1524173934736" TEXT="2. T&#xe1;ctico: ">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425649814087" ID="ID_201712297" MODIFIED="1425649824888" TEXT="Es la parte del sistema que realiza los planes operativos a partir de los planes estrat&#xe9;gicos. ">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1425649824890" ID="ID_1595746271" MODIFIED="1425649839548" TEXT="Se establece la funcionalidad del sistema. ">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1425649839549" ID="ID_1878458834" MODIFIED="1425649839552" TEXT="Planificaci&#xf3;n de sistemas (de Informaci&#xf3;n y Comunicaciones). ">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1425649810226" ID="ID_360329589" MODIFIED="1524173947766" TEXT="3. Operativo: ">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425649846960" ID="ID_237804183" MODIFIED="1425649852698" TEXT="parte del sistema que ejecuta los planes recibidos de los dos niveles superiores.">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1425649851308" ID="ID_468799351" MODIFIED="1425649853922" TEXT=" Planificaci&#xf3;n de proyectos.">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1425594063139" ID="ID_1730595378" MODIFIED="1524388283396" TEXT="Visi&#xf3;n general de M&#xe9;trica">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425594499232" ID="ID_102070908" MODIFIED="1524388288288" TEXT="M&#xe9;trica establece">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425594511843" ID="ID_1975568893" MODIFIED="1425594513253" TEXT="Un conjunto de pasos a realizar"/>
<node COLOR="#111111" CREATED="1425594523554" ID="ID_559508518" MODIFIED="1425594525014" TEXT="Un conjunto de productos a obtener"/>
<node COLOR="#111111" CREATED="1425594538916" ID="ID_1398278146" MODIFIED="1425594540256" TEXT="T&#xe9;cnicas y Pr&#xe1;cticas"/>
<node COLOR="#111111" CREATED="1425594558004" ID="ID_563928743" MODIFIED="1425594559854" TEXT="Participantes"/>
</node>
<node COLOR="#990000" CREATED="1425594597393" ID="ID_1201169492" MODIFIED="1524388300915" TEXT="Objetivos">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425594623534" ID="ID_938496291" MODIFIED="1425594625204" TEXT="Proporcionar o definir Sistemas de Informaci&#xf3;n que ayuden a conseguir los fines de la Organizaci&#xf3;n mediante la definici&#xf3;n de un marco estrat&#xe9;gico para el desarrollo de los mismos"/>
<node COLOR="#111111" CREATED="1425594638466" ID="ID_775723174" MODIFIED="1425594639837" TEXT="Dotar a la Organizaci&#xf3;n de productos software que satisfagan las necesidades de los usuarios dando una mayor importancia al an&#xe1;lisis de requisitos"/>
<node COLOR="#111111" CREATED="1425594656139" ID="ID_1732762948" MODIFIED="1425594657680" TEXT="Mejorar la productividad de los departamentos de Sistemas y Tecnolog&#xed;as de la Informaci&#xf3;n y las Comunicaciones, permitiendo una mayor capacidad de adaptaci&#xf3;n a los cambios y teniendo en cuenta la reutilizaci&#xf3;n en la medida de lo posible"/>
<node COLOR="#111111" CREATED="1425594670327" ID="ID_1274473947" MODIFIED="1425594671917" TEXT="Facilitar la comunicaci&#xf3;n y entendimiento entre los distintos participantes en la producci&#xf3;n de software a lo largo del ciclo de vida del proyecto"/>
<node COLOR="#111111" CREATED="1425594678698" ID="ID_681303326" MODIFIED="1425594680118" TEXT="Facilitar la operaci&#xf3;n, mantenimiento y uso de los productos software obtenidos"/>
<node COLOR="#111111" CREATED="1425594698531" ID="ID_780992044" MODIFIED="1425594700591" TEXT="Establecer un conjunto de tareas a realizar, t&#xe9;cnicas y productos a obtener para desarrollar sistemas de informaci&#xf3;n con una mayor calidad, productividad y satisfacci&#xf3;n de los usuarios y para facilitar su mantenimiento posterior"/>
</node>
<node COLOR="#990000" CREATED="1425594710078" ID="ID_60160761" MODIFIED="1524388302611" TEXT="&#xc1;mbito de aplicaci&#xf3;n">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425594727761" ID="ID_428202136" MODIFIED="1425594727761" TEXT="&#xf0b7; Administraci&#xf3;n Central del Estado (1&#xaa; Etapa)"/>
<node COLOR="#111111" CREATED="1425594727771" ID="ID_1353227479" MODIFIED="1425594727771" TEXT="&#xf0b7; Administraci&#xf3;n Auton&#xf3;mica."/>
<node COLOR="#111111" CREATED="1425594727771" ID="ID_815723748" MODIFIED="1425594727771" TEXT="&#xf0b7; Administraci&#xf3;n Local."/>
<node COLOR="#111111" CREATED="1425594727781" ID="ID_1049306019" MODIFIED="1425594727781" TEXT="&#xf0b7; Resto de empresas e instituciones"/>
<node COLOR="#111111" CREATED="1425594764626" ID="ID_610419471" MODIFIED="1425594766256" TEXT="M&#xe9;trica es una metodolog&#xed;a p&#xfa;blica que puede ser tambi&#xe9;n usada por las empresas privadas"/>
</node>
<node COLOR="#990000" CREATED="1425595658577" ID="ID_1448277431" MODIFIED="1524388308668" TEXT="Versiones">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425595665769" ID="ID_1537907872" MODIFIED="1425595675497">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Fotos%2086/Astic%204.png" />
  </body>
</html></richcontent>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1425595786558" ID="ID_1379192128" MODIFIED="1524388313424" TEXT="Breve rese&#xf1;a M2">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425595806010" ID="ID_408429329" MODIFIED="1425595807290" TEXT="A&#xf1;o 1993 y correcci&#xf3;n en 1995. Vigente hasta 2001 (M3)">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425595816982" ID="ID_1640693916" MODIFIED="1425595818562" TEXT="Metodolog&#xed;a para planificaci&#xf3;n y desarrollo de sistemas (no incluye mantenimiento).">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425595827683" ID="ID_1163654524" MODIFIED="1425595829233" TEXT="S&#xf3;lo contempla t&#xe9;cnicas de desarrollo estructurado de sistemas. No OO.">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425595846533" ID="ID_30904394" MODIFIED="1425595848033" TEXT="Incompleta porque se centra en actividades de desarrollo pero no toca Gesti&#xf3;n, Control y Operaci&#xf3;n. La calidad la deja a PGGC y la seguridad a Magerit">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425595863417" ID="ID_517069417" MODIFIED="1425595865117" TEXT="Da una gu&#xed;a de 9 t&#xe9;cnicas">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1425595879590" ID="ID_1256420091" MODIFIED="1524388381590" TEXT="Breve rese&#xf1;a M3">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425595891222" ID="ID_1839862853" MODIFIED="1425595892492" TEXT="Contempla los dos paradigmas de desarrollo: OO y Estructurado.">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425595902356" ID="ID_1351705543" MODIFIED="1425595903606" TEXT="Incluye el mantenimiento">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425595912407" ID="ID_1058051182" MODIFIED="1524388444448" TEXT="Incluye cuatro interfaces que completan las actividades de:">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425595923422" ID="ID_1586269716" MODIFIED="1425595925022" TEXT="Gesti&#xf3;n de proyectos"/>
<node COLOR="#111111" CREATED="1425595941795" ID="ID_1213532994" MODIFIED="1425595943005" TEXT="Gesti&#xf3;n de configuraci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425595954653" ID="ID_1191311708" MODIFIED="1425595955973" TEXT="Aseguramiento de la calidad"/>
<node COLOR="#111111" CREATED="1425595967125" ID="ID_481389830" MODIFIED="1425595968325" TEXT="Seguridad"/>
</node>
<node COLOR="#990000" CREATED="1425595983598" ID="ID_95463228" MODIFIED="1425595984948" TEXT="Propone una gu&#xed;a de t&#xe9;cnicas mucho m&#xe1;s amplia donde se distingue entre t&#xe9;cnicas (de uso general y formalizado) y pr&#xe1;cticas (de uso particular y no formalizado).">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1425593891903" ID="ID_991015505" MODIFIED="1524388556300" POSITION="right" TEXT="M&#xe9;trica versi&#xf3;n 3">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1425632035518" ID="ID_819951427" MODIFIED="1524388567145" TEXT="Introducci&#xf3;n">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425632045332" ID="ID_19653400" MODIFIED="1524388569825" TEXT="Objetivos">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425632074863" ID="ID_1678272814" MODIFIED="1425632076073" TEXT="Mantener la sencillez, flexibilidad y adaptabilidad de la versi&#xf3;n 2.1"/>
<node COLOR="#111111" CREATED="1425632084742" ID="ID_1521728292" MODIFIED="1425632102257" TEXT="Mantener la estructura de actividades y tareas de la versi&#xf3;n 2.1"/>
<node COLOR="#111111" CREATED="1425632120517" ID="ID_722452935" MODIFIED="1425632121620" TEXT="Incorporar nuevas t&#xe9;cnicas, tecnolog&#xed;as y m&#xe9;todos presentes en los desarrollos actuales cubriendo en una &#xfa;nica estructura tanto desarrollo estructurado como orientado a objetos"/>
<node COLOR="#111111" CREATED="1425632131221" ID="ID_1960865135" MODIFIED="1425632132281" TEXT="Incorporar aspectos de gesti&#xf3;n, bajo el concepto de INTERFACES que facilitan la realizaci&#xf3;n de los procesos de apoyo y organizativo"/>
<node COLOR="#111111" CREATED="1425632137951" ID="ID_1057484421" MODIFIED="1425632139000" TEXT="&#xc9;nfasis en el uso de est&#xe1;ndares de calidad e Ingenier&#xed;a del Software"/>
</node>
<node COLOR="#990000" CREATED="1425632180114" ID="ID_766087869" MODIFIED="1524388574437" TEXT="Caracter&#xed;sticas">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425632199347" ID="ID_315621359" MODIFIED="1425632200391" TEXT="contempla el desarrollo de Sistemas de Informaci&#xf3;n para las distintas tecnolog&#xed;as que actualmente est&#xe1;n conviviendo y los aspectos de gesti&#xf3;n que asegurar&#xe1;n que un Proyecto cumple sus objetivos en t&#xe9;rminos de calidad y coste."/>
<node COLOR="#111111" CREATED="1425632213862" ID="ID_256097823" MODIFIED="1425632214983" TEXT="Su punto de partida es la versi&#xf3;n anterior de M&#xe9;trica"/>
<node COLOR="#111111" CREATED="1425632233724" ID="ID_860094596" MODIFIED="1425632234928" TEXT="En la elaboraci&#xf3;n de M&#xe9;trica versi&#xf3;n 3 se han tenido en cuenta los m&#xe9;todos de desarrollo m&#xe1;s extendidos, as&#xed; como los &#xfa;ltimos est&#xe1;ndares de ingenier&#xed;a del software y calidad, as&#xed; como referencias espec&#xed;ficas en cuanto a seguridad y gesti&#xf3;n de proyectos"/>
</node>
<node COLOR="#990000" CREATED="1425632253670" ID="ID_1706363517" MODIFIED="1524388578732" TEXT="M&#xe9;trica se basa en los siguiente est&#xe1;ndares">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425632271681" ID="ID_427940442" MODIFIED="1425632273252" TEXT="ISO 12207 &#x201d;Information technology -Software life cycle processes&#x201d;."/>
<node COLOR="#111111" CREATED="1425632291481" ID="ID_1614407146" MODIFIED="1425632292804" TEXT="ISO/IEC TR 15.504 (SPICE) &#x201c;Software Process Improvement and assurance standards Capability Determination&#x201d;"/>
<node COLOR="#111111" CREATED="1425632304045" ID="ID_1472137795" MODIFIED="1425632305459" TEXT="ISO 9000-3 &#x201c;Quality management and quality&#x201d;. Part 3: Guidelines for the application of ISO 9001 &#x2013; &#x201c;Model for Quality Assurance in Design/Development , Production, Installation and Servicing&#x201d;"/>
<node COLOR="#111111" CREATED="1425632316249" ID="ID_617013688" MODIFIED="1425632317538" TEXT="IEEE &#x201c;Standard Glossary of Software Engineering Terminology&#x201d;. Std. 610.12-1998"/>
<node COLOR="#111111" CREATED="1425632324920" ID="ID_31323692" MODIFIED="1425632325908" TEXT="IEEE Std. 1074-1998: Software life-cycle processes"/>
<node COLOR="#111111" CREATED="1425632334298" ID="ID_1708521558" MODIFIED="1425632338626" TEXT="OMG standard UML"/>
</node>
<node COLOR="#990000" CREATED="1425632377791" ID="ID_547382629" MODIFIED="1524388602789" TEXT="M&#xe9;trica incorpora conocimiento procedente de otras metodolog&#xed;as existentes">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425632389319" ID="ID_349727070" MODIFIED="1425632390420" TEXT="PGGC, &#x201c;Plan General de Garant&#xed;a de Calidad para las Administraciones P&#xfa;blicas&#x201d;"/>
<node COLOR="#111111" CREATED="1425632401206" ID="ID_1236391563" MODIFIED="1425632402569" TEXT="MAGERIT, &#x201c;Metodolog&#xed;a de An&#xe1;lisis y Gesti&#xf3;n de Riesgos de los Sistemas de Informaci&#xf3;n para las Administraciones P&#xfa;blicas&#x201d;"/>
<node COLOR="#111111" CREATED="1425632408674" ID="ID_551715634" MODIFIED="1425632409674" TEXT="EUROM&#xc9;TODO V.1"/>
<node COLOR="#111111" CREATED="1425632415861" ID="ID_990643035" MODIFIED="1425632417082" TEXT="SSADM V.4"/>
<node COLOR="#111111" CREATED="1425632424210" ID="ID_821323286" MODIFIED="1425632426138" TEXT="Merise"/>
<node COLOR="#111111" CREATED="1425632431763" ID="ID_1513814554" MODIFIED="1425632432706" TEXT="Ingenier&#xed;a de la Informaci&#xf3;n"/>
</node>
<node COLOR="#990000" CREATED="1425632446914" ID="ID_133245483" MODIFIED="1524388659663" TEXT="Nuevas Aportaciones de la versi&#xf3;n 3">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425632460544" ID="ID_1486822590" MODIFIED="1425632461986" TEXT="Cubre desarrollo estructurado y Orientado a Objetos"/>
<node COLOR="#111111" CREATED="1425632473496" ID="ID_168571451" MODIFIED="1524388672858" TEXT="Estructura basada en procesos (ISO 12207)">
<node COLOR="#111111" CREATED="1425632500954" ID="ID_89462119" MODIFIED="1524388683535" TEXT="se definen tres procesos principales">
<node COLOR="#111111" CREATED="1425632514128" ID="ID_592215430" MODIFIED="1425632514129" TEXT="o Planificaci&#xf3;n (no dentro de ISO 12207)"/>
<node COLOR="#111111" CREATED="1425632514129" ID="ID_1216433291" MODIFIED="1425632514130" TEXT="o Desarrollo"/>
<node COLOR="#111111" CREATED="1425632514130" ID="ID_700450901" MODIFIED="1425632514130" TEXT="o Mantenimiento"/>
</node>
</node>
<node COLOR="#111111" CREATED="1425632548428" ID="ID_316717433" MODIFIED="1425632549552" TEXT="Se incluyen Interfaces para aspectos de gesti&#xf3;n"/>
</node>
<node COLOR="#990000" CREATED="1425632796452" ID="ID_1698535811" MODIFIED="1524388917848" TEXT="elementos principales de la metodolog&#xed;a son">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425632804506" ID="ID_910725324" MODIFIED="1425632804506" TEXT="&#xf0b7; Procesos"/>
<node COLOR="#111111" CREATED="1425632804507" ID="ID_1742747025" MODIFIED="1425632804507" TEXT="&#xf0b7; Interfaces"/>
<node COLOR="#111111" CREATED="1425632804508" ID="ID_73357283" MODIFIED="1425632804508" TEXT="&#xf0b7; T&#xe9;cnicas y Pr&#xe1;cticas"/>
<node COLOR="#111111" CREATED="1425632804508" ID="ID_1575837455" MODIFIED="1425632804508" TEXT="&#xf0b7; Roles"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1425633015468" ID="ID_1924696778" MODIFIED="1524388935149" TEXT="Esquema General">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425633117281" ID="ID_1019245065" MODIFIED="1425649118983">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Fotos%2086/Astic%206.png" />
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1425632828419" ID="ID_884944154" MODIFIED="1524388988430" TEXT="Estructura general">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425632840924" ID="ID_1701487224" MODIFIED="1425649128764" TEXT="Procesos principales &#xf0e8; Actividades &#xf0e8; Tareas">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425633312912" ID="ID_1592810079" MODIFIED="1524388994255" TEXT="Define tres grandes procesos">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425633327084" ID="ID_1843588006" MODIFIED="1425633328350" TEXT="Planificaci&#xf3;n de Sistemas de Informaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425633336808" ID="ID_1195541293" MODIFIED="1524388996704" TEXT="Desarrollo">
<node COLOR="#111111" CREATED="1425633426662" ID="ID_107984924" MODIFIED="1425633426663" TEXT="&#xf0b7; EVS &#x2013; Estudio de Viabilidad del Sistema"/>
<node COLOR="#111111" CREATED="1425633426664" ID="ID_645043070" MODIFIED="1425633426665" TEXT="&#xf0b7; ASI &#x2013; An&#xe1;lisis del Sistema de Informaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425633426667" ID="ID_656928389" MODIFIED="1425633426667" TEXT="&#xf0b7; DSI &#x2013; Dise&#xf1;o del Sistema de Informaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425633426669" ID="ID_678746614" MODIFIED="1425633426670" TEXT="&#xf0b7; CSI &#x2013; Construcci&#xf3;n del Sistema de Informaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425633426672" ID="ID_1726674189" MODIFIED="1425633426672" TEXT="&#xf0b7; IAS &#x2013; Implantaci&#xf3;n y Aceptaci&#xf3;n del Sistema"/>
</node>
<node COLOR="#111111" CREATED="1425633345426" ID="ID_469490793" MODIFIED="1425633349667" TEXT="Mantenimiento de Sistemas de Informaci&#xf3;n"/>
</node>
<node COLOR="#990000" CREATED="1425633566680" ID="ID_1538662560" MODIFIED="1524389719033" TEXT="Para cada Tarea se especifica">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425633593693" ID="ID_1489938655" MODIFIED="1425633593694" TEXT="&#xf0b7; Productos de Entrada que sirven de base para la ejecuci&#xf3;n de la tarea"/>
<node COLOR="#111111" CREATED="1425633593694" ID="ID_1607746514" MODIFIED="1425633593695" TEXT="&#xf0b7; Productos de Salida que se generar&#xe1;n en la tarea"/>
<node COLOR="#111111" CREATED="1425633593695" ID="ID_920503365" MODIFIED="1425633593695" TEXT="&#xf0b7; T&#xe9;cnicas y Pr&#xe1;cticas que se deben aplicar para transformar los productos de entrada en los de salida"/>
<node COLOR="#111111" CREATED="1425633593695" ID="ID_1587699986" MODIFIED="1425633593696" TEXT="&#xf0b7; Participantes en la ejecuci&#xf3;n de la tarea"/>
</node>
<node COLOR="#990000" CREATED="1425633653330" ID="ID_1155574184" MODIFIED="1524389787032" TEXT="Interfaces">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425633724224" ID="ID_66589825" MODIFIED="1425633737046" TEXT="Gesti&#xf3;n de Proyectos (GP)."/>
<node COLOR="#111111" CREATED="1425633730819" ID="ID_1475127095" MODIFIED="1425633730820" TEXT="Seguridad (SEG)."/>
<node COLOR="#111111" CREATED="1425633745348" ID="ID_182823973" MODIFIED="1425633745348" TEXT="Aseguramiento de la Calidad (CAL)."/>
<node COLOR="#111111" CREATED="1425633752285" ID="ID_75577302" MODIFIED="1425633752285" TEXT="Gesti&#xf3;n de la Configuraci&#xf3;n (GC)."/>
</node>
<node COLOR="#990000" CREATED="1425647238660" ID="ID_543238596" MODIFIED="1524389827541" TEXT="T&#xe9;cnicas y Pr&#xe1;cticas">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="messagebox_warning"/>
<node COLOR="#111111" CREATED="1425647253973" ID="ID_1278910308" MODIFIED="1425647255004" TEXT="permiten a los responsables de la ejecuci&#xf3;n de una tarea la obtenci&#xf3;n de los productos resultantes de dicha tarea"/>
<node COLOR="#111111" CREATED="1425647266640" ID="ID_1798072282" MODIFIED="1524390072126" TEXT="Las t&#xe9;cnicas">
<node COLOR="#111111" CREATED="1425649506240" ID="ID_248785144" MODIFIED="1425940833228" TEXT=" conjunto de heur&#xed;sticas y procedimientos que se apoyan en est&#xe1;ndares"/>
<node COLOR="#111111" CREATED="1425940833228" ID="ID_157470402" MODIFIED="1425940833238" TEXT="utilizan notaci&#xf3;n espec&#xed;fica (sintaxis y sem&#xe1;ntica) y criterios de calidad en la forma de obtenci&#xf3;n del producto"/>
</node>
<node COLOR="#111111" CREATED="1425647309983" ID="ID_382886530" MODIFIED="1524390135677" TEXT="Las pr&#xe1;cticas">
<node COLOR="#111111" CREATED="1425647326455" ID="ID_55844387" MODIFIED="1425647331823" TEXT="representan un medio para la consecuci&#xf3;n de unos objetivos espec&#xed;ficos de manera r&#xe1;pida, segura y precisa, "/>
<node COLOR="#111111" CREATED="1425647331829" ID="ID_735732442" MODIFIED="1425647331832" TEXT="sin necesidad de cumplir unos criterios r&#xed;gidos preestablecidos"/>
</node>
<node COLOR="#111111" CREATED="1425649633047" ID="ID_1094116266" MODIFIED="1425649647774">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Fotos%2086/CEF%208.png" />
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425941799705" ID="ID_1936396115" MODIFIED="1524410071594" TEXT="JRP vs JAD">
<node COLOR="#111111" CREATED="1425941828031" ID="ID_1469490538" MODIFIED="1425941828031" TEXT="Las caracter&#xed;sticas de las sesiones JRP y JAD son comunes en cuanto a la din&#xe1;mica del desarrollo de las sesiones y la obtenci&#xf3;n de los modelos con el soporte de las herramientas adecuadas"/>
<node COLOR="#111111" CREATED="1425941846354" ID="ID_769066007" MODIFIED="1524410102898" TEXT="La diferencia radica en los productos de salida y en los perfiles de los participantes">
<node COLOR="#111111" CREATED="1425941860514" ID="ID_1124896782" MODIFIED="1425941860514" TEXT="En JRP son del nivel m&#xe1;s alto en la organizaci&#xf3;n en cuanto a visi&#xf3;n global del negocio y capacidad de decisi&#xf3;n"/>
<node COLOR="#111111" CREATED="1425941875626" ID="ID_1563818536" MODIFIED="1425941875626" TEXT="Las sesiones JRP tienen como objetivo potenciar la participaci&#xf3;n activa de la alta direcci&#xf3;n como medio para obtener los mejores resultados en el menor tiempo posible y con una mayor calidad de los productos"/>
<node COLOR="#111111" CREATED="1425941899071" ID="ID_137113196" MODIFIED="1524410124193" TEXT="Las sesiones JAD tienen como objetivo reducir el tiempo de desarrollo de un sistema manteniendo la calidad del mismo">
<node COLOR="#111111" CREATED="1425941915695" ID="ID_1849573857" MODIFIED="1425941915695" TEXT="se involucra a los usuarios a lo largo de todo el desarrollo del sistema"/>
<node COLOR="#111111" CREATED="1447715756049" ID="ID_695270589" MODIFIED="1447715756050" TEXT="Se establece un equipo de trabajo cuyos componentes y responsabilidades est&#xe1;n perfectamente identificados y su fin es conseguir el consenso entre las necesidades de los usuarios y los servicios del sistema en producci&#xf3;n."/>
<node COLOR="#111111" CREATED="1447715769909" ID="ID_284030662" MODIFIED="1447715769909" TEXT="Se llevan a cabo pocas reuniones, de larga duraci&#xf3;n y muy bien preparadas."/>
<node COLOR="#111111" CREATED="1447715782861" ID="ID_341975939" MODIFIED="1447715782862" TEXT="Durante la propia sesi&#xf3;n se elaboran los modelos empleando diagramas f&#xe1;ciles de entender y mantener, directamente sobre herramientas CASE"/>
</node>
<node COLOR="#111111" CREATED="1502262079338" ID="ID_1773974172" MODIFIED="1524410166989" TEXT="La practica JAD distingue distintos perfiles">
<node COLOR="#111111" CREATED="1502262123979" ID="ID_1683596595" MODIFIED="1502262126297" TEXT="Moderador (l&#xed;der Jad)"/>
<node COLOR="#111111" CREATED="1502262132006" ID="ID_228363837" MODIFIED="1502262134515" TEXT="Desarrolladores"/>
<node COLOR="#111111" CREATED="1502262139624" ID="ID_1040527446" MODIFIED="1506536442404" TEXT="Promotor, persona que ha impulsado el desarrollo."/>
<node COLOR="#111111" CREATED="1506536455367" ID="ID_927218343" MODIFIED="1506536456983" TEXT="Jefe de proyecto, responsable de la implantaci&#xf3;n del proyecto."/>
<node COLOR="#111111" CREATED="1506536469691" ID="ID_930818170" MODIFIED="1506536471382" TEXT="Especialista en modelizaci&#xf3;n, responsable de la elaboraci&#xf3;n de los modelos en el transcurso de la sesi&#xf3;n"/>
<node COLOR="#111111" CREATED="1506536485446" ID="ID_1670221413" MODIFIED="1506536487142" TEXT="Usuarios, responsables de definir los requisitos del sistema y validarlos"/>
</node>
<node COLOR="#111111" CREATED="1506536529017" ID="ID_135407789" MODIFIED="1524410202340" TEXT="En las sesiones de trabajo tipo JAD se distinguen dos tipos de productos">
<node COLOR="#111111" CREATED="1506536540335" ID="ID_1671249737" MODIFIED="1506536542236" TEXT="De preparaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1506536548025" ID="ID_1126747729" MODIFIED="1506536549609" TEXT="De resultado"/>
</node>
</node>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1425647416767" ID="ID_1528126406" MODIFIED="1524410786150" TEXT="Roles">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425647461450" ID="ID_1401348186" MODIFIED="1524410820503" TEXT="&#xf0b7; Directivo">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425940889308" ID="ID_631383120" MODIFIED="1524410823772" TEXT="Participantes">
<node COLOR="#111111" CREATED="1425649200184" ID="ID_1489909828" MODIFIED="1425649216994">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comit&#233; de direcci&#243;n;
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425649216999" ID="ID_346983444" MODIFIED="1425649226914">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comit&#233; de Seguimiento;
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425649226916" ID="ID_48182990" MODIFIED="1425649233888">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Directores de usuarios;
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425649233894" ID="ID_1023668723" MODIFIED="1524410966797">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Usuarios expertos.
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="messagebox_warning"/>
<node COLOR="#111111" CREATED="1425940930877" ID="ID_576221341" MODIFIED="1425940930877" TEXT="Desempe&#xf1;an la misma funci&#xf3;n que los Directores de Usuarios pero con mayor nivel de  detalle"/>
</node>
</node>
<node COLOR="#111111" CREATED="1425940910762" ID="ID_1399774261" MODIFIED="1524410984386" TEXT="caracter&#xed;sticas comunes">
<node COLOR="#111111" CREATED="1425940942558" ID="ID_1006957780" MODIFIED="1425940942558" TEXT="Intervienen en todos los procesos principales de M&#xe9;trica versi&#xf3;n 3"/>
<node COLOR="#111111" CREATED="1425940971828" ID="ID_1150252220" MODIFIED="1425940971828" TEXT="Incluye a personas con un nivel alto en la direcci&#xf3;n de la organizaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425940988230" ID="ID_1156646799" MODIFIED="1425940988230" TEXT="Deben tener un conocimiento del entorno y de la organizaci&#xf3;n"/>
</node>
</node>
<node COLOR="#990000" CREATED="1425647461452" ID="ID_1786552730" MODIFIED="1524411006081" TEXT="&#xf0b7; Jefe de Proyecto">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425941026293" ID="ID_115726796" MODIFIED="1524411008523" TEXT="Participantes">
<node COLOR="#111111" CREATED="1425649289529" ID="ID_794680329" MODIFIED="1425649307722" TEXT="Jefe de Proyecto; "/>
<node COLOR="#111111" CREATED="1425649307724" ID="ID_1852764348" MODIFIED="1425649314209" TEXT="Responsable (de implantaci&#xf3;n, de mantenimiento, de operaci&#xf3;n, de sistemas);"/>
<node COLOR="#111111" CREATED="1425649314212" ID="ID_424833951" MODIFIED="1466496350569" TEXT=" Responsable (de seguridad, de calidad).">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node COLOR="#111111" CREATED="1425941041413" ID="ID_871952754" MODIFIED="1524411226007" TEXT="Caracteristicas Comunes">
<node COLOR="#111111" CREATED="1425941063186" ID="ID_1752978178" MODIFIED="1425941063186" TEXT="todos ellos ejercen labores de coordinaci&#xf3;n y direcci&#xf3;n de equipos humanos"/>
</node>
</node>
<node COLOR="#990000" CREATED="1425647461454" ID="ID_1743127163" MODIFIED="1524411229916" TEXT="&#xf0b7; Consultor">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425941201732" ID="ID_1054949277" MODIFIED="1524411232175" TEXT="Participantes">
<node COLOR="#111111" CREATED="1425941154300" ID="ID_554369403" MODIFIED="1425941156591" TEXT="Consultor (de negocio, inform&#xe1;tico, de Tecnolog&#xed;as de la informaci&#xf3;n, de sistemas de informaci&#xf3;n)"/>
<node COLOR="#111111" CREATED="1425941192971" ID="ID_1635273689" MODIFIED="1425941194701" TEXT="Especialista en Comunicaciones"/>
<node COLOR="#111111" CREATED="1425649366964" ID="ID_1227203786" MODIFIED="1425649385275" TEXT="T&#xe9;cnico de Sistemas; ">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425649379649" ID="ID_608214718" MODIFIED="1425649390880" TEXT="T&#xe9;cnico de Comunicaciones.">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1425647461454" ID="ID_698928835" MODIFIED="1524411305913" TEXT="&#xf0b7; Analista">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425649405897" ID="ID_1626216491" MODIFIED="1425649418662" TEXT="Analistas; "/>
<node COLOR="#111111" CREATED="1425649418664" ID="ID_183325122" MODIFIED="1447332758428" TEXT="DBA, Administrador de Base de Datos">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425649424185" ID="ID_1651568972" MODIFIED="1425941353024" TEXT="Equipo de (Proyecto, Arquitectura, Implantaci&#xf3;n, Operaci&#xf3;n, Seguridad, Soporte T&#xe9;cnico);"/>
<node COLOR="#111111" CREATED="1425649430195" ID="ID_1775736206" MODIFIED="1425649440904">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;Equipo de Formaci&#243;n;
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425649440906" ID="ID_146178206" MODIFIED="1425649440910">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Grupo de aseguramiento de la Calidad
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node COLOR="#990000" CREATED="1425647461455" ID="ID_1448557152" MODIFIED="1524411397013" TEXT="&#xf0b7; Programador">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425649460042" ID="ID_1771134558" MODIFIED="1425649466845" TEXT="Programador."/>
</node>
</node>
<node COLOR="#00b439" CREATED="1425941395546" ID="ID_582204560" MODIFIED="1524411629895" TEXT="Herramientas">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425941420387" ID="ID_289083413" MODIFIED="1524411634645" TEXT="Gestor Metodol&#xf3;gico">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425941427438" ID="ID_1636293872" MODIFIED="1425941427438" TEXT="herramienta software"/>
<node COLOR="#111111" CREATED="1425941448904" ID="ID_1242312877" MODIFIED="1425941458706" TEXT="de ayuda a la aplicaci&#xf3;n de la metodolog&#xed;a en cada proyecto concreto "/>
<node COLOR="#111111" CREATED="1425941458706" ID="ID_1661846482" MODIFIED="1425941468548" TEXT="permite adaptar la estructura de M&#xe9;trica versi&#xf3;n 3 de acuerdo a las caracter&#xed;sticas del mismo, "/>
<node COLOR="#111111" CREATED="1425941468548" ID="ID_1096487980" MODIFIED="1425941468548" TEXT="permitiendo el seguimiento y control de sus actividades y tareas realizadas por distintos perfiles de usuario asignados a los participantes por el jefe de proyecto"/>
</node>
<node COLOR="#990000" CREATED="1425941485006" ID="ID_5823474" MODIFIED="1524411640209" TEXT="Selector de Herramientas">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425941427438" ID="ID_211756175" MODIFIED="1425941427438" TEXT="herramienta software"/>
<node COLOR="#111111" CREATED="1425941507691" ID="ID_1207566912" MODIFIED="1425941507691" TEXT="ayuda a seleccionar entre las CASE del mercado la que mejor se adapta a las necesidades de cada proyecto teniendo en cuenta las caracter&#xed;sticas de cada organizaci&#xf3;n"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1425647522034" ID="ID_429143975" MODIFIED="1524411699111" POSITION="right" TEXT="Procesos de M&#xe9;trica versi&#xf3;n 3">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1425647534142" ID="ID_1486061619" MODIFIED="1524411797355" TEXT="PLANIFICACI&#xd3;N DE SISTEMAS DE INFORMACI&#xd3;N">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425647613749" ID="ID_204577378" MODIFIED="1425647629646" TEXT="No esta dentro de ISO 12207">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425647647105" ID="ID_756915625" MODIFIED="1524411806329" TEXT="Objetivo">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425647828886" ID="ID_670828407" MODIFIED="1524411822466" TEXT="Obtener un marco de referencia para el desarrollo de SI que responda a los objetivos estrat&#xe9;gicos de la organizaci&#xf3;n">
<node COLOR="#111111" CREATED="1425647843320" ID="ID_616060818" MODIFIED="1425647843321" TEXT="a. Descripci&#xf3;n cr&#xed;tica de la situaci&#xf3;n actual"/>
<node COLOR="#111111" CREATED="1425647843324" ID="ID_979783159" MODIFIED="1425647843325" TEXT="b. Arquitectura de la informaci&#xf3;n de alto nivel"/>
<node COLOR="#111111" CREATED="1425647843326" ID="ID_1650471334" MODIFIED="1425647843327" TEXT="c. Propuesta de proyectos (con prioridades)"/>
<node COLOR="#111111" CREATED="1425647843329" ID="ID_1000425578" MODIFIED="1425647843330" TEXT="d. Propuesta de calendario y estimaci&#xf3;n de recursos"/>
</node>
<node COLOR="#111111" CREATED="1425647863039" ID="ID_1288168419" MODIFIED="1524411999984" TEXT="En este proceso">
<node COLOR="#111111" CREATED="1425647869828" ID="ID_639389377" MODIFIED="1425647869829" TEXT="1. Se estudian las necesidades de informaci&#xf3;n de los procesos de la organizaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425647869831" ID="ID_1897651151" MODIFIED="1425647869832" TEXT="2. Se definen los requisitos generales"/>
<node COLOR="#111111" CREATED="1425647869834" ID="ID_1326387220" MODIFIED="1425647869835" TEXT="3. Se obtienen modelos conceptuales de informaci&#xf3;n y de SI"/>
<node COLOR="#111111" CREATED="1425647869837" ID="ID_1402806682" MODIFIED="1425647869837" TEXT="4. Se eval&#xfa;an las opciones tecnol&#xf3;gicas y se propone un entorno"/>
<node COLOR="#111111" CREATED="1425647869839" ID="ID_1800623626" MODIFIED="1425647869840" TEXT="5. Se elabora un calendario de proyectos"/>
<node COLOR="#111111" CREATED="1425647869841" ID="ID_1959953222" MODIFIED="1425647869842" TEXT="6. Se planifican en detalle los proy. m&#xe1;s pr&#xf3;ximos"/>
<node COLOR="#111111" CREATED="1425647869844" ID="ID_668338609" MODIFIED="1425647869845" TEXT="7. Se mantiene actualizado el PSI"/>
</node>
</node>
<node COLOR="#990000" CREATED="1425647934568" ID="ID_1829298971" MODIFIED="1524412257186" TEXT="Visi&#xf3;n general">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425647951077" ID="ID_890901081" MODIFIED="1425647951078" TEXT="La presentaci&#xf3;n del Plan de Sistemas de Informaci&#xf3;n y la constituci&#xf3;n del equipo supone el arranque del proyecto"/>
<node COLOR="#111111" CREATED="1425648010177" ID="ID_663782790" MODIFIED="1425648010178" TEXT="La perspectiva del plan debe ser estrat&#xe9;gica y operativa, no tecnol&#xf3;gica"/>
<node COLOR="#111111" CREATED="1425648026717" ID="ID_768863793" MODIFIED="1425648028609" TEXT="es fundamental que la alta direcci&#xf3;n de la organizaci&#xf3;n tome parte activa en la decisi&#xf3;n del Plan de Sistemas de Informaci&#xf3;n"/>
</node>
<node COLOR="#990000" CREATED="1470729868821" ID="ID_1450593405" MODIFIED="1470729927004" TEXT="Para la elaboraci&#xf3;n del Plan de Sistemas de la Informaci&#xf3;n se estudian las necesidades de informaci&#xf3;n de los procesos afectados con el fin de definir requisitos generales y obtener modelos conceptuales de informaci&#xf3;n.">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#990000" CREATED="1425648052871" ID="ID_1126936795" MODIFIED="1524412417061" TEXT="Actividades">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425661683114" ID="ID_1985574628" MODIFIED="1425661696733">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Fotos%2086/PSI.png" />
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425663207684" ID="ID_14677644" MODIFIED="1524412443410" TEXT="ACTIVIDAD PSI 1: INICIO DEL PLAN DE SISTEMAS DE INFORMACI&#xd3;N">
<node COLOR="#111111" CREATED="1476780146191" ID="ID_1561357088" MODIFIED="1476780185955" TEXT="Los part&#xed;cipes son Comit&#xe9; de Direcci&#xf3;n y se realiza mediante Sesiones de trabajo">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node COLOR="#111111" CREATED="1425663224735" ID="ID_1352391390" MODIFIED="1425663226154" TEXT="ACTIVIDAD PSI 2: DEFINICI&#xd3;N Y ORGANIZACI&#xd3;N DEL PSI"/>
<node COLOR="#111111" CREATED="1425663240491" ID="ID_737710466" MODIFIED="1425663242394" TEXT="ACTIVIDAD PSI 3: ESTUDIO DE LA INFORMACI&#xd3;N RELEVANTE"/>
<node COLOR="#111111" CREATED="1425663257323" ID="ID_1841048463" MODIFIED="1425663259055" TEXT="ACTIVIDAD PSI 4: IDENTIFICACI&#xd3;N DE REQUISITOS"/>
<node COLOR="#111111" CREATED="1425663268711" ID="ID_529797109" MODIFIED="1455874578733" TEXT="ACTIVIDAD PSI 5: ESTUDIO DE LOS SISTEMAS DE INFORMACI&#xd3;N ACTUALES">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425663287634" ID="ID_603724102" MODIFIED="1455871247972" TEXT="ACTIVIDAD PSI 6: DISE&#xd1;O DEL MODELO DE SISTEMAS DE INFORMACI&#xd3;N">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425663299537" ID="ID_1024196441" MODIFIED="1425665344508" TEXT="ACTIVIDAD PSI 7: DEFINICI&#xd3;N DE LA ARQUITECTURA TECNOL&#xd3;GICA">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425663312766" ID="ID_1937255289" MODIFIED="1425663314310" TEXT="ACTIVIDAD PSI 8: DEFINICI&#xd3;N DEL PLAN DE ACCI&#xd3;N"/>
<node COLOR="#111111" CREATED="1425663322672" ID="ID_1945153812" MODIFIED="1425663324029" TEXT="ACTIVIDAD PSI 9: REVISI&#xd3;N Y APROBACI&#xd3;N DEL PSI"/>
</node>
<node COLOR="#990000" CREATED="1425942151666" ID="ID_1694489755" MODIFIED="1524413643074" TEXT="Participes">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425942165020" ID="ID_689766704" MODIFIED="1524413658960" TEXT="Comit&#xe9; de Direcci&#xf3;n.">
<node COLOR="#111111" CREATED="1447316121372" ID="ID_1654633665" MODIFIED="1447316776428" TEXT="Rol Directivo">
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1654633665" ENDARROW="Default" ENDINCLINATION="31;0;" ID="Arrow_ID_1708163862" SOURCE="ID_950742246" STARTARROW="None" STARTINCLINATION="31;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1654633665" ENDARROW="Default" ENDINCLINATION="129;0;" ID="Arrow_ID_1336365721" SOURCE="ID_1645088015" STARTARROW="None" STARTINCLINATION="129;0;"/>
</node>
</node>
<node COLOR="#111111" CREATED="1425942180448" ID="ID_950742246" MODIFIED="1447316772998">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Directores de Usuarios
    </p>
  </body>
</html></richcontent>
<arrowlink DESTINATION="ID_1654633665" ENDARROW="Default" ENDINCLINATION="31;0;" ID="Arrow_ID_1708163862" STARTARROW="None" STARTINCLINATION="31;0;"/>
</node>
<node COLOR="#111111" CREATED="1447316772998" ID="ID_1645088015" MODIFIED="1447316776428">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Usuarios expertos
    </p>
  </body>
</html></richcontent>
<arrowlink DESTINATION="ID_1654633665" ENDARROW="Default" ENDINCLINATION="129;0;" ID="Arrow_ID_1336365721" STARTARROW="None" STARTINCLINATION="129;0;"/>
</node>
<node COLOR="#111111" CREATED="1425942197461" ID="ID_1129660673" MODIFIED="1524413666974" TEXT="Jefe de Proyecto">
<node COLOR="#111111" CREATED="1447316190047" ID="ID_908162520" MODIFIED="1447316269621" TEXT="Rol Jefe de Proyecto">
<arrowlink DESTINATION="ID_908162520" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_1808474475" STARTARROW="None" STARTINCLINATION="0;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_908162520" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_1808474475" SOURCE="ID_908162520" STARTARROW="None" STARTINCLINATION="0;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_908162520" ENDARROW="Default" ENDINCLINATION="65;0;" ID="Arrow_ID_1110595816" SOURCE="ID_1871517311" STARTARROW="None" STARTINCLINATION="65;0;"/>
</node>
</node>
<node COLOR="#111111" CREATED="1447316250762" ID="ID_1871517311" MODIFIED="1447316269161">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Responsable de Mantenimiento.
    </p>
  </body>
</html></richcontent>
<arrowlink DESTINATION="ID_908162520" ENDARROW="Default" ENDINCLINATION="65;0;" ID="Arrow_ID_1110595816" STARTARROW="None" STARTINCLINATION="65;0;"/>
</node>
<node COLOR="#111111" CREATED="1425942172585" ID="ID_358197377" MODIFIED="1524413676198" TEXT="Consultores y Consultores Inform&#xe1;ticos">
<node COLOR="#111111" CREATED="1447316139661" ID="ID_1635849140" MODIFIED="1447316147670" TEXT="Rol Consultor"/>
</node>
<node COLOR="#111111" CREATED="1425942188580" ID="ID_1404121086" MODIFIED="1524413679223">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Equipo de soporte t&#233;cnico
    </p>
  </body>
</html></richcontent>
<node COLOR="#111111" CREATED="1447316289069" ID="ID_72068913" MODIFIED="1447368001125" TEXT="Rol Analista">
<linktarget COLOR="#b0b0b0" DESTINATION="ID_72068913" ENDARROW="Default" ENDINCLINATION="145;0;" ID="Arrow_ID_561053607" SOURCE="ID_91006972" STARTARROW="None" STARTINCLINATION="145;0;"/>
</node>
</node>
<node COLOR="#111111" CREATED="1447367977657" ID="ID_91006972" MODIFIED="1447368001125" TEXT="Equipo de Proyecto">
<arrowlink DESTINATION="ID_72068913" ENDARROW="Default" ENDINCLINATION="145;0;" ID="Arrow_ID_561053607" STARTARROW="None" STARTINCLINATION="145;0;"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1425647541771" ID="ID_719190739" MODIFIED="1524413806644" TEXT="DESARROLLO DE SISTEMAS DE INFORMACI&#xd3;N">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425663560714" ID="ID_1801073350" MODIFIED="1425663561946" TEXT="contiene todas las actividades y tareas que se deben llevar a cabo para desarrollar un sistema,">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425663577078" ID="ID_891597846" MODIFIED="1425663714639" TEXT="Cubre desde el an&#xe1;lisis de requisitos hasta la instalaci&#xf3;n del software">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425663766618" ID="ID_1032434184" MODIFIED="1524413813948" TEXT="Tareas que cubre">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425663789901" ID="ID_184721982" MODIFIED="1425663789901" TEXT="relativas al an&#xe1;lisis"/>
<node COLOR="#111111" CREATED="1425663803949" ID="ID_150534416" MODIFIED="1524413822177">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      dos partes en el dise&#241;o de sistemas:
    </p>
  </body>
</html></richcontent>
<node COLOR="#111111" CREATED="1425663812872" ID="ID_835149271" MODIFIED="1425663818551">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      arquitect&#243;nico
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425663818551" ID="ID_784648345" MODIFIED="1425663818566">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      detallado
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node COLOR="#111111" CREATED="1425663840703" ID="ID_993425149" MODIFIED="1425663842060" TEXT="pruebas unitarias y de integraci&#xf3;n del sistema"/>
</node>
<node COLOR="#990000" CREATED="1425647567617" ID="ID_1708706770" MODIFIED="1524413851432" TEXT="&#xf0b7; ESTUDIO DE VIABILIDAD DEL SISTEMA (EVS).">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425663890592" ID="ID_51387213" MODIFIED="1524413956860" TEXT="Objetivo">
<node COLOR="#111111" CREATED="1425663931495" ID="ID_1227902178" MODIFIED="1425663932743" TEXT="Analizar las necesidades y proponer una soluci&#xf3;n a corto plazo,  basada en criterios econ&#xf3;micos, t&#xe9;cnicos, legales y operativos."/>
</node>
<node COLOR="#111111" CREATED="1425663956596" ID="ID_528261662" MODIFIED="1524414082191" TEXT="Visi&#xf3;n General">
<node COLOR="#111111" CREATED="1425663991009" ID="ID_1649660814" MODIFIED="1425663992117" TEXT="Se estudian los requisitos que se han de satisfacer y, si procede, la situaci&#xf3;n actual"/>
<node COLOR="#111111" CREATED="1425664007124" ID="ID_460057592" MODIFIED="1425664008325" TEXT="Se plantean alternativas de soluci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425664018886" ID="ID_237124751" MODIFIED="1524414095046" TEXT="Para cada alternativa">
<node COLOR="#111111" CREATED="1425664026109" ID="ID_1636449448" MODIFIED="1425664027295" TEXT="valorar impacto en la organizaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425664042177" ID="ID_1322677652" MODIFIED="1425664043456" TEXT="inversi&#xf3;n a realizar"/>
<node COLOR="#111111" CREATED="1425664050711" ID="ID_287892468" MODIFIED="1425664051959" TEXT="riesgos asociados"/>
<node COLOR="#111111" CREATED="1425664062912" ID="ID_36568525" MODIFIED="1425664064113" TEXT="Evaluar las distintas alternativas y seleccionar la soluci&#xf3;n m&#xe1;s adecuada"/>
<node COLOR="#111111" CREATED="1425664086577" ID="ID_364530778" MODIFIED="1425664087653" TEXT="Si la justificaci&#xf3;n econ&#xf3;mica es obvia, el riesgo t&#xe9;cnico bajo, se esperan pocos problemas legales y existe una alternativa clara, este proceso se orienta a la especificaci&#xf3;n de requisitos, descripci&#xf3;n del nuevo sistema y planificaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425664103004" ID="ID_158183283" MODIFIED="1425664104392" TEXT="El estudio de la situaci&#xf3;n actual debe ajustarse a los beneficios que se puedan obtener de &#xe9;l"/>
</node>
</node>
<node COLOR="#111111" CREATED="1425664208898" ID="ID_1740690160" MODIFIED="1524414493966" TEXT="Actividades">
<node COLOR="#111111" CREATED="1425664220286" ID="ID_1562108663" MODIFIED="1425664231986">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Fotos%2086/EVS.png" />
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425664302966" ID="ID_1493066706" MODIFIED="1524414510158" TEXT="ACTIVIDAD EVS 1: ESTABLECIMIENTO DEL ALCANCE DEL SISTEMA">
<node COLOR="#111111" CREATED="1447368308818" ID="ID_1622036334" MODIFIED="1447368308819" TEXT="Si la justificaci&#xf3;n econ&#xf3;mica es obvia, el riesgo t&#xe9;cnico bajo, se esperan pocos problemas legales y no existe&#xd;ninguna alternativa razonable, no es necesario profundizar en el estudio de viabilidad del sistema, analizando&#xd;posibles alternativas y realizando una valoraci&#xf3;n y evaluaci&#xf3;n de las mismas, sino que &#xe9;ste se orientar&#xe1; a la&#xd;especificaci&#xf3;n de requisitos, descripci&#xf3;n del nuevo sistema y planificaci&#xf3;n"/>
</node>
<node COLOR="#111111" CREATED="1425664323932" ID="ID_1907283822" MODIFIED="1455874568793" TEXT="ACTIVIDAD EVS 2: ESTUDIO DE LA SITUACI&#xd3;N ACTUAL">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425664348471" ID="ID_1644863943" MODIFIED="1524414668068" TEXT="ACTIVIDAD EVS 3: DEFINICI&#xd3;N DE REQUISITOS DEL SISTEMA">
<icon BUILTIN="messagebox_warning"/>
<node COLOR="#111111" CREATED="1477344418848" ID="ID_370661099" MODIFIED="1477344445775" TEXT="Participan  Jefe de proyecto, Analistas y Usuarios Expertos"/>
</node>
<node COLOR="#111111" CREATED="1425664359781" ID="ID_706505937" MODIFIED="1425664361107" TEXT="ACTIVIDAD EVS 4: ESTUDIO DE ALTERNATIVAS DE SOLUCI&#xd3;N"/>
<node COLOR="#111111" CREATED="1425664379063" ID="ID_1414936506" MODIFIED="1425664380358" TEXT="ACTIVIDAD EVS 5: VALORACI&#xd3;N DE LAS ALTERNATIVAS">
<node COLOR="#111111" CREATED="1524498251543" ID="ID_902054538" MODIFIED="1524498267304" TEXT="Estudio de alternativas">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node COLOR="#111111" CREATED="1425664392370" ID="ID_1863238728" MODIFIED="1425664393711" TEXT="ACTIVIDAD EVS 6: SELECCI&#xd3;N DE LA SOLUCI&#xd3;N"/>
</node>
</node>
<node COLOR="#990000" CREATED="1425647567627" ID="ID_1888895076" MODIFIED="1524414947262" TEXT="&#xf0b7; AN&#xc1;LISIS DEL SISTEMA DE INFORMACI&#xd3;N (ASI).">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425664569290" ID="ID_1586525830" MODIFIED="1524414949504" TEXT="Objetivo">
<node COLOR="#111111" CREATED="1425664592783" ID="ID_119787482" MODIFIED="1425664594265" TEXT="obtener una especificaci&#xf3;n detallada del sistema de informaci&#xf3;n que satisfaga las necesidades de informaci&#xf3;n de los usuarios y sirva de  base para el posterior dise&#xf1;o del sistema"/>
</node>
<node COLOR="#111111" CREATED="1425664609397" ID="ID_1294544663" MODIFIED="1524415060455" TEXT="Visi&#xf3;n General">
<node COLOR="#111111" CREATED="1425664690065" ID="ID_409972890" MODIFIED="1425664692389" TEXT="se procede a entender el problema a resolver y posteriormente a generar una especificaci&#xf3;n detallada del sistema a construir"/>
<node COLOR="#111111" CREATED="1425664733059" ID="ID_795251670" MODIFIED="1524415087929" TEXT="Existen m&#xfa;ltiples enfoques a la hora de realizar el an&#xe1;lisis">
<node COLOR="#111111" CREATED="1425664747598" ID="ID_536095762" MODIFIED="1425664749283" TEXT="enfoque orientado a objetos"/>
<node COLOR="#111111" CREATED="1425664759329" ID="ID_1407158288" MODIFIED="1425664761810" TEXT="enfoque estructurado"/>
</node>
<node COLOR="#111111" CREATED="1425665037203" ID="ID_1493232922" MODIFIED="1524415093462" TEXT="Los principales pasos que se realizan">
<node COLOR="#111111" CREATED="1425665052273" ID="ID_658910583" MODIFIED="1425665054223" TEXT="Delimitar el sistema y el entorno y lo que &#xe9;ste va a hacer"/>
<node COLOR="#111111" CREATED="1425665074706" ID="ID_1480878913" MODIFIED="1425665076859" TEXT="Establecer los requisitos que el sistema debe cumplir"/>
<node COLOR="#111111" CREATED="1425665088995" ID="ID_586729937" MODIFIED="1425665090899" TEXT="Identificar los diferentes subsistemas que componen el sistema"/>
<node COLOR="#111111" CREATED="1425665111179" FOLDED="true" ID="ID_579007003" MODIFIED="1524173768048" TEXT="Seg&#xfa;n el enfoque que se emplee se confeccionar&#xe1; un conjunto de diagramas">
<node COLOR="#111111" CREATED="1425665157807" FOLDED="true" ID="ID_726607285" MODIFIED="1524173768047" TEXT="Enfoque Estructurado">
<node COLOR="#111111" CREATED="1425665172034" ID="ID_1759499253" MODIFIED="1425665173704" TEXT="Modelo de Datos"/>
<node COLOR="#111111" CREATED="1425665186386" ID="ID_1885306016" MODIFIED="1425665188040" TEXT="Modelo de Procesos"/>
</node>
<node COLOR="#111111" CREATED="1425665210785" FOLDED="true" ID="ID_813433456" MODIFIED="1524173768047" TEXT="Enfoque Orientado a Objetos">
<node COLOR="#111111" CREATED="1425665221518" ID="ID_1147922290" MODIFIED="1425665223592" TEXT="Diagrama de Casos de Uso"/>
<node COLOR="#111111" CREATED="1425665232329" ID="ID_709286853" MODIFIED="1425665235168" TEXT="Diagrama de Clases"/>
</node>
</node>
<node COLOR="#111111" CREATED="1425665266103" FOLDED="true" ID="ID_1101134842" MODIFIED="1524173768048" TEXT="Definir las interfaces de usuario del sistema">
<node COLOR="#111111" CREATED="1425665280782" ID="ID_757398659" MODIFIED="1425665282529" TEXT="el aspecto que tendr&#xe1;n los diferentes elementos a trav&#xe9;s de los que el usuario interact&#xfa;a con el sistema (pantallas, informes, etc.)."/>
</node>
<node COLOR="#111111" CREATED="1425665296179" ID="ID_1955894988" MODIFIED="1425665297817" TEXT="Tras confeccionar todos los documentos anteriores se procede a realizar una comprobaci&#xf3;n de consistencia de los mismos"/>
<node COLOR="#111111" CREATED="1425665314260" ID="ID_146512819" MODIFIED="1425665325180" TEXT="Realizar la especificaci&#xf3;n del plan de pruebas">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425665380778" ID="ID_1277694027" MODIFIED="1425665382479" TEXT="Presentaci&#xf3;n del an&#xe1;lisis realizado ante los usuarios y se aprueba por estos dicho an&#xe1;lisis"/>
</node>
</node>
<node COLOR="#111111" CREATED="1425665419420" ID="ID_1406200453" MODIFIED="1524415117053" TEXT="Actividades">
<node COLOR="#111111" CREATED="1425665483407" ID="ID_848061034" MODIFIED="1425665494389">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Fotos%2086/ASI.png" />
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425666119173" ID="ID_1730013355" MODIFIED="1425666121092" TEXT="ASI 1: Definici&#xf3;n del Sistema"/>
<node COLOR="#111111" CREATED="1425666130405" ID="ID_516303432" MODIFIED="1455874550127" TEXT="ASI 2: Establecimiento de Requisitos">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425666148080" ID="ID_786965439" MODIFIED="1425666149359" TEXT="ASI 3: Identificaci&#xf3;n de Subsistemas de An&#xe1;lisis"/>
<node COLOR="#111111" CREATED="1425666163555" ID="ID_1096179560" MODIFIED="1425666165178" TEXT="ASI 4: An&#xe1;lisis de los Casos de Uso"/>
<node COLOR="#111111" CREATED="1425666173680" ID="ID_16331304" MODIFIED="1425666175021" TEXT="ASI 5: An&#xe1;lisis de Clases"/>
<node COLOR="#111111" CREATED="1425666184912" ID="ID_285391341" MODIFIED="1524415218240" TEXT="ASI 6: Elaboraci&#xf3;n del Modelo de Datos">
<node COLOR="#111111" CREATED="1502260003419" ID="ID_945139548" MODIFIED="1502260008392" TEXT="participan los &quot;Usuarios expertos&quot;"/>
</node>
<node COLOR="#111111" CREATED="1425666196425" ID="ID_1057681672" MODIFIED="1425666197953" TEXT="ASI 7: Elaboraci&#xf3;n del Modelo de Procesos"/>
<node COLOR="#111111" CREATED="1425666211510" ID="ID_1858861533" MODIFIED="1425666213132" TEXT="ASI 8: Definici&#xf3;n de Interfaces de Usuario"/>
<node COLOR="#111111" CREATED="1425666223943" ID="ID_619079926" MODIFIED="1425666225441" TEXT="ASI 9: An&#xe1;lisis de la Consistencia y Especificaci&#xf3;n de Requisitos"/>
<node COLOR="#111111" CREATED="1425666242070" ID="ID_183483150" MODIFIED="1425666288231" TEXT="ASI 10: Especificaci&#xf3;n del Plan de Pruebas">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425666257421" ID="ID_821872207" MODIFIED="1425666258778" TEXT="ASI 11: Aprobaci&#xf3;n del An&#xe1;lisis del Sistema de Informaci&#xf3;n "/>
</node>
</node>
<node COLOR="#990000" CREATED="1425647567632" ID="ID_1844428334" MODIFIED="1524416173159" TEXT="&#xf0b7; DISE&#xd1;O DEL SISTEMA DE INFORMACI&#xd3;N (DSI).">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425666308963" ID="ID_219522053" MODIFIED="1524416175359" TEXT="Objetivo">
<node COLOR="#111111" CREATED="1425666337527" ID="ID_1090932754" MODIFIED="1425666339804" TEXT="objetivo principal la especificaci&#xf3;n de la construcci&#xf3;n del sistema."/>
</node>
<node COLOR="#111111" CREATED="1425666572120" ID="ID_1178743196" MODIFIED="1524416188527" TEXT="Visi&#xf3;n General">
<node COLOR="#111111" CREATED="1425666584444" ID="ID_1843858960" MODIFIED="1524416190812" TEXT="se debe definir">
<node COLOR="#111111" CREATED="1425666594319" ID="ID_268220845" MODIFIED="1425666595832" TEXT="La arquitectura a seguir (cliente / servidor, arquitecturas multinivel &#x2026;)"/>
<node COLOR="#111111" CREATED="1425666611963" ID="ID_670232439" MODIFIED="1425666613257" TEXT="El entorno tecnol&#xf3;gico que le dar&#xe1; soporte"/>
<node COLOR="#111111" CREATED="1425666625706" ID="ID_10308996" MODIFIED="1425666626954" TEXT="Una especificaci&#xf3;n detallada de los componentes del sistema de informaci&#xf3;n"/>
</node>
<node COLOR="#111111" CREATED="1425666635675" ID="ID_1726664838" MODIFIED="1524416204152" TEXT="A partir de esta informaci&#xf3;n se generan:">
<node COLOR="#111111" CREATED="1425666647687" ID="ID_1000034895" MODIFIED="1425666648966" TEXT="La especificaci&#xf3;n t&#xe9;cnica del plan de pruebas"/>
<node COLOR="#111111" CREATED="1425666660276" ID="ID_886021610" MODIFIED="1425666661742" TEXT="La definici&#xf3;n de los requisitos de implantaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425666671118" ID="ID_1546499272" MODIFIED="1425666672537" TEXT="El dise&#xf1;o de los procedimientos de migraci&#xf3;n y carga inicial, cuando proceda en el sistema."/>
</node>
</node>
<node COLOR="#111111" CREATED="1425666793703" ID="ID_559813461" MODIFIED="1524416278400" TEXT="Actividades">
<node COLOR="#111111" CREATED="1425666805730" ID="ID_1131592729" MODIFIED="1425666818897">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Fotos%2086/DSI.png" />
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425667151939" ID="ID_1801582016" MODIFIED="1425667374691" TEXT="DSI 1: Definici&#xf3;n de la arquitectura del sistema">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425667178147" ID="ID_35347320" MODIFIED="1425667381243" TEXT="DSI 2: Dise&#xf1;o de la arquitectura de soporte">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425667200314" ID="ID_48946211" MODIFIED="1425667201656" TEXT="DSI 3: Dise&#xf1;o de casos de uso reales"/>
<node COLOR="#111111" CREATED="1425667210610" ID="ID_1178085664" MODIFIED="1425667211889" TEXT="DSI 4: Dise&#xf1;o de clases"/>
<node COLOR="#111111" CREATED="1425667225992" ID="ID_392612572" MODIFIED="1455875623340" TEXT="DSI 5: Dise&#xf1;o de la arquitectura de m&#xf3;dulos del sistema">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425667238472" ID="ID_1943816518" MODIFIED="1425667239736" TEXT="DSI 6: Dise&#xf1;o f&#xed;sico de datos"/>
<node COLOR="#111111" CREATED="1425667257083" ID="ID_568851871" MODIFIED="1425667391836" TEXT="DSI 7: Verificaci&#xf3;n y aceptaci&#xf3;n de la arquitectura del sistema">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425667269500" ID="ID_1085103800" MODIFIED="1425667270764" TEXT="DSI 8: Generaci&#xf3;n de especificaciones de construcci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425667279937" ID="ID_1441575253" MODIFIED="1425667404846" TEXT="DSI 9: Dise&#xf1;o de la migraci&#xf3;n y carga inicial de datos">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425667292042" ID="ID_868974959" MODIFIED="1425667413629" TEXT="DSI 10: Especificaci&#xf3;n t&#xe9;cnica del plan de pruebas">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425667315567" ID="ID_123731587" MODIFIED="1425667424502" TEXT="DSI 11: Establecimiento de requisitos de implantaci&#xf3;n">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425667330684" ID="ID_1573065098" MODIFIED="1425667331916" TEXT="DSI 12: Aprobaci&#xf3;n del dise&#xf1;o del sistema de informaci&#xf3;n"/>
</node>
</node>
<node COLOR="#990000" CREATED="1425647567636" ID="ID_1647813328" MODIFIED="1524417087351" TEXT="&#xf0b7; CONSTRUCCI&#xd3;N DEL SISTEMA DE INFORMACI&#xd3;N (CSI).">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425667604105" ID="ID_1518737929" MODIFIED="1524417089740" TEXT="Objetivo ">
<node COLOR="#111111" CREATED="1425667616476" ID="ID_488676858" MODIFIED="1425667617943" TEXT="Codificar y probar los componentes del Sistema de Informaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425667643979" ID="ID_1275327288" MODIFIED="1425667646662" TEXT="Desarrollar los procedimientos de operaci&#xf3;n y seguridad."/>
<node COLOR="#111111" CREATED="1425667661794" ID="ID_126018485" MODIFIED="1425667663105" TEXT="Escribir los manuales de usuario y de explotaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425667682043" ID="ID_34896122" MODIFIED="1425667683369" TEXT="Realizar las pruebas unitarias, de integraci&#xf3;n y de sistema, faltando las de implantaci&#xf3;n y aceptaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425667696192" ID="ID_119517073" MODIFIED="1425667697706" TEXT="Construir los procedimientos de migraci&#xf3;n y carga inicial de datos, si procede."/>
</node>
<node COLOR="#111111" CREATED="1425667883613" ID="ID_1308735354" MODIFIED="1524417093904" TEXT="Actividades">
<node COLOR="#111111" CREATED="1425667901694" ID="ID_1098654091" MODIFIED="1425667912832">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Fotos%2086/CSI.png" />
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425667932800" ID="ID_1879651723" MODIFIED="1425667934251" TEXT="CSI 1: Preparaci&#xf3;n del entorno de generaci&#xf3;n y construcci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425667948790" ID="ID_288865230" MODIFIED="1425667950412" TEXT="CSI 2: Generaci&#xf3;n del c&#xf3;digo de los componentes y procedimientos"/>
<node COLOR="#111111" CREATED="1425667964109" ID="ID_1890375867" MODIFIED="1455875947817" TEXT="CSI 3: Ejecuci&#xf3;n de las pruebas unitarias">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425667979506" ID="ID_1720080044" MODIFIED="1455875951691" TEXT="CSI 4: Ejecuci&#xf3;n de las pruebas de integraci&#xf3;n">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425667997275" ID="ID_749797698" MODIFIED="1455875955981" TEXT="CSI 5: Ejecuci&#xf3;n de las pruebas del sistema">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425668010488" ID="ID_1023177286" MODIFIED="1425668266297" TEXT="CSI 6: Elaboraci&#xf3;n de los manuales de usuario">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425668208858" ID="ID_914349255" MODIFIED="1425668275408" TEXT="CSI 7: Definici&#xf3;n de la formaci&#xf3;n de usuarios finales">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425668224489" ID="ID_747807622" MODIFIED="1425668283614" TEXT="CSI 8: Construcci&#xf3;n de los componentes y procedimientos de migraci&#xf3;n y carga inicial de datos">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425668255128" ID="ID_792562193" MODIFIED="1425668256688" TEXT="CSI 9: Aprobaci&#xf3;n del sistema de informaci&#xf3;n "/>
</node>
</node>
<node COLOR="#990000" CREATED="1425647567639" ID="ID_327942082" MODIFIED="1524417418321" TEXT="&#xf0b7; IMPLANTACI&#xd3;N Y ACEPTACI&#xd3;N DEL SISTEMA (IAS).">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425668344174" ID="ID_1318052398" MODIFIED="1524417420745" TEXT="Objetivo">
<node COLOR="#111111" CREATED="1425668393532" ID="ID_168609287" MODIFIED="1425668521733" TEXT="El objetivo principal es la entrega y aceptaci&#xf3;n del sistema en su totalidad y la realizaci&#xf3;n de las actividades necesarias para el paso a producci&#xf3;n"/>
</node>
<node COLOR="#111111" CREATED="1425668738063" ID="ID_1085853133" MODIFIED="1524417429783" TEXT="Visi&#xf3;n General">
<node COLOR="#111111" CREATED="1425668746861" ID="ID_1500520776" MODIFIED="1425668748281" TEXT="Se prepara el entorno de explotaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425668757797" ID="ID_1103276730" MODIFIED="1425668758998" TEXT="Se instalan los componentes"/>
<node COLOR="#111111" CREATED="1425668767001" ID="ID_1162281214" MODIFIED="1425668768249" TEXT="Se activan los procedimientos manuales y autom&#xe1;ticos"/>
<node COLOR="#111111" CREATED="1425668780245" ID="ID_703388735" MODIFIED="1425668782413" TEXT="Se realiza la migraci&#xf3;n o carga inicial de datos"/>
<node COLOR="#111111" CREATED="1425668792070" ID="ID_1158748679" MODIFIED="1425668793240" TEXT="Se realiza la prueba de implantaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425668814378" ID="ID_234341972" MODIFIED="1425668815532" TEXT="Se realiza la prueba de aceptaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425668827544" ID="ID_533832768" MODIFIED="1425668828824" TEXT="Se prepara el mantenimiento"/>
</node>
<node COLOR="#111111" CREATED="1425668848261" ID="ID_900102986" MODIFIED="1524417432563" TEXT="Actividades">
<node COLOR="#111111" CREATED="1425668929023" ID="ID_890651473" MODIFIED="1425668944591">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Fotos%2086/IAS.png" />
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425668961642" ID="ID_1255534868" MODIFIED="1425668962812" TEXT="IAS 1: Establecimiento del plan de implantaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425668974746" ID="ID_90469834" MODIFIED="1425668975869" TEXT="IAS 2: Formaci&#xf3;n necesaria para la implantaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425668991423" ID="ID_66297511" MODIFIED="1425668992624" TEXT="IAS 3: Incorporaci&#xf3;n del sistema al entorno de operaci&#xf3;n "/>
<node COLOR="#111111" CREATED="1425669008317" ID="ID_246137934" MODIFIED="1425669009675" TEXT="IAS 4: Carga de datos al entorno de operaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425669025212" ID="ID_388427312" MODIFIED="1455875964816" TEXT="IAS 5: Pruebas de implantaci&#xf3;n del sistema">
<icon BUILTIN="messagebox_warning"/>
</node>
<node COLOR="#111111" CREATED="1425669036834" ID="ID_764393456" MODIFIED="1524417458420" TEXT="IAS 6: Pruebas de aceptaci&#xf3;n del sistema">
<icon BUILTIN="messagebox_warning"/>
<node COLOR="#111111" CREATED="1519145080791" ID="ID_1651514071" MODIFIED="1519145103636" TEXT="Las pruebas las realizan los usuarios expertos">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node COLOR="#111111" CREATED="1425669052512" ID="ID_1664263746" MODIFIED="1425669053963" TEXT="IAS 7: Preparaci&#xf3;n del mantenimiento del sistema"/>
<node COLOR="#111111" CREATED="1425669064478" ID="ID_1953020228" MODIFIED="1425669066006" TEXT="IAS 8: Establecimiento del acuerdo de nivel de servicio"/>
<node COLOR="#111111" CREATED="1425669081357" ID="ID_343807581" MODIFIED="1425669082667" TEXT="IAS 9: Presentaci&#xf3;n y aprobaci&#xf3;n del sistema"/>
<node COLOR="#111111" CREATED="1425669092776" ID="ID_225992254" MODIFIED="1425669095194" TEXT="IAS 10: Paso a producci&#xf3;n"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1425647548235" ID="ID_1478246629" MODIFIED="1524417551791" TEXT="MANTENIMIENTO DE SISTEMAS DE INFORMACI&#xd3;N">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425669608090" ID="ID_1065712876" MODIFIED="1524417554012" TEXT="Objetivo">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425669626140" ID="ID_499016956" MODIFIED="1425669627388" TEXT="obtener una nueva versi&#xf3;n de un sistema de informaci&#xf3;n preexistente, al cual se le aplican una serie de modificaciones o nuevas necesidades identificadas por los usuarios"/>
</node>
<node COLOR="#990000" CREATED="1425669636155" ID="ID_323346442" MODIFIED="1524417596351" TEXT="Visi&#xf3;n General">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425669657886" ID="ID_302871214" MODIFIED="1425669659243" TEXT="la fase m&#xe1;s larga de la vida de un sistema de informaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425669685763" ID="ID_340197755" MODIFIED="1524417604860" TEXT="M&#xe9;trica define los siguientes tipos de mantenimiento, aunque s&#xf3;lo trata los dos primeros">
<icon BUILTIN="messagebox_warning"/>
<node COLOR="#111111" CREATED="1425669699928" ID="ID_312500013" MODIFIED="1524417618849" TEXT="Correctivo">
<node COLOR="#111111" CREATED="1425669798131" ID="ID_211843756" MODIFIED="1425669803155" TEXT="cambios precisos para corregir errores del producto software"/>
</node>
<node COLOR="#111111" CREATED="1425669709023" ID="ID_588782067" MODIFIED="1524417622538" TEXT="Evolutivo">
<node COLOR="#111111" CREATED="1425669823279" ID="ID_1330451159" MODIFIED="1425669824854" TEXT="son las incorporaciones, modificaciones y eliminaciones necesarias en un producto software para cubrir la expansi&#xf3;n o cambio en las necesidades del usuario"/>
</node>
<node COLOR="#111111" CREATED="1425669736011" ID="ID_221143019" MODIFIED="1524417632541" TEXT="Adaptativo">
<node COLOR="#111111" CREATED="1425669900889" ID="ID_1821137431" MODIFIED="1425669902574" TEXT="son las modificaciones que afectan a los entornos en los que el sistema opera, por ejemplo, cambios de configuraci&#xf3;n del hardware, software de base, gestores de base de datos, comunicaciones, etc."/>
</node>
<node COLOR="#111111" CREATED="1425669746260" ID="ID_1057908307" MODIFIED="1524417666727" TEXT="Perfectivo">
<node COLOR="#111111" CREATED="1425669919188" ID="ID_1643868898" MODIFIED="1524417675908" TEXT="son las acciones llevadas a cabo para mejorar la calidad interna de los sistemas en cualquiera de sus aspectos">
<node COLOR="#111111" CREATED="1425669932869" ID="ID_895335687" MODIFIED="1425669934288" TEXT="reestructuraci&#xf3;n del c&#xf3;digo, definici&#xf3;n m&#xe1;s clara del sistema y optimizaci&#xf3;n del rendimiento y eficiencia."/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1483377659254" ID="ID_935944159" MODIFIED="1483377701751">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <strong>La asignaci&#243;n de la petici&#243;n es l</strong>a tarea del Mantenimiento de sistemas de informaci&#243;n en METRICA V3 en la que se determina el tipo de mantenimiento requerido por la petici&#243;n asignada
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node COLOR="#990000" CREATED="1425669952899" ID="ID_714312131" MODIFIED="1524417706080" TEXT="Actividades">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425670009327" ID="ID_156377620" MODIFIED="1425670023429">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Fotos%2086/MSI.png" />
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425670038811" ID="ID_895323030" MODIFIED="1425670040246" TEXT="MSI 1: Registro de la Petici&#xf3;n"/>
<node COLOR="#111111" CREATED="1425670050339" ID="ID_1018277599" MODIFIED="1425670051821" TEXT="MSI 2: An&#xe1;lisis de la Petici&#xf3;n"/>
<node COLOR="#111111" CREATED="1425670062148" ID="ID_562979421" MODIFIED="1425670063412" TEXT="MSI 3: Preparaci&#xf3;n de la Implementaci&#xf3;n de la Modificaci&#xf3;n"/>
<node COLOR="#111111" CREATED="1425670073209" ID="ID_369442858" MODIFIED="1425670074503" TEXT="MSI 4: Seguimiento y Evaluaci&#xf3;n de los Cambios hasta la Aceptaci&#xf3;n"/>
</node>
<node COLOR="#990000" CREATED="1455886283656" ID="ID_1325473420" MODIFIED="1455886293246" TEXT="Pruebas de Regresi&#xf3;n">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1425670126576" ID="ID_320687337" MODIFIED="1524484492378" POSITION="right" TEXT="Interfaces Metrica 3">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1425937938794" ID="ID_1425222079" MODIFIED="1524484511771" TEXT="Gesti&#xf3;n de Proyectos (GP)">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425938001565" ID="ID_1284294644" MODIFIED="1524485123094" TEXT="Finalidad principal ">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425938070627" ID="ID_426002814" MODIFIED="1425938122547" TEXT="Planificaci&#xf3;n temporal">
<arrowlink DESTINATION="ID_1132165210" ENDARROW="Default" ENDINCLINATION="476;0;" ID="Arrow_ID_296884834" STARTARROW="None" STARTINCLINATION="476;0;"/>
</node>
<node COLOR="#111111" CREATED="1425938085459" ID="ID_1117462306" MODIFIED="1425938120537" TEXT="Estimaci&#xf3;n de los recursos">
<arrowlink DESTINATION="ID_1132165210" ENDARROW="Default" ENDINCLINATION="445;0;" ID="Arrow_ID_1986539508" STARTARROW="None" STARTINCLINATION="445;0;"/>
</node>
<node COLOR="#111111" CREATED="1425938096851" ID="ID_500073045" MODIFIED="1524485140720" TEXT="Seguimiento">
<node COLOR="#111111" CREATED="1425938116916" ID="ID_1132165210" MODIFIED="1425938125058" TEXT="De las tareas y de los RRHH y RRMateriales que intervienen en el desarrollo del proyecto">
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1132165210" ENDARROW="Default" ENDINCLINATION="476;0;" ID="Arrow_ID_296884834" SOURCE="ID_426002814" STARTARROW="None" STARTINCLINATION="476;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1132165210" ENDARROW="Default" ENDINCLINATION="445;0;" ID="Arrow_ID_1986539508" SOURCE="ID_1117462306" STARTARROW="None" STARTINCLINATION="445;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1132165210" ENDARROW="Default" ENDINCLINATION="556;0;" ID="Arrow_ID_684560544" SOURCE="ID_1975221418" STARTARROW="None" STARTINCLINATION="556;0;"/>
</node>
</node>
<node COLOR="#111111" CREATED="1425938106675" ID="ID_1975221418" MODIFIED="1425938125058" TEXT="Control">
<arrowlink DESTINATION="ID_1132165210" ENDARROW="Default" ENDINCLINATION="556;0;" ID="Arrow_ID_684560544" STARTARROW="None" STARTINCLINATION="556;0;"/>
</node>
</node>
<node COLOR="#990000" CREATED="1425938024590" ID="ID_774255366" MODIFIED="1524485176915" TEXT="Como consecuencia de este control ">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425938142136" ID="ID_962276036" MODIFIED="1425938144006" TEXT="es posible conocer en todo momento qu&#xe9; problemas se producen y resolverlos o paliarlos lo m&#xe1;s pronto posible, lo cual evitar&#xe1; desviaciones temporales y econ&#xf3;micas">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1425938159109" ID="ID_1247208462" MODIFIED="1524485209259" TEXT="Grupos de actividades:">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="messagebox_warning"/>
<node COLOR="#111111" CREATED="1425938173821" ID="ID_194660675" MODIFIED="1524485268829" TEXT="Actividades de Inicio del Proyecto (GPI)">
<node COLOR="#111111" CREATED="1425938220598" ID="ID_904584403" MODIFIED="1425938220598" TEXT="Al principio del proyecto, al concluir el proceso Estudio de Viabilidad del Sistema, se realizar&#xe1;n las actividades de Estimaci&#xf3;n de Esfuerzo y Planificaci&#xf3;n del proyecto"/>
</node>
<node COLOR="#111111" CREATED="1425938181602" ID="ID_1518581280" MODIFIED="1524485335655" TEXT="Actividades de Seguimiento y Control (GPS)">
<node COLOR="#111111" CREATED="1425938230180" ID="ID_1414739438" MODIFIED="1425938267427" TEXT="Comprenden desde la asignaci&#xf3;n de las tareas hasta su aceptaci&#xf3;n interna por parte del equipo de proyecto, incluyendo la gesti&#xf3;n de incidencias y cambios en los requisitos que puedan presentarse y afectar a la planificaci&#xf3;n del proyecto. "/>
<node COLOR="#111111" CREATED="1425938267437" ID="ID_1230095353" MODIFIED="1425938267437" TEXT="El Seguimiento y Control del proyecto se realizan durante los procesos de An&#xe1;lisis, Dise&#xf1;o, Construcci&#xf3;n, Implantaci&#xf3;n y Aceptaci&#xf3;n, y Mantenimiento del Sistema de Informaci&#xf3;n, para vigilar el correcto desarrollo de las actividades y tareas establecidas en la planificaci&#xf3;n"/>
</node>
<node COLOR="#111111" CREATED="1425938187674" ID="ID_1510549705" MODIFIED="1524485555042" TEXT="Actividades de Finalizaci&#xf3;n del Proyecto (GPF)">
<node COLOR="#111111" CREATED="1425938305875" ID="ID_1797528108" MODIFIED="1425938305875" TEXT="al concluir el proyecto se realizan las tareas propias de Cierre del Proyecto y Registro de la Documentaci&#xf3;n de Gesti&#xf3;n"/>
</node>
</node>
<node COLOR="#990000" CREATED="1425938342985" ID="ID_1694987241" MODIFIED="1425938344065" TEXT="Estas actividades pueden requerir, en funci&#xf3;n de la complejidad del proyecto, el soporte de herramientas comerciales de gesti&#xf3;n de proyectos">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425938428790" ID="ID_312928327" MODIFIED="1425938441683">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Fotos%2086/GP%20astic%2038.png" />
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1425937945354" ID="ID_893133227" MODIFIED="1524485670885" TEXT="Seguridad (SEG)">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425938755880" ID="ID_1209841289" MODIFIED="1524485678191" TEXT="El objetivo">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425938778484" ID="ID_1164286538" MODIFIED="1425938778484" TEXT="incorporar en los sistemas de informaci&#xf3;n mecanismos de seguridad adicionales a los que se proponen en la propia metodolog&#xed;a, asegurando el desarrollo de cualquier tipo de sistema a lo largo de los procesos que se realicen para su obtenci&#xf3;n"/>
</node>
<node COLOR="#990000" CREATED="1425938862885" ID="ID_1343163283" MODIFIED="1524485742686" TEXT="contempla dos tipos de actividades diferenciadas">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425938875099" ID="ID_843418659" MODIFIED="1524485765171" TEXT="Actividades relacionadas con la seguridad intr&#xed;nseca del sistema de informaci&#xf3;n">
<node COLOR="#111111" CREATED="1425939052073" ID="ID_384980138" MODIFIED="1425939061884" TEXT="tareas (SSI)"/>
</node>
<node COLOR="#111111" CREATED="1425938888251" ID="ID_1675596631" MODIFIED="1524485771496" TEXT="Actividades que velan por la seguridad del propio proceso de desarrollo del sistema de informaci&#xf3;n">
<node COLOR="#111111" CREATED="1425939040971" ID="ID_442152443" MODIFIED="1425939040971" TEXT="(tareas SPD)"/>
</node>
</node>
<node COLOR="#990000" CREATED="1425938923237" ID="ID_1756742713" MODIFIED="1425938924357" TEXT="Las actividades relativas a la seguridad se desarrollan en paralelo con los procesos principales de M&#xe9;trica versi&#xf3;n 3">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425938953670" ID="ID_1858269073" MODIFIED="1524485836588" TEXT="permite la comunicaci&#xf3;n entre las tareas de desarrollo y las tareas de an&#xe1;lisis y gesti&#xf3;n de riesgos">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425938978334" ID="ID_1011274773" MODIFIED="1425938978344" TEXT="interrelaci&#xf3;n continua entre el equipo de desarrollo y el equipo de seguridad que, a trav&#xe9;s de la interfaz de seguridad, van cerrando cada etapa de M&#xe9;trica"/>
</node>
<node COLOR="#990000" CREATED="1425939152635" ID="ID_1270700417" MODIFIED="1425939153835" TEXT="hace posible incorporar durante la fase de desarrollo las funciones y mecanismos que refuerzan la seguridad del nuevo sistema y del propio proceso de desarrollo, asegurando su consistencia y seguridad.">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1425937952286" ID="ID_510631062" MODIFIED="1524486014804" TEXT="Aseguramiento de la Calidad (CAL)">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425939409930" ID="ID_1332602006" MODIFIED="1425939409930" TEXT="Se propone sustituir el PGGC por esta interfaz que plantea un marco com&#xfa;n que sirva de referencia para la definici&#xf3;n y puesta en marcha de planes espec&#xed;ficos de garant&#xed;a de calidad aplicables a proyectos concretos (ISO9000, ISO15504 SPICE, EFQM)">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425939521560" ID="ID_1109952466" MODIFIED="1524486042461" TEXT="Calidad">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425939533632" ID="ID_1666499292" MODIFIED="1425939533632" TEXT="&#x201c;grado en que un conjunto de caracter&#xed;sticas inherentes cumple con unos requisitos&#x201d; [ISO 9000:2000]"/>
</node>
<node COLOR="#990000" CREATED="1425939419501" ID="ID_1258204413" MODIFIED="1524486052623" TEXT="Objetivo">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425939438161" ID="ID_112837162" MODIFIED="1425939438161" TEXT="proporcionar un marco com&#xfa;n de referencia para la definici&#xf3;n y puesta en marcha de planes espec&#xed;ficos de aseguramiento de calidad aplicables a proyectos concretos"/>
</node>
<node COLOR="#990000" CREATED="1425939500413" ID="ID_1042734482" MODIFIED="1425939502243" TEXT="pretende dar confianza en que el producto re&#xfa;ne las caracter&#xed;sticas necesarias para satisfacer todos los requisitos del Sistema de Informaci&#xf3;n.">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425939547575" ID="ID_1088249588" MODIFIED="1524486126308" TEXT="El equipo de calidad deber&#xe1; realizar actividades que servir&#xe1;n para">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425939558338" ID="ID_1701942285" MODIFIED="1425939558338" TEXT="Reducir, eliminar y prevenir las deficiencias de calidad de los productos a obtener"/>
<node COLOR="#111111" CREATED="1425939570160" ID="ID_387962154" MODIFIED="1425939570160" TEXT="Alcanzar una razonable confianza en prestaciones y servicios esperados."/>
</node>
<node COLOR="#990000" CREATED="1425940088029" ID="ID_1459453552" MODIFIED="1524486189917" TEXT="Para conseguir estos objetivos">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425940094430" ID="ID_171194269" MODIFIED="1425940096500" TEXT="desarrollar un plan de aseguramiento de calidad espec&#xed;fico que se aplicar&#xe1; durante la planificaci&#xf3;n del proyecto de acuerdo a la estrategia de desarrollo adoptada en la gesti&#xf3;n del proyecto.  ">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1425940109133" ID="ID_1511198384" MODIFIED="1524486252736" TEXT="En el plan de aseguramiento de calidad se reflejan">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425940124436" ID="ID_305675220" MODIFIED="1425940124436" TEXT="las actividades de calidad a realizar (normales o extraordinarias),"/>
<node COLOR="#111111" CREATED="1425940124446" ID="ID_482883660" MODIFIED="1425940124446" TEXT="los est&#xe1;ndares a aplicar,"/>
<node COLOR="#111111" CREATED="1425940124446" ID="ID_1286437056" MODIFIED="1425940124446" TEXT="los productos a revisar,"/>
<node COLOR="#111111" CREATED="1425940124446" ID="ID_1891841524" MODIFIED="1425940124446" TEXT="los procedimientos"/>
<node COLOR="#111111" CREATED="1425940124446" ID="ID_1347396224" MODIFIED="1425940124446" TEXT="la normativa para informar de los defectos detectados a sus responsables y realizar el seguimiento de los mismos hasta su correcci&#xf3;n."/>
</node>
<node COLOR="#990000" CREATED="1425940147750" ID="ID_1577255734" MODIFIED="1524486458916" TEXT="El grupo de aseguramiento de calidad ">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425940153490" ID="ID_869078423" MODIFIED="1425940162682" TEXT="participa en la revisi&#xf3;n de los productos seleccionados para determinar si son conformes o no a los procedimientos, normas o criterios especificados">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1425940162682" ID="ID_31472426" MODIFIED="1425940162682" TEXT="totalmente independientes del equipo de desarrollo">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1425940217493" ID="ID_158717455" MODIFIED="1425940220623" TEXT="El establecimiento del plan de aseguramiento de calidad comienza en el Estudio de Viabilidad del Sistema">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1447798654690" ID="ID_1630634093" MODIFIED="1447798657216" TEXT="El Plan de Aseguramiento de la  Calidad se aplica a lo largo de todo el desarrollo, en los procesos de An&#xe1;lisis, Dise&#xf1;o, Construcci&#xf3;n, Implantaci&#xf3;n y Aceptaci&#xf3;n del Sistema y en su posterior Mantenimiento.">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1425937958218" ID="ID_432454965" MODIFIED="1524486580994" TEXT="Gesti&#xf3;n de la Configuraci&#xf3;n (GC)">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1425940280689" ID="ID_328270131" MODIFIED="1524486614739" TEXT="El objetivo">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425940503802" ID="ID_1513597672" MODIFIED="1425940503802" TEXT="mantener la integridad de los productos que se obtienen a lo largo del desarrollo"/>
<node COLOR="#111111" CREATED="1425940514584" ID="ID_1438717860" MODIFIED="1425940514584" TEXT="garantizar que no se realizan cambios incontrolados"/>
<node COLOR="#111111" CREATED="1425940525347" ID="ID_1871030338" MODIFIED="1425940525347" TEXT="garantizar que todos los participantes disponen de la versi&#xf3;n adecuada de los productos que manejan."/>
</node>
<node COLOR="#990000" CREATED="1425940563038" ID="ID_883652511" MODIFIED="1524497596076" TEXT="Elementos de configuraci&#xf3;n software">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425940573731" ID="ID_1054048726" MODIFIED="1425940573741" TEXT="ejecutables y c&#xf3;digo fuente modelos de datos"/>
<node COLOR="#111111" CREATED="1425940573741" ID="ID_1867417155" MODIFIED="1425940595832">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      modelos de procesos
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425940595832" ID="ID_1696566466" MODIFIED="1425940601861">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      especificaciones de requisitos
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425940601871" ID="ID_1130538607" MODIFIED="1425940601871">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      pruebas
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425940573741" ID="ID_414264184" MODIFIED="1425940573741" TEXT="etc.."/>
</node>
<node COLOR="#990000" CREATED="1425940620548" ID="ID_1316866975" MODIFIED="1425940620548" TEXT="se realiza durante todas las actividades asociadas al desarrollo del sistema, y hasta que deja de utilizarse.">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1425940640155" ID="ID_274114709" MODIFIED="1524497624605" TEXT="facilita el mantenimiento del sistema, ">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425940644805" ID="ID_490472714" MODIFIED="1425940660568" TEXT="aportando informaci&#xf3;n precisa para valorar el impacto de los cambios solicitados ">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1425940658737" ID="ID_37131618" MODIFIED="1425940664348" TEXT="reduciendo el tiempo de implementaci&#xf3;n de un cambio, tanto evolutivo como correctiv">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1425940679775" ID="ID_1475843788" MODIFIED="1524497663595" TEXT="Permite controlar el sistema como producto global a lo largo de su desarrollo ">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425940689287" ID="ID_1295765759" MODIFIED="1425940700769">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Generar informes sobre el estado de desarrollo&#160;&#160;
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1425940700769" ID="ID_97212907" MODIFIED="1425940700779">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reducir el n&#250;mero de errores de adaptaci&#243;n del sistema,
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node COLOR="#990000" CREATED="1425940735178" ID="ID_1160499918" MODIFIED="1524497675462" TEXT="permite definir las necesidades de gesti&#xf3;n de configuraci&#xf3;n para cada sistema de informaci&#xf3;n">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1425940745150" ID="ID_760210639" MODIFIED="1425940748470" TEXT="recogi&#xe9;ndolas en un plan de gesti&#xf3;n de configuraci&#xf3;n, en el que se especifican las actividades de identificaci&#xf3;n y registro de productos en el sistema de gesti&#xf3;n de configuraci&#xf3;n">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</map>
