<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1370240213531" ID="ID_79376443" MODIFIED="1379619590260" TEXT="M&#xe9;trica. CSI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370240240806" FOLDED="true" ID="ID_1674582982" MODIFIED="1372839806526" POSITION="right" TEXT="Preparaci&#xf3;n del Entorno de Generaci&#xf3;n y Construcci&#xf3;n">
<node CREATED="1370240270331" ID="ID_1446455503" MODIFIED="1370240300939" TEXT="Implantaci&#xf3;n de la Base de Datos F&#xed;sica o Ficheros"/>
<node CREATED="1370240302770" ID="ID_1883983162" MODIFIED="1370240319224" TEXT="Preparaci&#xf3;n del Entorno de Construcci&#xf3;n"/>
</node>
<node CREATED="1370240326265" FOLDED="true" ID="ID_29259927" MODIFIED="1372839995851" POSITION="right" TEXT="Generaci&#xf3;n del c&#xf3;digo de los componentes y procedimientos">
<icon BUILTIN="gohome"/>
<node CREATED="1370240351906" ID="ID_47893165" MODIFIED="1370240371905" TEXT="Generaci&#xf3;n del C&#xf3;digo de Componentes"/>
<node CREATED="1370240374844" ID="ID_1812109761" MODIFIED="1370240407793" TEXT="Generaci&#xf3;n del C&#xf3;digo de los Procedimientos de Operaci&#xf3;n y Seguridad"/>
</node>
<node CREATED="1372839816708" ID="ID_979724356" MODIFIED="1372840039551" POSITION="right" TEXT="Pruebas">
<cloud/>
<icon BUILTIN="button_ok"/>
<node CREATED="1370240411465" FOLDED="true" ID="ID_1685156363" MODIFIED="1384961103402" TEXT="Ejecuci&#xf3;n de las pruebas unitarias">
<icon BUILTIN="button_ok"/>
<node CREATED="1370240424017" ID="ID_1345467009" MODIFIED="1370240440709" TEXT="Preparaci&#xf3;n del Entorno de Pruebas Unitarias"/>
<node CREATED="1370240441058" ID="ID_1934547560" MODIFIED="1370240460061" TEXT="Realizaci&#xf3;n y evaluaci&#xf3;n de las pruebas Unitarias"/>
</node>
<node CREATED="1370240465043" FOLDED="true" ID="ID_915312278" MODIFIED="1378219765758" TEXT="Ejecuci&#xf3;n de las pruebas de integraci&#xf3;n">
<icon BUILTIN="button_ok"/>
<node CREATED="1370240476035" ID="ID_911645954" MODIFIED="1370240499857" TEXT="Preparaci&#xf3;n del Entorno de las Pruebas de Integraci&#xf3;n"/>
<node CREATED="1370240500143" ID="ID_396110913" MODIFIED="1370240515075" TEXT="Realizaci&#xf3;n de las Pruebas de Integraci&#xf3;n"/>
<node CREATED="1370240515361" ID="ID_73835479" MODIFIED="1370240549449" TEXT="Evaluaci&#xf3;n del Resultado de las Pruebas de Integraci&#xf3;n"/>
</node>
<node CREATED="1370240553901" FOLDED="true" ID="ID_1844252992" MODIFIED="1378219766538" TEXT="Ejecuci&#xf3;n de las pruebas del sistema">
<icon BUILTIN="button_ok"/>
<node CREATED="1370240585780" ID="ID_1681203860" MODIFIED="1370240606592" TEXT="Preparaci&#xf3;n del Entorno de las Pruebas del Sistema"/>
<node CREATED="1370240608173" ID="ID_1054707983" MODIFIED="1370240626747" TEXT="Realizaci&#xf3;n de las Pruebas del Sistema"/>
<node CREATED="1370240619663" ID="ID_1038819657" MODIFIED="1370240660553" TEXT="Evaluaci&#xf3;n del Resultado de las Pruebas del Sistema"/>
</node>
</node>
<node CREATED="1372839864219" ID="ID_210486154" MODIFIED="1372840026707" POSITION="right" TEXT="Usuarios">
<cloud/>
<icon BUILTIN="group"/>
<node CREATED="1370240663413" ID="ID_1709040108" MODIFIED="1372840018134" TEXT="Elaboraci&#xf3;n de los manuales de usuario">
<icon BUILTIN="desktop_new"/>
</node>
<node CREATED="1370240680316" FOLDED="true" ID="ID_1538812131" MODIFIED="1372840063804" TEXT="Definici&#xf3;n de la formaci&#xf3;n de los usuarios finales">
<icon BUILTIN="edit"/>
<node CREATED="1370240708419" ID="ID_1405135464" MODIFIED="1370240732492" TEXT="Definici&#xf3;n del Esquema de Formaci&#xf3;n"/>
<node CREATED="1370240732810" ID="ID_1223838486" MODIFIED="1370240752733" TEXT="Especificaci&#xf3;n de los Recursos y Entornos de Formaci&#xf3;n"/>
</node>
</node>
<node CREATED="1370240758137" FOLDED="true" ID="ID_1903733329" MODIFIED="1372840268063" POSITION="right" TEXT="Construcci&#xf3;n de los componentes y procedimientos de migraci&#xf3;n y carga inicial de datos">
<icon BUILTIN="gohome"/>
<icon BUILTIN="launch"/>
<node CREATED="1370240796255" ID="ID_245928424" MODIFIED="1370240806631" TEXT="Preparaci&#xf3;n del entorno de Migraci&#xf3;n"/>
<node CREATED="1370240807074" ID="ID_1571426278" MODIFIED="1370240853268" TEXT="Generaci&#xf3;n del c&#xf3;digo de los Componentes y Procedimientos de Migraci&#xf3;n y Carga Inicial de datos"/>
<node CREATED="1370240855723" ID="ID_1991083744" MODIFIED="1370240894818" TEXT="Realizaci&#xf3;n y Evaluaci&#xf3;n de las Pruebas de Migraci&#xf3;n y Carga Inicial de Datos"/>
</node>
<node CREATED="1370240905401" ID="ID_376993129" MODIFIED="1372840001582" POSITION="right" TEXT="Aprobaci&#xf3;n del Sistema de Informaci&#xf3;n">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1370336962704" ID="ID_1982100320" MODIFIED="1370336966310" POSITION="left" TEXT="Productos">
<node CREATED="1370337902600" ID="ID_523573365" MODIFIED="1370337928670" TEXT="De CSI">
<cloud/>
<node CREATED="1370337266749" ID="ID_885386632" MODIFIED="1381072282904" STYLE="bubble" TEXT="Codificaci&#xf3;n">
<node CREATED="1370337147128" ID="ID_1852484077" MODIFIED="1372841985333" TEXT="Base de datos f&#xed;sica"/>
<node CREATED="1370337128993" ID="ID_1939931319" MODIFIED="1372841985333" TEXT="C&#xf3;digo fuente de los componentes"/>
</node>
<node CREATED="1370337245213" ID="ID_297589037" MODIFIED="1376652428569" STYLE="bubble" TEXT="Resultado de pruebas">
<node CREATED="1370337197235" ID="ID_1953380117" MODIFIED="1372841985333" TEXT="Resultado de pruebas unitarias">
<icon BUILTIN="list"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1370337210004" ID="ID_986626817" MODIFIED="1372841985333" TEXT="Resultado de pruebas de integraci&#xf3;n">
<icon BUILTIN="korn"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1370337224363" ID="ID_722664749" MODIFIED="1372841985333" TEXT="Resultado de pruebas del sistema">
<icon BUILTIN="gohome"/>
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1370337367018" ID="ID_1908669280" MODIFIED="1372841985343" TEXT="Pruebas de Migraci&#xf3;n">
<icon BUILTIN="help"/>
</node>
</node>
<node CREATED="1370337315241" ID="ID_1648594673" MODIFIED="1372841985343" STYLE="bubble" TEXT="Manuales de usuario">
<icon BUILTIN="desktop_new"/>
<icon BUILTIN="male2"/>
</node>
</node>
<node CREATED="1370337828305" ID="ID_1017063157" MODIFIED="1370337831895" TEXT="Para IAS">
<node CREATED="1370337335701" FOLDED="true" ID="ID_1071983625" MODIFIED="1376652187148" TEXT="DEFINICION de la Formaci&#xf3;n">
<icon BUILTIN="down"/>
<node CREATED="1370337383094" ID="ID_1019683173" MODIFIED="1370337385623" TEXT="Esquema"/>
<node CREATED="1370337386097" ID="ID_1124471135" MODIFIED="1370337450059" TEXT="Materiales y Entorno"/>
</node>
<node CREATED="1372840505875" ID="ID_1245364132" MODIFIED="1372840534363" TEXT="El PLAN DE FORMACION se elabora en el IAS">
<icon BUILTIN="yes"/>
</node>
<node CREATED="1370337494533" ID="ID_986898403" MODIFIED="1376652183910" STYLE="bubble" TEXT="Migraci&#xf3;n">
<node CREATED="1370337498864" ID="ID_937430478" MODIFIED="1372841833275" TEXT="Componentes"/>
<node CREATED="1370337504159" ID="ID_557700521" MODIFIED="1372841833275" TEXT="Procedimientos"/>
</node>
</node>
</node>
<node COLOR="#ff3333" CREATED="1372775938186" ID="ID_1186986743" MODIFIED="1372840201249" POSITION="left" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="CSI.png" />
  </body>
</html></richcontent>
</node>
</node>
</map>
