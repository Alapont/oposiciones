<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1370207740306" ID="ID_1454609413" MODIFIED="1370207755095" TEXT="Metrica. EVS">
<node CREATED="1370207850495" ID="ID_134346753" MODIFIED="1372838546923" POSITION="right" TEXT="Establecimiento del alcance del sistema">
<icon BUILTIN="full-1"/>
<node CREATED="1370329411705" ID="ID_249570958" MODIFIED="1370329414686" TEXT="Tareas">
<node CREATED="1370207869506" ID="ID_1175050693" MODIFIED="1370207879770" TEXT="Estudio de la solicitud"/>
<node CREATED="1370207880977" ID="ID_1172059802" MODIFIED="1370207893241" TEXT="Identificaci&#xf3;n del alcance del Sistema"/>
<node CREATED="1370207893528" ID="ID_928753887" MODIFIED="1370207903389" TEXT="Especificaci&#xf3;n del alcance del EVS"/>
</node>
</node>
<node CREATED="1370207907981" FOLDED="true" ID="ID_994742808" MODIFIED="1372838546252" POSITION="right" TEXT="Estudio de la situaci&#xf3;n actual">
<icon BUILTIN="full-2"/>
<icon BUILTIN="xmag"/>
<node CREATED="1370207920611" ID="ID_1322916736" MODIFIED="1370207943886" TEXT="Valoraci&#xf3;n del estudio de la situaci&#xf3;n actual"/>
<node CREATED="1370207944282" ID="ID_834218429" MODIFIED="1372766653552" TEXT="Identificaci&#xf3;n de los usuarios participantes en el estudio de la situaci&#xf3;n actual"/>
<node CREATED="1370207976645" ID="ID_1747393733" MODIFIED="1372766660735" TEXT="Identificaci&#xf3;n de los SI existentes"/>
<node CREATED="1370208005528" ID="ID_1188329898" MODIFIED="1370208021567" TEXT="Realizaci&#xf3;n del diagn&#xf3;stico de la situaci&#xf3;n actual"/>
</node>
<node CREATED="1370208041743" FOLDED="true" ID="ID_703892520" MODIFIED="1372838547833" POSITION="right" TEXT="Definici&#xf3;n de requisitos del sistema">
<icon BUILTIN="full-3"/>
<icon BUILTIN="help"/>
<node CREATED="1370208056370" ID="ID_474057861" MODIFIED="1370208077150" TEXT="Identificaci&#xf3;n de las Directrices T&#xe9;cnicas y de Gesti&#xf3;n"/>
<node CREATED="1370208078762" ID="ID_1517930184" MODIFIED="1370208086159" TEXT="Identificaci&#xf3;n de Requisitos"/>
<node CREATED="1370208086414" ID="ID_1473305196" MODIFIED="1370208093187" TEXT="Catalogaci&#xf3;n de Requisitos"/>
</node>
<node CREATED="1370208098418" FOLDED="true" ID="ID_193194092" MODIFIED="1372834407771" POSITION="right" TEXT="Estudio de alternativas de soluci&#xf3;n">
<icon BUILTIN="full-4"/>
<icon BUILTIN="list"/>
<node CREATED="1370208120391" ID="ID_1320625267" MODIFIED="1370208135353" TEXT="Preselecci&#xf3;n de Alternativas de Soluci&#xf3;n"/>
<node CREATED="1370208135749" ID="ID_1599642679" MODIFIED="1370208159338" TEXT="Descripci&#xf3;n de las Alternativas de Soluci&#xf3;n"/>
</node>
<node CREATED="1370208173150" FOLDED="true" ID="ID_1614977043" MODIFIED="1372834412612" POSITION="right" TEXT="Valoraci&#xf3;n de las alternativas">
<icon BUILTIN="full-5"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1370208184702" ID="ID_575657630" MODIFIED="1370208196576" TEXT="Estudio de la Inversi&#xf3;n"/>
<node CREATED="1370208197502" ID="ID_43034140" MODIFIED="1370208203151" TEXT="Estudio de los Riesgos"/>
<node CREATED="1370208203563" ID="ID_1969029169" MODIFIED="1370208211879" TEXT="Planificaci&#xf3;n de Alternativas"/>
</node>
<node CREATED="1370208234178" FOLDED="true" ID="ID_785151051" MODIFIED="1372834418583" POSITION="right" TEXT="Selecci&#xf3;n de la soluci&#xf3;n">
<icon BUILTIN="full-6"/>
<icon BUILTIN="edit"/>
<node CREATED="1370208242471" ID="ID_1473037475" MODIFIED="1370208258227" TEXT="Convocatoria de Presentaci&#xf3;n"/>
<node CREATED="1370208258685" ID="ID_767593696" MODIFIED="1370208276346" TEXT="Evaluaci&#xf3;n de Alternativas y Selecci&#xf3;n"/>
<node CREATED="1370208276758" ID="ID_111480612" MODIFIED="1370208283281" TEXT="Aprobaci&#xf3;n de la soluci&#xf3;n"/>
</node>
<node CREATED="1370329423350" ID="ID_338601171" MODIFIED="1370329439709" POSITION="left" TEXT="Productos">
<node CREATED="1370329439709" ID="ID_1189768566" MODIFIED="1372834252561" STYLE="bubble" TEXT="Descripci&#xf3;n del Sistema"/>
<node CREATED="1370329540537" ID="ID_1562287046" MODIFIED="1372834252561" STYLE="bubble" TEXT="Descripci&#xf3;n de la Situaci&#xf3;n"/>
<node CREATED="1370329451469" ID="ID_333240688" MODIFIED="1372834252561" STYLE="bubble" TEXT="Cat&#xe1;logo de Requisitos"/>
<node CREATED="1370329580590" ID="ID_1157014473" MODIFIED="1372834252561" STYLE="bubble" TEXT="Alternativas de Soluci&#xf3;n"/>
</node>
<node CREATED="1372772918773" ID="ID_1388509927" MODIFIED="1372834173895" POSITION="left">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="ASI.png" />
  </body>
</html></richcontent>
</node>
</node>
</map>
