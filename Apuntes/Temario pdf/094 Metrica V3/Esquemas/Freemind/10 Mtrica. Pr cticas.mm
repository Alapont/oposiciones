<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1377853203578" ID="ID_406068515" MODIFIED="1377853340842" TEXT="M&#xe9;trica. Pr&#xe1;cticas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370341172881" ID="ID_1044558722" MODIFIED="1377853229443" POSITION="right" TEXT="Pr&#xe1;cticas">
<node CREATED="1370344073116" ID="ID_1445167554" MODIFIED="1377853248327" TEXT="An&#xe1;lisis de Impacto. Elementos implicados en las peticiones de cambio.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370344090459" ID="ID_885906813" MODIFIED="1370344102146" TEXT="Catalogaci&#xf3;n"/>
<node CREATED="1370344102573" ID="ID_1053403082" MODIFIED="1370344109610" TEXT="C&#xe1;lculo de Accesos"/>
<node CREATED="1370344140270" ID="ID_1010923817" MODIFIED="1370344145498" TEXT="Caminos de Acceso"/>
<node CREATED="1370344176985" ID="ID_1060950593" MODIFIED="1370344186222" TEXT="Diagrama de Representaci&#xf3;n"/>
<node CREATED="1370344186509" ID="ID_1372155019" MODIFIED="1377853244288" TEXT="Factores Cr&#xed;ticos de &#xc9;xito">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370344210150" ID="ID_446868686" MODIFIED="1370344220012" TEXT="Impacto en la Organizaci&#xf3;n"/>
<node CREATED="1370344237458" ID="ID_1545397242" MODIFIED="1370344241392" TEXT="Presentaciones"/>
<node CREATED="1370344241647" ID="ID_515376094" MODIFIED="1384952485242" TEXT="Prototipado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370344247240" ID="ID_1065910747" MODIFIED="1377853255551" TEXT="Maquetas de interfaces">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370344257606" ID="ID_1969584320" MODIFIED="1377853257591" TEXT="Identificaci&#xf3;n de los usuarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1370344271778" ID="ID_1047495954" MODIFIED="1377853259407" TEXT="Analizar las funciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
</node>
<node CREATED="1370344281552" ID="ID_896314841" MODIFIED="1384952492700" TEXT="Pruebas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370344294882" FOLDED="true" ID="ID_1936921761" MODIFIED="1383570812229" TEXT="Pruebas unitarias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370344300929" ID="ID_4145602" MODIFIED="1372845259127" TEXT="Pruebas estructurales o de caja blanca"/>
<node CREATED="1370344307097" ID="ID_149063565" MODIFIED="1372845268778" TEXT="Pruebas funcionales o de caja negra"/>
</node>
<node CREATED="1370344321012" FOLDED="true" ID="ID_843819698" MODIFIED="1384952533464" TEXT="Pruebas de integraci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370344331068" ID="ID_825337681" MODIFIED="1370344349102" TEXT="De arriba abajo  (top-down)">
<icon BUILTIN="down"/>
</node>
<node CREATED="1370344351058" ID="ID_1480693283" MODIFIED="1370344370201" TEXT="De abajo arriba (bottom-up)">
<icon BUILTIN="up"/>
</node>
<node CREATED="1370344377188" ID="ID_578838653" MODIFIED="1370344387221" TEXT="Estrategias combinadas"/>
</node>
<node CREATED="1370344373186" FOLDED="true" ID="ID_997064080" MODIFIED="1384952929595" TEXT="Pruebas del sistema">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370344417758" ID="ID_1243457170" MODIFIED="1377853266702" TEXT="Rendimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372783156158" ID="ID_43096468" MODIFIED="1377853268798" TEXT="Funcionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372783160287" ID="ID_1104090663" MODIFIED="1377853271286" TEXT="de Comunicaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370344425049" ID="ID_517340500" MODIFIED="1377853273118" TEXT="Volumen">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370344427615" ID="ID_1689829609" MODIFIED="1377853275438" TEXT="Sobrecarga">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370344431242" ID="ID_1819033937" MODIFIED="1377853277429" TEXT="Disponibilidad de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370344441546" ID="ID_1693727596" MODIFIED="1377853279221" TEXT="Facilidad de uso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370344447544" ID="ID_1776780098" MODIFIED="1377853281229" TEXT="Operaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370344451233" ID="ID_967171677" MODIFIED="1377853283301" TEXT="Entorno">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370344454502" ID="ID_613259051" MODIFIED="1377853285501" TEXT="Seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370344464369" FOLDED="true" ID="ID_1680368900" MODIFIED="1384952526584" TEXT="Pruebas de Implantaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370344489102" ID="ID_1396714495" MODIFIED="1383570865343" TEXT="En el entorno real">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372845416502" ID="ID_666584337" MODIFIED="1383570867854" TEXT="Requisitos no funcionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370344502823" FOLDED="true" ID="ID_1888065900" MODIFIED="1384952538409" TEXT="Pruebas de Aceptaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370344515654" ID="ID_1827919561" MODIFIED="1370344525796" TEXT="Validaci&#xf3;n"/>
<node CREATED="1370344526114" ID="ID_19690821" MODIFIED="1370344542527" TEXT="Definidas por el usuario"/>
<node CREATED="1370344543125" ID="ID_1306174674" MODIFIED="1370344551770" TEXT="Preparadas por el equipo de desarrollo"/>
</node>
<node CREATED="1370344577672" FOLDED="true" ID="ID_1391678163" MODIFIED="1384952539922" TEXT="Pruebas de Regresi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370344587197" ID="ID_1218695630" MODIFIED="1377853310828" TEXT="Eliminar el efecto onda">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370344625579" ID="ID_999596635" MODIFIED="1377853313292" TEXT="Cada vez que se hace un cambio en el sistema">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370344654229" ID="ID_999963762" MODIFIED="1370344664074" TEXT="Repetici&#xf3;n de pruebas ya realizadas"/>
</node>
</node>
<node CREATED="1370344675000" ID="ID_735747115" MODIFIED="1384952937599" TEXT="Revisi&#xf3;n formal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370344686802" ID="ID_616590304" MODIFIED="1377853308172" TEXT="Producto intermedio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370344731137" ID="ID_344648899" MODIFIED="1377853318411" TEXT="Proceso riguroso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370344738492" ID="ID_872790767" MODIFIED="1370344745171" TEXT="Productos de importancia"/>
</node>
<node CREATED="1370344704001" ID="ID_873830691" MODIFIED="1384952952624" TEXT="Revisi&#xf3;n t&#xe9;cnica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370344753585" ID="ID_1432996176" MODIFIED="1370344758221" TEXT="Producto intermedio"/>
<node CREATED="1372845560973" ID="ID_514985724" MODIFIED="1372845565383" TEXT="Participantes">
<node CREATED="1372845565383" ID="ID_66208041" MODIFIED="1377853322771" TEXT="Jefe de proyecto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372845570886" ID="ID_177851570" MODIFIED="1377853325299" TEXT="Responsable del grupo de aseguramiento de la calidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1370344793872" ID="ID_1434538578" MODIFIED="1384952550140" TEXT="Sesiones de Trabajo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370344815096" FOLDED="true" ID="ID_121671131" MODIFIED="1384953058753" TEXT="JAD (Joint Application Design)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370344892808" ID="ID_754115305" MODIFIED="1370344899019" TEXT="Reducir tiempo de desarrollo"/>
<node CREATED="1370344899399" ID="ID_1708982925" MODIFIED="1370344905953" TEXT="Implicaci&#xf3;n de los usuarios"/>
<node CREATED="1370344923711" ID="ID_715027939" MODIFIED="1370859187895" TEXT="Actividades">
<node CREATED="1370344929743" ID="ID_761108801" MODIFIED="1370344932286" TEXT="Inicio"/>
<node CREATED="1370344932588" ID="ID_1487512007" MODIFIED="1370344937597" TEXT="Desarrollo"/>
<node CREATED="1370344937868" ID="ID_734693045" MODIFIED="1370344942223" TEXT="Finalizaci&#xf3;n"/>
</node>
</node>
<node CREATED="1370344818006" FOLDED="true" ID="ID_1260489700" MODIFIED="1384953059625" TEXT="JRP (Joint Requirements Planning)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370344953788" ID="ID_1926707070" MODIFIED="1370344967144" TEXT="Implicaci&#xf3;n alta direcci&#xf3;n"/>
<node CREATED="1370344973858" ID="ID_178246744" MODIFIED="1370344981972" TEXT="Mejores resultados en menos tiempo"/>
<node CREATED="1370345004660" ID="ID_382235479" MODIFIED="1370859189209" TEXT="Actividades">
<node CREATED="1370345014543" ID="ID_622672919" MODIFIED="1370345017852" TEXT="Iniciaci&#xf3;n"/>
<node CREATED="1370345018497" ID="ID_240757419" MODIFIED="1370345022649" TEXT="B&#xfa;squeda"/>
<node CREATED="1370345022920" ID="ID_188363883" MODIFIED="1370345032609" TEXT="Preparaci&#xf3;n"/>
<node CREATED="1370345032865" ID="ID_1479492702" MODIFIED="1370345038467" TEXT="Realizaci&#xf3;n"/>
<node CREATED="1370345038785" ID="ID_909089500" MODIFIED="1370345042640" TEXT="Finalizaci&#xf3;n"/>
</node>
</node>
<node CREATED="1370344867824" ID="ID_1185164149" MODIFIED="1383998630080" TEXT="Entrevistas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372845809591" ID="ID_1419700540" MODIFIED="1383998634978" TEXT="Reuniones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</map>
