<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1370241723696" ID="ID_20594093" MODIFIED="1378220287225" TEXT="M&#xe9;trica. MSI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370241741486" ID="ID_39357267" MODIFIED="1381074142333" POSITION="right" TEXT="Registro de la Petici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372851498377" ID="ID_1809286654" MODIFIED="1383588696679" TEXT="Registro de la petici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372851509124" ID="ID_551344727" MODIFIED="1383588698660" TEXT="Asignaci&#xf3;n de la petici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370241764239" ID="ID_187205126" MODIFIED="1381074143592" POSITION="right" TEXT="An&#xe1;lisis de la Petici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372851576590" ID="ID_306586775" MODIFIED="1383588700876" TEXT="Verificaci&#xf3;n y Estudio de la petici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372851589293" ID="ID_955051816" MODIFIED="1383588702654" TEXT="Estudio de propuesta de la soluci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370241778380" ID="ID_522324101" MODIFIED="1381074145138" POSITION="right" TEXT="Preparaci&#xf3;n de la Implementaci&#xf3;n de la Modificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372851606837" ID="ID_1367673393" MODIFIED="1383588705010" TEXT="Identificaci&#xf3;n de los Elementos afectados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372851621392" ID="ID_653546000" MODIFIED="1383588706460" TEXT="Establecimiento del Plan de Acci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372851630815" ID="ID_1579808286" MODIFIED="1383588708067" TEXT="Especificaci&#xf3;n del Plan de Pruebas de Regresi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370241791757" ID="ID_186958360" MODIFIED="1381074146587" POSITION="right" TEXT="Seguimiento y Evaluaci&#xf3;n de los Cambios hasta la Aceptaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372851682376" ID="ID_393907145" MODIFIED="1383588710142" TEXT="Seguimiento de los Cambios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372851692775" ID="ID_477768170" MODIFIED="1378220302154" TEXT="Realizaci&#xf3;n de las Pruebas de regresi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372851705203" ID="ID_807319961" MODIFIED="1378220305228" TEXT="Aprobaci&#xf3;n y Cierre de la Petici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370341216912" ID="ID_1607745615" MODIFIED="1376653368499" POSITION="left" TEXT="Tipos">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      METRICA define estos cuatro tipos de mantenimiento, pero s&#243;lo trata los dos primeros.
    </p>
  </body>
</html></richcontent>
<node BACKGROUND_COLOR="#ff9999" CREATED="1370341219980" ID="ID_1836851277" MODIFIED="1376653423890" STYLE="bubble" TEXT="Correctivo"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1370341234618" ID="ID_555976426" MODIFIED="1376653429585" STYLE="bubble" TEXT="Evolutivo. Cambios o eliminaciones en las necesidades del usuario"/>
<node CREATED="1370341238511" ID="ID_1958998130" MODIFIED="1370341242272" TEXT="Perfectivo"/>
<node CREATED="1370341242528" ID="ID_1734096689" MODIFIED="1374739692260" TEXT="Adaptativo. Cambios en el entorno en que opera el sistema."/>
</node>
<node CREATED="1372841890159" ID="ID_947467797" MODIFIED="1372841913854" POSITION="left" TEXT="Recibimos el Plan de Mantenimiento y SLAs de IAS"/>
</node>
</map>
