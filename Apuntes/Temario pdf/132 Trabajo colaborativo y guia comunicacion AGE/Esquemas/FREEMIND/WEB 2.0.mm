<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1432218085048" ID="ID_139342754" MODIFIED="1432218096371" TEXT="T120 WEB 2.0">
<node CREATED="1432218281967" ID="ID_1424090012" MODIFIED="1432218291347" POSITION="right" TEXT="Evoluci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432218292496" ID="ID_1086636654" MODIFIED="1432218314922">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2001:</i>&#160;estalla burbuja tecn
    </p>
  </body>
</html></richcontent>
<node CREATED="1432218315508" ID="ID_1503290993" MODIFIED="1432218391376">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2004: </i>brainstorming&#160; <u>O'Reilly &amp; MediaLive Int </u>&#8594; registran concepto Web 2.0
    </p>
  </body>
</html></richcontent>
<node CREATED="1432218392831" ID="ID_1005126814" MODIFIED="1432218436562">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2005:</i>&#160;ref bibliogr&#225;fica &#8594; art&#237;culo 'What is Web 2.0'
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1432218748353" ID="ID_700915097" MODIFIED="1432218866675">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Web 1.0</i>: web de <u>datos</u>&#160;y p&#225;gs est&#225;ticas html &#8594; <i>Web 1.5</i>: CMS sirve p&#225;gs din&#225;micas (Obj: visitas)
    </p>
  </body>
</html></richcontent>
<node CREATED="1432218779263" ID="ID_209351386" MODIFIED="1432218835303">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Web 2.0</i>: web de <u>usuarios</u>&#160;(Obj: interacci&#243;n y RRSS)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1432218460513" ID="ID_1886773668" MODIFIED="1449827173169" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Defs
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432218463937" ID="ID_740824083" MODIFIED="1432219039291">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Utilidades y servs de Internet sobre <u>BD modificable por usuarios</u>&#160;en contenido y/o forma de presentarlo
    </p>
  </body>
</html></richcontent>
<node CREATED="1432218537922" ID="ID_1605427846" MODIFIED="1432219073042">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Participaci&#243;n</b>: usuario genera contenidos, servs y valor
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1432218566763" ID="ID_1390463143" MODIFIED="1432218661735">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Plataforma abierta de <u>comunidades</u>&#160;de usuarios y servicios de <u>colaboraci&#243;n&#160;e intercambio</u>&#160;&#225;gil de info
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1432218667277" ID="ID_1830127934" MODIFIED="1432218719771">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Apps y p&#225;gs de Internet que usan <u>inteligencia colectiva</u>&#160;&#160;para servs interactivos en red
    </p>
  </body>
</html></richcontent>
<node CREATED="1432218703307" ID="ID_1666020974" MODIFIED="1432218711149" TEXT="Usuario controla sus datos">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1432218889571" ID="ID_805686877" MODIFIED="1449827192264">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Autoservicio</u>&#160;&#160;del cliente y gesti&#243;n de datos algor&#237;tmica para llegar a&#160; <u>toda la red</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1432218927732" ID="ID_1470301432" MODIFIED="1432218963879">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Larga cola</u>&#160;(nichos) constituye el grueso de la red
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1432218974743" ID="ID_1954152216" MODIFIED="1449827162270" TEXT="Ppio: servicio mejora autom&#xe1;ticamente cuanta +gente lo usa"/>
</node>
</node>
<node CREATED="1432219113699" ID="ID_1784209543" MODIFIED="1432221043763" POSITION="right" TEXT="Apps Web 2.0">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432219116549" ID="ID_1157795406" MODIFIED="1449827212026">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Deben permitir interacci&#243;n
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1432219125736" ID="ID_731579938" MODIFIED="1432219166762">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>De</i>&#160;<i>contenidos</i>&#160;<i>contributiva </i>: usuario puede a&#241;adir y compartir info
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1432219167859" ID="ID_1245390919" MODIFIED="1449826465377">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>De contenidos combinatoria </i>: interrelaci&#243;n de contenidos de distintas BD (mashup)
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1432219192306" ID="ID_1358807535" MODIFIED="1432219207627">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>De interfaz</i>: preferencias est&#233;ticas o funciones
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1432219209398" ID="ID_929805457" MODIFIED="1432219238902">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Generativa</i>: el s&#170; decide c&#243;mo/qu&#233; datos presentar tras analizar comportamiento de usuario
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1432219273348" ID="ID_1687216806" MODIFIED="1449826961073" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ppios de O'Reilly
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432219367099" ID="ID_1386650759" MODIFIED="1449826942314">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>WWW como plataforma</i>: sw gratuito <u>en la Web</u>&#160;y no en el ord
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1432219388648" ID="ID_1541939500" MODIFIED="1449826950083">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Aprovechar <u>inteligencia colectiva</u></i>: usuario tratado como co-desarrollador
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1432219413344" ID="ID_1138266969" MODIFIED="1449826966258">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Gesti&#243;n</i>&#160;<i>de</i>&#160;<i>BD</i>: lo valioso de las apps son los datos ('Intel Inside'), usuario los&#160; <u>puede manipular</u>
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1432219463979" ID="ID_1131541103" MODIFIED="1449826981107">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Fin de ciclo de actualizs de sw </i>: sw como servicio (no prod),&#160; <u>fase beta</u>&#160;permanente (pruebas y modifs)
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1432219540582" ID="ID_321756701" MODIFIED="1432219739544">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Pgmaci&#243;n ligera</i>: apps no centralizadas y escalables, reutiliz, servs <u>mashups</u>&#160;(apps web h&#237;bridas)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1432219587229" ID="ID_1292489559" MODIFIED="1432219755084">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>No limitado a 1 solo disp </i>: <u>sindicaci&#243;n</u>&#8594;etiquetado de contenids para distrib autom en distintas platafs
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1432219684960" ID="ID_1349005647" MODIFIED="1449826990484">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Experiencias enriquecedoras del usuario</i>:&#160;interfaces sencillas de <u>acceso</u>&#160; en todo lugar/momento
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1432219792245" ID="ID_1619478175" MODIFIED="1449827012101" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Herrams de
    </p>
    <p>
      trabajo colab
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432219799768" ID="ID_329327597" MODIFIED="1432219807107" TEXT="Social Networking (RRSS)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432219899691" ID="ID_1935864856" MODIFIED="1449827132940" TEXT="Creaci&#xf3;n de espacios virtuales que promuevan comunidades e instancias de intercambio social">
<node CREATED="1432219933769" ID="ID_179797298" MODIFIED="1432219963262" TEXT="Fb, Tw, Ig, LIn..."/>
</node>
</node>
<node CREATED="1432219807482" ID="ID_215218929" MODIFIED="1432219811692" TEXT="Contenidos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432219973465" ID="ID_1607819315" MODIFIED="1449827131522" TEXT="Lectura y escritura en l&#xed;nea, distribuci&#xf3;n e intercambio">
<node CREATED="1432220132179" ID="ID_1423572316" MODIFIED="1432220191019">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Generados por Usuario</i>: sin conocimientos especiales&#8594; 'periodismo ciudadano'
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1432220032084" ID="ID_153764909" MODIFIED="1432220235098">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Sw de Weblogs</i>: crear y adm blogs (Wordpress, Blogger, Drupal)
    </p>
  </body>
</html></richcontent>
<node CREATED="1432220071874" ID="ID_1954926476" MODIFIED="1432220093996">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Trackback/pingback</i>: autor de blog notifica a otro que le ha referenciado
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1432220094675" ID="ID_422531599" MODIFIED="1449827023043">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Permalink</i>: link <u>no modificable</u>&#160;aunq la entrada se mueva a otro sitio de la BD
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1432219813075" ID="ID_948930153" MODIFIED="1449826485514">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Org Social e
    </p>
    <p>
      Intelig de Info
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432220424842" ID="ID_513684575" MODIFIED="1449827129146" TEXT="Etiquetar, sindicar e indexar para facilitar orden y almacen de info">
<icon BUILTIN="xmag"/>
<node CREATED="1432220483453" ID="ID_418800797" MODIFIED="1449827140772">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Folksonom&#237;a</i>: taxonom&#237;a&#160;por usuarios (libre decisi&#243;n)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1432220576877" ID="ID_598645836" MODIFIED="1449826835618">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Web</b>&#160;<b>sem&#225;ntica</b>
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432220629415" ID="ID_1802793529" MODIFIED="1432220645321">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estructura info similar a los humanos en el cerebro, req interop &#8594;&#160;est&#225;ndares de metadatos
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1432220673048" ID="ID_1609318689" MODIFIED="1432220767127">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Orientada a que <u>procesadores de info</u>&#160;entiendan lengs de metadatos (OWL)
    </p>
  </body>
</html></richcontent>
<node CREATED="1432220659835" ID="ID_481197365" MODIFIED="1449826823949" TEXT="Diferente a Web 2.0: orientada al usuario">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1432220844531" ID="ID_1540238917" MODIFIED="1449826836911">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Sindicaci&#243;n</i>
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1449826490611" ID="ID_1367115351" MODIFIED="1449827107367">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>RSS:</i>&#160;(Really Simple Sindic) formato XML para&#160;compartir&#160; <u>contenido</u>&#160;web&#8594;&#160;agregadores (pe Feedly)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1449826797448" ID="ID_1231011679" MODIFIED="1449826803617">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Atom: </i>mejora RSS
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1449826518080" ID="ID_382867016" MODIFIED="1449826673780">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>RDF: </i>(Resource Descr Framew, W3C) sintaxis XML para intercambiar <u>grafos</u>&#160;q describen recs en la web
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1449826612672" ID="ID_187369830" MODIFIED="1449827049242">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modelo metadatos
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1449826619623" ID="ID_826743951" MODIFIED="1449826735003">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>OWL:</i>&#160;(Onthology Web Lang) leng XML para compartir&#160;&#160; <u>ontolog&#237;as</u>. Sobre RDF
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1449826713631" ID="ID_1130283189" MODIFIED="1449826727086" TEXT="Versiones: Lite&#x2192; DL&#x2192; Full (+expresivo)"/>
</node>
<node CREATED="1449826735755" ID="ID_1529398646" MODIFIED="1449827070250">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>SPARQL: </i>(SPARQ RDF Queries Lang)&#160;leng de&#160; <u>consulta de grafos</u>&#160;&#160;RDF
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1449826780329" ID="ID_1873897076" MODIFIED="1449826790490">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>RDF Schema:</i>&#160;extensi&#243;n sem&#225;ntica de RDF
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1432219830334" ID="ID_1249172875" MODIFIED="1432219838555" TEXT="Apps y servicios (mashup)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432221000401" ID="ID_1239882959" MODIFIED="1449827122751">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      H&#237;brido de recursos (varias fuentes) para ofrecer servs de valor a&#241;adido
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1432221017839" ID="ID_132941880" MODIFIED="1432221074308" TEXT="Integraci&#xf3;n transparente de apps Web 2.0 (interop)"/>
</node>
</node>
</node>
<node CREATED="1432219842138" ID="ID_1143158976" MODIFIED="1449826926606" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ppios de Mobile Web 2.0
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432221184200" ID="ID_636939144" MODIFIED="1432221213431" TEXT="Interacci&#xf3;n entre disps m&#xf3;viles y apps Web 2.0">
<icon BUILTIN="xmag"/>
<node CREATED="1432221197605" ID="ID_63136436" MODIFIED="1432221208897" TEXT="Por Joakar y Fish, se suman a los de O&apos;Reilly"/>
</node>
<node CREATED="1432221214065" ID="ID_1146214258" MODIFIED="1432221368350">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contenidos personales e id, Usuario como tag y no n&#186;, Nodos globales y multileng (VoIP), Motor de convergencia dig, AJAX como pgmaci&#243;n disruptiva, Servs por ubicaci&#243;n, B&#250;squedas&#160;&#160;+eficientes
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1432221375611" ID="ID_529767898" MODIFIED="1449826910690">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>AJAX: </i>(Async JavaScript &amp; XML)&#160;tecn de desarrollo web para crear RIAs
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1449826884401" ID="ID_449050904" MODIFIED="1449826884404">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Usuario realiza cambios sin recargar p&#225;g (pe elegir opc)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1432221510698" ID="ID_1592659682" MODIFIED="1449826901539">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>RIA: </i>(Rich Internet App)&#160;usa navegador como si fuera un ord (proceso en cliente, datos en serv)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1432221637719" ID="ID_421064356" MODIFIED="1449826920218">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Gmail, Windows Live, Java Applet, HTML5
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</map>
