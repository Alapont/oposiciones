<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1427809926111" ID="ID_1991655636" MODIFIED="1480692673140">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      T 29&#160;&#160;HERRAMIENTAS DE DIRECCI&#211;N TIC
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1427811277448" ID="ID_412532881" MODIFIED="1480692621218" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      FUNCIONES
    </p>
    <p>
      DE DIRECCI&#211;N TIC
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427811292662" ID="ID_1847854119" MODIFIED="1427811588172" TEXT="Planificaci&#xf3;n estrat&#xe9;gica">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427811578368" ID="ID_1244756119" MODIFIED="1427812557693" TEXT="Funci&#xf3;n principal (objs a largo plazo)">
<node CREATED="1427811459461" ID="ID_1278873757" MODIFIED="1427811955925" TEXT="Conocimientos t&#xe9;cnicos, interlocuci&#xf3;n, planes alternativos y eval, plan de acci&#xf3;n, pgms por objetivo "/>
</node>
</node>
<node CREATED="1427811315273" ID="ID_1264552119" MODIFIED="1427811390388" TEXT="Gesti&#xf3;n de recursos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427811561429" ID="ID_1031093360" MODIFIED="1427811576988" TEXT="Urgente y de mayor dedicaci&#xf3;n">
<node CREATED="1427811589753" ID="ID_1393234465" MODIFIED="1427811964901" TEXT="Equipo motivado y aut&#xf3;nomo (evitando escalar), intelig emocional, gesti&#xf3;n con proveedores "/>
</node>
</node>
<node CREATED="1427811319096" ID="ID_1549179111" MODIFIED="1427811390757" TEXT="Gesti&#xf3;n del cambio">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427811666540" ID="ID_699827039" MODIFIED="1427811680348" TEXT="Necesidad de modernizaci&#xf3;n de la AGE">
<node CREATED="1427812009112" ID="ID_571670520" MODIFIED="1427815833512" TEXT="Visi&#xf3;n global de la org para nuevas soluciones organizativas orientadas al ciudadano"/>
<node CREATED="1427811721093" ID="ID_1438372" MODIFIED="1427811876122" TEXT="Modelo de Cambio Quest: preparaci&#xf3;n&#x2192; compromiso&#x2192; mejora &#x2192; revisi&#xf3;n "/>
</node>
</node>
</node>
<node CREATED="1427813031560" ID="ID_1479122545" MODIFIED="1480692506703" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      HERRAMIENTAS
    </p>
    <p>
      DE DIRECCI&#211;N TIC
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1480689799281" ID="ID_342514588" MODIFIED="1480690387671" TEXT="De evaluaci&#xf3;n tecnol&#xf3;gica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1480689932781" ID="ID_766666096" MODIFIED="1480690045218" TEXT="An&#xe1;lisis de mercado"/>
<node CREATED="1480690020781" ID="ID_1623352563" MODIFIED="1480690026031" TEXT="Benchmarking"/>
<node CREATED="1480689966031" ID="ID_1399350443" MODIFIED="1480689982421" TEXT="Vigilancia tecnol&#xf3;gica"/>
<node CREATED="1480689994265" ID="ID_162432885" MODIFIED="1480689996625" TEXT="Inteligencia competitiva"/>
<node CREATED="1480690005796" ID="ID_32665370" MODIFIED="1480690008046" TEXT="Prospectiva tecnol&#xf3;gica"/>
<node CREATED="1480690036328" ID="ID_1410112530" MODIFIED="1480690039093" TEXT=" Valoraci&#xf3;n financiera    "/>
</node>
<node CREATED="1480689886593" ID="ID_1658084609" MODIFIED="1480690381921" TEXT="De planificaci&#xf3;n estrat&#xe9;gica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1480690345484" ID="ID_1098092002" MODIFIED="1480690348234" TEXT="Roadmapping tecnol&#xf3;gico"/>
<node CREATED="1480690350781" ID="ID_893669779" MODIFIED="1480690360296" TEXT="Matrices de decisi&#xf3;n "/>
<node CREATED="1480690362609" ID="ID_1962692960" MODIFIED="1480690371578" TEXT="An&#xe1;lisis de portfolio"/>
<node CREATED="1480690510656" ID="ID_1558564263" MODIFIED="1480691698796">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CUADRO DE MANDO
    </p>
    <p>
      (David Norton y Robert Kaplan)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1480691576843" ID="ID_1102112570" MODIFIED="1480691882843" TEXT="Caracter&#xed;sticas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1480691886546" ID="ID_481809944" MODIFIED="1480691916796" TEXT="Documento peri&#xf3;dico que sintetiza variables clave de gesti&#xf3;n al nivel correspondiente."/>
<node CREATED="1480691918328" ID="ID_256003632" MODIFIED="1480691935468" TEXT="Suele estar Preparado / Definido por Control de gesti&#xf3;n en conjunci&#xf3;n con los receptores del mismo."/>
<node CREATED="1480691936500" ID="ID_359332979" MODIFIED="1480691954500" TEXT="Concepto piramidal de la informaci&#xf3;n: a mayor nivel de recepci&#xf3;n, m&#xe1;s s&#xed;ntesis y m&#xe1;s globalidad.      "/>
<node CREATED="1480691955937" ID="ID_511926080" MODIFIED="1480691978578" TEXT="Presentaci&#xf3;n normalizada para evitar problemas de comparaci&#xf3;n y/o comprensi&#xf3;n."/>
</node>
<node CREATED="1480692335015" ID="ID_752472134" MODIFIED="1480692347718" TEXT="Conceptos b&#xe1;sicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1480692360546" ID="ID_401038997" MODIFIED="1480692366750" TEXT="Mapas estrat&#xe9;gicos"/>
<node CREATED="1480691743437" ID="ID_1590576671" MODIFIED="1480691842343" TEXT="Perspectivas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1480691753609" ID="ID_1335569318" MODIFIED="1480691753609" TEXT="">
<node CREATED="1480691755312" ID="ID_166636614" MODIFIED="1480691798343">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    Financiera
  </body>
</html></richcontent>
</node>
<node CREATED="1480691769890" ID="ID_133775045" MODIFIED="1480691808156" TEXT="Clientes"/>
<node CREATED="1480691786578" ID="ID_1047153038" MODIFIED="1480691824437" TEXT="Perspectiva de Procesos Internos"/>
<node CREATED="1480691824984" ID="ID_420929277" MODIFIED="1480691836921" TEXT="Infraestructuras (Learning &amp; Growth)"/>
</node>
</node>
<node CREATED="1480692422156" ID="ID_1188275334" MODIFIED="1480692431953" TEXT="Objetivos estrat&#xe9;gicos"/>
<node CREATED="1480692432609" ID="ID_1280705119" MODIFIED="1480692446937" TEXT="Indicadores estrat&#xe9;gicos"/>
<node CREATED="1480692448312" ID="ID_290679807" MODIFIED="1480692450125" TEXT="Metas"/>
<node CREATED="1480692450593" ID="ID_1946087207" MODIFIED="1480692459671" TEXT="Responsables"/>
<node CREATED="1480692460500" ID="ID_1530188265" MODIFIED="1480692470328" TEXT="Proyectos estrat&#xe9;gicos"/>
</node>
<node CREATED="1480692065578" ID="ID_649516130" MODIFIED="1480692072843" TEXT="Ventajas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1480692075421" ID="ID_1790255627" MODIFIED="1480692102046" TEXT="Visi&#xf3;n Integral de la Organizaci&#xf3;n en diferentes perspectivas"/>
<node CREATED="1480692103828" ID="ID_442767862" MODIFIED="1480692151406" TEXT="Permite conocer la situaci&#xf3;n actual de la empresa, al dar respuesta a algunas preguntas clave como: &#xbf;Cu&#xe1;l es nuestro punto de partida? &#xbf;Cu&#xe1;les son nuestros puntos m&#xe1;s fuertes? &#xbf;Y los m&#xe1;s d&#xe9;biles? ..."/>
<node CREATED="1480692153843" ID="ID_661865430" MODIFIED="1480692176390" TEXT="Facilita el alineamiento organizativo y la priorizaci&#xf3;n estrat&#xe9;gica, y por tanto, la flexibilidad en entornos de cambio."/>
<node CREATED="1480692177578" ID="ID_1208660439" MODIFIED="1480692193546" TEXT="Influye en el comportamiento de las personas clave, alineando su actuaci&#xf3;n hacia la consecuci&#xf3;n de unos objetivos que surgen de un proceso colectivo en el que han participado y con el que se comprometen mediante la asunci&#xf3;n de responsabilidades en la obtenci&#xf3;n de unas metas concretas."/>
</node>
</node>
</node>
<node CREATED="1480689899546" ID="ID_562713853" MODIFIED="1480690394578" TEXT="De implementaci&#xf3;n tecnol&#xf3;gica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1480690399203" ID="ID_2333393" MODIFIED="1480690412156" TEXT="Auditorias Tecnol&#xf3;gicas "/>
<node CREATED="1480690413328" ID="ID_1068050712" MODIFIED="1480690431500" TEXT="Certificaciones"/>
<node CREATED="1480690447546" ID="ID_1908926737" MODIFIED="1480690479718" TEXT="Ingenier&#xed;a y reingenier&#xed;a"/>
<node CREATED="1480690480593" ID="ID_693954490" MODIFIED="1480690489468" TEXT="Gesti&#xf3;n de proyectos"/>
</node>
</node>
</node>
</map>
