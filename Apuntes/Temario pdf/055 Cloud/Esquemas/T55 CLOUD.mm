<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1431072880100" ID="ID_1776470931" MODIFIED="1480696344750">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      T52 CLOUD
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1431073382632" ID="ID_250538927" MODIFIED="1431073497894" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Paradigma que permite ofrecer <u>servs&#160;de computaci&#243;n</u>&#160;(config y compartidos) a usuarios por&#160; <u>Internet</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1431073407382" ID="ID_1732113984" MODIFIED="1431073466651" TEXT="Respuesta r&#xe1;pida, flexible y adaptativa seg&#xfa;n necesidades (pago por consumo)"/>
</node>
<node CREATED="1431073511194" ID="ID_834974406" MODIFIED="1431073519934" POSITION="right" TEXT="Estructura">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431073520346" ID="ID_1247864167" MODIFIED="1431073536689" TEXT="Consumidor de Servs">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431073550783" ID="ID_1827446968" MODIFIED="1431073730704" TEXT="Org, particular o SI que consume una instancia de un servicio cloud ">
<node CREATED="1431074985396" ID="ID_1069167221" MODIFIED="1435739830882">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Herrams de Integraci&#243;n</i>: servs con resto de su infraestr TIC (propia u otros servs cloud)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1431073537254" ID="ID_943257241" MODIFIED="1431073542161" TEXT="Creador de Servs">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431073627083" ID="ID_459072208" MODIFIED="1431073644104" TEXT="Dise&#xf1;a, implementa y mantiene artefactos de ejec y gesti&#xf3;n de un serv cloud">
<node CREATED="1431074991231" ID="ID_1023844193" MODIFIED="1435739810280">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Herrams de Desarrollo:</i>&#160;de apps, BD, plantillas MVs... de una plataforma de PaaS
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1431073542503" ID="ID_1123286377" MODIFIED="1431073547120" TEXT="Proveedor de Servs">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431073762880" ID="ID_808287446" MODIFIED="1431073858953" TEXT="Gesti&#xf3;n de plataforma de servs y los expone a Consumidores">
<node CREATED="1431075000448" ID="ID_1348843041" MODIFIED="1435739866087">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Infr f&#237;sica: </i>servs,&#160;almacenam, red, CPD y edificios
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1431075027371" ID="ID_1255316264" MODIFIED="1435739957636">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Servs: </i>(PaaS, SaaS, BPaaS, IaaS) sobre capa de<b>virtualizaci&#243;n</b>&#160;(escalab y abstrac de recursos)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1431074322816" ID="ID_833045282" MODIFIED="1431074689252" TEXT="Plataforma Gesti&#xf3;n de Servs">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431074580581" ID="ID_1690689889" MODIFIED="1431074744884">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Gesti&#243;n Operacional de Recursos:</i>&#160;correcta implementaci&#243;n de servs
    </p>
  </body>
</html></richcontent>
<node CREATED="1431074609969" ID="ID_1228974794" MODIFIED="1431074748476" TEXT="Aprovis autom, orquestaci&#xf3;n, gesti&#xf3;n, monitoriz "/>
</node>
<node CREATED="1431074682803" ID="ID_702327094" MODIFIED="1431074733929">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Gesti&#243;n de Procesos de Negocio</i>: entrega seg&#250;n SLA contratado
    </p>
  </body>
</html></richcontent>
<node CREATED="1431074707226" ID="ID_1109602036" MODIFIED="1431074722536" TEXT="Medici&#xf3;n de consumo, facturaci&#xf3;n, contabilidad"/>
</node>
</node>
</node>
</node>
<node CREATED="1431074887935" ID="ID_331237459" MODIFIED="1431075087057" POSITION="right" TEXT="Agentes intervinientes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431074898648" ID="ID_1617242007" MODIFIED="1435740041348">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Proveedor:</i>&#160;presta servs en la nube a
    </p>
  </body>
</html></richcontent>
<node CREATED="1431074913038" ID="ID_1728705389" MODIFIED="1435740015303">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Suscriptor: </i>usuario de servs cloud
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1431074924534" ID="ID_122395360" MODIFIED="1435740003025">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Intermediario: </i>entre usuarios y proveedores (pe HP usa CPDs de Espa&#241;a)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1431074946998" ID="ID_41420157" MODIFIED="1435740054609">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Habilitador:</i>&#160;venden elementos sw-hw necesarios para el desarrollo y prestaci&#243;n
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1431074960823" ID="ID_1569370527" MODIFIED="1435740062208">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Auditor: </i>verifica cumplimiento de SLA y seguridad
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1431075087846" ID="ID_172961257" MODIFIED="1431075186106" POSITION="right" TEXT="Familias de servicio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431075097727" ID="ID_766529417" MODIFIED="1431075110316" TEXT="SW (SaaS)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431075310465" ID="ID_1782650178" MODIFIED="1431075337629" TEXT="Operaci&#xf3;n, soporte y mantenim de una app que usar&#xe1; el cliente por tiempo contratado">
<node CREATED="1431075347199" ID="ID_1258747297" MODIFIED="1431075359942" TEXT="App reside en proveedor"/>
</node>
<node CREATED="1431075389604" ID="ID_949468354" MODIFIED="1431075788837" TEXT="Niveles de madurez">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431075400148" ID="ID_257446502" MODIFIED="1435740091405">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>1:</i>&#160;cliente tiene <u>su propia versi&#243;n</u>&#160;personaliz de la app
    </p>
  </body>
</html></richcontent>
<node CREATED="1431075420132" ID="ID_1616872064" MODIFIED="1435740101206">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2:</i>&#160;proveedor aloja <u>una instancia indep por cada app</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1431075438019" ID="ID_413032925" MODIFIED="1435740155703">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>3: </i>prov aloja <u>una sola instancia,</u>&#160;clientes separados por pol&#237;ticas de autoriz
    </p>
  </body>
</html></richcontent>
<node CREATED="1431075514215" ID="ID_735022266" MODIFIED="1435740164201">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>4: </i>prov <u>agrupa</u>&#160;clientes en granja con <u>eq de carga</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1431075110626" ID="ID_1872312946" MODIFIED="1431075116507" TEXT="Plataforma (PaaS)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431075569666" ID="ID_1393571148" MODIFIED="1431078204594" TEXT="APIs, hw y sw para desplegar apps y servs web (desarrolladores) sin preocuparse del mantenim">
<node CREATED="1431075636833" ID="ID_695780291" MODIFIED="1431075650444" TEXT="Cada PaaS usa un leng de pgm, entorno y librer&#xed;as"/>
</node>
</node>
<node CREATED="1431075116857" ID="ID_680792849" MODIFIED="1431075124955" TEXT="Infraestructura (IaaS)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431075673064" ID="ID_716558238" MODIFIED="1431075749486" TEXT="Infraestructura de computaci&#xf3;n (recursos IT) y gesti&#xf3;n bajo demanda">
<node CREATED="1431075711971" ID="ID_679465420" MODIFIED="1431075734064" TEXT="Capa+baja y compleja de pir&#xe1;mide (virtualizaci&#xf3;n)"/>
</node>
<node CREATED="1431075946081" ID="ID_697567264" MODIFIED="1431075957963" TEXT="Comunics o Red (CaaS, NaaS)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431075958465" ID="ID_359708198" MODIFIED="1431078242919" TEXT="Conectividad de tx y red entre nubes&#x2192; VPN, BoD (BW), Red m&#xf3;vil virtualizada"/>
</node>
</node>
<node CREATED="1435739635350" ID="ID_943266155" MODIFIED="1435739662186" TEXT="Cuanto +abajo en la pir&#xe1;mide, +resp del cliente">
<icon BUILTIN="yes"/>
<node CREATED="1435739672643" ID="ID_15258535" MODIFIED="1435739736885">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>SaaS (cliente): </i>prot datos, gesti&#243;n id y autentic
    </p>
  </body>
</html></richcontent>
<node CREATED="1435739700373" ID="ID_701648569" MODIFIED="1435739730956">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>IaaS:</i>&#160;clientes deben asegurar sus apps desplegadas en la nube
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1431075125370" ID="ID_1590550015" MODIFIED="1435740940588" TEXT="Proceso de Negocio (BPaaS)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431076017898" ID="ID_1698208510" MODIFIED="1431076047299" TEXT="Gesti&#xf3;n externa en Internet de un proceso de negocio (pe pasarelas de pago)"/>
</node>
</node>
<node CREATED="1431076129820" ID="ID_1315568383" MODIFIED="1431076513043" POSITION="right" TEXT="Tipos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431076154724" ID="ID_995730299" MODIFIED="1435740789694">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Privada
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431076516696" ID="ID_1665593517" MODIFIED="1435740655018">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Un solo cliente</u>&#160;&#160;preocupado por seg y prot datos
    </p>
  </body>
</html></richcontent>
<node CREATED="1431076565807" ID="ID_140655320" MODIFIED="1435740833695">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Control, pol&#237;tica int de seg, recs locales, r&#225;pida puesta en serv, servs a sedes
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1431076607665" ID="ID_1855153776" MODIFIED="1431076666245" TEXT="Elevada inversi&#xf3;n y ROI lento">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node CREATED="1431076682953" ID="ID_1193782042" MODIFIED="1435740793459">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      P&#250;blica
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431076687538" ID="ID_1662748350" MODIFIED="1435740704600">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Recs <u>compartidos</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1431076718745" ID="ID_612400026" MODIFIED="1435740758818">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No inversi&#243;n y ROI r&#225;pido, escalab, externaliz, sols estandar, flexib recs
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1431076992481" ID="ID_155461246" MODIFIED="1435740774634">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lenta puesta en serv, dif&#237;cil cumplir pol&#237;tics int, compart no transp, dep d prov
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node CREATED="1431077108184" ID="ID_1344406336" MODIFIED="1435740797655">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      H&#237;brida
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431077231737" ID="ID_87973686" MODIFIED="1431077259825" TEXT="Combina necesidad de nube priv (autoprov de servs cr&#xed;ticos) con flex y escalab de apps no cr&#xed;ticas">
<node CREATED="1431077287786" ID="ID_1203385179" MODIFIED="1431077326861" TEXT="Compleja integraci&#xf3;n y decidir c&#xf3;mo distribuir las apps (deben ser indep)">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node CREATED="1431077329482" ID="ID_1716821657" MODIFIED="1435740801440">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comunitaria
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431077333002" ID="ID_493505285" MODIFIED="1435740875158">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Alianza</u>&#160;de orgs para implantar infraestr con objs similares y marco de seg/priv com&#250;n
    </p>
  </body>
</html></richcontent>
<node CREATED="1431077392042" ID="ID_1022839443" MODIFIED="1435740860077">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pol&#237;tica int de seg, -costes, r&#225;pido ROI
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1431077404171" ID="ID_271671914" MODIFIED="1435740867814">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Seg depende de anfitri&#243;n de la infaestr
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
</node>
<node CREATED="1431077434394" ID="ID_1073521887" MODIFIED="1431077436628" POSITION="right" TEXT="Ventajas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431077491361" ID="ID_217091889" MODIFIED="1431077603116" TEXT="Puesta en marcha del serv">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431077496312" ID="ID_1313994978" MODIFIED="1431077594551" TEXT="Inmediatez (navegador), no inversi&#xf3;n inicial, comodidad, amplia oferta, empresa se centra en negocio"/>
</node>
<node CREATED="1431077600051" ID="ID_664350703" MODIFIED="1431077603453" TEXT="Econ&#xf3;micas">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431077604259" ID="ID_1633813587" MODIFIED="1435739223254" TEXT="On-demand, redimensio, aprov din&#xe1;mico, ahorro en rrhh mantenim espacio y electric, competividad">
<node CREATED="1435739251996" ID="ID_1834383939" MODIFIED="1435739255183" TEXT="Escalabilidad">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1431077714568" ID="ID_196770245" MODIFIED="1431077720225" TEXT="Simplicidad de uso y operaci&#xf3;n">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431077720616" ID="ID_1690029725" MODIFIED="1431077772065" TEXT="Accesib (pc+internet), mantenim remoto, dispo, actualizaciones, f&#xe1;cil prescindir del serv"/>
</node>
</node>
<node CREATED="1431077437882" ID="ID_608920515" MODIFIED="1431077440683" POSITION="right" TEXT="Desventajas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431077781980" ID="ID_3142072" MODIFIED="1435739333663">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Dependencia de comunics, dispo (preocup por SLA), clientes cautivos de proveedor (vendor&#160;<u>lock-in </u>)
    </p>
  </body>
</html></richcontent>
<node CREATED="1431077866986" ID="ID_532277144" MODIFIED="1435739306198">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sols
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431077872641" ID="ID_971281279" MODIFIED="1431078136236">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estandarizar APIs y objetos, pol&#237;tica de bakcups, Servs de Custodia de C&#243;digo
    </p>
    <p>
      &#160;(recup c&#243;d fuente en caso de cese), garantizar soporte al s&#170;
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1435739262761" ID="ID_1103146221" MODIFIED="1435739265027" TEXT="Riesgos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1435739265480" ID="ID_1548440165" MODIFIED="1435739494664">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      -gobernanza dl cliente, cumplim ley y medidas seg (certif), acceso a interfaz de gesti&#243;n, fallo de aislam
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1435739520616" ID="ID_1339525234" MODIFIED="1435739607193">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Vulnerabs:&#160;</i>accesos no autoriz (AAA), baja entrop&#237;a de claves entre MVs
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1435740401700" ID="ID_2866515" MODIFIED="1435740406270" POSITION="right" TEXT="Programaci&#xf3;n de apps">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1435740410574" ID="ID_1719081473" MODIFIED="1435740924876" TEXT="Escalab y dispo">
<icon BUILTIN="xmag"/>
<node CREATED="1435740420430" ID="ID_1732095729" MODIFIED="1435740628564">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Dise&#241;o <u>sin estados</u>,&#160;cada componente escalable, accesible (gu&#237;as OWASP), paraleliz, recs on-demand
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1435740406597" ID="ID_1501404600" MODIFIED="1435741807053" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Proveedores
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1435741806925" ID="ID_996608480" MODIFIED="1435741825942">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sols priv
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1435741106221" ID="ID_871807842" MODIFIED="1435741530998">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Amazon WS:<b>&#160; </b></i><u>IaaS</u>, AMI (plantilla/imagen de MV), hiperv Xen. EC2 (Elastic Computing), S3 (Storage)
    </p>
  </body>
</html></richcontent>
<node CREATED="1435741280362" ID="ID_1485342785" MODIFIED="1435741447946">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Windows Azure:&#160;<b>&#160;</b></i><u>PaaS</u>, VHD (MV), hiperv Azure
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1435741601382" ID="ID_1171958384" MODIFIED="1435741890993">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Google App Engine:&#160;</i><u>PaaS</u>, esc autom, no soporta Windows, sandbox en SO abiertos, hiperv Xen/Kvm
    </p>
  </body>
</html></richcontent>
<node CREATED="1435741698784" ID="ID_208289995" MODIFIED="1435742237795">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>VMWare vCloud Suite:</i>&#160;<u>IaaS</u>, hiperv VMWare, muy flexible ( <u>cualquier SO</u>)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1435741793767" ID="ID_1540237817" MODIFIED="1435742149509">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sols abiertas (IaaS)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1435741960798" ID="ID_108281285" MODIFIED="1435742172578">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Eucalyptus (especs Amazon), Open Nebula, <u>Open Stack</u>&#160;(cualquier hiperv), Cloud Stack (Apache)
    </p>
  </body>
</html></richcontent>
<node CREATED="1435742239563" ID="ID_179860528" MODIFIED="1435742264732">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Posibles para Cloud AP:</i>&#160;VMWare y OpenStack
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
<node CREATED="1435742334255" ID="ID_1625573603" MODIFIED="1435742689835" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Normativa a provs de AP
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1435742374130" ID="ID_1578621699" MODIFIED="1435742455737">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>LOPD: </i>pgm '<u>Safe Harbor</u>' para transfs UE-EEUU, coop para dchos ARCO
    </p>
  </body>
</html></richcontent>
<node CREATED="1435742464849" ID="ID_1835611945" MODIFIED="1435742599233">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>ENS y ENI: </i>cumplirlos para evitar cautiv
    </p>
  </body>
</html></richcontent>
<node CREATED="1435742655904" ID="ID_933943281" MODIFIED="1435742709506">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>C&#243;digo penal:</i>&#160;sanciones importantes (mucha dep)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1435742485950" ID="ID_269595907" MODIFIED="1435742790724">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>TRLCSP: </i>contrato servs con <u>di&#225;log compet</u>, coste imprevisible y dif&#237;cil seg, evitar subcontrat
    </p>
  </body>
</html></richcontent>
<node CREATED="1435742619373" ID="ID_425663933" MODIFIED="1435742799595">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>LPI:</i>&#160;cl&#225;usulas espec&#237;ficas de <u>confid y no divulg</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1435742566049" ID="ID_1470804081" MODIFIED="1435742812307">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>LSSI:</i>&#160;cumplir obligs d contrt electr
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
</map>
