<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1378134578456" ID="ID_1841118917" MODIFIED="1378134837791" TEXT="ERP. CRM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378134840105" ID="ID_1979833003" MODIFIED="1384531490337" POSITION="right" TEXT="ERP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378134893547" ID="ID_1746674213" MODIFIED="1384531492287" TEXT="Definici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378134901470" ID="ID_1296628915" MODIFIED="1384425059188" TEXT="Integrados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378134905675" ID="ID_1958732673" MODIFIED="1384425061048" TEXT="Parametrizables">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1552641291190" ID="ID_800773632" MODIFIED="1552641306089" TEXT="Modularidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1552641312489" ID="ID_1074982257" MODIFIED="1552641321496" TEXT="BD centralizada ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378135090235" ID="ID_592467513" MODIFIED="1384425117353" TEXT="Tecnolog&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378135104543" ID="ID_251029982" MODIFIED="1384531498293" TEXT="Son sistemas transaccionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378135115684" ID="ID_1606879920" MODIFIED="1384425073404" TEXT="Prima el tiempo de respuesta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378135126124" ID="ID_1308030579" MODIFIED="1384425076665" TEXT="Bases de datos muy normalizadas (relacionales)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378135169567" ID="ID_1870375452" MODIFIED="1384425271541" TEXT="En el caso de gama alta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378135178018" ID="ID_545825110" MODIFIED="1384425080446" TEXT="Entorno de desarrollo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378135185399" ID="ID_1468926724" MODIFIED="1384425082567" TEXT="Entorno de preproducci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378135194727" ID="ID_826364398" MODIFIED="1384425085859" TEXT="Entorno de producci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1378135232961" ID="ID_1494220809" MODIFIED="1384531509104" TEXT="Ejemplos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378135240485" ID="ID_675250213" MODIFIED="1378135800856" TEXT="SAP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378135272867" FOLDED="true" ID="ID_1142629696" MODIFIED="1384531939120" TEXT="Parte t&#xe9;cnica: &quot;Basis Components&quot; (Base de datos, red, sistema operativo, etc.)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378135299900" ID="ID_355350279" MODIFIED="1378135798752" TEXT="Lenguaje de programaci&#xf3;n: ABAP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1378135355982" ID="ID_189213783" MODIFIED="1382981290452" TEXT="Dynamics NAV de Microsoft (Navision)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378135406392" ID="ID_700317186" MODIFIED="1382981292702" TEXT="Microsoft Dynamics AX (Microsoft Business Solutions)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1378135436162" ID="ID_1884940976" MODIFIED="1378135569254" POSITION="right" TEXT="CRM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378135506085" ID="ID_1717291690" MODIFIED="1384531755024" TEXT="Caracter&#xed;sticas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378135518593" ID="ID_626414684" MODIFIED="1378135789415" TEXT="Multicanal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378135528745" ID="ID_622854780" MODIFIED="1378135534268" TEXT="Tel&#xe9;fono"/>
<node CREATED="1378135534590" ID="ID_156019778" MODIFIED="1378135537348" TEXT="Internet"/>
<node CREATED="1378135537999" ID="ID_1421707430" MODIFIED="1378135542060" TEXT="Correo tradicional"/>
<node CREATED="1378135542310" ID="ID_228361793" MODIFIED="1378135551504" TEXT="Correo electr&#xf3;nico"/>
</node>
<node CREATED="1378135559248" ID="ID_773707205" MODIFIED="1378135791895" TEXT="Integrado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378135576816" ID="ID_1697627341" MODIFIED="1384425292360" TEXT="Tipos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378135579507" ID="ID_1894945573" MODIFIED="1552641212883" TEXT="Anal&#xed;tico o back-office">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378135629579" ID="ID_1897004571" MODIFIED="1384425018530" TEXT="Se centra en explotar el conocimiento del cliente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378135641891" ID="ID_264792041" MODIFIED="1384531787722" TEXT="Tecnolog&#xed;as">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378135653299" ID="ID_477311436" MODIFIED="1384425020541" TEXT="Bases de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378135663204" ID="ID_785144601" MODIFIED="1384425023071" TEXT="Datawarehouse y reporting">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378135675076" ID="ID_61873057" MODIFIED="1384425024771" TEXT="Data mining">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1378135610122" ID="ID_760783225" MODIFIED="1384625160674" TEXT="Operacional o front-office">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378135710406" ID="ID_1739294191" MODIFIED="1384425029095" TEXT="Interacciones directas con el cliente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378135721222" ID="ID_1467302474" MODIFIED="1384531808283" TEXT="Tecnolog&#xed;as">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378135734466" ID="ID_864330548" MODIFIED="1384425034148" TEXT="Call-center">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378135739391" ID="ID_514237405" MODIFIED="1384425036118" TEXT="Software de gesti&#xf3;n de incidencias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378135748512" ID="ID_734206235" MODIFIED="1384425038308" TEXT="P&#xe1;ginas web">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378135753511" ID="ID_1008856365" MODIFIED="1384425041310" TEXT="Software de gesti&#xf3;n de correo electr&#xf3;nico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378135764560" ID="ID_1336639921" MODIFIED="1384425044942" TEXT="Herramientas de movilidad para agentes de venta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1378135809570" ID="ID_1500943134" MODIFIED="1378135824904" POSITION="right" TEXT="PRM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378135814133" ID="ID_960631728" MODIFIED="1378135822401" TEXT="Relaci&#xf3;n con los socios"/>
</node>
</node>
</map>
