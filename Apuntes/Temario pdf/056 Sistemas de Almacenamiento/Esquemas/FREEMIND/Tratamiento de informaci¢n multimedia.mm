
<map version="0.9.0">
    <node TEXT="**TEMA 23 : TRATAMIENTO DE INFORMACIÓN MULTIMEDIA**" FOLDED="false" POSITION="right" ID="596694bff679c10001ad2f5d" X_COGGLE_POSX="0" X_COGGLE_POSY="0">
        <edge COLOR="#b4b4b4"/>
        <font NAME="Helvetica" SIZE="17"/>
        <node TEXT="**TRATAMIENTO DE AUDIO**" FOLDED="false" POSITION="left" ID="9b18ad5f491606efe33caacb48d8101d">
            <edge COLOR="#e68782"/>
            <font NAME="Helvetica" SIZE="15"/>
            <node TEXT="**Tipos de sonido **" FOLDED="false" POSITION="left" ID="66bea840a40d5c3fc24da6263f271f02">
                <edge COLOR="#e37e7b"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="Habla" FOLDED="false" POSITION="left" ID="20b68522326a1bac9ae4c7cc3a9d68a7">
                    <edge COLOR="#e57f7d"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="**Codificación del habla :**para la comunicación entre personas, relacionado con la conversión A/D y la compresión de la señal digital" FOLDED="false" POSITION="left" ID="844abcaa09679e0723e72e90c08e602e">
                        <edge COLOR="#e77e7d"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Síntesis del habla :**para la comunicación entre máquina y persona" FOLDED="false" POSITION="left" ID="6ae565b162616584c0e26e5358106304">
                        <edge COLOR="#e67d7c"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Reconocimiento y comprensión del habla :**para la comunicación entre persona y máquina" FOLDED="false" POSITION="left" ID="e9697a024dde714ea8296f824e9216b5">
                        <edge COLOR="#e67d7c"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
                <node TEXT="Otros sonidos: el sonido audible consiste en ondas sonoras que se producen cuando las oscilaciones de la presión del aire se convierten en vibraciones detectadas por el oído humano. Estas vibraciones se convierten en impulsos nerviosos que son percibidos por el cerebro." FOLDED="false" POSITION="left" ID="5cee75cc50a32919249644b497567d5d">
                    <edge COLOR="#e07876"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
            </node>
            <node TEXT="**MIDI **" FOLDED="false" POSITION="left" ID="fbfc974eaab50492a155beef5797277c">
                <edge COLOR="#e57d79"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="Estándar de comunicaciones para instrumentos musicales electrónicos y computadoras." FOLDED="false" POSITION="left" ID="67c5b1c14bcea345478f765073466860">
                    <edge COLOR="#e47876"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="Protocolo para pasar descripciones detalladas de una partitura musical como notas, secuencias de notas e instrumentos" FOLDED="false" POSITION="left" ID="cd0b80c1573079fb088cc56ee6ae13bb">
                    <edge COLOR="#e78380"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="Lista de órdenes de grabaciones de acciones musicales que cuando se envían a un dispositivo de reproducción MIDI produce sonido. No es sonido digitalizado" FOLDED="false" POSITION="left" ID="955588101b292b4452e912c7f0ef6d23">
                    <edge COLOR="#e57f7d"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
            </node>
            <node TEXT="**Digitalización**" FOLDED="false" POSITION="left" ID="bb506ef3e79e27b81c0d4832f792e3a4">
                <edge COLOR="#e7817d"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="Los sonidos digitalizados son muestras de sonido. Cada enésima fracción de segundo se toma una muestra y se guarda como información en bits o bytes" FOLDED="false" POSITION="left" ID="622ecda9376ffd9b9a5264580ccf396a">
                    <edge COLOR="#e78785"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="Velocidad de muestreo : frecuencia con que se toman las muestras" FOLDED="false" POSITION="left" ID="5c773faa3dacda2de69a8762becc2a1c">
                    <edge COLOR="#e57f7d"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="Tamaño de la muestra : cantidad de información almacenada de cada muestra. 8 y 16 bits" FOLDED="false" POSITION="left" ID="6e6a4456ca1d7c85d038edef6fa0e738">
                    <edge COLOR="#e57977"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="El valor de cada muestra se redondea al entero más próximo (cuantización) y si la amplitud es más grande que el intervalo disponible se hace un recorte (**clipping**) de la parte alta o baja de la onda." FOLDED="false" POSITION="left" ID="515ef4974dc096c2a529804322752bf7">
                    <edge COLOR="#e97c79"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="El ruido en la cuantización se puede suavizar mediante el **dithering** agregando ruido al azar para suavizar transiciones" FOLDED="false" POSITION="left" ID="dafda693cd66b01012879cab42368ce5">
                    <edge COLOR="#e67e7c"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
            </node>
            <node TEXT="**Compresión de audio**" FOLDED="false" POSITION="left" ID="94a0f2ac0707e94ea2133352f5b832ec">
                <edge COLOR="#e48582"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="El sonido es más difícil de comprimir que las imágenes, especialmente usando métodos de compresión sin pérdidas" FOLDED="false" POSITION="left" ID="ecf69f41a384d764bdfeb95416ecda4a">
                    <edge COLOR="#e68684"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="Las más comunes son las que se basan en la **codificación diferencial**" FOLDED="false" POSITION="left" ID="fe2e27573b36d349572362bcb29a6dec">
                    <edge COLOR="#e48382"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="**DPCM**: se toma como valor siguiente la diferencia con el valor de la señal en el instante anterior" FOLDED="false" POSITION="left" ID="927147b32b7de03f101edf6386c7760c">
                        <edge COLOR="#e58887"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Modulación delta :**forma particular de DPCM en la cual la diferencia entre el valor actual y el siguiente se codifica con un solo bit, indicando si el valor de la señal se incrementa o decrementa en un &quot;quantum&quot;" FOLDED="false" POSITION="left" ID="3790214a1e627d7c0555d14e01907fd2">
                        <edge COLOR="#e58a89"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**ADPCM :**" FOLDED="false" POSITION="left" ID="d699806abcea667961de402fd2b702c4">
                        <edge COLOR="#e58c8b"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
            </node>
            <node TEXT="**Códec :**programa o dispositivo hardware capaz de codificar o decodificar una señal o flujo de datos digitales." FOLDED="false" POSITION="left" ID="ce6555a6a06e227952d8fafbd92cd7de">
                <edge COLOR="#e47e7a"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="MP3 : elimina las frecuencias inaudibles conservando la esencia del sonido." FOLDED="false" POSITION="left" ID="7422251b2db57fa5ac0cae78fc3b1555">
                    <edge COLOR="#e67b78"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Dolby Digital :**" FOLDED="false" POSITION="left" ID="3ca7cc461f5e1d3af55630dcae4c4578">
                    <edge COLOR="#e6807e"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**AAC**: formato usado por Apple, de más calidad que MP3" FOLDED="false" POSITION="left" ID="62926006c1e33a64796b8712998fe9d2">
                    <edge COLOR="#e78583"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
            </node>
            <node TEXT="**Formatos**" FOLDED="false" POSITION="left" ID="52e0583660672c3b99fd2554060a5eb2">
                <edge COLOR="#e48582"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="**WAV **: el más usado en plataformas Windows" FOLDED="false" POSITION="left" ID="20118e9b4e0c073acdd446bde3b208f4">
                    <edge COLOR="#e27e7c"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Otros :** AU, VOX, AIFF, WMA" FOLDED="false" POSITION="left" ID="f662309430cffb2a14f56e72b5ef3e65">
                    <edge COLOR="#e27c7a"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
            </node>
            <node TEXT="**Herramientas**: Stereo Studio, Sound Edit Pro, WinAmp" FOLDED="false" POSITION="left" ID="24026b6f9cac2ae576d7e63bed1f543f">
                <edge COLOR="#e58481"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="Operaciones de edición" FOLDED="false" POSITION="left" ID="3147b7edd0b136cc97d15bc2547ad2f8">
                    <edge COLOR="#e27d7c"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="Recortes, uniones, conversión de formatos, ecualización, procesamiento digital de señales, sonido a la inversa" FOLDED="false" POSITION="left" ID="77822a4bf0647bab831f04edcd624909">
                        <edge COLOR="#e47f7e"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
            </node>
        </node>
        <node TEXT="**REALIDAD VIRTUAL**" FOLDED="false" POSITION="right" ID="c81cfb7238b527513d9df2328a0b7941">
            <edge COLOR="#988ee3"/>
            <font NAME="Helvetica" SIZE="15"/>
            <node TEXT="**Concepto :**entorno de escenas u objetos de apariencia real, generado mediante tecnología informática, que crea en el usuario la sensación de estar inmerso en él." FOLDED="false" POSITION="right" ID="3ad13c28cef304c0b1fa69e916be08d1">
                <edge COLOR="#9d95e4"/>
                <font NAME="Helvetica" SIZE="13"/>
            </node>
            <node TEXT="**Tipos**" FOLDED="false" POSITION="right" ID="d3d5f08c0c8990077a49931e6d6d4560">
                <edge COLOR="#9c94e3"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="**Inmersiva :**ambiente tridimensional generado por ordenador y que se accede mediante , cascos, guantes u otros dispositivos que capturan la posición y rotación del sujeto." FOLDED="false" POSITION="right" ID="3976719d2f370c7904bec4d8437244ad">
                    <edge COLOR="#958edf"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**No inmersiva :**se vale de medios como Internet para interactuar en tiempo real con personas en espacios y ambientes virtuales, sin necesidad de dispositivos adicionales" FOLDED="false" POSITION="right" ID="dc09fc99c81fb2044580af234caa2731">
                    <edge COLOR="#9f98e4"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
            </node>
            <node TEXT="**VRML(Virtual Reality Modeling Language) :**lenguaje para crear ambientes tridimensionales y poder interactuar con ellos. Los ambientes son creados por vectores y son usados generalmente en la web." FOLDED="false" POSITION="right" ID="cbe54d7fbb3dd39ca1807e371975b118">
                <edge COLOR="#978fe4"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="Consiste en un fichero de texto donde se detallan los vértices y aristas de polígonos en 3D, junto con colores, texturas, transparencias, efectos..etc. Puede comprimirse para transmitirse más rápidamente por Internet" FOLDED="false" POSITION="right" ID="5b2d567cb36531927d04f2eea4bde597">
                    <edge COLOR="#9993e4"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="Su desarrollo es llevado a cabo por el Consorcio Web3D que posteriormente comenzó a trabajar en su sucesor llamado **X3D**" FOLDED="false" POSITION="right" ID="5e1fc6604906949a9b6a56f8484a705e">
                    <edge COLOR="#9c96e5"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="**X3D :**lenguaje para gráficos vectoriales definido por norma ISO que puede emplear una sintaxis similar a XML así como una del tipo VRML. " FOLDED="false" POSITION="right" ID="c5bec4030a066706770b16ca6ebcf72e">
                        <edge COLOR="#9691e4"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
            </node>
            <node TEXT="**Gafas de realidad virtual **" FOLDED="false" POSITION="right" ID="d180b296926b712c4b4e79da4320a8b2">
                <edge COLOR="#9c94e5"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="Características" FOLDED="false" POSITION="right" ID="9a7027a7bed7a95d321453decd9467c4">
                    <edge COLOR="#a29ce9"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="**Hardware necesario :**conexión a ordenador, o a smartphone o a videoconsola" FOLDED="false" POSITION="right" ID="145e86fae4a16b9ab03543370b2f1ea0">
                        <edge COLOR="#9a94e9"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Resolución :**" FOLDED="false" POSITION="right" ID="c62d21eed7f5a15a0a887b90d68abcc7">
                        <edge COLOR="#9691e8"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Tasa de refresco :**grado de fluidez de las imágenes en la pantalla" FOLDED="false" POSITION="right" ID="b7d646359bb9b8d682ee2c1aea6f08cc">
                        <edge COLOR="#9994e7"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Ángulo de visión :**cuanto más ángulo más sensación de inmersión" FOLDED="false" POSITION="right" ID="5df310d75852a3f9bed40ca75178d045">
                        <edge COLOR="#a39ee9"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Sensores :**integrados o no en las gafas" FOLDED="false" POSITION="right" ID="5c5713d637f70d4f5d3ad00cd10ef881">
                        <edge COLOR="#9691e8"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Área de rastreo :**superficie dentro de la cual nuestros movimientos son registrados por los sensores de posición" FOLDED="false" POSITION="right" ID="a0aa608a64ec7e1e45fcaa5e1fadcc55">
                        <edge COLOR="#a49fe9"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Gráficos 3D :**" FOLDED="false" POSITION="right" ID="a41e21656279e8d097ab50e9c9a796e6">
                        <edge COLOR="#aaa5ea"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Técnicas estereoscopia :**nos permiten dar profundidad y realismo a las imágenes tridimensionales" FOLDED="false" POSITION="right" ID="8b559f7af2c0e4a035fda2b7f7d9e820">
                        <edge COLOR="#9994e7"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Simulación comportamiento :**" FOLDED="false" POSITION="right" ID="ead082f58897c82e42ecdfb85ba4987a">
                        <edge COLOR="#9792e7"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Facilidad para navegar :**" FOLDED="false" POSITION="right" ID="baadf94be0987fe36c61f31af1f2124d">
                        <edge COLOR="#a5a0e9"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Técnicas para una inmersión total :**" FOLDED="false" POSITION="right" ID="76b2bee499c412ff44abf8a02e91634e">
                        <edge COLOR="#aaa6eb"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
            </node>
            <node TEXT="**Desarrolladores de la tecnología**" FOLDED="false" POSITION="right" ID="e0c926b1070d6ea2313a97997c7c9341">
                <edge COLOR="#9f98e3"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="**Samsung :**con su modelo de gafas Gear VR" FOLDED="false" POSITION="right" ID="32607095165987751f9c3425cd35e2dc">
                    <edge COLOR="#a49ee4"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Facebook :**Oculus Rift" FOLDED="false" POSITION="right" ID="f58724dc698d55e13d9d6ad7d9f9c443">
                    <edge COLOR="#a09be4"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Sony :**PlayStation VR" FOLDED="false" POSITION="right" ID="a5089c17e0500f3aa91a2cc81621e53e">
                    <edge COLOR="#9690df"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Microsoft :**Hololens para realidad aumentada" FOLDED="false" POSITION="right" ID="8754770e455a2bd9f5348479e9117104">
                    <edge COLOR="#a09ae3"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Google :**Google Daydream" FOLDED="false" POSITION="right" ID="4a2f95fa196c3fd9aa260c513bfaa3e2">
                    <edge COLOR="#a09ae3"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
            </node>
        </node>
        <node TEXT="**TRATAMIENTO DE IMÁGENES**" FOLDED="false" POSITION="left" ID="a61baeebae7a021bb9a2f0cba820d408">
            <edge COLOR="#9ed56b"/>
            <font NAME="Helvetica" SIZE="15"/>
            <node TEXT="**Concepto de imágen**; función bidimensional de intensidad de luz f(x,y) donde x e y representan las coordenadas espaciales y el valor de f en un punto cualquiera (x,y) es proporcional al brillo o nivel de gris (escala de grises) de la imagen en ese punto" FOLDED="false" POSITION="left" ID="e0399adc0419e17d7a3a53edffbc1ace">
                <edge COLOR="#a0d66a"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="Los elementos  de una distribución digital se denominan elementos de la imagen o **pixels.**" FOLDED="false" POSITION="left" ID="4b2acd4655096a1f849db52ea6b87455">
                    <edge COLOR="#9dd462"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
            </node>
            <node TEXT="**Técnicas de compresión**" FOLDED="false" POSITION="left" ID="69deeef15daa642116b1ddfed6ccb204">
                <edge COLOR="#9dd565"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="Sin pérdida de información" FOLDED="false" POSITION="left" ID="0eb45ac861728962906254d3aa992cb3">
                    <edge COLOR="#a3d66c"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="**Run-length encode (RLE) :**Es el método más simple. Consiste en almacenar el número de caracteres que se repiten, seguido del carácter" FOLDED="false" POSITION="left" ID="2366744b9f2b7fb36d45d4ac56ce2d98">
                        <edge COLOR="#a2d369"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Codificación de Huffman :**consiste en asignarle código de bits más cortos a los datos que mayor frecuencia de aparición tienen y códigos más largos a los que aparecen con menos regularidad" FOLDED="false" POSITION="left" ID="4b19c04454c28107a26b933a5ff8f750">
                        <edge COLOR="#a9d773"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Codificación artimética :**codifica una secuencia de símbolos mediante una representación binaria, las secuencias de bits son obtenidas a partir de intervalos que tienen valores entre cero y uno" FOLDED="false" POSITION="left" ID="15ff55de7bf0eeaad3be87a28943d19f">
                        <edge COLOR="#a1d664"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Lempel-Ziv**: es un algoritmo que realiza un análisis desde cadenas o palabras de un determinado alfabeto, cuyas longitudes no exceden un prescrito entero L. Posteriormente  se asignan estas cadenas o palabras secuencialmente hacia un único código de palabras de longitud LZ." FOLDED="false" POSITION="left" ID="6314287b22f255c543d394b9374f23c5">
                        <edge COLOR="#a1d466"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
                <node TEXT="Con pérdidas de información" FOLDED="false" POSITION="left" ID="30b05e5187ce7b5f3319ff92711688a9">
                    <edge COLOR="#9fd466"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="**Codificación por transformación :**utiliza una transformada para hacer corresponder la imagen con un conjunto de coeficientes de transformada, a los que se les aplica un proceso de cuantificación en donde un número significativo de los coeficientes tienen valores pequeños poco significativos, los cuales se pueden eliminar mediante la cuantización, produciéndose la pérdida de información, aunque ello no suponga una distorsión de la imagen" FOLDED="false" POSITION="left" ID="82910d68f924e92cad17c65f981b9160">
                        <edge COLOR="#a7d86e"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Vector de cuantización :**la idea central es seleccionar un conjunto representativo de una imagen, Se divide la imagen en bloques de tamaño fijo llamados vectores. Se construye una tabla que contiene vectores diferentes encontrados en la imagen original" FOLDED="false" POSITION="left" ID="6cd8b032ac8372ea75f0d5170898d11a">
                        <edge COLOR="#a7d86e"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Compresión fractal :**un fractal es un objeto semigeométrico cuya estructura básica, fragmentada o irregular, se repite a diferentes escalas. " FOLDED="false" POSITION="left" ID="b6f8c9d2db5668e54280ea6a686687b7">
                        <edge COLOR="#a3d46a"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
            </node>
            <node TEXT="**Formatos de archivos de imágenes**" FOLDED="false" POSITION="left" ID="ee3540a6855f81369cacf5db317be238">
                <edge COLOR="#a4d771"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="**Mapa de bits :**despliega la imagen como un conjunto de píxeles de colores individuales. Cada pixel es una celda del mapa de bits" FOLDED="false" POSITION="left" ID="69e0174ace4ac43ba44ad68fa86868b2">
                    <edge COLOR="#a9d975"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="**JPEG :**formato estándar ISO , utiliza una técnica de compresión con pérdida" FOLDED="false" POSITION="left" ID="78ac9b1ea3310a9ae793eb1821c08721">
                        <edge COLOR="#a9d773"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**GIF**: se limita a 256 colores y permite almacenar imágenes estáticas como diagramas simples o logos con áreas de color sólido. También almacena imágenes como animaciones simples. Método de compresión sin pérdida" FOLDED="false" POSITION="left" ID="ab84e1cac0e7fe6d678049c2c920c149">
                        <edge COLOR="#aedb79"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**PNG :**soporta hasta 16 millones de colores, con algoritmo de compresión sin pérdida. Adecuado para gráficos pero no soporta animación" FOLDED="false" POSITION="left" ID="4a96efe5feed7cb9671ae62e2b0c3032">
                        <edge COLOR="#aad973"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**TIFF :**formato de archivo de imagen etiquetado, donde además de los datos de la imagen contienen etiquetas donde se archiva información sobre las características de la imagen que sirven para su tratamiento posterior. Diversas formas de compresión" FOLDED="false" POSITION="left" ID="71e342294239b4684223f54d4c6e51ae">
                        <edge COLOR="#add87c"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**BMP :**maneja colores de hasta 24 bits de profundidad, usados en los programas de Microsoft. Compresión sin pérdidas" FOLDED="false" POSITION="left" ID="b30d5f55a90a71dcbfdd5eab1f9de397">
                        <edge COLOR="#a6d96b"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
                <node TEXT="**Vectoriales :**conjunto de vectores basados en ecuaciones matemáticas, las cuales describen geométricamente una imagen, pudiendo corresponder a puntos, lineas, curvas, polígonos.. Generalmente son de mayor calidad" FOLDED="false" POSITION="left" ID="677c14ad6b510a38d5e49e31063495af">
                    <edge COLOR="#acda7a"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="Privativos" FOLDED="false" POSITION="left" ID="04015cb54790cebdedb331754ac2cfda">
                        <edge COLOR="#abd975"/>
                        <font NAME="Helvetica" SIZE="12"/>
                        <node TEXT="PostScript, DWG, CDR (Corel)" FOLDED="false" POSITION="left" ID="07bd58f127245d51a7f4a0d5017f0c25">
                            <edge COLOR="#acda72"/>
                            <font NAME="Helvetica" SIZE="12"/>
                        </node>
                    </node>
                    <node TEXT="Libres" FOLDED="false" POSITION="left" ID="0466ea63e2c9b9b44e262b87c0a6067e">
                        <edge COLOR="#aad775"/>
                        <font NAME="Helvetica" SIZE="12"/>
                        <node TEXT="PDF, ODG, VML" FOLDED="false" POSITION="left" ID="2e52a41ffb5600dd92474022ccfd9f81">
                            <edge COLOR="#a8d66e"/>
                            <font NAME="Helvetica" SIZE="12"/>
                        </node>
                    </node>
                </node>
            </node>
        </node>
        <node TEXT="**INTRODUCCIÓN**" FOLDED="false" POSITION="right" ID="8a9fcbacfec9427e41c66e7e36f1cdb7">
            <edge COLOR="#7aa3e5"/>
            <font NAME="Helvetica" SIZE="15"/>
            <node TEXT="**Conceptos**" FOLDED="false" POSITION="right" ID="8b0a82e00688dfa668316c4867a1d69c">
                <edge COLOR="#79a5e6"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="**Multimedia :**cualquier objeto o sistema que utiliza múltiples medios de expresión físicos o digitales para presentar o comunicar información" FOLDED="false" POSITION="right" ID="c38d30b0f7bbe3be64fa5fd67d0a420f">
                    <edge COLOR="#78a6e5"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="**Interactiva :**cuando el usuario tiene libre control sobre la presentación de los contenidos" FOLDED="false" POSITION="right" ID="03496a1d1b55f45e33a6af77687ec769">
                        <edge COLOR="#7cabe7"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Directa :**el usuario es forzado a visualizar el contenido en un orden predeterminado" FOLDED="false" POSITION="right" ID="830c5e8a8cfba2b5d2b6ae22a5cca0fc">
                        <edge COLOR="#77a7e4"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
                <node TEXT="**Hipermedia :**forma especial de multimedia interactiva que emplea estructuras de navegación más complejas que aumentan el control del usuario sobre el flujo de información" FOLDED="false" POSITION="right" ID="6a5fca17da9798e4efb914005ed1e7a4">
                    <edge COLOR="#85aee6"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="Cuando un programa, documento o presentación combina adecuadamente los medios, se mejora notablemente la atención, comprensión y el aprendizaje ya que se acercará a la manera natural en que los seres humanos nos comunicamos, usando varios sentidos para comprender " FOLDED="false" POSITION="right" ID="b4832d1a93c4902068cca7f779616649">
                        <edge COLOR="#85b0e8"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
            </node>
            <node TEXT="**Tipos de información multimedia**" FOLDED="false" POSITION="right" ID="8973b0b9aa03accd51157109e299699e">
                <edge COLOR="#76a3e3"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="**Texto** : sin formatear, formateado, lineal e hipertexto" FOLDED="false" POSITION="right" ID="25458da87e5f85cae11b8679895d2da1">
                    <edge COLOR="#6fa0e0"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Gráficos :**para representar esquemas, planos, dibujos lineales..." FOLDED="false" POSITION="right" ID="2deb226502bde47e11bf01a8b50b235f">
                    <edge COLOR="#77a6e4"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Imágenes :**documentos formados por píxeles, generados por copia del entorno (escaneado, fotografía digital)" FOLDED="false" POSITION="right" ID="8093992469a25afb781a5071e03e1ff3">
                    <edge COLOR="#79a7e4"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Animación :**presentación de un número de gráficos por segundo que genera en el observador la sensación de movimiento" FOLDED="false" POSITION="right" ID="77db22578c3c0623633622f3cf755b2f">
                    <edge COLOR="#78a6e3"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Video :**presentación de un número de imágenes por segundo que genera en el observador la sensación de movimiento. Sintetizadas o captadas" FOLDED="false" POSITION="right" ID="495ca81118dcaa5700745ce7103fec58">
                    <edge COLOR="#76a6e5"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Sonido :**puede ser habla, música, o de otros orígenes" FOLDED="false" POSITION="right" ID="7fd55e62c5c1078dfbc871b52262516a">
                    <edge COLOR="#70a0df"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
            </node>
            <node TEXT="**Características**" FOLDED="false" POSITION="right" ID="099c23d05ffe9338dac687e364047e3c">
                <edge COLOR="#76a2e5"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="**Controlados por ordenador :**la presentación de la información multimedia debe estar controlada por un ordenador que también participa en la producción, almacenamiento, edición, transmisión, etc." FOLDED="false" POSITION="right" ID="106a7558b3677bde7fadcab62a627ee6">
                    <edge COLOR="#6c9de3"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Integrados :**" FOLDED="false" POSITION="right" ID="5171dbbaa7587f94b0dc1066a75f7261">
                    <edge COLOR="#6c9ee5"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Almacenamiento digital de la información :**" FOLDED="false" POSITION="right" ID="6a4467675f55eed3a0f719690040b5a2">
                    <edge COLOR="#80aae7"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Interactividad :**" FOLDED="false" POSITION="right" ID="71d6c464988317e1bdd91c32c2f918ab">
                    <edge COLOR="#74a3e5"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="Selección del momento de comienzo" FOLDED="false" POSITION="right" ID="b0246523760eaae9f802b31af9add01d">
                        <edge COLOR="#7face6"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="Especificación de la secuencia" FOLDED="false" POSITION="right" ID="bd381e87885c24906bd9aec1a63ab675">
                        <edge COLOR="#7caae7"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="Control sobre velocidad" FOLDED="false" POSITION="right" ID="a9d7441bdaebfb4fbfcdd1228879012b">
                        <edge COLOR="#79a7e4"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="Modificación de la forma de presentación" FOLDED="false" POSITION="right" ID="420adbe0d6be079e519083be2caafb14">
                        <edge COLOR="#7ba8e4"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="Entradas por parte del usuario para anotar, modificar o enriquecer la información" FOLDED="false" POSITION="right" ID="3a73144e00157455b2791f5aea4e2695">
                        <edge COLOR="#76a7e7"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="Entradas del usuario que son procesadas y generan respuestas específicas" FOLDED="false" POSITION="right" ID="62a2f27d95c33ec29de2cbe4c6bc944f">
                        <edge COLOR="#7baae7"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
            </node>
            <node TEXT="**Pasos para elaborar un producto multimedia**" FOLDED="false" POSITION="right" ID="0fd2a8ee49020a12938adc42dd099bbf">
                <edge COLOR="#77a3e4"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="**Definir el mensaje clave :**" FOLDED="false" POSITION="right" ID="b555e7adf23cb72d934b2ce19aa54403">
                    <edge COLOR="#7ea8e3"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Conocer al público :**" FOLDED="false" POSITION="right" ID="3ed83194b271eadab28e7017ed2c99c6">
                    <edge COLOR="#71a0e2"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Desarrollo o guión :**" FOLDED="false" POSITION="right" ID="8822a08b641f988b6cea31655ebe7fb2">
                    <edge COLOR="#7ca7e3"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Creación de un prototipo :**" FOLDED="false" POSITION="right" ID="8f77cb0efc528722f0095932cd5212e7">
                    <edge COLOR="#79a5e2"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Creación del producto**" FOLDED="false" POSITION="right" ID="2df44e23b5dd0bd5da6296603717187e">
                    <edge COLOR="#78a6e5"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
            </node>
            <node TEXT="**Tipologías**" FOLDED="false" POSITION="right" ID="42e235e725bbf38639ece0bab18dafdd">
                <edge COLOR="#72a0e5"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="**Multimedia educativa :**a través del e-learning y las redes sociales." FOLDED="false" POSITION="right" ID="bde866a512daf19915e6175ed82ed95d">
                    <edge COLOR="#689be3"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Multimedia publicitaria :**en todo tipo de dispositivo para su divulgación siendo las redes sociales la herramienta de más difusión viral." FOLDED="false" POSITION="right" ID="6ba0c512914b71fa1766f2a168120d28">
                    <edge COLOR="#71a2e6"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Multimedia comercial :**bbdd, promociones, catálogos, simuladores, páginas web." FOLDED="false" POSITION="right" ID="a48afc9caa6ef3824bf0ea94c4f5107d">
                    <edge COLOR="#6b9de4"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Multimedia informativa :**noticias, prensa, revistas, televisión, con presentación de forma masiva y actualizada al momento de los hechos." FOLDED="false" POSITION="right" ID="e956e51a6d432921be2b59eee4d3085f">
                    <edge COLOR="#6a9ce3"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
            </node>
            <node TEXT="**Ámbitos de aplicación**" FOLDED="false" POSITION="right" ID="e415953265f4291bc9f0eaf02a3ef88c">
                <edge COLOR="#81aae7"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="**Mundo empresarial :**presentación de proyectos, resultados, productos, previsiones., publicidad, formación..." FOLDED="false" POSITION="right" ID="e71098e8b27151928d4c68c43d5156b6">
                    <edge COLOR="#79a5e3"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Industria :**sistemas de control industrial, herramientas de simulación, sistemas de gestión de piezas y stocks o producción." FOLDED="false" POSITION="right" ID="bcb3b76151078d77bb56e59ddb662214">
                    <edge COLOR="#82ace6"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Educación :**modificación radical del proceso educativo a todos los niveles. Mayor innovación y beneficio" FOLDED="false" POSITION="right" ID="d2166b02771ede97a9d049740d6fead8">
                    <edge COLOR="#88afe6"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Ámbito doméstico :**juegos, comunicaciones, control domótico, televisión interactiva, acceso doméstico a Internet" FOLDED="false" POSITION="right" ID="b8e450a7fecf604bd699a2edc466346d">
                    <edge COLOR="#83ade7"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Lugares públicos :**bibliotecas, museos, centros comerciales, aeropuertos, o vías públicas, donde se implementan puntos de acceso a la información. Sistemas de compr ade entradas, reserva de alojamientos, planos interactivos, pago electrónico, etc." FOLDED="false" POSITION="right" ID="3673c6cd47e53f56419d1c184907534e">
                    <edge COLOR="#87afe7"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
            </node>
        </node>
        <node TEXT="**TRATAMIENTO DE VIDEO**" FOLDED="false" POSITION="right" ID="b9d235d643edeb646d27c37a47c6f061">
            <edge COLOR="#efa670"/>
            <font NAME="Helvetica" SIZE="15"/>
            <node TEXT="**Video analógico**: es una señal eléctrica que varía con el tiempo y que se obtiene a partir de muestrear, de forma periódica la información que llega de una cámara: un patrón de distribución espacial de intensidad luminosa cambiante con el tiempo (barrido o scanning)" FOLDED="false" POSITION="right" ID="964a9f401b8c039852b9a1f2a8b890d8">
                <edge COLOR="#ed9e68"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="Métodos de barrido" FOLDED="false" POSITION="right" ID="731d069544a547d82d324d6f2e53f32a">
                    <edge COLOR="#ee9963"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="**Progresivo :**recorre u obtiene una imagen que se denomina cuadro o frame cada T segundos." FOLDED="false" POSITION="right" ID="6cd1b0401c25047730892462f1c290b8">
                        <edge COLOR="#ec9059"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Entrelazado :**se dibujan primero las líneas pares y luego las impares" FOLDED="false" POSITION="right" ID="24dae8c47d98e482162ecb88e1e0aa28">
                        <edge COLOR="#ef9662"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
                <node TEXT="Parámetros de la señal de vídeo analógico" FOLDED="false" POSITION="right" ID="7833a402f0e2630490a461a2d1d121e7">
                    <edge COLOR="#ee975f"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="**Intervalo en blanco horizontal:**" FOLDED="false" POSITION="right" ID="dc2d38b262699381c1fd380943e499e7">
                        <edge COLOR="#ef925a"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Relación de aspecto**: relación entre la distancia horizontal y vertical de recorrido" FOLDED="false" POSITION="right" ID="e9dc6ab1a50903e9cf337eb045eb4201">
                        <edge COLOR="#ef925a"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Sincronización :**" FOLDED="false" POSITION="right" ID="e023273f7dcd44564e4b90938890f4ef">
                        <edge COLOR="#f0935b"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Resolución horizontal**" FOLDED="false" POSITION="right" ID="c8cf8d804d98a05439cdd93d45bb9af6">
                        <edge COLOR="#f19b68"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Resolución vertical**" FOLDED="false" POSITION="right" ID="ef8ce1ad4d7a173944654c31a6442cfc">
                        <edge COLOR="#ec8e55"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Velocidad de cuadro y entrelazado**" FOLDED="false" POSITION="right" ID="45064d12e5dbd1e13bd3bfc8958cf303">
                        <edge COLOR="#f09c69"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
                <node TEXT="**Estándares :**NTSC, PAL, SECAM, HDTV" FOLDED="false" POSITION="right" ID="0838e930b7c37787973cdb27b387f7c5">
                    <edge COLOR="#ee945b"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
            </node>
            <node TEXT="**Video digital**" FOLDED="false" POSITION="right" ID="cb3e55d34c1ce0bbac320f9b0edf2674">
                <edge COLOR="#eea16b"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="Conversión de la señal eléctrica a bits" FOLDED="false" POSITION="right" ID="2ab8ffe3eb1e1e2960b14a6ebb5603b0">
                    <edge COLOR="#f0a16d"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="Resolución marcada por el número de pixels por línea y el número de líneas por cuadro." FOLDED="false" POSITION="right" ID="f56509873eb9da58af06e6e39f930c46">
                    <edge COLOR="#eda270"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="La pérdida de resolución se pone de manifiesto con el **aliasing**, donde se ve el pixelado de los bordes de los objetos." FOLDED="false" POSITION="right" ID="5f638829a93ba0437fc5d2deccff5a1d">
                    <edge COLOR="#ec9963"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
                <node TEXT="**Métodos de compresión**" FOLDED="false" POSITION="right" ID="74426b29951802f9c4f0576aebabe0a0">
                    <edge COLOR="#eb9862"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="**Basados en formas de ondas :**" FOLDED="false" POSITION="right" ID="b362f97fc8d68a865cbbcc85b4327c9b">
                        <edge COLOR="#ea925d"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Basadas en objetos :**" FOLDED="false" POSITION="right" ID="442b085eab9457651958a838e1e4039e">
                        <edge COLOR="#eb9662"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Basadas en modelos :**" FOLDED="false" POSITION="right" ID="0b38804aa46c29fa6f07c634fff66084">
                        <edge COLOR="#ea9663"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**Basadas en fractales :**" FOLDED="false" POSITION="right" ID="f83b4fe277c5472d45527235179ecb76">
                        <edge COLOR="#ed9864"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
                <node TEXT="**Estándares de compresión de video **" FOLDED="false" POSITION="right" ID="61bb6eb1db8a46d7ed569d43eaa19550">
                    <edge COLOR="#eea16f"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="**MJPEG :**trata cada cuadro de una secuencia de video de forma diferente  y le aplica un proceso de compresión independiente del resto." FOLDED="false" POSITION="right" ID="07e8a5c2d71cfe22a53fe55ddb3e550e">
                        <edge COLOR="#eda376"/>
                        <font NAME="Helvetica" SIZE="12"/>
                        <node TEXT="**Beneficios :**se pueden realizar modificaciones más precisas en un editor de video. Es posible empezar a reproducir en cualquier cuadro. Formato útil como medio de almacenamiento por la cantidad de información que contiene." FOLDED="false" POSITION="right" ID="6c69da419fcde28fa518491005be93a0">
                            <edge COLOR="#ed9d70"/>
                            <font NAME="Helvetica" SIZE="12"/>
                        </node>
                        <node TEXT="**Inconvenientes :**genera ficheros excesivamente largos" FOLDED="false" POSITION="right" ID="655a439b2621bc6321f0c26b923df8c8">
                            <edge COLOR="#ed9a6c"/>
                            <font NAME="Helvetica" SIZE="12"/>
                        </node>
                    </node>
                    <node TEXT="**H 261 :**estándar internacional de video utilizado en videoconferencia." FOLDED="false" POSITION="right" ID="b4730a65ce22c79a41e23cbf88fee0d8">
                        <edge COLOR="#f09c6a"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**MPEG :**familia de estándares de la ISO/IEC para la codificación de señales de video. MPEG-1 sigue una estructura de datos jerárquica de forma que posibilita el proceso decodificador" FOLDED="false" POSITION="right" ID="1da48f02aacf3fb69ef86493c1547427">
                        <edge COLOR="#ed9a68"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
                <node TEXT="**Codecs **" FOLDED="false" POSITION="right" ID="65f53b74fc3c0a9b43728e3de2523161">
                    <edge COLOR="#ec9d69"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="Cinepak: optimiza el espacio" FOLDED="false" POSITION="right" ID="85b0273b2a4348f2e3d72f814e404cc4">
                        <edge COLOR="#eb9662"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="Sorensen: para transmisión de video en web" FOLDED="false" POSITION="right" ID="03a2006321fd15300c2fff4adce2b86a">
                        <edge COLOR="#ed9762"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="Indeo: desarrollado por Intel para procesadores Pentium" FOLDED="false" POSITION="right" ID="e0dab3b5aa4cd55e569bc1084290c2a6">
                        <edge COLOR="#ed9660"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="Quicktime : desarrollado por Apple" FOLDED="false" POSITION="right" ID="fc9be899fd4a671380cc73136c7bb0c8">
                        <edge COLOR="#ec955f"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="Animation: para comprimir películas generadas por ordenador." FOLDED="false" POSITION="right" ID="8ba4e96a87b10d936bd06792ba3fb833">
                        <edge COLOR="#ec9d6d"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
                <node TEXT="**Contenedores de video**: No es ni un compresor de video ni un codec. Permite incluir en un mismo archivo varios flujos de audio, video y subtítulos, puediéndose reproduric en cualquier reproductor de medios" FOLDED="false" POSITION="right" ID="e1f058107c6965914680aa1dd5d5d514">
                    <edge COLOR="#ec9e6b"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="AVI : lanzado por Microsoft" FOLDED="false" POSITION="right" ID="a39b80bf59070d03e10b97ba005f9043">
                        <edge COLOR="#eda376"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="OGM : " FOLDED="false" POSITION="right" ID="ddbbc54c51db2e882929fa12edf4274b">
                        <edge COLOR="#ec9f71"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="Matroska MKV : código abierto multiplataforma" FOLDED="false" POSITION="right" ID="e03ce0a5e9894a20acc62220ef90c234">
                        <edge COLOR="#eba276"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
            </node>
        </node>
        <node TEXT="**DIGITALIZACIÓN DE SEÑALES**" FOLDED="false" POSITION="left" ID="cd46ad4bd34502fe20799d2cffba3135">
            <edge COLOR="#67d7c4"/>
            <font NAME="Helvetica" SIZE="15"/>
            <node TEXT="**Tipos de señales**" FOLDED="false" POSITION="left" ID="eb129e23ce0ef32951b50fd030fceeb5">
                <edge COLOR="#6ed8c4"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="**Analógicas :**representadas mediante funciones que toman un infinito número de valores en cualquier intervalo de tiempo considerado. La información va contenida en la propia forma de onda." FOLDED="false" POSITION="left" ID="58d459faf2c2c362802b4d022470dc6d">
                    <edge COLOR="#77dbc7"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="Una señal analógica , en general estará compuesta por la suma de muchas señales senoidales de distintas frecuencias" FOLDED="false" POSITION="left" ID="91fca0c1abea0185e80621ab700e1dce">
                        <edge COLOR="#72dcc5"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
                <node TEXT="**Digitales :**se representan mediante funciones que toman un número finito de valores en cualquier intervalo de tiempo.La información está contenida en los pulsos codificados y no en la forma de onda." FOLDED="false" POSITION="left" ID="37a6df18542b01439272959cfe5f6237">
                    <edge COLOR="#6ed9c4"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="Las señales digitales se basan en la forma de onda cuadrada" FOLDED="false" POSITION="left" ID="013eab4af7e3063b923d24c4d66ce092">
                        <edge COLOR="#66d5bd"/>
                        <font NAME="Helvetica" SIZE="12"/>
                        <node TEXT="Ventajas" FOLDED="false" POSITION="left" ID="c8efa73237cbff8661ec94ce8f595f1e">
                            <edge COLOR="#64d3b9"/>
                            <font NAME="Helvetica" SIZE="12"/>
                            <node TEXT="Cuando una señal es atenuada o experimenta perturbaciones leves, puede ser reconstruida y amplificada mediante sistemas de regeneración de señales" FOLDED="false" POSITION="left" ID="4d78de51bcea602d858777bf256eafc0">
                                <edge COLOR="#60d3b6"/>
                                <font NAME="Helvetica" SIZE="12"/>
                            </node>
                            <node TEXT="Cuenta con sistemas de detección y corrección de errores" FOLDED="false" POSITION="left" ID="ace0dadf45b4654323a627c9e13508d5">
                                <edge COLOR="#6fd4bb"/>
                                <font NAME="Helvetica" SIZE="12"/>
                            </node>
                            <node TEXT="Facilidad para el procesamiento de la señal" FOLDED="false" POSITION="left" ID="5c2201ed7b8f4eea0a1dba7206da67b0">
                                <edge COLOR="#65d6ba"/>
                                <font NAME="Helvetica" SIZE="12"/>
                            </node>
                            <node TEXT="Permite la multigeneración infinita sin pérdidas de calidad" FOLDED="false" POSITION="left" ID="7512faab14cf6594a5dab7c1c7db3008">
                                <edge COLOR="#6ed3ba"/>
                                <font NAME="Helvetica" SIZE="12"/>
                            </node>
                            <node TEXT="Es posible aplicar técnicas de compresión de datos, más eficientes que con señales analógicas" FOLDED="false" POSITION="left" ID="e029d49799fdf51f0a6d3603c1dd746f">
                                <edge COLOR="#59d2b4"/>
                                <font NAME="Helvetica" SIZE="12"/>
                            </node>
                        </node>
                        <node TEXT="Inconvenientes" FOLDED="false" POSITION="left" ID="617c8fd0f6c2728e1676beadcce55eff">
                            <edge COLOR="#67d2b9"/>
                            <font NAME="Helvetica" SIZE="12"/>
                            <node TEXT="Se necesita una conversión analógica-digital previa y una decodificación posterior" FOLDED="false" POSITION="left" ID="c25fd2e1ddc371bb2c323dea291b98ca">
                                <edge COLOR="#64d3b7"/>
                                <font NAME="Helvetica" SIZE="12"/>
                            </node>
                            <node TEXT="Si no se emplea un número suficiente de niveles de cuantificación en el proceso de digitalización, la relación señal a ruido se reducirá con relación a la de la señal analógica original que se cuantificó" FOLDED="false" POSITION="left" ID="80559b1fb7da0dfbdc324ea09acd3033">
                                <edge COLOR="#71d4bb"/>
                                <font NAME="Helvetica" SIZE="12"/>
                            </node>
                        </node>
                    </node>
                </node>
            </node>
            <node TEXT="**Muestreo**: cantidad de veces que medimos el valor de la señal en un periodo de tiempo" FOLDED="false" POSITION="left" ID="89d5722fe404f450ee1060bcde2f7876">
                <edge COLOR="#69d5c1"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="Según el t**eorema de Nyquist-Shannon** la cantidad de veces que debemos medir una señal para no perder información debe ser al menos el doble de la frecuencia máxima que alcanza dicha señal." FOLDED="false" POSITION="left" ID="50f52595d9edd62c4281797eb6e7a0c6">
                    <edge COLOR="#6bd5c0"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
            </node>
            <node TEXT="**Cuantificación :**número de símbolos que utilizamos para guardar una medida de una señal. Para guardar la medida la codificamos con un conjunto de bits." FOLDED="false" POSITION="left" ID="598b0aa07b48c8c5297e85a48250c653">
                <edge COLOR="#6ed8c4"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="A mayor número de bits empleados para guardar la medida mayor exactitud. Lo usual son valores de 8 y 16 bits por calan de información" FOLDED="false" POSITION="left" ID="c0b699ce3fe4d29df055eeacaa3bbe4e">
                    <edge COLOR="#72d8c3"/>
                    <font NAME="Helvetica" SIZE="12"/>
                </node>
            </node>
            <node TEXT="**Digitalización :**consiste en la transcripción de señales analógicas en señales digitales con el propósito de facilitar su procesamiento y hacer la señal resultante más inmune al ruido" FOLDED="false" POSITION="left" ID="ee6f9cf97b45ce898042460d2aafe6a6">
                <edge COLOR="#73d7c5"/>
                <font NAME="Helvetica" SIZE="13"/>
                <node TEXT="Fases" FOLDED="false" POSITION="left" ID="679d80e236f299365fa7f8c7af8c05ac">
                    <edge COLOR="#6cd4c0"/>
                    <font NAME="Helvetica" SIZE="12"/>
                    <node TEXT="**1. Muestreo :**se toman muestras periódicas de la amplitud de onda. El número de muestras por segundo es lo que se conoce por frecuencia de muestreo" FOLDED="false" POSITION="left" ID="98e5d915b2054a2ed1e34e560152ddf2">
                        <edge COLOR="#6ad2bc"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**2. Retención :**las muestras tomadas son retenidas por un circuito de retención, el tiempo suficiente para permitir evaluar su nivel. Es un recurso técnico, no matemático" FOLDED="false" POSITION="left" ID="fca19d2aaad43bdd1f414b50f0da78de">
                        <edge COLOR="#74d8c3"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**3. Cuantificación :**se mide el nivel de voltaje de cada una de las muestras. Se asigna un margen de valor a cada señal analizada a un único valor de salida" FOLDED="false" POSITION="left" ID="22f9f0e35f1ad9760809965d6ca07a18">
                        <edge COLOR="#65d3bc"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                    <node TEXT="**4. Codificación :**consiste en traducir los valores obtenidos durante la cuantificación al código binario" FOLDED="false" POSITION="left" ID="830bc71aed2872f8b274a0f45558cc53">
                        <edge COLOR="#6fd7c1"/>
                        <font NAME="Helvetica" SIZE="12"/>
                    </node>
                </node>
            </node>
        </node>
    </node>
</map>