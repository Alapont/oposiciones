CONTENIDOS:
* ANTERIOR:
- Renumeración del tema: pasa de ser el Tema 064 al 067
- Se divide el contenido de la carpeta Contenidos en dos con objeto de aumentar la claridad: ‘Java EE’ y ‘Adicional’
- ‘Java EE’ contiene la documentación relacionada con Java EE
- ‘Adicional’ mantiene contenido de ediciones anteriores que no se ha clasificado en la carpeta ‘Java EE’
- Se crea el archivo 't067 - Entorno de desarrollo JAVA.pdf' a partir del antiguo archivo 'Resumen' ampliando su contenido.
- Se han eliminado archivos obsoletos o con copyright
- Se mantienen los mismos esquemas que existían el año anterior
* PREPARATIC XXVII:
- Se añade como sección una tabla resumen de las versiones de Java y una imágen con la pila de tecnologías Java. Recolocación de contenidos