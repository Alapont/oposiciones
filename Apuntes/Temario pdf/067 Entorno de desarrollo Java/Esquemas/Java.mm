<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1367750759810" ID="ID_1628839898" MODIFIED="1377960082409" TEXT="Java">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367752641751" FOLDED="true" ID="ID_1677441896" MODIFIED="1385572292013" POSITION="right" TEXT="Especificaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367752652868" ID="ID_1986199361" MODIFIED="1367752679278" TEXT="Antes de 2006">
<node CREATED="1367752680610" ID="ID_1970552349" MODIFIED="1367752687071" TEXT="J2ME"/>
<node CREATED="1367752687560" ID="ID_167873991" MODIFIED="1367752692242" TEXT="J2SE"/>
<node CREATED="1367752693355" ID="ID_643408879" MODIFIED="1367752697413" TEXT="J2EE"/>
</node>
<node CREATED="1367752699900" ID="ID_665777768" MODIFIED="1377960040086" TEXT="Ahora (a partir de 1.4)">
<node CREATED="1367752704416" ID="ID_1011029107" MODIFIED="1379263122977" TEXT="Java SE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367752710632" ID="ID_1259358855" MODIFIED="1379263124038" TEXT="Java ME (Java Micro Edition)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367752721950" ID="ID_1374953283" MODIFIED="1379263124786" TEXT="Java EE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372348150613" ID="ID_89547640" MODIFIED="1379263112665" TEXT="JSR (Java Specification Request): especificaciones propuestas y finales de la tecnolog&#xed;a Java">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1380791549353" ID="ID_973124238" MODIFIED="1380791605577" TEXT="Blueprints: buenas practicas de Java para desarrollo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382863028757" ID="ID_970098252" MODIFIED="1382863068334" TEXT="Anotaciones: a&#xf1;aden metainformaci&#xf3;n al c&#xf3;digo fuente, que ser&#xe1;n explotadas por el entorno de ejecuci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1382863074920" ID="ID_1720201241" MODIFIED="1382864251876" TEXT="Inyecci&#xf3;n de dependencias">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      A partir de la metainformaci&#243;n (anotaciones) el contenedor/servidor Java EE es capaz de &#8221;inyectar &#8221; referencias a otros objetos en determinados atributos de los componentes JEE sin necesidad de que el propio componente lo tenga que hacer por si mismo.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1367751575992" ID="ID_1288756792" MODIFIED="1395691844552" POSITION="right" TEXT="Ejecuci&#xf3;n en el cliente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367751598849" FOLDED="true" ID="ID_754854783" MODIFIED="1385572404664" TEXT="Plug-in en el browser">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367751657575" ID="ID_115722860" MODIFIED="1384678134832" TEXT="JVM distinto del navegador por defecto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1367751607608" ID="ID_643279737" MODIFIED="1385572297667" TEXT="Webstart (JAWS)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367751728892" ID="ID_771290608" MODIFIED="1385572401736" TEXT="Las applets NO se ejecutan en el navegador">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376902500584" ID="ID_1869139305" MODIFIED="1385572400522" TEXT="Se ejecutan en la sandbox de las applets, con algunas extensiones menores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376903445023" ID="ID_377730589" MODIFIED="1385572397669" TEXT="No requiere la firma de las aplicaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367751797963" ID="ID_198791146" MODIFIED="1385572399150" TEXT="No necesita la red para ejecutar las aplicaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376902805132" FOLDED="true" ID="ID_1921766760" MODIFIED="1385572393570" TEXT="Actualizaci&#xf3;n autom&#xe1;tica de versiones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376902878471" ID="ID_1547491158" MODIFIED="1376903388146" TEXT="Evitan los problemas de diferentes versiones de la aplicaci&#xf3;n"/>
<node CREATED="1376903123745" ID="ID_1909592690" MODIFIED="1376903233331" TEXT="Garantiza que se ejecuta la &#xfa;ltima versi&#xf3;n"/>
</node>
<node CREATED="1376903390778" ID="ID_681928700" MODIFIED="1377960107431" TEXT="Permite seleccionar el JRE con el que se ejecuta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376902949155" ID="ID_940677221" MODIFIED="1377960111846" TEXT="Usa JNLP (Java Network Launching Protocol) para especificar los requisitos de las aplicaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376903046758" ID="ID_1450294380" MODIFIED="1384678181102" TEXT="Pack 2000">
<node CREATED="1376903051642" ID="ID_1530834393" MODIFIED="1376903068644" TEXT="Compresi&#xf3;n de .jar con s&#xf3;lo las clases"/>
</node>
</node>
<node CREATED="1369723599315" ID="ID_1537367131" MODIFIED="1380794082854" TEXT="Compilaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
<node CREATED="1369723615812" ID="ID_1620777810" MODIFIED="1377960123827" TEXT="c&#xf3;digo intermedio bytecode">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369723655832" ID="ID_565685965" MODIFIED="1380794077682" TEXT="ejecutable en JVM mediante JIT">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1367751842854" ID="ID_569999471" MODIFIED="1380794062288" TEXT="DEBE EXISTIR JRE para la interpretaci&#xf3;n y ejecuci&#xf3;n del bytecode   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1367752798383" ID="ID_425896503" MODIFIED="1385572411868" POSITION="right" TEXT="Arquitectura Java EE">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>NO ES UN PRODUCTO: se trata de un conjunto de especificaciones de APIs Java. La responsabilidad de la implementaci&#243;n de cada API depende del fabricante. Describe: </b>
    </p>
    <p>
      <b>- Paquetes </b>
    </p>
    <p>
      <b>- Clases </b>
    </p>
    <p>
      <b>- Interfaces </b>
    </p>
    <p>
      
    </p>
    <p>
      <b>La &#250;ltima versi&#243;n es 6.0.</b>
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1382863900922" ID="ID_198228620" MODIFIED="1395691846826" TEXT="Esquema General">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1382863912592" ID="ID_619092502" MODIFIED="1382863965672">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Esquema%20general.png" />
  </body>
</html></richcontent>
<node CREATED="1380794099140" ID="ID_1144963385" MODIFIED="1384852501436" TEXT="Servidor de aplicaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1382862695909" ID="ID_233376647" MODIFIED="1385572427906" TEXT="Libres">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1380794112273" ID="ID_1643637628" MODIFIED="1382862679515" TEXT="Sun: Glassfish">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1380794126104" ID="ID_645398429" MODIFIED="1382864672149" TEXT="Red Hat: JBOSS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382864654299" ID="ID_1716701979" MODIFIED="1382864675585" TEXT="JBOSS Inc.: Wildfly">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382862654259" ID="ID_884444015" MODIFIED="1382862663363" TEXT="Apache: Ger&#xf3;nimo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382864498017" ID="ID_428643145" MODIFIED="1382864526480" TEXT="ObjectWeb: JONAS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382864705707" ID="ID_35106286" MODIFIED="1382864718068" TEXT="Linux: JEUS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1382862825306" ID="ID_89523519" MODIFIED="1385572429939" TEXT="Propietarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1382862831691" ID="ID_316714581" MODIFIED="1383161088501" TEXT="Oracle: Weblogic">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382864631693" ID="ID_1279745307" MODIFIED="1383161092106" TEXT="IBM: Websphere">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382862840645" ID="ID_1150367851" MODIFIED="1382862877182" TEXT="Oracle: Oracle Application Server">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1382864078761" ID="ID_159696488" MODIFIED="1395907041754" TEXT="Contenedores y APIs">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1382864135888" ID="ID_31909939" MODIFIED="1384852592882">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Arquitectura%20Java.gif" />
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367752857531" ID="ID_957865440" MODIFIED="1385572597383" TEXT="Contenedor Web (tomcat)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367752870879" ID="ID_398840416" MODIFIED="1380792655521" TEXT="servlets">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367752904778" ID="ID_1158933555" MODIFIED="1385572609811" TEXT="Clase httpservlet">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367752942756" ID="ID_1336987810" MODIFIED="1382003770672" TEXT="Objetos request y response">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1367752875206" ID="ID_1612898181" MODIFIED="1380792657208" TEXT="JSP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367752956229" ID="ID_1797655844" MODIFIED="1385572610757" TEXT="servlet + presentaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367753378967" ID="ID_611309328" MODIFIED="1382864197159" TEXT="Directivas JSP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369724569108" ID="ID_1137657526" MODIFIED="1377960318453" TEXT="page">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369724574404" ID="ID_562307513" MODIFIED="1377960322446" TEXT="include">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369724582587" ID="ID_1149170564" MODIFIED="1377960326565" TEXT="taglib">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1367753386486" ID="ID_211597578" MODIFIED="1382864199792" TEXT="Etiquetas JSP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369724724627" ID="ID_1993720646" MODIFIED="1383160756936" TEXT="useBean">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369724748080" ID="ID_1979890751" MODIFIED="1383160757800" TEXT="setProperty">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1367753383234" ID="ID_214357818" MODIFIED="1382864198624" TEXT="Elementos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369724625138" ID="ID_462695761" MODIFIED="1383160753722" TEXT="Declaraciones &lt;%!declaracion%&gt;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369724667201" ID="ID_780667134" MODIFIED="1383160754911" TEXT="Expresiones &lt;%=expresi&#xf3;n%&gt;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369724687068" ID="ID_1167260857" MODIFIED="1383160755560" TEXT="Scriptlets &lt;%%&gt;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372347307203" ID="ID_1645715181" MODIFIED="1385572621953" TEXT="JSTL (JSP Standard Tag Library)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Juego de etiquetas est&#225;ndar para JSPs de alto nivel
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1382865510719" ID="ID_801251158" MODIFIED="1385572638868" TEXT="JSF (Java Server Faces)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;librer&#237;a de etiquetas JSTL orientadas al dise&#241;o de interfaces gr&#225;ficos WEB (componentes gr&#225;ficos)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1382863630903" ID="ID_357402323" MODIFIED="1382863650909" TEXT="DA SOPORTE A LA CAPA WEB   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1367753393436" ID="ID_211172258" MODIFIED="1385572598727" TEXT="Contenedor EJB">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367753527863" ID="ID_1615818383" MODIFIED="1382003845162" TEXT="Distribuidos (RMI/IIOP): est&#xe1;ndar de comunicaci&#xf3;n sobre CORBA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367753557994" ID="ID_1212915424" MODIFIED="1382863609631" TEXT="Tipos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367753565552" ID="ID_1537335206" MODIFIED="1382863612643" TEXT="Session">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367753593774" ID="ID_349205037" MODIFIED="1377960420118" TEXT="Stateless">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367753599788" ID="ID_136094862" MODIFIED="1377960424548" TEXT="Statefull">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1367753608453" ID="ID_592380328" MODIFIED="1377960416327" TEXT="Message driven: como session, pero as&#xed;ncronos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367753569429" ID="ID_1955693595" MODIFIED="1382865152722" TEXT="Entity">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Reemplazados en Java EE 5.0 por entidades JPA</b>
    </p>
  </body>
</html></richcontent>
<arrowlink DESTINATION="ID_1847230362" ENDARROW="Default" ENDINCLINATION="332;0;" ID="Arrow_ID_1612476116" STARTARROW="None" STARTINCLINATION="332;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367753629698" ID="ID_167124875" MODIFIED="1380794179133" TEXT="Bean Managed Persistence (BMP)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367753640421" ID="ID_1833849890" MODIFIED="1377960432099" TEXT="Container Managed Persistence (CMP)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1382863566151" ID="ID_1070181414" MODIFIED="1382863604771" TEXT="Dan soporte a la LOGICA DE NEGOCIO    ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1384680111756" ID="ID_1096383737" MODIFIED="1384680144425" TEXT="Contenedor de aplicaci&#xf3;n cliente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384680125320" ID="ID_241431771" MODIFIED="1385572856858" TEXT="Contenedor de applets">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367751132773" ID="ID_1021402258" MODIFIED="1384682712801" TEXT="applets">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367751139693" ID="ID_1036029284" MODIFIED="1377960509475" TEXT="Incrustado HTML &lt;applet&gt;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367751235968" ID="ID_224051387" MODIFIED="1377960513172" TEXT="contenedor o sandbox">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367751262464" ID="ID_18903768" MODIFIED="1369723025663" TEXT="applet no seguro ni local">
<node CREATED="1367751268527" ID="ID_1198415755" MODIFIED="1367751424379" TEXT="librer&#xed;as">
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1367751316169" ID="ID_1093508769" MODIFIED="1367751421680" TEXT="ficheros locales">
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1367751326175" ID="ID_692010766" MODIFIED="1367751418810" TEXT="red">
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1367751363545" ID="ID_1401745469" MODIFIED="1367751416127" TEXT="propiedades">
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1367751379558" ID="ID_970647961" MODIFIED="1367751413459" TEXT="ventanas">
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1367751385120" ID="ID_8955888" MODIFIED="1367751410557" TEXT="conexiones m&#xe1;quina descarga">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1367751398356" ID="ID_949106481" MODIFIED="1367751407281" TEXT="m&#xe9;todos p&#xfa;blicos">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1367751504366" ID="ID_2603718" MODIFIED="1367751527068" TEXT="firma del applet">
<icon BUILTIN="back"/>
<node CREATED="1367751530914" ID="ID_265192315" MODIFIED="1367751535063" TEXT="seguro"/>
</node>
</node>
<node CREATED="1372342174975" ID="ID_90903520" MODIFIED="1380794302327" TEXT="Fuertemente tipado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367752229892" ID="ID_1774051381" MODIFIED="1377960563404" TEXT="Seguridad">
<node CREATED="1367752238950" ID="ID_1786831200" MODIFIED="1372348143326" TEXT="Comprobaci&#xf3;n en compilaci&#xf3;n y ejecuci&#xf3;n"/>
<node CREATED="1367752261092" ID="ID_475014402" MODIFIED="1367752265774" TEXT="punteros">
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1367752268666" ID="ID_377774981" MODIFIED="1367752321771" TEXT="Desbordamientos">
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1367752324304" ID="ID_980866721" MODIFIED="1367752332028" TEXT="M&#xe9;todos privados">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
</node>
<node CREATED="1367753761455" FOLDED="true" ID="ID_731794624" MODIFIED="1385573497100" TEXT="APIs">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367753814075" ID="ID_1847230362" MODIFIED="1385573448304" TEXT="Java Persistence: JPA. Acceso a bases de datos relacionales.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1382865191798" ID="ID_1401811024" MODIFIED="1382865330692" TEXT="Permite definir un mapeo objeto-relacional para tipos de entidades">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382865242051" ID="ID_939390883" MODIFIED="1384680742813" TEXT="Entidad: objeto Java que representa los datos almacenados en una tupla de BD   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382865277479" ID="ID_1969504655" MODIFIED="1382865332835" TEXT="Gestiona la consulta, carga, modificaci&#xf3;n y escritura de entidades">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382865313131" ID="ID_349560634" MODIFIED="1382865333752" TEXT="Se asienta sobre JDBC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1367753800559" ID="ID_216336017" MODIFIED="1377960441786" TEXT="JDBC ">
<arrowlink DESTINATION="ID_507770708" ENDARROW="Default" ENDINCLINATION="501;0;" ID="Arrow_ID_1926368788" STARTARROW="None" STARTINCLINATION="501;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="up"/>
</node>
<node CREATED="1367753827888" ID="ID_44460715" MODIFIED="1377960444610" TEXT="Mensajes: JMS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367753848832" ID="ID_151179551" MODIFIED="1385573449725" TEXT="Transacciones: JTA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367753892443" ID="ID_1867930436" MODIFIED="1367753909042" TEXT="Aplicaci&#xf3;n"/>
<node CREATED="1367753909937" ID="ID_8920862" MODIFIED="1367753916584" TEXT="Gestor de transacci&#xf3;n"/>
</node>
<node CREATED="1367753936277" ID="ID_689756527" MODIFIED="1380792736885" TEXT="CORBA: Java IDL usando el protocolo IIOP  ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367753951854" ID="ID_1714731458" MODIFIED="1377960447231" TEXT="Directorios y servicios de nombres: JNDI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367753962798" ID="ID_1367736628" MODIFIED="1377960448557" TEXT="Correo electr&#xf3;nico: Java Mail Technology">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367754087018" ID="ID_1854482244" MODIFIED="1385573461907" TEXT="Web services">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367754094812" ID="ID_1505900231" MODIFIED="1377961696910" TEXT="Parser de ficheros XML con modelo SAX o DOM: JAXP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367754130355" ID="ID_1701538199" MODIFIED="1377961699234" TEXT="Procesamiento XML: StAX. Permite vistas XML de datos no XML">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369733362082" ID="ID_978216007" MODIFIED="1369733381506" TEXT="Llamadas RPC">
<node CREATED="1369733385850" ID="ID_263885602" MODIFIED="1377961706894" TEXT="JAX-RPC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369733401537" ID="ID_29311174" MODIFIED="1382865633239" TEXT="JAX-WS (Java API for XML services)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
</node>
<node CREATED="1369733410466" ID="ID_1274856623" MODIFIED="1382865635578" TEXT="JAXB: vincular objetos Java con datos XML">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376398375209" ID="ID_243063640" MODIFIED="1376398386906" TEXT="NO permite CORBA"/>
</node>
</node>
<node CREATED="1367767490946" ID="ID_757711335" MODIFIED="1377961709094" TEXT="Acceso a registros UDDI: JAXR">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367767136611" ID="ID_70852208" MODIFIED="1377961711184" TEXT="Acceso a  mensajes con adjuntos seg&#xfa;n SOAP: SAAJ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384676607995" ID="ID_792654161" MODIFIED="1384676626701" TEXT="Programaci&#xf3;n SOAP: Axis">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369733671257" ID="ID_1639905959" MODIFIED="1377961715365" TEXT="Seguridad a nivel de mensaje: XWSS (WSS de OASIS)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1367754184965" ID="ID_1466101134" MODIFIED="1377960452847" TEXT="MIME: JAF">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367754263925" ID="ID_925976578" MODIFIED="1377960454017" TEXT="Conectores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367754282185" ID="ID_430216670" MODIFIED="1377960455078" TEXT="Usuarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367754310571" ID="ID_1041658293" MODIFIED="1377962030705" TEXT="Autenticaci&#xf3;n: JAAS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367754321887" ID="ID_1091592936" MODIFIED="1377962032931" TEXT="Control: JACC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1367751876511" FOLDED="true" ID="ID_1103208683" MODIFIED="1385573805816" TEXT="Interfaces">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367751919060" ID="ID_412667249" MODIFIED="1377960482175" TEXT="JNI (c&#xf3;digo nativo)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367752109936" ID="ID_811580444" MODIFIED="1385573501641" TEXT="Acceso a datos">
<node CREATED="1367752135653" ID="ID_507770708" MODIFIED="1385573503299" TEXT="JDBC">
<node CREATED="1367752149125" ID="ID_428387139" MODIFIED="1380792872609" TEXT="Tipo I: Driver NO java que hace llamadas a ODBC   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367752159411" ID="ID_538288162" MODIFIED="1380792896871" TEXT="Tipo II: Driver NO Java que llama a funciones nativas de la Base de datos     ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367752163709" ID="ID_714890903" MODIFIED="1377960499772" TEXT="Tipo III: Driver Java que se comunica con un Middleware">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367752169083" ID="ID_1101424164" MODIFIED="1377960502970" TEXT="Tipo IV: Driver Java que se comunica con la base de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1367755605780" FOLDED="true" ID="ID_916733495" MODIFIED="1385573639628" POSITION="right" TEXT="Distribuci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367754500088" ID="ID_1732254507" MODIFIED="1377960716604" TEXT="Ficheros WAR">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367754527539" ID="ID_1805644354" MODIFIED="1367754539975" TEXT="Librer&#xed;as"/>
<node CREATED="1367754540370" ID="ID_650389948" MODIFIED="1367754547065" TEXT="Servlets o JSPs"/>
<node CREATED="1367754547601" ID="ID_1959527672" MODIFIED="1367754556776" TEXT="HTML"/>
<node CREATED="1367754557062" ID="ID_1827989018" MODIFIED="1382021604821" TEXT="Deployment: web.xml">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1367750952872" ID="ID_789536378" MODIFIED="1377960721316" TEXT="Ficheros JAR">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367750960194" ID="ID_363447985" MODIFIED="1367750979900" TEXT="Distribuci&#xf3;n"/>
<node CREATED="1367750980748" ID="ID_1556850166" MODIFIED="1377960737259" TEXT="Clases im&#xe1;genes y sonidos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367750994827" ID="ID_80410235" MODIFIED="1377960730457" TEXT="Puede incluir META-INF">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367751006553" ID="ID_1518222659" MODIFIED="1377960734108" TEXT="MANIFEST.MF">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367751015856" ID="ID_1579682544" MODIFIED="1367751031411" TEXT="extensi&#xf3;n.sf"/>
<node CREATED="1367751035255" ID="ID_1320917343" MODIFIED="1367751042090" TEXT="firmas digitales"/>
</node>
<node CREATED="1367751070144" ID="ID_345327088" MODIFIED="1377960742048" TEXT="Comprimido LZW (mismo que ZIP)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1367754575447" ID="ID_1366679767" MODIFIED="1377960746728" TEXT="Ficheros EAR (EJB)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372346705869" ID="ID_554273559" MODIFIED="1372346729561" TEXT="Jigsaw: plataforma de servidor para demostraciones"/>
</node>
<node CREATED="1367751925963" FOLDED="true" ID="ID_1084321031" MODIFIED="1385573802858" POSITION="right" TEXT="Entornos de desarrollo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367751965782" ID="ID_1481071232" MODIFIED="1377767291136" TEXT="AWT (Abstract Window Toolkit)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377767185629" ID="ID_452457751" MODIFIED="1377767194362" TEXT="Compuesto de ">
<node CREATED="1377767194364" ID="ID_1510666295" MODIFIED="1377964408818" TEXT="Librer&#xed;as gr&#xe1;ficas"/>
<node CREATED="1377767210631" ID="ID_1491760391" MODIFIED="1377767218064" TEXT="Sistema de ventanas"/>
</node>
<node CREATED="1377767248473" ID="ID_724110951" MODIFIED="1377767281872" TEXT="Suministra una interfaz gr&#xe1;fica de usuario">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377767113442" ID="ID_1221463388" MODIFIED="1377767286440" TEXT="Depende de los SO">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377767268474" ID="ID_1382754453" MODIFIED="1377767288840" TEXT="Pesado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1367751975290" ID="ID_489848761" MODIFIED="1377767293192" TEXT="Swing: amplia la funcionalidad de AWT">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372334983047" ID="ID_494580805" MODIFIED="1377767295080" TEXT="Patr&#xf3;n MVC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1367752046639" ID="ID_830669165" MODIFIED="1377767297009" TEXT="SWT: Eclipse">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1367754684330" FOLDED="true" ID="ID_1426726192" MODIFIED="1385573754528" POSITION="right" TEXT="Frameworks">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367754841886" ID="ID_1380924559" MODIFIED="1384681256917" TEXT="Apache Struts: Implementa el MVC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369734172954" ID="ID_107159972" MODIFIED="1369734199877" TEXT="Organizaba JSPs y servlets"/>
<node CREATED="1369734228513" ID="ID_169591020" MODIFIED="1369734245740" TEXT="Separaci&#xf3;n del modelo en capas"/>
</node>
<node CREATED="1367755271275" ID="ID_210584640" MODIFIED="1384681260572" TEXT="WebWork">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367755309129" ID="ID_1760651114" MODIFIED="1379253324052" TEXT="Jer&#xe1;rquico, controlador central"/>
</node>
<node CREATED="1367754851174" ID="ID_737961603" MODIFIED="1384681261690" TEXT="Spring">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367755040958" ID="ID_1108899276" MODIFIED="1367755050585" TEXT="Basado en beans"/>
<node CREATED="1367755051324" ID="ID_175453328" MODIFIED="1367755067503" TEXT="Peque&#xf1;os frameworks">
<node CREATED="1382867571034" ID="ID_614877918" MODIFIED="1382867596314" TEXT="Seguridad: Spring Security"/>
</node>
</node>
<node CREATED="1367755143344" ID="ID_404933530" MODIFIED="1384681266828" TEXT="Hibernate">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367755212171" ID="ID_527382528" MODIFIED="1367755219942" TEXT="Basado en persistencia"/>
<node CREATED="1367755220400" ID="ID_823170347" MODIFIED="1369734302677" TEXT="mapeo de objetos">
<node CREATED="1367755241081" ID="ID_953490906" MODIFIED="1367755249334" TEXT="librer&#xed;a CGLIB"/>
</node>
</node>
<node CREATED="1367755402068" ID="ID_459676945" MODIFIED="1384681268083" TEXT="iBATIS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367755458493" ID="ID_431159250" MODIFIED="1367755471457" TEXT="Capa de persistencia">
<node CREATED="1367755555392" ID="ID_1075519697" MODIFIED="1367755569324" TEXT="Data Access Objects"/>
<node CREATED="1367755569783" ID="ID_1578408758" MODIFIED="1367755575182" TEXT="Data Mapper"/>
</node>
</node>
<node CREATED="1367755341708" ID="ID_466388144" MODIFIED="1384681272332" TEXT="Tapestry: modelo de componentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367755381825" ID="ID_1108558425" MODIFIED="1367755388254" TEXT="Especificaci&#xf3;n"/>
<node CREATED="1367755388759" ID="ID_763849117" MODIFIED="1367755391647" TEXT="Plantilla"/>
<node CREATED="1367755392043" ID="ID_249677788" MODIFIED="1367755396678" TEXT="Implementaci&#xf3;n"/>
</node>
<node CREATED="1367755477593" ID="ID_1908756063" MODIFIED="1383224967670" TEXT="Wicket: tambi&#xe9;n basado en componentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367755500112" ID="ID_1147899231" MODIFIED="1385573741051" TEXT="JSF: componentes de interfaz de usuario">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1380793938972" ID="ID_924498542" MODIFIED="1382008184576" TEXT="Usa JSP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1385573693833" ID="ID_211639680" MODIFIED="1385573717668" TEXT="Maverick">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</map>
