<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<attribute_registry SHOW_ATTRIBUTES="selected"/>
<node CREATED="1441616197856" ID="ID_1222163164" MODIFIED="1557071326966">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      MICROSOFT .NET
    </p>
  </body>
</html></richcontent>
<node CREATED="1441616321396" ID="ID_1225326926" MODIFIED="1441616325315" POSITION="right" TEXT="Apps distribuidas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441616904269" ID="ID_1381302898" MODIFIED="1441616919756" TEXT="Arq de capas">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441616920079" ID="ID_1511217680" MODIFIED="1445416142848" TEXT="Pol&#xed;ticas de seg, adm y coms son transversales a todas las capas">
<icon BUILTIN="yes"/>
</node>
<node CREATED="1441617707522" ID="ID_170187621" MODIFIED="1441617710707" TEXT="Presentaci&#xf3;n">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441621099628" ID="ID_1390073297" MODIFIED="1441621192214">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comps de UI (<u>interacc usuario</u>, valida datos, ASP.NET) y comps de proceso usuario (org acciones)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441617711193" ID="ID_574065933" MODIFIED="1441617719469" TEXT="Empresarial">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441621208302" ID="ID_1574485866" MODIFIED="1441621453088">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Workflows, comps empr (reglas de neg, .NET Enterp Serv o WS, <u>transaccs</u>), entidades empr (datos entre comps), interfaz de servs (ofrecen funcs a comps) y agentes de servs ( acceso a serv ext)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441617714403" ID="ID_77827654" MODIFIED="1441621445122">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Datos (comps l&#243;g de acceso)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441621357592" ID="ID_120549190" MODIFIED="1441621594125">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      ADO.NET o XML,&#160;no realizan transaccs ni invocan comps, funcs&#160; <u>CRUD</u>&#160;(Create/Read/Update/Delete)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1441617000972" ID="ID_482720447" MODIFIED="1441617004431" TEXT="Reqs dise&#xf1;o">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441617004435" ID="ID_1010725425" MODIFIED="1441617038465">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Funcionales: </i>func, accesib y flex / <i>Operativos:</i>&#160;escalab, dispo, mantenim, seg, rendim, fiab, adm
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441616326765" ID="ID_1611680725" MODIFIED="1441616329251" TEXT="Servicios Web">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441617071451" ID="ID_1719766939" MODIFIED="1441617246042">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comps sw para poder compartir datos con otros pgms por internet e <u>indep</u>&#160;de la plataf
    </p>
  </body>
</html></richcontent>
<node CREATED="1441617113941" ID="ID_1558666192" MODIFIED="1441617130908" TEXT="Est&#xe1;ndares ab, facilidad, reutiliz, B2B">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1441617141042" ID="ID_738802147" MODIFIED="1441617605569">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Protocolos:</i>&#160;XML (creaci&#243;n), WSDL (descrip), UDDI (localiz) y SOAP (comunic)&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1441616333854" ID="ID_1551849699" MODIFIED="1445415732664" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Plataforma .NET
    </p>
    <p>
      (v4.6)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441617266999" ID="ID_803945930" MODIFIED="1445415419635">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tecns para transformar Internet en una plataf&#160;<u>distrib a escala completa</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1441617356270" ID="ID_745135451" MODIFIED="1445415404219">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      F&#225;cil&#160;desarrollo, indep del leng y plataf hw, interop, f&#225;cil migraci&#243;n
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
<node CREATED="1445415404877" ID="ID_1509327862" MODIFIED="1445415410789" TEXT="Sobre Windows">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node CREATED="1441616338894" ID="ID_384184749" MODIFIED="1557071932290">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      .NET Framework
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441617415541" ID="ID_1634735831" MODIFIED="1445414388500">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Base</u>&#160;com&#250;n donde se construyen los comps de app (interpreta <u>OO</u>). No req c&#243;digo aparte
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1445414315777" ID="ID_82879481" MODIFIED="1557072661821">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: left">
      <img src="img/756px-Diagrama_NET.jpg" width="378" height="300" />
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1445415732647" ID="ID_1322858220" MODIFIED="1445415761770" TEXT="CLS">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445415455110" ID="ID_1005429657" MODIFIED="1445415802714">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Common Lang Spec) todos los lengs fuente deben cumplir estas especs (dif sintaxis pero =opers)
    </p>
  </body>
</html></richcontent>
<node CREATED="1445415519555" ID="ID_636701733" MODIFIED="1445415924334" TEXT="Permite desarr apps en partes con dif lengs">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1445415556227" ID="ID_1722208593" MODIFIED="1445416514638">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>CTS: </i>(Common Type Sys) declr, uso y adm de <u>tipos</u>&#160;en CLR para lengs compatibles
    </p>
  </body>
</html></richcontent>
<node CREATED="1445415501613" ID="ID_354023089" MODIFIED="1445415518373" TEXT="Java no cumple">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1445415748340" ID="ID_1467333016" MODIFIED="1557072080413" TEXT="CIL">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<node CREATED="1441617896588" ID="ID_307693125" MODIFIED="1445415891089">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Common&#160;Intermed Lang, antes MSIL)&#160;desarrolladores precompilan a este leng para ejec en CLR
    </p>
  </body>
</html></richcontent>
<node CREATED="1445415814388" ID="ID_410683325" MODIFIED="1445415820228" TEXT="Indep del leng">
<icon BUILTIN="xmag"/>
</node>
</node>
</node>
</node>
<node CREATED="1441619994464" ID="ID_1974503542" MODIFIED="1441619997372" TEXT="ASP.NET">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441620000931" ID="ID_582639574" MODIFIED="1445417005411">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Modelo de desarrollo</u>&#160;para crear una app Web (en serv IIS) aprovechando al m&#225;x el CLR
    </p>
  </body>
</html></richcontent>
<node CREATED="1441620055441" ID="ID_1089574383" MODIFIED="1441620063747" TEXT="Compatible con sintaxis ASP pero distinto">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1441620146216" ID="ID_603302517" MODIFIED="1445417331507">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Creaci&#243;n de UI</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1445417296144" ID="ID_1023034843" MODIFIED="1445417296149">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Windows Forms (apps escritorio, cliente pesado) o Web Forms (solo req naveg)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1445417331500" ID="ID_875702013" MODIFIED="1445417334340" TEXT="WPF">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445417298498" ID="ID_1798227480" MODIFIED="1445417860720">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Winds Present Found)&#160; <u>separa</u>&#160;present de l&#243;g de app (mejor exp visual). Leng <u>XAML</u>&#160;(eXt App)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1445417471024" ID="ID_1670458365" MODIFIED="1445417914655">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>WCF: </i>(Winds Comm Found) capa abstracci&#243;n permite creaci&#243;n de apps orientadas a <u>servs</u>&#160;(SOA)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1445417927423" ID="ID_1139483874" MODIFIED="1445417982130">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>WWF: </i>(Workflow) plantilla de pgmaci&#243;n, motor ejec y herrams para integrar&#160; <u>flujos</u>&#160;de trabajo en la app
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1445417994766" ID="ID_1680562444" MODIFIED="1445418079347">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Winds Cardspace: </i>t&#233;cnica de <u>id&#160;de usuarios</u>&#160;en una app (similar a MS Passport)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1445417582723" ID="ID_239510080" MODIFIED="1445418092500">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>LINQ: </i>(Lang-Integr Query) permite consultas de datos de&#160; <u>dif fuentes</u>&#160;(no estruct)&#8594; objs, XML, BDR...
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441619372650" ID="ID_1788001779" MODIFIED="1441619375751" TEXT="ADO.NET">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441619375756" ID="ID_512658735" MODIFIED="1441619963280">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comps sw para acceso a <u>datos sin conexi&#243;n</u>&#160;&#160;(+escalab)
    </p>
  </body>
</html></richcontent>
<node CREATED="1441619963276" ID="ID_689676538" MODIFIED="1441619981266" TEXT="Forma parte de la biblio &apos;System&apos;">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1441619586290" ID="ID_1385289023" MODIFIED="1445416885859">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>DataSet:</i>&#160;<u>acceso</u>&#160;a datos indep del origen y almac en <u>cach&#233;</u>&#160;(tablas en memo, evita mantener conexi&#243;n)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1441619660618" ID="ID_1282222410" MODIFIED="1445416932858">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>DataProvider: </i><u>manip</u>&#160;de datos. Objetos&#8594;
    </p>
  </body>
</html></richcontent>
<node CREATED="1441619699925" ID="ID_632315432" MODIFIED="1445416761021">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Connection (a BD), Command (consulta), DataReader (acc r&#225;pido) y DataAdapter (<u>puente</u>&#160;DataSet-BD)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1441619221074" ID="ID_1135357733" MODIFIED="1445419416907">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Libs de Clases Base
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441619229467" ID="ID_1795788186" MODIFIED="1441619354741">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>'System': </i>jer&#225;rq y extensible (resto se constuyen por herencia)
    </p>
  </body>
</html></richcontent>
<node CREATED="1441619354746" ID="ID_1188992905" MODIFIED="1441619358389">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#218;nica e indep del leng
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1441619282945" ID="ID_1550427015" MODIFIED="1445416692442">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Namespace:</i>&#160;id y org clases en <u>grupos</u>&#8594; System.WinForms,&#160;.Data, .Drawing, .XML, .Web
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441617463747" ID="ID_1778018977" MODIFIED="1441617465878" TEXT="CLR">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441617486905" ID="ID_1787223818" MODIFIED="1445414474281">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Common Lang Runtime)&#160;entorno de&#160; <u>ejec</u>&#160;de apps .NET en distintos lengs
    </p>
  </body>
</html></richcontent>
<node CREATED="1445414416276" ID="ID_62066152" MODIFIED="1445416988295">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Potencia <u>seg</u>&#160;de las apps y les ofrece memo (no acceden directo)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1441617519751" ID="ID_1831547098" MODIFIED="1445414891181">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Compiler CIL</u>&#160;(a c&#243;d m&#225;q), adm c&#243;digo y exceps, carga clases, depuraci&#243;n, verifica tipos, multiproc&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441620441826" ID="ID_1004508628" MODIFIED="1445419450145">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Visual Studio.NET
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441620453711" ID="ID_1292425597" MODIFIED="1441620471830">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Herram de desarrollo</u>&#160;de apps escritorio y web
    </p>
  </body>
</html></richcontent>
<node CREATED="1441620497455" ID="ID_1802470619" MODIFIED="1441620519576" TEXT="Lengs .NET, integra WS, plataf RAD (alta productiv), extensible y abierto">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1441617570465" ID="ID_1388448104" MODIFIED="1445416154682" TEXT="Ensamblados">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441618931620" ID="ID_619709647" MODIFIED="1445416096091">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Unidades fundam de <u>implem</u>&#160;de apps (completas), tras compilar c&#243;d fuente
    </p>
  </body>
</html></richcontent>
<node CREATED="1445416096096" ID="ID_1580800348" MODIFIED="1445416121910">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Objeto autocontenido&#8594; f&#225;cil instal, ctrl permisos
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1445416154666" ID="ID_1731732653" MODIFIED="1445416157295" TEXT="Comps">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445416158583" ID="ID_1519284062" MODIFIED="1445416386001">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Manifiesto (oblig)</i>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1445416237415" ID="ID_1362452546" MODIFIED="1445416373396">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Nombre y versi&#243;n, nombre seg (firmado), ref cultur, refs a otros ensamb, &#237;ndice ficheros (con huellas)&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1445416258356" ID="ID_1317845253" MODIFIED="1445416306557">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Metadatos, C&#243;digo CIL (puede ir firmado por desarrollador) y Recursos (pe imgs)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1441620183496" ID="ID_196687441" MODIFIED="1441620186573" TEXT="Seguridad">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441620186577" ID="ID_1646717553" MODIFIED="1441620423614">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mecs para <u>proteger&#160;recs y c&#243;digo</u>&#160;de usuarios no autorizados
    </p>
  </body>
</html></richcontent>
<node CREATED="1441620371690" ID="ID_1076736501" MODIFIED="1445418216801">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Jer&#225;rq:</i>&#160;<u>Machine.config</u>&#160;&#160;(XML en serv, pol&#237;tica global) y&#160; <u>Web.config</u>&#160;(en app web, concreta)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441620206100" ID="ID_553290331" MODIFIED="1441620435943">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Seg de apps web:</i>compara credenciales con permisos
    </p>
  </body>
</html></richcontent>
<node CREATED="1441620252955" ID="ID_1771923777" MODIFIED="1441620322696">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      MS Passport (perfil b&#225;sico, single sign-on), formularios (solicitudes), certifs de cliente, IP (IIS)&#160;&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441620335133" ID="ID_1477222512" MODIFIED="1441620364966">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Seg de acceso a c&#243;digo:</i>&#160;anti c&#243;digo m&#243;vil malicioso, basado en evidencias
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1557070857908" ID="ID_713700516" MODIFIED="1557071308715" POSITION="right" TEXT="Implementaciones .NET">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1557070976411" ID="ID_669868761" MODIFIED="1557071567180" TEXT=".NET Standard: define API com&#xfa;n de todas las implementaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1557070873557" ID="ID_1426266519" MODIFIED="1557071381412" TEXT=".NET Framework">
<node CREATED="1557071383592" ID="ID_1542767014" MODIFIED="1557071430996" TEXT="&quot;Cl&#xe1;sico&quot;"/>
<node CREATED="1557071432089" ID="ID_41900657" MODIFIED="1557071435172" TEXT="Runtime: CLR"/>
</node>
<node CREATED="1557070897574" ID="ID_920284697" MODIFIED="1557071441572" TEXT=".NET Core">
<node CREATED="1557071442776" ID="ID_539692060" MODIFIED="1557071558629" TEXT="Open source, multiOS"/>
<node CREATED="1557071451960" ID="ID_123741715" MODIFIED="1557071453988" TEXT="Runtime: coreCLR"/>
</node>
<node CREATED="1557070900533" ID="ID_1179256938" MODIFIED="1557071460661" TEXT="Mono">
<node CREATED="1557071463544" ID="ID_1562802141" MODIFIED="1557071472036" TEXT="Open source, origen en linux"/>
<node CREATED="1557071473833" ID="ID_602250680" MODIFIED="1557071475460" TEXT="Runtime: &quot;Mono runtime&quot;"/>
</node>
<node CREATED="1557070903222" ID="ID_984371001" MODIFIED="1557071482404" TEXT="UWP [Universal Windows Platform]">
<node CREATED="1557071484344" ID="ID_1404281674" MODIFIED="1557071544357" TEXT="Propio de Windows, para PCs, tabletas, etc."/>
<node CREATED="1557071545674" ID="ID_1570200567" MODIFIED="1557071546789" TEXT="Runtime &quot;.NET Native for UWP&quot;"/>
</node>
</node>
</node>
</map>
