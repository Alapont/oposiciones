<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1367755657980" ID="ID_646820484" MODIFIED="1377962502366" TEXT=".NET">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372331888558" ID="ID_1380317002" MODIFIED="1384853753155" POSITION="right" TEXT="Caracter&#xed;sticas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372331898379" ID="ID_537568460" MODIFIED="1382009909234" TEXT="Integrado en Windows">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372331905547" ID="ID_838124413" MODIFIED="1382009911226" TEXT="Orientado a objetos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1367755739874" ID="ID_946828259" MODIFIED="1480937337000" POSITION="right" TEXT="Infraestructura">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367755861805" ID="ID_1556146345" MODIFIED="1367755875673" TEXT="Lenguajes de programaci&#xf3;n">
<node CREATED="1367756541182" ID="ID_854933729" MODIFIED="1367756545488" TEXT="Visual Basic"/>
<node CREATED="1367756545852" ID="ID_1932793119" MODIFIED="1367756547976" TEXT="C++"/>
<node CREATED="1367756548637" ID="ID_1901847427" MODIFIED="1367756551775" TEXT="C#"/>
<node CREATED="1367756552420" ID="ID_744559183" MODIFIED="1367756559458" TEXT="Etc..."/>
</node>
<node CREATED="1367756002586" ID="ID_1978197105" MODIFIED="1367756009608" TEXT=".NET Remoting"/>
<node CREATED="1376042429693" ID="ID_761163320" MODIFIED="1384683887896" TEXT="2.0">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369678772592" ID="ID_140215404" MODIFIED="1480937334341" TEXT="CLR (Common Language Runtime)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
<icon BUILTIN="gohome"/>
<node CREATED="1372332638929" ID="ID_1149423980" MODIFIED="1480937322976" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="img/CLR.png" />
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1367756090081" ID="ID_1069271964" MODIFIED="1377673156111" TEXT="C&#xf3;digo intermedio (MSIL, actualmente CIL o IL)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
<icon BUILTIN="list"/>
<node CREATED="1369678779530" ID="ID_1810101394" MODIFIED="1377673158903" STYLE="bubble" TEXT="C&#xf3;digo nativo (ejecuci&#xf3;n por JIT)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
<icon BUILTIN="list"/>
</node>
</node>
<node CREATED="1369678974641" ID="ID_245600200" MODIFIED="1377673171583" TEXT="Basado en CLS (Common Language Specification)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="up"/>
</node>
<node CREATED="1375522490451" ID="ID_713215164" MODIFIED="1377673169480" TEXT="Usan CTS Para la especificaci&#xf3;n de tipos comunes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376042803870" ID="ID_1154459209" MODIFIED="1377673174359" TEXT="Seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376042831026" ID="ID_390505997" MODIFIED="1377673166752" TEXT="Garbage Collector y gesti&#xf3;n de la memoria">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1376043434715" ID="ID_1283861760" MODIFIED="1377672939950" TEXT="FCL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367755878596" ID="ID_1835110345" MODIFIED="1378112830395" TEXT="BCL (Base Class Library)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      API de alto nivel para permitir el acceso a los servicios del CLR a trav&#xe9;s de una jerarqu&#xed;a denominada espacio de nombres
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376045603300" ID="ID_1264049968" MODIFIED="1376045614881" TEXT="Componentes espec&#xed;ficos"/>
</node>
<node CREATED="1367756024569" ID="ID_1095608199" MODIFIED="1377673009225" TEXT="WinForms">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367755963126" ID="ID_422526529" MODIFIED="1385570765567" TEXT="ASP.NET">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367755976739" ID="ID_1000532414" MODIFIED="1378112828265" TEXT="Servicios Web XML">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367755989396" ID="ID_744208186" MODIFIED="1372332067400" TEXT="Enterprise Services COM+"/>
<node CREATED="1367756012406" ID="ID_422424069" MODIFIED="1367756024569" TEXT="Presentaci&#xf3;n">
<node CREATED="1367756030892" ID="ID_1114370010" MODIFIED="1377672949510" TEXT="Web Forms">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367756036766" ID="ID_804086446" MODIFIED="1377672951670" TEXT="Mobile Web Forms">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1367755951303" ID="ID_446122249" MODIFIED="1385570960806" TEXT="ADO.NET">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367756334089" ID="ID_1319039567" MODIFIED="1383223475131" STYLE="bubble" TEXT="Proveedores de datos .NET Framework">
<arrowlink DESTINATION="ID_863868559" ENDARROW="Default" ENDINCLINATION="148;0;" ID="Arrow_ID_689330652" STARTARROW="None" STARTINCLINATION="148;0;"/>
<arrowlink DESTINATION="ID_863868559" ENDARROW="Default" ENDINCLINATION="148;0;" ID="Arrow_ID_1748265420" STARTARROW="None" STARTINCLINATION="148;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367756377800" ID="ID_876574292" MODIFIED="1377673020505" TEXT="Connection: conectividad al origen de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367756383408" ID="ID_1294880410" MODIFIED="1377673022649" TEXT="Command: par&#xe1;metros para comandos de base de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367756387659" ID="ID_432195369" MODIFIED="1377673024609" TEXT="DataReader: secuencia de datos de alto rendimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367756395670" ID="ID_1606720456" MODIFIED="1377673026802" TEXT="DataAdapter: utiliza objetos Command para ejecutar sentencias SQL en el origen de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1382080039660" ID="ID_171891139" MODIFIED="1382080046780" TEXT="SelectCommand"/>
<node CREATED="1382080047210" ID="ID_688761931" MODIFIED="1382080053971" TEXT="InsertCommand"/>
<node CREATED="1382080054451" ID="ID_1528395050" MODIFIED="1382080061502" TEXT="UpdateCommand"/>
<node CREATED="1382080062252" ID="ID_102791790" MODIFIED="1382080068132" TEXT="DeleteCommand"/>
</node>
<node CREATED="1372332869204" ID="ID_92852005" MODIFIED="1372332891162" TEXT="Componentes dise&#xf1;ados para:">
<node CREATED="1372332891162" ID="ID_202524311" MODIFIED="1372332898899" TEXT="Manipulaci&#xf3;n de datos"/>
<node CREATED="1372332899235" ID="ID_1278035707" MODIFIED="1372332919562" TEXT="Acceso r&#xe1;pido de datos de s&#xf3;lo lectura y avance"/>
</node>
</node>
<node CREATED="1367756351381" ID="ID_863868559" MODIFIED="1380794834897" STYLE="bubble" TEXT="Dataset">
<linktarget COLOR="#b0b0b0" DESTINATION="ID_863868559" ENDARROW="Default" ENDINCLINATION="148;0;" ID="Arrow_ID_689330652" SOURCE="ID_1319039567" STARTARROW="None" STARTINCLINATION="148;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_863868559" ENDARROW="Default" ENDINCLINATION="148;0;" ID="Arrow_ID_1748265420" SOURCE="ID_1319039567" STARTARROW="None" STARTINCLINATION="148;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369678398172" ID="ID_181951898" MODIFIED="1377672969751" TEXT="DataTableCollection">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369678426565" ID="ID_134988377" MODIFIED="1377672975423" TEXT="DataTable">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369678432172" ID="ID_381542618" MODIFIED="1377672978503" TEXT="DataRowCollection">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369678446380" ID="ID_737727177" MODIFIED="1377672981631" TEXT="DataColumCollection">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369678453659" ID="ID_961725250" MODIFIED="1377672984175" TEXT="ConstraintCollection">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1372333059660" ID="ID_1355549774" MODIFIED="1377673016393" TEXT="Para acceder a datos independientemente del origen: XML, datos locales, etc.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372333199026" ID="ID_1416681926" MODIFIED="1480937308905">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="img/ADO.png" />
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1376042493262" ID="ID_1200240599" MODIFIED="1385571506699" TEXT="3.0">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376042260994" ID="ID_1455387421" MODIFIED="1377673039058" TEXT="Windows Presentation Foundation (WPF)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376042279025" ID="ID_1415255447" MODIFIED="1377673041218" TEXT="Windows Communication Foundation (WCF)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376042394828" ID="ID_209784037" MODIFIED="1377673043682" TEXT="Windows WorkFlow Foundation (WF)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376042512071" ID="ID_484933192" MODIFIED="1377673053874" TEXT="Card Space">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376042531368" ID="ID_340081340" MODIFIED="1377673071267" TEXT="3.5">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376042536154" ID="ID_337943917" MODIFIED="1377673056211" TEXT="ADO.NET Entity Framework">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372353968577" ID="ID_813455230" MODIFIED="1377673060315" TEXT="Extensiones LINQ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372353992900" ID="ID_14103928" MODIFIED="1374393176728" TEXT="Conjunto de caracter&#xed;sticas que proporcionan capacidades avanzadas de consulta a Visual Basic y C#"/>
<node CREATED="1372354008024" ID="ID_188520787" MODIFIED="1372354010741" TEXT="Conjuntos"/>
<node CREATED="1372354011019" ID="ID_369310561" MODIFIED="1372354017367" TEXT="Transformaciones"/>
</node>
<node CREATED="1376042598165" ID="ID_706422530" MODIFIED="1377673076900" TEXT="4.0">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376042602204" ID="ID_1187231726" MODIFIED="1376042612317" TEXT="Parallel LINQ"/>
<node CREATED="1376042612936" ID="ID_1391329565" MODIFIED="1376042625018" TEXT="Task Paralell Library"/>
</node>
</node>
</node>
</node>
<node CREATED="1376043054133" ID="ID_1956480746" MODIFIED="1480937314024">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="img/DotNET_Framework_Stack.Png" />
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1367756147565" ID="ID_1191453003" MODIFIED="1382009982457" POSITION="left" TEXT="Desarrollo: MS Visual Studio .NET">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367762544116" ID="ID_957294906" MODIFIED="1377673127590" POSITION="right" TEXT="Transacciones distribuidas: MS-DTC">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#xa0;Componente de Microsoft Windows para transacciones con m&#xfa;ltiples proveedores de datos
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367762571891" ID="ID_32982582" MODIFIED="1382009980255" POSITION="left" TEXT="Directorio: ADSI (Active Directory Services Interfaces)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372353289662" FOLDED="true" ID="ID_703945293" MODIFIED="1384853755275" POSITION="left" TEXT="Objetos ASP ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372353301994" ID="ID_1168933684" MODIFIED="1372353306841" TEXT="Application"/>
<node CREATED="1372353307473" ID="ID_1683644020" MODIFIED="1372353311928" TEXT="ASPError"/>
<node CREATED="1372353312988" ID="ID_1215499408" MODIFIED="1372353315366" TEXT="Session"/>
<node CREATED="1372353315849" ID="ID_1764965429" MODIFIED="1372353318360" TEXT="Request"/>
<node CREATED="1372353318838" ID="ID_674449378" MODIFIED="1372353323708" TEXT="Response"/>
<node CREATED="1372353324211" ID="ID_1255730771" MODIFIED="1372353329521" TEXT="Server"/>
<node CREATED="1372353329965" ID="ID_1838566678" MODIFIED="1372353335776" TEXT="ObjectContext"/>
</node>
</node>
</map>
