<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1448046152154" ID="ID_278725370" MODIFIED="1517571498823">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T86&#160;SISTEMAS
    </p>
    <p style="text-align: center">
      ESPECIALES
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1448046569761" ID="ID_103477574" MODIFIED="1448046585771" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sistemas RT
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448046664069" ID="ID_737257121" MODIFIED="1448046778092">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Rx info del <u>estado</u>&#160;de su entorno y act&#250;an sobre &#233;l si detectan&#160; <u>desviaciones</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1448046767539" ID="ID_1678692081" MODIFIED="1448046767543">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      App: s&#170; de control
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1448046729704" ID="ID_666512551" MODIFIED="1448046814963">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Restriccs en <u>t de resp</u>&#160;y ejec de procs, info actualiz, <u>inactivo</u>&#160; para att con +rapidez, alto <u>rendim</u>&#160;hw-sw
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<icon BUILTIN="messagebox_warning"/>
</node>
<node CREATED="1448046826533" ID="ID_643473511" MODIFIED="1448046829321" TEXT="SO RT">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448046829326" ID="ID_577828767" MODIFIED="1448046870065">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Manejo eficaz de IRQs y priorids (modif en ejec), memo <u>real</u>&#160;(no virtual) y gesti&#243;n FIFO
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1448046872544" ID="ID_546535927" MODIFIED="1448046881020">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Com entre procesos
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448046881239" ID="ID_1655963385" MODIFIED="1448046897076">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Exclusi&#243;n mutua (recs no compartibles), sync y bloqueo (evitar 'deadlock')
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1448046897729" ID="ID_1677074290" MODIFIED="1448046942293" TEXT="Sem&#xe1;foro">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="go"/>
<node CREATED="1448046904672" ID="ID_1089005186" MODIFIED="1448047035987">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Var entera no neg (0 &#243; 1) y compartida por procs, resuelve probls de com
    </p>
  </body>
</html></richcontent>
<node CREATED="1448047748386" ID="ID_1884407104" MODIFIED="1448098025801">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      = q M&#250;tex
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1448046946015" ID="ID_671824877" MODIFIED="1448046949343">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ops
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448046949438" ID="ID_1385521596" MODIFIED="1448046969458">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>SIGNAL:</i>&#160;valor <u>+1</u>. Si era 0, pone en marcha un proceso detenido
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1448046970251" ID="ID_1457821563" MODIFIED="1448047745645">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>WAIT:</i>&#160;valor <u>-1</u>. Si era 0, detiene proceso (no le deja entrar xq el recurso ya est&#225; ocupado)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1448046588768" ID="ID_630871667" MODIFIED="1448046592607" TEXT="An&#xe1;lisis">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448047071371" ID="ID_1047586727" MODIFIED="1448047077928" TEXT="Redes de Petri">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448047077933" ID="ID_359625408" MODIFIED="1448047094078">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Grafo de <u>flujo de se&#241;ales</u>, describen la concurrencia
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1448047096371" ID="ID_1482777806" MODIFIED="1448047100763">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Elems
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448047101045" ID="ID_723250156" MODIFIED="1448047130499">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lugares (conectados a Transiciones) y Testigos
    </p>
  </body>
</html></richcontent>
<node CREATED="1448047110697" ID="ID_233577930" MODIFIED="1448047235560">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lugares reciben Testigos
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1448047235567" ID="ID_814041745" MODIFIED="1448047235570">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cuando todos tienen, Transici&#243;n de 'dispara' pasando Testigos a Lugares q cuelgan de la Transici&#243;n
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1448046593145" ID="ID_1620912919" MODIFIED="1448098073663">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Dise&#241;o</b>
    </p>
    <p>
      (Estructurado)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448098081903" ID="ID_1613579672" MODIFIED="1448098084271" TEXT="Reqs">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448098084276" ID="ID_164984866" MODIFIED="1448098163061">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Consistencia, funcionalidad y rapidez
    </p>
  </body>
</html></richcontent>
<node CREATED="1448098142109" ID="ID_1744140504" MODIFIED="1448098172309">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Implica&#8594;&#160;sw predecible, fiable, sync y div en tareas, modular, interfaz y mantenible
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1448098184566" ID="ID_1529249506" MODIFIED="1448098226569">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Dise&#241;o orientado al&#160; <u>flujo</u>&#160;de datos, com y sync de tareas y ocultaci&#243;n de info (entre m&#243;dulos)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1448046615301" ID="ID_1477203251" MODIFIED="1448098357641" TEXT="DARTS">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<node CREATED="1448098238337" ID="ID_866857039" MODIFIED="1448098271367">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Design Approach for RT Sys) m&#233;todo de dise&#241;o, extensi&#243;n del An&#225;lisis y Dise&#241;o Estructurados&#160;
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1448098276862" ID="ID_858343334" MODIFIED="1448098283395">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Fases
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448098283503" ID="ID_749563320" MODIFIED="1448098416297">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Espec de reqs&#8594; An&#225;lisis <u>DFD</u>&#8594; Descomp en <u>tareas</u>&#160;(funcs cr&#237;ticas en el t)&#8594;
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1448098338404" ID="ID_1298945495" MODIFIED="1448098338408">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#8594;Def de Interfaces (com y sync entre tareas)&#8594; Dise&#241;o de tareas (con un Gestor de Transics de Estado)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1448046574837" ID="ID_1645749403" MODIFIED="1448046586208" POSITION="right" TEXT="Transaccionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448098622978" ID="ID_1405580099" MODIFIED="1448098665099">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Incluyen ops muy bien&#160; <u>estructuradas</u>&#160;y de rutina (nivel op) para procesar <u>grandes vols</u>&#160;de info
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
</node>
<node CREATED="1448098719900" ID="ID_98530602" MODIFIED="1448098775193">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Generan <u>informes</u>&#8594;
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="list"/>
<node CREATED="1448098770523" ID="ID_818260130" MODIFIED="1448098794046">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Docs de acci&#243;n (inician otra transac), docs de info (confirman), reg de
    </p>
    <p>
      transacs (listas), elaborados (errores o pendientes) y de ctrl (impacto)
    </p>
  </body>
</html></richcontent>
<node CREATED="1448098694517" ID="ID_1808330608" MODIFIED="1448098708769" TEXT="No dan apoyo a la toma de decisiones">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1448046621327" ID="ID_937975613" MODIFIED="1448046624678" TEXT="Transacci&#xf3;n">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448098437804" ID="ID_1282033659" MODIFIED="1448098463600">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Conj de <u>eventos</u>&#160;como unidad <u>indiv</u>&#160;de trabajo (todos tienen &#233;xito o ninguno)
    </p>
  </body>
</html></richcontent>
<node CREATED="1448098467058" ID="ID_914215683" MODIFIED="1448098502412">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Involucra el&#160; <u>traslado</u>&#160;de algo de <u>valor</u>&#160;entre entidades
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1448098548747" ID="ID_1948805121" MODIFIED="1448098565190">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      App: negs&#8594; ejec r&#225;pida y m&#237;n riesgos
    </p>
  </body>
</html></richcontent>
<node CREATED="1448098566219" ID="ID_1738274605" MODIFIED="1448098587372">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Tipos:</i>&#160;rec&#237;procas (cada particip rx y sacrifica un valor, pe venta) o no
    </p>
  </body>
</html></richcontent>
<node CREATED="1448098588776" ID="ID_1229617781" MODIFIED="1448098604067">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Props: <u>ACID</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1448098848756" ID="ID_1985005037" MODIFIED="1448098853592" TEXT="Monitores de transac">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448098853597" ID="ID_1225441988" MODIFIED="1448098880267">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comps de soporte para <u>construir y config apps</u>&#160;&#160;c/s confiables de transacs distrib
    </p>
  </body>
</html></richcontent>
<node CREATED="1448098881001" ID="ID_1205282312" MODIFIED="1448098896111" TEXT="Garantizan props ACID y alto rendim">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1448098898613" ID="ID_220410637" MODIFIED="1448098900560" TEXT="OLTP">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448098900565" ID="ID_1960047771" MODIFIED="1448099001639">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Online Transac Process) procesa info en RT&#8594; actualiz&#160; <u>fiable</u>&#160;de la BD en cada transac e <u>integridad</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1448098959819" ID="ID_507798824" MODIFIED="1448098985273">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Req s&#170; abierto y compat con arq/SO
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1448046629965" ID="ID_869695007" MODIFIED="1448099258625">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      An&#225;lisis
    </p>
    <p>
      y Dise&#241;o
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448099017201" ID="ID_1300682448" MODIFIED="1448099019631" TEXT="DFD">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448099019636" ID="ID_24753148" MODIFIED="1448099113993">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Transac</u>&#160;(elem de datos) desencadena el<u>Flujo</u>&#160;de Transac a lo largo de varios <u>Caminos</u>&#160;de acci&#243;n
    </p>
  </body>
</html></richcontent>
<node CREATED="1448099261106" ID="ID_1847103570" MODIFIED="1448099592708" TEXT="Desde el Centro de Transac"/>
</node>
</node>
<node CREATED="1448099501197" ID="ID_295581817" MODIFIED="1517571182888">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="transac.png" />
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1448099131002" ID="ID_1225694542" MODIFIED="1448099133011" TEXT="Etapas">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448099133015" ID="ID_1629573711" MODIFIED="1448099602850">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Id flujos</u>&#160;de transacc y transformaci&#243;n
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1448099537566" ID="ID_74371746" MODIFIED="1448099542212">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Id Centro de Transac, Transac y Caminos
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1448099542217" ID="ID_1753092624" MODIFIED="1448099557440">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Transformar flujos a estructuras sw- <u>m&#243;dulos</u>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1448099552288" ID="ID_1574281851" MODIFIED="1448099562127">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Factorizar/<u>descomp</u>&#160;transac en &#225;rbol de funcs
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1448099562133" ID="ID_1083289359" MODIFIED="1448099562136">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Refinar</u>&#160;(bajo acopl, alta cohesi&#243;n...)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
