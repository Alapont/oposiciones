<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1369949323261" ID="ID_328269164" MODIFIED="1517571123342" TEXT="Diagrama de Flujo de Datos (DeMarco)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Crean un modelo funcional del sistema
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369949862868" ID="ID_1833358441" MODIFIED="1397801120655" POSITION="right" TEXT="Componentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369949867064" ID="ID_378918008" MODIFIED="1517571722435" TEXT="Procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369949928842" ID="ID_1260049919" MODIFIED="1369949938559" TEXT="Explotar la burbuja"/>
<node CREATED="1369949979188" ID="ID_74340157" MODIFIED="1369949982795" TEXT="Reglas">
<node CREATED="1369949982798" ID="ID_576504773" MODIFIED="1376644211216" TEXT="Transformaci&#xf3;n de los datos ">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      El proceso debe poder generar las salidas a partir de las entradas
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1369949998703" ID="ID_974726376" MODIFIED="1370082790587" TEXT="P&#xe9;rdida de informaci&#xf3;n: entran y salen datos"/>
</node>
</node>
<node CREATED="1369949883101" ID="ID_172665526" MODIFIED="1517571723562" TEXT="Almacenes de Datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369950070827" ID="ID_1008298981" MODIFIED="1369950083349" TEXT=" Normalmente en el nivel m&#xe1;s bajo de la jerarqu&#xed;a"/>
<node CREATED="1376644111775" ID="ID_174971444" MODIFIED="1378201312955" TEXT="Su contenido debe especificarse en el DD">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370082796588" ID="ID_840560945" MODIFIED="1378201314899" TEXT="Pueden existir en todos los niveles">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372690974186" ID="ID_1176048828" MODIFIED="1384613851165" TEXT="Por convenio, si el flujo es etiquetado, se supone que el almacen  es un registro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1369949888318" ID="ID_960376256" MODIFIED="1397801122511" TEXT="Flujos de Datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369950193327" ID="ID_1542855934" MODIFIED="1378201324028" TEXT="Entidades externas con procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369950201443" ID="ID_1769181525" MODIFIED="1378201325051" TEXT="Procesos entre ellos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369950252324" ID="ID_34106543" MODIFIED="1378201326667" TEXT="Procesos con almacenes de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369983601642" ID="ID_1641104798" MODIFIED="1379442482893" TEXT="Consulta. Flecha del almacen al proceso.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369984329026" ID="ID_424025964" MODIFIED="1379442486122" TEXT="Actualizaci&#xf3;n. Flecha del proceso al almac&#xe9;n.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369984335818" ID="ID_1175399233" MODIFIED="1379442489507" TEXT="Di&#xe1;logo. Flecha en los dos sentidos: Consulta + actualizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1369984417010" ID="ID_166363952" MODIFIED="1384613866309" TEXT="Tipo de informaci&#xf3;n en el flujo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369984435223" ID="ID_1908445609" MODIFIED="1378201330156" TEXT="Elemento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369984439665" ID="ID_229668759" MODIFIED="1378201331212" TEXT="Grupo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369984443530" ID="ID_1210918856" MODIFIED="1378201332340" TEXT="Par de di&#xe1;logo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369984458404" ID="ID_1586049297" MODIFIED="1369984466369" TEXT="Iniciador de la comunicaci&#xf3;n"/>
<node CREATED="1369984466698" ID="ID_1103664340" MODIFIED="1369984472969" TEXT="Respuesta obtenida"/>
</node>
<node CREATED="1369984476521" ID="ID_1530768541" MODIFIED="1378201333988" TEXT="M&#xfa;ltiple: varios flujos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1369949894461" ID="ID_174401966" MODIFIED="1384613880205" TEXT="Entidades Externas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369950130431" ID="ID_672320857" MODIFIED="1378210119871" TEXT="Generalmente en el diagrama de contexto">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se pueden repetir varias veces en el digrama de contexto o en otros niveles para mejorar la legibilidad
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369950150981" ID="ID_1909714139" MODIFIED="1384613887136" TEXT="Tipos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369950092776" ID="ID_78333902" MODIFIED="1383134476128" TEXT="Consumidores de Informaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369950032533" ID="ID_1204113964" MODIFIED="1383134477440" TEXT="Generadores de Informaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369950100873" ID="ID_322988771" MODIFIED="1378201344380" TEXT="Generadores y Consumidores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1369984493345" ID="ID_687438918" MODIFIED="1397801149764" POSITION="right" TEXT="Ampliaci&#xf3;n de Ward y Mellon">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369984562289" ID="ID_1801736749" MODIFIED="1369984578624" TEXT="Necesidades no cubiertas">
<node CREATED="1369984580041" ID="ID_561872258" MODIFIED="1378201353957" TEXT="Flujos de datos de manera continua. Doble flecha">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369984592322" ID="ID_1854662440" MODIFIED="1378201357597" TEXT="Informaci&#xf3;n de control. Flechas y procesos discontinuos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369984599065" ID="ID_500578008" MODIFIED="1378201382350" TEXT="Ocurrencias m&#xfa;ltiples">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369984606610" ID="ID_1601254646" MODIFIED="1369984618544" TEXT="Estados del sistema"/>
</node>
<node CREATED="1369984651121" ID="ID_1532831544" MODIFIED="1369984658752" TEXT="Nuevos elementos">
<node CREATED="1369984671282" ID="ID_1096708261" MODIFIED="1378201368245" TEXT="Flujos de datos continuos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369984683082" ID="ID_219559942" MODIFIED="1378201371974" TEXT="Procesos de control">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369984698506" ID="ID_1580088255" MODIFIED="1378201373062" TEXT="Flujos de control">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369984703465" ID="ID_844960647" MODIFIED="1378201376550" TEXT="Almac&#xe9;n de control">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369984711818" ID="ID_1994935835" MODIFIED="1378201378877" TEXT="Procesos m&#xfa;ltiples">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1369984789705" ID="ID_1907095244" MODIFIED="1384615412980" TEXT="Diagramas de flujo de control DFCs">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384615415943" ID="ID_1929837501" MODIFIED="1397801164288" TEXT="HATLEY propuso representar los flujos de datos y de control en diagramas separados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372674052052" ID="ID_1285851020" MODIFIED="1372674058810">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="DFD.%20Ward%20y%20Mellor.png"/>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1369987783090" ID="ID_1327997219" MODIFIED="1383134500273" POSITION="right" TEXT="Descomposici&#xf3;n en niveles">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369987795393" FOLDED="true" ID="ID_423458482" MODIFIED="1384614237025" TEXT="Jerarqu&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369987809733" ID="ID_962418844" MODIFIED="1378201390263" TEXT="Diagrama de Contexto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369987825586" ID="ID_1152744901" MODIFIED="1378201392511" TEXT="Diagrama de Subsistemas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369987833915" ID="ID_1283455810" MODIFIED="1378201393455" TEXT="Diagramas de Funciones de los Subsistemas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369987851057" ID="ID_227011748" MODIFIED="1378201394694" TEXT="Diagrama de Subfunciones de las Funciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369987863986" ID="ID_331016635" MODIFIED="1378201396238" TEXT="Diagrama de Procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1369987939594" ID="ID_1164976148" MODIFIED="1384618752930" TEXT="Procesos elementales, primitivas o at&#xf3;micos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369994463667" ID="ID_51193161" MODIFIED="1383134539069" TEXT="Identificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369987945465" ID="ID_1128396037" MODIFIED="1383134529573" TEXT="Tipo de Acceso  de la Funci&#xf3;n a los Datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369987971562" ID="ID_649168484" MODIFIED="1383134527173" TEXT="Tipo de Proceso: Alta, Baja, modificaci&#xf3;n o Consulta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369987992819" ID="ID_697890086" MODIFIED="1383134532013" TEXT="Tipo de Tratamiento: Batch u Online">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369987976730" ID="ID_471859329" MODIFIED="1383134536661" TEXT="Especificaci&#xf3;n de la Actividad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384615480160" ID="ID_1715645119" MODIFIED="1384615535570" TEXT="Puede hacerse especificaci&#xf3;n textual o pseudoc&#xf3;digo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384615504144" ID="ID_761051281" MODIFIED="1384615532870" TEXT="Ecuaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384615510766" ID="ID_1498718373" MODIFIED="1384615533630" TEXT="Diagramas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384615515386" ID="ID_366728773" MODIFIED="1384615534187" TEXT="Referencia del algoritmo a emplear">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384615525330" ID="ID_667385072" MODIFIED="1384615534746" TEXT="Tablas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1369988051946" ID="ID_1994538911" MODIFIED="1383134534085" TEXT="Frecuencia de ejecuci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370083251383" ID="ID_1779933109" MODIFIED="1378201461786" TEXT="Pueden aparecer en cualquier nivel">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369994514058" ID="ID_1704772860" MODIFIED="1384614246414" TEXT="Comprobaciones de Consistencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369994544951" ID="ID_1057103234" MODIFIED="1384614558813" TEXT="Consistencia interna">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369994560218" ID="ID_1250791745" MODIFIED="1384615882067" TEXT="Todos los flujos deben estar en el Diccionario de Datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369994576120" ID="ID_1043240567" MODIFIED="1378201478082" TEXT="Balanceo">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se comprueba que todos los flujos de entrada y de salida del nivel n+1 aparecen en el nivel N
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369994590184" ID="ID_1420433606" MODIFIED="1378201482042" TEXT="Datos de identificaci&#xf3;n rellenos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1369994598303" ID="ID_219954832" MODIFIED="1384614560158" TEXT="Consistencia entre m&#xf3;dulos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369994647641" ID="ID_1644206132" MODIFIED="1378201454145" TEXT="Los almacenes deben formar parte del modelo de datos">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Un almac&#xe9;n se puede puede corresponder con una entidad, atributos o varias entidades relacionadas
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369994650829" ID="ID_1463837117" MODIFIED="1378201488882" TEXT="Todas las entidades del MD deben ser accedidas por un proceso primitivo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1369993530581" ID="ID_1226068693" MODIFIED="1384614564476" TEXT="Elaboraci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369994317644" ID="ID_1278421316" MODIFIED="1384614588026" TEXT="Reglas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369993539171" ID="ID_714158271" MODIFIED="1384614582054" TEXT="Nombre &#xfa;nicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369993549879" ID="ID_321379462" MODIFIED="1384614581281" TEXT="No decisiones l&#xf3;gicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369993560541" ID="ID_249456767" MODIFIED="1384614579610" TEXT="Sin detalle">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369993566680" ID="ID_1454797533" MODIFIED="1378201509396" TEXT="No m&#xe1;s de 7 procesos en un diagrama">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378207618374" ID="ID_401313575" MODIFIED="1378207684331" TEXT="An&#xe1;lisis de transacci&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si de un proceso se derivan varios flujos de datos
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1369994336667" ID="ID_1974097480" MODIFIED="1384614586997" TEXT="Pasos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369994340810" ID="ID_539710989" MODIFIED="1384614584372" TEXT="Elaboraci&#xf3;n del DFD de Contexto (n&#xfa;mero 0)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369994353873" ID="ID_410019426" MODIFIED="1384614585240" TEXT="Expansi&#xf3;n de la Burbuja de Contexto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369994384395" ID="ID_1070535554" MODIFIED="1384614586022" TEXT="Expansi&#xf3;n de la Burbuja de Nivel N para Crear DFD N+1">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369994412264" ID="ID_1000771340" MODIFIED="1378201504651" TEXT="Al menos deben existir 2 procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369994428716" ID="ID_1110456788" MODIFIED="1378201507355" TEXT="No m&#xe1;s de 7 burbujas por DFD">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
