<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1369829560308" ID="ID_1552310289" MODIFIED="1481477497855" TEXT="BPM">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1481475760092" ID="ID_663131622" MODIFIED="1481477497855" POSITION="right" TEXT="BPM (Business Process Management)">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1427650070181" ID="ID_683654828" MODIFIED="1481477497855" TEXT="Gesti&#xf3;n sistem&#xe1;tica de procesos de negocio">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1427650089342" ID="ID_126959134" MODIFIED="1481477497855" TEXT="Optimizar y adaptar procesos, colaborar con roles, patrones, simular escenarios, m&#xe9;tricas">
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1427650174184" ID="ID_1107640747" MODIFIED="1481477497855" TEXT="Proceso: tareas interrelacionadas para obtener producto">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1427650214575" ID="ID_742512057" MODIFIED="1481477497855" TEXT="Orquestaci&#xf3;n">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1427650228336" ID="ID_1469897793" MODIFIED="1481477497855" TEXT="Controlado por &#xfa;nica entidad">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1427650259351" ID="ID_774113486" MODIFIED="1481477497855" TEXT="S&#xaa; jer&#xe1;rquico">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1427650242526" ID="ID_666058097" MODIFIED="1481477497855" TEXT="Se componen servs para generar un nuevo proceso">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1427650264616" ID="ID_1062620288" MODIFIED="1481477497855" TEXT="Coreograf&#xed;a">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1427650268736" ID="ID_867109477" MODIFIED="1481477497855" TEXT="Requiere protocolos de mensajes">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1427650314955" ID="ID_1811777803" MODIFIED="1481477497855" TEXT="S&#xaa; entre pares">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1427650242526" ID="ID_1790474203" MODIFIED="1481477497855" TEXT="Servs para permitir colab entre negocios">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1427650847526" ID="ID_231553335" MODIFIED="1481477497840" TEXT="Reengineering (BPR)">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1427650856206" ID="ID_1140909597" MODIFIED="1481477497840" TEXT="Reconcepci&#xf3;n y redise&#xf1;o radical de procesos de negocios para mejorar coste, calidad, serv y rapidez">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1369829718312" ID="ID_1933563329" MODIFIED="1481477497840" POSITION="right" TEXT="BPMN (Business Process Modeling Notation)">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1369829787965" ID="ID_702977621" MODIFIED="1481477497840" TEXT="Notaci&#xf3;n gr&#xe1;fica est&#xe1;ndar para modelar procesos de negocio (workflow)">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1481477376118" ID="ID_1248840620" MODIFIED="1481477497840" TEXT="Incluye pasos y actividades, pero sin lenguaje">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1384435821531" ID="ID_1263403143" MODIFIED="1481477497840" TEXT="Estandarizado por BPMI (Business Process Management Initiative), actualmente OMG">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427650348561" ID="ID_1906488835" MODIFIED="1481477644757" TEXT="Versi&#xf3;n actual v2.0.2 (diciembre 2013)">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427650647889" ID="ID_87751759" MODIFIED="1481477497840" TEXT="Diagramas: objs de flujo y conexi&#xf3;n, swimlanes (procesos paralelos) y artefactos ">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1369853711721" ID="ID_885699408" MODIFIED="1481477497840" POSITION="right" TEXT="Workflow - Modelo de Referencia WfMC">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1369853757266" ID="ID_84059981" MODIFIED="1481477497840" TEXT="Motor de workflow: control de la ejecuci&#xf3;n">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369853810495" ID="ID_34484669" MODIFIED="1481477497840" TEXT="Servicio de representaci&#xf3;n de Workflow (Workflow Enactment Service)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="rgb(0, 0, 0)" face="Times New Roman">A software service that may consist of one or more workflow engines in order to create, manage and execute workflow instances. Applications may interface to this service via the workflow application programming interface (WAPI). </font>
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1378133950016" ID="ID_896195843" MODIFIED="1481477497840" TEXT="Contiene varios motores de Workflow">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1369853857508" ID="ID_634685619" MODIFIED="1481477497840" TEXT="Interfaz de programaci&#xf3;n de aplicaciones (WAPI)">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1369853875629" ID="ID_866132285" MODIFIED="1481477497840" TEXT="Herramientas de definici&#xf3;n de procesos (interfaz 1)">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1369829718312" ID="ID_253722684" MODIFIED="1481477497840" TEXT="BPMN">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369829960496" ID="ID_1811103673" MODIFIED="1481477497840" TEXT="Necesita una expresi&#xf3;n program&#xe1;tica">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1369829816129" ID="ID_488001649" MODIFIED="1481477497840" TEXT="BPML (Business Process Modeling Language)">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1384443291911" ID="ID_1810224187" MODIFIED="1481477497840" TEXT="Obsoleto, pasa a BPEL">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1369829903209" ID="ID_990094313" MODIFIED="1481477497840" TEXT="BPEL (Business Processs Execution Language)">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1369830107049" ID="ID_354705120" MODIFIED="1481477497840" TEXT="Basado en XML">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369830139297" ID="ID_465248109" MODIFIED="1481477497840" TEXT="Est&#xe1;ndarizado por OASIS (Organization for the Advancement of Structured Information Standards)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Organization for the Advancement of Structured Information Standards</b>
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369830156096" ID="ID_1517108654" MODIFIED="1481477497840" TEXT="Lenguaje de orquestaci&#xf3;n: enfocado a un participante">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369830210497" ID="ID_1346613150" MODIFIED="1481477497840" TEXT="Servicios Web + l&#xf3;gica ">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369830234425" ID="ID_1561117143" MODIFIED="1481477497840" TEXT="WDSL 1.1 + XML Schema  para describir los mensajes">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372503489531" ID="ID_980415354" MODIFIED="1481477497840" TEXT="No existe una notificaci&#xf3;n gr&#xe1;fica est&#xe1;ndar">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1369829926936" ID="ID_1208553666" MODIFIED="1481477497840" TEXT="XPDL (XML Process Definition Language)">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1369829943051" ID="ID_47607754" MODIFIED="1481477497840" TEXT="Creada por WfMC (Workflow Management Coalition)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="rgb(0, 0, 0)" face="sans-serif">Es un&#160;</font><font color="rgb(11, 0, 128)" face="sans-serif" size="13px"><a title="Consorcio" href="http://es.wikipedia.org/wiki/Consorcio" style="background-repeat: repeat; line-height: 19.1875px; background-image: none; font-family: sans-serif; font-weight: normal; text-decoration: none; word-spacing: 0px; letter-spacing: normal; color: rgb(11, 0, 128); background-position: initial initial; text-transform: none; font-variant: normal; text-indent: 0px; background-color: rgb(255, 255, 255); white-space: normal; font-size: 13px; text-align: start; font-style: normal">consorcio</a></font><font color="rgb(0, 0, 0)" face="sans-serif">&#160;industrial formado por usuarios vendedores, analistas etc para definir est&#225;ndares para la interoperabilidad de sistemas de gesti&#243;n de&#160;</font><font color="rgb(11, 0, 128)" face="sans-serif" size="13px"><a title="Flujos de trabajo" href="http://es.wikipedia.org/wiki/Flujos_de_trabajo" class="mw-redirect" style="line-height: 19.1875px; background-repeat: repeat; background-image: none; font-family: sans-serif; text-decoration: underline; font-weight: normal; word-spacing: 0px; letter-spacing: normal; color: rgb(11, 0, 128); background-position: initial initial; text-transform: none; font-variant: normal; text-indent: 0px; background-color: rgb(255, 255, 255); white-space: normal; font-size: 13px; text-align: start; font-style: normal"><u>flujos de trabajo</u></a></font>
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369830020833" ID="ID_1842906108" MODIFIED="1481477497840" TEXT="Proporciona un fichero XML de intercambio">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369830354785" ID="ID_1083795215" MODIFIED="1481477497824" TEXT="Preferido para implementar relaciones humanas">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369830395856" ID="ID_1805573427" MODIFIED="1481477497824" TEXT="Almacena y permite el intercambio de diagramas de procesos">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372225393090" ID="ID_158011281" MODIFIED="1481477497824" TEXT="Lenguaje de orquestaci&#xf3;n y ejecuci&#xf3;n">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1369853940944" ID="ID_1845239608" MODIFIED="1481477497824" TEXT="Aplicaciones clientes (interfaz 2)">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369853953759" ID="ID_771073556" MODIFIED="1481477497824" TEXT="Aplicaciones invocadas (interfaz 3)">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369854047164" ID="ID_983278381" MODIFIED="1481477497824" TEXT="Funciones de interoperabilidad (interfaz 4)">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369854083926" ID="ID_1335348987" MODIFIED="1481477497824" TEXT="Herramientas de administraci&#xf3;n y monitorizaci&#xf3;n (interfaz 5)">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</map>
