<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1427902986301" ID="ID_307193241" MODIFIED="1517570709705" TEXT="T86 AN&#xc1;LISIS ESTRUCTURADO">
<node CREATED="1427903719894" ID="ID_54979664" MODIFIED="1427904076614" POSITION="right" TEXT="Datos y procesos son entidades distintas">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1427903458173" ID="ID_960833041" MODIFIED="1427904094294" TEXT="Representar SI a construir (modelizar)">
<icon BUILTIN="xmag"/>
<node CREATED="1427903770880" ID="ID_1297748590" MODIFIED="1427907609100" TEXT="Se suelen realizar varios modelos, para poder balancear (comprobar) al final"/>
<node CREATED="1427903488719" ID="ID_371830586" MODIFIED="1517570748537" TEXT="Mv3: en ASI, tras obtener el DRS (ver T91)"/>
</node>
</node>
<node CREATED="1427903536647" ID="ID_126606518" MODIFIED="1427906583811" POSITION="right" TEXT="MCP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427903547831" ID="ID_1266652533" MODIFIED="1427905050745" TEXT="Diagrama de Flujo de Datos (DFD)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427903855812" ID="ID_1695142259" MODIFIED="1427904120800" TEXT="Representa procesos, flujo de datos y transformaciones">
<icon BUILTIN="info"/>
<node CREATED="1427903927403" ID="ID_144805065" MODIFIED="1427903957698" TEXT="Gr&#xe1;fico, l&#xf3;gico (no f&#xed;sico), breve y comprensible"/>
<node CREATED="1427904490922" ID="ID_1618831403" MODIFIED="1427904541572" TEXT="DF Control: para sistemas RT con eventos (l&#xed;nea discontinua)"/>
</node>
<node CREATED="1427905409730" ID="ID_1292250678" MODIFIED="1427905480631" TEXT="SADT (Structured Analysis and Design Technique)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427905428851" ID="ID_697875414" MODIFIED="1427905466617" TEXT="Alternativa a DFD, funci&#xf3;n con input/output, control y mecanismos"/>
</node>
<node CREATED="1427903975092" ID="ID_1509844642" MODIFIED="1427904563057" TEXT="Elementos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427903977948" ID="ID_1017935364" MODIFIED="1427904129426" TEXT="Entidad externa">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427903981397" ID="ID_1396872344" MODIFIED="1427903997745" TEXT="Aportan o reciben info (elipse)">
<node CREATED="1427903997746" ID="ID_942698346" MODIFIED="1427905571654" TEXT="No se pueden comunicar entre s&#xed; directamente"/>
</node>
</node>
<node CREATED="1427904566840" ID="ID_213064246" MODIFIED="1427904619363" TEXT="Proceso">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427904570289" ID="ID_1130957759" MODIFIED="1427907000126" TEXT="Activ que transforma datos (caja o burbuja, * si primitivo)">
<node CREATED="1427904589262" ID="ID_656366317" MODIFIED="1427907007110" TEXT="Siempre entre entidad ext y almac&#xe9;n"/>
</node>
</node>
<node CREATED="1427904619705" ID="ID_1804480910" MODIFIED="1427904735824" TEXT="Almac&#xe9;n">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427904629445" ID="ID_1135635524" MODIFIED="1427904706690" TEXT="Dep&#xf3;sito datos (rect&#xe1;ngulo)">
<node CREATED="1427904640786" ID="ID_1217490092" MODIFIED="1427905370540" TEXT="No modif datos (R&amp;W), accedido por 2&#xf3;+ procesos"/>
<node CREATED="1427904762064" ID="ID_1654300604" MODIFIED="1427904801986" TEXT="Necesario para info as&#xed;nc, no se comunica con otro (necesita proc)"/>
</node>
</node>
<node CREATED="1427904736038" ID="ID_1637129282" MODIFIED="1427905310414" TEXT="Flujo datos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427904743205" ID="ID_1931045114" MODIFIED="1427904831397" TEXT="Comunicaci&#xf3;n, conecta elementos (flecha)">
<node CREATED="1427904837364" ID="ID_1236307026" MODIFIED="1427904848136" TEXT="No activa procesos ni modif datos"/>
</node>
</node>
</node>
<node CREATED="1427903898047" ID="ID_655314696" MODIFIED="1427904558075" TEXT="Descomposici&#xf3;n sucesiva de procesos (top-down)">
<node CREATED="1433342628190" ID="ID_1061419336" MODIFIED="1433342644413" TEXT="Se debe mantener siempre la continuidad del flujo de info">
<icon BUILTIN="yes"/>
</node>
<node CREATED="1427905148797" ID="ID_702411860" MODIFIED="1433318622256">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Nivel 0 (Diagr <u>Contexto </u>): entidades ext y proceso ppal
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1427905173117" ID="ID_164274381" MODIFIED="1433318637094">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Nivel 1 (Diagr <u>Sistema</u>): flujos y almacenes ppales (<u>subsistemas</u>)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1427905188844" ID="ID_1074110912" MODIFIED="1433318633352">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Nivel 2: descomposici&#243;n de subsistemas en DFDs (<u>funciones</u>)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1427905241294" ID="ID_1823586973" MODIFIED="1433318642213">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Nivel 3: <u>subfunciones</u>&#160;con eventos
    </p>
  </body>
</html></richcontent>
<node CREATED="1427905255893" ID="ID_928878260" MODIFIED="1433318646426">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Nivel 4: <u>procesos</u>&#160;primitivos (espec)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1427903709904" ID="ID_72023097" MODIFIED="1427903844969" TEXT="Diccionario de Datos (DD)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427904196392" ID="ID_1766703624" MODIFIED="1427906093872" TEXT="Describe y referencia elementos del DFD">
<node CREATED="1427905987928" ID="ID_1539932982" MODIFIED="1427906119671">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      =, +, [ | ]&#160;(selecci&#243;n), { } (iteraciones, lista), ( ) (opc), **(comentario), @ (campo almac&#233;n)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1427903845288" ID="ID_1644174375" MODIFIED="1427903850329" TEXT="Especificaci&#xf3;n de procesos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427906130862" ID="ID_1221313097" MODIFIED="1427908859973" TEXT="Describe l&#xf3;gica de procesos primitivos (pseudoc&#xf3;digo)">
<node CREATED="1427906150987" ID="ID_650720938" MODIFIED="1427906172356" TEXT="M&#xe9;todo de acceso, tratamiento, freq de ejec, algoritmos..."/>
</node>
</node>
</node>
<node CREATED="1427906297200" ID="ID_1710394579" MODIFIED="1427906583155" POSITION="right" TEXT="MCD">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427906301998" ID="ID_860991568" MODIFIED="1427906311176" TEXT="Diagrama de Estructura de Datos (DED)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427906313343" ID="ID_532658152" MODIFIED="1433318713937">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Necesidades de info e interrels entre entidades de datos (<u>modelo l&#243;gico </u>)
    </p>
  </body>
</html></richcontent>
<node CREATED="1427906354668" ID="ID_1469002961" MODIFIED="1427907290517" TEXT="Estructura datos, cntrl errores, mantenim">
<icon BUILTIN="xmag"/>
</node>
</node>
</node>
<node CREATED="1427906405871" ID="ID_1670142346" MODIFIED="1427906467653" TEXT="Diagrama de Entidad/Relaci&#xf3;n">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427906411187" ID="ID_1564071953" MODIFIED="1427906475829" TEXT="Alternativa a DED, para muy alto nivel (Plan de Sistemas)">
<node CREATED="1427906418631" ID="ID_728773087" MODIFIED="1427906808778" TEXT="Relaciones N-arias (en DED son binarias), +real y riguroso"/>
</node>
</node>
</node>
<node CREATED="1427906530195" ID="ID_1148425466" MODIFIED="1427906586651" POSITION="right" TEXT="MCDin&#xe1;mico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427907178935" ID="ID_104735081" MODIFIED="1427907187903" TEXT="Historia de Vida de Entidades (HVE)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427907188262" ID="ID_157768925" MODIFIED="1427907452705" TEXT="Representa evoluci&#xf3;n de entidades de datos en el tiempo (eventos: crear, modif, borrar)">
<node CREATED="1427907212989" ID="ID_1231989472" MODIFIED="1427907461593" TEXT="Verif coherencia DFD-DED"/>
</node>
</node>
</node>
<node CREATED="1427907493171" ID="ID_517575077" MODIFIED="1427908429270" POSITION="right" TEXT="Balanceo entre modelos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427908392326" ID="ID_360480020" MODIFIED="1427908433376" TEXT="Evitar inconsistencias o definiciones faltantes">
<icon BUILTIN="xmag"/>
</node>
<node CREATED="1427908482173" ID="ID_1614331204" MODIFIED="1427908492349" TEXT="DFD-DD-Espec procesos">
<node CREATED="1427908437909" ID="ID_1507387669" MODIFIED="1427908478619" TEXT="Todos los flujos y almacenes en DFD deben estar definidos en DD, y viceversa"/>
<node CREATED="1427908498043" ID="ID_774201206" MODIFIED="1427908911843" TEXT="Cada proceso se asocia con DFD inferior o con espec (no con ambos)"/>
<node CREATED="1427908522589" ID="ID_128237355" MODIFIED="1427908547959" TEXT="E/S coinciden en DFD inferior y en su espec"/>
</node>
<node CREATED="1427908624089" ID="ID_126199852" MODIFIED="1427908643567" TEXT="DFD-DER-Espec procesos">
<node CREATED="1427908643568" ID="ID_1297477971" MODIFIED="1427908661633" TEXT="Cada almac&#xe9;n DFD se corresponde con Entidad del DER, y nombres coinciden"/>
<node CREATED="1427908665573" ID="ID_1405825661" MODIFIED="1427908695982" TEXT="Entradas en el DD se aplican al DFD y al DER"/>
</node>
<node CREATED="1427908696771" ID="ID_1091211387" MODIFIED="1427908717016" TEXT="HVE-DFD-DED">
<node CREATED="1427908705526" ID="ID_117825740" MODIFIED="1427908756690" TEXT="Cada evento del HVE tratado por proceso en DFD"/>
<node CREATED="1427908758736" ID="ID_329404968" MODIFIED="1427908786662" TEXT="DED refleja repercusiones que evento en una entidad tiene sobre otras"/>
</node>
</node>
</node>
</map>
