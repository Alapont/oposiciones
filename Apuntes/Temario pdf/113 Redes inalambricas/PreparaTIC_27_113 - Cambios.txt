CONTENIDOS:
	- Documento "PreparaTIC_27_113 - Resumen":
		- Se actualiza la tabla del apartado "2.2. Variantes para el interfaz aire".
		- Se añaden las dos últimas versiones de Bluetooth en el apartado "6. Bluetooth".
		- El antiguo apartado "7. NFC" se renombra como "7. Otras tecnologías WPAN y WMAN" donde queda incluida la tecnología NFC y ZigBee.
	- Documento "PreparaTIC_27_113 - Resumen express":
		- Se ha creado este documento.