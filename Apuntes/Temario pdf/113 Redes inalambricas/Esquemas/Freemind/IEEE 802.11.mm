<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1377086922670" ID="ID_829506974" MODIFIED="1378554983074" TEXT="IEEE 802.11">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368720406126" ID="ID_866277775" MODIFIED="1375465020472" POSITION="right" TEXT="802.11">
<node CREATED="1368720410194" FOLDED="true" ID="ID_1389426690" MODIFIED="1391672876609" TEXT="Interfaz radioel&#xe9;ctrico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375641586644" ID="ID_289601536" MODIFIED="1382615305799" TEXT="PHY: se divide en dos subniveles">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375641597020" ID="ID_1271572722" MODIFIED="1377189237184" TEXT="PLCP (Physical Layer Convergence Protocol)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375642030795" ID="ID_1669214204" MODIFIED="1377189208728" TEXT="Interfaz con el nivel MAC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375641743541" ID="ID_788453527" MODIFIED="1391194421848" TEXT="Responsable de la capa Carrier Sense de CSMA/CA">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reserva de canal f&#237;sico
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375465133038" ID="ID_1263438384" MODIFIED="1381330791437" TEXT="Se usa CCA (Clear Channel Assesment)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Indica para la trama a transmitir que el medio est&#225; ocupado o no
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378556254512" ID="ID_443624169" MODIFIED="1378556265761" TEXT="CS (Carrier Sense)"/>
<node CREATED="1378556266174" ID="ID_1854424696" MODIFIED="1378556276023" TEXT="ED (Energy Detection)"/>
</node>
<node CREATED="1368720972846" ID="ID_1590590394" MODIFIED="1378557312911" TEXT="Problema de nodo oculto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377171497911" ID="ID_525676596" MODIFIED="1377171507582" TEXT="Problema del nodo expuesto">
<node CREATED="1377171511512" ID="ID_22488836" MODIFIED="1377171537768" TEXT="Una estaci&#xf3;n no transmite porque parece que otra interferir&#xed;a"/>
</node>
</node>
<node CREATED="1368720836356" ID="ID_337810708" MODIFIED="1391193356008" TEXT="Reserva de canal virtual (RTS/CTS)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368721060618" ID="ID_824543374" MODIFIED="1384765993830" TEXT="RTS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
<node CREATED="1368721067214" ID="ID_1830473400" MODIFIED="1378557349143" TEXT="CTS (NAV - Network Allocation Vector)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Incluye NAV que significa una reserva del medio para el resto de las estaciones, ya que la ha pedido el RTS va a transmitir.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="back"/>
</node>
</node>
<node CREATED="1369052668665" ID="ID_1120088899" MODIFIED="1378557312912" TEXT="Se resuelve el problema del nodo oculto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375641655655" ID="ID_753137531" MODIFIED="1377189206044" TEXT="PMD (Physical Medium Dependent)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368720455950" ID="ID_1290356576" MODIFIED="1384867336706" TEXT="Modulaci&#xf3;n espectro ensanchado (spread spectrum)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368720533525" ID="ID_1650316491" MODIFIED="1368720542533" TEXT="Baja densidad de potencia y redundancia"/>
<node CREATED="1368720545205" ID="ID_1287094465" MODIFIED="1384867860090" TEXT="T&#xe9;cnicas">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cada t&#233;cnica se corresponde a un est&#225;ndar del nivel f&#237;sico (IR es otro est&#225;ndar)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368720550833" ID="ID_1314363789" MODIFIED="1378555441185" TEXT="FHSS Modificaci&#xf3;n de portadora por salto en frecuencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368720586923" ID="ID_401538632" MODIFIED="1378555450841" TEXT="DSSS Modulaci&#xf3;n de la portadora por secuencia directa (chip) ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378570970559" ID="ID_401326161" MODIFIED="1378571004506" TEXT="OFDM (Orthogonal Frecuency Division Muntiplexing)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378571008304" ID="ID_158711683" MODIFIED="1391193330698" TEXT="HR/DSSS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378571150268" ID="ID_794996279" MODIFIED="1384867894643" TEXT="High-rate DSSS: este es el que usa 802.11b">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1375641875722" ID="ID_1483357413" MODIFIED="1378571175800" TEXT="IR (Infrarojo, pero no se usa)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368720485648" ID="ID_1756642716" MODIFIED="1378636274274" TEXT="Radiocanal de 22MHz">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1368720767403" FOLDED="true" ID="ID_1468987471" MODIFIED="1391672872571" TEXT="Acceso al medio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368721228016" ID="ID_1329566688" MODIFIED="1384868210803" TEXT="Modo PCF (Point Coordination Function)   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378557129802" ID="ID_650590000" MODIFIED="1384766095821" TEXT="&quot;Libre de contienda&quot;">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ya que no se compite por el medio: el coordinador va asign&#225;ndolo. Se trata de un modo de selecci&#243;n. Las estaciones deben transmitir algo, aunque sea una trama nula.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368721332064" ID="ID_607998402" MODIFIED="1384766098831" TEXT="Un coordinador env&#xed;a una trama baliza (beacon frame)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378557096776" ID="ID_311160546" MODIFIED="1384766112295" TEXT="Cuando se recibe la trama se puede transmitir durante un tiempo PIFS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378556772618" ID="ID_1104744694" MODIFIED="1384868355303" TEXT="Las estaciones que lo usen tienen una mayor prioridad que DCF (PIFS&lt;DIFS)  ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1373618864298" ID="ID_1160569018" MODIFIED="1384868393954" TEXT="Modo DCF (Distributed Coordination Function)   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378557395158" ID="ID_1742081726" MODIFIED="1378557409150" TEXT="Espera a transmitir por un tiempo DIFS"/>
<node CREATED="1378557409612" ID="ID_929916785" MODIFIED="1378557436171" TEXT="Si durante ese tiempo el canal est&#xe1; ocupado, no transmite"/>
<node CREATED="1378557436681" ID="ID_1892012164" MODIFIED="1378557580169" TEXT="Al cabo de un tiempo aleatorio, lo vuelve a intentar"/>
</node>
<node CREATED="1369033423783" ID="ID_81632452" MODIFIED="1384868402371" TEXT="HCF (Hybrid Coordination Function introducido en 802.11e)   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369033428596" ID="ID_742750272" MODIFIED="1369033548287" TEXT="EDCA (Enhanced Distributed Channel Access)">
<node CREATED="1391672396651" ID="ID_1757315418" MODIFIED="1391672411614" TEXT="Define el tr&#xe1;fico de mayor prioridad">
<node CREATED="1391672668705" ID="ID_988643496" MODIFIED="1391672678985" TEXT="Se definen categor&#xed;as de acceso"/>
</node>
<node CREATED="1391672612398" ID="ID_1852651266" MODIFIED="1391672630887" TEXT="Las estaciones con tr&#xe1;fico de mayor prioridad esperan menos para transmitir"/>
</node>
<node CREATED="1369033436230" ID="ID_1918100758" MODIFIED="1369033580298" TEXT="HCCA (Hybrid Controlled Channel Access)">
<node CREATED="1391672414101" ID="ID_959835372" MODIFIED="1391672444671" TEXT="Existe un controlador como PCF">
<node CREATED="1391672838840" ID="ID_998435108" MODIFIED="1391672859000" TEXT="la configuraci&#xf3;n es para toda la red y no por AP"/>
</node>
<node CREATED="1391672444943" ID="ID_864614459" MODIFIED="1391672831439" TEXT="Se definen las clases de tr&#xe1;fico y los flujos de tr&#xe1;fico con par&#xe1;metros de qOs"/>
</node>
</node>
</node>
<node CREATED="1368721376288" FOLDED="true" ID="ID_215441613" MODIFIED="1391672875384" TEXT="Arquitectura">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368721385118" ID="ID_584756653" MODIFIED="1368721396995" TEXT="Redes Ad-Hoc"/>
<node CREATED="1368721397331" ID="ID_1461714497" MODIFIED="1368721401967" TEXT="Infraestructura">
<node CREATED="1368721477584" ID="ID_1465700969" MODIFIED="1368721488989" TEXT="2,4 GHz">
<node CREATED="1368721488990" ID="ID_1379989303" MODIFIED="1368721522390" TEXT="S&#xf3;lo 3 AP en la misma zona"/>
<node CREATED="1368721502359" ID="ID_1520732642" MODIFIED="1368721527569" TEXT="Canales separados 22 MHz"/>
</node>
<node CREATED="1368721531510" ID="ID_1525323771" MODIFIED="1368721541940" TEXT="5 GHz">
<node CREATED="1368721541941" ID="ID_1649911981" MODIFIED="1368721549574" TEXT="8 AP"/>
</node>
</node>
</node>
<node CREATED="1368721674140" ID="ID_430062071" MODIFIED="1391193969467" TEXT="Grupos de trabajo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368721681521" ID="ID_630786878" MODIFIED="1383413519092" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="802.11.png" />
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <table style="border-top-width: 0; border-bottom-style: solid; border-right-width: 0; border-left-style: solid; border-top-style: solid; border-left-width: 0; width: 80%; border-bottom-width: 0; border-right-style: solid" border="0">
      <tr>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-top-style: solid; border-left-width: 1; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            802.11f
          </p>
        </td>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-top-style: solid; border-left-width: 1; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            Definir funciones que permitan a un usuario cambiar de un canal a otro (roaming), como GSM
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-top-style: solid; border-left-width: 1; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            802.11e
          </p>
        </td>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-top-style: solid; border-left-width: 1; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            Introduce par&#225;metros de QoS en el acceso al medio (HDC)
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-top-style: solid; border-left-width: 1; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            802.11h
          </p>
        </td>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-top-style: solid; border-left-width: 1; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            Suplemento al 802.11a para cubrir requisito de procedimientos de control de potencia
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-top-style: solid; border-left-width: 1; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            802.11i
          </p>
        </td>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-top-style: solid; border-left-width: 1; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            Seguridad m&#225;s elevada con AES (WPA2)
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-top-style: solid; border-left-width: 1; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            802.11n
          </p>
        </td>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-top-style: solid; border-left-width: 1; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            5 GHz, hasta 100 Mbps
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-top-style: solid; border-left-width: 1; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            802.11ac
          </p>
        </td>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-top-style: solid; border-left-width: 1; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            Mejora de 802.11n amplia el ancho de banda hasta 160 MHz y la velocidad hasta 1 Gbps (en espera de ser ratificada)
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-top-style: solid; border-left-width: 1; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            802.11d
          </p>
        </td>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-top-style: solid; border-left-width: 1; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            Especificaciones de nivel f&#237;sico
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-left-width: 1; border-top-style: solid; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            802.11ad (WiGig)
          </p>
        </td>
        <td valign="top" style="border-top-width: 1; border-bottom-style: solid; border-right-width: 1; border-left-style: solid; border-left-width: 1; border-top-style: solid; width: 50%; border-bottom-width: 1; border-right-style: solid">
          <p style="margin-bottom: 1; margin-top: 1; margin-left: 1; margin-right: 1">
            Opera tambi&#233;n en la banda de 60 GHz. 7Gbps, pero siempre en l&#237;nea de visi&#243;n.
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1368723033392" ID="ID_1978219558" LINK="../../Vol%2002/Esquemas/Seguridad%20en%20la%20red.mm" MODIFIED="1391197105454" TEXT="Seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368723075900" ID="ID_910523502" MODIFIED="1377170978875" TEXT="WEP">
<node CREATED="1368723084359" ID="ID_864229168" MODIFIED="1373619173097" TEXT="RC4 con claves de 40, 128 y 256 bits (802.11)"/>
</node>
<node CREATED="1377170980147" ID="ID_580337448" MODIFIED="1377170994764" TEXT="WPA con TKIP"/>
<node CREATED="1368723115581" ID="ID_1319194286" MODIFIED="1373619194268" TEXT="WPA-2 con AES (802.11i)"/>
<node CREATED="1368723138346" ID="ID_1002084155" MODIFIED="1368723154360" TEXT="Validaciones MAC"/>
<node CREATED="1373618734600" ID="ID_392537581" MODIFIED="1373618742158" TEXT="Disminuci&#xf3;n de la potencia"/>
<node CREATED="1391623307518" ID="ID_536167568" MODIFIED="1391623324418" TEXT="No publicar el SSID del AP"/>
</node>
<node CREATED="1391622963788" ID="ID_1476855326" MODIFIED="1391622971452" TEXT="Normativa">
<node CREATED="1391622971454" ID="ID_1135104787" MODIFIED="1391622977264" TEXT="2,4 GHz">
<node CREATED="1391622977266" ID="ID_1625506657" MODIFIED="1391622983391" TEXT="UN-85">
<node CREATED="1391622983393" ID="ID_199423350" MODIFIED="1391622992662" TEXT="PIRE max de 100mW"/>
</node>
</node>
<node CREATED="1391622995448" ID="ID_1221902392" MODIFIED="1391623003310" TEXT="5 GHz">
<node CREATED="1391623003312" ID="ID_684353044" MODIFIED="1391623008385" TEXT="UN-128">
<node CREATED="1391623008387" ID="ID_229593951" MODIFIED="1391623025722" TEXT="PIRE max. de 1 W"/>
</node>
</node>
<node CREATED="1391623220669" ID="ID_1832732169" MODIFIED="1391623226525" TEXT="Banda ISM">
<node CREATED="1391623226527" ID="ID_1636321469" MODIFIED="1391623245578" TEXT="Deber&#xe1;n aceptar interferencias de otros servicios"/>
</node>
</node>
</node>
</node>
</map>
