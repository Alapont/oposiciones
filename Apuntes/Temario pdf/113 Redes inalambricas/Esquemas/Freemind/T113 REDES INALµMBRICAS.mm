<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1437552538714" ID="ID_911345084" MODIFIED="1437552958788">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T107 REDES
    </p>
    <p style="text-align: center">
      INAL&#193;MBRICAS
    </p>
    <p style="text-align: center">
      Y DE ACCESO
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1437552946954" ID="ID_1426265070" MODIFIED="1437563028342" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Wireless
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437552664653" ID="ID_628328831" MODIFIED="1437563014928">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      802.11
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437553402979" ID="ID_524571922" MODIFIED="1437553449451">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Prot de coms v&#237;a radio para <u>WLAN</u>, manteniendo caracts de enlace Ethernet cableado
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1437553796457" ID="ID_340385706" MODIFIED="1437563212421">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Flexib, bajo mantenim, robusto, movilidad y escalab
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1437553818533" ID="ID_806591283" MODIFIED="1437555407240" TEXT="Alcance limitado, riesgos, -vel q cable y +errores">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1437553453042" ID="ID_1155024094" MODIFIED="1437553519306">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      802.11 es<u>+amplio que WiFi</u>&#8594; no todo prod 802.11 es WiFi
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1437553559141" ID="ID_1594273957" MODIFIED="1437555804818">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Capas 1 y 2 OSI y cumple <u>802.3</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1437553727676" ID="ID_1545543284" MODIFIED="1437556500993">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Banda ISM: </i>(Ind, Scien, Med) 2,4-5 GHz, <u>uso libre</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
</node>
<node CREATED="1437552678942" ID="ID_247128512" MODIFIED="1437553964133" TEXT="Acceso al medio">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437553965850" ID="ID_1891330531" MODIFIED="1437554314737">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>CSMA/CA:</i>&#160;solo tx cuando nadie y espera ACK. &#218;til&#160;si hay pocos datos
    </p>
  </body>
</html></richcontent>
<node CREATED="1437554246166" ID="ID_1031179331" MODIFIED="1437554331180">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>RTS/CTS: </i>punto acceso decide qui&#233;n&#160;tx (-BW por sobrecarga)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1437554236117" ID="ID_460782058" MODIFIED="1437557250218">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modulaci&#243;n
    </p>
    <p>
      (acceso m&#250;ltiple)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437555489933" ID="ID_520326918" MODIFIED="1437555515339">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      La velocidad/BW <u>se comparte</u>&#160;entre usuarios, ya que comparten el medio (dom colisi&#243;n)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1437556867825" ID="ID_413284556" MODIFIED="1437564461785" TEXT="CDMA">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437554349875" ID="ID_1175110416" MODIFIED="1437563986011">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>FHSS:&#160;</i>(Freq Hoping Spead Spectr) info va modulando a <u>saltos</u>&#160;subportadoras
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1437554391680" ID="ID_196138793" MODIFIED="1437563271750">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Solo llega a 1Mbps&#8594; uso en&#160; <u>Bluetooth</u>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1437554409260" ID="ID_197371289" MODIFIED="1437564492372">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>DSSS: </i>(Direct Seq SS) mux, +robusto y poca pot. (de)modula pulsos con&#160; <u>c&#243;digo pseudoaleat</u>&#160;(CHIP)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1437554465992" ID="ID_781985195" MODIFIED="1437557258732">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      11Mbps&#8594; <u>3G</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1437556891045" ID="ID_1533625615" MODIFIED="1437556893310" TEXT="FDMA">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437554498745" ID="ID_192772775" MODIFIED="1437555542387">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>OFDM: </i>(Ortog Freq Div Mux)&#160; <u>divide BW</u>&#160;en subportadoras
    </p>
  </body>
</html></richcontent>
<node CREATED="1437554551168" ID="ID_1567423475" MODIFIED="1437563330308">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Supera 11Mbps&#8594; uso en <u>WiFi</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</node>
<node CREATED="1437554664306" ID="ID_1839289478" MODIFIED="1437554918251">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Duplexaci&#243;n&#160;(com bidir)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437554669594" ID="ID_1765267602" MODIFIED="1437554913037">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>TDD:</i>&#160;= freq, se turnan en el t (CSMA/CD)
    </p>
  </body>
</html></richcontent>
<node CREATED="1437554737084" ID="ID_1122288854" MODIFIED="1437554762438">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>FDD:</i>&#160;una freq para cada sentido (asc y descend)
    </p>
  </body>
</html></richcontent>
<node CREATED="1437554882038" ID="ID_1193672561" MODIFIED="1437554894504">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>MIMO:&#160;</i>antenas, dup espacial
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1437554587778" ID="ID_197662165" MODIFIED="1437554591546" TEXT="Est&#xe1;ndares">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437554591546" ID="ID_114599778" MODIFIED="1437563366514">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>802.11b:</i>&#160;DSSS, 11Mbps, 2,4-2,48GHz, TDD
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1437554922098" ID="ID_1284659775" MODIFIED="1437555319350">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>.11a: </i>OFDM, 24-54Mbps, <u>5GHz</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1437554980413" ID="ID_1805979331" MODIFIED="1437556550627">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>.11g:</i>&#160;OFDM-DSSS, 36-54Mbps, 2,4GHz (migra .11a)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1437555041901" ID="ID_1166776030" MODIFIED="1437564754476">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i><b>802.11n:&#160;</b></i>OFDM-FEC-64QAM, <u>108-600Mbps</u>, 2,4-5GHz, 52 ports, MIMO, 20 y 40MHz/canal, 90m
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="messagebox_warning"/>
</node>
<node CREATED="1437555105843" ID="ID_1336850339" MODIFIED="1437556569573">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>802.11ac (WiFi Gigabit):</i>&#160;256QAM, 1Gbps, <u>5GHz</u>, 20 40 y 80 MHz/canal
    </p>
  </body>
</html></richcontent>
<node CREATED="1437565677287" ID="ID_624982200" MODIFIED="1437565728723">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>.11e:</i>&#160;QoS / <i>.11i: </i>seg /<i>.11f:</i>&#160;roaming
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1437552694502" ID="ID_1833201703" MODIFIED="1437552697752" TEXT="Componentes">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437555863670" ID="ID_200576734" MODIFIED="1437564720255">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Terminal Usuario: </i><u>NIC</u>+transceptor
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1437555876574" ID="ID_1320437307" MODIFIED="1437555898751">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Punto Acceso:&#160;</i>tx info de red cableada a NICs
    </p>
  </body>
</html></richcontent>
<node CREATED="1437555899880" ID="ID_1720599885" MODIFIED="1437555946760">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Controlador AP:</i>&#160;si hay varios AP por cobert o tr&#225;f
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1437552698525" ID="ID_719897169" MODIFIED="1437552703095" TEXT="Modos de operaci&#xf3;n">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437555951420" ID="ID_1100603562" MODIFIED="1437556027969">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Red Ad-hoc:</i>&#160;<u>sin AP</u>&#160;(s&#170; distrib-peer to peer)
    </p>
  </body>
</html></richcontent>
<node CREATED="1437556064855" ID="ID_756332323" MODIFIED="1437556095258">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Shared Key: </i>clave secreta compartida entre clientes
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1437555979619" ID="ID_778090653" MODIFIED="1437556272237">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Red Infraestr: </i><u>AP</u>&#160;coord coms de su &#225;rea (+BW y cobert)
    </p>
  </body>
</html></richcontent>
<node CREATED="1437556117838" ID="ID_1478288485" MODIFIED="1437563441685">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>WEP:&#160;</i>(Wired Eq Priv) autentic+encrip, <u>clave viaja</u>&#160;en tramas&#8594; vulnerable!
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1437556287003" ID="ID_1392355616" MODIFIED="1437556452238">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>WAP:&#160;</i>(WiFi Protect Acc, 802.11i) evita deficiencias WEP, claves <u>din&#225;micas</u>&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1437552732521" ID="ID_708125101" MODIFIED="1437563015551">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      WiMAX
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437557736101" ID="ID_406964855" MODIFIED="1437557999158">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Worldwide Inteop for Microwave Acc) interfaz a&#233;rea para s&#170; fijos de acceso <u>WMAN</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1437557876234" ID="ID_161114890" MODIFIED="1437565568523">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>App: </i>lugares donde no se pueda asumir cableado por coste o riesgo
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1437557920161" ID="ID_640012290" MODIFIED="1437565127214">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Vers&#225;til</u>&#160;(todo tipo de servs), +<u>cobertura&#160;y seg</u>&#160;que WiFi
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1437558051407" ID="ID_1916755730" MODIFIED="1437558055103" TEXT="Est&#xe1;ndares">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437558055104" ID="ID_446123357" MODIFIED="1437558158168">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>802.16</b>&#160;engloba .a, .c y .d
    </p>
  </body>
</html></richcontent>
<node CREATED="1437558073444" ID="ID_1818804986" MODIFIED="1437558457145">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>.16e:</i>&#160;anexo, disps de mov reducida (3G). WiBro en Corea
    </p>
  </body>
</html></richcontent>
<node CREATED="1437558092034" ID="ID_547659034" MODIFIED="1437558103224">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>.16m:</i>&#160;reqs 4G
    </p>
  </body>
</html></richcontent>
<node CREATED="1437558120275" ID="ID_1968484084" MODIFIED="1437558132881">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>.16.1a: </i>+fiabilidad
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1437552736282" ID="ID_812072380" MODIFIED="1437558136992" TEXT="Caracter&#xed;sticas">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437558176311" ID="ID_174240780" MODIFIED="1437559194079">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      OFDM,&#160;<u>75Mbps</u>,&#160;2-11GHz y 10-66GHz, 20MHz/canal, 50km (NLOS) o 8-10km, QoS
    </p>
  </body>
</html></richcontent>
<node CREATED="1437558499950" ID="ID_1792805304" MODIFIED="1437558639062">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Compatible</u>&#160;con 802.11a en 5GHz
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1437558668299" ID="ID_870857387" MODIFIED="1437559206199">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Seguridad: </i>cifrado (triple-DES y RSA) y autentic ( <u>certifs</u>&#160;X509)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1437552742095" ID="ID_207785658" MODIFIED="1437563015983" TEXT="Bluetooth">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437558830340" ID="ID_1379322170" MODIFIED="1437559601485">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Espec para <u>WPAN</u>&#160;(tfn m&#243;vil y accesorios) de bajo coste y consumo
    </p>
  </body>
</html></richcontent>
<node CREATED="1437558906191" ID="ID_1607317069" MODIFIED="1437558922500">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>802.15.1</b>&#160;(no es de nivel 2)
    </p>
  </body>
</html></richcontent>
<node CREATED="1437559240751" ID="ID_984635776" MODIFIED="1437563866676" TEXT="Interfiere con WiFi! (incompat)">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1437552736282" ID="ID_1264820800" MODIFIED="1437565454853">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Caracter&#237;sts
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437558940519" FOLDED="true" ID="ID_882749864" MODIFIED="1437566472240">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>v1.1: </i>GFSK, <u>1Mbps</u>&#160;(723kbps),&#160;2,4-2,48GHz, 1MHz/canal, 10m (opc 100m)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1437559471527" ID="ID_1281093512" MODIFIED="1437565434391">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      v2+EDR (3Mbps, -consum), v3 (24Mbps), v4 (100Mbps)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1437559273538" ID="ID_902845063" MODIFIED="1437559331900">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Topolog&#237;as:&#160;</i>estrella&#8594; <u>piconet</u>&#160;(maestro+7disps) y scatternet (grupo de piconets)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1437560201139" ID="ID_1640117226" MODIFIED="1437563016407">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      LMDS
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437560203044" ID="ID_1304834764" MODIFIED="1437560524325">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Local Multipoint Distrib Serv) s&#170; de acceso fijo de &#250;ltima milla (usuario-centralita local)&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1437560331072" ID="ID_843440453" MODIFIED="1437565554870">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Servs paquetizados (IP) y VoD
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1437560465973" ID="ID_356935412" MODIFIED="1437560511252">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      QPSK,&#160;8Mbps, <u>28-31GHz</u>, 100m
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1437552958776" ID="ID_1094286362" MODIFIED="1437563021495" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Acceso
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437552820082" ID="ID_1044515007" MODIFIED="1437563016847" TEXT="xDSL">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437560939384" ID="ID_1135888239" MODIFIED="1437561063446">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Dig Suscriber Loop) t&#233;cnicas q&#160;aprovechan el par trenzado de la <u>RTC</u>&#160;para tx datos (multim/interact)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1437561991451" ID="ID_523290215" MODIFIED="1437562038784">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>DSL:</i>&#160;RDSI (2 canales a 64kbps)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1437561078176" ID="ID_507875892" MODIFIED="1437564067907">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Oferta del Bucle de Abonado
    </p>
    <p>
      (servs de acceso)
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437561084145" ID="ID_524165072" MODIFIED="1437564186618">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Desagregado totalmente:</i>&#160;operadora totalmente <u>indep</u>&#160;(propia red y servs), elegida por usuario
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1437561153073" ID="ID_1972250585" MODIFIED="1437561270000">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Desagregado compartido: </i><u>voz</u>&#160;por <u>Telef&#243;nica</u>&#160;y datos por otra operadora
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1437561191338" ID="ID_986536550" MODIFIED="1437564107058">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Indirecto:</i>&#160;<u>operador revende</u>&#160;ADSL, pero Telef&#243;nica (operador con poder signif) presta el serv&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1437552823187" ID="ID_1053680586" MODIFIED="1437561291700" TEXT="ADSL">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437561298120" ID="ID_1733374377" MODIFIED="1437562679826">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>8/1,3Mbps</u>&#160;(asim&#233;trico), Splitter separa voz (lf) de datos (hf), DSLAM mux se&#241;ales DSL en centralita
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1437561472625" ID="ID_298237827" MODIFIED="1437566072821">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      ITU G992.1 / .2 (Lite: -BW, no req Splitter)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1437561492668" ID="ID_605046324" MODIFIED="1437565848742">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Modulaci&#243;n:&#160;</i>CAP (Carrierless Ampl&amp;Ph, QAM-down y QPSK-up) y DMT (Dig MultiTone, subportadoras)
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1437561559617" ID="ID_1878353096" MODIFIED="1437561616539">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Espectro: </i>Voz (&lt;4kHz)-User a Red (4-40kHz)-Red a User (40-500kHz)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1437562330957" ID="ID_1651094329" MODIFIED="1437563664267">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      A partir de <u>2,5km</u>&#160;no hay dif entre ADSL/2/2+
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1437561631495" ID="ID_1266479471" MODIFIED="1437566348922">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>ADSL2:</i>&#160;<u>12/2Mbps</u>, usa <u>varios pares</u>/l&#237;neas tfn, G992.3
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1437561678077" ID="ID_273736654" MODIFIED="1437561730287">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>ADSL2+:</i>&#160;<u>24/2Mbps</u>, aprovecha doble cantidad de espectro, G992.5
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1437552827209" ID="ID_1416671758" MODIFIED="1437561757437" TEXT="Otras">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437561758346" ID="ID_711570789" MODIFIED="1437566281294">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>HDSL:</i>&#160;(High Data Rate) proporciona enlaces RDSI E1 (2Mbps), 2 pares,<u>sim&#233;trico</u>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1437561931552" ID="ID_1757865023" MODIFIED="1437562100532">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>SHDSL:</i>&#160;(Symmetric High Speed) 1 solo par y +alcance
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1437562140589" ID="ID_928747652" MODIFIED="1437562242652">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>VDSL:</i>&#160;(Very High Rate) <u>51-55Mbps</u>&#160;a 300m, permite <u>FTTx</u>, modo asim (TV, VoD) o sim (videoconf)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1437552863428" ID="ID_179722981" MODIFIED="1437563017327" TEXT="PLC">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437562751840" ID="ID_893917900" MODIFIED="1437565009379">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Power Line Comms) aprovecha <u>red el&#233;ctrica</u>&#160;(baja-media tensi&#243;n) para tx voz y datos (HF) a LANs
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1437562825689" ID="ID_1530106702" MODIFIED="1437566261484" TEXT="Medio hostil para tx datos">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1437552865100" ID="ID_666333293" MODIFIED="1437562856016">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      OPERA (Open PLC EU Research Alliance)
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437562833912" ID="ID_529533904" MODIFIED="1437563734739">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (CE y otros) acelera el desarrollo de PLC para Internet, VoIP y TV
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1437562891144" ID="ID_115975698" MODIFIED="1437562955864">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      OFDM, <u>200Mbps</u>&#160;entre &lt;450 usuarios
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
