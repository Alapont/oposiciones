<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1454069582892" ID="ID_455797610" MODIFIED="1454069587620" TEXT="CONST 78">
<node CREATED="1454069596510" ID="ID_504041798" MODIFIED="1454346194326" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      10 t&#237;tulos, 169 arts y 9 disps trans y 4 adic
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1454346269692" ID="ID_448367684" MODIFIED="1480695516763">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Parte dogm&#225;tica (pre&#225;mb, prelim, I) y org&#225;nica (resto)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1454069609618" ID="ID_1562755918" MODIFIED="1454072302428">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Aprob por CG (31-oct), ratificada por refer&#233;ndum (6-dic) y sancionada por Rey (27-dic)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454346336938" ID="ID_1529554592" MODIFIED="1454346342489" POSITION="right" TEXT="Pre&#xe1;mbulo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454346347109" ID="ID_1161615724" MODIFIED="1454346530682">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Convivencia democr, Estado Dcho, prot Espa&#241;a (cult, trads, dchos), soc avanz (progr eco-cult), coop
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1454069673759" ID="ID_33026281" MODIFIED="1454351724423" POSITION="right" TEXT="Preliminar">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454069678879" ID="ID_308660563" MODIFIED="1454070749435">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estado social-demo, Monarq parl, unidad, <u>creaci&#243;n partidos</u>&#160;y sindics/asocs, FFAA
    </p>
  </body>
</html></richcontent>
<node CREATED="1454069767217" ID="ID_849843088" MODIFIED="1454351654506">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Garant&#237;as jur: </i>legal, jerarq y public normas, respons
    </p>
  </body>
</html></richcontent>
<node CREATED="1454321711652" ID="ID_634900030" MODIFIED="1454351672889">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Valores sup:</i>&#160;libert, just, ig y pol&#237;tica plural
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1454069854827" ID="ID_1667816291" MODIFIED="1454069863172" POSITION="right" TEXT="I. Dchos fund">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454069865076" ID="ID_1079039928" MODIFIED="1454069944700" TEXT="Cap 1: Esps/extranjeros">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1454069995335" ID="ID_114897373" MODIFIED="1454070012903" TEXT="Cap 2: Dchos y libs">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454070012908" FOLDED="true" ID="ID_694216710" MODIFIED="1480695349205">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sec 1: Dchos fund
    </p>
    <p>
      y libs (art 15-29)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1454070021500" ID="ID_233333923" MODIFIED="1454081526735">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Igualdad</u>&#160;(art 14, aparte) integr f&#237;s, lib ideo/relig, seg, det prev&lt;<u>72h</u>&#160;y abog, <u>habeas corpus</u>&#160;(det ileg)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1454070214992" ID="ID_156810369" MODIFIED="1454071097926">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Art 18:</i>&#160;honor, domicilio y coms secretas salvo res jud,&#160; <u>inform&#225;tica limitada</u>&#160;(18.4)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1454070281390" ID="ID_912837378" MODIFIED="1454416372328">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lib expresi&#243;n/c&#225;tedra, reuni&#243;n (manifs&#160; <u>previa com</u>), <u>asoc</u>&#160;(reg de <u>public</u>), particip, tutela (inoc)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1454070586072" ID="ID_788094433" MODIFIED="1454406021162">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No trabajos forz, ense&#241;anza, autonom&#237;a Univ, sindic,&#160; <u>huelga</u>,&#160;<u>petici&#243;n</u>&#160;indiv-colect por escrito a CG
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454070789352" ID="ID_1249809971" MODIFIED="1454070802497">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sec 2: Dchos y deberes
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454070874055" ID="ID_1758084131" MODIFIED="1454071669825">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      S&#170; <u>tribut</u>&#160;equit, mili, matrim, herencia/exprops, fundac,&#160; <u>trabajo sin discrim</u>&#160;sex, convenios, mercado
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454071065975" ID="ID_1888393785" MODIFIED="1454071082049">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cap 3: Pol&#237;tica socioeco
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454071076633" ID="ID_1534428021" MODIFIED="1454071492709">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Prot familiar y salud p&#250;b, <u>renta equit</u>, jornada lab, <u>SS</u>, emigrantes, deporte, cult, ciencia y MA
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1454071502647" ID="ID_1498580596" MODIFIED="1454071676480">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No especulaci&#243;n, particip juventud, integr disminuidos,&#160; <u>pensiones</u>/servs,&#160;def consumidor, orgs prof
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454071681447" ID="ID_560563263" MODIFIED="1454071957710" TEXT="Cap 4: Garant&#xed;as">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454071686759" ID="ID_295062316" MODIFIED="1454329589289">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tutela dchos fund&#8594; cualquier ciud ante&#160; <u>Tribs ord o rec amparo</u>&#160;al TC
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1454071858336" ID="ID_1984415673" MODIFIED="1454351719195">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Def Pueblo:</i>&#160;design por&#160; <u>CG</u>, supervisa AP y dchos (todos) por&#160;<u>5 a&#241;os</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454071969166" ID="ID_651027180" MODIFIED="1454071974778" TEXT="Cap 5: Suspensi&#xf3;n">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454071974783" ID="ID_660256258" MODIFIED="1454083117222">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estado de <u>excepci&#243;n o sitio </u>&#8594; algunos derechos, no todos
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454072198292" ID="ID_1323889044" MODIFIED="1454083240906" POSITION="right" TEXT="II. Corona">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454082228153" ID="ID_866875068" MODIFIED="1454411594557">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Jefe Estado, repre internac, inviol y actos <u>refrendados</u>&#160;por Presi/Minist&#8594;resps (exc Casa Real)
    </p>
  </body>
</html></richcontent>
<node CREATED="1454082294409" ID="ID_1567964929" MODIFIED="1454083009023">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Sucesi&#243;n: </i>orden (var&#243;n, +edad) o <u>CG</u>&#160;eligen seg&#250;n intereses, matrim ok, abdics por ley org
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454083240892" ID="ID_893558698" MODIFIED="1454083245347" TEXT="Proclam ante las CG">
<node CREATED="1454083051415" ID="ID_1980477590" MODIFIED="1454155036712">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Inhabilit reconocida por CG&#8594; Pr&#237;nicipe asume o pariente +pr&#243;x, si no&#8594;&#160; <u>CG</u>&#160;nombra a <u>1-3-5</u>&#160;pers
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454083275613" ID="ID_1559208879" MODIFIED="1454083624929" TEXT="Funcs">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1454083280096" ID="ID_192681162" MODIFIED="1454330005578">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sanc/prom <u>leyes</u>&#160;y decretos CM, conv/dis&#160; <u>CG</u>, eleccs y refers; decl <u>guerra/paz</u>&#160;prev aut CG, prop y nombrar <u>Presi</u>&#160;y&#160;<u>miembros</u>&#160;Gob&#160;a prop de Presi, presidir CM a pet Presi, sup FA/RAcad, modera instits
    </p>
  </body>
</html></richcontent>
<node CREATED="1454083788408" ID="ID_1695288387" MODIFIED="1454083816327">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Prop y nombrar Presi&#8594; refrenda <u>Presi Congreso</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1454072313735" ID="ID_1089811400" MODIFIED="1454072320592" POSITION="right" TEXT="III: Cortes Gen">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454072332931" ID="ID_1942630717" MODIFIED="1454072341802">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cap 1: C&#225;maras
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454083866064" ID="ID_961856485" MODIFIED="1454084430709">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Potestad <u>leg</u>, aprob Presus, ctrl Gob, inviol, ctrl judicial&#8594; Sala&#160; <u>Penal-TS</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1454084295034" ID="ID_159605281" MODIFIED="1454084347766">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Incompat:</i>&#160;TC, altos cargos AP (exc Gob), DP, Jueces, FFSS, Junta Elect
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454084455797" ID="ID_1580179454" MODIFIED="1454084592352">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Propios <u>Reglams</u>&#160;(may abs)
    </p>
  </body>
</html></richcontent>
<node CREATED="1454084592357" ID="ID_1267599348" MODIFIED="1454084886036">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sept-dic/feb-jun&#8594; sesiones extraord: Dip Perm (21 pers) a su pet, del Gob o de may abs
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="calendar"/>
<node CREATED="1454084919680" ID="ID_933298839" MODIFIED="1454084966342">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>P&#250;b</u>&#160;salvo may abs
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454084693382" ID="ID_1890670713" MODIFIED="1454321843761">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Plenos y Comisiones&#8594;</i>&#160;pueden delegar en <u>Coms Legs Perm</u>&#160;aprob de proys/props de ley
    </p>
  </body>
</html></richcontent>
<node CREATED="1454084724693" ID="ID_513816828" MODIFIED="1454084755159">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reforma const, internac, ley org y de bases y Presus
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="stop-sign"/>
</node>
</node>
<node CREATED="1454083917141" ID="ID_983111021" MODIFIED="1454084102960">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Congreso (baja)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454083923554" ID="ID_990050806" MODIFIED="1454411631329">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      300-400 (<u>350</u>) Diputs sufr univ/Ley d'Hont,&#160; <u>2/prov</u>&#160;(1 Ceu y 1 Mel), 4 a&#241;os&#8594; elecs tras 30-60 d&#237;as
    </p>
  </body>
</html></richcontent>
<node CREATED="1454084022425" ID="ID_1524430412" MODIFIED="1454084117968">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Electos convoc &lt;<u>25 d&#237;as</u>&#160;tras elecs
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454084078065" ID="ID_1346603681" MODIFIED="1454084107126">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Senado (alta)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454084079862" ID="ID_294535039" MODIFIED="1454084277013">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Repr territ, <u>4/prov</u>&#160;(2 Ceu, 2 Mel, 3 isla may/1 men) + CA&#8594; 1+1/mill&#243;n habs, 4 a&#241;os
    </p>
  </body>
</html></richcontent>
<node CREATED="1454085648538" ID="ID_746176552" MODIFIED="1454406221718">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Puede <u>vetar</u>&#160;proy ley (en 2 meses)&#8594; may abs Congr
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1454072349976" ID="ID_1557280305" MODIFIED="1454085417190" TEXT="Cap 2: Leyes">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454085410121" ID="ID_904500503" MODIFIED="1454085415901" TEXT="Tipos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1454085024685" ID="ID_1776917036" MODIFIED="1454085282726" TEXT="Org&#xe1;nicas">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454085028139" ID="ID_980671725" MODIFIED="1454085069591">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Dchos y libs fund, Estatutos Aut y r&#233;g electoral&#8594; may abs Congreso&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1454085246011" ID="ID_776244757" MODIFIED="1454346754599">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ordinarias&#8594; resto (may simple)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454085098474" ID="ID_1041620508" MODIFIED="1454085256342" TEXT="Decreto Legislativo">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454085102822" ID="ID_119468499" MODIFIED="1454347345730">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Deleg leg al <u>Gob</u>&#8594;&#160;textos articulados (ley de&#160; <u>bases</u>) o refundidos (ordinaria). Nunca org&#225;nicos
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454085284410" ID="ID_1638386399" MODIFIED="1454085291091" TEXT="Decreto-ley">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454085291096" ID="ID_14188135" MODIFIED="1454085382930">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Urgente&#8594; <u>Gob</u>&#160;(nunca temas org&#225;nicos)&#8594; votaci&#243;n del Congreso en &lt;30 d&#237;as
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454085418152" ID="ID_1034087186" MODIFIED="1454085421909" TEXT="Iniciativa leg">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454085421913" ID="ID_993988351" MODIFIED="1454085512617">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Gob, C/S, Asambleas CA o popular (&gt;<u>500k</u>&#160;firmas, nunca org&#225;nica/trib/internac)
    </p>
  </body>
</html></richcontent>
<node CREATED="1454085551041" ID="ID_1418700606" MODIFIED="1454085567794" TEXT="Proyecto de ley (aprob CM) o Proposici&#xf3;n (-priorit)"/>
</node>
</node>
<node CREATED="1454085727737" ID="ID_126330654" MODIFIED="1454085732837" TEXT="Sanci&#xf3;n/promulg">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454085732841" ID="ID_723297941" MODIFIED="1454085744573">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Rey</u>&#160;en &lt;15 d&#237;as tras aprob CG
    </p>
  </body>
</html></richcontent>
<node CREATED="1454085745212" ID="ID_960208068" MODIFIED="1454085825637">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si especial trascendencia&#8594; conv <u>refer&#233;ndum</u>&#160;(a prop del Presi autorizada por Congr)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
<node CREATED="1454072356509" ID="ID_1462367361" MODIFIED="1454072370016" TEXT="Cap 3: Tratados Int">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454085979772" ID="ID_735630219" MODIFIED="1454086070757">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Autoriza&#160; <u>CG</u>&#160;por ley org si&#8594;&#160;tema pol&#237;tico, militar, territ, dchos fund, Hacienda, modif otra ley&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1454086091775" ID="ID_1469889308" MODIFIED="1454086130817">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Exige <u>revisi&#243;n</u>&#160;Const si va en contra
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1454072382453" ID="ID_576554392" MODIFIED="1454087176874" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      IV. Gob y AP
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454086344291" ID="ID_1813988439" MODIFIED="1454086350038" TEXT="Gob">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454086353077" ID="ID_1204104406" MODIFIED="1454086398794">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Potestad <u>ejec y reglam</u>, pol&#237;tica int-ext, Adm civ/milit y def del Estado
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1454086837971" ID="ID_482488308" MODIFIED="1454086846911" TEXT="Presi, Vicepresis y Ministros">
<node CREATED="1454087762479" ID="ID_825390761" MODIFIED="1454087770997">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Consejo Estado: </i>&#243;rg consultivo
    </p>
  </body>
</html></richcontent>
<node CREATED="1454083866064" ID="ID_101312761" MODIFIED="1454087153268">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ctrl judicial&#8594; Sala&#160; <u>Penal-TS</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1454086848542" ID="ID_313274036" MODIFIED="1454086860047" TEXT="Presi">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454086860052" ID="ID_567471171" MODIFIED="1454087055564">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Rey consulta</u>&#160;con repres de partidos y propone&#8594; expone su pgm al Congreso y pide <u>confianza</u>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1454086962438" ID="ID_887340694" MODIFIED="1454087044372">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      May abs, o simple en 48h. Si no&#8594; nuevas props en &lt;<u>2 meses</u>&#8594; Rey disuelve y nuevas elecs
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454087171523" ID="ID_1073992173" MODIFIED="1454087173042" TEXT="AP">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454087184988" ID="ID_216937888" MODIFIED="1454350274533">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ppios</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1454350274539" ID="ID_554952668" MODIFIED="1454351262336">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Eficacia, jerarq, descentr (a dif pers jur) / desconcentr (= pers jur) y coord con somet a ley y dcho
    </p>
  </body>
</html></richcontent>
<node CREATED="1454350209876" ID="ID_61510777" MODIFIED="1454351205868">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      AGE, CCAA, EELL e Instit (OOAA, Ents P&#250;b y Agencias)&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454350294491" ID="ID_249207673" MODIFIED="1454351461527">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      LOFAGE
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1454350391770" ID="ID_1286565459" MODIFIED="1454351306903">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>&#211;rgs sup:</i>&#160;Ministros y Secres Estado
    </p>
  </body>
</html></richcontent>
<node CREATED="1454350979165" ID="ID_1544643465" MODIFIED="1454350993595">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Creaci&#243;n por <u>RD</u>-Presi
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454350412033" ID="ID_936795276" MODIFIED="1454351081990">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>&#211;rgs directivos</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1454350799242" ID="ID_822942907" MODIFIED="1454351315907">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Subsecres/Secres Gen, Secres Gen T&#233;cn/Dirs Gen y Subdir Gen (<u>no alto</u>&#160;cargo)
    </p>
  </body>
</html></richcontent>
<node CREATED="1454350903378" ID="ID_1683935332" MODIFIED="1454350942644">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Creaci&#243;n por <u>RD</u>-CM (inf&#8594;OM)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454351081974" ID="ID_982802484" MODIFIED="1454351099462">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Servs comunes&#8594;&#160;Subsecre + SGT
    </p>
  </body>
</html></richcontent>
<node CREATED="1454350767690" ID="ID_360366332" MODIFIED="1454416621722">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Org territ:&#160; </i>adscritos a MinHAP&#8594; Deleg Gob en CA (= Subsecre) y Subdel Gob (= Subdir Gen)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1454072389124" ID="ID_1526258035" MODIFIED="1454072395955" POSITION="right" TEXT="V. Rels Gob-CG">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454087825885" ID="ID_785812757" MODIFIED="1454088023171">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CG&#160;pueden pedir&#8594;
    </p>
  </body>
</html></richcontent>
<node CREATED="1454088009705" ID="ID_998200308" MODIFIED="1454088440659">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Info a cualquier autoridad Estado/CA y presencia de miembros Gob&#160;(q pueden solicitar a funcionarios)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454088107109" ID="ID_295646135" MODIFIED="1454088113359" TEXT="Cuesti&#xf3;n de confianza">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454088113363" ID="ID_661816326" MODIFIED="1454416543894">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Solo&#160; <u>Presi</u>&#160;la puede solicitar al Congreso sobre su pgm o declaraci&#243;n&#8594; req may <u>simple</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454088185147" ID="ID_122642548" MODIFIED="1454088312367">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Moci&#243;n de censura
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454088191405" ID="ID_1742663377" MODIFIED="1454088263116">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Congreso exige responsab al Gob&#8594; req may <u>abs</u>&#160;(a los 5 d&#237;as)
    </p>
  </body>
</html></richcontent>
<node CREATED="1454088219947" ID="ID_1803992545" MODIFIED="1454088241181">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Prop por &gt;<u>1/10</u>&#160;Congr e incluye candidato a Presi
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454088304844" ID="ID_894019186" MODIFIED="1454088310291" TEXT="Dimisi&#xf3;n Presi">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454088311227" ID="ID_712523612" MODIFIED="1454088343187">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si se niega confianza o prospera una moci&#243;n&#8594; Rey nombra nuevo Presi
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454088465080" ID="ID_1834689221" MODIFIED="1454089176793" TEXT="Estados">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1454088467157" ID="ID_384773739" MODIFIED="1454089167620">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Alarma:</i>&#160;cat&#225;strofe, crisis salud, no servs p&#250;b
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1454088483468" ID="ID_1275106503" MODIFIED="1454347686398">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Decl por decreto CM- <u>Gob</u>&#160;durante &lt;<u>15 d&#237;as</u>&#160;(o + si autoriza el Congr)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454088631758" ID="ID_969892947" MODIFIED="1454089024758">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Excepci&#243;n: </i>se suspenden algunos dchos
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1454088634362" ID="ID_268289441" MODIFIED="1454088753993">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Decl por decreto CM- <u>Gob</u>&#160;previa autoriz del <u>Congr</u>&#160;&#160;y durante &lt;<u>30 d&#237;as</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454088730371" ID="ID_1018119360" MODIFIED="1454089131557">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Sitio: </i>guerra, no hay garant&#237;as constit
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1454088731951" ID="ID_1521093253" MODIFIED="1454089193724">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Decl por <u>Congreso</u>&#160;(may abs) a prop del <u>Gob</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1454072410875" ID="ID_1645106460" MODIFIED="1454091536701" POSITION="right" TEXT="VI. Poder Jud">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454090902916" ID="ID_120187995" MODIFIED="1454348237506">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Potestad <u>jurisdic</u>, Jueces/Magis <u>indep&#160;e inamov</u>, unidad (exc militar), jurado <u>popular</u>&#160;(prov)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1454091018897" ID="ID_539576965" MODIFIED="1454091086324">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Gratis</u>&#160;si se acredita insufic de recursos, actos p&#250;b, proced pref <u>oral</u>, indemniz por error
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1454091497403" ID="ID_575932855" MODIFIED="1454091520226">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Incompat</u>&#160;con otros cargos p&#250;b, partidos o sindics
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454091095161" ID="ID_364702216" MODIFIED="1454091104064">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Consejo Gen del PJ
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454091107963" ID="ID_1684265485" MODIFIED="1454328207429">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#211;rg de gob del PJ por <u>5 a&#241;os</u>&#8594; Presi <u>TS</u>&#160;+ 20 miembros (12 jueces, <u>4C y 4S</u>&#160;eleg por 3/5)
    </p>
  </body>
</html></richcontent>
<node CREATED="1454091223289" ID="ID_1971587940" MODIFIED="1454091412923">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Trib Supremo:</i>&#160;&#243;rg sup, Presi nombr por <u>Rey</u>&#160;a prop de CGPJ
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454091348362" ID="ID_260771586" MODIFIED="1454091352213" TEXT="Minist Fiscal">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454091352220" ID="ID_1046163913" MODIFIED="1454347441866">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Defiende legalidad, dchos ciuds (rec <u>amparo</u>) y vela por indep de Tribs
    </p>
  </body>
</html></richcontent>
<node CREATED="1454091388879" ID="ID_1943568888" MODIFIED="1454155315946">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Fiscal Gen del Estado: </i>nombr por <u>Rey</u>&#160;a prop de Gob, oido el CGPJ
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454348075069" ID="ID_1294655395" MODIFIED="1454348079848" TEXT="Jerarqu&#xed;a">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454348079853" ID="ID_490943782" MODIFIED="1454348271926">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Trib Supremo &gt; Aud Nac &gt; Tribs Sup (CA) &gt; Aud Prov &gt; Juzgados 1&#170;inst o Paz
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454072417614" ID="ID_224416071" MODIFIED="1454092976893" POSITION="right" TEXT="VII. Eco y Hacienda">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454092976875" ID="ID_347560277" MODIFIED="1454093012764">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Potestad <u>tributaria</u>&#160;corresponde al <u>Estado</u>&#8594; riqueza subord al inter&#233;s general
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1454092725085" ID="ID_1809066436" MODIFIED="1454092851006">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Gob</u>&#160;elab proys de planif eco por medio de un Consejo
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454093030090" ID="ID_1263198107" MODIFIED="1454093037574">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Presup Gen Estado
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454093037739" ID="ID_1245623310" MODIFIED="1454093126442">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Anual, elab por <u>Gob</u>&#160;y aprob por <u>CG</u>&#160;(presentado <u>3 meses</u>&#160;antes del fin anterior)
    </p>
  </body>
</html></richcontent>
<node CREATED="1454093185285" ID="ID_826498428" MODIFIED="1454093205576">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      La ley PGE <u>no</u>&#160;puede crear <u>tributos</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454093218018" ID="ID_727305893" MODIFIED="1454346225860">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estab presup (art 135, Ref)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454093233017" ID="ID_607996288" MODIFIED="1454346248922">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>D&#233;ficit</u>&#160;&#160;Estado/CA &lt; m&#225;rgenes de la <u>UE</u>&#160;seg&#250;n PIB (salvo emerg por may abs Congr)
    </p>
  </body>
</html></richcontent>
<node CREATED="1454093315259" ID="ID_1193010921" MODIFIED="1454093429667">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Req <u>autoriz</u>&#160;para emitir deuda p&#250;b o contraer cr&#233;dito
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454093457225" ID="ID_10924411" MODIFIED="1454093462018" TEXT="Trib Cuentas">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454093462023" ID="ID_657004777" MODIFIED="1454329432258">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#211;rg sup <u>fiscalizador</u>&#160;(dep de <u>CG</u>)&#8594; se le rinden las cuentas del Estado y SP y las censura
    </p>
  </body>
</html></richcontent>
<node CREATED="1454329351944" ID="ID_250023286" MODIFIED="1454329467564">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Informe&#160; <u>anual</u>&#160;a CG con infraccs. =indep, inamov e incompat q jueces
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1454072425287" ID="ID_1563671782" MODIFIED="1454072437409" POSITION="right" TEXT="VIII. Org Territ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454072439634" ID="ID_1741369790" MODIFIED="1454072443473" TEXT="Cap 1: Ppios">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454151727654" ID="ID_1147685110" MODIFIED="1454151918498">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Autonom&#237;a</u>&#160;&#160;CA/provs/municips,&#160; <u>solidaridad</u>&#160;e igual (equil), libre circul y establec de personas
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454072444314" ID="ID_1355199375" MODIFIED="1454072461940">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cap 2: EELL
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454151826597" ID="ID_1301783232" MODIFIED="1454152107459">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Municipios:</i>&#160;&#160;pers jur <u>plena</u>&#8594; Ayunts con Alcalde (por Concej o sufr univ) y Concejales (suf univ)&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1454152071598" ID="ID_1672696919" MODIFIED="1454152087120">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tribs: propios, del Estado y CA
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454151947922" ID="ID_217314517" MODIFIED="1454151995887">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Provincias: </i>pers jur <u>propia</u>&#8594; l&#237;mites aprob por <u>CG</u>&#160; por ley org
    </p>
  </body>
</html></richcontent>
<node CREATED="1454151996487" ID="ID_1791167396" MODIFIED="1454152049527">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Gob:&#160;<u>Diputaciones</u>&#160;o Corp repre / Islas&#8594; Cabildos o Consejos
    </p>
  </body>
</html></richcontent>
<node CREATED="1454406539258" ID="ID_1410339722" MODIFIED="1454406545969" TEXT="Se permiten otras agrupaciones de munic"/>
</node>
</node>
</node>
<node CREATED="1454072451409" ID="ID_219087427" MODIFIED="1454072456640" TEXT="Cap 3: CCAA">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454152126897" ID="ID_1804883226" MODIFIED="1454153395562" TEXT="art 143">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<node CREATED="1454152129968" ID="ID_198039085" MODIFIED="1454152205288">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Provs lim&#237;trofes con caracts comunes,&#160; <u>hist&#243;ricas</u>&#160;e islas&#8594; autogob
    </p>
  </body>
</html></richcontent>
<node CREATED="1454152216244" ID="ID_1227841660" MODIFIED="1454152267509">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Iniciativa: Diputs/Cabildos + <u>2/3</u>&#160;municips
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454152289698" ID="ID_1174346201" MODIFIED="1454152405851">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>CG:</i>&#160;autoriz CA de 1 prov y Estatutos de territs no integr, y posible iniciativa&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1454152438005" ID="ID_1024255546" MODIFIED="1454152466460">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Federaciones</u>&#160;(s&#237; convenios previa autoriz CG)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="stop-sign"/>
</node>
</node>
<node CREATED="1454152492328" ID="ID_156862533" MODIFIED="1454152494587" TEXT="Estatutos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454152494591" ID="ID_1834518198" MODIFIED="1454152672581">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Elab por <u>Asamblea</u>&#160;(Diputs + C/S elegidos) y elev a <u>CG</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1454152571426" ID="ID_1011552622" MODIFIED="1454154947316">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Denom, delimit, institucs y compets asumidas
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1454152648023" ID="ID_630473409" MODIFIED="1454154935086">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reforma&#8594; aprob CG
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1454152788736" ID="ID_456107283" MODIFIED="1454349120172">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Competencias
    </p>
    <p>
      (art 148, 149)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454152791911" ID="ID_1094649619" MODIFIED="1454349130953">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Asumibles
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454152802961" ID="ID_1089248976" MODIFIED="1454152923185">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Org instit, ord terrir, obras p&#250;b, transp, puertos no comerc, agri/ganad/pesca, montes, MA,
    </p>
    <p>
      aguas, ferias, artesan&#237;a, museos/bibl, cultura, turismo, deporte, asist soc, sanidad, vigil
    </p>
  </body>
</html></richcontent>
<node CREATED="1454152849468" ID="ID_1472124948" MODIFIED="1454153457481" TEXT="Ampliables tras 5 a&#xf1;os">
<icon BUILTIN="calendar"/>
</node>
</node>
</node>
<node CREATED="1454152935617" ID="ID_1779503794" MODIFIED="1454349124569">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Exclusivas Estado
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454152938926" ID="ID_145357923" MODIFIED="1454416801765">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Igualdad</u>, emigr, internac, defensa, <u>justicia</u>,&#160;<u>legisl</u>&#160;merc/penal/penit/lab/civ/PI/contr, comerc ext,
    </p>
    <p>
      moneda, medidas, hacienda, <u>SS</u>, funcionarios/PAC, armas, comunics,&#160; <u>autoriz refer&#233;ndum</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454153184522" ID="ID_156151083" MODIFIED="1454153198161">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Delegadas
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1454153189397" ID="ID_583806692" MODIFIED="1454406469927">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>CG</u>&#160;a CA a trav&#233;s de&#160;<u>ley marco</u>&#8594; fijado por ley estatal
    </p>
  </body>
</html></richcontent>
<node CREATED="1454153317430" ID="ID_1069306181" MODIFIED="1454406474450">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Estado</u>&#160;a CA a trav&#233;s de <u>ley org</u>&#8594; facultades estatales
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1454153384828" ID="ID_1356128008" MODIFIED="1454153397976" TEXT="art 151">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<node CREATED="1454153492648" ID="ID_1514769667" MODIFIED="1454153581781">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      R&#233;g especial&#8594;<i>&#160;</i>iniciativa de Diputs + <u>3/4</u>&#160;municips + <u>refer&#233;ndum</u>&#160;(may abs de cada prov)
    </p>
  </body>
</html></richcontent>
<node CREATED="1454153610880" ID="ID_580289990" MODIFIED="1454348871543">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      +compets sin esperar 5 a&#241;os
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454153663420" ID="ID_1248979573" MODIFIED="1454153665805" TEXT="Estatutos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454153668638" ID="ID_1161595377" MODIFIED="1454153825265">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Elab por <u>Asamblea</u>&#160;de C/S elegidos (y convoc por Gob)
    </p>
  </body>
</html></richcontent>
<node CREATED="1454153819407" ID="ID_1388906241" MODIFIED="1454153837443">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Remit a <u>Com Constit del Congr </u>&#8594; refer&#233;ndum&#8594; elev a CG&#8594; sanc por <u>Rey</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1454153926704" ID="ID_1850987893" MODIFIED="1454153943270" TEXT="&#xd3;rgs">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454153943274" ID="ID_1654170158" MODIFIED="1454154036852">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Asamblea Leg:</i>&#160;sufr univ
    </p>
  </body>
</html></richcontent>
<node CREATED="1454154036857" ID="ID_934459182" MODIFIED="1454154052846">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Consejo de Gob:</i>&#160;Presi eleg por Asamb y nombr por Rey&#8594; repr <u>CA</u>&#160; y del <u>Estado</u>&#160;en ella
    </p>
  </body>
</html></richcontent>
<node CREATED="1454154076565" ID="ID_980851003" MODIFIED="1454154086164">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Trib Superior: </i>justicia
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454154099597" ID="ID_435196797" MODIFIED="1454154646645">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Control:&#160; </i>Trib Const, Gob, jurisd cont-adm y Trib Cuentas
    </p>
  </body>
</html></richcontent>
<node CREATED="1454154164566" ID="ID_1270785718" MODIFIED="1454154680621">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Deleg del Gob:</i>&#160;dirige y coord Adm Estado en la CA
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1454154596074" ID="ID_1525121938" MODIFIED="1454154693007">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cumplim forzoso de obligs:</i>&#160;&#160;Gob req al Presi CA, si no&#8594; medidas aprob por <u>Senado</u>&#160;(may abs)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454154760289" ID="ID_1204257776" MODIFIED="1454154764446" TEXT="Autonom&#xed;a financ">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454154764450" ID="ID_1377168395" MODIFIED="1454348907626">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Impuestos (Estado y propios), <u>Fondo</u>&#160;Compens territ ( <u>CG</u>), PGE (seg&#250;n servs asum), ingr priv, cr&#233;ditos
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1454072473390" ID="ID_375590291" MODIFIED="1454155754590" POSITION="right" TEXT="IX. Trib Const">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454091107963" ID="ID_1432270572" MODIFIED="1454328189505">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      12 miembros nombr por Rey (<u>4C y 4S</u>&#160;eleg por 3/5, <u>2 Gob y 2 CGPJ</u>) juristas con &gt;15 a&#241;os exp
    </p>
  </body>
</html></richcontent>
<node CREATED="1454155229730" ID="ID_1371113113" MODIFIED="1454416442048">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>9 a&#241;os</u>, =incompats q jueces / <i>Presi:</i>&#160;nombr por Rey a prop del pleno <u>TC</u>&#160;por <u>3 a&#241;os</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1454155754573" ID="ID_10779588" MODIFIED="1454155764318" TEXT="Funcs">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1454155333317" ID="ID_1715805918" MODIFIED="1454155760652">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Rec de Inconstit
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454155384341" ID="ID_112293160" MODIFIED="1454155504547">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contra&#160; <u>leyes</u>&#8594; Presi, DP, 50C y 50S y CCAA (&#243;rgs y Asamb)
    </p>
  </body>
</html></richcontent>
<node CREATED="1454155643674" ID="ID_693373093" MODIFIED="1454155784617">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cuesti&#243;n de Inconst:</i>&#160; <u>juez</u>&#160;sobre ley de cuya validez depende&#160;el fallo (efecto no suspensivo)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454155410764" ID="ID_631872959" MODIFIED="1454155761864" TEXT="Rec de Amparo">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454155414732" ID="ID_1948689576" MODIFIED="1454347425889">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contra&#160; <u>actos</u>&#160;q violen dchos y libs fund&#8594; cualquier pers nat/jur, DP y&#160; <u>MF</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1454348689554" ID="ID_1006793945" MODIFIED="1454412004172">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si es por app de una ley&#8594; <u>autocuesti&#243;n</u>&#160;&#160;de constit (resp: el legislador)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454155547262" ID="ID_1201353743" MODIFIED="1454155748002">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Conflictos de competencia
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454155552812" ID="ID_827279763" MODIFIED="1454348543231">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estado vs CCAA (o entre ellas). Gob puede impugnar resols de CA
    </p>
  </body>
</html></richcontent>
<node CREATED="1454348386878" ID="ID_1205776936" MODIFIED="1454348555583">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      + (no se respeta orden compets), - (nadie se hace cargo), EELL (contra ley CA)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1454155790190" ID="ID_1699596207" MODIFIED="1454155797837" TEXT="Sentencias">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454155791609" ID="ID_829869310" MODIFIED="1454155879325">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Public en <u>BOE</u>, no cabe recurso y subsiste la vigencia de <u>parte no inconstit</u>&#160;&#160;de la ley
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454072484370" ID="ID_1385980164" MODIFIED="1454346974358" POSITION="right" TEXT="X. Reforma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454085421913" ID="ID_714402587" MODIFIED="1454156486733">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Iniciativa:&#160; </i>Gob, C/S o Asambleas CA ( <u>no popular </u>) y nunca en estado de alarma, excep o sitio
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1454346974339" ID="ID_1741629779" MODIFIED="1454346978976" TEXT="Ordinaria">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454155907215" ID="ID_335411681" MODIFIED="1454156053623">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Proys aprob por 3/5 C&#225;maras. Si no&#8594; Comisi&#243;n mixta elab texto&#8594; may abs Senado y 2/3 Congr&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1454156054251" ID="ID_1613294290" MODIFIED="1454347035254">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Aprob <u>CG</u>&#8594; <u>refer&#233;ndum</u>&#160;en 15 d&#237;as si lo pide <u>1/10&#160;de C o S</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1454156132177" ID="ID_737197017" MODIFIED="1454156456935">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Rev total o T&#237;t Pre/I-cap2-sec1/II
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1454156160984" ID="ID_653820481" MODIFIED="1454156273103">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Aprob por 2/3 C&#225;maras y <u>disoluci&#243;n</u>&#160;&#160;de CG
    </p>
  </body>
</html></richcontent>
<node CREATED="1454156266698" ID="ID_131068500" MODIFIED="1454156292732">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Nuevas C&#225;maras&#8594; nuevo texto aprob por 2/3 C&#225;maras&#8594; <u>refer&#233;ndum</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
</map>
