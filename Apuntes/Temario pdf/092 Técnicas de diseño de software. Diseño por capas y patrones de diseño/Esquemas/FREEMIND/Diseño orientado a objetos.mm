<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1369934844324" ID="ID_505670976" MODIFIED="1378202655972" TEXT="Dise&#xf1;o orientado a objetos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369934906966" ID="ID_1883011678" MODIFIED="1517572557842" POSITION="right" TEXT="Caracter&#xed;sticas">
<node CREATED="1369934915324" ID="ID_1782433428" MODIFIED="1369934918315" TEXT="Modularidad"/>
<node CREATED="1369934932020" ID="ID_21503483" MODIFIED="1369934934863" TEXT="Ocultaci&#xf3;n"/>
<node CREATED="1369934935324" ID="ID_1595750871" MODIFIED="1369934942821" TEXT="Acoplamiento d&#xe9;bil"/>
<node CREATED="1369934943188" ID="ID_1613134880" MODIFIED="1369934948407" TEXT="Cohesi&#xf3;n fuerte"/>
<node CREATED="1369934949172" ID="ID_729184844" MODIFIED="1369934953509" TEXT="Extensibilidad"/>
<node CREATED="1369934954039" ID="ID_644567499" MODIFIED="1369934958448" TEXT="Integrable"/>
<node CREATED="1369934958858" ID="ID_1256150600" MODIFIED="1369934965453" TEXT="Reusabilidad"/>
</node>
<node CREATED="1369934859779" ID="ID_431382122" MODIFIED="1385638555795" POSITION="right" TEXT="Metodolog&#xed;a OMT (Jim Rumbaugh)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369934978561" ID="ID_1719061427" MODIFIED="1369934982070" TEXT="Fases">
<node CREATED="1369934995768" ID="ID_1372108941" MODIFIED="1369934999742" TEXT="An&#xe1;lisis"/>
<node CREATED="1369935000025" ID="ID_1601634901" MODIFIED="1369935002093" TEXT="Dise&#xf1;o"/>
<node CREATED="1369935002484" ID="ID_953364770" MODIFIED="1369935006767" TEXT="Implementaci&#xf3;n"/>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1369935009524" ID="ID_1234876138" MODIFIED="1375109706800" STYLE="bubble" TEXT="Modelos">
<node CREATED="1369935028148" ID="ID_878916767" MODIFIED="1517572563217" TEXT="Modelo de objetos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935037994" ID="ID_1094789281" MODIFIED="1378202664685" TEXT="Diagrama de Objetos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935047715" ID="ID_841325314" MODIFIED="1375109696275" TEXT="nodos: clases">
<node CREATED="1369935177051" ID="ID_340456095" MODIFIED="1375109696275" TEXT="Clases: rect&#xe1;ngulo"/>
<node CREATED="1369935284073" ID="ID_1858945237" MODIFIED="1375109696276" TEXT="Objeto: rect&#xe1;ngulo con esquinas redondeadas"/>
</node>
<node CREATED="1369935055384" ID="ID_487636243" MODIFIED="1375109696276" TEXT="arcos: relaciones"/>
</node>
<node CREATED="1369935161420" ID="ID_1730622062" MODIFIED="1375109696277" TEXT="Ampliaci&#xf3;n del diagrama E/R"/>
</node>
<node CREATED="1369935101731" ID="ID_1375315476" MODIFIED="1517572564661" TEXT="Modelo funcional">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935112492" ID="ID_1321008174" MODIFIED="1378202669702" TEXT="Diagramas de Flujo de Datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935127963" ID="ID_854566379" MODIFIED="1375109696278" TEXT="nodos: procesos"/>
<node CREATED="1369935136700" ID="ID_1079966724" MODIFIED="1375109696278" TEXT="arcos: flujos de datos"/>
</node>
</node>
<node CREATED="1369935069813" ID="ID_1359505507" MODIFIED="1517572565643" TEXT="Modelo din&#xe1;mico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935077694" ID="ID_156788296" MODIFIED="1378202671718" TEXT="Diagramas de Estado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935085306" ID="ID_860982862" MODIFIED="1375109696280" TEXT="nodos: estados"/>
<node CREATED="1369935092113" ID="ID_1750873719" MODIFIED="1375109696280" TEXT="arcos: transiciones"/>
</node>
</node>
</node>
</node>
<node CREATED="1369934870524" ID="ID_1218140740" MODIFIED="1384617684038" POSITION="right" TEXT="UML (Booch, Jacobson y Rumbaugh)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935452104" ID="ID_1976213944" MODIFIED="1517572560802" TEXT="Vistas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935458800" ID="ID_205564140" MODIFIED="1385638808746" TEXT="Casos de uso. Funcionalidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935753315" ID="ID_1323118646" MODIFIED="1369935760839" TEXT="Analisis"/>
</node>
<node CREATED="1369935463177" ID="ID_1765041725" MODIFIED="1385638809306" TEXT="L&#xf3;gica. Funcionalidad interna y estructura est&#xe1;tica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935763258" ID="ID_1914617592" MODIFIED="1369935768167" TEXT="Desarrolladores"/>
</node>
<node CREATED="1369935467066" ID="ID_942580889" MODIFIED="1385638810130" TEXT="Componentes. M&#xf3;dulos del sistema y dependencias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935853855" ID="ID_1046602564" MODIFIED="1369935863707" TEXT="Desarrolladores"/>
</node>
<node CREATED="1369935471701" ID="ID_597968804" MODIFIED="1385638810530" TEXT="Concurrencia. Divisi&#xf3;n en procesos y procesadores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935899888" ID="ID_2259065" MODIFIED="1369935912347" TEXT="Desarrolladores e integradores"/>
<node CREATED="1369935912732" ID="ID_1336157487" MODIFIED="1369935921065" TEXT="Problemas de recursos"/>
</node>
<node CREATED="1369935478292" ID="ID_148675669" MODIFIED="1385638811298" TEXT="Despliegue. Distribuci&#xf3;n f&#xed;sica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935952080" ID="ID_1124135729" MODIFIED="1369935961671" TEXT="Desarrolladores e integradores"/>
</node>
</node>
<node CREATED="1369935484379" ID="ID_1616384473" LINK="../Metrica/M&#xe9;trica%20(II).mm" MODIFIED="1385640267007" TEXT="Diagramas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935494393" ID="ID_175147156" MODIFIED="1517572586644" TEXT="Casos de uso (Jacobson)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370037081137" ID="ID_1765768339" MODIFIED="1370037091186" TEXT="Elementos">
<node CREATED="1370037091187" ID="ID_1125344947" MODIFIED="1378202776921" TEXT="Actores ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370037094766" ID="ID_578390093" MODIFIED="1378202780490" TEXT="Casos de uso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370037099269" ID="ID_71553682" MODIFIED="1517572605659" TEXT="Relaciones">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      En la versi&#xf3;n de esta t&#xe9;cnica en METRICA V.3 s&#xf3;lo aparecen las relaciones "usa" y "extiende"
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369949242458" ID="ID_1992640208" MODIFIED="1378202784610" TEXT="Extensi&#xf3;n: relaci&#xf3;n de dependencia donde un caso de uso es especializaci&#xf3;n de otro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370099190720" ID="ID_1879262026" MODIFIED="1378202788954" TEXT="Generalizaci&#xf3;n. Creaci&#xf3;n de superclases.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369949248775" ID="ID_1777496040" MODIFIED="1378202792019" TEXT="Inclusi&#xf3;n: relaci&#xf3;n de dependencia entre dos casos de uso porque un escenario est&#xe1; incluido en otro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374660691300" ID="ID_1588843908" MODIFIED="1378202794714" TEXT="Relaci&#xf3;n o asociaci&#xf3;n: participaci&#xf3;n de un actor en un caso de uso.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1369935501440" ID="ID_233723319" MODIFIED="1385638937080" TEXT="Est&#xe1;ticos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935507375" ID="ID_982696846" MODIFIED="1517572588019" TEXT="De Clases">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370099567842" ID="ID_838968451" MODIFIED="1382184014065" TEXT="clases">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370099575557" ID="ID_164067885" MODIFIED="1382184016796" TEXT="interfaces">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370099582664" ID="ID_1348711641" MODIFIED="1382184017508" TEXT="colaboraciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370099586311" ID="ID_1711424809" MODIFIED="1382184018156" TEXT="relaciones">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      METRICA a&#xf1;ade una relaci&#xf3;n m&#xe1;s: composici&#xf3;n.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374661326542" ID="ID_1404885720" MODIFIED="1378202812235" TEXT="Herencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374661337033" ID="ID_1471516731" MODIFIED="1378202809163" TEXT="Agregaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374661344308" ID="ID_1197754305" MODIFIED="1378202814083" TEXT="Asociaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374661350832" ID="ID_1251892415" MODIFIED="1378202815163" TEXT="Dependencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1369935516846" ID="ID_375722976" MODIFIED="1517572589347" TEXT="De Objetos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370099598732" ID="ID_684395258" MODIFIED="1382184025619" TEXT="objetos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370099602265" ID="ID_1003782305" MODIFIED="1382184026361" TEXT="relaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1369935524305" ID="ID_486475298" MODIFIED="1384962044368" TEXT="Din&#xe1;micos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369935533014" ID="ID_1992921172" MODIFIED="1378202843797" TEXT="De Estado (tiempos). Autom&#xe1;tas.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369935537700" ID="ID_193150482" MODIFIED="1517572591834" TEXT="De Secuencia. Mensajes y evoluci&#xf3;n temporal.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1381080270414" ID="ID_1567301781" MODIFIED="1381080317562" TEXT="Foco de control">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tiempo en que el objeto se encuentra ejecutando alguna acci&#xf3;n
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1369935542632" ID="ID_1028141951" MODIFIED="1378202847124" TEXT="De Colaboraci&#xf3;n. Combina diagrama de objetos + secuencia: flujo de eventos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369935547541" ID="ID_1610192491" MODIFIED="1517572819586" TEXT="De Actividad">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Una actividad es una acci&#xf3;n no at&#xf3;mica en curso, dentro de un diagrama de estados.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384520289156" ID="ID_1119735208" MODIFIED="1384520337909" TEXT="Producen una acci&#xf3;n que origina un cambio en el sistema o retorna una acci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384520317978" ID="ID_1461172621" MODIFIED="1384520340159" TEXT="Contiene">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370099699779" ID="ID_1733313416" MODIFIED="1384519691193" TEXT="estados de actividad y de acci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370099735069" ID="ID_1603279838" MODIFIED="1384519692633" TEXT="objetos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370099738854" ID="ID_521772424" MODIFIED="1384519693343" TEXT="restricciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370099741596" ID="ID_227167790" MODIFIED="1384519694043" TEXT="transiciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1369935558350" ID="ID_400042425" MODIFIED="1378202856309" TEXT="De Componentes. Elementos SW de un nodo. interfaces. Paquetes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369935564126" ID="ID_724570021" MODIFIED="1517572596548" TEXT="De Despliegue. Nodos, Hardware y software.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372695737520" ID="ID_1548284223" MODIFIED="1378202882510" TEXT="Nodos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372695740819" ID="ID_185976855" MODIFIED="1372695745796" TEXT="HW y SW"/>
<node CREATED="1372695746287" ID="ID_626095344" MODIFIED="1378202885902" TEXT="Conexiones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1369935578218" ID="ID_308882062" MODIFIED="1384520128961" TEXT="Elementos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369936218755" ID="ID_212053952" MODIFIED="1378202967626" TEXT="Notas">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sirve para a&#xf1;adir un comentario a un diagrama o elemento del mismo
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369936221299" ID="ID_1586801260" MODIFIED="1517572582425" TEXT="Dependencias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384520133357" ID="ID_526311913" MODIFIED="1384520188208" TEXT="Significa que un cambio en el elemento destino puede significar un cambio en el elemento origen">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384520157568" ID="ID_1882726217" MODIFIED="1384520190570" TEXT="Se representa por una l&#xed;nea de trazo discontinuo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1369935585431" ID="ID_927804064" MODIFIED="1384518232988" TEXT="Mecanismos generales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370123929399" ID="ID_1708549242" MODIFIED="1385640650907" POSITION="right" TEXT="UML 2.2.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1517573735538" ID="ID_1477289234" MODIFIED="1517573774224">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="UML_diagrams_overview.jpg" />
  </body>
</html>
</richcontent>
<node CREATED="1517573757177" ID="ID_185331011" MODIFIED="1517573766708" TEXT="Ojo, se ha eliminado el diagrama de colaboraci&#xf3;n"/>
</node>
</node>
<node CREATED="1385637823229" ID="ID_1126640216" MODIFIED="1517572555722" POSITION="left" TEXT="Patrones de dise&#xf1;o">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385637839993" ID="ID_690850610" MODIFIED="1385638092260" TEXT="Objetivos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385637874149" ID="ID_75993676" MODIFIED="1385638084880" TEXT="Cat&#xe1;logos de elementos reusables">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1385637889025" ID="ID_1800737424" MODIFIED="1385638087055" TEXT="Evitar la reiteraci&#xf3;n en la b&#xfa;squeda de soluciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1385637904336" ID="ID_89812688" MODIFIED="1385638087646" TEXT="Formalizar un VOCABULARIO COMUN">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1385637917025" ID="ID_647163752" MODIFIED="1385640122437" TEXT="ESTANDARIZAR el modo en que se realiza el dise&#xf1;o">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1385637934797" ID="ID_398448388" MODIFIED="1385640104092" TEXT="Facilitar el APRENDIZAJE de las nuevas generaciones de dise&#xf1;adores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1385637963930" ID="ID_256731989" MODIFIED="1385638093195" TEXT="No pretenden">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385637969585" ID="ID_1756906912" MODIFIED="1385638089829" TEXT="Imponer ciertas alternativas  de dise&#xf1;o">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1385637984820" ID="ID_1365225370" MODIFIED="1385638090509" TEXT="Eliminar la creatividad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1385638061049" ID="ID_1790604741" MODIFIED="1385640965573" POSITION="left" TEXT="GoF (Gang of Four)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385638102638" ID="ID_778159261" MODIFIED="1517572553061" TEXT="Object Pool (No pertenece a GoF)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385638120842" ID="ID_660823788" MODIFIED="1385638124511" TEXT="Clonaci&#xf3;n"/>
</node>
<node CREATED="1385638108011" ID="ID_1146307387" MODIFIED="1517572554412" TEXT="Abstract Factory">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385638128686" ID="ID_442023848" MODIFIED="1385638136551" TEXT="Objetos de distintas familias"/>
</node>
<node CREATED="1385638139222" ID="ID_1985422298" MODIFIED="1517572569926" TEXT="Builder">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385638143912" ID="ID_124289113" MODIFIED="1385638162845" TEXT="Abstracci&#xf3;n en un &#xfa;nico punto"/>
</node>
<node CREATED="1385638165006" ID="ID_1611294829" MODIFIED="1517572571083" TEXT="Factory Method">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385638176217" ID="ID_1360947483" MODIFIED="1385638202878" TEXT="Centraliza en una clase constructora "/>
</node>
<node CREATED="1385638205079" ID="ID_1570251386" MODIFIED="1517572572065" TEXT="Prototype">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385638226778" ID="ID_839160292" MODIFIED="1385638236886" TEXT="Nuevos objetos en una instancia ya existente"/>
</node>
<node CREATED="1385638208894" ID="ID_676639052" MODIFIED="1385638221465" TEXT="Singleton">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1385638270623" ID="ID_1914681674" MODIFIED="1385640146761" POSITION="left" TEXT="Estructurales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385638282180" ID="ID_1142740342" MODIFIED="1385638379737" TEXT="Adapter o Wrapper">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1385638289945" ID="ID_1733281556" MODIFIED="1517572574171" TEXT="Brigde">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385638354145" ID="ID_496669331" MODIFIED="1385638369940" TEXT="Desacopla una abstracci&#xf3;n de su implementaci&#xf3;n"/>
</node>
<node CREATED="1385638294544" ID="ID_197261892" MODIFIED="1517572575003" TEXT="Composite">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385638345235" ID="ID_593618385" MODIFIED="1385638352209" TEXT="Objeto compuesto"/>
</node>
<node CREATED="1385638299935" ID="ID_1074608139" MODIFIED="1385638327438" TEXT="Decorator">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1385638304157" ID="ID_100043963" MODIFIED="1517572576770" TEXT="Facade">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385638390984" ID="ID_217889322" MODIFIED="1385638414200" TEXT="Proporciona una interfaz unificada simple"/>
</node>
<node CREATED="1385638308388" ID="ID_496933221" MODIFIED="1517572577513" TEXT="Flyweight">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385638419472" ID="ID_660485438" MODIFIED="1385638431156" TEXT="Reduce la redundancia"/>
</node>
<node CREATED="1385638314563" ID="ID_1928295720" MODIFIED="1385638329485" TEXT="Proxy">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1385638317547" ID="ID_884680238" MODIFIED="1517572578554" TEXT="M&#xf3;dulo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385638440019" ID="ID_336521608" MODIFIED="1385638452399" TEXT="Agrupa varios elementos relacionados"/>
</node>
</node>
<node CREATED="1385638462997" ID="ID_201244101" MODIFIED="1385640653631" POSITION="left" TEXT="De comportamiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1385638474866" ID="ID_1658667803" MODIFIED="1385638481079" TEXT="Chain of responsability"/>
<node CREATED="1385638481408" ID="ID_382353974" MODIFIED="1385638484110" TEXT="Command"/>
<node CREATED="1385638491846" ID="ID_933918145" MODIFIED="1385638495763" TEXT="Interpreter"/>
<node CREATED="1385638496061" ID="ID_1907183096" MODIFIED="1385638502339" TEXT="Iterator"/>
<node CREATED="1385638502692" ID="ID_457610204" MODIFIED="1385638505699" TEXT="Mediator"/>
<node CREATED="1385638514203" ID="ID_863816019" MODIFIED="1385638516554" TEXT="Observer"/>
<node CREATED="1385638517227" ID="ID_1643657687" MODIFIED="1385638526506" TEXT="Template Method"/>
<node CREATED="1385638526827" ID="ID_1497322268" MODIFIED="1385638529762" TEXT="Visitor"/>
</node>
</node>
</map>
