<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1369933925367" ID="ID_1080968871" MODIFIED="1378201852851" TEXT="Dise&#xf1;o estructurado (Constantine, Yourdon)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370123448467" ID="ID_1310099364" MODIFIED="1384617193342" POSITION="right" TEXT="Objetivos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370123457821" ID="ID_288270136" MODIFIED="1379592691662" TEXT="Maximizar la inteligibilidad del sistema">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370123471560" ID="ID_539025819" MODIFIED="1379592694719" TEXT="Minimizar el coste de mantenimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370123486917" ID="ID_194435716" MODIFIED="1379592697933" TEXT="Facilitar la prueba">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370123496369" ID="ID_1638716414" MODIFIED="1379592698791" TEXT="Integraci&#xf3;n del sistema">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370123506394" ID="ID_186689567" MODIFIED="1384617195006" POSITION="right" TEXT="Principios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370123511117" ID="ID_457308849" MODIFIED="1379592702863" TEXT="Modularizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370123517246" ID="ID_1637469974" MODIFIED="1379592703736" TEXT="Independencia modular">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370123528997" ID="ID_1006124742" MODIFIED="1379592704438" TEXT="Modelizaci&#xf3;n conceptual">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370123536773" ID="ID_34187460" MODIFIED="1379592705078" TEXT="Principio de &quot;caja negra&quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1381180861777" ID="ID_695861107" MODIFIED="1384617196991" POSITION="right" TEXT="Diagramas de estructura">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1381180887914" ID="ID_1272611952" MODIFIED="1382182222682" TEXT="Jerarqu&#xed;a de m&#xf3;dulos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1381180907125" ID="ID_1737964671" MODIFIED="1381180981157" TEXT="M&#xf3;dulos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1381180951936" ID="ID_411740420" MODIFIED="1381180956977" TEXT="Caja negra"/>
</node>
<node CREATED="1381180913014" ID="ID_1014647595" MODIFIED="1381181105320" TEXT="Invocaciones (relaciones entre m&#xf3;dulos)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1381180928638" ID="ID_793401512" MODIFIED="1381180983482" TEXT="Cuplas (comunicaciones entre m&#xf3;dulos)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1381181005827" ID="ID_1744964378" MODIFIED="1381181164125" TEXT="Cupla de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1381181012231" ID="ID_484475053" MODIFIED="1381181161458" TEXT="Cupla modificada">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se aporta un argumento que deber&#225; modificar el valor de un m&#243;dulo
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1381181088439" ID="ID_716936121" MODIFIED="1381181162066" TEXT="Cupla de resultados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1381181134000" ID="ID_1776227452" MODIFIED="1384617197673" POSITION="right" TEXT="Criterios de validaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1369933938676" ID="ID_280673520" MODIFIED="1385565729351" STYLE="bubble" TEXT="Acoplamiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#00ff33" CREATED="1369933994234" ID="ID_1096779213" MODIFIED="1378397617397" TEXT="Normal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#00ff33" CREATED="1369933997245" ID="ID_554120565" MODIFIED="1378397619380" TEXT="Acoplamiento de datos. Param&#xe9;tros.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#00ff33" CREATED="1369934004342" ID="ID_690636315" MODIFIED="1378397620117" TEXT="Acoplamiento por estampado. Estructura.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ff9933" CREATED="1369934015619" ID="ID_1660676856" MODIFIED="1378397620674" TEXT="Acoplamiento de control. Se pasan datos de control">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ff0033" CREATED="1369934059525" ID="ID_704373101" MODIFIED="1378397621434" TEXT="Acoplamiento externo. Dos m&#xf3;dulos hacen referencia a una variable global">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ff0033" CREATED="1369934067355" ID="ID_928643255" MODIFIED="1378397622111" TEXT="Acoplamiento com&#xfa;n. Una misma &#xe1;rea de datos global">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ff0033" CREATED="1369934076486" ID="ID_922823620" MODIFIED="1378397622811" TEXT="Acoplamiento de contenido. Un m&#xf3;dulo accede al contenido de otro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1369933943535" ID="ID_887930172" MODIFIED="1384617448788" STYLE="bubble" TEXT="Cohesi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369934218000" ID="ID_1979443811" MODIFIED="1378397600311" TEXT="Funcional. Una &#xfa;nica funci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369934223175" ID="ID_1446038271" MODIFIED="1378397601268" TEXT="Secuencial. Varias funciones se pasan un dato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369934230637" ID="ID_1136240479" MODIFIED="1378397601807" TEXT="Comunicacional. Mismos datos de entrada y salida">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369934243739" ID="ID_1998641766" MODIFIED="1378397603023" TEXT="Procedimental o procedural. Tareas en un orden secuencial">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369934262991" ID="ID_1509780404" MODIFIED="1378397603801" TEXT="Temporal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369934277808" ID="ID_1977638319" MODIFIED="1378397604517" TEXT="L&#xf3;gica. Actividades de la misma categor&#xed;a ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369934280802" ID="ID_1874156174" MODIFIED="1378397605256" TEXT="Coincidental o casual. Actividades sin relaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1369934596548" ID="ID_1684793754" MODIFIED="1384617198486" POSITION="right" TEXT="Descomposici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369934611294" ID="ID_527413774" MODIFIED="1378397606301" TEXT="Fan-in: medida de reusabilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369934614950" ID="ID_1911542048" MODIFIED="1378397607140" TEXT="Fan-out: medida de complejidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</map>
