<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1434734330744" ID="ID_1397204057" MODIFIED="1517488851849">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      T31 DIR. DE PROY.
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1434735695080" ID="ID_1495778513" MODIFIED="1434735703002" POSITION="right" TEXT="Planif de un proy">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434811071026" ID="ID_1874436582" MODIFIED="1436442789755">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Selecci&#243;n de estrategia para producir un producto, def de <u>activs</u>&#160;y asignaci&#243;n de <u>recursos</u>&#160;a las activs
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1434811219439" ID="ID_110913790" MODIFIED="1436513923677">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Distribuye esfuerzo estimado <u>previamente</u>&#160;y evoluciona con el tiempo
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1434735919410" ID="ID_1458363588" MODIFIED="1436436837515" TEXT="Pasos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434811301911" ID="ID_314488798" MODIFIED="1434811374883">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Id tareas y deps&#8594;&#160;Estimar esfuerzo por tareas&#8594; Asignar recursos&#8594;&#160;&#160;Red de Tareas&#8594; Agenda
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1436436837499" ID="ID_1000315960" MODIFIED="1436436840907" TEXT="Herrams">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434811741147" ID="ID_1864481175" MODIFIED="1434820203980">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mapa activs (MCV y Metodolog&#237;a)&#8594;&#160;T&#233;cns de estimaci&#243;n&#8594;Planif recursos&#8594;T&#233;cns de planif&#8594;Planif temp
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1434735704998" ID="ID_25451080" MODIFIED="1434735710545" TEXT="Planif temp de desarrollo">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434811578795" ID="ID_354698312" MODIFIED="1434811608579" TEXT="Previsi&#xf3;n de realizaci&#xf3;n de activs en fechas">
<node CREATED="1434811608580" ID="ID_719930085" MODIFIED="1436436988447">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Perspectivas: </i>fin ya establecido, o l&#237;mites aprox (Org Sw fija fin y distribuye esf&#8594;mejor uso de recurss)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1434735712303" ID="ID_1852196090" MODIFIED="1434820318299" POSITION="right" TEXT="T&#xe9;cnicas de planif">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434735717049" ID="ID_1482317760" MODIFIED="1434735768535">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      M&#233;todo PERT
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434811888554" ID="ID_1274931852" MODIFIED="1436442436739">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (T&#233;cns de Eval y Revisi&#243;n de Pgms) estima <u>duraci&#243;n</u>&#160;de activs por <u>probab</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1434812166111" ID="ID_158771323" MODIFIED="1436442894225">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Planif, ejec y controla proys con coord de gran n&#186; de activs (establece&#160;<u>deps</u>)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1434811908502" ID="ID_1227797201" MODIFIED="1434812477925">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>CPM: </i>(M&#233;todo Camino Cr&#237;tico) escoge duraci&#243;n adecuada para q <u>coste sea m&#237;n</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1434812009082" ID="ID_282211773" MODIFIED="1434812094716">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Otras: </i>MCE (PERT Coste, como CPM), ROY (no usa activs ficticias) y GERT (Gr&#225;fica)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1434735728743" ID="ID_1937916988" MODIFIED="1434735732301" TEXT="Conceptos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434813416000" ID="ID_433710218" MODIFIED="1434813436937">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Activ:&#160;</i>con duraci&#243;n y recursos (flecha)
    </p>
  </body>
</html></richcontent>
<node CREATED="1434813589205" ID="ID_791748132" MODIFIED="1434820396476">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Activs ficticias:</i>&#160;&#160;sin duraci&#243;n ni recs, indican&#160; <u>interrelacs o t de espera</u>&#160;para cumplir reglas de sucesos
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1434813437645" ID="ID_1025829141" MODIFIED="1434820410967">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Suceso:</i>&#160;inicio/fin de activ, no consume recs
    </p>
  </body>
</html></richcontent>
<node CREATED="1434813456815" ID="ID_391712687" MODIFIED="1436437565506">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Unicidad (inicio y fin), design sucesiva (numerar anteriores) y&#160;&#160;&#160;<u>un&#237;voca</u>&#160;(no dos flechas con =sucesos i/f)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1434813739303" ID="ID_1587040068" MODIFIED="1434813766268">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Prelaciones: </i>activs necesarias antes de iniciar otra
    </p>
  </body>
</html></richcontent>
<node CREATED="1434813766968" ID="ID_203291213" MODIFIED="1434820444471">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lineales (A&lt;B-precede), converg (fin varias para iniciar una) o diverg (fin una para iniciar varias)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1434813822405" ID="ID_598769173" MODIFIED="1434814655544">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Early:</i>&#160;t m&#237;n para llegar a suceso, <u>lo +pronto</u>&#160;q puede comenzar una activ, camino +largo <u>dsd&#160;suc incial</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1434813922515" ID="ID_1529808140" MODIFIED="1434820473270" TEXT="Early del suc final = Duraci&#xf3;n proy">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1434813976406" ID="ID_835536686" MODIFIED="1434814666817">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Last:&#160;</i>t m&#225;x q se puede retrasar una activ, <u>lo +tarde</u>&#160;q puede comenzar, camino +largo <u>hasta&#160;suc final</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1434814062271" ID="ID_875863766" MODIFIED="1434814085730">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si activs convergen&#8594; t m&#237;n (Last) o m&#225;x (Early)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1434814516351" ID="ID_1239732896" MODIFIED="1434820498218">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Oscilaci&#243;n de suceso:</i>&#160;=Last-Early
    </p>
  </body>
</html></richcontent>
<node CREATED="1434814551209" ID="ID_1939344769" MODIFIED="1434814598905">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Camino cr&#237;tico:</i>&#160;de mayor duraci&#243;n formado por activs cr&#237;ticas (retrasan el final, holgura=0), osc=0
    </p>
  </body>
</html></richcontent>
<node CREATED="1434814617206" ID="ID_654970156" MODIFIED="1434814628198" TEXT="1 &#xf3; +">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1434814675918" ID="ID_1833912923" MODIFIED="1436443040918">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Holgura total:</i>&#160;&#160;t q se puede retrasar activ sin perjudicar<u>proy</u>; =L(fin)-E(ini)-Dur
    </p>
  </body>
</html></richcontent>
<node CREATED="1434814768706" ID="ID_1369172665" MODIFIED="1436443035841">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>H libre:&#160;</i>HT consumible por activ sin perjudicar <u>sigs</u>; =E(fin)-E(ini)-Dur
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1434814829227" ID="ID_322802357" MODIFIED="1434820569074">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>H indep: </i>si no se gasta en la propia activ, se pierde; =E(fin)-L(ini)-Dur
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1434735734185" ID="ID_1938968987" MODIFIED="1434735737075" TEXT="Confecci&#xf3;n">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434814887561" ID="ID_921735069" MODIFIED="1434815117331">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>1. Activs y prels:</i>&#160;Matriz Encadenam o Cuadro Prels
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1434815067507" ID="ID_1654151319" MODIFIED="1436443743009">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2. Grafo y num sucesos</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1436443743011" ID="ID_716743663" MODIFIED="1436443831935">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Red Tareas&#8594;m&#233;todos: Diagr de Flechs (ADM);o Precedencia (PDM) si ROY&#8594;nodos (activs) son rect&#225;ngs
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1434815123852" ID="ID_1687855452" MODIFIED="1436438846954">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>3. Asign tiempos a activs:</i>&#160;distrib <u>prob beta</u>&#8594; D=(t_opt+t_pesim+4&#183;t_prob)/6
    </p>
  </body>
</html></richcontent>
<node CREATED="1436438784418" ID="ID_915682700" MODIFIED="1436445930305">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Varianza de activ:</i>&#160;V=[(t_pesim-t_opt)/6]^2
    </p>
  </body>
</html></richcontent>
<node CREATED="1436438876207" ID="ID_63651475" MODIFIED="1436438906944">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Desv t&#237;pica: </i>&#963; = V^1/2
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1436444144446" ID="ID_1934607343" MODIFIED="1436444335073">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Desv cr&#237;tica de un proy: </i><u>suma&#160;de varianzas</u>&#160;de las activs cr&#237;ticas&#8594; ra&#237;z
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1434815189359" ID="ID_1058385463" MODIFIED="1436444202534">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      4. C&#225;lculo de Early y Last, holguras y camino cr&#237;tico&#160;
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1434815233470" ID="ID_1679058237" MODIFIED="1436444208480">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>5. C&#225;lculo de prob de retr/adel el proy:</i>&#160;seg&#250;n t medio de realiz y <u>V</u>&#160;de activs cr&#237;ticas
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1434815280067" ID="ID_194971099" MODIFIED="1436514398810">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>6. Calendario:</i>&#160;diagr Gantt&#8594; se se&#241;alan <u>hitos</u>&#160;de seguim y control
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1434735740145" ID="ID_1134743484" MODIFIED="1434815561924" TEXT="Duraci&#xf3;n-coste de una activ">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434815561912" ID="ID_1516916968" MODIFIED="1434816266848" TEXT="Compromiso">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434815297320" ID="ID_166189282" MODIFIED="1434815536377">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Coste total = Directos (de la activ, inverso a su duraci&#243;n)+De estructura (generales, prop a dur proy)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1434815566647" ID="ID_234263141" MODIFIED="1434816299152">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Duraci&#243;n &#243;ptima del proy&#8594; coste m&#237;n del proy
    </p>
  </body>
</html></richcontent>
<node CREATED="1434816162662" ID="ID_23918824" MODIFIED="1434816212547">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Reducir dur proy:</i>&#160;implica reducir dur activs cr&#237;ticas y elegir las de -coef coste
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1434815910046" ID="ID_1385263943" MODIFIED="1434816125266">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Coef de coste de una activ:</i>&#160;&#8710;c = (Cdir&#8211;Cdur_m&#237;n)/Dur_min&#8211;Dur_normal
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1434735720153" ID="ID_1399153940" MODIFIED="1434735775659" TEXT="Diagrama GANTT">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434816353655" ID="ID_939308059" MODIFIED="1436441240221">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cronograma, representa plan de trabajo&#8594; tareas, inicio/fin y c&#243;mo se&#160; <u>encadenan</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1434816374926" ID="ID_703010763" MODIFIED="1434816388113" TEXT="Realiza planif y programaci&#xf3;n al mismo tiempo">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1434735787244" ID="ID_1335433215" MODIFIED="1434735797552" TEXT="EDT o WBS">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434816391617" ID="ID_139032971" MODIFIED="1436438667090">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Estruct de descomp de trabajos) descompon <u>activs</u>&#160;seg&#250;n proy y les asigna&#160; <u>cuentas</u>&#160;utilizadas
    </p>
  </body>
</html></richcontent>
<node CREATED="1434820713720" ID="ID_936123329" MODIFIED="1434820721220">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sirve de lista de <u>comprob</u>&#160;y herram de <u>contab anal&#237;tica</u>&#160;del proy
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1434735799534" ID="ID_1801715515" MODIFIED="1436513736817" POSITION="right" TEXT="Recursos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436513736753" ID="ID_739236375" MODIFIED="1436513741105" TEXT="Inventario">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434816498727" ID="ID_1204301331" MODIFIED="1434817415365">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Caracter&#237;sticas de recursos:</i>&#160;descrip, disponib, fecha en q se requiere y duraci&#243;n
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1434735811013" ID="ID_23634350" MODIFIED="1436513841202" TEXT="Humanos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434735960613" ID="ID_1411676599" MODIFIED="1434735964376" TEXT="Considerar">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434816540494" ID="ID_690606108" MODIFIED="1434816587398" TEXT="Fechas de incorp, patr&#xf3;n l&#xed;mites, histograma, capacidad personas y reqs de tareas, no infraut recs">
<node CREATED="1434816588089" ID="ID_174522695" MODIFIED="1434816601580" TEXT="Intentar asignar 1 tarea por persona">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1436513841156" ID="ID_1221424087" MODIFIED="1436513945676">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      T&#233;cnicas de asign
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434735964846" ID="ID_1553417943" MODIFIED="1434735969178" TEXT="Patr&#xf3;n de l&#xed;mites">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434816604415" ID="ID_1129146222" MODIFIED="1434816617677" TEXT="Marca l&#xed;mites de un proy para establecer l&#xed;mites de recursos aproximados">
<node CREATED="1436443167885" ID="ID_1345738944" MODIFIED="1436443176298" TEXT="Ayuda en la asign de RRHH"/>
</node>
<node CREATED="1434816621965" ID="ID_1408050741" MODIFIED="1434816624718" TEXT="Obtenci&#xf3;n">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434816624997" ID="ID_616266496" MODIFIED="1436514071306">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Descontar</u>&#160;de la estimaci&#243;n el esf (E) y dur (T) de la fase de <u>Dise&#241;o</u>&#160;Func (activs diferentes al resto)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1434817109086" ID="ID_187557296" MODIFIED="1436514016098">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Fases del proy:</i>&#160;&#160;Arranque (se van incorp pers)&#8594;Pleno rendim (estable)&#8594;Fin (van reduciendo pers)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1434817176997" ID="ID_1680111311" MODIFIED="1434817192346" TEXT="Distribuir RRHH">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434817192674" ID="ID_1188385086" MODIFIED="1436514109211">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Jefe Proy siempre presente, T/3 (fases),&#160;&#160;E/2 para la fase de pleno rendim
    </p>
  </body>
</html></richcontent>
<node CREATED="1436514032349" ID="ID_528565703" MODIFIED="1436514135757" TEXT="RRHH=E/T+Jefe Proy"/>
</node>
</node>
</node>
</node>
<node CREATED="1434735970953" ID="ID_823186330" MODIFIED="1434735975300" TEXT="Histograma de recursos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434817304825" ID="ID_1954954737" MODIFIED="1436514173160">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Refleja la <u>incorp</u>&#160;de RRHH al proy a partir de de Gantt y el m&#225;x del patr&#243;n, y verifica <u>coherencia</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1434735813309" ID="ID_1108230583" MODIFIED="1434735815566" TEXT="T&#xe9;cnicos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434735980851" ID="ID_907745441" MODIFIED="1434735982068" TEXT="HW">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434817495986" ID="ID_147784624" MODIFIED="1434820819378">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      S&#170; de desarrollo (anfitri&#243;n), m&#225;q objetivo (ord dnd se implantar&#225; sw) y otros elementos hw del nuevo s&#170;
    </p>
  </body>
</html></richcontent>
<node CREATED="1434817547207" ID="ID_1931768603" MODIFIED="1434817556253" TEXT="Normalmente anfitri&#xf3;n=m&#xe1;q objetivo"/>
</node>
</node>
<node CREATED="1434735985702" ID="ID_1540989813" MODIFIED="1434735986903" TEXT="SW">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434818494357" ID="ID_1458511691" MODIFIED="1434818537783">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Herrams orientadas a c&#243;digo (compiladores, editores), de metodolog&#237;a y CASE (integra anteriores)
    </p>
  </body>
</html></richcontent>
<node CREATED="1434818550528" ID="ID_529006361" MODIFIED="1434818617329">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Reut de sw</u>&#160;es tb recurso sw (bloques catalogados, estand y validados)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1434735820127" ID="ID_1812319549" MODIFIED="1434735832958" POSITION="right" TEXT="Seguimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434818627803" ID="ID_1619932824" MODIFIED="1436514206826">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Recolecci&#243;n de datos para an&#225;lisis y estudio de la evol del proy (compara <u>real vs planif</u>)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1434818680718" ID="ID_938424904" MODIFIED="1436514253343">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reuniones, revisiones, comprob <u>hitos</u>&#160;en fechas, comparar comienzo real de tarea con planif
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1434735991735" ID="ID_1179394162" MODIFIED="1434735996098" TEXT="Control">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434818745261" ID="ID_1461279113" MODIFIED="1436514583958">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Para adm recursos, sol problemas y dirigir personal
    </p>
  </body>
</html></richcontent>
<node CREATED="1434818759850" ID="ID_1856226535" MODIFIED="1436514505454">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Def medida&#8594;Registrar avance&#8594;<u>Desviac</u>&#160;(holgura restante o demora)&#8594;Acciones (reasign recs y plazos)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1436514583943" ID="ID_808070614" MODIFIED="1436514629760">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Acciones ante desvs
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<node CREATED="1436514442117" ID="ID_381269781" MODIFIED="1436514730720">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      - <u>Acortar</u>&#160;dur de activs <u>cr&#237;ticas</u>&#8594;+coste (jornadas de +horas sin tocar RRHH) y:
    </p>
    <p>
      - <u>Alargar</u>&#160;dur de activs no cr&#237;ticas para compensar&#8594; aprovechar <u>holgura</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1434735843780" ID="ID_1668858568" MODIFIED="1434735849937" TEXT="Diagr de extrapolaci&#xf3;n">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434818841063" ID="ID_1980815225" MODIFIED="1436439983004">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cronolog&#237;a de estimaciones de consumo de recursos, sirve&#160;para obtener&#160; <u>previsiones de desviacs</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1434818970150" ID="ID_940656929" MODIFIED="1434819039927">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Nueva fecha</u>&#160;de fin de proy extrapolando tendencia de desviac
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
</node>
</node>
<node CREATED="1434735838586" ID="ID_1890239107" MODIFIED="1434820882726" POSITION="right" TEXT="Plan de proyecto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434819054133" ID="ID_401596784" MODIFIED="1434819102044">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Gu&#237;a de gesti&#243;n</u>&#160;(docu breve) con info de costes y agenda, se usa a lo largo del CV del sw
    </p>
  </body>
</html></richcontent>
<node CREATED="1434819087946" ID="ID_969767015" MODIFIED="1434819094063" TEXT="Se produce al final de la etapa de Planif"/>
</node>
<node CREATED="1434820882715" ID="ID_683904959" MODIFIED="1436440486570" TEXT="Contenido">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<node CREATED="1434819146242" ID="ID_172671151" MODIFIED="1436440481351">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Alcance y objs, estimacs, gesti&#243;n&#160; <u>riesgos</u>, agenda y costes, recursos, org pers y mecs seg y control
    </p>
  </body>
</html></richcontent>
<node CREATED="1436440527779" ID="ID_574037943" MODIFIED="1436440556377">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>An&#225;lisis Riesgos:</i>&#160;id, estim (prob y consecs), eval y gesti&#243;n
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1434811000306" ID="ID_582366545" MODIFIED="1434811002740" POSITION="right" TEXT="Herramientas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434811437384" ID="ID_1527138129" MODIFIED="1434820909956">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Crean autom&#225;ticamente&#160;planes detallados, usadas por expertos (Jefe Proy)
    </p>
  </body>
</html></richcontent>
<node CREATED="1434811451312" ID="ID_1013644353" MODIFIED="1434820920924">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>CA-PLANMACS:&#160; </i>&#225;rbol jer&#225;rquico (proy, fases, tareas y activs) y factores de distrib de esfuerzo
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1434811513464" ID="ID_1531349650" MODIFIED="1434811548474">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>SuperProk, Micr Proj y Proj Manager Workbench:</i>&#160;planes manuales, seguim semiautom
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</map>
