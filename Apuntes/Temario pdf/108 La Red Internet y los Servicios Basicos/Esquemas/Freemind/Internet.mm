<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1376905636581" ID="ID_1390380193" MODIFIED="1376905655011" TEXT="Internet">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366791766574" ID="ID_714673991" MODIFIED="1391107521658" POSITION="right" TEXT="Organizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366791773165" ID="ID_1635144286" MODIFIED="1383398853276" TEXT="ICANN">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369379442046" ID="ID_603811003" MODIFIED="1384779461840" TEXT="Organizaciones de soporte">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366791795156" ID="ID_1332678054" MODIFIED="1378566872631" TEXT="GNSO (nombres de primer nivel gen&#xe9;ricos (gTLDs)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366791823258" ID="ID_458034310" MODIFIED="1378566875428" TEXT="ccNSO (nombres de dominio de primer nivel de c&#xf3;digos de pa&#xed;ses)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1366791884277" ID="ID_18026238" MODIFIED="1378566917498" TEXT="ASO">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369379448793" ID="ID_34785127" MODIFIED="1378566877680" TEXT="Crea nuevas Direcciones IP">
<arrowlink DESTINATION="ID_1782141452" ENDARROW="Default" ENDINCLINATION="371;0;" ID="Arrow_ID_1184471260" STARTARROW="None" STARTINCLINATION="371;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375464602454" ID="ID_1782141452" MODIFIED="1384863889339" TEXT="IANA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376907346687" ID="ID_1239728814" MODIFIED="1378566870054" TEXT="Nombres de dominio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376907160837" ID="ID_697761786" MODIFIED="1378566880836" TEXT="Gestiona la zona &quot;DNS Root &quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376907255207" ID="ID_1406729257" MODIFIED="1378566915422" TEXT="Gestiona las zonas de dominio .int y .arpa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376907361144" ID="ID_733606905" MODIFIED="1376907379736" TEXT="Base de datos de dominio del nivel m&#xe1;s alto"/>
<node CREATED="1376907385289" ID="ID_314156781" MODIFIED="1376907398649" TEXT="Repositorios de pr&#xe1;cticas de IDN"/>
</node>
<node CREATED="1376907220177" ID="ID_365476105" MODIFIED="1378566888532" TEXT="Asigna puertos y n&#xfa;meros de protocolo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376907493310" ID="ID_1315936123" MODIFIED="1378566892303" TEXT="Distribuye direcciones IP libres para los RIR">
<arrowlink DESTINATION="ID_140429642" ENDARROW="Default" ENDINCLINATION="206;0;" ID="Arrow_ID_726601744" STARTARROW="None" STARTINCLINATION="206;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1376907542656" ID="ID_140429642" MODIFIED="1384863908438" TEXT="RIR (Regional Internet Registers)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376907564253" ID="ID_67791670" MODIFIED="1376907600463" TEXT="AfriNIC (Africa)"/>
<node CREATED="1376907568296" ID="ID_1497699983" MODIFIED="1376907610291" TEXT="APNIC (Asia/Pacifico)"/>
<node CREATED="1376907577609" ID="ID_303897507" MODIFIED="1376907629795" TEXT="ARIN (Am&#xe9;rica del Norte)"/>
<node CREATED="1376907579754" ID="ID_1158243771" MODIFIED="1376907642876" TEXT="LACNIC (Latinoam&#xe9;rica)"/>
<node CREATED="1376907584577" ID="ID_738265262" MODIFIED="1376907664916" TEXT="RIPE NCC (Europa, Oriente Medio y Asia Central)"/>
<node CREATED="1376907732312" ID="ID_239355642" MODIFIED="1378566961621" TEXT="Asignan direcciones IP ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376907746301" ID="ID_196067223" MODIFIED="1376907748160" TEXT="ISP"/>
<node CREATED="1376907748536" ID="ID_1632677496" MODIFIED="1376907768624" TEXT="NIR (National Internet Registers)"/>
</node>
</node>
<node CREATED="1376908185387" ID="ID_1284065169" MODIFIED="1383223041759" TEXT="Est&#xe1;ndares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366791959398" ID="ID_515826308" MODIFIED="1377505338470" TEXT="W3C">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376989561774" ID="ID_394087580" MODIFIED="1377336736493" TEXT=" HTML, XML, XHTML SOAP, CSS, WAI, DOM, P3P">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1366792018579" ID="ID_1844095027" MODIFIED="1376989580894" TEXT="ISOC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366792025872" ID="ID_662740076" MODIFIED="1381402712542" TEXT="IETF">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1366792036283" ID="ID_467458780" MODIFIED="1366792040684" TEXT="RFC">
<node CREATED="1371661504892" ID="ID_614214693" MODIFIED="1371661525453" TEXT="STD (Internet Standard)">
<icon BUILTIN="forward"/>
</node>
</node>
</node>
<node CREATED="1375464409154" ID="ID_1025600285" MODIFIED="1381402256545" TEXT="IESG (Internet Engineering Steering Group)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375464450213" ID="ID_82331582" MODIFIED="1375464465728" TEXT="Responsable t&#xe9;cnico final de est&#xe1;ndares"/>
<node CREATED="1381402653648" ID="ID_1338791597" MODIFIED="1381402681505" TEXT="Responsable de la gesti&#xf3;n del IETF"/>
</node>
<node CREATED="1366792045791" ID="ID_1962068279" MODIFIED="1381402718363" TEXT="IAB">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368378155299" ID="ID_1720019471" MODIFIED="1368378168257" TEXT="Infraestructura tecnol&#xf3;gica"/>
</node>
</node>
</node>
</node>
<node CREATED="1375261219649" ID="ID_556761185" MODIFIED="1391637428044" POSITION="right" TEXT="Arquitectura">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375261222576" ID="ID_26090002" MODIFIED="1377170163259" TEXT="Red troncal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375261227168" ID="ID_1709347916" MODIFIED="1377170165255" TEXT="Proveedores de acceso internacionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375261239905" ID="ID_1188950470" MODIFIED="1377170167315" TEXT="Proveedores de acceso nacionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375261249226" ID="ID_1812217332" MODIFIED="1377170169187" TEXT="Redes corporativas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1381567767028" ID="ID_1850161917" MODIFIED="1382602325199" TEXT="IXP (Internet Exchange Points)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Puntos de intercambio entre dos ISPs
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1381568019779" ID="ID_1111785942" MODIFIED="1381568028747" TEXT="Hace uso de enrutamiento BGP"/>
</node>
</node>
<node CREATED="1376907061346" FOLDED="true" ID="ID_1504459521" MODIFIED="1391105930965" POSITION="right" TEXT="Protocolos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377067076822" ID="ID_696951558" MODIFIED="1384778530290" TEXT="Gopher">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377067103363" ID="ID_1899280848" MODIFIED="1377505509045" TEXT="B&#xfa;squeda e intercambio de ficheros en la Web">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377067121527" ID="ID_34604375" MODIFIED="1377505505924" TEXT="Predecesor del HTTP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377067178950" ID="ID_1769276176" MODIFIED="1377067215888" TEXT="Sigue en uso, no en IE ni Chrome, pero tiene un add-on  en Mozilla"/>
</node>
<node CREATED="1369738613498" ID="ID_252589361" MODIFIED="1391105891382" TEXT="HTTP (HyperText Transfer Protocol)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369738634102" ID="ID_1402128203" MODIFIED="1377505434881" TEXT="Protocolo de petici&#xf3;n y respuesta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378632350146" ID="ID_96451529" MODIFIED="1378632367766" TEXT="Utiliza formato MIME en los mensajes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377505253942" ID="ID_259258927" MODIFIED="1381403657425" TEXT="Desarrollado por la W3C e IETF   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369738646306" ID="ID_1622404898" MODIFIED="1382604274531" TEXT="Protocolo sin estado">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No guarda informaci&#243;n acerca de conexiones anteriores (por eso se usan las cookies).
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369738657994" ID="ID_475166392" MODIFIED="1377505100699" TEXT="RFC 2616">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369738578026" ID="ID_764049811" MODIFIED="1377505566013" TEXT="URL (Uniform Resource Locator)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369738586270" ID="ID_488657116" MODIFIED="1377505278891" TEXT="RFC 1738">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377505648976" ID="ID_357049791" MODIFIED="1377505677018" TEXT="Es un tipo de URI (Uniform Resource Identifier)"/>
<node CREATED="1377505717227" ID="ID_963276629" MODIFIED="1377505802641" TEXT="Sintaxis">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377505721385" ID="ID_1182601835" MODIFIED="1377505807538" TEXT="Protocolo (http, ftp, tc.)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377505736812" ID="ID_1737802053" MODIFIED="1377505810450" TEXT="host">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377505777638" ID="ID_503508569" MODIFIED="1377505782605" TEXT="Nombre de dominio"/>
<node CREATED="1377505782965" ID="ID_1969329063" MODIFIED="1377505786950" TEXT="Direcci&#xf3;n IP"/>
</node>
<node CREATED="1377505750764" ID="ID_197116291" MODIFIED="1377505813281" TEXT=":N&#xfa;mero de puerto (opcional)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377505789694" ID="ID_1567227008" MODIFIED="1377505815969" TEXT="Path del recurso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1376905662349" ID="ID_880914536" MODIFIED="1384778649712" TEXT="FTP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376906008443" FOLDED="true" ID="ID_1920490014" MODIFIED="1384863990469" TEXT="Tipos de transferencia">
<node CREATED="1376906015799" ID="ID_1493378829" MODIFIED="1376906017946" TEXT="ASCII"/>
<node CREATED="1376906018765" ID="ID_1557664803" MODIFIED="1376906024946" TEXT="Binarios"/>
</node>
<node CREATED="1376906116144" ID="ID_886868821" MODIFIED="1383399236694" TEXT="Modos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376905671046" FOLDED="true" ID="ID_1602786243" MODIFIED="1384864020070" TEXT="Activo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376905696054" ID="ID_472888536" MODIFIED="1377505054153" TEXT="Puerto 20 en el servidor para los datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376906088896" ID="ID_783790113" MODIFIED="1377505055801" TEXT="Puerto 21 en el servidor para el control">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376905705639" ID="ID_1753549466" MODIFIED="1376905719661" TEXT="Puerto aleatorio mayor de 1024 en el cliente">
<node CREATED="1378195698186" ID="ID_146444397" MODIFIED="1378195707697" TEXT="LO ABRE EL SERVIDOR">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1376905723072" ID="ID_476156321" MODIFIED="1382605172356" TEXT="Comandos tipo Port">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376905753513" ID="ID_158468653" MODIFIED="1378195581883" TEXT="Problema de seguridad">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-indent: 0px; font-variant: normal; background-color: rgb(255, 255, 255); margin-left: 0px; font-weight: normal; margin-top: 0px; text-align: start; word-spacing: 0px; color: rgb(0, 0, 0); white-space: normal; font-family: Calibri, Helvetica, Arial, sans-serif; margin-right: 0px; margin-bottom: 20px; letter-spacing: normal; text-transform: none; font-style: normal; line-height: 19.1875px; font-size: 16px">
      <font face="SansSerif" size="3">Using normal or passive FTP, a client begins a session by sending a request to communicate through&#160;</font><u><font size="3" face="SansSerif" color="rgb(102, 51, 102)"><a style="text-decoration: underline; color: rgb(102, 51, 102)" href="http://searchnetworking.techtarget.com/definition/TCP">TCP</a></font></u><font size="3" face="SansSerif">&#160;port 21, the port that is conventionally assigned for this use at the FTP server. This communication is known as the Control Channel connection. </font>
    </p>
    <p style="text-indent: 0px; font-variant: normal; background-color: rgb(255, 255, 255); margin-left: 0px; font-weight: normal; margin-top: 0px; text-align: start; word-spacing: 0px; color: rgb(0, 0, 0); white-space: normal; font-family: Calibri, Helvetica, Arial, sans-serif; margin-right: 0px; margin-bottom: 20px; letter-spacing: normal; text-transform: none; font-style: normal; line-height: 19.1875px; font-size: 16px">
      <font size="3" face="SansSerif">Using &quot;normal&quot; FTP communication, the client requestor also includes in the same PORT command packet on the Control Channel <b>a second port number </b>that is to be used when data is to be exchanged; the port-to-port exchange for data is known as the Data Channel. <b>The FTP server then initiates the exchange from its own port 20 to whatever port was designated by the client. </b>However,<b>&#160;because the server-initiated communication is no longer controlled by the client and can't be correlated by a firewall to the initial request, the potential exists for uninvited data to arrive from anywhere posing as a normal FTP transfe</b>r.</font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1376905732712" FOLDED="true" ID="ID_518752473" MODIFIED="1384864021470" TEXT="Pasivo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376905746496" ID="ID_1493513041" MODIFIED="1376905856171" TEXT="Creado para solucionar el problema de seguridad"/>
<node CREATED="1376905870910" ID="ID_1523466054" MODIFIED="1377505067042" TEXT="El cliente env&#xed;a un comando tipo PASV">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376905897583" ID="ID_1502330233" MODIFIED="1378195661998" TEXT="El servidor le indica un puerto mayor que 1023 para conectarse">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="3" face="SansSerif" color="rgb(0, 0, 0)">Using passive FTP, a PASV command is sent instead of a PORT command. Instead of specifying a port that the server can send to, the PASV command <b>asks&#160;the server to specify a port it wishes to use for the Data Channel connection</b>. The server replies on the Control Channel with the port number which<b>&#160;the client then uses to initiate an exchange on the Data Channel</b>. The server will thus always be responding to client-initiated requests on the Data Channel and the firewall can coorelate these.</font>
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376905946465" ID="ID_1421548407" MODIFIED="1378195718073" TEXT="El cliente inicia una conexi&#xf3;n en el puerto indicado por el servidor">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378195719260" ID="ID_1413842674" MODIFIED="1378195736394" TEXT="LO ABRE EL CLIENTE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1376906143586" ID="ID_646412982" MODIFIED="1381321901570" TEXT="El servidor usa el puerto 21 para el control">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376906162979" ID="ID_625593437" MODIFIED="1377505079650" TEXT="El puerto 20 de datos NO se usa en el modo pasivo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1377067435318" FOLDED="true" ID="ID_545262052" MODIFIED="1384864023086" TEXT="Archie">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377067443934" ID="ID_217086252" MODIFIED="1377067457254" TEXT="Herramienta para indexar ficheros FTP"/>
</node>
</node>
<node CREATED="1376908589661" ID="ID_1880163926" MODIFIED="1377066999968" TEXT="DNS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377066763847" ID="ID_215608260" MODIFIED="1384864030281" TEXT="WebDAV (Web Distributing Authoring and Versioning)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377066851659" ID="ID_1916174025" MODIFIED="1377066909974" TEXT="Extensi&#xf3;n de HTTP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377066867074" ID="ID_1192577577" MODIFIED="1382002878842" TEXT="Edici&#xf3;n distribuida de documentos de diferentes servidores Web">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377066973476" ID="ID_1075883907" MODIFIED="1377066992884" TEXT="Permite el mantenimiento de las propiedades de los documentos"/>
<node CREATED="1377066923294" ID="ID_400032800" MODIFIED="1382002883985" TEXT="RFC 4918">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1369738868706" FOLDED="true" ID="ID_1218633785" MODIFIED="1391637426330" POSITION="right" TEXT="Web 2.0">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369738889304" ID="ID_1417889580" MODIFIED="1383399632495" TEXT="La web es la plataforma, en ella se desarrolla y se ejecuta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369738924610" ID="ID_342140928" MODIFIED="1383399633692" TEXT="La informaci&#xf3;n es el procesador">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369738939450" ID="ID_1034917411" MODIFIED="1383399634464" TEXT="Movidos por una arquitectura de participaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369738973426" ID="ID_1172582216" MODIFIED="1383399635228" TEXT="beta perpetuo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369738988752" ID="ID_928530775" MODIFIED="1383399638281" TEXT="los usuarios son co-desarrolladores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1391106679397" ID="ID_482581696" MODIFIED="1391106910765" TEXT="Servicios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1391106688755" ID="ID_1360089542" MODIFIED="1391106690973" TEXT="Blogs">
<node CREATED="1391106784707" ID="ID_879041879" MODIFIED="1391106788127" TEXT="Espacio personal"/>
<node CREATED="1391106788428" ID="ID_949799992" MODIFIED="1391106808043" TEXT="Otros autores pueden a&#xf1;adir sus comentarios"/>
</node>
<node CREATED="1391106691240" ID="ID_546981726" MODIFIED="1391106695047" TEXT="Redes sociales">
<node CREATED="1391106834146" ID="ID_885899770" MODIFIED="1391106899151" TEXT="Sitios web donde cada usuario tiene una p&#xe1;gina y se comunica con otros usuarios"/>
</node>
<node CREATED="1391106695370" ID="ID_24320071" MODIFIED="1391106697404" TEXT="Wikis">
<node CREATED="1391106843821" ID="ID_868304430" MODIFIED="1391106849447" TEXT="Espacio colaborativo"/>
<node CREATED="1391106855523" ID="ID_1650927645" MODIFIED="1391106869260" TEXT="Varias personas elaboran contenidos de forma as&#xed;ncrona"/>
</node>
<node CREATED="1391106748920" ID="ID_1932953688" MODIFIED="1391106769208" TEXT="Entornos para compartir recursos (Google Drive)"/>
</node>
<node CREATED="1391106911339" ID="ID_345171977" MODIFIED="1391107071622" TEXT="Tecnolog&#xed;as">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1391106917350" ID="ID_301256720" MODIFIED="1391107120221" TEXT="Tecnolog&#xed;as de intercambio de datos:XML, JSON"/>
<node CREATED="1391106968236" ID="ID_504839100" MODIFIED="1391107125085" TEXT="T&#xe9;cnicas de contenido din&#xe1;mico (AJAX, Java)"/>
<node CREATED="1391106922471" ID="ID_1622007820" MODIFIED="1391107136476" TEXT="Tecnolog&#xed;as de sindicaci&#xf3;n de contenidos (RSS, Atom)"/>
<node CREATED="1391106995041" ID="ID_479607927" MODIFIED="1391107011591" TEXT="T&#xe9;cnicas de presentaci&#xf3;n: CSS, XHTML"/>
<node CREATED="1391107138147" ID="ID_1384902019" MODIFIED="1391107150654" TEXT="Aplicaciones h&#xed;bridas (mashups)">
<node CREATED="1376992999005" ID="ID_216085688" MODIFIED="1391105967812" TEXT="Mashup">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376993004057" ID="ID_640580534" MODIFIED="1377505454530" TEXT="Aplicaci&#xf3;n Web h&#xed;brida">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376993022511" ID="ID_89922428" MODIFIED="1377505457330" TEXT="Usa contenido de m&#xe1;s de una fuente para crear un nuevo servicio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1369740957738" ID="ID_1532162062" MODIFIED="1391368970131" TEXT="Sindicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369740969138" ID="ID_220220690" MODIFIED="1384864267590" TEXT="RSS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1382866499211" ID="ID_1295711813" MODIFIED="1382866525762" TEXT="Tecnolog&#xed;a completa para la distribuci&#xf3;n de contenidos Web">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369740979602" ID="ID_1430657734" MODIFIED="1382866539247" STYLE="bubble" TEXT="TAMBIEN es un formato de archivo de la familia XML">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382866660665" ID="ID_1412520478" MODIFIED="1382866686617" TEXT="NO es una fuente web. Es un FORMATO  de fuente Web">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1391368986898" ID="ID_316777175" MODIFIED="1391369000957" TEXT="Enumera los contenidos y los enlaces de una p&#xe1;gina Web"/>
<node CREATED="1375092817980" ID="ID_1292920776" MODIFIED="1382866559585" TEXT="Cada fuente de contenidos se denomina canal RSS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1380315865318" ID="ID_82719079" MODIFIED="1383399678102" TEXT="NO ES UN AGREGADOR. Los lectores o agregadores Web leen estos tipos de archivo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1391369046848" ID="ID_1583728550" MODIFIED="1391369090308" TEXT="Las &#xfa;ltimas versiones de exploradores no necesitan agregadores"/>
<node CREATED="1391369118251" ID="ID_26238365" MODIFIED="1391369141445" TEXT="Leyendo estos ficheros los agregadores proporcionan un resumen de la p&#xe1;gina web"/>
</node>
<node CREATED="1375092697665" ID="ID_100906737" MODIFIED="1381404220027" TEXT="RSS 1.0: RDF Site Summary, usa XML y RDF">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375092645992" ID="ID_216290919" MODIFIED="1381404222414" TEXT="RSS 2.0: Really Simple Syndication">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1369741008130" ID="ID_574472464" MODIFIED="1391106021879" TEXT="Atom">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1382866691748" ID="ID_1397249812" MODIFIED="1382866826613" TEXT="Otro Formato de fuente web, como RSS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382866800880" ID="ID_1588551378" MODIFIED="1382866816690" TEXT="TAMBIEN forma parte de XML">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1391368281845" ID="ID_1588619902" MODIFIED="1391368747480">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="rssdiagram.jpg" />
  </body>
</html></richcontent>
</node>
<node CREATED="1380315783166" ID="ID_296253143" MODIFIED="1380315790518" TEXT="Sirve para personalizar los contenidos que cada usuario quer&#xed;a ver al acceder a un sitio Web">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1382867476151" ID="ID_1898913764" MODIFIED="1384864090632" TEXT="Web sem&#xe1;ntica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369741147506" ID="ID_1049921015" MODIFIED="1384864273995" TEXT="RDF (Resource Description Framework)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Descripci&#243;n de los recursos e intercambio de dicha informaci&#243;n en ls Web.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369741213674" ID="ID_1263948808" MODIFIED="1382866727044" TEXT="A&#xf1;ade metadatos sem&#xe1;nticos a los contenidos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372411767575" ID="ID_216316074" MODIFIED="1380882100399" TEXT="Describe los recursos en tripletes: sujeto-predicado-objeto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376390777558" ID="ID_1906971557" MODIFIED="1382606854477" TEXT="Utiliza XML para el intercambio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372412005278" ID="ID_1898099781" MODIFIED="1384864298638" TEXT="OWL (Web Ontology Language)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tambi&#233;n a&#241;ade significados a los contenidos. Facilita el proceso de la informaci&#243;n por m&#225;quinas, en vez de la presentaci&#243;n para los humanos
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372412061621" ID="ID_1455701957" MODIFIED="1380882094247" TEXT="Construido sobre RDF">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372412070206" ID="ID_1163402169" MODIFIED="1380882097680" TEXT="Codificado en XML">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382866953224" ID="ID_1876156818" MODIFIED="1382867253156" TEXT="Procede de DAMP + OIL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382866988857" ID="ID_186465709" MODIFIED="1382866999089" TEXT="Variantes">
<node CREATED="1382866999091" ID="ID_1657466840" MODIFIED="1382867249198" TEXT="OWL Lite">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382867005054" ID="ID_75394370" MODIFIED="1382867250134" TEXT="OWL DL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382867009676" ID="ID_1957803286" MODIFIED="1382867250720" TEXT="OWL Full">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1370776785997" ID="ID_1124105699" MODIFIED="1384779868949" POSITION="right" TEXT="Robots">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370776793143" ID="ID_792947221" MODIFIED="1377420882221" TEXT="Spiders. Accede a todos los enlaces del hipertexto y los almacena en el motor de b&#xfa;squeda">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370776796676" ID="ID_1707996508" MODIFIED="1377420889205" TEXT="Knowbots. Localizan referencias hipertextuales hacia un servidor concreto.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370776800685" ID="ID_1784674399" MODIFIED="1377420892625" TEXT="Wanderers. Realizan estad&#xed;sticas sobre la red.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376992744371" ID="ID_1309332393" MODIFIED="1381404507019" TEXT="Webcrawler">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376992750896" ID="ID_1837376499" MODIFIED="1376992759708" TEXT="Buscan nuevo contenido"/>
</node>
</node>
</node>
</map>
