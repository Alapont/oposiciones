<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1372233353557" ID="ID_971927052" MODIFIED="1379168068378" TEXT="Windows">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372236419631" FOLDED="true" ID="ID_562531996" MODIFIED="1408360518699" POSITION="right" TEXT="Arquitectura">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372234193662" ID="ID_192663780" MODIFIED="1372235290829" TEXT="Aplicaciones (.NET Visual Basic)">
<node CREATED="1372235039489" ID="ID_878278940" MODIFIED="1372235062764" TEXT="Interfaz Win32">
<node CREATED="1372234198323" ID="ID_117535317" MODIFIED="1372234988647" TEXT="RPC">
<arrowlink DESTINATION="ID_1854910904" ENDARROW="Default" ENDINCLINATION="275;0;" ID="Arrow_ID_892683248" STARTARROW="None" STARTINCLINATION="211;-8;"/>
</node>
</node>
</node>
<node CREATED="1372234152491" ID="ID_1854910904" MODIFIED="1408360303667" TEXT="Modo usuario">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372234161808" ID="ID_1854550885" MODIFIED="1384549338425" TEXT="Applets: Win 32">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372234385129" ID="ID_406916795" MODIFIED="1384549340125" TEXT="Servicios de NT">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372234310926" ID="ID_67754302" MODIFIED="1384549340796" TEXT="GUI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372234171170" ID="ID_1491771029" MODIFIED="1384549341373" TEXT="DLLs">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372234323386" ID="ID_1500603755" MODIFIED="1384549341982" TEXT="API del subsistema (kernel32.dll)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372234333739" ID="ID_864673693" MODIFIED="1408360319918" TEXT="API de NT nativa (ntdll.dll)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372234783734" ID="ID_1449824382" MODIFIED="1381933731579" TEXT="Llamadas al sistema: operan sobre objetos y devuelven &quot;manejadores&quot;">
<arrowlink DESTINATION="ID_745053414" ENDARROW="Default" ENDINCLINATION="497;0;" ID="Arrow_ID_263025916" STARTARROW="None" STARTINCLINATION="497;0;"/>
<node CREATED="1372234811267" ID="ID_1279487971" MODIFIED="1372234820924" TEXT="Sincronizaci&#xf3;n"/>
<node CREATED="1372234821323" ID="ID_1035652330" MODIFIED="1372234825031" TEXT="E/S"/>
<node CREATED="1372234825430" ID="ID_1244606087" MODIFIED="1372234829545" TEXT="Programa"/>
<node CREATED="1372234830085" ID="ID_934147564" MODIFIED="1372234851131" TEXT="GUI "/>
</node>
</node>
</node>
<node CREATED="1372234108051" ID="ID_745053414" MODIFIED="1391266530160" TEXT="Modo Kernel">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372234108051" ID="ID_30979389" MODIFIED="1384541278981" TEXT="NTOS(ntoskrnl.exe)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se&#160;trata de la imagen del kernel.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372234268945" ID="ID_1998064032" MODIFIED="1385557185679" TEXT="Nivel Ejecutivo (Executive): la mayor parte de los servicios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372236008577" ID="ID_1718113889" MODIFIED="1372236015829" TEXT="Escrito en C"/>
<node CREATED="1372236818138" ID="ID_560907846" MODIFIED="1372236828016" TEXT="Componentes">
<node CREATED="1372236828017" ID="ID_688578914" MODIFIED="1384541334739" TEXT="Gestor de objetos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372236849916" ID="ID_682603492" MODIFIED="1384541336009" TEXT="Gestor de I/O">
<arrowlink DESTINATION="ID_1095765259" ENDARROW="Default" ENDINCLINATION="371;0;" ID="Arrow_ID_1307310400" STARTARROW="None" STARTINCLINATION="371;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372236871736" ID="ID_1468711548" MODIFIED="1384541336768" TEXT="Gestor de procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372236883290" ID="ID_514054589" MODIFIED="1384541337325" TEXT="Gestor de memoria">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372236888503" ID="ID_1762479490" MODIFIED="1384541337914" TEXT="Gestor de cach&#xe9;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372236894782" ID="ID_146819074" MODIFIED="1377799341652" TEXT="Monitor de referencia de seguridad (EAL4+)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1372234280354" ID="ID_1764104637" MODIFIED="1385557183377" TEXT="Microkernel">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372235779077" ID="ID_1186341236" MODIFIED="1408360343047" TEXT="abstracciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372235862119" ID="ID_1309521000" MODIFIED="1384377332917" TEXT="Hilos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372235939805" ID="ID_1150498319" MODIFIED="1384377326849" TEXT="CPU donde se ejecuta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372235969955" ID="ID_1162462608" MODIFIED="1384377329891" TEXT="Quantum">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372235989505" ID="ID_69087902" MODIFIED="1384377331451" TEXT="Cuando se ejecuta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372236792582" ID="ID_408174648" MODIFIED="1382884448774" TEXT="Se dividen en modo kernel y de usuario">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372235871486" ID="ID_893234318" MODIFIED="1372235880504" TEXT="Excepciones"/>
<node CREATED="1372235880923" ID="ID_522645603" MODIFIED="1372235882605" TEXT="Traps"/>
</node>
<node CREATED="1372235888830" ID="ID_1151977916" MODIFIED="1408360343993" TEXT="Sincronizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372235913883" ID="ID_1912403923" MODIFIED="1372235920895" TEXT="Objetos de control"/>
<node CREATED="1372235921298" ID="ID_256341190" MODIFIED="1382884438583" TEXT="Dispatcher">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1372234478094" ID="ID_1163891559" MODIFIED="1377799354635" TEXT="HAL (Hardware Abstraction Layer)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372234601504" ID="ID_1095765259" MODIFIED="1391266537762" TEXT="Drivers: Bibliotecas de v&#xed;nculos din&#xe1;micos que se cargan mediante el ejecutivo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372234606008" ID="ID_907784713" MODIFIED="1372234608123" TEXT="GUI"/>
<node CREATED="1372234608444" ID="ID_1519689399" MODIFIED="1372234612016" TEXT="Dispositivos"/>
</node>
<node CREATED="1376589658451" ID="ID_481415734" MODIFIED="1384377293324" TEXT="EL KERNEL DESDE NT SE CONSIDERA HIBRIDO">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Hay que tener en cuenta que algunos procesos pueden ejecutarse en modo kernel, luego no es completamente microkernel.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1384541214554" ID="ID_1058872306" MODIFIED="1384541229026">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="275px-Windows_2000_architecture.svg.png" />
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1372237332949" FOLDED="true" ID="ID_1939973937" MODIFIED="1408360947552" POSITION="right" TEXT="Procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372238929734" FOLDED="true" ID="ID_672187859" MODIFIED="1408360712161" TEXT="Entidades">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372238204567" ID="ID_835408573" MODIFIED="1385557199062" TEXT="Tareas o Trabajos: agrupaci&#xf3;n de procesos ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372237129458" ID="ID_959058007" MODIFIED="1377799388328" TEXT="Procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372237146618" ID="ID_347850041" MODIFIED="1382884578816" TEXT="PEB (Process Environment Block)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372237176082" ID="ID_846323802" MODIFIED="1372237186398" TEXT="Espacio de direcciones virtuales"/>
<node CREATED="1372237186793" ID="ID_293635722" MODIFIED="1372237220110" TEXT="Lista de m&#xf3;dulos cargados"/>
<node CREATED="1372237223445" ID="ID_578570272" MODIFIED="1372237229940" TEXT="Directorio actual de trabajo"/>
</node>
<node CREATED="1372238230172" ID="ID_1668087458" MODIFIED="1382884576702" TEXT="No pueden estar en m&#xe1;s de un trabajo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372237238317" ID="ID_60838397" MODIFIED="1385557316279" TEXT="Hilos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372238282648" ID="ID_1933021359" MODIFIED="1380717121407" TEXT="No pueden estar en m&#xe1;s de un proceso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1380717123361" ID="ID_1250849130" MODIFIED="1381934917651" STYLE="bubble" TEXT="Usan multitarea preemptiva">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372238334118" ID="ID_299450436" MODIFIED="1377799405842" TEXT="No contienen fibras">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372238403707" ID="ID_472172305" MODIFIED="1377799415572" TEXT="Modos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372237295625" ID="ID_1783138835" MODIFIED="1391266878629" TEXT="Usuario">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372237269686" ID="ID_805833565" MODIFIED="1372237292360" TEXT="TEB (Thread Environment Block)"/>
</node>
<node CREATED="1372237299098" ID="ID_389837286" MODIFIED="1377799421796" TEXT="Kernel">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377944067718" ID="ID_196028714" MODIFIED="1377944125584" TEXT="UMS (User Mode Scheduling)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Permite separar el proceso en modo usuario y modo kernel
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372238537136" ID="ID_1623976837" MODIFIED="1384549652719" TEXT="Una pila en cada modo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372238525993" ID="ID_1914691757" MODIFIED="1384371740253" TEXT="Como en Unix, pueden estar en varios estados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1372238215726" ID="ID_471213813" MODIFIED="1377799428062" TEXT="Fibras: hilo ligero en modo Usuario">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372238446486" ID="ID_238900558" MODIFIED="1380717168169" TEXT="No es necesario entrar y salir del modo kernel">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1380717102552" ID="ID_949394221" MODIFIED="1381934911661" STYLE="bubble" TEXT="Usan multitarea cooperativa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372238459113" ID="ID_816128044" MODIFIED="1372238471065" TEXT="Menor sobrecarga que el cambio de hilos"/>
</node>
</node>
<node CREATED="1372238557099" ID="ID_302997712" MODIFIED="1408360941255" TEXT="Comunicaci&#xf3;n entre procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1372238592070" ID="ID_1199060792" MODIFIED="1408360715088" TEXT="Pipes (como UNIX)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372238618484" ID="ID_1039629919" MODIFIED="1372238630862" TEXT="Byte"/>
<node CREATED="1372238623422" ID="ID_1777633603" MODIFIED="1372238626532" TEXT="Mensaje"/>
</node>
<node CREATED="1372238597194" ID="ID_1590207980" MODIFIED="1377799445402" TEXT="Ranuras o slots de correo (apenas se usan)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372238607746" ID="ID_685149300" MODIFIED="1377799448725" TEXT="Sockets (como UNIX)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372238688157" ID="ID_512708388" MODIFIED="1408360720194" TEXT="RPCs">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372238810329" ID="ID_304782638" MODIFIED="1382884639740" TEXT="sobre un nivel de transporte">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372238825893" ID="ID_1989949669" MODIFIED="1372238833018" TEXT="sockets"/>
<node CREATED="1372238833321" ID="ID_551999735" MODIFIED="1372238871824" TEXT="ALPC (Llamada Avanzada a Procedimiento Local)"/>
</node>
</node>
</node>
<node CREATED="1372238896299" ID="ID_1131661244" MODIFIED="1408360778578" TEXT="Sincronizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372238917675" ID="ID_1538743992" MODIFIED="1377799457282" TEXT="Sem&#xe1;foros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372238921311" ID="ID_1718669946" MODIFIED="1377799459837" TEXT="Mutex">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372238924695" ID="ID_199340517" MODIFIED="1377799463948" TEXT="Eventos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372238972049" ID="ID_328522949" MODIFIED="1377799469827" TEXT="Funcionan con HILOS, no con PROCESOS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372239080208" ID="ID_1178127831" MODIFIED="1408360945304" TEXT="Planificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372239094825" ID="ID_1595446746" MODIFIED="1377799476090" TEXT="Prioridades 1 a 15">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1372239143375" FOLDED="true" ID="ID_1019629803" MODIFIED="1408363992447" POSITION="right" TEXT="Memoria">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372239169211" ID="ID_1142135404" MODIFIED="1372239173161" TEXT="x86">
<node CREATED="1372239173164" ID="ID_1548797722" MODIFIED="1384377528198" TEXT="32 bits de direcci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372239179910" ID="ID_1844610932" MODIFIED="1384377530024" TEXT="4 Gb de memoria virtual">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372239229370" ID="ID_1098337506" MODIFIED="1391699971784" TEXT="Asignaci&#xf3;n de direcciones virtuales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372239244650" ID="ID_1810558868" MODIFIED="1382885215179" TEXT="Estados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372239248259" ID="ID_666413711" MODIFIED="1381935160544" TEXT="Inv&#xe1;lida">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372239257073" ID="ID_1156016140" MODIFIED="1381935162993" TEXT="Reservada: inv&#xe1;lida, pero no se asignar&#xe1;n esas direcciones virtuales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372239266120" ID="ID_1353675848" MODIFIED="1381935165099" TEXT="Confirmada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372239286785" ID="ID_1091981209" MODIFIED="1382885263984" TEXT="Fallos de p&#xe1;gina">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372239303564" ID="ID_1683857890" MODIFIED="1381935152837" TEXT="Actualizar la entrada en la tabla: fallos suaves">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372239770035" ID="ID_1822963643" MODIFIED="1381935155739" TEXT="Leer la p&#xe1;gina del disco porque no est&#xe1; en memoria: fallos duros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372239587000" ID="ID_494179275" MODIFIED="1377799544850" STYLE="bubble" TEXT="Bit de acceso (A)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372239654506" ID="ID_1135426619" MODIFIED="1377799547589" TEXT="Si un acceso pudo modificar la p&#xe1;gina">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372239672480" ID="ID_1699548355" MODIFIED="1382885877617" TEXT="Para usar el Algoritmo LRU">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372239596846" ID="ID_1695211738" MODIFIED="1377799554774" STYLE="bubble" TEXT="Bit de p&#xe1;gina sucia (D)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372239689132" ID="ID_432050576" MODIFIED="1377799559799" TEXT="Si una p&#xe1;gina ha sido modificada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372239708803" ID="ID_1193572299" MODIFIED="1377799563091" TEXT="Si no se ha modificado, no es necesario escribirla en disco">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1372239366922" ID="ID_616140113" MODIFIED="1384377537246" TEXT="Paginaci&#xf3;n bajo demanda">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372239435176" ID="ID_1867541757" MODIFIED="1377799523595" TEXT="No se admite segmentaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372239463011" ID="ID_1478264900" MODIFIED="1377799573495" TEXT="P&#xe1;ginas de 4 Mb para el kernel y aplicaciones extensas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372239489559" ID="ID_1848454917" MODIFIED="1382885920050" TEXT="Trata con procesos, no con hilos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372239529729" ID="ID_46906890" MODIFIED="1372239545092" TEXT="VAD (Virtual Address Descriptors)"/>
</node>
<node CREATED="1372240186167" ID="ID_152719488" MODIFIED="1391291102018" TEXT="Memoria f&#xed;sica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372240191471" ID="ID_402643170" MODIFIED="1385557858171" TEXT="P&#xe1;ginas para asignar a procesos (se comprueban antes de traerlas del disco)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372240256683" ID="ID_1574185542" MODIFIED="1377799583825" TEXT="Lista de p&#xe1;ginas libres">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372240216286" ID="ID_4353041" MODIFIED="1377799588129" TEXT="Lista de p&#xe1;ginas en espera">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372240206676" ID="ID_648080225" MODIFIED="1382885929004" TEXT="Lista de p&#xe1;ginas modificadas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372240370994" ID="ID_207436572" MODIFIED="1377799590664" TEXT="Lista de p&#xe1;ginas con ceros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372240382576" ID="ID_176440520" MODIFIED="1377799593745" TEXT="P&#xe1;ginas con errores de HW">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1372240435027" FOLDED="true" ID="ID_764808897" MODIFIED="1408364002488" POSITION="right" TEXT="E/S">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372240467376" ID="ID_24890645" MODIFIED="1382903680116" TEXT="Prioridades (5)">
<node CREATED="1372240493518" ID="ID_960139320" MODIFIED="1372240498929" TEXT="Cr&#xed;tica"/>
<node CREATED="1372240499357" ID="ID_1390289510" MODIFIED="1372240505064" TEXT="Alta "/>
<node CREATED="1372240505331" ID="ID_530392216" MODIFIED="1372240507043" TEXT="Normal"/>
<node CREATED="1372240507413" ID="ID_96657614" MODIFIED="1372240508757" TEXT="Baja "/>
<node CREATED="1372240509079" ID="ID_332042347" MODIFIED="1372240514499" TEXT="Muy baja"/>
</node>
</node>
<node CREATED="1372240544912" ID="ID_1200363805" MODIFIED="1408364003292" POSITION="right" TEXT="Sistemas de archivos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372240549497" ID="ID_1975937470" MODIFIED="1377067858791" TEXT="FAT-32">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372240561948" ID="ID_784873329" MODIFIED="1377067805724" TEXT="Direcciones de 32 bits">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372240571559" ID="ID_1889606439" MODIFIED="1377067863367" TEXT="Particiones de hasta 2 TB">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372240582267" ID="ID_85745355" MODIFIED="1372240591924" TEXT="No hay seguridad"/>
</node>
<node CREATED="1372240553194" ID="ID_1071867371" MODIFIED="1377067846462" TEXT="NTFS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372240596667" ID="ID_677992656" MODIFIED="1377067834365" TEXT="Direcciones de 64 bits">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1391700782073" ID="ID_104536590" MODIFIED="1391700822258" TEXT="16 TiB clusteres de 4 Kbi (2 elevado a 32 -1)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372240603884" ID="ID_1361375668" MODIFIED="1377080382315" TEXT="Nombre limitados a 256 caracteres">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372240645154" ID="ID_262944825" MODIFIED="1377080379971" TEXT="Sensibles a may&#xfa;sculas y min&#xfa;sculas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372240619606" ID="ID_1264283784" MODIFIED="1377080377467" TEXT="Rutas hasta 32767 caracteres">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372240703589" ID="ID_1292179224" MODIFIED="1372240711268" TEXT="ID de archivo"/>
<node CREATED="1372240717982" ID="ID_451405287" MODIFIED="1377067848678" TEXT="MFT (Master File Table)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372240784219" ID="ID_1422856935" MODIFIED="1384377934158" TEXT="Secuencia de registros de 1 KB">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372240794143" ID="ID_1690657050" MODIFIED="1377080354105" TEXT="Cada registro describe un archivo o directorio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372240829161" ID="ID_1146248376" MODIFIED="1391700944149" TEXT="Atributos de cada archivo (13)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Como un i-nodo de Unix
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372240836233" ID="ID_1283196844" MODIFIED="1384377929961" TEXT="Lista de direcciones de disco donde se encuentran sus bloques">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1372240914756" ID="ID_1542133072" MODIFIED="1377080369259" TEXT="Arbol B+ para b&#xfa;squeda alfab&#xe9;tica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372240986425" ID="ID_961188933" MODIFIED="1377080372906" TEXT="Driver EFS: sistema de cifrado de archivos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1372241081927" FOLDED="true" ID="ID_215352596" MODIFIED="1408360142503" POSITION="right" TEXT="Seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372241140579" ID="ID_763090496" MODIFIED="1372241145686" TEXT="SID de usuario"/>
<node CREATED="1372241159675" ID="ID_274906658" MODIFIED="1377799626161" TEXT="DACL (Discreccional ACL): Lista de control de acceso discreccional">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372241214326" ID="ID_223559701" MODIFIED="1372241221904" TEXT="Descriptor de seguridad">
<node CREATED="1372241314845" ID="ID_1696521570" MODIFIED="1372241319944" TEXT="Encabezado"/>
<node CREATED="1372241320235" ID="ID_1351160121" MODIFIED="1372241328962" TEXT="SID del propietario"/>
<node CREATED="1372241329225" ID="ID_208120238" MODIFIED="1372241333691" TEXT="SID del grupo"/>
<node CREATED="1372241334591" ID="ID_1738364514" MODIFIED="1372241338704" TEXT="DACL">
<node CREATED="1372241381720" ID="ID_439559441" MODIFIED="1372241404410" TEXT="ACE (Access Control Entry)"/>
</node>
<node CREATED="1372241339797" ID="ID_1509524520" MODIFIED="1372241342048" TEXT="SACL"/>
</node>
</node>
<node CREATED="1372241427168" ID="ID_1835073967" MODIFIED="1377799659317" TEXT="UAC(User Account Control)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376579905695" ID="ID_754220615" MODIFIED="1377799644841" TEXT="Protocolo Kerberos de autenticaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372241194468" ID="ID_1213808096" MODIFIED="1377799629748" TEXT="Inicio de sesi&#xf3;n: token de acceso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1376579819797" ID="ID_1788101913" MODIFIED="1384550404220" TEXT="bitlocker">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376579830551" ID="ID_745580675" MODIFIED="1380717602891" TEXT="Cifrado de discos duros completos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376579846734" ID="ID_458243818" MODIFIED="1377799664782" TEXT="Se proporciona desde Windows Vista">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376579866491" ID="ID_552190619" MODIFIED="1377799669234" TEXT="Desde Windows 7 incorpora funcionalidad para USB">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1372241548090" FOLDED="true" ID="ID_406959869" MODIFIED="1408360138679" POSITION="right" TEXT="Consola: Windows Powershell, incluida en .NET framework desde 2006">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376579797926" ID="ID_981650828" MODIFIED="1376579803334" TEXT="Consola de comandos"/>
<node CREATED="1376579803591" ID="ID_522436809" MODIFIED="1376579811628" TEXT="Lenguaje de scripting"/>
</node>
<node CREATED="1372241619004" FOLDED="true" ID="ID_170685669" MODIFIED="1408360140087" POSITION="right" TEXT="Ediciones de Windows 7">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372241629459" ID="ID_59196360" MODIFIED="1377067802588" TEXT="Starter">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372241634851" ID="ID_1821726076" MODIFIED="1377067809053" TEXT="Home Basic">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372241640197" ID="ID_377030211" MODIFIED="1377067811589" TEXT="Home Premium">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372241651996" ID="ID_1571569783" MODIFIED="1377067813933" TEXT="Professional">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372241666573" ID="ID_488754880" MODIFIED="1377067816484" TEXT="Enterprise">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372241672215" ID="ID_1420682545" MODIFIED="1377067818997" TEXT="Ultimate">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372241864789" FOLDED="true" ID="ID_474653480" MODIFIED="1391290857225" POSITION="right" TEXT="Windows Server 2008R2">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372242073973" ID="ID_1212485070" MODIFIED="1372242090142" TEXT="Live Migration"/>
<node CREATED="1372242090519" ID="ID_728886887" MODIFIED="1372242097662" TEXT="Hyper-V"/>
<node CREATED="1372265492947" ID="ID_370940740" MODIFIED="1377067794284" TEXT="Ojo: en Windows 200x NO existen los BDC. Todos son primarios.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372265297740" ID="ID_1056240530" MODIFIED="1379169123575" TEXT="Tambi&#xe9;n admite NFS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372241478195" ID="ID_520828280" MODIFIED="1382962869770" TEXT="Hasta 256 procesadores l&#xf3;gicos en W7 y Server 2008 R2">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1382962955240" ID="ID_1597670829" LINK="Windows%20Server%202012.mm" MODIFIED="1382962973388" POSITION="right" TEXT="Windows Server 2012">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</map>
