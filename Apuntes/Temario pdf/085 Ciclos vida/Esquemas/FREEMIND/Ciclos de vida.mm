<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1369998229683" ID="ID_384632080" MODIFIED="1378199194246" TEXT="Ciclos de vida">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372750660809" FOLDED="true" ID="ID_428091514" MODIFIED="1385574248189" POSITION="right" TEXT="Conceptos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376645178437" ID="ID_997047141" MODIFIED="1385573841862" TEXT="ISO 12207">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="idea"/>
<node CREATED="1376645245921" ID="ID_1519176617" MODIFIED="1384506805660" TEXT="Marco de referencia que contiene procesos, actividades y tareas"/>
<node CREATED="1376645261553" ID="ID_1610596382" MODIFIED="1376645369468" TEXT="Involucrados en: Desarrollo, explotaci&#xf3;n y mantenimiento"/>
<node CREATED="1376645278226" ID="ID_510209881" MODIFIED="1376645375526" TEXT="Abarca: Desde la defici&#xf3;n de requisitos hasta la finalizaci&#xf3;n de su uso"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1370020278097" ID="ID_1612216889" MODIFIED="1376580195078" STYLE="bubble" TEXT="Objetivos">
<node CREATED="1370020288967" ID="ID_572502998" MODIFIED="1375108267930" TEXT="Definir las actividades a realizar y en qu&#xe9; orden"/>
<node CREATED="1370020302542" ID="ID_811151847" MODIFIED="1375108267930" TEXT="Asegurar la consistencia"/>
<node CREATED="1370020320153" ID="ID_1274462002" MODIFIED="1375108267931" TEXT="Proporcionar puntos de control"/>
</node>
<node CREATED="1376822759637" ID="ID_1553384790" MODIFIED="1376822809366" TEXT="Existe un proceso espec&#xed;fico para la adaptaci&#xf3;n del resto de procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372750672624" ID="ID_1620453233" MODIFIED="1380966239548" TEXT="L&#xed;nea base: especificaci&#xf3;n o producto que ha sido revisado formalmente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1376645416472" FOLDED="true" ID="ID_1843971734" MODIFIED="1385574600765" POSITION="right" TEXT="Tradicionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369998296183" ID="ID_933897467" MODIFIED="1384506876296" TEXT="Modelo en cascada (Royce)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369998582719" ID="ID_16413312" MODIFIED="1384507448805">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="cascada.png" />
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370100321059" ID="ID_858826183" MODIFIED="1370100334576" TEXT="La documentaci&#xf3;n gu&#xed;a todo el proceso"/>
</node>
</node>
<node CREATED="1369998648734" FOLDED="true" ID="ID_1122754765" MODIFIED="1385574443215" TEXT="Prototipos">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      En general los prototipos no son adecuados para proyectos muy cortos (menos de un mes) o muy largos. Son &#243;ptimos para 3-5 meses.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369998663359" FOLDED="true" ID="ID_768835544" MODIFIED="1385574273010" TEXT="Prototipado r&#xe1;pido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370071250075" ID="ID_1434384947" MODIFIED="1375087199896" TEXT="Objetivo: crear y validar requisitos"/>
<node CREATED="1370071261921" ID="ID_56570262" MODIFIED="1370071272266" TEXT="Debe hacerse de forma r&#xe1;pida"/>
<node CREATED="1370071272615" ID="ID_1286815948" MODIFIED="1375087172702" STYLE="bubble" TEXT="Debe ser DESECHABLE"/>
</node>
<node CREATED="1369998258394" ID="ID_1387427388" MODIFIED="1384604067860" TEXT="Prototipado evolutivo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369998708484" ID="ID_1344891447" MODIFIED="1378199613790" TEXT="James Martin: no se lo que quiero, pero cuando lo vea, te lo digo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374676709860" ID="ID_768250158" MODIFIED="1380781970076" STYLE="bubble" TEXT="Se hace evolucionar al prototipo. NO SE DESECHA   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375087328617" ID="ID_1193392675" MODIFIED="1379440794281" TEXT="No se conocen los requisitos. Se conocer&#xe1;n con la evoluci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369998721406" ID="ID_874315298" MODIFIED="1383126007892" TEXT="4GL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369998734440" ID="ID_458888538" MODIFIED="1380968148251" TEXT="RAD (Rapid Application Development)   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378207746058" ID="ID_1598388404" MODIFIED="1379440812128" TEXT="Planificaci&#xf3;n de requerimientos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378207756131" ID="ID_83689477" MODIFIED="1379440813781" TEXT="Dise&#xf1;o con usuario">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378207769155" ID="ID_621850563" MODIFIED="1379440814889" TEXT="Construcci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378207773780" ID="ID_1467537076" MODIFIED="1379440815918" TEXT="Implantaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1374676661762" ID="ID_1798185777" MODIFIED="1383126012493" TEXT="Hace uso de la reutilizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372662133721" ID="ID_444504389" MODIFIED="1384507405319" TEXT="Implica un dise&#xf1;o suficientemente flexible como para incorporar modificaciones">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Esto no ocurre en dos situaciones:
    </p>
    <p>
      - Es necesario una fuerte integraci&#243;n con otras aplicaciones.
    </p>
    <p>
      - Se trata de situaciones puente, el nuevo software est&#225; reemplazando poco a poco al existente.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
</node>
<node CREATED="1375087255124" ID="ID_1854504862" MODIFIED="1384507179210" TEXT="INCONVENIENTE: se puede considerar una nueva versi&#xf3;n de CODE AND FIX       ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node CREATED="1369998272503" ID="ID_619135903" MODIFIED="1384604077253" TEXT="Prototipado incremental">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372662089011" ID="ID_1361994096" MODIFIED="1379440786700" TEXT="Los requisitos se conocen en su totalidad, a diferencia del anterior">
<arrowlink DESTINATION="ID_1193392675" ENDARROW="Default" ENDINCLINATION="166;0;" ID="Arrow_ID_1698207275" STARTARROW="None" STARTINCLINATION="166;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372662175164" ID="ID_1311782492" MODIFIED="1384507465532" TEXT="La implementaci&#xf3;n se va dosificando, ampliando la FUNCIONALIDAD">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378278120096" ID="ID_246203617" MODIFIED="1384507463761" TEXT="Ventajas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378278123833" ID="ID_33022649" MODIFIED="1384507431700" TEXT="Desarrollo r&#xe1;pido. Indicado para requisitos de tiempo estrictos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="idea"/>
</node>
<node CREATED="1378278149270" ID="ID_658067562" MODIFIED="1384507444483" TEXT="Los usuarios conocen lo que esperan">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378278199043" ID="ID_1872059916" MODIFIED="1384507456677" TEXT="Inconvenientes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378278204739" ID="ID_1976729634" MODIFIED="1384507453037" TEXT="Los usuarios pueden ver en el prototipo el desarrollo final">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378278221409" ID="ID_1366444840" MODIFIED="1384507458768" TEXT="Intromisi&#xf3;n en el desarrollo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378278270092" ID="ID_987704540" MODIFIED="1384507459459" TEXT="Pueden no cumplirse las expectativas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1376645430632" FOLDED="true" ID="ID_559826637" MODIFIED="1385574598726" POSITION="right" TEXT="Alternativos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369998287783" ID="ID_255410332" MODIFIED="1383149571239" TEXT="Modelo en espiral (Boehm)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369998800296" ID="ID_1811005134" MODIFIED="1369998806522">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="espiral.png" />
  </body>
</html></richcontent>
<node CREATED="1370100297784" ID="ID_950167473" MODIFIED="1384507583168" TEXT="Se adapta a la orientaci&#xf3;n a OBJECTOS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1370100863362" ID="ID_52747310" MODIFIED="1384507599517" TEXT="Permite la REUTILIZACI&#xd3;N de software existente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1370100915308" ID="ID_1667887898" MODIFIED="1384507620017" TEXT="Permite preparar la EVOLUCI&#xd3;N del ciclo de vida">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1370100934722" ID="ID_167907423" MODIFIED="1384507631789" TEXT="Proporciona un mecanismo para incorporar la CALIDAD">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1370100957448" ID="ID_1798485310" MODIFIED="1384507678970" TEXT="Adecuado para la temprana eliminaci&#xf3;n de ERRORES">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1370100977224" ID="ID_945907536" MODIFIED="1384507773236" TEXT="No implica PROCEDIMIENTOS SEPARADOS para el desarrollo y la mejora">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1370101019514" ID="ID_160356548" MODIFIED="1384507825641" TEXT="Proporciona un marco viable para desarrollos software-HARDWARE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-7"/>
</node>
</node>
</node>
<node CREATED="1369998847838" ID="ID_1829912883" MODIFIED="1383128273388" TEXT="Modelos basados en la transformaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370071921607" ID="ID_1888484382" MODIFIED="1380968905407" TEXT="Transforman una especificaci&#xf3;n formal en un producto software"/>
<node CREATED="1369998928531" ID="ID_1605752561" MODIFIED="1372696000118" TEXT="Robert Pressmann:Usan 4GL"/>
<node CREATED="1369998948959" ID="ID_1954101635" MODIFIED="1372695986498" TEXT="Herramientas CASE (modelos de transformaci&#xf3;n de Carma McClure)"/>
</node>
</node>
<node CREATED="1370100496196" ID="ID_1990590796" MODIFIED="1384604232652" POSITION="right" TEXT="Alternativas actuales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369998981165" FOLDED="true" ID="ID_559611907" MODIFIED="1385574724484" TEXT="PUDS (Proceso Unificado de Desarrollo Software) o RUP (Rational Unified Process)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370100250361" ID="ID_1590670461" MODIFIED="1379441226449" TEXT="Centrado en la arquitectura">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370100259932" ID="ID_1256229120" MODIFIED="1379441228087" TEXT="Actividades dirigidas por casos de uso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370100281434" ID="ID_1812698639" MODIFIED="1379441228992" TEXT="Soportan la orientaci&#xf3;n a objetos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370100648018" ID="ID_23707035" MODIFIED="1379441230692" TEXT="Iterativo e incremental">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370071978945" ID="ID_381710338" MODIFIED="1370100377481" TEXT="Fases del ciclo de desarrollo">
<node CREATED="1370000044404" ID="ID_1115542241" MODIFIED="1378199955245" TEXT="Iniciaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370000051307" ID="ID_1864661998" MODIFIED="1378199956917" TEXT="Elaboraci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370000055370" ID="ID_150463579" MODIFIED="1384604417600" TEXT="Construcci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370000140375" ID="ID_314914775" MODIFIED="1370000148817" TEXT="Flujo de implementaci&#xf3;n"/>
<node CREATED="1370000149212" ID="ID_107385685" MODIFIED="1370000154269" TEXT="Flujo de pruebas"/>
</node>
<node CREATED="1370000063194" ID="ID_1079987803" MODIFIED="1378199961581" TEXT="Transici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1380782324358" ID="ID_1094808483" MODIFIED="1383127520093" TEXT="Cada fase tiene: ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1380782356002" ID="ID_441037632" MODIFIED="1380782406790" TEXT="Iteraciones de desarrollo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370100429657" ID="ID_1397595656" MODIFIED="1380782459828" TEXT="">
<node CREATED="1376580960709" ID="ID_1673321219" MODIFIED="1376580969744" TEXT="Modelado de negocio"/>
<node CREATED="1376580970109" ID="ID_1552428998" MODIFIED="1376580972872" TEXT="Requisitos"/>
<node CREATED="1376580973143" ID="ID_1605338483" MODIFIED="1376580989572" TEXT="An&#xe1;lisis y dise&#xf1;o"/>
<node CREATED="1376580989999" ID="ID_1568241827" MODIFIED="1376580997894" TEXT="Implementaci&#xf3;n"/>
<node CREATED="1376580998228" ID="ID_1853313999" MODIFIED="1376581000523" TEXT="Pruebas"/>
<node CREATED="1376581000794" ID="ID_384688677" MODIFIED="1376581003167" TEXT="Despliegue"/>
</node>
</node>
<node CREATED="1380782369063" ID="ID_1092746653" MODIFIED="1380782408326" TEXT="Objetivos clave">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1380782395630" ID="ID_1267804132" MODIFIED="1383127130785" TEXT="Se traducen en hitos (Productos entregables)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1374653825748" ID="ID_200301514" MODIFIED="1374653845247">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Development-iterative.gif" />
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1369999025446" ID="ID_542694361" MODIFIED="1385574726135" TEXT="DSBC (Desarrollo Software Basado en Componentes)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370072252539" ID="ID_1116476604" MODIFIED="1370072280279" TEXT="Basado en COTS (Commercial-Off-The-Shelf)"/>
<node CREATED="1370072395343" ID="ID_831132012" MODIFIED="1370072400283" TEXT="Ventajas">
<node CREATED="1370072400283" ID="ID_267981106" MODIFIED="1370072409955" TEXT="Reducci&#xf3;n de tiempos y costes"/>
<node CREATED="1370072410943" ID="ID_1685103000" MODIFIED="1370072416951" TEXT="Aumento de la fiabilidad"/>
</node>
<node CREATED="1370072419125" ID="ID_111676652" MODIFIED="1370072428090" TEXT="Inconvenientes">
<node CREATED="1370072428090" ID="ID_1322926021" MODIFIED="1378200186423" TEXT="Incompatibilidad ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370072454475" ID="ID_1786864799" MODIFIED="1378200187846" TEXT="Inflexibilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370072463032" ID="ID_1488414132" MODIFIED="1378200188926" TEXT="Complejidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1369999031631" ID="ID_1275176738" MODIFIED="1384604497394" TEXT="Programaci&#xf3;n Extrema">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Es necesario profundizar en este punto
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="help"/>
<node CREATED="1376581875420" ID="ID_1055576852" MODIFIED="1383127604901" TEXT="Planning Game">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376581891154" ID="ID_1834251895" MODIFIED="1376581906237" TEXT="Especificaci&#xf3;n de las caracter&#xed;sticas deseadas"/>
<node CREATED="1376581919085" ID="ID_1580581793" MODIFIED="1376581935174" TEXT="Se determinan los factores m&#xe1;s importantes"/>
</node>
<node CREATED="1376558705240" ID="ID_691502535" MODIFIED="1383127608989" TEXT="Planificaci&#xf3;n de entregas, normalmente cada dos semanas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1376558717995" ID="ID_940922240" MODIFIED="1383127612094" TEXT="Planificaci&#xf3;n de iteraciones o sprints">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1376558733052" ID="ID_1626321782" MODIFIED="1383127614206" TEXT="Planificaci&#xf3;n diaria">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1376581987811" ID="ID_1364493654" MODIFIED="1383127616861" TEXT="Realimentaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376581992642" ID="ID_1686817159" MODIFIED="1383127619301" TEXT="Del cliente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376581998329" ID="ID_1626304483" MODIFIED="1383127621053" TEXT="Del equipo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</map>
