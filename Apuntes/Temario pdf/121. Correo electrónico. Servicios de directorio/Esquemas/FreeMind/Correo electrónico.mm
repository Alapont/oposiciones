<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1375361122600" ID="ID_894744935" MODIFIED="1378567523453" TEXT="Correo electr&#xf3;nico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375361135497" FOLDED="true" ID="ID_398320211" MODIFIED="1385159222890" POSITION="right" TEXT="Arquitectura X.400">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375361170072" ID="ID_78633483" MODIFIED="1385159157805" TEXT="MTS (Sistema de transferencia de mensajes)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375361214693" ID="ID_1486991095" MODIFIED="1381321725167" TEXT="MTA (Agente de Transferencia de Mensajes)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384242463715" ID="ID_603474356" MODIFIED="1384242494578" TEXT="MRS (Sistema de Enrutamiento de Mensajes)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375361172539" ID="ID_560400747" MODIFIED="1385159187133" TEXT="MHS (Sistema de tratamiento de mensajes)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375361225904" ID="ID_703477385" MODIFIED="1377851195156" TEXT="UA (Agente de Usuario)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375361291890" ID="ID_1412578152" MODIFIED="1377851203419" TEXT="MS (Repositorio de mensajes)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1382865892107" ID="ID_1544636704" MODIFIED="1382865914498" TEXT="RUA (Agente de Usuario Remoto) ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375361228219" ID="ID_1378732602" MODIFIED="1375361231346" TEXT="U"/>
<node CREATED="1377851001582" ID="ID_568401363" MODIFIED="1377851197100" TEXT="DL (Listas de distribuci&#xf3;n)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1377851040744" ID="ID_1821594980" MODIFIED="1385159190596" TEXT="AU (Access Units)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377851054389" ID="ID_1989971316" MODIFIED="1377851073256" TEXT="Permiten el enlace a sistemas de correo diferentes"/>
<node CREATED="1381321697694" ID="ID_756199198" MODIFIED="1381321723036" TEXT="Proceso de aplicaci&#xf3;n que actua con el MTA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1377851118378" FOLDED="true" ID="ID_264533646" MODIFIED="1385159213093" TEXT="Mensajes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377851127214" ID="ID_883883885" MODIFIED="1377851186236" TEXT="ME (envelope)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377851157456" ID="ID_1391514788" MODIFIED="1377851161166" TEXT="Normal (0)"/>
<node CREATED="1377851161631" ID="ID_933464511" MODIFIED="1377851167174" TEXT="No urgente (1)"/>
<node CREATED="1377851167615" ID="ID_1744663312" MODIFIED="1377851173105" TEXT="Urgente (2)"/>
<node CREATED="1377851233643" ID="ID_91089721" MODIFIED="1377851240010" TEXT="Protocolo P1"/>
</node>
<node CREATED="1377851145680" ID="ID_1303566767" MODIFIED="1377851187932" TEXT="MC (contenido)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377851277081" ID="ID_450791279" MODIFIED="1384864654945" TEXT="Cabecera (BNF)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377851286248" ID="ID_1747047711" MODIFIED="1377851296663" TEXT="Contenido"/>
<node CREATED="1377851289368" ID="ID_382122952" MODIFIED="1377851292951" TEXT="Adjuntos"/>
<node CREATED="1377851299200" ID="ID_229626521" MODIFIED="1377851302703" TEXT="Protocolo P2"/>
</node>
</node>
<node CREATED="1375361340350" ID="ID_146680862" MODIFIED="1377184375644" STYLE="bubble" TEXT="Lenguaje ASN.1">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375635688994" ID="ID_39811077" MODIFIED="1385159214809" TEXT="Protocolos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375635695716" ID="ID_352770718" MODIFIED="1377184379506" TEXT="P1: entre MTAs">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375635706527" ID="ID_1591403144" MODIFIED="1377184382269" TEXT="P2: entre UA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375635715607" ID="ID_332264259" MODIFIED="1377184385968" TEXT="P3: entre MTA y UA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375635744259" ID="ID_25831788" MODIFIED="1377184389551" TEXT="P7: entre UA y MS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1384157079233" FOLDED="true" ID="ID_602151094" MODIFIED="1384864962142" TEXT="MPDU">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384157088229" ID="ID_1524581546" MODIFIED="1384157120046" TEXT="Pueden ser sondas, informes o los propios mensajes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375361426009" FOLDED="true" ID="ID_825332905" MODIFIED="1385159267162" POSITION="right" TEXT="SMTP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375361429635" ID="ID_1444875313" MODIFIED="1385159129772" TEXT="RFCs">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375361434574" FOLDED="true" ID="ID_389446218" MODIFIED="1384865073939" TEXT="821: intercambio de mensajes mediante cliente-servidor">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384157275510" ID="ID_652807701" MODIFIED="1384157286978" STYLE="bubble" TEXT="STD 10">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375361438789" FOLDED="true" ID="ID_1424800471" MODIFIED="1384865080524" TEXT="822: formatos de mensaje">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375361669524" ID="ID_997074847" MODIFIED="1377184424518" STYLE="bubble" TEXT="Notaci&#xf3;n BNF">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378801379515" ID="ID_844559757" MODIFIED="1378801382604" TEXT="Cabecera"/>
<node CREATED="1378801382892" ID="ID_1487989301" MODIFIED="1378801385872" TEXT="Sobre "/>
<node CREATED="1378801386316" ID="ID_1987586857" MODIFIED="1378801388859" TEXT="Cuerpo"/>
</node>
<node CREATED="1375361445784" ID="ID_1992584076" MODIFIED="1377184420476" TEXT="974: procedimiento de enrutado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375543344291" ID="ID_1230325713" MODIFIED="1384865606394" TEXT="S&#xf3;lo permite caracteres ASCII (es un protocolo de 7 bits)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375543329039" ID="ID_1197184176" MODIFIED="1377184450709" TEXT="No permite ficheros adjuntos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384157325912" ID="ID_338712131" MODIFIED="1384865687236" STYLE="bubble" TEXT="RFC 5321: Es el usado hoy en d&#xed;a e incluye ESMTP (8 bits)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375361493685" FOLDED="true" ID="ID_1618132523" MODIFIED="1385159392368" POSITION="right" TEXT="MIME (Multipurpouse Internet Mail Extensions)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375361521731" ID="ID_327007037" MODIFIED="1377184435512" TEXT="RFCs 2045 a 2049">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378632393334" ID="ID_1044184649" MODIFIED="1378632424837" TEXT="Usado en la mayor&#xed;a de protocolos de Internet (HTTP)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384865566849" ID="ID_431544383" MODIFIED="1384865597512" TEXT="Permite un mayor n&#xfa;mero de caracteres (8 bits) ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375361734937" ID="ID_1230131783" MODIFIED="1377184443862" TEXT="Permite incluir ficheros adjuntos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375543366071" ID="ID_77392271" MODIFIED="1377184447582" TEXT="Permite multimedia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375361790035" FOLDED="true" ID="ID_420952599" MODIFIED="1385159148242" POSITION="right" TEXT="POP3">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375361808737" ID="ID_227755921" MODIFIED="1377852791892" TEXT="RFC 1939">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375361821333" ID="ID_991725149" MODIFIED="1384865731470" TEXT="Puerto 110">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375361845887" ID="ID_128254272" MODIFIED="1384865734126" TEXT="Comandos en modo caracter">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375361866895" ID="ID_523046175" MODIFIED="1384865736974" TEXT="As&#xed;ncron&#xed;a en recepci&#xf3;n de mensajes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384865896159" ID="ID_764775621" MODIFIED="1384865917108" TEXT="La gesti&#xf3;n de mensajes y carpetas es LOCAL para cada CLIENTE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375361898234" FOLDED="true" ID="ID_841818294" MODIFIED="1385159149100" POSITION="right" TEXT="IMAP4">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375361916365" ID="ID_206529706" MODIFIED="1377852795085" TEXT="RFC 2060">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375361992383" ID="ID_1613444773" MODIFIED="1384865727062" TEXT="Puerto 143">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375361926345" ID="ID_388050390" MODIFIED="1377184440171" TEXT="Puede almacenar mensajes de m&#xfa;ltiples usuarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375361965780" ID="ID_876537688" MODIFIED="1384865722854" TEXT="Puede interpretar objetos MIME multiparte">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384865859134" ID="ID_156928442" MODIFIED="1384865894854" TEXT="La gesti&#xf3;n de mensajes y carpetas (borrado, copias, etc.) se hace en el SERVIDOR">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375543377226" FOLDED="true" ID="ID_586441473" LINK="../../Vol%2002/Esquemas/Seguridad%20en%20la%20red.mm" MODIFIED="1384864476538" POSITION="right" TEXT="Seguridad">
<node CREATED="1375543380971" ID="ID_1437590025" MODIFIED="1384777931202" TEXT="S/MIME">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375543603383" ID="ID_697203895" MODIFIED="1384864473169" TEXT="PKCS #7">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375543385470" ID="ID_1471638584" MODIFIED="1377184475037" TEXT="PGP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375543454998" ID="ID_148062095" MODIFIED="1377184471634" TEXT="GPG (GNU Privacy Guard): reemplazo de PGP por GNU">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377364950849" ID="ID_482326177" MODIFIED="1377364986544" TEXT="PEM: Protocolo de seguridad extremo a extremo entre UA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375550869726" ID="ID_816926707" MODIFIED="1375550889986" POSITION="left" TEXT="OJO: El correo electr&#xf3;nico PERMITE el env&#xed;o de fax"/>
</node>
</map>
