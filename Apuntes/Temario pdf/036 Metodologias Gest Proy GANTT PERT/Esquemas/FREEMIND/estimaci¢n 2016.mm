<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1431704764211" ID="ID_703525372" MODIFIED="1434618396884">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      T89 ESTIMACI&#211;N
    </p>
  </body>
</html></richcontent>
<node CREATED="1431705153273" ID="ID_761328233" MODIFIED="1436440290367" POSITION="right" TEXT="Introducci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431705161371" ID="ID_450941000" MODIFIED="1431705722383" TEXT="Proyecto">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431705166092" ID="ID_146061119" MODIFIED="1434618416295">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Combinaci&#243;n de recursos reunidos en org temporal para conseguir un objetivo&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1431705624845" ID="ID_1131470035" MODIFIED="1431705748691" TEXT="La Planif Estrat&#xe9;gica se concreta en proyectos">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1431705185507" ID="ID_133726688" MODIFIED="1431705194917" TEXT="Gesti&#xf3;n de proyectos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431705195338" ID="ID_1019967589" MODIFIED="1431705222181" TEXT="Uso de t&#xe9;cnicas y activs para producto de alta calidad, dentro de un presup y con planif de tiempos"/>
<node CREATED="1436434176570" ID="ID_5221953" MODIFIED="1436435764309">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Elementos clave
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436434194778" ID="ID_1823614240" MODIFIED="1436440340744">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Inicio (&#225;mbito y objs)&#8594; <b>Estimaci&#243;n</b>&#160;(E,T,&#8364;)&#8594;&#160;&#160;<u>An&#225;lis Riesgos</u>&#160;(id, estim, eval y gesti&#243;n)&#8594; Planif t&#8594; Cntrl&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1431705229498" ID="ID_1768969867" MODIFIED="1434618472955">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      M&#233;trica v3 &#8594; <u>Interfaz GP</u>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1431705245149" ID="ID_704225938" MODIFIED="1436435257196">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      GP Inicio ( <b>Estimaci&#243;n</b>&#160;y Planif, <u>tras EVS</u>), GP Seg y control y GP Fin (catalogaci&#243;n)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1431705358294" ID="ID_1175978614" MODIFIED="1431705451865" TEXT="Ppio W5HH">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431705364278" ID="ID_1456504090" MODIFIED="1431705381177" TEXT="Preguntas para def caracter&#xed;sticas clave del proyecto">
<node CREATED="1431705381486" ID="ID_1313462752" MODIFIED="1434618508579">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Why (xq se desarrolla), What, When, Who (respons), Where (ubic de respons), How, How much
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1431705498713" ID="ID_174615508" MODIFIED="1431705503235" TEXT="PMBOK">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431705503561" ID="ID_1489934907" MODIFIED="1431705576883" TEXT="(Proj Manage Body of Knowledge) est&#xe1;ndar del PMInstitue acreditado por ANSI">
<node CREATED="1431705540848" ID="ID_327255266" MODIFIED="1431705567915" TEXT="Marco concept de Direcc, Norma de Direc y &#xc1;reas de conocim de gesti&#xf3;n"/>
</node>
</node>
<node CREATED="1431705862988" ID="ID_138657971" MODIFIED="1431706383204" TEXT="Estimaci&#xf3;n sw">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431705867340" ID="ID_1621375768" MODIFIED="1431706478185" TEXT="Proporciona valor a variables para la realizaci&#xf3;n de un trabajo dentro de un rango de tolerancia">
<icon BUILTIN="info"/>
<node CREATED="1431705890805" ID="ID_1216329301" MODIFIED="1431705902347" TEXT="Minimizar errores de riesgo">
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1431705913764" ID="ID_583626624" MODIFIED="1434619760616">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Predicciones:</i>&#160;esfuerzo, t de desarrollo, rrhh y/o coste
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1431706173368" ID="ID_125974557" MODIFIED="1434621205094">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Factores de riesgo:</i>&#160;complejidad del proy, tama&#241;o y <u>disponib hist&#243;rica</u>&#160;&#160;(proys anteriores)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1431705931508" ID="ID_556333489" MODIFIED="1434621312386">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Punto de partida:</i>&#160;requiere conocer algo del proyecto ( <u>estimaci&#243;n previa </u>) &#8594; convenimos <b>tama&#241;o</b>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1431709055879" ID="ID_1470911201" MODIFIED="1431709089218" TEXT="Lo obtendremos primero mediante m&#xe9;trica (PF)"/>
</node>
<node CREATED="1431706320238" ID="ID_71749713" MODIFIED="1431706328535" TEXT="Disparadores de coste (drivers)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431706329005" ID="ID_1806271226" MODIFIED="1431706362449" TEXT="Influyen en la estimaci&#xf3;n &#x2192;  qu&#xe9; producto, con qu&#xe9; se produce, qui&#xe9;n, c&#xf3;mo se organiza, para qui&#xe9;n"/>
</node>
<node CREATED="1431706374238" ID="ID_1307013026" MODIFIED="1431706380544" TEXT="Marco temporal de estimaci&#xf3;n">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431706385181" ID="ID_313787283" MODIFIED="1434619831890">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cuanto +se avanza, +se conoce (estim +precisa)
    </p>
  </body>
</html></richcontent>
<node CREATED="1431706398838" ID="ID_1080256646" MODIFIED="1431706462750">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reco: 2 estimaciones (al <u>comienzo</u>, y establecidos los <u>reqs</u>)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1431706663682" ID="ID_349543816" MODIFIED="1436436176347" POSITION="right" TEXT="M&#xe9;tricas sw">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431706691233" ID="ID_949215994" MODIFIED="1431706724787" TEXT="App cont&#xed;nua de t&#xe9;cnicas basadas en medidas de proceso de desarrollo y productos">
<icon BUILTIN="info"/>
<node CREATED="1431706714082" ID="ID_978181358" MODIFIED="1434619912210">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Producir info de gesti&#243;n y obtener&#160; <u>tama&#241;o</u>&#160;como punto de partida&#160;
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1431709010285" ID="ID_1975919850" MODIFIED="1431709013830" TEXT="Orientaciones">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431706739457" ID="ID_844410986" MODIFIED="1431709022375" TEXT="Tama&#xf1;o">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431706750122" ID="ID_1420298906" MODIFIED="1431707473445" TEXT="Medidas directas/objetivas (mismo valor siempre, concretas)">
<node CREATED="1431707136301" ID="ID_1820783449" MODIFIED="1431707254047" TEXT="Requiere disponer de info hist&#xf3;rica">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1431706778937" ID="ID_976824434" MODIFIED="1431707151144" TEXT="L&#xed;neas de C&#xf3;digo (LDC)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431707151134" ID="ID_1822584414" MODIFIED="1431707180944" TEXT="No comentario ni l&#xed;nea blanco">
<node CREATED="1431707189620" ID="ID_909089035" MODIFIED="1434620117477">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No considera reutiliz, costes fijos ni tareas q no producen instruccs
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
</node>
<node CREATED="1431706802236" ID="ID_656630120" MODIFIED="1431709029967" TEXT="Funci&#xf3;n">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431706807332" ID="ID_1571056180" MODIFIED="1431706845564" TEXT="Medidas indirectas/subjetivas (valor seg&#xfa;n juicio personal, abstractas)">
<node CREATED="1431706835979" ID="ID_1150739476" MODIFIED="1436435658204">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Tama&#241;o</u>&#160;a partir de la funcionalidad, utilidad o&#160; <u>complejidad</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1431706964622" ID="ID_824251992" MODIFIED="1434706205748">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Punto de Funci&#243;n (PF, FPA)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431707308729" ID="ID_1480385460" MODIFIED="1431708938970">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Medida emp&#237;rica, medible en primeros pasos y tb mide<u>tama&#241;o</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1431707327953" ID="ID_609479759" MODIFIED="1434620062622" TEXT="Requiere formaci&#xf3;n, tiempo e inversi&#xf3;n">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1431707344547" ID="ID_341315000" MODIFIED="1434620237282" TEXT="Indep del entorno, metodolog&#xed;a y pgmaci&#xf3;n; permite pasar luego a LDC (mediante tablas)">
<icon BUILTIN="button_ok"/>
<node CREATED="1436435134139" ID="ID_850600812" MODIFIED="1436435140693" TEXT="M&#xe9;trica v3">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
<node CREATED="1431706864483" ID="ID_1434562939" MODIFIED="1431709035999" TEXT="Persona">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431706872738" ID="ID_1229975838" MODIFIED="1431706893982" TEXT="Informan sobre forma en que se desarrolla el sw y efectividad de herramientas o m&#xe9;todos"/>
</node>
</node>
<node CREATED="1431707501000" ID="ID_595632253" MODIFIED="1431708989601" TEXT="M&#xe9;todos de PF">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431707508760" ID="ID_349092250" MODIFIED="1431707670807" TEXT="Albretch">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431707515551" ID="ID_1382563249" MODIFIED="1436434110222">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Tradicional) Medidas <u>contables</u>&#160;de info del sw y valoraciones&#160; <u>subjetivas</u>&#160;de su <u>complejidad</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
</node>
<node CREATED="1431707657669" ID="ID_1402359890" MODIFIED="1434620781199">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1. N&#186; elementos de info del sw
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431707680453" ID="ID_1819792719" MODIFIED="1434622294739">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ficheros l&#243;g int (ILF):</i>&#160;&#160;dentro de una app
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1431707728509" ID="ID_1510136615" MODIFIED="1434622412300">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ficheros de interfaz ext (EIF):</i>&#160;mantenidos por otra app, tx info a otro s&#170;
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1431707800380" ID="ID_1450762814" MODIFIED="1431707842769">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Entradas ext (EI):</i>&#160;datos/info control que se introducen en la app, a&#241;aden/cambian info del ILF
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1431707888218" ID="ID_1818172424" MODIFIED="1434620476685">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Salidas ext (EO):</i>&#160;mensajes de error, gr&#225;ficos o informes
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1431707915416" ID="ID_968105535" MODIFIED="1431707944726">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Consultas ext (EQ):</i>&#160;peticiones de info a la app (pe ayudas)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1431707995212" ID="ID_776453072" MODIFIED="1434620566170">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2. Valoraci&#243;n de complejidad:</i>&#160;baja, media o alta para cada elemento, seg&#250;n tablas predefs
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1431708060720" ID="ID_197399884" MODIFIED="1434620651175">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>3. Factores de ponderaci&#243;n:</i>&#160;para cada grado de complejidad (tablas predef)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1431708078616" ID="ID_1468439810" MODIFIED="1434620799801">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>4. C&#225;lculo de PF No ajustados (PFNA): </i>se suman los productos obtenidos
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1431708102735" ID="ID_1836400473" MODIFIED="1434621028433" TEXT="5. C&#xe1;lculo de PF">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1431708108495" ID="ID_1628208454" MODIFIED="1434622616547">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      PF = sumPFNA x Factor de Ajuste&#8594; FA=0,65+0,01x(sum Fi)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1431708137902" ID="ID_653211844" MODIFIED="1431708215693">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Fi:</i>&#160;<b>14</b>&#160;valores de ajuste de complejidad seg&#250;n caracts generales del s&#170;
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1431708295359" ID="ID_1219096687" MODIFIED="1431708299064" TEXT="MARK II">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431708299455" ID="ID_1928000118" MODIFIED="1431708454547" TEXT="Por Symons, critica a Albretch (demasiado simple)">
<icon BUILTIN="info"/>
<node CREATED="1431708327130" ID="ID_753394633" MODIFIED="1436436000671">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Elementos sw: </i>transaccs&#160;tipo&#160;l&#243;gicas&#8594; <u>funciones</u>&#160; (E/Proceso/S), Entidad en lugar de fichero
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1431708455152" ID="ID_81880897" MODIFIED="1434622568867">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1. C&#225;lculo de PFNA para cada funci&#243;n
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431708471469" ID="ID_1272407256" MODIFIED="1434620930273">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      PFNA = Ne x1,66 + Ni x0,58 +No x0,26
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1431708520810" ID="ID_1641631899" MODIFIED="1431708576762" TEXT="Ne (Entidades-tipo referenciadas), Ni (n&#xba; tipos de datos E), No (datos S)"/>
</node>
<node CREATED="1431708689850" ID="ID_1189008905" MODIFIED="1434621031138">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      2. C&#225;lculo de PF
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1431708108495" ID="ID_1211818446" MODIFIED="1434622621771">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      PF = sumPFNA x Factor de Ajuste&#8594; FA=0,65+0,005x(sum Fi)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1431708137902" ID="ID_985273120" MODIFIED="1431708728982">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Fi:</i>&#160;<b>19</b>&#160;valores de ajuste de complejidad (14 de Albretch + 5 nuevos)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1431708780150" ID="ID_708830028" MODIFIED="1431708782600" TEXT="Batch">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431708783013" ID="ID_449813700" MODIFIED="1436434169209">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si s&#170; tiene parte batch y online, se calculan&#160; <u>por separado</u>&#160;y se suman (FA seg&#250;n m&#233;todo escogido)
    </p>
  </body>
</html></richcontent>
<node CREATED="1431708824719" ID="ID_1532003378" MODIFIED="1431708835449" TEXT="PF= PF (batch) + PF (online)"/>
</node>
</node>
</node>
</node>
<node CREATED="1431708842491" ID="ID_952707046" MODIFIED="1436436113147" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      T&#233;cnicas
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431709125846" ID="ID_688359785" MODIFIED="1434621140468">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Una vez tenemos PF/LDC
    </p>
  </body>
</html></richcontent>
<node CREATED="1431709159320" ID="ID_1474961204" MODIFIED="1431709230754" TEXT="Ecuaciones (modelo emp&#xed;rico) &#x2192; Descomposi&#xf3;n de tareas">
<node CREATED="1431709748436" ID="ID_1954221193" MODIFIED="1434621152117">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>M&#233;todo exitoso:</i>&#160;&#160;desv del coste final &lt;30%, permite refinar durante CV, f&#225;cil, herrams y docum
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1431709303751" ID="ID_1319107416" MODIFIED="1434705324976">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modelos emp&#237;ricos
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431709309199" ID="ID_1986105145" MODIFIED="1431709357933" TEXT="Ecuaciones a partir de proyectos terminados para hacer predicciones">
<icon BUILTIN="info"/>
</node>
<node CREATED="1431709370641" ID="ID_18479912" MODIFIED="1434622768988">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Clasificaciones
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431709379154" ID="ID_1460966470" MODIFIED="1434621095958" TEXT="Estad&#xed;sticos (Walston-Felix), teor&#xed;as (Putnam) o compuestos (intuici&#xf3;n+estad&#xed;stico+expertos:COCOMO)  "/>
<node CREATED="1431709454045" ID="ID_1608500465" MODIFIED="1431709473307">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lineales, exponenciales, anal&#237;ticos (con funciones) o tabulares (tablas)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1431709475028" ID="ID_569997077" MODIFIED="1434622785596">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pressman
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431709504373" ID="ID_1849735831" MODIFIED="1431709576916" TEXT="De recursos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431709577578" ID="ID_383007855" MODIFIED="1434701339808" TEXT="Simple-variable est&#xe1;ticos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1431709591851" ID="ID_1269826110" MODIFIED="1434705404736">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estimaci&#243;n= c1 x (tama&#241;o proy)^c2 (c: ctes seg&#250;n&#160;&#160;<u>datos hist&#243;ricos </u>)
    </p>
  </body>
</html></richcontent>
<node CREATED="1434701286041" ID="ID_1617596251" MODIFIED="1434705415085" TEXT="COCOMO"/>
</node>
</node>
<node CREATED="1431709680480" ID="ID_68472970" MODIFIED="1434701340994" TEXT="Multivariable est&#xe1;ticos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431709690475" ID="ID_1612791057" MODIFIED="1431709704389">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estimaci&#243;n= c11 x e1 + c21 x e2 +.. (ei: caract i-&#233;sima del sw)&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1431709706969" ID="ID_431749468" MODIFIED="1434701342057" TEXT="Multivariable din&#xe1;micos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431709711213" ID="ID_1664739611" MODIFIED="1434701313014">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Proyectan reqs de recursos como funci&#243;n del tiemp
    </p>
  </body>
</html></richcontent>
<node CREATED="1434701313016" ID="ID_957500777" MODIFIED="1434701326640">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Putman (SLIM)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1431709520812" ID="ID_369929246" MODIFIED="1431709576525" TEXT="De PF">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431709566035" ID="ID_423284983" MODIFIED="1431709573150">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      De recursos simple-variable est&#225;ticos basados en medida del PF
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1431709522501" ID="ID_1549252348" MODIFIED="1431709575973" TEXT="De estudio temporal">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431709538922" ID="ID_1378672456" MODIFIED="1434622890872" TEXT="Mide trabajo &#xfa;til por jornada y persona (Esterling)"/>
</node>
</node>
</node>
</node>
<node CREATED="1434704466634" ID="ID_1958720028" MODIFIED="1434705351225">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Staffing Size (Lorentz y Kidd)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434704481269" ID="ID_1140475986" MODIFIED="1434704508417">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      M&#233;tricas para estimar personas necesarias en desarrollo <u>OO</u>&#160;y su t de particip
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1434705315038" ID="ID_1157237733" MODIFIED="1436436114789" POSITION="right" TEXT="Modelos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434701251327" ID="ID_70443396" MODIFIED="1434702602321" TEXT="COCOMO">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434701257890" ID="ID_750326002" MODIFIED="1434705622688">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Boehm) Modelo Constructivo de Costes, matem&#225;tico/algor&#237;tm a partir de&#160; <u>LDC</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1434701466914" ID="ID_1475028442" MODIFIED="1434705631294">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Desarrollos MCV en <u>cascada</u>: desde An&#225;lisis Funcional a Implantaci&#243;n (no contempla fases previas)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1434701541105" ID="ID_593812793" MODIFIED="1434701653359">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sol&#8594; <i>Distrib de esfuerzo por fases:</i>&#160;a partir de tablas, seg&#250;n versi&#243;n y complejidad
    </p>
  </body>
</html></richcontent>
<node CREATED="1434701568281" ID="ID_15533062" MODIFIED="1434701703501" TEXT="Tb calcula cu&#xe1;nto hay que imputar de m&#xe1;s por fases no contempladas">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1434704419317" ID="ID_127591100" MODIFIED="1434704452045">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Regla 40-20-40:</i>&#160;ASI+DSI (40%), CSI (20%), Pruebas+depuraci&#243;n (40%)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1434701713136" ID="ID_768683475" MODIFIED="1434701725550" TEXT="Modos de complejidad">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434701725933" ID="ID_1122677710" MODIFIED="1436435928563">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Org&#225;nico (&lt;50KLDC), Semiacoplado (300KLDC) o Empotrado (complejo, acople hw-sw,&#160;<u>reqs r&#237;gidos</u>)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1434701816374" ID="ID_977991965" MODIFIED="1434701828942" TEXT="Versiones">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434701819766" ID="ID_1999831092" MODIFIED="1434701828061" TEXT="B&#xe1;sico">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434701854825" ID="ID_1209190807" MODIFIED="1434701880081" TEXT="Estimaciones r&#xe1;pidas pero precisi&#xf3;n muy limitada (30%)">
<node CREATED="1434701880082" ID="ID_255385649" MODIFIED="1434701995639" TEXT="E = a&#xb7;KLDC^b (personas-mes) / T=c&#xb7;E^d (meses)">
<node CREATED="1434702023906" ID="ID_145382199" MODIFIED="1436435873138" TEXT="Ctes seg&#xfa;n versi&#xf3;n y complej (tabla)"/>
<node CREATED="1436435562585" ID="ID_1797729698" MODIFIED="1436435580088" TEXT="1 mes-hombre =152h-hombre"/>
</node>
</node>
</node>
<node CREATED="1434702105827" ID="ID_559593563" MODIFIED="1434702108981" TEXT="Intermedio">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434702109451" ID="ID_75527591" MODIFIED="1434702268095">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tienen en cuenta <u>15 factores</u>&#160;de eval subjetiva (Mi, tablas) y +precisi&#243;n (70%)
    </p>
  </body>
</html></richcontent>
<node CREATED="1434702133824" ID="ID_1149151287" MODIFIED="1434702227848">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      E=a&#183;KLDC^b&#183;<b>FAE</b>&#160;/ Factor Ajuste Esf = &#928; Mi (Multiplics de Esf)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1434702277648" ID="ID_220092888" MODIFIED="1434702279865" TEXT="Avanzado">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434702307254" ID="ID_1855498724" MODIFIED="1434702405143">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Particiona proy en <u>fases</u>&#160;para estimar esfuerzo y jerarquiza los Mi
    </p>
  </body>
</html></richcontent>
<node CREATED="1434702367773" ID="ID_1517565651" MODIFIED="1434702383299" TEXT="Muy complejo, grandes proys"/>
</node>
</node>
</node>
</node>
<node CREATED="1434702602906" ID="ID_795981309" MODIFIED="1434702608647" TEXT="COCOMO II-2000">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434702609453" ID="ID_945721781" MODIFIED="1434702730418">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mejora COCOMO intermedio. Parte de <u>PF</u>&#160;(luego pasa a LDC) y no interviene modo de complejidad
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1434702688896" ID="ID_1637389152" MODIFIED="1434702705337" TEXT="Modelo de Dise&#xf1;o Preliminar (EDM)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434702705791" ID="ID_875420688" MODIFIED="1434703421843">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      En fase inicial, con <u>5</u>&#160;facts de <u>escala</u>&#160;y <u>7</u>&#160;elemnts d <u>ajuste</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1434703261127" ID="ID_1031265891" MODIFIED="1434703432766">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      E=A&#183;KLDC^<b>B&#183;FA</b>&#160;; B: sum(FE); FA: &#928; EA / T=c&#183;E^d&#183;<b>SCED</b>&#160; (Esf de calend)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1434703442808" ID="ID_1796219232" MODIFIED="1434703450356" TEXT="Modelo Post-Arq (PAN)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434703450739" ID="ID_349661383" MODIFIED="1434703477761">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      En fase DSI, eqs an&#225;logas a EDM pero con <u>17</u>&#160;elementos de <u>ajuste</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1434702739189" ID="ID_1311700797" MODIFIED="1434702748730" TEXT="Modelo de Composici&#xf3;n de App (AVM)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434702749520" ID="ID_1143936596" MODIFIED="1434703034183">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Para apps de<u>prototipado</u>&#160;evolutivo (RAD) y basadas en generadores de pantallas
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1434702798243" ID="ID_802093866" MODIFIED="1434703508423">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Puntos de Objeto</b>&#160;(PO)
    </p>
  </body>
</html></richcontent>
<node CREATED="1434702848190" ID="ID_903882037" MODIFIED="1434702930150">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se estiman como PF en Albretch, asume que sw se compone de <u>pantallas, informes y componentes</u>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1434702935889" ID="ID_1891642278" MODIFIED="1434702991498" TEXT="PO Ajustados = PONA&#xb7;FR / Factor Reut = 100-r/100; r: %objetos reutiliz"/>
<node CREATED="1434703000874" ID="ID_954718203" MODIFIED="1434703023491" TEXT="E=PO/PROD; PROD: productividad del equipo (PO/personas-es, seg&#xfa;n tabla)"/>
</node>
</node>
</node>
<node CREATED="1434703701042" ID="ID_426490936" MODIFIED="1434703727283" TEXT="Putnam (SLIM)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434703756042" ID="ID_647614839" MODIFIED="1434703800582" TEXT="Basado en teor&#xed;as, multivar din&#xe1;mico para grandes proy">
<node CREATED="1434703811581" ID="ID_329789162" MODIFIED="1434704367967" TEXT="Parte de Prod (LDC-PF)=cte (Factor de proporc)&#xb7;E&#xb7;T"/>
</node>
<node CREATED="1434704056575" ID="ID_1766404986" MODIFIED="1434704190092">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Establece Distrib de <b>Rayleigh</b>&#160;&#8594; P=c&#183;E^1/3&#183;T^4/3 &#8594; peqs cambios en T provocan grandes en E
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1434704133668" ID="ID_925421459" MODIFIED="1436435807655" TEXT="Ajuste: P=PP&#xb7;(E/B)^1/3&#xb7;T^4/3 ; PP: productiv; B: habilidad seg&#xfa;n tama&#xf1;o"/>
</node>
<node CREATED="1434704233880" ID="ID_517544230" MODIFIED="1434704350809" TEXT="En cada punto del proy&#x2192; E(t)=2&#xb7;K&#xb7;a&#xb7;t&#xb7;e^-[a&#xb7;(t^2)]; K: esf total, &#xe1;rea; a: entrada de personas, tangente"/>
</node>
<node CREATED="1434704810411" ID="ID_1843161586" MODIFIED="1434704820816" TEXT="Kartner">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434704882408" ID="ID_581694123" MODIFIED="1434704906438">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estima a partir de Puntos de <u>Casos de Uso</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1434704896484" ID="ID_875941028" MODIFIED="1434705006224">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Asigna complejidad a cada caso seg&#250;n transacs y a cada actor seg&#250;n su tipo
    </p>
  </body>
</html></richcontent>
<node CREATED="1434704980302" ID="ID_284374444" MODIFIED="1434704998079" TEXT="13 factores t&#xe9;cns y 8 ambientales (seg&#xfa;n personas)"/>
</node>
</node>
</node>
</node>
<node CREATED="1434704718182" ID="ID_978245173" MODIFIED="1436436116032" POSITION="right" TEXT="Herramientas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434704721686" ID="ID_1346066733" MODIFIED="1434704797155">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CA-ESTIMACS (macro, PF), SLIM (Putnam), COSTAR (COCOMO), Before You Leap (PF y COCOMO)&#160;&#160;
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1434705749697" ID="ID_813436530" MODIFIED="1434705760232" TEXT="SISDEL">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<node CREATED="1434705756802" ID="ID_161051461" MODIFIED="1434705803653" TEXT="S&#xaa; Integrado de Soporte al Desarrollo de Equipos L&#xf3;gicos (AAPP)">
<node CREATED="1434705826911" ID="ID_234861983" MODIFIED="1434705870939">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>EDIM: </i>Estimaci&#243;n y Dimensionamiento del proy
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1434705871655" ID="ID_1601160499" MODIFIED="1434705964339">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>PPLAN: </i>Planificiaci&#243;n del proy
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1434705896909" ID="ID_137202110" MODIFIED="1434705908370">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>GCAL:</i>&#160;Plan de Garant&#237;a de Calidad
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1434706513803" ID="ID_641732230" MODIFIED="1434706519054" TEXT="Mejora la productividad">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</node>
</node>
</map>
