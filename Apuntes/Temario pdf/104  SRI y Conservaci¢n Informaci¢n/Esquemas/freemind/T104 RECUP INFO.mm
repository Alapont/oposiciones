<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1433517132408" ID="ID_593650334" MODIFIED="1480688614359" TEXT="T100 RECUP INFO">
<node CREATED="1433517157742" ID="ID_474741593" MODIFIED="1433773606251" POSITION="right" TEXT="Introducci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433517179917" ID="ID_15777738" MODIFIED="1433518062331">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Gesti&#243;n eficiente y efectiva de gran vol datos para recup <u>info pertinente</u>&#160;&#160;r&#225;pida y sencilla
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1433517217600" ID="ID_1549865135" MODIFIED="1433524592641" TEXT="Representaci&#xf3;n, almacenam, org y acceso a elementos de info">
<icon BUILTIN="xmag"/>
<node CREATED="1433524624474" ID="ID_1161730954" MODIFIED="1433524648996" TEXT="Info guardada en SGDocumental">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1433517276385" ID="ID_67206221" MODIFIED="1433524532643">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>S&#170; RI:</i>&#160;id de docus relevantes de una colecci&#243;n bajo consulta (pe Google)
    </p>
  </body>
</html></richcontent>
<node CREATED="1433517315456" ID="ID_744478759" MODIFIED="1433524738780">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Info <u>no estructurada</u>, en leng natural y recup probabil&#237;stica&#160;(al contrario que SGBDR)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1433517405177" ID="ID_281274209" MODIFIED="1433521287950">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Visi&#243;n l&#243;gica de docs:</i>&#160;&#160;se representan con&#160;<u>t&#233;rminos de &#237;ndice</u>&#160;(TI, keywords) y pesos relativos
    </p>
  </body>
</html></richcontent>
<node CREATED="1433517430085" ID="ID_569722453" MODIFIED="1433517485109">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      De texto completo (a partir de todas las palabras que contiene el doc) o filtrado
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1433520567056" ID="ID_1851523225" MODIFIED="1433773603922" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: left">
      <b>Indexaci&#243;n y recup autom&#225;tica</b>
    </p>
    <p style="text-align: left">
      (T&#233;cnicas previas)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1433521093064" ID="ID_787240145" MODIFIED="1433772035578">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1&#186;. Crear&#160;fichero invertido (&#237;ndices invertidos)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1433521149408" ID="ID_171941941" MODIFIED="1433772077351">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tabla con <u>Vocabulario</u>&#160;(TI, diccionario) y&#160; <u>Ocurrencias</u>&#160;(listas de docs donde aparece cada TI y n&#186;veces)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1433521248757" ID="ID_1549974533" MODIFIED="1433521263065" TEXT="B&#xfa;squeda muy eficiente, compara consultas con ocurrencias de cada TI">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1433521295394" ID="ID_812205088" MODIFIED="1433521316790" TEXT="Espacio necesario para almacenar fichero invertido y revisiones en cada modific">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1433521108265" ID="ID_801465505" MODIFIED="1433771591594">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      2&#186;. App t&#233;cnicas de leng natural
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433521427942" ID="ID_1379482871" MODIFIED="1433521448438" TEXT="Eliminaci&#xf3;n de palabras que no aportan riqueza (art&#xed;culos, pronombres...)">
<node CREATED="1433521337139" ID="ID_778284560" MODIFIED="1433521380323">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reduce espacio de fichero inverso&#8594; indexa <u>solo TI de utilidad</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1433521469990" ID="ID_1175063051" MODIFIED="1433771429387">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Index sint&#225;ctica:</i>&#160;por sintagmas (<u>multipalabra</u>)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1433521449300" ID="ID_81210567" MODIFIED="1433521589477">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Index morfol&#243;gica:</i>&#160;&#160;stemming&#8594; reduce palabras a su <u>ra&#237;z</u>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1433521485074" ID="ID_1831267049" MODIFIED="1433521606816">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Index sem&#225;ntica:</i>&#160;por <u>comprensi&#243;n</u>&#160;de palabras (mediante diccionarios y redes)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1433517642680" ID="ID_205088665" MODIFIED="1448644624752" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: left">
      <b>Modelaci&#243;n de </b>
    </p>
    <p style="text-align: left">
      <b>docs y consultas</b>
    </p>
    <p style="text-align: left">
      (aprox estad&#237;stica)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1433517702167" ID="ID_866904943" MODIFIED="1433519461562" TEXT="Cl&#xe1;sicos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1433517812954" ID="ID_453679812" MODIFIED="1433517815980" TEXT="Booleano">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433517817396" ID="ID_703630447" MODIFIED="1433525289824">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Basado en teor&#237;a de conjuntos: consulta por AND, OR, NOT
    </p>
  </body>
</html></richcontent>
<node CREATED="1433517831417" ID="ID_1402491770" MODIFIED="1433525255635">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Poco intuitivo y doc relevante solo si cumple condics (no hay grados)&#8594; no establece orden (ranking)&#160;
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1433518331316" ID="ID_1898256994" MODIFIED="1433771696171">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>C&#225;lculo de pesos de TI de consultas</i>&#8594; <i>Binario:</i>&#160;1 (si TI est&#225; presente en doc) &#243;&#160;0 (ausente)
    </p>
  </body>
</html></richcontent>
<node CREATED="1433518949029" ID="ID_1905696226" MODIFIED="1448644392580">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No contempla coincidencia parcial y no tiene en cuenta el contexto
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node CREATED="1433518009963" ID="ID_984028147" MODIFIED="1433518018044" TEXT="Vectorial">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433518097205" ID="ID_18899827" MODIFIED="1433518205424">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cada doc y consulta es un vector formado por pesos de sus TI
    </p>
  </body>
</html></richcontent>
<node CREATED="1433518144935" ID="ID_657002830" MODIFIED="1433525456177" TEXT="No binario: contempla coincidencias parciales">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1433518164177" ID="ID_747662158" MODIFIED="1433525483748">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Doc relevante seg&#250;n su similitud con vector consulta&#8594; orden de resultados por relevancia
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1433518313030" ID="ID_1614082093" MODIFIED="1433518325562" TEXT="Asume que TI son indep&#x2192; posibles errores en resultados">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1433518331316" ID="ID_1360051006" MODIFIED="1433520322769">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>C&#225;lculo de pesos</i>&#8594; <i>TF&#160;</i>(freq de aparici&#243;n de TI en doc)<i>x IDF </i>(en todos los doc)
    </p>
  </body>
</html></richcontent>
<node CREATED="1448644235115" ID="ID_248465174" MODIFIED="1448644303711">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>IDF: </i>(freq inv docum) n&#186; docs totales / n&#186; docs con TI
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1433518409541" ID="ID_1708469538" MODIFIED="1448644373727" TEXT="Mejores TI: aparecen muchas veces en un doc pero no en resto">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1433518591590" ID="ID_1250198227" MODIFIED="1433518624504">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Probabil&#237;stico
    </p>
    <p>
      (Binary Indep Retrieval)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433518621406" ID="ID_852724618" MODIFIED="1433525893631">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Basado en teorema <u>Bayes</u>: importancia de un TI seg&#250;n aparici&#243;n en docs relevantes y resto
    </p>
  </body>
</html></richcontent>
<node CREATED="1433518714451" ID="ID_1767680817" MODIFIED="1433519100298" TEXT="Binario: considera solo presencia/ausencia de TI en doc">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1433518765119" ID="ID_21501898" MODIFIED="1433518843879">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>C&#225;lculo de pesos&#8594; hip&#243;tesis inicial:</i>&#160;&#160;prob de ser buen/mal descriptor
    </p>
  </body>
</html></richcontent>
<node CREATED="1433518854817" ID="ID_591674851" MODIFIED="1433518877375" TEXT="Realimentaci&#xf3;n: aprendizaje con usuario y reasignaci&#xf3;n de nuevos pesos"/>
</node>
</node>
</node>
<node CREATED="1433517705585" ID="ID_1067380669" MODIFIED="1433517713190" TEXT="Avanzados">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433519242282" ID="ID_1913442176" MODIFIED="1448644428515">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Teor&#237;as de
    </p>
    <p>
      conjuntos
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433519270067" ID="ID_278187100" MODIFIED="1433525655435">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Evitar criterio binario del modelo booleano para permitir <u>coincidencias parciales y ordenaci&#243;n</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
</node>
<node CREATED="1433519255112" ID="ID_331198543" MODIFIED="1433519259050" TEXT="L&#xf3;gica borrosa">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433519298531" ID="ID_593949424" MODIFIED="1448644440586">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cada TI se asocia a un conj borroso, y cada doc tiene un grado de pertenencia a dicho conjunto
    </p>
  </body>
</html></richcontent>
<node CREATED="1448644407271" ID="ID_780914555" MODIFIED="1448644433133" TEXT="Establece grados de verdad, imita l&#xf3;g humana"/>
<node CREATED="1433519477080" ID="ID_121412263" MODIFIED="1433519481365" TEXT="App: investigaci&#xf3;n"/>
</node>
<node CREATED="1433519341867" ID="ID_547122534" MODIFIED="1433519396565">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Pr&#243;x</i>&#160;<i>entre dos TI:</i>&#160;calcula factor correlaci&#243;n (seg&#250;n n&#186; docs que tiene cada TI y docs que tienen ambos)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1433519259585" ID="ID_1729505041" MODIFIED="1433519263819" TEXT="Booleano extendido">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433519401645" ID="ID_1397524564" MODIFIED="1433525662504">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Rebaja condiciones para pertenecer al conjunto resultado, combina modelo vect y bool
    </p>
  </body>
</html></richcontent>
<node CREATED="1433519466965" ID="ID_1111325139" MODIFIED="1433519474340" TEXT="Potente pero muy complejo">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1433519434549" ID="ID_1883382673" MODIFIED="1433519457551">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Normas-p:</i>&#160;permiten refinamiento adicional sobre rigor de c&#225;lculo
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1433519507490" ID="ID_66124148" MODIFIED="1433519511333" TEXT="Algebraicos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433519514524" ID="ID_324338425" MODIFIED="1433519594302" TEXT="Vectorial generalizado">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433519623738" ID="ID_770581858" MODIFIED="1433519696825">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Utiliza&#160; <u>Minterms</u>&#160;(vectores ortogonales) para representar dep entre TI y les asigna pesos
    </p>
  </body>
</html></richcontent>
<node CREATED="1433519599577" ID="ID_1191835109" MODIFIED="1433519681806" TEXT="Corrige hip&#xf3;tesis de que TI son indep">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1433519704866" ID="ID_878083853" MODIFIED="1433525688250" TEXT="Coste computacional">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node CREATED="1433519519502" ID="ID_974704263" MODIFIED="1433519594790" TEXT="Index por Sem&#xe1;ntica Latente (LSI)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433519776617" ID="ID_1620182134" MODIFIED="1433519867016">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Representa docs y consultas con <u>conceptos</u>&#160;e ideas (no TI) &#8594; espacio de -dimensiones
    </p>
  </body>
</html></richcontent>
<node CREATED="1433519801881" ID="ID_1572949743" MODIFIED="1433519813034" TEXT="Requiere aprendizaje autom"/>
</node>
</node>
<node CREATED="1433519527440" ID="ID_357691374" MODIFIED="1433519595214" TEXT="Redes neuronales">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433519879580" ID="ID_850127212" MODIFIED="1433519915791" TEXT="Simula cerebro humano para reconocer patrones">
<node CREATED="1433519916636" ID="ID_631984860" MODIFIED="1433519953062">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cada neurona es una <u>unidad de proceso</u>, conectadas por arcos (pesos) a trav&#233;s de&#160;capas
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1433519531089" ID="ID_104829974" MODIFIED="1433519533955" TEXT="Bayesanos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433519990984" ID="ID_1113368081" MODIFIED="1433520012680" TEXT="Evitar c&#xe1;lculos complejos de modelos probabil&#xed;sticos">
<icon BUILTIN="xmag"/>
<node CREATED="1433520041984" ID="ID_313557925" MODIFIED="1433520099130" TEXT="Redes de Inferencia y Redes de Confianza"/>
<node CREATED="1433520065146" ID="ID_230341322" MODIFIED="1433520086620" TEXT="Relacionan prob previa de q doc sea relevante con prob posterior segun caracts observadas"/>
</node>
</node>
<node CREATED="1433517708162" ID="ID_654104676" MODIFIED="1433525175451" TEXT="Estructurados">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433519540059" ID="ID_505684600" MODIFIED="1433519551200" TEXT="Listas no solapadas">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433520353234" ID="ID_845295481" MODIFIED="1433520474636">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Divide doc en colecci&#243;n de <u>segmentos</u>&#160;disjuntos (m&#250;ltiples listas seg&#250;n criterios)&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1433520404315" ID="ID_871354457" MODIFIED="1433520416610" TEXT="2 ficheros invertidos: con comps estructurales y con TI"/>
</node>
</node>
<node CREATED="1433519544404" ID="ID_1955508959" MODIFIED="1433519550320" TEXT="Nodos pr&#xf3;ximos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433520429629" ID="ID_675955957" MODIFIED="1433520467885">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Doc representado por <u>estructura jer&#225;rquica</u>&#160;de regiones (nodos)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1433520508552" ID="ID_55557678" MODIFIED="1433772202828" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Medidas de eval del S&#170;RI
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433520521075" ID="ID_523806089" MODIFIED="1448644462089" TEXT="Tradicionales">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433520531414" ID="ID_227475350" MODIFIED="1433520651555">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Relevancia: </i>c&#243;mo una consulta se ajusta a un doc (coincide), medida objetiva
    </p>
  </body>
</html></richcontent>
<node CREATED="1433520537319" ID="ID_974789809" MODIFIED="1433520670438">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Pertinencia: </i>c&#243;mo un doc se ajusta a una necesidad de info (satisfacci&#243;n), medida subjetiva
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1448644462062" ID="ID_1378165161" MODIFIED="1448644469785" TEXT="&#xcd;ndices">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1433520542433" ID="ID_425539952" MODIFIED="1448644505059">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Precisi&#243;n o Pertinencia:</i>&#160;(calidad) habilidad del s&#170; para evitar ruido
    </p>
  </body>
</html></richcontent>
<node CREATED="1448644505065" ID="ID_1614265431" MODIFIED="1448644505068">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Docs relevantes recuperados /docs recup
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1433520551403" ID="ID_211645505" MODIFIED="1448644572225">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ruido: </i>docs no relevantes recup / docs recup (complementario a precisi&#243;n)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1433520546489" ID="ID_1850190621" MODIFIED="1448644698336">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Exhaustividad, Retorno o Respuesta: </i>(cantidad) habilidad del s&#170; para recuperar docs relevantes
    </p>
  </body>
</html></richcontent>
<node CREATED="1448644552275" ID="ID_715904971" MODIFIED="1448644552278">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Docs relevantes recup / docs relevantes
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1433520722575" ID="ID_125973441" MODIFIED="1433772255265" TEXT="Medidas inversas (deben compensarse)">
<icon BUILTIN="yes"/>
<node CREATED="1433520555420" ID="ID_655097693" MODIFIED="1433772309240">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Medida-f:</i>&#160;cuando Prec y Exh est&#225;n muy compensadas&#8594; media arm&#243;nica ponderada de ambas
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1433520523547" ID="ID_493807510" MODIFIED="1433520529439" TEXT="Del usuario">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433520560141" ID="ID_130288175" MODIFIED="1433520908367">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Efectividad o satisfacci&#243;n:</i>&#160;lo que el usuario quiere entre lo que recibe (subjetivo)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1433520910660" ID="ID_1990720602" MODIFIED="1433520937765">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ratio de cobertura: </i>docs relevantes conocidos recup / docs relevantes conocidos
    </p>
  </body>
</html></richcontent>
<node CREATED="1433520948398" ID="ID_1363675704" MODIFIED="1433520993534">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ratio de novedad:</i>&#160;docs relevantes desconocidos recup / docs relevantes recup
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1433521019995" ID="ID_1452921122" MODIFIED="1433521079742">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Exh relativa: </i>docs relevantes recup examinados por usuario / docs que quiere examinar
    </p>
  </body>
</html></richcontent>
<node CREATED="1433521046220" ID="ID_936693522" MODIFIED="1433521070436">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Esfuerzo&#160;de exh:</i>&#160;docs relevantes deseados / docs que ha de examinarse para localizarlos
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1433521611772" ID="ID_1271052417" MODIFIED="1433772203771" POSITION="right" TEXT="Recup de info multimedia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433522317266" ID="ID_1605874473" MODIFIED="1433522439617">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Dificultar de indexar info contenida en diferentes medios&#8594;&#160; <u>heterogeneidad sem&#225;ntica</u>&#160;de BDs
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1433522395511" ID="ID_255166123" MODIFIED="1433522420266">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Metadatos:</i>&#160;necesarios para categorizar colecciones de objetos
    </p>
  </body>
</html></richcontent>
<node CREATED="1433522444053" ID="ID_429590997" MODIFIED="1433772743053" TEXT="Sem&#xe1;nticos (asunto del doc), De contexto (objs ext-autor) o Estructurales (presentaci&#xf3;n del doc) "/>
</node>
<node CREATED="1433522510888" ID="ID_113265311" MODIFIED="1433522525110" TEXT="Recup de im&#xe1;genes basadas en contenidos (CBIR)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433522525379" ID="ID_747902958" MODIFIED="1433522538994" TEXT="An&#xe1;lisis de caracts de bajo nivel detectadas autom (color, textura, forma)"/>
</node>
</node>
<node CREATED="1433521622619" ID="ID_1136622742" MODIFIED="1433772204625" POSITION="right" TEXT="B&#xfa;squedas web">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433522582467" ID="ID_1400611990" MODIFIED="1433522620418">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Consultas: </i>de info (general), de navegaci&#243;n (p&#225;g ppal) o transaccional (compra, descarga, reserva)
    </p>
  </body>
</html></richcontent>
<node CREATED="1433522626820" ID="ID_765168099" MODIFIED="1433772814777">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Shingling:&#160; </i>t&#233;cn para id p&#225;gs <u>duplicadas</u>&#8594; busca grupos iguales de palabras consecutivas
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1433522559342" ID="ID_1956754949" MODIFIED="1433523018901">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Subs&#170; de motor de b&#250;squeda
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433522744390" ID="ID_283348893" MODIFIED="1433523082678">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Recolector (crawler):</i>&#160;busca p&#225;gs y las <u>incorpora</u>&#160;a la colecci&#243;n junto con enlaces que las interconecta
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1433522820840" ID="ID_1696095947" MODIFIED="1433523051488" TEXT="Robusto: anti trampas ara&#xf1;a (infinitas p&#xe1;gs de un dominio)"/>
<node CREATED="1433522834950" ID="ID_1187492367" MODIFIED="1433522850590" TEXT="Respeta pol&#xed;ticas de acceso a servs Web"/>
</node>
<node CREATED="1433522874693" ID="ID_1708497996" MODIFIED="1433523095239">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Indexador:</i>&#160;convierte colecci&#243;n en estructura +manejable y peque&#241;a&#8594; <u>&#205;ndice</u>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1433522899620" ID="ID_912128435" MODIFIED="1433523101007">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Buscador (Search Engine):</i>&#160;<u>recupera</u>&#160;p&#225;gs del &#237;ndice a partir de consulta de usuario
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1433523068897" ID="ID_470074636" MODIFIED="1433526531087">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Distrib de &#237;ndices: </i>serv global (web)&#8594;m&#250;ltiples m&#225;qs repartidas
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1433523157859" ID="ID_1088673325" MODIFIED="1433523283473">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Por t&#233;rminos (org global) o&#160; <u>docus</u>&#160;(org local, +habitual, cada nodo tiene index de un subconj de p&#225;gs)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1433523316688" ID="ID_799031299" MODIFIED="1433523373401">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Categorizaci&#243;n por enlaces:</i>&#160;influencia de resultado seg&#250;n enlaces que le hacen ref (PageRank)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1433523382520" ID="ID_72817166" MODIFIED="1433523411203">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Anchor text:</i>&#160;texto q acompa&#241;a a hiperv&#237;nculo, da info sobre p&#225;g destino
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1433523411848" ID="ID_1598418481" MODIFIED="1433523452138">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Concentrador: </i>p&#225;g que incluye refs a otras ('enlaces de inter&#233;s')
    </p>
  </body>
</html></richcontent>
<node CREATED="1433523427800" ID="ID_724761480" MODIFIED="1433523444249">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Autoridad:</i>&#160;p&#225;g citada
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1433523456794" ID="ID_760197212" MODIFIED="1433523531052">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Agentes inteligentes: </i>pgms aut&#243;nomos, coop con otros, reaccionan y son adaptables
    </p>
  </body>
</html></richcontent>
<node CREATED="1433523487335" ID="ID_423186814" MODIFIED="1433523503210" TEXT="Exploran autom web para recuperar p&#xe1;gs relevantes a una necesidad">
<node CREATED="1433523512125" ID="ID_1200324101" MODIFIED="1433523517869" TEXT="Lentos, reducen exhaustividad">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
</node>
<node CREATED="1433521638592" ID="ID_164969776" MODIFIED="1433772206196" POSITION="right" TEXT="T&#xe9;cnicas de recup de info">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433523589541" ID="ID_1888009977" MODIFIED="1433523615123" TEXT="Extracci&#xf3;n de Info">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433523632866" ID="ID_1547099074" MODIFIED="1433524274090">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Extrac autom de info <u>estructurada</u>&#160;a partir de texto no estructurado
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1433523662554" ID="ID_1669774232" MODIFIED="1433526603967" TEXT="Datos precisos, manipulables y analizables (pe XML o Relacional)">
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1433523696660" ID="ID_726565249" MODIFIED="1433523707921" TEXT="1. An&#xe1;lisis Local">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433523708239" ID="ID_497210234" MODIFIED="1433773227357">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>1. An&#225;lisis l&#233;xico:</i>&#160;div texto en frases y fragmentos (con diccionario)
    </p>
  </body>
</html></richcontent>
<node CREATED="1433523736255" ID="ID_520078446" MODIFIED="1433773232818">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2. Id nombres:</i>&#160;propios, orgs, lugares, expresiones...
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1433523758165" ID="ID_206633594" MODIFIED="1433773238002">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>3. An&#225;lisis sint&#225;ctico: </i>limitado a ciertas rel gramaticales
    </p>
  </body>
</html></richcontent>
<node CREATED="1433523793639" ID="ID_241728248" MODIFIED="1433773242103">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>4. Id hechos:</i>&#160;extracci&#243;n de eventos o rels relevantes para la b&#250;squeda
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1433523843012" ID="ID_1379926330" MODIFIED="1433523848399" TEXT="2. An&#xe1;lisis Global">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433523906107" ID="ID_190288106" MODIFIED="1433526656131" TEXT="An&#xe1;lisis de los hechos identificados en relaci&#xf3;n con el texto en su conjunto ">
<icon BUILTIN="xmag"/>
</node>
<node CREATED="1433523852968" ID="ID_570376732" MODIFIED="1433773369492">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>1. Correferencialidad:</i>&#160;id pronombres y sintagmas nominales de un =objeto
    </p>
  </body>
</html></richcontent>
<node CREATED="1433523876047" ID="ID_318838824" MODIFIED="1433773377091">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2. Inferencia:</i>&#160;(deducci&#243;n) integraci&#243;n de hechos, explicita info sobre evento
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1433523934770" ID="ID_1507607567" MODIFIED="1433523955627">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>3. Relleno de formularios:</i>&#160;info relevante extra&#237;da se estructura en formato de salida requerido
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1433523615386" ID="ID_1422366681" MODIFIED="1433523625775" TEXT="Miner&#xed;a de textos (Datamining)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433523960425" ID="ID_203286787" MODIFIED="1433524276038">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Procesos inform&#225;ticos derivan &#160;<u>info de alta calidad</u>&#160;a partir de texto no estructurado
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1433524041889" ID="ID_1047158945" MODIFIED="1433526717319" TEXT="Sentiment, resumen, conocim, taxonom&#xed;as, E/R, categorizaci&#xf3;n "/>
</node>
<node CREATED="1433524071424" ID="ID_1546510650" MODIFIED="1433524094762">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>1. Estructuraci&#243;n de texto de entrada:</i>&#160;an&#225;lisis gramatical e inserci&#243;n de resultado en BD
    </p>
  </body>
</html></richcontent>
<node CREATED="1433524167110" ID="ID_1267919171" MODIFIED="1433524196372" TEXT="T&#xe9;cns de RI, Leng Nat y Extracci&#xf3;n Info"/>
</node>
<node CREATED="1433524095687" ID="ID_533473639" MODIFIED="1433526813710">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2. Extracci&#243;n de patrones:</i>&#160;relaciones o patrones escondidos (aprendizaje autom y estad&#237;sticas)
    </p>
  </body>
</html></richcontent>
<node CREATED="1433526768078" ID="ID_837888956" MODIFIED="1433526806829">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Miner&#237;a de datos
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1433524117662" ID="ID_844165303" MODIFIED="1433524236488">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>3. Eval e interpretaci&#243;n del resultado: </i>se vuelca en otra BD con interfaz gr&#225;fico para usuario
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</map>
