<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1428576366552" ID="ID_674750583" MODIFIED="1480688677515">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      T100 CONSERVACI&#211;N INFO
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1428592316783" ID="ID_1306185086" MODIFIED="1428592319144" POSITION="right" TEXT="Backup">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428592548259" ID="ID_1942345751" MODIFIED="1446219517636">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>NDMP: </i>(Netw Data Manag) prot de backup autom de <u>NAS</u>&#160;o serv de ficheros
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1428592561242" ID="ID_265362088" MODIFIED="1446219559956">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>VTL:</i>&#160;(Virtual Tape Lib) disco aparenta ser <u>cinta</u>, y cuando acaba el backup vuelca info sobre cinta real
    </p>
  </body>
</html></richcontent>
<node CREATED="1428592587618" ID="ID_1496684680" MODIFIED="1446219608810">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      - t de respuesta (disco tiene acceso aleat)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1428593193705" ID="ID_724823350" MODIFIED="1446219941073">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>ILM:</i>&#160;(Info Lifecycle Manag) s&#170; encargado de mover datos&#160;<u>autom</u>&#160;seg&#250;n pol&#237;ticas y transp&#8594;pe filtros mail
    </p>
  </body>
</html></richcontent>
<node CREATED="1446219883811" ID="ID_45567858" MODIFIED="1446219900613">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Alinea infr IT con reqs de negocio, grandes vol&#250;ms de datos
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1428592694996" ID="ID_1488460094" MODIFIED="1446219579190" TEXT="Tipos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428592699677" ID="ID_714731088" MODIFIED="1446219596244">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Normal: </i>copia <u>total</u>, borra bit de modificado
    </p>
  </body>
</html></richcontent>
<node CREATED="1428592961440" ID="ID_1121388846" MODIFIED="1446219656769">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Intermedia:</i>&#160;copia <u>total</u>, <u>no borra</u>&#160;bit modif&#8594; Si se quiere&#160;&#160;copia extra de un d&#237;a sin afectar al resto
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1428592721236" ID="ID_390840364" MODIFIED="1446219721446">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Incremental:</i>&#160;solo copia archivos modific&#160;desde &#250;ltima copia ( <u>inc, dif o nor </u>), borra bit de modif
    </p>
  </body>
</html></richcontent>
<node CREATED="1428592805200" ID="ID_1681259186" MODIFIED="1446219701217" TEXT="Req -capac y t">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1428592831318" ID="ID_986613713" MODIFIED="1446219717514">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Diferencia: </i>solo copia archivos modific desde &#250;ltima copia&#160; <u>normal</u>, <u>no borra</u>&#160;bit de modif
    </p>
  </body>
</html></richcontent>
<node CREATED="1428592864254" ID="ID_307938973" MODIFIED="1428592877092" TEXT="Restauraci&#xf3;n +sencilla">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1428593015599" ID="ID_806926393" MODIFIED="1446219756381">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Rotaci&#243;n de medios: </i>dicta n&#186; cintas a utilizar para backup&#8594;&#160;12+4+7=23
    </p>
  </body>
</html></richcontent>
<node CREATED="1428593062291" ID="ID_611484466" MODIFIED="1446219770060">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Abuelo:</i>&#160;1 copia completa al <u>mes</u>, se guarda durante 1 <u>a&#241;o</u>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1428593079931" ID="ID_822111525" MODIFIED="1446219777272">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Padre: </i>1 copia completa <u>semanal</u>, se guarda durante 1 <u>mes</u>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1428593106239" ID="ID_961629251" MODIFIED="1446219784986">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Hijo: </i>1 copia completa, incr o dif&#160; <u>diaria</u>, se guardan durante 1 <u>semana</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</map>
