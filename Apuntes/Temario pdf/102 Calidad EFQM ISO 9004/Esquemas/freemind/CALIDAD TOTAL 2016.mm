<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1441793622793" ID="ID_1609615498" MODIFIED="1441793983486">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T92 CALIDAD TOTAL
    </p>
    <p style="text-align: center">
      (Enfoque moderno)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441794033583" ID="ID_1196844716" MODIFIED="1441794042638" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Calidad total
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441795912614" ID="ID_1182594182" MODIFIED="1441799388071">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estrategia global&#8594; integra perspectiva <u>cliente</u>&#160;(indicadores ext) con eficiencia de <u>empresa</u>&#160;(indics int)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1441795915467" ID="ID_1614043385" MODIFIED="1441796064605">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Seguim de todo el <u>proceso de att</u>&#160;al cliente
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441796091607" ID="ID_1436015948" MODIFIED="1441797290679">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Satisfacer</u>&#160;expects clientes, aportar valor a empresa y facilitar gesti&#243;n del cambio
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1441796144025" ID="ID_1968641018" MODIFIED="1441797308790">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Implica&#8594; <u>integrar</u>&#160;cal en estrategia y <u>s&#170; cal</u>&#160;operativo y con valor (bucle&#160; <u>PDCA</u>)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441794037329" ID="ID_1329929327" MODIFIED="1441814159816">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sistema de Cal Total
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441796463244" ID="ID_1645719070" MODIFIED="1441796642929">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Subs&#170; de Gesti&#243;n de cal total</i>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441796541413" ID="ID_1277447900" MODIFIED="1441796618062">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Define la estruct org, resps, proceds y recs para&#160;<u>planif, controlar</u>&#160;&#160;y mejorar la cal
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441796548073" ID="ID_1717029961" MODIFIED="1441796646925">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Subs&#170; de Aseg de cal total
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441796555118" ID="ID_973102721" MODIFIED="1441796596872">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Elems de <u>confianza</u>&#160;de que los prods/servs satisfacen expectativas&#8594; normas ISO, docum y audits
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1441794062008" ID="ID_1538417490" MODIFIED="1441794064264" TEXT="Costes">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441796652373" ID="ID_1625398" MODIFIED="1441796673709">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Directos:&#160;</i>de conformidad (prev y eval) y no conf (fallos)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441796674336" ID="ID_1464467790" MODIFIED="1441796697291">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Indirectos:</i>&#160;por no cumplir expectativas (insatisf y mala imagen)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441796702814" ID="ID_1528158538" MODIFIED="1441796795010">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Punto &#243;ptimo:&#160; </i>nivel de cal prestada = percibida&#8594; costes de conform = no conf
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441796772065" ID="ID_1938723687" MODIFIED="1441796783412" TEXT="Eficiencia (m&#xe1;x satisf a m&#xed;n coste)">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</node>
<node CREATED="1441794097825" ID="ID_30199800" MODIFIED="1441794150373" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Est&#225;ndares
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441797472156" ID="ID_512437560" MODIFIED="1441814196728">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modelos de ref para <u>evaluar&#160;capacidad de procs</u>&#8594; elecci&#243;n seg&#250;n prop&#243;sitos y estado de desarroll org
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
</node>
<node CREATED="1441794112757" ID="ID_655263513" MODIFIED="1441797988254">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CMM
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441797560438" ID="ID_253003959" MODIFIED="1441798777982">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Capab Maturity) grado de <u>madurez</u>&#160;de pr&#225;cts de gesti&#243;n&#160;de la org y <u>acciones de mejora</u>&#160;de sus procs
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1441794119313" ID="ID_678399785" MODIFIED="1441798271204">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Niveles de
    </p>
    <p>
      madurez
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1441798217947" ID="ID_494542941" MODIFIED="1441798304618">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Capacidad del proceso de sw, con objs a satisfacer para evol al pr&#243;x nivel
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441797748209" ID="ID_919180430" MODIFIED="1441798144109">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>&#193;rea Clave del Proc:</i>&#160;(KPA) funcs q deben llevarse a cabo en cada nivel
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441797615445" ID="ID_1217142095" MODIFIED="1441814224173">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>1. Inicial: </i>no hay metodolog&#237;a, &#233;xito seg&#250;n esfuerzo indiv&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1441797675851" ID="ID_1222557191" MODIFIED="1441798268803">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2. Repetible:</i>permite <u>estimar</u>&#160;a partir de &#233;xitos anteriores
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441797803994" ID="ID_960065028" MODIFIED="1441798133056">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>KPA:&#160;</i>gesti&#243;n de reqs, planif, seguim y ctrl,&#160; <u>aseg cal</u>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441797843205" ID="ID_694485324" MODIFIED="1441798268542">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>3.&#160;Definido: </i><u>activs bien def</u>&#160;(construcc del s&#170;), proceso docum y estandar, gesti&#243;n riesgo
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441797938696" ID="ID_1563385826" MODIFIED="1441797961280">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>KPA:&#160;</i>def y mejora procs, formaci&#243;n, coord, revisi&#243;n
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441797995178" ID="ID_1980744509" MODIFIED="1441798268333">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>4.&#160;Gestionado:</i>&#160;a&#241;ade <u>gesti&#243;n a procesos</u>, realim dsd 1as activs, se recopilan medidas del proceso
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441798099644" ID="ID_1652273040" MODIFIED="1441798166195">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>KPA: </i>gesti&#243;n cuantit del proy y&#160; <u>gesti&#243;n de cal</u>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441798168513" ID="ID_1596024479" MODIFIED="1441798268101">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>5. Optimizado: </i><u>mejora cont&#237;nua</u>&#160;de procs en base a results de activs
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441798201675" ID="ID_1662470224" MODIFIED="1441798216005">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>KPA:</i>&#160;prev de defectos y gesti&#243;n de cambios
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1441798375141" ID="ID_633882452" MODIFIED="1441798875618">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>CMMI</i>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441798862306" ID="ID_1965643727" MODIFIED="1441798862313">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mejora usabilidad <u>integrando</u>&#160;varios modelos
    </p>
  </body>
</html></richcontent>
<node CREATED="1441798455059" ID="ID_521303191" MODIFIED="1441798513563">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Diagn&#243;stico&#160; <u>preciso</u>&#160;de madurez, elimina inconsist/duplics, term y reglas comunes, consist con SPICE
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1441798534073" ID="ID_1327802714" MODIFIED="1441798565585">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>v1.3:&#160;</i>DEV (desarr prod/serv), ACQ (adquis, sumin y contrat ext) y SVC (gesti&#243;n servs)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441798868620" ID="ID_719683298" MODIFIED="1441798975450">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Represent
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441798922519" ID="ID_1405950396" MODIFIED="1441799004440">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Continua:</i>&#160;nivel para cada <u>KPA</u>&#8594; incompl, realizado, gestionado y def
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441798951813" ID="ID_739904783" MODIFIED="1441798992162">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Por etapas: </i>nivel para <u>org</u>&#160;en conjunto&#8594; 1 a 5 como CMM
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1441799013013" ID="ID_196916248" MODIFIED="1441810151394">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Eval: </i>orgs <u>no certificables</u>, pero pueden medir su progr/nivel
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1441799050218" ID="ID_1909816581" MODIFIED="1441799104869">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>ARC: </i>(Appraisal Reqs for CMMI) compara procs de la org y planifica mejoras
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1441799105694" ID="ID_1207446813" MODIFIED="1441799199085">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>SCAMPI: </i>(Appr Meth for Proc Improve) cumple ARC, m&#233;todo oficial del Sw Eng Inst (SEI)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1441794117177" ID="ID_948717371" MODIFIED="1441797991761" TEXT="SPICE">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441799216978" ID="ID_783547257" MODIFIED="1441799485465">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Sw Proc Improve &amp; Capab Determ,&#160;<u>ISO 15504</u>) estnd p&#250;b&#160;para eval procesos de sw
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441799275410" ID="ID_1821800820" MODIFIED="1441799494867">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Procesos:</i>&#160;en categs&#8594; cliente-suministr, ing, soporte, gesti&#243;n y org &#160;
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1441799319289" ID="ID_412097410" MODIFIED="1441814667088">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Capacidd: </i>6 nivels y 9 atrib de proc&#8594; incompl, realiz, gestionad, establec, predec y optimiz
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1441794166146" ID="ID_405134156" MODIFIED="1441794173957" POSITION="right" TEXT="ISO 9000">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441809344726" ID="ID_1319690853" MODIFIED="1441809636479">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Normas para <u>S&#170; de Gesti&#243;n de Cal</u>&#8594;&#160;conceptos (9000), reqs (9001) y gu&#237;as de mejora (9004)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1441809437136" ID="ID_1953005458" MODIFIED="1441810335334">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>9004:&#160;</i>5 niveles de madurez con directrices de mejora cont&#237;nua
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1441794178962" ID="ID_1470784296" MODIFIED="1441814349602">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ppios
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441809653200" ID="ID_1439300940" MODIFIED="1441814382345">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Enfoque a cliente, a recs (particip personal), a procesos y a gesti&#243;n (mejora cont&#237;nua-<u>PDCA</u>)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441814319946" ID="ID_926967459" MODIFIED="1441814683350">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Liderazg (papel Dir), decisions basadas en hechos y rel beneficiosa mutua con proveed
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1441794208484" ID="ID_496024437" MODIFIED="1441814423992">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Otras
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441810523150" ID="ID_356026355" MODIFIED="1441810553009">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>ISO/IEC 20000: </i><u>gesti&#243;n de servs</u>&#160;de TI certificable a nivel mundial
    </p>
  </body>
</html></richcontent>
<node CREATED="1441810558519" ID="ID_1672719041" MODIFIED="1441810601927">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Parte 1 (Especs, 217 reqs para entrega de servs) y Parte 2 (Pr&#225;cticas de mejora)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441794225593" ID="ID_894771674" MODIFIED="1441810746290">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>ITIL:</i>&#160;(Info Techn Infr Lib, v3) buenas pr&#225;cticas de&#160; <u>entrega de servs</u>&#160;TI con calidad y eficiencia
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1441810670998" ID="ID_776666885" MODIFIED="1441810781327">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No certificable (ayuda a conseguir ISO), conjunto de libros/&#225;reas
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1441794230080" ID="ID_333882179" MODIFIED="1441794290415" POSITION="right" TEXT="Modelo EFQM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441810808556" ID="ID_1004583507" MODIFIED="1441811210036">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (EU Found for Quality Manag, v2013) marco EU de&#160; <u>Gesti&#243;n de Cal Total</u>
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1441810852273" ID="ID_1738109918" MODIFIED="1441810902123">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Excelencia</u>&#8594; implic de todos los empleados en mejora cont&#237;nua de procesos
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1441794251291" ID="ID_1190839385" MODIFIED="1441794255363" TEXT="Conceptos de excelencia">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441810905884" ID="ID_393112104" MODIFIED="1441810975339">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      +valor a clientes, sostenible, capacidad, innov, liderar con visi&#243;n e inspir, agilidad, talento y resultados
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441794290402" ID="ID_460279397" MODIFIED="1441812011191">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Autoeval
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441811254846" ID="ID_1900340783" MODIFIED="1441811268282" TEXT="Examen global y sistem de activs y results comparados con EFQM">
<node CREATED="1441811750532" ID="ID_1082738995" MODIFIED="1441811777689">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Info de fortalezas y &#225;reas de mejora (base de mejora cont)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1441794255842" ID="ID_198431775" MODIFIED="1441811985854">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Criterios
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441811021736" ID="ID_723118916" MODIFIED="1441811230160">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Facilitadores del &#233;xito
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441811032039" ID="ID_1308093260" MODIFIED="1441811148304">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Liderazgo (Dir), Estrateg, Personas (partic), Alianzas y Recs (efici y compart) y Proc/prod/servs (coop)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441811151648" ID="ID_1628343169" MODIFIED="1441811155410" TEXT="Resultados">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441811155414" ID="ID_1172076948" MODIFIED="1441811197512">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      En Clientes (satisf), en Personas (motiv), en Sociedad (bienestar) y Clave (grupos de inter&#233;s)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1441794261299" ID="ID_1691607814" MODIFIED="1441814452490">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Esquema REDER
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1441811861228" ID="ID_22995662" MODIFIED="1441811917035">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      M&#233;todo para <u>puntuar</u>&#160;memos de orgs presentadas al <u>Premio EU de Cal</u>&#160;(concedido por EFQM)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441813942548" ID="ID_914051219" MODIFIED="1441813947875" TEXT="4 perspectivas para eval criterios"/>
</node>
<node CREATED="1441812056305" ID="ID_526473723" MODIFIED="1441812332344">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i><u>R</u>esultados:&#160;</i>grado en que se alcanzan <u>objs</u>&#160;y <u>relevancia</u>/utilidad
    </p>
  </body>
</html></richcontent>
<node CREATED="1441812120401" ID="ID_566161158" MODIFIED="1441812184928">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i><u>E</u>nfoque:</i>&#160;<u>planif</u>&#160;a necesidades de grupos de inter&#233;s y<u>procesos</u>&#160;bien def
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1441812186643" ID="ID_1689790588" MODIFIED="1441812230985">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i><u>D</u>espliegue:</i>&#160;<u>implantaci&#243;n</u>&#160;de lo planif y <u>forma</u>&#160;sistem&#225;tica&#160;de realiz
    </p>
  </body>
</html></richcontent>
<node CREATED="1441812231640" ID="ID_332490044" MODIFIED="1441812272712">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i><u>E</u>val, <u>R</u>ev y Perfecc:</i>&#160;<u>mediciones</u>&#160;regulares, aprendizaje y <u>mejoras</u>&#160;seg&#250;n results
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1441794303745" ID="ID_967090081" MODIFIED="1441812459896" POSITION="right" TEXT="Herramientas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441812464655" ID="ID_1118374796" MODIFIED="1441812541888">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Elems de gesti&#243;n para facilitar y estructurar procesos de mejora
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1441812481217" ID="ID_686649454" MODIFIED="1441812499163" TEXT="Id probls y sols, necesidades clientes, priorizar, eval, id causas de no cal">
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1441812501048" ID="ID_939342985" MODIFIED="1441812615621">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Diagr del pareto:</i>&#160;el 20% de los elems q contrib a un efecto son resps del 80%&#8594; <u>centrarse</u>&#160;en lo vital
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1441812549911" ID="ID_319560723" MODIFIED="1441812607513">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Depliegue de Func de Cal:</i>&#160;id <u>necesidades</u>&#160;durante todos los procesos (traduce reqs a especs t&#233;cn)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441812630761" ID="ID_1362486767" MODIFIED="1441814536002">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se centra en detalles, voz del usuario en todo el proc, -t y coste, mejora cont
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1441812655115" ID="ID_890254010" MODIFIED="1441812753597">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Metod 6-SIGMA:</i>&#160;estrat de mejora cont, id y elimina causas&#160;de <u>errores</u>&#160;basado en <u>estad&#237;stica</u>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441812755733" ID="ID_805769710" MODIFIED="1441812800058">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Proceso de fabric/prest sigue distrib <u>Gauss</u>&#8594; prob de estar fuera = prob defecto
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1441794322769" ID="ID_402608941" MODIFIED="1441794328288" POSITION="right" TEXT="RD 951/2005">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441813210170" ID="ID_590673800" MODIFIED="1441813258012">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Marco gen para mejora de cal en la <u>AGE</u>&#8594; 6 pgms para impulsar <u>mejora cont</u>&#160;en los SP
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1441813284111" ID="ID_393896382" MODIFIED="1441813325910">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>AEPCS:</i>&#160;(Ag de Eval de Pol&#237;ts y Cal de Servs) asesora a AP y presta servs a ciuds
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441813355578" ID="ID_799575722" MODIFIED="1441813478913" TEXT="Pgms">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441813357050" ID="ID_711874177" MODIFIED="1441814585256">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      An&#225;lisis de demanda y satisf (encuestas), Cartas de servs, Quejas y sug (adoptar medidas), Eval de cal de orgs (EFQM, EVAM y CAF), Reconoci (certifs y premios) y&#160; <u>OCSP</u>&#160;(Observ de Cal de SP, informe anual)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441813676490" ID="ID_1843373007" MODIFIED="1441813723468">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>CAF: </i>(Marco Com&#250;n de Eval, EU) EFQM+Speyer (m&#233;todo alem&#225;n)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1441794331780" ID="ID_1051129354" MODIFIED="1441794339144" TEXT="Modelo EVAM">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441813502374" ID="ID_540134217" MODIFIED="1441813868995">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Eval, Aprendiz y Mejora, MinHAP) <u>autoeval</u>&#160;asistida con 5 criterios/ejes y 16 subcrits/aspectos
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441813571860" ID="ID_1810013862" MODIFIED="1441813808787">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Cuestionario</u>&#160;&#160;para cada aspecto, basado en<u>PDCA</u>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441811032039" ID="ID_165802734" MODIFIED="1441813657987">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ejes: </i>Liderazgo (pol&#237;t y estrat), Procesos, Personas, Alianzas y Recs, y Resultados
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</map>
