<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1370528794989" ID="ID_828927634" MODIFIED="1378233915181" TEXT="EFQM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374861117995" ID="ID_1038390614" MODIFIED="1377854324777" POSITION="right" TEXT="Desarrollado en 1991 por la European Foundation for Quality Management (EFQM)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370528931093" ID="ID_537323823" MODIFIED="1377854317648" POSITION="right" TEXT="Agentes Facilitadores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370528966175" ID="ID_1697624022" MODIFIED="1516882593544" STYLE="fork" TEXT="Liderazgo (10)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1370529331232" ID="ID_1037494176" MODIFIED="1370529337415" TEXT="Misi&#xf3;n, visi&#xf3;n y valores"/>
<node CREATED="1370529355784" ID="ID_1762697289" MODIFIED="1377854334547" TEXT="Implicaci&#xf3;n personal de los l&#xed;deres">
<node CREATED="1370529379971" ID="ID_511734336" MODIFIED="1370529386050" TEXT="Con la mejora continua"/>
<node CREATED="1370529386563" ID="ID_515254970" MODIFIED="1370529397206" TEXT="Con clientes, asociados y representantes"/>
</node>
<node CREATED="1370529404550" ID="ID_195164023" MODIFIED="1370529413078" TEXT="Cultura de excelencia"/>
<node CREATED="1370529413345" ID="ID_1990200473" MODIFIED="1370529421347" TEXT="Impulso del cambio"/>
</node>
<node CREATED="1370528982629" ID="ID_1238151985" MODIFIED="1516882592169" TEXT="Personas (10)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1370529576531" ID="ID_1184487415" MODIFIED="1370529598852" TEXT="Planificaci&#xf3;n, gesti&#xf3;n y mejora de los recursos humanos"/>
<node CREATED="1370529617837" ID="ID_1063062822" MODIFIED="1370529634552" TEXT="Identificaci&#xf3;n, desarrollo y mejora del conocimiento"/>
<node CREATED="1370529638100" ID="ID_606612639" MODIFIED="1370529653628" TEXT="Implicaci&#xf3;n y asunci&#xf3;n de responsabilidades"/>
<node CREATED="1370529667906" ID="ID_1827245764" MODIFIED="1370529674895" TEXT="Di&#xe1;logo entre personas"/>
<node CREATED="1370529675224" ID="ID_61617566" MODIFIED="1370529696025" TEXT="Recompensa, reconocimiento y atenci&#xf3;n a las personas"/>
</node>
<node CREATED="1370528985767" ID="ID_1845796129" MODIFIED="1516882590444" TEXT="Estrategia (10)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1370529441469" ID="ID_38359260" MODIFIED="1370529456565" TEXT="Necesidades y expectativas actuales y futuras"/>
<node CREATED="1370529480258" ID="ID_1210365698" MODIFIED="1370529508282" TEXT="Indicadores de rendimiento, investigaci&#xf3;n, aprendizaje y actividades externas"/>
<node CREATED="1370529535823" ID="ID_1269462592" MODIFIED="1370529548389" TEXT="Se desarrolla, revisa y actualiza"/>
<node CREATED="1370529555176" ID="ID_212898711" MODIFIED="1370529787628" TEXT="Se comunica y despliega mediante procesos clave"/>
</node>
<node CREATED="1370528997642" ID="ID_1029603377" MODIFIED="1516882587965" TEXT="Alianzas y Recursos (10)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
<node CREATED="1370529701841" ID="ID_1016018800" MODIFIED="1370529710607" TEXT="Gesti&#xf3;n de las alianzas externas"/>
<node CREATED="1370529711170" ID="ID_797208393" MODIFIED="1370529726403" TEXT="Gesti&#xf3;n de los recursos econ&#xf3;micos y financieros"/>
<node CREATED="1370529726920" ID="ID_1744081991" MODIFIED="1370529742922" TEXT="Gesti&#xf3;n de los edificios, equipos y materiales"/>
<node CREATED="1370529743304" ID="ID_1219316883" MODIFIED="1370529750423" TEXT="Gesti&#xf3;n de la tecnolog&#xed;a"/>
<node CREATED="1370529766121" ID="ID_67129964" MODIFIED="1370529782761" TEXT="Gesti&#xf3;n de la informaci&#xf3;n y el conocimiento"/>
</node>
<node CREATED="1370529005543" ID="ID_189342523" MODIFIED="1516882586637" TEXT="Procesos, productos y servicios (10)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-5"/>
<node CREATED="1370529799085" ID="ID_1451138711" MODIFIED="1370529811655" TEXT="Dise&#xf1;o y gesti&#xf3;n sistem&#xe1;tica"/>
<node CREATED="1370529812295" ID="ID_1786045209" MODIFIED="1370529821583" TEXT="Introducci&#xf3;n de las mejoras necesarias"/>
<node CREATED="1370529822009" ID="ID_696022041" MODIFIED="1370529961822" TEXT="Dise&#xf1;o y desarrollo de los productos y servicios"/>
<node CREATED="1370529841690" ID="ID_1705698164" MODIFIED="1370529859259" TEXT="Producci&#xf3;n, distribuci&#xf3;n y servicio de atenci&#xf3;n"/>
<node CREATED="1370529873169" ID="ID_1411346096" MODIFIED="1370529884674" TEXT="Gesti&#xf3;n y mejora de las relaciones con los clientes"/>
</node>
</node>
<node CREATED="1370528941897" ID="ID_1947121630" MODIFIED="1377854320440" POSITION="right" TEXT="Resultados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node COLOR="#ff3333" CREATED="1370529029326" ID="ID_375482115" MODIFIED="1516882585333" TEXT="Resultados en los clientes (15)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-6"/>
<node CREATED="1370529909275" ID="ID_1435596668" MODIFIED="1370529915933" TEXT="Medidas de percepci&#xf3;n"/>
<node CREATED="1370529916288" ID="ID_445708254" MODIFIED="1370529923577" TEXT="Indicadores de rendimiento"/>
</node>
<node CREATED="1370529020105" ID="ID_704741588" MODIFIED="1516882584176" TEXT="Resultados en el personal (10)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-7"/>
<node CREATED="1370529909275" ID="ID_1576538547" MODIFIED="1370529915933" TEXT="Medidas de percepci&#xf3;n"/>
<node CREATED="1370529916288" ID="ID_1327745561" MODIFIED="1370529923577" TEXT="Indicadores de rendimiento"/>
</node>
<node CREATED="1370529035944" ID="ID_1408972869" MODIFIED="1516882582938" TEXT="Resultados en la Sociedad (10)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-8"/>
<node CREATED="1370529909275" ID="ID_464735968" MODIFIED="1370529915933" TEXT="Medidas de percepci&#xf3;n"/>
<node CREATED="1370529916288" ID="ID_1195137642" MODIFIED="1370529923577" TEXT="Indicadores de rendimiento"/>
</node>
<node COLOR="#ff3333" CREATED="1370529047687" ID="ID_660837354" MODIFIED="1516882578763" STYLE="fork" TEXT="Resultados clave (15)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-9"/>
<node CREATED="1370529977444" ID="ID_1372121330" MODIFIED="1370529990167" TEXT="Resultados clave del rendimiento"/>
<node CREATED="1370529990522" ID="ID_719049886" MODIFIED="1370529998612" TEXT="Indicadores clave del rendimiento"/>
</node>
</node>
<node CREATED="1370529131883" ID="ID_5086326" MODIFIED="1370529148698" POSITION="left" TEXT="Esquema L&#xf3;gico REDER">
<node CREATED="1370529148699" ID="ID_344981062" MODIFIED="1370529152859" TEXT="Resultados"/>
<node CREATED="1370529153248" ID="ID_1194995053" MODIFIED="1370529156181" TEXT="Enfoque"/>
<node CREATED="1370529156630" ID="ID_461221260" MODIFIED="1370529159361" TEXT="Despliegue"/>
<node CREATED="1370529159737" ID="ID_346918389" MODIFIED="1383496901552" TEXT="Evaluaci&#xf3;n y Revisi&#xf3;n"/>
</node>
<node CREATED="1370529189150" ID="ID_1832872738" MODIFIED="1370529201265" POSITION="left" TEXT="PDCA">
<node CREATED="1370529201267" ID="ID_1341816383" MODIFIED="1370529276590" TEXT="Planificar y desarrollar Enfoques"/>
<node CREATED="1370529229914" ID="ID_579073452" MODIFIED="1370529267593" TEXT="Desplegar los enfoques"/>
<node CREATED="1370529279595" ID="ID_1725329583" MODIFIED="1370529297171" TEXT="Evaluar y revisar los enfoques"/>
<node CREATED="1370529297436" ID="ID_896295034" MODIFIED="1370529310576" TEXT="Determinar los Resultados"/>
</node>
<node CREATED="1374860510913" ID="ID_1670782483" LINK="TallerEFQM.pdf" MODIFIED="1377854479194" POSITION="right" TEXT="Cambio en el 2010"/>
</node>
</map>
