<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1370434342268" ID="ID_1458108874" MODIFIED="1379690252108" TEXT="ISO 9001:2000">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1380963161427" ID="ID_365964945" MODIFIED="1380963181657" POSITION="right" TEXT="Reservada a empresas que realicen soporte o desarrollo de servicios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370461476844" ID="ID_57724747" MODIFIED="1370461481850" POSITION="right" TEXT="Ventajas">
<node CREATED="1370461481852" ID="ID_1670051770" MODIFIED="1370461489074" TEXT="Amplia aplicabilidad"/>
<node CREATED="1370461489703" ID="ID_175377309" MODIFIED="1370461501159" TEXT="Afecta a la mayor&#xed;a de las &#xe1;reas funcionales"/>
<node CREATED="1370461501571" ID="ID_513332076" MODIFIED="1370461508921" TEXT="Reconocimiento y apariencia"/>
<node CREATED="1370461509243" ID="ID_86688354" MODIFIED="1370461519957" TEXT="Libertad de implementaci&#xf3;n"/>
<node CREATED="1370461521646" ID="ID_1399224878" MODIFIED="1370461531255" TEXT="Incrementa las oportunidades de negocio"/>
</node>
<node CREATED="1370461538822" ID="ID_1543206903" MODIFIED="1370461542565" POSITION="right" TEXT="Cr&#xed;ticas">
<node CREATED="1370461551713" ID="ID_697085146" MODIFIED="1370461566977" TEXT="Es muy general"/>
</node>
<node CREATED="1372924826946" ID="ID_10671042" MODIFIED="1372924851640" POSITION="right" TEXT="Propone REQUISITOS a cumplir por el Sistema de Gesti&#xf3;n de la Calidad"/>
<node CREATED="1372924852019" ID="ID_390376189" MODIFIED="1382293566721" POSITION="right" TEXT="Sirve para la certificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370434356239" ID="ID_1268494572" MODIFIED="1383495329006" POSITION="right" TEXT="Sistema de Gesti&#xf3;n de la Calidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370434850997" ID="ID_1396338670" MODIFIED="1370434854871" TEXT="PDCA"/>
<node CREATED="1370434867517" ID="ID_786689516" MODIFIED="1370434878319" TEXT="Documentaci&#xf3;n"/>
<node CREATED="1370434878685" ID="ID_1433669228" MODIFIED="1370434887618" TEXT="ISO 9000-3">
<node CREATED="1370434887619" ID="ID_630213095" MODIFIED="1370434895493" TEXT="Procesos"/>
<node CREATED="1370434896061" ID="ID_998309957" MODIFIED="1370434899119" TEXT="Plantillas"/>
<node CREATED="1370434899581" ID="ID_36655490" MODIFIED="1370434909293" TEXT="Modelos de ciclo de vida"/>
<node CREATED="1370434946765" ID="ID_881690596" MODIFIED="1370434972790" TEXT="Descripci&#xf3;n de herramientas, t&#xe9;cnicas, tecnolog&#xed;as y m&#xe9;todos"/>
<node CREATED="1370434973221" ID="ID_402243393" MODIFIED="1370434994361" TEXT="Gu&#xed;as de codificaci&#xf3;n, dise&#xf1;o, desarrollo y pruebas"/>
</node>
</node>
<node CREATED="1370434428941" ID="ID_1881506039" MODIFIED="1516882678829" POSITION="right" TEXT="Responsabilidad de la Direcci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370435007205" FOLDED="true" ID="ID_586141083" MODIFIED="1385641930195" TEXT="Planificando el sistema de la calidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376674111947" ID="ID_68093774" MODIFIED="1376674129045" TEXT="Estableciendo la pol&#xed;tica de la calidad"/>
<node CREATED="1376674129487" ID="ID_1891969370" MODIFIED="1376674139582" TEXT="Estableciendo los objetivos de la calidad"/>
</node>
<node CREATED="1370435015957" ID="ID_1556558471" MODIFIED="1382293855228" TEXT="Llevando a cabo las revisiones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370434444629" ID="ID_1372236306" MODIFIED="1516882680482" POSITION="right" TEXT="Gesti&#xf3;n de los Recursos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370435048157" ID="ID_1507866327" MODIFIED="1382293808147" TEXT="Implementar y mantener el SGC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370435063045" ID="ID_273189406" MODIFIED="1382293844276" TEXT="Aumentar la satisfacci&#xf3;n del cliente a trav&#xe9;s del cumplimiento de los requisitos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370435708901" ID="ID_927863418" MODIFIED="1370435717442" TEXT="ISO 9000-3">
<node CREATED="1370435717443" ID="ID_227381223" MODIFIED="1370435738679" TEXT="Competencia, toma de conciencia, formaci&#xf3;n"/>
<node CREATED="1370435739021" ID="ID_1346281163" MODIFIED="1370435758623" TEXT="Infraestructura: hardware, software, etc."/>
</node>
</node>
<node CREATED="1370434463717" ID="ID_632160206" MODIFIED="1516882681522" POSITION="right" TEXT="Realizaci&#xf3;n del Producto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370435891933" ID="ID_1668533366" MODIFIED="1370435900147" TEXT="Determinar">
<node CREATED="1370435900148" ID="ID_845290732" MODIFIED="1382293834963" TEXT="Objetivos de la calidad y los requisitos para el producto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370435916117" ID="ID_1757228492" MODIFIED="1382293837381" TEXT="Necesidad de establecer procesos, documentos y de proporcionar recursos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370436020221" ID="ID_1835803232" MODIFIED="1382293839830" TEXT="Actividades de verificaci&#xf3;n, validaci&#xf3;n, etc.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370436053037" ID="ID_1403127670" MODIFIED="1370436059295" TEXT="ISO 9000-3">
<node CREATED="1370436091125" ID="ID_1219669518" MODIFIED="1370436136663" TEXT="Procesos, actividades y tareas usando modelo de ciclos de vida apropiados."/>
<node CREATED="1370436137733" ID="ID_790584662" MODIFIED="1370436180703" TEXT="Planificaci&#xf3;n de la Calidad"/>
<node CREATED="1370436188917" ID="ID_519867600" MODIFIED="1370436199208" TEXT="Procesos relacionados con el cliente"/>
<node CREATED="1370436227773" ID="ID_1089596828" MODIFIED="1370436239223" TEXT="Dise&#xf1;o y desarrollo del producto"/>
</node>
</node>
<node CREATED="1370434479469" ID="ID_1072823028" MODIFIED="1516882683711" POSITION="right" TEXT="Medici&#xf3;n, An&#xe1;lisis y Mejora">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370461359789" ID="ID_1144240867" MODIFIED="1379956221439" TEXT="Medici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370461363669" ID="ID_791276453" MODIFIED="1370461369123" TEXT="Seguimiento"/>
<node CREATED="1370461369359" ID="ID_334868289" MODIFIED="1370461376364" TEXT="Auditor&#xed;as internas"/>
</node>
<node CREATED="1370461408525" ID="ID_1927381618" MODIFIED="1379956222367" TEXT="An&#xe1;lisis">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370461416196" ID="ID_936048688" MODIFIED="1370461427452" TEXT="Satisfacci&#xf3;n del cliente"/>
<node CREATED="1370461428121" ID="ID_395366902" MODIFIED="1370461438507" TEXT="Conformidad con los requisitos"/>
</node>
<node CREATED="1370461440562" ID="ID_1293003625" MODIFIED="1379956223207" TEXT="Mejora">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370461446772" ID="ID_918802994" MODIFIED="1370461454343" TEXT="Acciones correctivas"/>
<node CREATED="1370461455137" ID="ID_1958459243" MODIFIED="1370461460734" TEXT="Acciones preventivas"/>
</node>
</node>
</node>
</map>
