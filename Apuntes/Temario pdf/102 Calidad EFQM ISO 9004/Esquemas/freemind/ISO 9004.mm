<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1370530406504" ID="ID_793733062" MODIFIED="1378233755749" TEXT="ISO 9004">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372924735815" ID="ID_121847398" MODIFIED="1383495802950" POSITION="right" TEXT="Se trata de una norma de uso interno.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375349014715" ID="ID_1582725010" MODIFIED="1381085931303" POSITION="right" STYLE="bubble" TEXT="Es una gu&#xed;a para los servicios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372924713734" ID="ID_857009329" MODIFIED="1381085934791" POSITION="right" STYLE="bubble" TEXT="Proporciona INSTRUCCIONES para mejorar el rendimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374831384133" ID="ID_1270990792" MODIFIED="1378237771641" POSITION="right" TEXT="Principios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374831388840" ID="ID_1370036587" MODIFIED="1383495661911" TEXT="Enfoque al cliente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1374831395162" ID="ID_23004191" MODIFIED="1383495663018" TEXT="Liderazgo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1374831400070" ID="ID_1762577706" MODIFIED="1383495663564" TEXT="Participaci&#xf3;n del personal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1374831407265" ID="ID_1831958412" MODIFIED="1383495664422" TEXT="Enfoque basado en procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1374831421380" ID="ID_6840547" MODIFIED="1383495664968" TEXT="Enfoque de sistema para la gesti&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1374831430936" ID="ID_659604988" MODIFIED="1383495665811" TEXT="Mejora continua">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1374831436596" ID="ID_730777294" MODIFIED="1383495666544" TEXT="Enfoque basado en hechos para la toma de decisiones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-7"/>
</node>
<node CREATED="1374831450450" ID="ID_657235113" MODIFIED="1383495667324" TEXT="Relaciones mutuamente beneficiosas para el proveedor">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-8"/>
</node>
</node>
<node CREATED="1372924758647" ID="ID_830664979" MODIFIED="1383495791297" POSITION="right" TEXT="Ciclo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370530537591" ID="ID_60404984" MODIFIED="1378233745188" TEXT="Responsabilidad de la direcci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370530556875" ID="ID_581267803" MODIFIED="1516882639902" TEXT="Gesti&#xf3;n de los recursos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370530567381" ID="ID_256145909" MODIFIED="1370530571954" TEXT="Personal"/>
<node CREATED="1370530572380" ID="ID_1266812146" MODIFIED="1370530580094" TEXT="Infraestructura"/>
<node CREATED="1370530580445" ID="ID_1543536474" MODIFIED="1370530586488" TEXT="Ambiente de trabajo"/>
<node CREATED="1370530603370" ID="ID_1113878017" MODIFIED="1370530606764" TEXT="Informaci&#xf3;n"/>
<node CREATED="1370530607121" ID="ID_1960793150" MODIFIED="1370530614483" TEXT="Proveedores y Alianzas"/>
<node CREATED="1370530614952" ID="ID_232201928" MODIFIED="1370530620359" TEXT="Recursos naturales"/>
<node CREATED="1370530620785" ID="ID_132955635" MODIFIED="1370530628892" TEXT="Recursos financieros"/>
</node>
<node CREATED="1370530631114" ID="ID_456462249" MODIFIED="1516882642360" TEXT="Realizaci&#xf3;n del producto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370530643368" ID="ID_938998844" MODIFIED="1370530661816" TEXT="Procesos relacionados con las partes interesadas"/>
<node CREATED="1370530662231" ID="ID_653675420" MODIFIED="1370530670309" TEXT="Proceso de dise&#xf1;o y desarrollo"/>
<node CREATED="1370530670635" ID="ID_1062491158" MODIFIED="1370530676125" TEXT="Procesos de compra"/>
<node CREATED="1370530688941" ID="ID_158290052" MODIFIED="1370530712441" TEXT="Procesos de producci&#xf3;n y prestaci&#xf3;n de servicio"/>
<node CREATED="1370530714141" ID="ID_275938200" MODIFIED="1370530725163" TEXT="Procesos de seguimiento y medici&#xf3;n"/>
</node>
<node CREATED="1370530728496" ID="ID_562084370" MODIFIED="1516882643731" TEXT="Medici&#xf3;n, an&#xe1;lisis y mejora">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370530760018" ID="ID_519560346" MODIFIED="1370530768744" TEXT="El desempe&#xf1;o del sistema"/>
<node CREATED="1370530769024" ID="ID_986338464" MODIFIED="1370530771808" TEXT="Los procesos"/>
<node CREATED="1370530772223" ID="ID_1870867710" MODIFIED="1370530775641" TEXT="El producto"/>
<node CREATED="1370530776133" ID="ID_882515951" MODIFIED="1370530786245" TEXT="Las partes interesadas"/>
<node CREATED="1370530833297" ID="ID_1558240968" MODIFIED="1370530837306" TEXT="Mejoras">
<node CREATED="1370530837307" ID="ID_1691517420" MODIFIED="1370530842403" TEXT="Acci&#xf3;n correctiva"/>
<node CREATED="1370530843307" ID="ID_1132839607" MODIFIED="1370530868749" TEXT="Prevenci&#xf3;n de p&#xe9;rdidas"/>
<node CREATED="1370530869101" ID="ID_636590287" MODIFIED="1370530880698" TEXT="Mejora contin&#xfa;a de la organizaci&#xf3;n"/>
</node>
</node>
</node>
<node CREATED="1370530894692" ID="ID_1124128936" MODIFIED="1378237784480" POSITION="left" TEXT="Autoevaluaci&#xf3;n del modelo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370532872516" ID="ID_1817350255" MODIFIED="1382349325116" TEXT="1.-Sin aproximaci&#xf3;n formal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370532901928" ID="ID_1565684577" MODIFIED="1382349326915" TEXT="2.-Aproximaci&#xf3;n reactiva">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370532910387" ID="ID_989930293" MODIFIED="1382349334754" TEXT="3.-Aproximaci&#xf3;n del sistema formal estable">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370532921034" ID="ID_574469027" MODIFIED="1382349338339" TEXT="4.-Enf&#xe1;sis en la mejora continua">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370532931478" ID="ID_1847132580" MODIFIED="1382349339219" TEXT="5.-Desempe&#xf1;o de &quot;mejor en su clase&quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</map>
