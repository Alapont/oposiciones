<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1370077400673" ID="ID_733661335" MODIFIED="1378203944276" TEXT="Metodolog&#xed;as">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370077408468" ID="ID_779157388" MODIFIED="1384520479214" POSITION="right" TEXT="Clasificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370077413238" ID="ID_425944274" MODIFIED="1384618718985" TEXT="Desarrollo estructurado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370077589285" FOLDED="true" ID="ID_427781312" MODIFIED="1384618787053" TEXT="Orientadas a procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370077628381" ID="ID_511034295" MODIFIED="1380986187642" TEXT="Diccionario de Datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370077616539" ID="ID_184241726" MODIFIED="1380986193944" TEXT="Diagramas de Flujo de Datos (DFD)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370077633776" ID="ID_1449652224" MODIFIED="1383128693620" TEXT="Especificaciones de procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370078381796" ID="ID_375770948" MODIFIED="1378203578348" TEXT="De Marco, Gane&amp;Sarson, Yourdon/Constantine">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370077656238" ID="ID_1824879234" MODIFIED="1384521576135" TEXT="Orientadas a datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370078102369" ID="ID_269349511" MODIFIED="1384639264386" TEXT="Caracter&#xed;sticas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370078108655" ID="ID_1995027629" MODIFIED="1378203439759" TEXT="Definici&#xf3;n de la estructura de datos (DED)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370078169785" ID="ID_1341520443" MODIFIED="1370078187594" TEXT="La estructura de control se deriva de la de datos"/>
<node BACKGROUND_COLOR="#ff6666" CREATED="1370078189168" ID="ID_1786237665" MODIFIED="1378203434550" TEXT="Se representa mediante secuencia, selecci&#xf3;n y repetici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370078215108" ID="ID_913117943" MODIFIED="1378203446926" TEXT="El dise&#xf1;o l&#xf3;gico precede y est&#xe1; separado del f&#xed;sico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370077664218" ID="ID_1667841130" MODIFIED="1384639282877" TEXT="Orientadas a Datos Jer&#xe1;rquicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370077747055" ID="ID_204678437" MODIFIED="1384618801867" TEXT="Warnier-Orr">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370077758603" ID="ID_987455207" MODIFIED="1378203462231" TEXT="CLP (Construcci&#xf3;n L&#xf3;gica de Programas)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
<node CREATED="1370077807256" ID="ID_1189748571" MODIFIED="1378203464391" TEXT="DSED (Desarrollo de Sistemas Estructurados de Datos)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
<node CREATED="1374677611200" ID="ID_1897680241" MODIFIED="1378203471632" TEXT="Construcciones b&#xe1;sicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374677643281" ID="ID_124430932" MODIFIED="1378203485168" TEXT="Jerarqu&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374677648853" ID="ID_306599013" MODIFIED="1378203491777" TEXT="Secuencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374677663045" ID="ID_1205647996" MODIFIED="1378203492753" TEXT="Repetici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374677679039" ID="ID_19843596" MODIFIED="1383128921125" TEXT="Alternancia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1374677688469" ID="ID_1695265334" MODIFIED="1378203474464" TEXT="Construcciones avanzadas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374677696371" ID="ID_764781095" MODIFIED="1374677700281" TEXT="Concurrencia"/>
<node CREATED="1374677700807" ID="ID_145422827" MODIFIED="1374677703665" TEXT="Recursi&#xf3;n"/>
</node>
</node>
</node>
<node CREATED="1374677739835" ID="ID_1127282705" MODIFIED="1382185323886" TEXT="Se identifican los resultados y se va hacia atr&#xe1;s">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370077843098" ID="ID_1193916010" MODIFIED="1384618825413" TEXT="Jackson">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370077849927" ID="ID_519985550" MODIFIED="1384521732412" TEXT="JSP (Jackson Structured Programming)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374677861026" ID="ID_1525608006" MODIFIED="1378203422686" TEXT="Desarrollo de programas individuales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374677880101" ID="ID_333422080" MODIFIED="1378203526770" TEXT="Como procesos secuenciales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374679240451" ID="ID_575536096" MODIFIED="1384522013238" TEXT="Componentes tipo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374679247058" ID="ID_1258268729" MODIFIED="1374679255979" TEXT="Operaciones fundamentales"/>
<node CREATED="1374679256330" ID="ID_1966440103" MODIFIED="1378203506761" TEXT="Secuencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374679264462" ID="ID_730727038" MODIFIED="1378203508937" TEXT="Selecci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374679259967" ID="ID_138013609" MODIFIED="1378203507673" TEXT="Iteraci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1370077896574" ID="ID_206186260" MODIFIED="1383150196255" TEXT="JSD (Jackson System Development)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374677921666" ID="ID_632164481" MODIFIED="1378203429117" TEXT="Desarrollo de sistemas completos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374677962901" ID="ID_155716724" MODIFIED="1378203529458" TEXT="Puede extenderse a varios sistemas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374678276865" ID="ID_849045949" MODIFIED="1384521807332" TEXT="Principios de operaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374678285838" ID="ID_1388469627" MODIFIED="1382185364646" TEXT="El desarrollo debe comenzar con el modelado del mundo real">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374678348687" ID="ID_1599669442" MODIFIED="1382185366838" TEXT="El modelo debe ser ordenado en el tiempo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374678397792" ID="ID_1555558443" MODIFIED="1382185367611" TEXT="El modo de implementaci&#xf3;n consiste en pasar de las especificaciones a los procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1374678492205" ID="ID_491485145" MODIFIED="1374678495890" TEXT="Pasos">
<node CREATED="1374678495892" ID="ID_540769178" MODIFIED="1378203538410" TEXT="Etapa de modelado (an&#xe1;lisis)">
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1374678512319" ID="ID_634517226" MODIFIED="1378203540491" TEXT="Etapa de red (dise&#xf1;o)">
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1374678523326" ID="ID_270915298" MODIFIED="1378203542299" TEXT="Etapa de implementaci&#xf3;n (realizaci&#xf3;n)">
<icon BUILTIN="full-3"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1370077690508" FOLDED="true" ID="ID_344421497" MODIFIED="1384618957774" TEXT="Orientadas a Datos no Jer&#xe1;rquicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372669026713" ID="ID_944955916" MODIFIED="1378203609702" TEXT="Preponderancia total de los datos frente a los procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370077948949" ID="ID_1816997790" MODIFIED="1370102158650" TEXT="Fases">
<node CREATED="1370077958358" ID="ID_1051183664" MODIFIED="1370077963717" TEXT="Planificaci&#xf3;n "/>
<node CREATED="1370077964352" ID="ID_187696563" MODIFIED="1370077967475" TEXT="An&#xe1;lisis"/>
<node CREATED="1370077967782" ID="ID_462521939" MODIFIED="1370077970290" TEXT="Dise&#xf1;o"/>
<node CREATED="1370077970517" ID="ID_848830718" MODIFIED="1370077974690" TEXT="Construcci&#xf3;n"/>
</node>
<node CREATED="1370122417495" ID="ID_1495628617" MODIFIED="1370122454404" TEXT="Ejemplos: Ingenier&#xed;a de la Informaci&#xf3;n"/>
<node CREATED="1370122458351" ID="ID_157421933" MODIFIED="1378203593581" TEXT="James Martin, Clive Finkelstein">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1370084719524" ID="ID_816927418" MODIFIED="1384522087715" TEXT="Ejemplos">
<node CREATED="1370077464434" ID="ID_1824668286" MODIFIED="1383128712661" TEXT="M&#xe9;trica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370077457046" ID="ID_1982420954" MODIFIED="1383128722397" TEXT="MERISE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370077453072" ID="ID_1535455233" MODIFIED="1383128716525" TEXT="SSADM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1370077422402" FOLDED="true" ID="ID_1922142049" MODIFIED="1384619013611" TEXT="Desarrollo orientado a objetos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370078482966" ID="ID_214893139" MODIFIED="1380985791884" TEXT="Enfoque Puro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370077475125" ID="ID_1671921788" MODIFIED="1378203741715" TEXT="Booch (OOAD)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370078513216" ID="ID_328921001" MODIFIED="1370078523144" TEXT="CRC/RDD"/>
</node>
<node CREATED="1370078526706" ID="ID_1416508304" MODIFIED="1379585041453" TEXT="Enfoque Evolutivo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370077479025" ID="ID_1831077227" MODIFIED="1383129237707" TEXT="Jim Rumbaugh (OMT)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370077484350" ID="ID_1547961820" MODIFIED="1378203660128" TEXT="UML (Booch, Jackobson y Rumbaugh)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370077481451" FOLDED="true" ID="ID_358320789" MODIFIED="1383129719375" TEXT="RUP (Evoluci&#xf3;n de OOAD, OMT y OOSE, de Jackobson)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374678594889" ID="ID_1721925624" MODIFIED="1374678608116">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Development-iterative.gif" />
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1370078575298" ID="ID_1382069362" MODIFIED="1370078605860" TEXT="Identificar conceptos del dominio"/>
<node CREATED="1370078606129" ID="ID_1342882365" MODIFIED="1370078619292" TEXT="Poca separaci&#xf3;n entre an&#xe1;lisis y dise&#xf1;o"/>
<node CREATED="1370078642989" ID="ID_1409397940" MODIFIED="1370078652067" TEXT="Reutilizaci&#xf3;n"/>
<node CREATED="1370078653664" ID="ID_810841258" MODIFIED="1370078685134" TEXT="Alto grado de iteraci&#xf3;n y solapamiento"/>
</node>
<node CREATED="1370078899905" FOLDED="true" ID="ID_541151182" MODIFIED="1384619016200" TEXT="Metodolog&#xed;as de Tiempo Real">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370084769680" ID="ID_493118704" MODIFIED="1378203671600" TEXT="DART">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370077431807" FOLDED="true" ID="ID_768608643" MODIFIED="1384619072339" TEXT="M&#xe9;todos &#xc1;giles">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370078945688" ID="ID_1273434157" MODIFIED="1384522172511" TEXT="12 principios del Manifiesto Agil">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370079269314" ID="ID_1371864380" MODIFIED="1380985946371" TEXT="Interacci&#xf3;n frecuente con el cliente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370079279631" ID="ID_779668287" MODIFIED="1378203704010" TEXT="Personal motivado y preparado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370079319873" ID="ID_1673232016" MODIFIED="1378203705113" TEXT="Desarrollo sostenido de software">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370079352448" ID="ID_143888339" MODIFIED="1378203706681" TEXT="La simplicidad es esencial">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370079365407" FOLDED="true" ID="ID_1951889782" MODIFIED="1384522494363" TEXT="Suposiciones">
<node CREATED="1370079379107" ID="ID_311273667" MODIFIED="1384522267028" TEXT="Requisitos no predecibles"/>
<node CREATED="1370079390331" ID="ID_218295096" MODIFIED="1380985874470" TEXT="Se intercalan dise&#xf1;o y construcci&#xf3;n"/>
</node>
<node CREATED="1370079408972" FOLDED="true" ID="ID_641511687" MODIFIED="1384522495003" TEXT="Habilidades">
<node CREATED="1370079413585" ID="ID_1313874790" MODIFIED="1370079416907" TEXT="Competencia"/>
<node CREATED="1370079417397" ID="ID_1597299190" MODIFIED="1370079422926" TEXT="Enfoque Com&#xfa;n"/>
<node CREATED="1370079423229" ID="ID_1742806346" MODIFIED="1370079427541" TEXT="Colaboraci&#xf3;n"/>
<node CREATED="1370079428219" ID="ID_1952287095" MODIFIED="1370079441651" TEXT="Habilidad para tomar decisiones"/>
<node CREATED="1370079444188" ID="ID_662931595" MODIFIED="1370079454635" TEXT="Capacidad para resolver problemas"/>
<node CREATED="1370079475301" ID="ID_275204957" MODIFIED="1370079493643" TEXT="Confianza y respeto mutuo"/>
<node CREATED="1370079484891" ID="ID_1403008551" MODIFIED="1370079489859" TEXT="Autoorganizaci&#xf3;n"/>
</node>
<node CREATED="1370079512542" ID="ID_1820030603" MODIFIED="1384522328262" TEXT="Diferencias con las cl&#xe1;sicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370079526121" ID="ID_1439893244" MODIFIED="1378203692905" TEXT="M&#xe9;todos adaptables en vez de predictivos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370079543448" ID="ID_62873509" MODIFIED="1378203697393" TEXT="Orientados a personas en vez de procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370079573630" ID="ID_1786668189" MODIFIED="1378203680849" TEXT="Ejemplos: XP, DAS, MDSD, DCC, SCRUM, Cristal, etc">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370079620515" FOLDED="true" ID="ID_126096332" MODIFIED="1384619074782" TEXT="Basadas en el conocimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370079631017" ID="ID_263239934" MODIFIED="1378203729499" TEXT="KADS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1370079662477" ID="ID_1292051943" MODIFIED="1384520478144" POSITION="right" TEXT="Herramientas CASE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370079689154" FOLDED="true" ID="ID_885434349" MODIFIED="1384619128949" TEXT="Clasificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370079698098" ID="ID_457779369" MODIFIED="1384619102353" TEXT="Por la fase">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370079704795" ID="ID_438782337" MODIFIED="1378203778676" TEXT="Upper CASE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370079721297" ID="ID_1640368072" MODIFIED="1378203908410" TEXT="Planificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1370079740371" ID="ID_1754437267" MODIFIED="1378203780421" TEXT="Middle CASE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370079748622" ID="ID_1908733980" MODIFIED="1378203907514" TEXT="An&#xe1;lisis y Dise&#xf1;o">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1370079771503" ID="ID_550281950" MODIFIED="1378203781381" TEXT="Lower CASE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370079784014" ID="ID_1236803255" MODIFIED="1378203903802" TEXT="Generaci&#xf3;n de c&#xf3;digo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
</node>
<node CREATED="1370079798325" ID="ID_589854463" MODIFIED="1378203904938" TEXT="Pruebas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1370079847976" ID="ID_1942037361" MODIFIED="1378203782805" TEXT="Ingenier&#xed;a Inversa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370079858580" FOLDED="true" ID="ID_1287783934" MODIFIED="1384619112725" TEXT="Ingenier&#xed;a de Ida y Vuelta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370079930127" ID="ID_1446079489" MODIFIED="1370079950240" TEXT="El dise&#xf1;o y el c&#xf3;digo fuente siempre est&#xe1;n en corcondancia"/>
</node>
</node>
<node CREATED="1370079965785" FOLDED="true" ID="ID_797755199" MODIFIED="1384619127617" TEXT="Por el soporte">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370079971932" ID="ID_175123722" MODIFIED="1370079977945" TEXT="Tool CASE"/>
<node CREATED="1370079978326" ID="ID_1492682566" MODIFIED="1378203794173" TEXT="I-CASE: Desarrollo completo de un sistema de informaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372613547479" ID="ID_1045695472" MODIFIED="1380986016962" TEXT="META-CASE: permiten el desarrollo de herramientas a medida">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376896149237" ID="ID_1518399657" MODIFIED="1378203796301" TEXT="CAST: Herramientas de soporte a la prueba">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376895983218" ID="ID_362949506" MODIFIED="1378203797389" TEXT="IPSE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376896355652" ID="ID_1677886537" MODIFIED="1378203831199" TEXT="Gesti&#xf3;n de proyectos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376896365815" ID="ID_1921876227" MODIFIED="1378287996705" TEXT="Est&#xe1;ndar de intercambio de datos entre herramientas CASE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1370080005716" ID="ID_730440370" MODIFIED="1384619129829" TEXT="Tipos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370080010628" ID="ID_530491013" MODIFIED="1370080021188" TEXT="Herramientas de Diagramaci&#xf3;n"/>
<node CREATED="1370080023656" ID="ID_962283247" MODIFIED="1370080035673" TEXT="Generadores de Pantallas e Informes"/>
<node CREATED="1370080046375" ID="ID_588309606" MODIFIED="1370080057821" TEXT="An&#xe1;lisis, Dise&#xf1;o y Verificaci&#xf3;n"/>
<node CREATED="1370080073307" ID="ID_1814241690" MODIFIED="1370080079983" TEXT="Herramientas de Programaci&#xf3;n"/>
<node CREATED="1370080080377" ID="ID_1380440345" MODIFIED="1370080088095" TEXT="Generadores de C&#xf3;digo"/>
<node CREATED="1370080088790" ID="ID_341339708" MODIFIED="1370080094130" TEXT="Mantenimiento"/>
<node CREATED="1370080095159" ID="ID_837254702" MODIFIED="1370080105295" TEXT="Planificaci&#xf3;n y Gesti&#xf3;n de Proyectos"/>
</node>
<node CREATED="1370080136415" ID="ID_1953714211" MODIFIED="1384522666204" TEXT="Ventajas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370080142161" ID="ID_220160376" MODIFIED="1378203847391" TEXT="Aumento de la Productividad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370080154625" ID="ID_1085562163" MODIFIED="1378203849063" TEXT="Reducci&#xf3;n del tiempo de Desarrollo y Costes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370080211040" ID="ID_1049956512" MODIFIED="1378203851904" TEXT="Mejora de la Calidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370080217350" ID="ID_728782688" MODIFIED="1378203853016" TEXT="Mejora de la Documentaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370080227282" ID="ID_508553153" MODIFIED="1378203869392" TEXT="Reducci&#xf3;n del Mantenimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370080261295" ID="ID_1259321861" MODIFIED="1384522839166" TEXT="Costes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370080264162" ID="ID_1829075443" MODIFIED="1378203881817" TEXT="Hardware y Sistemas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370080273254" ID="ID_305532122" MODIFIED="1378203883225" TEXT="Software">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370080276697" ID="ID_242226373" MODIFIED="1378203884209" TEXT="Formaci&#xf3;n y Consultor&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370080299121" ID="ID_1434307031" MODIFIED="1378203885745" TEXT="Aspectos Organizacionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</map>
