<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1370073319528" ID="ID_514996410" MODIFIED="1378200276786" TEXT="An&#xe1;lisis de requisitos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370075682715" ID="ID_1726010850" MODIFIED="1385575733955" POSITION="right" TEXT="Actividades del an&#xe1;lisis">
<node CREATED="1370075694027" ID="ID_1918559607" MODIFIED="1378200281899" TEXT="Educci&#xf3;n de Requisitos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370075704074" ID="ID_550359020" MODIFIED="1378200282875" TEXT="An&#xe1;lisis de Requisitos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370075711174" ID="ID_1930842971" MODIFIED="1378200284099" TEXT="Representaci&#xf3;n de Requisitos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370075720487" ID="ID_1165024474" MODIFIED="1378200285555" TEXT="Validaci&#xf3;n de Requisitos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1370073353643" ID="ID_327316544" MODIFIED="1385575915949" POSITION="right" STYLE="bubble" TEXT="Clasificaci&#xf3;n de los requisitos">
<node CREATED="1370073363714" ID="ID_1788208131" MODIFIED="1375107088917" TEXT="SOMMERVILLE">
<node CREATED="1370073384168" ID="ID_663642468" MODIFIED="1383132422533" TEXT="Nivel de sistema">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Referencia no s&#243;lo al software, sino a todas las partes implicadas.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1382177830926" ID="ID_1319131969" MODIFIED="1382177842803" TEXT="Requerimientos funcionales a nivel abstracto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370073416910" ID="ID_178888730" MODIFIED="1383132426783" TEXT="Propiedades no funcionales del sistema: seguridad, rendimiento, disponibilidad.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370073423692" ID="ID_1047873327" MODIFIED="1380972420551" TEXT="Caracter&#xed;sticas que no debe mostrar el sistema">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370073389735" ID="ID_392069309" MODIFIED="1378200305292" TEXT="Nivel de software">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370073448567" ID="ID_1730465458" MODIFIED="1378200299123" TEXT="Requerimientos funcionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370073456889" ID="ID_417048810" MODIFIED="1378200300220" TEXT="Requerimientos no funcionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370073491797" ID="ID_871519256" MODIFIED="1375107088917" TEXT="Seguridad"/>
<node CREATED="1370073496921" ID="ID_1952828181" MODIFIED="1375107088917" TEXT="Rendimiento"/>
<node CREATED="1370073505122" ID="ID_720706493" MODIFIED="1375107088917" TEXT="Entorno"/>
<node CREATED="1370073512131" ID="ID_116305673" MODIFIED="1375107088917" TEXT="Etc."/>
</node>
</node>
<node CREATED="1370073534957" ID="ID_1258307564" MODIFIED="1382177677678" TEXT="Requisitos de usuario">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Declaraciones abstractas de los requerimientos del usuario final o cliente
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370073592997" ID="ID_228613946" MODIFIED="1375107088917" TEXT="Lenguaje natural o diagramas"/>
</node>
<node CREATED="1382177596383" ID="ID_1146728585" MODIFIED="1382177718168" TEXT="Requisitos del sistema">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Descripci&#243;n detallada de la funcionalidad a proporcionar.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1370073676596" ID="ID_1948119700" MODIFIED="1385576038267" POSITION="right" TEXT="Caracter&#xed;sticas">
<node CREATED="1370073683841" ID="ID_1318742490" MODIFIED="1378200314940" TEXT="Completos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370073693102" ID="ID_560690864" MODIFIED="1378200315980" TEXT="Consistentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370073701619" ID="ID_1023560165" MODIFIED="1378200317148" TEXT="No ambiguos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370073710463" ID="ID_606891153" MODIFIED="1378200318244" TEXT="Verificables">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370073715622" ID="ID_1911932071" MODIFIED="1378200319237" TEXT="Comprensibles">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370073720254" ID="ID_1873228991" MODIFIED="1378200320037" TEXT="F&#xe1;ciles de Modificar">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370073856376" ID="ID_1471905279" MODIFIED="1385576045332" POSITION="right" TEXT="Problemas">
<node CREATED="1370073867150" ID="ID_18666653" MODIFIED="1370073934733" TEXT="Definici&#xf3;n del Alcance">
<node CREATED="1370073877280" ID="ID_568847755" MODIFIED="1370073881649" TEXT="La organizaci&#xf3;n"/>
<node CREATED="1370073892620" ID="ID_821346321" MODIFIED="1370073895634" TEXT="El entorno"/>
<node CREATED="1370073895977" ID="ID_1142778628" MODIFIED="1370073900916" TEXT="El proyecto"/>
</node>
<node CREATED="1370073905467" ID="ID_1643892296" MODIFIED="1370073925572" TEXT="Problemas de Comprensi&#xf3;n"/>
<node CREATED="1370073912746" ID="ID_1280440485" MODIFIED="1370073920633" TEXT="Problemas de Volatilidad"/>
</node>
<node CREATED="1370073986671" FOLDED="true" ID="ID_788484936" MODIFIED="1384605329747" POSITION="right" TEXT="T&#xe9;cnicas">
<node CREATED="1375348796934" ID="ID_710020878" MODIFIED="1378200370551" STYLE="bubble" TEXT="Herramientas: EXCELERATOR, SADT, TAGS, etc.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1380270130940" ID="ID_1807465991" MODIFIED="1384605248037" TEXT="De alto nivel">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1370073996179" FOLDED="true" ID="ID_518671122" MODIFIED="1383132789415" STYLE="bubble" TEXT="JAD (Joint Application Design)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370074064572" ID="ID_265683126" MODIFIED="1375107158291" TEXT="Pasos">
<node CREATED="1370074069617" ID="ID_1715273845" MODIFIED="1375107158291" TEXT="Definici&#xf3;n del Proyecto"/>
<node CREATED="1370074077840" ID="ID_43988211" MODIFIED="1378200335566" TEXT="Investigaci&#xf3;n">
<node CREATED="1370074218088" ID="ID_343433040" MODIFIED="1375107158291" TEXT="Especificaciones preliminares">
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1370074081977" ID="ID_936996466" MODIFIED="1378200336462" TEXT="Preparaci&#xf3;n">
<node CREATED="1370074169392" ID="ID_1991297620" MODIFIED="1375107158291" TEXT="Documento de trabajo">
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1370074091639" ID="ID_781904725" MODIFIED="1378200338912" TEXT="Sesi&#xf3;n JAD o JRP">
<node CREATED="1370074135195" ID="ID_499539153" MODIFIED="1375107158291" TEXT="roles">
<node CREATED="1370074141992" ID="ID_1663027934" MODIFIED="1375107158291" TEXT="jefe"/>
<node CREATED="1370074144517" ID="ID_314483733" MODIFIED="1375107158291" TEXT="acta"/>
</node>
<node CREATED="1370074189144" ID="ID_1314338732" MODIFIED="1375107158291" TEXT="Especificaciones formales">
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1370074102017" ID="ID_1483101334" MODIFIED="1375107158291" TEXT="Documento Final"/>
</node>
</node>
<node CREATED="1370074027996" ID="ID_1010301598" MODIFIED="1378200332805" TEXT="JRP (Joint Requirements Planning)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370074285851" ID="ID_1108391839" MODIFIED="1370074296457" TEXT="Igual que JAD"/>
<node CREATED="1372666487900" ID="ID_733609156" MODIFIED="1372666497079" TEXT="M&#xe1;s dirigido a directivos"/>
</node>
<node CREATED="1370074298880" ID="ID_1493613298" MODIFIED="1378200344349" TEXT="Prototipos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1380270207113" ID="ID_864535429" MODIFIED="1380270218746" TEXT="Entorno de Bucles Adaptativo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1380270222638" ID="ID_1201639010" MODIFIED="1380270271079" TEXT="Factores Cr&#xed;ticos del &#xe9;xito">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1380270023323" ID="ID_1123148639" MODIFIED="1384605254014" TEXT="De bajo nivel">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370074404473" FOLDED="true" ID="ID_1589746247" MODIFIED="1384605262453" TEXT="Entrevistas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370074415358" ID="ID_781406908" MODIFIED="1370074418333" TEXT="Fases">
<node CREATED="1370074418336" ID="ID_1522572189" MODIFIED="1379439429210" TEXT="Identificaci&#xf3;n de Candidatos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1370074427496" ID="ID_1482755601" MODIFIED="1379439434545" TEXT="Preparaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1370074445395" ID="ID_1958945165" MODIFIED="1379439439693" TEXT="Ejecuci&#xf3;n de la Entrevista">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1370074452534" ID="ID_1411641747" MODIFIED="1379439446885" TEXT="Actividades Posteriores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
</node>
</node>
<node CREATED="1370074463935" ID="ID_1861331913" MODIFIED="1370074471072" TEXT="Tipos de preguntas">
<node CREATED="1370074471073" ID="ID_983793439" MODIFIED="1370074474354" TEXT="Generales"/>
<node CREATED="1370074485950" ID="ID_132022488" MODIFIED="1370074491693" TEXT="Abiertas"/>
<node CREATED="1370074492212" ID="ID_623187563" MODIFIED="1370074497899" TEXT="Cerradas"/>
<node CREATED="1370074511160" ID="ID_990302792" MODIFIED="1370074517550" TEXT="Preguntas que elevan el nivel"/>
<node CREATED="1370074517785" ID="ID_1281663467" MODIFIED="1370074521168" TEXT="De Contexto"/>
</node>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1370074534889" FOLDED="true" ID="ID_526923037" MODIFIED="1384605323609" STYLE="bubble" TEXT="PIECES: 6 categor&#xed;as de aspecto que el analista debe estudiar con los usuarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374679784738" ID="ID_1536869911" MODIFIED="1375107062269" TEXT="6 categor&#xed;as de aspecto que el analista debe estudiar con los usuarios">
<node CREATED="1370074624513" ID="ID_462482398" MODIFIED="1378200362286" TEXT="Rendimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370074633784" ID="ID_803744857" MODIFIED="1378200363398" TEXT="Informaci&#xf3;n y Datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370074646379" ID="ID_1030103807" MODIFIED="1378200364614" TEXT="Econom&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370074664744" ID="ID_738684946" MODIFIED="1378200365542" TEXT="Control">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370074667932" ID="ID_653486860" MODIFIED="1378200366366" TEXT="Eficiencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370074670717" ID="ID_548350156" MODIFIED="1378200367254" TEXT="Servicios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1374679854081" ID="ID_131486767" MODIFIED="1375107062269" TEXT="Se especifican preguntas para cada uno de estos aspectos"/>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1370074305606" ID="ID_675428944" MODIFIED="1384605267577" STYLE="bubble" TEXT="Brainstorming">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370074330598" ID="ID_402848561" MODIFIED="1375107145826" TEXT="Ventajas">
<node CREATED="1370074334068" ID="ID_181278406" MODIFIED="1375107145826" TEXT="M&#xfa;ltiples puntos de vista"/>
<node CREATED="1370074340995" ID="ID_594200941" MODIFIED="1375107145826" TEXT="Formula un problema de distintas formas"/>
</node>
<node CREATED="1370074363805" ID="ID_1731084948" MODIFIED="1375107145826" TEXT="Fases">
<node CREATED="1370074366778" ID="ID_964192058" MODIFIED="1379439404203" TEXT="Preparaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1370074374856" ID="ID_1905060289" MODIFIED="1379439409351" TEXT="Generaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1370074379007" ID="ID_1008571674" MODIFIED="1379439412144" TEXT="Consolidaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
</node>
</node>
<node CREATED="1380270255352" ID="ID_1193956928" MODIFIED="1380270273659" TEXT="Casos de uso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1380270259968" ID="ID_538957536" MODIFIED="1380270275920" TEXT="An&#xe1;lisis de mercado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1370074738206" FOLDED="true" ID="ID_703474771" MODIFIED="1384612823480" POSITION="right" TEXT="Validaci&#xf3;n de requerimientos">
<node CREATED="1370075005129" ID="ID_1826725392" MODIFIED="1370075017035" TEXT="T&#xe9;cnicas">
<node CREATED="1370075017036" ID="ID_108037250" MODIFIED="1370075031945" TEXT="Revisi&#xf3;n por equipo de revisores"/>
<node CREATED="1370075066830" ID="ID_278833016" MODIFIED="1370075080427" TEXT="Generaci&#xf3;n de prototipos"/>
<node CREATED="1370075080692" ID="ID_1060658635" MODIFIED="1370075084558" TEXT="Casos de prueba"/>
</node>
<node CREATED="1370075034471" ID="ID_1513306284" MODIFIED="1370075043059" TEXT="Tipos de verificaciones">
<node CREATED="1370074748625" ID="ID_912100086" MODIFIED="1370074798339" TEXT="Verificaciones de Validez"/>
<node CREATED="1370074757536" ID="ID_17781044" MODIFIED="1370074764945" TEXT="Verificaciones de Consistencia"/>
<node CREATED="1370074765354" ID="ID_132293034" MODIFIED="1370074775396" TEXT="Verificaciones de Completitud"/>
<node CREATED="1370074775937" ID="ID_180262171" MODIFIED="1370074782437" TEXT="Verificaciones de Realismo"/>
<node CREATED="1370074782886" ID="ID_810090468" MODIFIED="1370074792458" TEXT="Verificalidad"/>
</node>
</node>
<node CREATED="1370075113859" FOLDED="true" ID="ID_63991266" MODIFIED="1384612828111" POSITION="right" TEXT="Gesti&#xf3;n de requisitos">
<node CREATED="1370075126631" ID="ID_62820872" MODIFIED="1370075133010" TEXT="Control de cambios">
<node CREATED="1370075254407" ID="ID_1585571365" MODIFIED="1370075260176" TEXT="Pasos">
<node CREATED="1370075151261" ID="ID_1386338464" MODIFIED="1370075159365" TEXT="Propuestas de cambios"/>
<node CREATED="1370075159659" ID="ID_1570195937" MODIFIED="1370075176640" TEXT="An&#xe1;lisis de impactos"/>
<node CREATED="1370075185714" ID="ID_1788888584" MODIFIED="1370075190761" TEXT="Toma de decisiones"/>
<node CREATED="1370075196027" ID="ID_106128924" MODIFIED="1370075202393" TEXT="Comunicaci&#xf3;n"/>
<node CREATED="1370075202879" ID="ID_1097916944" MODIFIED="1370075212117" TEXT="Incorporaci&#xf3;n"/>
<node CREATED="1370075212574" ID="ID_1341336537" MODIFIED="1370075221657" TEXT="Medici&#xf3;n de la estabilidad"/>
</node>
</node>
<node CREATED="1370075133290" ID="ID_1311633036" MODIFIED="1370075239892" TEXT="Control de versiones"/>
<node CREATED="1370075308341" ID="ID_955330376" MODIFIED="1370075315687" TEXT="Trazabilidad de los requisitos">
<node CREATED="1370075428779" ID="ID_1835311551" MODIFIED="1370075432854" TEXT="Hacia adelante"/>
<node CREATED="1370075433245" ID="ID_730312499" MODIFIED="1370075437029" TEXT="Hacia atr&#xe1;s"/>
</node>
</node>
</node>
</map>
