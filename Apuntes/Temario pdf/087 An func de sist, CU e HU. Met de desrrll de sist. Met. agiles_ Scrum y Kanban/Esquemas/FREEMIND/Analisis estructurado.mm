<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1370106666870" ID="ID_1206309722" MODIFIED="1378200822846" TEXT="An&#xe1;lisis estructurado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384614664550" ID="ID_1568923451" MODIFIED="1384614773175" POSITION="right" TEXT="Objetivos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384614669021" ID="ID_1241141656" MODIFIED="1384614771779" TEXT="Describir lo que el CLIENTE quiere">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1384614681546" ID="ID_1376837766" MODIFIED="1384614770942" TEXT="Establecer la BASE para la creaci&#xf3;n de un dise&#xf1;o de software">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1384614711467" ID="ID_1031318567" MODIFIED="1384616307906" TEXT="Definir un conjunto de requisitos que se puedan VALIDAR   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
</node>
<node CREATED="1376643285431" ID="ID_247113049" MODIFIED="1384614662471" POSITION="right" TEXT="Evoluci&#xf3;n del an&#xe1;lisis estructurado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376643254053" ID="ID_93870255" MODIFIED="1378200611826" TEXT="De Marco">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376643262513" ID="ID_622925872" MODIFIED="1378200616065" TEXT="DFD">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384614908149" ID="ID_1743305406" MODIFIED="1384615072104" TEXT="Diagrama f&#xed;sico de flujo de datos del Sistema actual (que)">
<arrowlink DESTINATION="ID_411390793" ENDARROW="Default" ENDINCLINATION="26;0;" ID="Arrow_ID_936264616" STARTARROW="None" STARTINCLINATION="26;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1384614928897" ID="ID_411390793" MODIFIED="1384615075331" TEXT="Diagrama l&#xf3;gico de flujo de datos del sistema actual (como)">
<arrowlink DESTINATION="ID_58067719" ENDARROW="Default" ENDINCLINATION="25;0;" ID="Arrow_ID_1142107903" STARTARROW="None" STARTINCLINATION="25;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1384614944800" ID="ID_58067719" MODIFIED="1384615080585" TEXT="Diagrama l&#xf3;gico de flujo de datos del nuevo sistema (que)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1384614963326" ID="ID_1487423161" MODIFIED="1384615085473" TEXT="Diagrama f&#xed;sico del flujo de datos del nuevo sistema (como)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
</node>
</node>
<node CREATED="1376643366578" ID="ID_99374945" MODIFIED="1384509573250" TEXT="An&#xe1;lisis del sistema actual">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1376643304976" ID="ID_1903925791" MODIFIED="1378200619522" TEXT="Ward- Mellor">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376643312891" ID="ID_1721786455" MODIFIED="1382180504018" TEXT="A&#xf1;aden los Diagramas de Flujo de Control (DFC)   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376643325976" ID="ID_201663551" MODIFIED="1378200623938" TEXT="Aplicados a los Sistemas de Tiempo Real">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1376643341160" ID="ID_685696765" MODIFIED="1378200626337" TEXT="Yourdon">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384615152837" ID="ID_226354700" MODIFIED="1384616262068" TEXT="Loa analistas invert&#xed;an demasiado tiempo en la modelizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384615168361" ID="ID_463356260" MODIFIED="1384616277203" TEXT="PARALISIS DEL ANALISIS   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1376643349099" ID="ID_1648327842" MODIFIED="1378200628337" TEXT="Introduce el an&#xe1;lisis centrado en el nuevo sistema">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384615097494" ID="ID_747779851" MODIFIED="1384616254468" TEXT="Modelo f&#xed;sico de flujo de datos del sistema actual">
<node CREATED="1384615119950" ID="ID_1105095588" MODIFIED="1384616272334" TEXT="MODELO DE ENTORNO   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1384615222008" ID="ID_1455008787" MODIFIED="1384615235960" TEXT="Modelo l&#xf3;gico de datos"/>
<node CREATED="1384615236231" ID="ID_273596096" MODIFIED="1384615259113" TEXT="Diagramas l&#xf3;gicos de flujo de datos del nuevo sistema"/>
<node CREATED="1384615262930" ID="ID_329255288" MODIFIED="1384615286337" TEXT="Diagrama f&#xed;sicos del flujo de datos del nuevo sistema"/>
</node>
<node CREATED="1370107004961" ID="ID_1189434263" MODIFIED="1384613392352" TEXT="An&#xe1;lisis Esencial (Yourdon)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372671754206" ID="ID_203022643" MODIFIED="1384638529010" TEXT="MODELO AMBIENTAL: relaciones con el exterior">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372671821716" ID="ID_1944849130" MODIFIED="1378200898666" TEXT="Descripci&#xf3;n breve del proposito del sistema">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372671840868" ID="ID_1122921776" MODIFIED="1378200877507" TEXT="Diagrama de contexto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372671846914" ID="ID_1390256248" MODIFIED="1380974324461" TEXT="Lista de eventos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372671862055" ID="ID_817835840" MODIFIED="1384616295415" TEXT="Eventos orientados a flujos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372671869926" ID="ID_1558891463" MODIFIED="1384616296586" TEXT="Eventos temporales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372671874506" ID="ID_103943147" MODIFIED="1384616297075" TEXT="Eventos de control">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372671887979" ID="ID_164701370" MODIFIED="1378200883867" TEXT="Descripci&#xf3;n del Diccionario de Datos Inicial">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372671770377" ID="ID_1134835662" MODIFIED="1384638535770" TEXT="MODELO DE COMPORTAMIENTO: procesamiento interno">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372671928083" ID="ID_769381926" LINK="Diagrama%20de%20Flujo%20de%20Datos.mm" MODIFIED="1380974019603" TEXT="Conjunto de diagramas de flujo de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372671976160" ID="ID_1067979378" MODIFIED="1384615340659" TEXT="Subsistema">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372671979554" ID="ID_1858876647" MODIFIED="1384615342137" TEXT="Funci&#xf3;n primitiva">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372671990186" ID="ID_1233481765" MODIFIED="1384509554701" TEXT="Diccionario de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372672001225" ID="ID_665670468" MODIFIED="1378200893530" TEXT="Especificaciones de contexto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1370106724683" ID="ID_2003578" MODIFIED="1384509550208" POSITION="right" TEXT="T&#xe9;cnicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370106728612" FOLDED="true" ID="ID_726592544" MODIFIED="1385575567984" TEXT="Diccionario de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1372672133401" ID="ID_18446434" MODIFIED="1378200632537" TEXT="Los datos del diccionario se definen s&#xf3;lo una vez">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376583179773" ID="ID_1724821505" MODIFIED="1378200635577" TEXT="Describen y referencian todos los datos de un DFD">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372672173575" ID="ID_1313645073" MODIFIED="1380973752046" TEXT="Toda definici&#xf3;n debe describir como se compone este dato y el conjunto de valores que puede tomar">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372672704526" ID="ID_1747353699" MODIFIED="1384510075467" TEXT="Datos">
<node CREATED="1372672712055" ID="ID_1382280576" MODIFIED="1372672714443" TEXT="Nombre"/>
<node CREATED="1372672715028" ID="ID_266416930" MODIFIED="1372672716994" TEXT="Alias"/>
<node CREATED="1372672717376" ID="ID_913579620" MODIFIED="1372672722370" TEXT="Formato del Dato"/>
<node CREATED="1372672726107" ID="ID_51757432" MODIFIED="1372672732552" TEXT="Informaci&#xf3;n Adicional"/>
<node CREATED="1372672732918" ID="ID_370196382" MODIFIED="1372672743155" TEXT="Usuario y fecha de creaci&#xf3;n"/>
<node CREATED="1372672743662" ID="ID_1054315620" MODIFIED="1372672761451" TEXT="Usuario y fecha de &#xfa;ltima modificaci&#xf3;n"/>
<node CREATED="1372672761880" ID="ID_1569070822" MODIFIED="1372672773552" TEXT="Tipo de elemento"/>
</node>
<node CREATED="1372672836042" ID="ID_1220965349" MODIFIED="1372672849478" TEXT="Definici&#xf3;n del Formato de Datos">
<node CREATED="1372672859831" FOLDED="true" ID="ID_766160567" MODIFIED="1384613278820" TEXT="Formato BNF">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Backus-Naur Form (Como la RFC&#160;&#160;822 para el correo electr&#243;nico)
    </p>
  </body>
</html></richcontent>
<node CREATED="1372672926674" ID="ID_1801309898" MODIFIED="1372672979617" TEXT="&lt;Nombre_dato&gt;"/>
<node CREATED="1372673032902" ID="ID_724689573" MODIFIED="1372673143069" TEXT="Operador">
<node CREATED="1372673131757" ID="ID_1651616206" MODIFIED="1372673137124" TEXT="::="/>
<node CREATED="1372673146916" ID="ID_264546545" MODIFIED="1372673205881" TEXT="Secuencia: (+)"/>
<node CREATED="1372673158751" ID="ID_331702711" MODIFIED="1372673211171" TEXT="Selecci&#xf3;n: ([|])"/>
<node CREATED="1372673176079" ID="ID_1538610392" MODIFIED="1372673192244" TEXT="Iteraci&#xf3;n: (n{}m)"/>
<node CREATED="1372673225487" ID="ID_1025730295" MODIFIED="1372673239872" TEXT="Opci&#xf3;n: (())"/>
<node CREATED="1372673240832" ID="ID_1521355109" MODIFIED="1372673254453" TEXT="Comentario: (**)"/>
<node CREATED="1372673270907" ID="ID_1616196059" MODIFIED="1372673285465" TEXT="Identificador:(@)"/>
<node CREATED="1372673290840" ID="ID_1963461604" MODIFIED="1372673293711" TEXT="Alias"/>
</node>
<node CREATED="1372672979968" ID="ID_1802063549" MODIFIED="1372672989860" TEXT="&lt;Componentes&gt;">
<node CREATED="1372673008420" ID="ID_1516594965" MODIFIED="1372673017095" TEXT="Simples, elementales o primitivos"/>
<node CREATED="1372673017540" ID="ID_705902785" MODIFIED="1372673024343" TEXT="Compuestos o agregados"/>
</node>
</node>
</node>
<node CREATED="1372673346113" ID="ID_457902598" MODIFIED="1372673349429" TEXT="Tipos">
<node CREATED="1372673349429" ID="ID_806851538" MODIFIED="1378200734251" TEXT="Activos. Fuerzan a los programadores a utilizar las definiciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372673356458" ID="ID_1390935772" MODIFIED="1378200736739" TEXT="Pasivos. Permiten que los programadores puedan definir datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1372673508253" ID="ID_1219142802" MODIFIED="1383151738874" TEXT="Operaciones">
<node CREATED="1372673512739" ID="ID_823581028" MODIFIED="1378200758994" TEXT="Localizar todos los lugares donde est&#xe1; un dato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372673527726" ID="ID_721907273" MODIFIED="1378200762282" TEXT="Determinar si un determinado dato se usa o no">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372673548391" ID="ID_1446927103" MODIFIED="1378200763721" TEXT="Definici&#xf3;n exacta de un dato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372673562270" ID="ID_954579038" MODIFIED="1372673573910" TEXT="Listado de todos los elementos del diccionario"/>
<node CREATED="1372673574308" ID="ID_620645049" MODIFIED="1372673584933" TEXT="Descripci&#xf3;n de las relaciones entre los datos"/>
<node CREATED="1372673593757" ID="ID_1787468550" MODIFIED="1372673601293" TEXT="Hacer cruces con otros modelos"/>
<node CREATED="1372673716294" ID="ID_1479548235" MODIFIED="1372673720881" TEXT="Control de versiones"/>
</node>
<node CREATED="1372673726865" ID="ID_1272384890" MODIFIED="1376643715172" TEXT="Diccionario de Recursos de Informaci&#xf3;n: Toda la informaci&#xf3;n que circula por la organizaci&#xf3;n"/>
</node>
<node CREATED="1370106752793" ID="ID_1389772220" MODIFIED="1384613377782" TEXT="Partici&#xf3;n Funcional">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370106765783" FOLDED="true" ID="ID_392683808" LINK="Diagrama%20de%20Flujo%20de%20Datos.mm" MODIFIED="1384509528408" TEXT="Diagrama de Flujo de Datos (DFD)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370106850513" ID="ID_431695892" MODIFIED="1370106876113" TEXT="Especificaciones de Proceso"/>
</node>
<node CREATED="1370106816778" ID="ID_97822502" MODIFIED="1384509530499" TEXT="Diagrama de Flujos de Control (DFC)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370106905573" ID="ID_660316112" MODIFIED="1370106913025" TEXT="Especificaciones de Control"/>
</node>
</node>
<node CREATED="1370106934871" ID="ID_1525858609" MODIFIED="1384613384205" TEXT="Partici&#xf3;n Est&#xe1;tica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370106945355" ID="ID_1115253467" LINK="Modelo%20ER.mm" MODIFIED="1384509532324" TEXT="Modelo Entidad-Relaci&#xf3;n (E/R, de Chen)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376931537416" ID="ID_1722979472" MODIFIED="1384509534392" TEXT="Diagrama de Estructura de Datos (DED)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376931557966" ID="ID_1202805861" MODIFIED="1376931568817" TEXT="Modelo l&#xf3;gico de datos"/>
<node CREATED="1376931774429" ID="ID_1291344234" MODIFIED="1376931782026" TEXT="S&#xf3;lo relaciones 1:N"/>
</node>
</node>
<node CREATED="1370106967097" ID="ID_283893604" MODIFIED="1384613391538" TEXT="Partici&#xf3;n Din&#xe1;mica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370106976176" ID="ID_759078796" MODIFIED="1384509540754" TEXT="Diagrama de Transici&#xf3;n de Estados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1370106991612" ID="ID_1810959218" MODIFIED="1384509543047" TEXT="Tablas de Activaci&#xf3;n de Procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</map>
