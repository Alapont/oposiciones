## Tema096: INTEGRACIÓN CONTINUA
## Cambios: 2020/sept/25

- Resumen
	[+] añadido un apartado nuevo de CI/CD/CD (despliegue continuo, entrega continua)
	[+] añadido un apartado nuevo de "Estrategias de Despliegues": Canary, BlueGreen, DogFooding
	[+] añadido tipos de Jobs (nightly, commit,...)
	[-] eliminado sección de Redmine de este Tema
	[±] moficaciones varias: simplificados algunos párrafos / añadido detalles menores.


- Resumen Express
	[+] este año se ha creado el nuevo resumen Express