<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1481290314409" ID="ID_782657610" MODIFIED="1557433755826" TEXT="Integraci&#xf3;n Continua">
<node CREATED="1557433906298" ID="ID_1614470336" MODIFIED="1557433919618" POSITION="right" TEXT="Sistema de Control de Versiones">
<node CREATED="1557433980098" ID="ID_492376627" MODIFIED="1557433984837" TEXT="CVS"/>
<node CREATED="1557433987874" ID="ID_443729222" MODIFIED="1557433996065" TEXT="Subversion"/>
<node CREATED="1557434198149" ID="ID_357326854" MODIFIED="1557434202229" TEXT="Mercurial"/>
<node CREATED="1557434209189" ID="ID_1983582676" MODIFIED="1557434213602" TEXT="Bitbucket"/>
<node CREATED="1557433998613" ID="ID_65236139" MODIFIED="1557434002417" TEXT="Git"/>
</node>
<node CREATED="1557433805906" ID="ID_1388477327" MODIFIED="1557433823387" POSITION="right" TEXT="Repositorio de Artefactos">
<node CREATED="1557434006586" ID="ID_753365183" MODIFIED="1557434010065" TEXT="Nexus"/>
<node CREATED="1557434014117" ID="ID_622799221" MODIFIED="1557434139042" TEXT="Jfrog Artifactory"/>
<node CREATED="1557434139813" ID="ID_1554094986" MODIFIED="1557434142658" TEXT="Archiva"/>
</node>
<node CREATED="1557433778435" ID="ID_1046238896" MODIFIED="1557433828908" POSITION="left" TEXT="Construcci&#xf3;n">
<node CREATED="1557434545258" ID="ID_294864783" MODIFIED="1557434552210" TEXT="Apache ant"/>
<node CREATED="1557434553013" ID="ID_141749810" MODIFIED="1557434555665" TEXT="Maven"/>
</node>
<node CREATED="1481290471130" ID="ID_351452269" MODIFIED="1557433887651" POSITION="left" TEXT="Testing / Analisis">
<node CREATED="1557433831018" ID="ID_1559651260" MODIFIED="1557433873010" TEXT="Selenium&#xa;Firebug &#xa;Notepad++&#xa;SoapUI&#xa;Wireshark&#xa;JMeter / Badboy Software&#xa;Cobertura / Emma&#xa;SonarQube &#xa;Beyond Compare  &#xa;Cucumber &#xa;Fitnesse"/>
</node>
<node CREATED="1557434564922" ID="ID_1309884681" MODIFIED="1557434578418" POSITION="right" TEXT="Integraci&#xf3;n">
<node CREATED="1557434582517" ID="ID_767739302" MODIFIED="1557434587730" TEXT="Jenkins"/>
<node CREATED="1557434588085" ID="ID_1641322452" MODIFIED="1557434593492" TEXT="Travis CI"/>
<node CREATED="1557434601589" ID="ID_354496144" MODIFIED="1557434604260" TEXT="Bamboo"/>
</node>
</node>
</map>
