<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1379517015625" ID="ID_833648040" MODIFIED="1379517030217" TEXT="Digitalizaci&#xf3;n de documentos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379519713910" ID="ID_764993389" MODIFIED="1383898146515" POSITION="right" TEXT="Componentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379519718868" ID="ID_1591641661" MODIFIED="1385106364423" TEXT="Imagen electr&#xf3;nica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1379519852172" ID="ID_1759282839" MODIFIED="1383900780064" TEXT="Formatos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379519859268" ID="ID_1020228981" LINK="Cat&#xe1;logo%20de%20est&#xe1;ndares.mm" MODIFIED="1383900778324" TEXT="NTI Cat&#xe1;logo de est&#xe1;ndares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379519901101" ID="ID_94514910" MODIFIED="1379519912384" TEXT="Resoluci&#xf3;n de 200 p&#xed;xeles">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379519918539" ID="ID_1442570823" MODIFIED="1385111875622" TEXT="Fiel al documento origen">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379519928998" ID="ID_1151565278" MODIFIED="1383945165665" TEXT="a) Respetar&#xe1; geometr&#xed;a del documento original">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379519943787" ID="ID_1122500736" MODIFIED="1383945171758" TEXT="b) Sin caracteres o gr&#xe1;ficos que no figurasen en el documento origen">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379519961227" ID="ID_288155085" MODIFIED="1383945179051" TEXT="c) Generaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379519972471" ID="ID_1173607428" MODIFIED="1383900769224" TEXT="Procedimiento inform&#xe1;tico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379519991866" ID="ID_177244044" MODIFIED="1383945199190" TEXT="a) Digitalizaci&#xf3;n por medio fotoel&#xe9;ctrico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379520009948" ID="ID_264075633" MODIFIED="1383945241589" TEXT="b) Optimizaci&#xf3;n autom&#xe1;tica de la imagen electr&#xf3;nica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379520032071" ID="ID_288200645" MODIFIED="1383945237687" TEXT="c) Asignaci&#xf3;n de los metadatos">
<arrowlink DESTINATION="ID_1360684896" ENDARROW="Default" ENDINCLINATION="561;0;" ID="Arrow_ID_1448448192" STARTARROW="None" STARTINCLINATION="561;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383945216120" ID="ID_1726607360" MODIFIED="1383945231668" TEXT="d) Si procede, firma electr&#xf3;nica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379520044985" ID="ID_1074045949" MODIFIED="1383905526200" TEXT="Operaciones de mantenimiento preventivo y comprobaciones rutinarias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1379519728003" ID="ID_1360684896" MODIFIED="1385106368058" TEXT="Metadatos m&#xed;nimos obligatorios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1379519742475" ID="ID_1976267120" MODIFIED="1385106371209" TEXT="Firma de la imagen electr&#xf3;nica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1379519798147" ID="ID_1308856714" MODIFIED="1383898149620" TEXT="Si copia aut&#xe9;ntica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379519804541" ID="ID_866716632" LINK="Copiado%20aut&#xe9;ntico%20y%20conversi&#xf3;n.mm" MODIFIED="1379580178048" TEXT="NTI De Copiado aut&#xe9;ntico y conversi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</map>
