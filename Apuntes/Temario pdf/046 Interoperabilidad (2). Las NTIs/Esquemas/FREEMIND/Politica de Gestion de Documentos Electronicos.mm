<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1379514966164" ID="ID_1143130443" MODIFIED="1379515420205" TEXT="Pol&#xed;tica de Gesti&#xf3;n de Documentos Electr&#xf3;nicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379514982911" ID="ID_1216469489" MODIFIED="1397554830474" POSITION="right" TEXT="Contenido de la pol&#xed;tica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379514992294" ID="ID_1292738263" MODIFIED="1383936178989" TEXT="Alcance y &#xe1;mbito de aplicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1379515007526" FOLDED="true" ID="ID_752456513" MODIFIED="1397554947686" TEXT="Roles de actores involucrados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1379515086096" ID="ID_645485783" MODIFIED="1383936284429" TEXT="Alta direcci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1379515092095" ID="ID_785820534" MODIFIED="1383936287253" TEXT="Responsables de procesos de gesti&#xf3;n que aplicar&#xe1;n la pol&#xed;tica en el marco de los procesos a su cargo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1379515107671" ID="ID_281607800" MODIFIED="1383936290295" TEXT="Personal responsable del programa de planificaci&#xf3;n, implantaci&#xf3;n y administraci&#xf3;n del tratamiento de documentos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1379515152389" ID="ID_142233439" MODIFIED="1383936328953" TEXT="Personal implicado en la gesti&#xf3;n de DOCUMENTOS ELECTRONICOS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
</node>
</node>
<node CREATED="1379515016706" ID="ID_636544693" MODIFIED="1383936315785" TEXT="Directrices para la estructuraci&#xf3;n y desarrollo de los procedimientos de GESTION DOCUMENTAL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1379515037291" ID="ID_1604778707" MODIFIED="1383936188817" TEXT="Acciones de formaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1379515042447" ID="ID_1010332220" MODIFIED="1383936192498" TEXT="Actuaciones de supervisi&#xf3;n y auditor&#xed;a de los procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1379515062625" ID="ID_988694118" MODIFIED="1383936197303" TEXT="Revisi&#xf3;n del contenido de la pol&#xed;tica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-6"/>
</node>
</node>
<node CREATED="1383936373764" ID="ID_1899647517" MODIFIED="1383936390237" POSITION="right" TEXT="Programa de tratamiento de documentos electr&#xf3;nicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379515214750" FOLDED="true" ID="ID_1795113944" MODIFIED="1397554902439" POSITION="right" TEXT="Procesos de gesti&#xf3;n de documentos electr&#xf3;nicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379515229874" ID="ID_251444157" MODIFIED="1383936402218" TEXT="Captura de documentos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1383944463575" ID="ID_801990045" LINK="Documento%20electr&#xf3;nico.mm" MODIFIED="1383944551810" TEXT="NTI de Documento Electr&#xf3;nico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379515235779" ID="ID_1730452866" MODIFIED="1383936407241" TEXT="Registro legal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1383944503663" ID="ID_1620997000" LINK="Digitalizaci&#xf3;n%20de%20documentos.mm" MODIFIED="1383944534775" TEXT="NTI de Digitalizaci&#xf3;n de Documentos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379515249781" ID="ID_1103349096" MODIFIED="1383936424495" TEXT="Clasificaci&#xf3;n de documentos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1383944568852" ID="ID_1571322018" LINK="Expediente%20electr&#xf3;nico.mm" MODIFIED="1383944607635" TEXT="NTI de Expediente Electr&#xf3;nico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379515256980" ID="ID_411873248" MODIFIED="1383936428769" TEXT="Descripci&#xf3;n de documentos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1379515263571" ID="ID_1914778591" MODIFIED="1383936431172" TEXT="Acceso a los documentos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1379515268992" ID="ID_746656054" MODIFIED="1383936434027" TEXT="Calificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1379515275568" ID="ID_1058524069" MODIFIED="1383936436569" TEXT="Conservaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-7"/>
</node>
<node CREATED="1379515280053" ID="ID_1342757980" MODIFIED="1383936439065" TEXT="Transferencia de documentos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-8"/>
</node>
<node CREATED="1379515292135" ID="ID_1828924625" MODIFIED="1383936442107" TEXT="Destrucci&#xf3;n o eliminaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-9"/>
<node CREATED="1379515305873" ID="ID_1907718532" MODIFIED="1383936445649" TEXT="Normativa de eliminaci&#xf3;n de Patrimonio Documental">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ff3399" CREATED="1379515325004" ID="ID_964983157" MODIFIED="1384194063872" STYLE="bubble" TEXT="Medidas de seguridad 3/2010">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379515366274" FOLDED="true" ID="ID_763128376" MODIFIED="1397554896381" POSITION="right" TEXT="Asignaci&#xf3;n de METADATOS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379515377561" FOLDED="true" ID="ID_897107502" MODIFIED="1397554895458" TEXT="Esquema de Metadatos e-EMGDE ">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379515394231" ID="ID_1567722306" MODIFIED="1383940165292" TEXT="Disponible en el Centro de Interoperabilidad Sem&#xe1;ntica (CISE)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383936545183" ID="ID_1441090311" MODIFIED="1383936598069" TEXT="Incluye los metadatos m&#xed;nimos obligatorios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383936564271" ID="ID_1445140955" LINK="Documento%20electr&#xf3;nico.mm" MODIFIED="1383936658316" TEXT="NTI de Documento Electr&#xf3;nico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383936584230" ID="ID_1254320096" LINK="Expediente%20electr&#xf3;nico.mm" MODIFIED="1383936675336" TEXT="NTI de Expediente Electr&#xf3;nico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1383940048711" ID="ID_434042058" MODIFIED="1383940067653" TEXT="Se basa en un modelo entidad-relaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379579746916" ID="ID_1014815119" MODIFIED="1381833492614" TEXT="Entidades">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379579864724" ID="ID_259798574" MODIFIED="1381832513824" TEXT="Documentos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="edit"/>
</node>
<node CREATED="1379579812798" ID="ID_1975607189" MODIFIED="1381832520725" TEXT="Agentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="smiley-neutral"/>
</node>
<node CREATED="1379579823946" ID="ID_693508416" MODIFIED="1381832558169" TEXT="Actividades ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="desktop_new"/>
</node>
<node CREATED="1379579828939" ID="ID_1431529215" MODIFIED="1381832530410" TEXT="Regulaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="list"/>
</node>
<node CREATED="1379579873717" ID="ID_250423281" MODIFIED="1381832547717" TEXT="Relaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="group"/>
</node>
</node>
</node>
<node CREATED="1379579843659" ID="ID_257827376" MODIFIED="1397554882558" TEXT="Caracter&#xed;sticas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379579881853" ID="ID_1456958095" MODIFIED="1379580064635" TEXT="Multi-entidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379579886518" ID="ID_1726620127" MODIFIED="1379580065308" TEXT="Uso de relaciones y eventos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379579894317" ID="ID_1882511378" MODIFIED="1379580065772" TEXT="Especificaci&#xf3;n de obligaci&#xf3;n de uso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379579906295" ID="ID_556342607" MODIFIED="1379580066427" TEXT="Aplicaci&#xf3;n de esquemas de valores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379579916959" ID="ID_1285350879" MODIFIED="1379580067123" TEXT="Independiente de la tecnolog&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379579937752" ID="ID_618305631" MODIFIED="1379580067611" TEXT="Flexibilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379579942344" ID="ID_796241867" MODIFIED="1379580068411" TEXT="Repetibilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379579948136" ID="ID_1654317163" MODIFIED="1383936506738" TEXT="Extensibilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379579957529" ID="ID_1116288684" MODIFIED="1379580069971" TEXT="Interoperabilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379579965513" ID="ID_496842987" MODIFIED="1379580070627" TEXT="Compatibilidad con otras normas t&#xe9;cnicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379579978217" ID="ID_1080043508" MODIFIED="1379580071315" TEXT="Posibilidad de reutilizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379579994162" ID="ID_251297433" MODIFIED="1383944025995" TEXT="Consta de 23 elementos de tipos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379580003478" ID="ID_183286051" MODIFIED="1379580073915" TEXT="Obligatorios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379580008091" ID="ID_50029844" MODIFIED="1379580074571" TEXT="Condicionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379580014347" ID="ID_243057868" MODIFIED="1379580075179" TEXT="Opcionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</map>
