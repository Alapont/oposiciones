<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1379520332978" ID="ID_1084323513" MODIFIED="1379521130621" TEXT="Pol&#xed;tica de firma electr&#xf3;nica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379520739263" ID="ID_249814578" MODIFIED="1385118310764" POSITION="right" TEXT="Definici&#xf3;n y contenido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379520442169" ID="ID_749359982" MODIFIED="1385123098326" TEXT="Definir&#xe1;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379520448722" ID="ID_1857510094" MODIFIED="1379520471397" TEXT="Creaci&#xf3;n validaci&#xf3;n y conservaci&#xf3;n de firmas electr&#xf3;nicas"/>
<node CREATED="1379520472892" ID="ID_300703480" MODIFIED="1379520499648" TEXT="Caracter&#xed;sticas y requisitos de los sistemas, certificados y sellos de tiempo"/>
</node>
<node CREATED="1379520505921" ID="ID_393003825" MODIFIED="1384261625087" TEXT="Incluir&#xe1;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379520526392" ID="ID_327052111" MODIFIED="1383947070341" TEXT="Alcance y &#xe1;mbito">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1379520539916" ID="ID_1048328858" MODIFIED="1383947072882" TEXT="Identificaci&#xf3;n del documento o responsable">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1379520552804" ID="ID_93454778" MODIFIED="1397555059658" TEXT="Reglas comunes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1379520559567" ID="ID_1360595299" MODIFIED="1397555061509" TEXT="1) Formatos admitidos de firma electr&#xf3;nica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379522199544" ID="ID_156123021" MODIFIED="1385118879012" TEXT="Ser&#xe1;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379522086865" ID="ID_962681026" MODIFIED="1379669103505" TEXT="Est&#xe1;ndares abiertos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379522096458" ID="ID_1965895837" MODIFIED="1379669104353" TEXT="Definidos por la CE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379522117300" ID="ID_646961761" MODIFIED="1384255534262" TEXT="Compatibles con la pol&#xed;tica de generaci&#xf3;n y validaci&#xf3;n de firmas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379522153654" ID="ID_1195962038" MODIFIED="1384255539441" TEXT="Permitan desarrollar funcionalidades avanzadas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379522167570" ID="ID_1676068068" MODIFIED="1384255541797" TEXT="Firmas longevas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379522214380" ID="ID_1648804771" MODIFIED="1379522249466" TEXT="Cada organizaci&#xf3;n determinar&#xe1; los formatos y estructuras concretas"/>
<node CREATED="1379522253427" ID="ID_774472848" MODIFIED="1379522270647" TEXT="Cada organizaci&#xf3;n identificar&#xe1; a la entidad gestora"/>
<node CREATED="1379522273966" ID="ID_958071147" MODIFIED="1379522541910" TEXT="La pol&#xed;tica electr&#xf3;nica incluir&#xe1; los procedimientos de actualizaci&#xf3;n"/>
<node CREATED="1379522381538" FOLDED="true" ID="ID_1171575238" MODIFIED="1385120131963" TEXT="Transmisiones de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379522400276" ID="ID_1724587656" LINK="Cat&#xe1;logo%20de%20est&#xe1;ndares.mm" MODIFIED="1384256032192" TEXT="NTI de Cat&#xe1;logo de est&#xe1;ndares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379522414890" ID="ID_1338013085" MODIFIED="1384256640624" TEXT="Contenido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379522424949" ID="ID_61366815" LINK="Cat&#xe1;logo%20de%20est&#xe1;ndares.mm" MODIFIED="1384256058556" TEXT="NTI de Cat&#xe1;logo de est&#xe1;ndares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379522440017" ID="ID_1657447115" MODIFIED="1383983765045" TEXT="XAdEs (ETSI 101 903)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379522444406" ID="ID_1385387941" MODIFIED="1383983782704" TEXT="CAdES (ETSI 101 733)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379522452217" ID="ID_1198426571" MODIFIED="1383983810472" TEXT="PAdES (No admitido en la pol&#xed;tica de la AGE)  (ETSI 102 778-3)   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379522477749" ID="ID_71363845" MODIFIED="1379522561084" TEXT="Perfil m&#xed;nimo de formato &quot;-EPES&quot;, clase b&#xe1;sica (BES)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379522592341" ID="ID_1579621038" MODIFIED="1397555105333" TEXT="Consideraciones de casos particulares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379526126190" ID="ID_352105880" MODIFIED="1384256665116" TEXT="Documento electr&#xf3;nico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379526155657" ID="ID_1798268712" MODIFIED="1384256692838" STYLE="bubble" TEXT="Metadato &quot;Formato de firma&quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379526174212" ID="ID_589966472" MODIFIED="1384256692838" TEXT="XAdES internally detached signature">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contenido firmado y firma comparten una misma estructura XML como nodos independientes y del mismo nivel
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526202853" ID="ID_909997048" MODIFIED="1384256692838" TEXT="XAdES enveloped signature">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      La firma se ubica dentro&#160;el fichero firmado justo despu&#233;s del contenido
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526216617" ID="ID_1812390633" MODIFIED="1384256692838" TEXT="CAdES detached/explicit signature">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contenido y firma constituyen ficheros independientes
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526235484" ID="ID_1602103967" MODIFIED="1384256692838" TEXT="CAdES attached /implicit signature (enveloping)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      El fichero de firma envuelve al propio contenido firmado, de forma que para acceder al contenido hay que interpretar la firma
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526250293" ID="ID_363997530" MODIFIED="1384256692838" TEXT="PAdES">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contenido y firma quedan bajo el mismo fichero PDF, que permiten al acceso a ambos de forma independiente
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379526267532" ID="ID_980951491" MODIFIED="1383947521097" TEXT="Factura electr&#xf3;nica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379526282235" ID="ID_275927106" LINK="../Facturae.mm" MODIFIED="1384256216806" TEXT="Facturae (PRE/2971/2007)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1379526383850" ID="ID_893608781" MODIFIED="1384256724975" TEXT="Reglas de uso de algoritmo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379672180821" FOLDED="true" ID="ID_972248243" MODIFIED="1385120305531" TEXT="Seguridad gen&#xe9;rica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379672203540" ID="ID_1908109367" MODIFIED="1384256338816" TEXT="Se tomar&#xe1; la referencia a la URN donde se publican las funciones hash y los algoritmos de firma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379672260480" ID="ID_1433180444" MODIFIED="1385119209219" TEXT="Algoritmos v&#xe1;lidos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379526488442" ID="ID_1479646451" MODIFIED="1379672322735" TEXT="XML-Dsig (XML Digital Signature)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526501229" ID="ID_545710424" MODIFIED="1379672304843" TEXT="CMS (Cryptographic Message Syntax)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379526507057" FOLDED="true" ID="ID_938052967" MODIFIED="1385120309884" TEXT="Alta seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379526515676" ID="ID_1284062673" MODIFIED="1379669086592" TEXT="CCN-STIC 405">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526529402" ID="ID_1401698138" MODIFIED="1379669087496" TEXT="CCN-STIC 407">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1379520573181" FOLDED="true" ID="ID_1133416561" MODIFIED="1385124167755" TEXT="2) Reglas de creaci&#xf3;n de firma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379526617848" ID="ID_29818866" MODIFIED="1385119266752" TEXT="funcionalidades">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379526646416" ID="ID_437906157" MODIFIED="1383948074939" TEXT="Selecci&#xf3;n del fichero a ser firmado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1379526675378" ID="ID_370440715" MODIFIED="1385124500051" TEXT="Verificaciones previas">
<arrowlink DESTINATION="ID_1951786594" ENDARROW="Default" ENDINCLINATION="428;0;" ID="Arrow_ID_1262472993" STARTARROW="None" STARTINCLINATION="428;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1379526689990" ID="ID_985492888" MODIFIED="1383981842225" TEXT="a) Que la firma electr&#xf3;nica puede ser validada ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526708960" ID="ID_1965351832" MODIFIED="1383981847342" TEXT="b) Que los certificados se encuentran bajo una Declaraci&#xf3;n de Pol&#xed;ticas de Certificaci&#xf3;n espec&#xed;fica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526745080" ID="ID_1952186780" MODIFIED="1383981851414" TEXT="c) Validez del certificado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526769156" ID="ID_1091500174" MODIFIED="1379526797029" TEXT="Si alguna de estas verificaciones es err&#xf3;nea, el proceso de firma se interrumpir&#xe1;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379526809035" ID="ID_636564313" MODIFIED="1383948078662" TEXT="Se crear&#xe1; un fichero de firma seg&#xfa;n el formato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1379672403664" ID="ID_1511126932" MODIFIED="1384258420524" TEXT="No debe contener contenido din&#xe1;mico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
</node>
</node>
</node>
<node CREATED="1379526840431" ID="ID_940022968" MODIFIED="1385119280261" TEXT="La VINCULACI&#xd3;N del firmante se establecer&#xe1; mediante las etiquetas, que proporcionar&#xe1;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estas mismas etiquetas se considerar&#225;n para la validar la Firma en las reglas de validaci&#243;n de firma.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379526905488" ID="ID_133402643" MODIFIED="1379669345891" TEXT="Fecha y hora de firma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526915406" ID="ID_1579326264" MODIFIED="1379669348043" TEXT="Certificado del firmante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526923287" ID="ID_290002522" MODIFIED="1379669348939" TEXT="Pol&#xed;tica de firma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526932230" ID="ID_1733479339" MODIFIED="1379669349731" TEXT="Formato del objeto original">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526941937" ID="ID_68825087" MODIFIED="1383948286216" TEXT="Opcionalmente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379526950624" ID="ID_245343411" MODIFIED="1383948282047" TEXT="Lugar geogr&#xe1;fico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526957663" ID="ID_1099538663" MODIFIED="1383948283343" TEXT="Rol de la persona">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526964315" ID="ID_1499545892" MODIFIED="1383948284102" TEXT="Acci&#xf3;n del firmante sobre el documento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379526974790" ID="ID_557403634" MODIFIED="1383948284660" TEXT="Sello del tiempo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379527140217" ID="ID_753151135" MODIFIED="1385120807025" TEXT="Counter signature (Contra firma o firma secuencial)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379527159209" ID="ID_1626968705" MODIFIED="1384258409760" TEXT="Multiples firmas al mismo nivel">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379527169776" ID="ID_119270985" MODIFIED="1384258431367" TEXT="Independientes">
<icon BUILTIN="forward"/>
</node>
</node>
</node>
<node CREATED="1379520585182" FOLDED="true" ID="ID_1951786594" MODIFIED="1385124508506" TEXT="3) Reglas de validaci&#xf3;n de firma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379527545854" ID="ID_209187728" MODIFIED="1384261491404" TEXT="Para validar la FIRMA electr&#xf3;nica se consider&#xe1;ra">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379527237277" ID="ID_1772071305" MODIFIED="1384257335592" TEXT="Garant&#xed;a de que la firma es v&#xe1;lida para el fichero espec&#xed;fico que est&#xe1; firmado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1379527556133" ID="ID_1346562139" MODIFIED="1384258302897" TEXT="Fecha y hora de la firma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1379527569897" ID="ID_1680632814" MODIFIED="1379669702730" TEXT="Si hay sellos de tiempo, el m&#xe1;s antiguo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379527585328" ID="ID_128950428" MODIFIED="1383983159684" TEXT="Si no, car&#xe1;cter &#xfa;nicamente indicativo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379527641032" FOLDED="true" ID="ID_554430031" MODIFIED="1385124497758" TEXT="Certificado del firmante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1379527253450" ID="ID_1592358002" MODIFIED="1384257157405" TEXT="Validez de los CERTIFICADOS    ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379527272430" ID="ID_1576525570" MODIFIED="1384257982102" TEXT="Se tomar&#xe1; como instante de tiempo de referencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379527306609" ID="ID_629362779" MODIFIED="1385119740322" TEXT="Momento en que se produjo la firma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383984184092" ID="ID_796426171" MODIFIED="1383984611433" TEXT="a) Si se facilitan los hist&#xf3;ricos y llevan un SELLO de tiempo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383984207187" ID="ID_672042019" MODIFIED="1383984625769" TEXT="b) Si se trata de firmas LONGEVAS que incluyen las evidencias de la validez">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379527345745" ID="ID_1235513578" MODIFIED="1383984292475" TEXT="En otros casos, el momento de la validaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1383984469908" ID="ID_1863491342" MODIFIED="1384258274457" TEXT="Se comprobar&#xe1;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379527380870" ID="ID_371200394" MODIFIED="1383984514151" TEXT="No revocados o suspendidos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1379527391419" ID="ID_933740255" MODIFIED="1383984517989" TEXT="Validez de toda la cadena de certificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1379527410784" ID="ID_1043600234" MODIFIED="1383984521405" TEXT="Ha sido expedido por un prestador de servicios de confianza bajo una Declaraci&#xf3;n de Pr&#xe1;cticas de Certificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1379527443956" ID="ID_1908505932" MODIFIED="1383984524073" TEXT="Verificaci&#xf3;n de los sellos de tiempo de los formatos implementados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
</node>
</node>
</node>
</node>
<node CREATED="1379527652568" ID="ID_1503317735" MODIFIED="1384258301415" TEXT="Pol&#xed;tica de firma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
<node CREATED="1379527677064" ID="ID_927000370" MODIFIED="1379670165815" TEXT="hash e identificador (OID)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379527950782" ID="ID_506961239" MODIFIED="1383948351189" TEXT="Si hay varias firmas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379527958361" ID="ID_1678673077" MODIFIED="1384258459245" TEXT="Se comprobar&#xe1;n todas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
</node>
</node>
</node>
</node>
<node CREATED="1379520597770" FOLDED="true" ID="ID_1316702496" MODIFIED="1385124715784" TEXT="Reglas de confianza">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
<node CREATED="1379528072718" FOLDED="true" ID="ID_1528851962" MODIFIED="1385124561297" TEXT="Para los certificados electr&#xf3;nicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379528110499" ID="ID_690939733" MODIFIED="1383984074449" TEXT="V&#xe1;lidos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379528118149" ID="ID_1951280948" MODIFIED="1379672767937" TEXT="Ley 59/2003 y Directiva 1999/93/CE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379528148201" ID="ID_369015877" MODIFIED="1383984097537" TEXT="Nuevas tipolog&#xed;as definidas en la Ley 11/2007">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379673035175" ID="ID_852634769" MODIFIED="1379673108560" TEXT="Prestadores de servicios de certificaci&#xf3;n (art. 19 R.D 4/2010 y art. 21 Ley 11/2007)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383983977884" ID="ID_396426441" MODIFIED="1383984042608" TEXT="Los prestadores de servicios de certificaci&#xf3;n se podr&#xe1;n consultar en la TLS ">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      TLS:Listas de servcios de confianza
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379528209668" ID="ID_246979936" MODIFIED="1383983972182" TEXT="Tiempo de gracia para la validaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379528232510" ID="ID_1105806871" MODIFIED="1383984054793" TEXT="Tiempo m&#xe1;ximo permitido para refresco completo de las CRLs">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379528245202" ID="ID_1932021279" MODIFIED="1383984057866" TEXT="Tiempo m&#xe1;ximo para actualizaci&#xf3;n del estado del certificado OCSP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379520618658" FOLDED="true" ID="ID_456188251" MODIFIED="1385124658953" TEXT="Para los sellos de tiempo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379528414881" ID="ID_1593982261" MODIFIED="1379673280487" TEXT="Elementos b&#xe1;sicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379528315097" ID="ID_1883173410" MODIFIED="1379673287304" TEXT="Identidad emisora del sello">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379528331033" ID="ID_172521640" MODIFIED="1379673284160" TEXT="Tipo de solicitud">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379528346317" ID="ID_793640625" MODIFIED="1379673290847" TEXT="Valores resumen &quot;anterior&quot;, &quot;actual&quot; y &quot;siguiente&quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379528371876" ID="ID_724634918" MODIFIED="1379673292688" TEXT="Fecha y hora UTC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379528379603" ID="ID_1736623058" MODIFIED="1379673293752" TEXT="Firma electr&#xf3;nica de todo lo anterior">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379528443955" FOLDED="true" ID="ID_1953729758" MODIFIED="1385121771294" TEXT="El sellado del tiempo puede ser a&#xf1;adido por ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379528454688" ID="ID_683815619" MODIFIED="1383947156275" TEXT="Emisor ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379528457789" ID="ID_1797396353" MODIFIED="1383947155538" TEXT="Receptor">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379528461561" ID="ID_56616142" MODIFIED="1383947157365" TEXT="Un tercero">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379528520360" ID="ID_86303898" MODIFIED="1380650398200" TEXT="Est&#xe1;ndar ETSI TS 102 023">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379520622578" ID="ID_1204498961" MODIFIED="1385124693226" TEXT="Para las firmas longevas">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1. Firma B&#225;sica (AdES &#173; BES), firma b&#225;sica para satisfacer los requisitos de la firma electr&#243;nica avanzada.
    </p>
    <p>
      2. AdES &#173; T, se a&#241;ade un sellado de tiempo (TimeStamp) con el fin de situar en el tiempo el instante en que se firma un documento.
    </p>
    <p>
      3. AdES &#173; C, a&#241;ade un conjunto de referencias a los certificados de la cadena de certificaci&#243;n y su estado, como base para una verificaci&#243;n longeva.
    </p>
    <p>
      4. AdES &#173; X, a&#241;ade sellos de tiempo a las referencias creadas en el paso anterior.
    </p>
    <p>
      5. AdES &#173; XL, a&#241;ade los certificados y la informaci&#243;n de revocaci&#243;n de los mismos, para su validaci&#243;n a largo plazo.
    </p>
    <p>
      6. AdES &#173; A, permite la adici&#243;n de sellos de tiempo peri&#243;dicos para garantizar la integridad de la firma archivada o guardada para futuras verificaciones.
    </p>
    <p>
      Se ha conseguido una firma longeva &#8220;auto&#173;verificable&#8221; con el paso del tiempo.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379528593964" ID="ID_872970258" MODIFIED="1383985395898" TEXT="Sello del tiempo para verificar el certificado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379528629967" ID="ID_16142822" MODIFIED="1383985402872" TEXT="Para conversi&#xf3;n de firma electr&#xf3;nica a longeva">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379528653871" ID="ID_191272497" MODIFIED="1383985508375" TEXT="Se verificar&#xe1; la firma y el cumplimiento de est&#xe1;ndares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379528671680" ID="ID_820106864" MODIFIED="1383985509872" TEXT="Se completar&#xe1; la firma almacenando las referencias a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379528707734" ID="ID_1299834822" MODIFIED="1383985510949" TEXT="Certificados: del firmante y de la cadena de verificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379528750896" ID="ID_641711607" MODIFIED="1383985511573" TEXT="Informaciones del estado de los certificados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379528771407" ID="ID_942397954" MODIFIED="1383985512852" TEXT="Sellado a las referencias anteriores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379528798582" ID="ID_409435279" MODIFIED="1383985408300" TEXT="Validaci&#xf3;n mediante CRLs o OCSP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379520604194" FOLDED="true" ID="ID_1741434949" MODIFIED="1385124738653" TEXT="Reglas opcionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-5"/>
<node CREATED="1379520658109" ID="ID_1072425882" MODIFIED="1383983585877" TEXT="Reglas espec&#xed;ficas de compromiso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379520675619" FOLDED="true" ID="ID_1796629335" MODIFIED="1385124735689" TEXT="Reglas de certificados de atributos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379520686114" ID="ID_1843614641" MODIFIED="1383947183367" TEXT="Info adicional a los certificados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379520700914" ID="ID_1322098462" MODIFIED="1383985579762" TEXT="Condiciones de archivado y custodia de firmas electr&#xf3;nicas">
<arrowlink DESTINATION="ID_1596373416" ENDARROW="Default" ENDINCLINATION="635;0;" ID="Arrow_ID_878266628" STARTARROW="None" STARTINCLINATION="635;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1379520768568" ID="ID_1465849041" MODIFIED="1397555225360" POSITION="right" TEXT="Datos identificativos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379520783571" FOLDED="true" ID="ID_1233616883" MODIFIED="1397555230871" TEXT="Documento de pol&#xed;tica de firma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379520791739" ID="ID_1572430193" MODIFIED="1383947103205" TEXT="Nombre">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379520794544" ID="ID_1104066893" MODIFIED="1383947104321" TEXT="Versi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379520797054" ID="ID_483594611" MODIFIED="1380650940288" TEXT="Identificador (OID en ASN.1, URI en XML) de la pol&#xed;tica ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379520802826" ID="ID_710324977" MODIFIED="1379670021932" TEXT="URI de referencia de la pol&#xed;tica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379520804595" ID="ID_223173506" MODIFIED="1379670028723" TEXT="Fecha  de expedici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379520808891" ID="ID_1934425073" MODIFIED="1383947107993" TEXT="&#xe1;mbito de aplicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379520826039" ID="ID_1205388544" MODIFIED="1379669990478" TEXT="Periodo de validez">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379520838986" FOLDED="true" ID="ID_787287425" MODIFIED="1397555233040" TEXT="Identificaci&#xf3;n de su gestor">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379520853994" ID="ID_880423124" MODIFIED="1383947117262" TEXT="Nombre">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379520856791" ID="ID_103603390" MODIFIED="1383947118378" TEXT="Direcci&#xf3;n de contacto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379520862733" ID="ID_1210008046" MODIFIED="1379670580423" TEXT="OID del gestor de la pol&#xed;tica de firma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379520881438" FOLDED="true" ID="ID_151924231" MODIFIED="1385124907087" POSITION="right" TEXT="Actores involucrados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379520889991" ID="ID_893440628" MODIFIED="1379670130172" TEXT="Firmante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379521007762" ID="ID_1526732279" MODIFIED="1379670133341" TEXT="Verificador">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379521027532" ID="ID_778187813" MODIFIED="1379670136429" TEXT="Prestador de servicios de certificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379521084646" ID="ID_1139481281" MODIFIED="1379670138509" TEXT="Emisor y gestor de la pol&#xed;tica de firma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379521133668" FOLDED="true" ID="ID_121304909" MODIFIED="1397555298877" POSITION="right" TEXT="Usos de la firma electr&#xf3;nica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379521147615" ID="ID_941945860" MODIFIED="1379670180774" TEXT="Transmisiones de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379521162362" ID="ID_1046141431" MODIFIED="1379670187575" TEXT="Autenticidad, integridad y no repudio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379521232620" FOLDED="true" ID="ID_377007778" MODIFIED="1397555301842" POSITION="right" TEXT="Interacci&#xf3;n con otras pol&#xed;ticas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379521287910" ID="ID_1308838803" MODIFIED="1384261141531" TEXT="Formatos XML y ASN.1 para que otras aplicaciones puedan identificar la pol&#xed;tica de firma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379521315632" ID="ID_1703917901" MODIFIED="1397555236803" POSITION="right" TEXT="Gesti&#xf3;n de la pol&#xed;tica de firma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379670433957" ID="ID_1680843223" MODIFIED="1384261621124" TEXT="Actualizaci&#xf3;n atendiendo a ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379521333265" ID="ID_286179787" MODIFIED="1379670478267" TEXT="Modificaciones por necesidades propias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379521343481" ID="ID_1027996174" MODIFIED="1379670480115" TEXT="Cambios en las pol&#xed;ticas relacionadas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379521354516" ID="ID_248789565" MODIFIED="1379670481179" TEXT="Cambios en los certificados electr&#xf3;nicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379521404547" ID="ID_1596373416" MODIFIED="1385121844755" POSITION="right" TEXT="Archivado y custodia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379521422439" ID="ID_209303438" MODIFIED="1395944908779" TEXT="Garantizar fiabilidad a lo largo del tiempo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379521434795" ID="ID_383944188" MODIFIED="1383983653316" TEXT="Firmas longevas, mediante las que se a&#xf1;adir&#xe1;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379521454825" ID="ID_992849332" MODIFIED="1395945071527" TEXT="Informaci&#xf3;n del estado del certificado + Sello del tiempo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379521460028" ID="ID_1862691816" MODIFIED="1383947277069" TEXT="Certificados que conforman la cadena de confianza">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379521440363" ID="ID_1316750590" MODIFIED="1379521486920" TEXT="Otros m&#xe9;todos t&#xe9;cnicos"/>
</node>
<node CREATED="1379521536333" ID="ID_1547939096" MODIFIED="1385124937039" TEXT="Almacenamiento de los certificados e informaciones de estado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379521569123" ID="ID_212704213" MODIFIED="1383985619808" TEXT="Dentro de la firma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379521596079" ID="ID_1570681732" MODIFIED="1379670551647" TEXT="sellado con AdES-X o -A">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379521629402" ID="ID_596707559" MODIFIED="1383985617390" TEXT="Dep&#xf3;sito espec&#xed;fico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379521651220" ID="ID_1350044378" MODIFIED="1383947240025" TEXT="sellado independiente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379521680131" ID="ID_933394443" MODIFIED="1385124952967" TEXT="Protecci&#xf3;n frente a obsolescencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379521698278" ID="ID_1636483903" MODIFIED="1379670569511" TEXT="Mecanismos de resellado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379521716768" ID="ID_7245355" MODIFIED="1384260421764" TEXT="Almacenamiento en un dep&#xf3;sito seguro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379521738556" ID="ID_1298117257" MODIFIED="1379670574096" TEXT="marcas de fecha y hora">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</map>
