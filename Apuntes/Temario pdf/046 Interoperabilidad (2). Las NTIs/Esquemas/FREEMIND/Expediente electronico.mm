<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1379513837834" ID="ID_1354979961" MODIFIED="1379514738666" TEXT="Expediente electr&#xf3;nico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383939273272" FOLDED="true" ID="ID_5878600" MODIFIED="1385111059678" POSITION="right" TEXT="Ambito">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383939290166" ID="ID_205096364" MODIFIED="1383939329490" TEXT="Expedientes electr&#xf3;nicos en el &#xe1;mbito del art&#xed;culo 3 de R.D. 4/2010">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379513882036" ID="ID_433680458" MODIFIED="1385119878242" POSITION="right" TEXT="Componentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379513890406" ID="ID_1199110865" MODIFIED="1391853790898" TEXT="Documentos electr&#xf3;nicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1379513922503" ID="ID_1518590485" LINK="Documento%20electr&#xf3;nico.mm" MODIFIED="1379578739765" TEXT="Cumplir&#xe1;n la NTI de Documento Electr&#xf3;nico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379514153101" ID="ID_1779712860" MODIFIED="1385106075057" TEXT="&#xcd;ndice electr&#xf3;nico, que deber&#xe1; incluir para su intercambio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1379514169785" ID="ID_276728518" MODIFIED="1383939462921" TEXT="a) La fecha de generaci&#xf3;n del &#xed;ndice">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379514179153" ID="ID_1463704179" MODIFIED="1385119881143" TEXT=" b) Para cada documento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379514185028" ID="ID_1143995638" MODIFIED="1379578715849" TEXT="Identificador">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379514191071" ID="ID_1358356943" MODIFIED="1379578717706" TEXT="Huella digital">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379514196149" ID="ID_20777806" MODIFIED="1379578719687" TEXT="Funci&#xf3;n resumen">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379514225549" ID="ID_1259287318" LINK="Cat&#xe1;logo%20de%20est&#xe1;ndares.mm" MODIFIED="1379578752666" TEXT="NTI Cat&#xe1;logo de est&#xe1;ndares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379514543733" ID="ID_450101554" MODIFIED="1383896545854" TEXT="Opcionalmente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379514243573" ID="ID_1326173713" MODIFIED="1383896551455" TEXT="Fecha de incorporaci&#xf3;n al expediente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379514533460" ID="ID_54291431" MODIFIED="1383896556619" TEXT="Orden del documento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379514589777" ID="ID_682541759" MODIFIED="1383939503550" TEXT="c) Si es el caso, la disposici&#xf3;n de los documentos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379514002022" ID="ID_133121319" MODIFIED="1385105944812" TEXT="Firma del &#xed;ndice electr&#xf3;nico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
<node BACKGROUND_COLOR="#ffcc99" CREATED="1379514011343" ID="ID_115582722" MODIFIED="1396987400327" STYLE="bubble" TEXT="Metadatos del expediente electr&#xf3;nico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
<node BACKGROUND_COLOR="#ffcc99" CREATED="1379514035578" ID="ID_1550061011" MODIFIED="1383936865656" TEXT="M&#xed;nimos obligatorios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379514047566" ID="ID_270655765" MODIFIED="1383936810058" TEXT="Definidos en el Anexo I">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379514055920" ID="ID_1934984000" MODIFIED="1383936810058" TEXT="Se asociar&#xe1;n para su remisi&#xf3;n o puesta a disposici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379514083290" ID="ID_408739049" MODIFIED="1383936810058" TEXT="No ser&#xe1;n modificados posteriormente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379514099475" ID="ID_466299571" MODIFIED="1383936810058" TEXT="Complementarios"/>
</node>
</node>
<node CREATED="1379514112665" FOLDED="true" ID="ID_1142576688" MODIFIED="1385111796780" POSITION="right" TEXT="Intercambio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379514126136" ID="ID_112709975" MODIFIED="1383896529693" TEXT="Mediante las estructuras establecidas en el Anexo II">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379513989363" ID="ID_1763528997" MODIFIED="1379514724236" TEXT="Indice electr&#xf3;nico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379514607756" ID="ID_1038669085" MODIFIED="1383936997227" TEXT="Si es entre AAPP, de forma automatizada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379514623223" ID="ID_1750157265" MODIFIED="1379578995781" TEXT="SARA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379514629627" ID="ID_1144172344" MODIFIED="1383896534763" TEXT="Asiento Registral">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379514636065" ID="ID_1944454025" MODIFIED="1383939535261" TEXT="NTI Intercambio de Asientos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379514662419" ID="ID_1601842851" MODIFIED="1379514708215" TEXT="Si es traspaso de custodia o responsabilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379514681726" ID="ID_970843243" MODIFIED="1379514710992" TEXT="Se verifica la autenticidad e integridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</map>
