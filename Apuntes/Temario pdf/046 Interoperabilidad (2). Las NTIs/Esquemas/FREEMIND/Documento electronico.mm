<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1379513224664" ID="ID_678482978" MODIFIED="1379513660134" TEXT="Documento electr&#xf3;nico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379513245948" ID="ID_1692039866" MODIFIED="1391853766765" POSITION="right" TEXT="&#xc1;mbito">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379513258984" ID="ID_1296493550" MODIFIED="1383916720050" TEXT="Documentos administrativos electr&#xf3;nicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379513272796" ID="ID_1501693637" MODIFIED="1383938936050" TEXT="Cualquier otro susceptible de formar parte de un documento electr&#xf3;nico">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>NO SOLO LOS ADMINISTRATIVOS!!!</b>
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1379513298169" ID="ID_1689595139" MODIFIED="1391853768408" POSITION="right" TEXT="Componentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379513305371" ID="ID_148650095" MODIFIED="1383938977969" TEXT="a) Contenido, entendido como conjunto de datos o informaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379513310025" ID="ID_382891141" MODIFIED="1383939009491" TEXT="b) Firma electr&#xf3;nica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379513352730" ID="ID_120473035" MODIFIED="1385068414646" TEXT="SIEMPRE tendr&#xe1; asociada al menos una firma electr&#xf3;nica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffcc99" CREATED="1379513316023" ID="ID_1157962766" MODIFIED="1391853771443" STYLE="bubble" TEXT="c) Metadatos del documento electr&#xf3;nico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ffcc99" CREATED="1379513384143" ID="ID_1302165534" MODIFIED="1383940476776" TEXT="M&#xed;nimos obligatorios">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Los metadatos m&#237;nimos obligatorios tambi&#233;n se encuentran en el modelo EMGDE (v. NTI de pol&#237;tica de Gesti&#243;n de documentos)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379513392434" ID="ID_1559452192" MODIFIED="1383936874548" TEXT="Definidos en el Anexo I">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379513403641" ID="ID_1060112782" MODIFIED="1383939066331" TEXT="Estar&#xe1;n presentes en cualquier proceso de intercambio de documentos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379513426565" ID="ID_1529243327" MODIFIED="1383936874548" TEXT="No ser&#xe1;n modificados en ninguna fase posterior">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379513445184" ID="ID_1080383954" MODIFIED="1383939085667" TEXT="Se podr&#xe1;n asignar complementarios">
<node CREATED="1383939118425" ID="ID_1995025879" LINK="Pol&#xed;tica%20de%20Gesti&#xf3;n%20de%20Documentos%20Electr&#xf3;nicos.mm" MODIFIED="1383939150935" TEXT="Seg&#xfa;n NTI de Pol&#xed;tica de gesti&#xf3;n de Documentos"/>
</node>
</node>
</node>
<node CREATED="1379513488312" FOLDED="true" ID="ID_425855727" MODIFIED="1385111020834" POSITION="right" TEXT="Formato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379513506012" ID="ID_1919780760" LINK="Cat&#xe1;logo%20de%20est&#xe1;ndares.mm" MODIFIED="1383936922050" TEXT="Ficheros seg&#xfa;n CATALOGO DE EST&#xc1;NDARES (NTI)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379513543685" FOLDED="true" ID="ID_1216702198" MODIFIED="1385111006872" POSITION="right" TEXT="Intercambio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379513560153" ID="ID_806029485" MODIFIED="1383939206714" TEXT="Debe tener los componentes descritos">
<arrowlink DESTINATION="ID_1689595139" ENDARROW="Default" ENDINCLINATION="263;0;" ID="Arrow_ID_972586220" STARTARROW="None" STARTINCLINATION="263;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379513590579" ID="ID_1200601953" MODIFIED="1383896032086" TEXT="Seg&#xfa;n estructura definida en el Anexo II">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379513614003" ID="ID_1925664261" MODIFIED="1383896032913" TEXT="Se usar&#xe1; SARA ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</map>
