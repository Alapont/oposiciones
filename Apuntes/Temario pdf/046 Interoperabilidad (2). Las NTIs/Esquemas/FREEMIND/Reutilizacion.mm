<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1379535722791" ID="ID_725945878" MODIFIED="1383917432970" TEXT="Reutilizaci&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Desarrolla la ley 37/2007 y se trata de la informaci&#243;n que las AAPP ponen a disposici&#243;n de empresas o ciudadanos para su uso y posible desarrollo de aplicaciones, lo que se conoce como &quot;Open Data&quot;. En este caso no se usan servicios Web, como es el caso de la intermediaci&#243;n, sino documentos HTML. Se formar&#225;n cat&#225;logos de informaci&#243;n reutilizables, con interfaces de publicaci&#243;n y consulta.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379535769665" ID="ID_685044728" MODIFIED="1391853359154" POSITION="right" TEXT="Informaci&#xf3;n reutilizable">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379535778688" ID="ID_736208635" MODIFIED="1381857406711" TEXT="Mayor relevancia y potencial social y econ&#xf3;mico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379535799036" ID="ID_1481382380" MODIFIED="1379581731200" TEXT="Documentos primarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379535807679" ID="ID_1834517991" MODIFIED="1379581732130" TEXT="Nivel granular lo m&#xe1;s fino posible">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379535818719" ID="ID_457503406" MODIFIED="1379581739190" TEXT="Tendr&#xe1;n asociada informaci&#xf3;n estructurada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379535857698" ID="ID_376479288" MODIFIED="1379581741225" TEXT="Actualizados a sus &#xfa;ltimas versiones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379535870829" ID="ID_1603097650" MODIFIED="1385117320520" POSITION="right" TEXT="Identificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379535875989" ID="ID_551249414" MODIFIED="1379581744438" TEXT="Referencias &#xfa;nicas y un&#xed;vocas (URI)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379535900127" ID="ID_832050845" MODIFIED="1391853542909" TEXT="Para la construcci&#xf3;n de URIs">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379535915532" ID="ID_707916923" MODIFIED="1379581746362" TEXT="Protocolos HTTP y HTTPS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379536007889" ID="ID_1356440710" MODIFIED="1379581748308" TEXT="Esquema consistente, extensible y persistente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379536050998" ID="ID_774103542" MODIFIED="1379581749214" TEXT="Estructura comprensible">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379536060287" ID="ID_1106758227" MODIFIED="1379581750215" TEXT="No desvelar&#xe1;n informaci&#xf3;n acerca de la implementaci&#xf3;n t&#xe9;cnica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379536744472" ID="ID_921368461" MODIFIED="1379582652642" TEXT="Persistencia, no deber&#xe1;n variar">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379536770748" ID="ID_1126585970" MODIFIED="1383913717518" TEXT="Nueva ubicaci&#xf3;n HTTP 3XX">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379536794155" ID="ID_1914410709" MODIFIED="1383913720038" TEXT="Ha desaparecido HTTP 410">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1379536090542" ID="ID_1313907217" MODIFIED="1385117337618" POSITION="right" TEXT="Descripci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379536113129" ID="ID_1711611167" MODIFIED="1383913488532" TEXT="Metadatos m&#xed;nimos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379536124295" ID="ID_1811914684" MODIFIED="1383939881442" TEXT="CISE para la reutilizaci&#xf3;n de vocabularios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379536162412" ID="ID_1104768868" MODIFIED="1385117343780" POSITION="right" TEXT="Formatos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379536173670" ID="ID_997900475" MODIFIED="1383914942937" TEXT="Usar&#xe1;n est&#xe1;ndares abiertos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379536183857" ID="ID_1808674847" MODIFIED="1379582229089" TEXT="De forma complementaria, est&#xe1;ndares de uso generalizado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379536283550" ID="ID_17754331" MODIFIED="1383917672874" TEXT="Preferentemente formatos que ofrezc&#xe1;n representaci&#xf3;n SEM&#xc1;NTICA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1379536204612" ID="ID_1413441542" LINK="Cat&#xe1;logo%20de%20est&#xe1;ndares.mm" MODIFIED="1383939785705" STYLE="bubble" TEXT="Se ce&#xf1;ir&#xe1;n a la NTI de Cat&#xe1;logo de est&#xe1;ndares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379536362725" ID="ID_1926110618" MODIFIED="1385117353109" POSITION="right" TEXT="Puesta a disposici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379536372526" ID="ID_1456422812" MODIFIED="1383913522629" TEXT="Principio de accesibilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379536402878" ID="ID_1798469224" MODIFIED="1383913525677" TEXT="Informaci&#xf3;n estructurada sobre los documentos susceptibles de reutilizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379536540928" ID="ID_1010504540" MODIFIED="1385117359380" POSITION="right" TEXT="Cat&#xe1;logos de informaci&#xf3;n p&#xfa;blica reutilizables">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379536564155" ID="ID_21936197" MODIFIED="1391853531706" TEXT="Se implementar&#xe1;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379536578753" ID="ID_867492940" MODIFIED="1397557839415" TEXT="Una interfaz de PUBLICACION, para que las entidades puedan publicar sus datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379536588017" ID="ID_1526156225" MODIFIED="1397557858540" TEXT="Una interfaz de CONSULTA, para su uso automatizado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379536604415" ID="ID_1234658520" MODIFIED="1383917773726" TEXT="En la descripci&#xf3;n de cada categor&#xed;a de los documentos incluiran los metadatos obligatorios (Anexo III)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379536618369" ID="ID_688185597" MODIFIED="1385113861461" TEXT="El acceso al contenido se har&#xe1;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379536625206" ID="ID_1783215308" MODIFIED="1381858029683" TEXT="Mediante documentos HTML">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379536642047" ID="ID_1381076459" MODIFIED="1385113874487" TEXT="Mediante informaci&#xf3;n procesable autom&#xe1;ticamente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379536681347" ID="ID_935970808" MODIFIED="1379536691243" TEXT="Vocabulario DCAT">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</map>
