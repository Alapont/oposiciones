<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1379512548063" ID="ID_1302893698" MODIFIED="1379513081456" TEXT="Cat&#xe1;logo de est&#xe1;ndares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379512631143" ID="ID_1121483686" MODIFIED="1383895469091" POSITION="right" TEXT="Est&#xe1;ndares m&#xed;nimos necesarios para interoperabilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379512657466" ID="ID_1515519689" MODIFIED="1391805869366" POSITION="right" TEXT="Estado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379512662328" ID="ID_979288837" MODIFIED="1379513149284" TEXT="Admitido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379512666389" ID="ID_898493410" MODIFIED="1379513151125" TEXT="En abandono">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379512682543" ID="ID_1957154863" MODIFIED="1391805870425" POSITION="right" TEXT="Uso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379512695109" ID="ID_1742963111" MODIFIED="1383944334924" TEXT="Cada Administraci&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Parecido a lo que se hace con los modelos de datos
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379512703012" ID="ID_491545260" MODIFIED="1380629261290" TEXT="Est&#xe1;ndares que se ajusten a sus necesidades">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379512717136" ID="ID_1616918981" MODIFIED="1383894672041" TEXT="Para interacci&#xf3;n con otras administraciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379512725648" ID="ID_1797348633" MODIFIED="1381770519053" TEXT="seleccionados por el emisor">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379512755029" ID="ID_431407718" MODIFIED="1380629265672" TEXT="Publicar&#xe1; los que use para servicios y tr&#xe1;mites">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379512771432" ID="ID_101167826" MODIFIED="1383894624285" TEXT="Podr&#xe1; usar otros formatos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379512784092" ID="ID_643450012" MODIFIED="1391805876519" POSITION="right" TEXT="Revisi&#xf3;n y actualizaci&#xf3;n del Cat&#xe1;logo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379512847093" ID="ID_924582775" MODIFIED="1379513143169" TEXT="Con periodicidad ANUAL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379512802445" ID="ID_1915829246" MODIFIED="1379512810044" TEXT="Encuesta a las Administraciones"/>
<node CREATED="1379512810440" ID="ID_690517316" MODIFIED="1379512879294" TEXT="Valoraci&#xf3;n y eliminaci&#xf3;n"/>
<node CREATED="1379512814473" ID="ID_1722043115" MODIFIED="1379512820340" TEXT="Revisi&#xf3;n del resto"/>
<node CREATED="1379512820627" ID="ID_1897998508" MODIFIED="1379512829973" TEXT="Identificaci&#xf3;n de nuevos est&#xe1;ndares"/>
<node CREATED="1379512883122" ID="ID_583063131" MODIFIED="1379512893186" TEXT="Valoraci&#xf3;n de nuevas necesidades"/>
</node>
<node CREATED="1379512918136" ID="ID_1032345196" MODIFIED="1379578041869" TEXT="Se podr&#xe1; solicitar formalmente una revisi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379512935477" ID="ID_592149959" MODIFIED="1383943340932" TEXT="Secretar&#xed;a Ejecutiva del Comit&#xe9; Sectorial de Administraci&#xf3;n Electr&#xf3;nica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379512992876" ID="ID_1552436725" MODIFIED="1391805879109" POSITION="right" TEXT="Atributos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379512999578" ID="ID_1260255761" MODIFIED="1379578066455" TEXT="Cadena de Interoperabilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379513015730" ID="ID_1315297233" MODIFIED="1379578068499" TEXT="Categor&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379513036704" ID="ID_476881035" MODIFIED="1379578069560" TEXT="Nombre">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379513039036" ID="ID_1325907276" MODIFIED="1379578070355" TEXT="Tipo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379513041119" ID="ID_938587087" MODIFIED="1379578071198" TEXT="Versi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379513046883" ID="ID_1863721638" MODIFIED="1379578071993" TEXT="Extensi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379513051072" ID="ID_1928801824" MODIFIED="1379578072929" TEXT="Especificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379513056633" ID="ID_1880275492" MODIFIED="1379578074286" TEXT="Estado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</map>
