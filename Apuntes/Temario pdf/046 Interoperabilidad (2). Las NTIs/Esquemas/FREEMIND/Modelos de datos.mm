<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1379533105812" ID="ID_800214625" MODIFIED="1383937494974" TEXT=" Relaci&#xf3;n de modelos de datos">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Se publicar&#225;n en el Centro de Interoperabilidad Sem&#225;ntica (CISE)</b>
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379533125835" ID="ID_1506181818" MODIFIED="1391805938118" POSITION="right" TEXT="Modelos de datos a publicar">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379533177847" ID="ID_1005695356" MODIFIED="1383938266608" TEXT="Sujetos a intercambio con los ciudadanos y otras AAPP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1379533208176" ID="ID_534635588" MODIFIED="1385110012677" TEXT=" Infraestructuras, servicios y herramientas comunes que NO sean de uso exclusivamente INTERNO">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
</node>
<node CREATED="1379533233274" ID="ID_1321172260" MODIFIED="1385112479063" POSITION="right" TEXT="Estructura de intercambio de los modelos de datos, deber&#xe1; contener:">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379533242773" ID="ID_114277928" MODIFIED="1383937639612" TEXT="a) Activos sem&#xe1;nticos, en formato XSD">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379533259044" ID="ID_1826527340" MODIFIED="1391805942647" TEXT="b) Guias explicativas, en formato PDF, que incluir&#xe1;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379533299171" ID="ID_95990737" MODIFIED="1383937620370" TEXT="Descripci&#xf3;n de los tipos y definiciones de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1379533323811" ID="ID_1630453795" MODIFIED="1383937623319" TEXT="Condiciones de seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1379533334408" ID="ID_1302449309" MODIFIED="1383937625284" TEXT="Condiciones que deben cumplir los receptores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1379533351490" ID="ID_1151645870" MODIFIED="1383937627333" TEXT="Ejemplos de implementaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1379533367172" ID="ID_1890194940" MODIFIED="1383937629067" TEXT="Opcionalmente, manuales de ayuda">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-5"/>
</node>
</node>
</node>
<node CREATED="1383937702543" ID="ID_763466253" MODIFIED="1391805964224" POSITION="right" TEXT="Identificaci&#xf3;n de los modelos de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383937727989" ID="ID_676521896" MODIFIED="1383937738919" TEXT="Descritos en el Anexo II"/>
<node CREATED="1383937739301" ID="ID_609560646" LINK="Cat&#xe1;logo%20de%20est&#xe1;ndares.mm" MODIFIED="1384196975382" TEXT="La descripci&#xf3;n se ajustar&#xe1; a la NTI del Cat&#xe1;logo de Est&#xe1;ndares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379533458925" ID="ID_1034264451" MODIFIED="1391805965241" POSITION="right" TEXT="Interacci&#xf3;n con el CISE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383937878821" ID="ID_925182052" MODIFIED="1383944301833" TEXT="Cada Administraci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383937892101" ID="ID_22126392" MODIFIED="1383937960082" TEXT="a) Identificar&#xe1; los modelos de datos susceptibles de ser intercambiados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383937923716" ID="ID_392167773" MODIFIED="1391806032132" TEXT="b) Facilitar&#xe1; la estructura de intercambio de los modelos de datos, mediante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383938000003" ID="ID_902379426" MODIFIED="1383938030426" TEXT="Recopilaci&#xf3;n y dep&#xf3;sito a trav&#xe9;s de SARA, para su carga masiva"/>
<node CREATED="1383938030807" ID="ID_957552449" MODIFIED="1383938167244" TEXT="Entorno de intercambio identificado por su URL"/>
<node CREATED="1383938053399" ID="ID_1566484867" MODIFIED="1383938084912" TEXT="Publicaci&#xf3;n de cada modelo de datos a trav&#xe9;s de las herramientas de que disponga el CISE"/>
</node>
<node CREATED="1383938090951" ID="ID_915716707" MODIFIED="1383938152546" TEXT="c) Actualizar&#xe1; de forma proactiva la informaci&#xf3;n facilitada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383938124386" ID="ID_1289498546" MODIFIED="1383938153881" TEXT="d) Podr&#xe1; consultar la informaci&#xf3;n disponible en el repositorio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379533796254" ID="ID_973553660" MODIFIED="1383912469428" POSITION="right" TEXT="En el Comit&#xe9; Sectorial de Administraci&#xf3;n Electr&#xf3;nica se identificar&#xe1;n, catalogar&#xe1;n y priorizar&#xe1;n los modelos de datos comunes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379533749061" ID="ID_100297618" MODIFIED="1385112512743" TEXT="Los modelos de datos comunes podr&#xe1;n ser ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379533771540" ID="ID_569639188" MODIFIED="1391806067636" TEXT="Obligatoria aplicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383938254718" ID="ID_1099527530" MODIFIED="1383938269828" TEXT="Identificados por el CISE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379533760128" ID="ID_533753875" MODIFIED="1379581330283" TEXT="Preferente aplicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</map>
