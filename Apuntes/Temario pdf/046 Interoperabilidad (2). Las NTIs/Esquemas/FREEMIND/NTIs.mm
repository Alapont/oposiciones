<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1383938323365" ID="ID_51053026" MODIFIED="1383940517679" TEXT="NTIs">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383940736270" ID="ID_1940100651" MODIFIED="1383940810339" POSITION="right" TEXT="Relativas a servicios prestados">
<cloud/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383940749506" ID="ID_541430152" LINK="Reutilizaci&#xf3;n.mm" MODIFIED="1383940777310" TEXT="Reutilizaci&#xf3;n de la informaci&#xf3;n (Open Data)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383940781953" ID="ID_1103717407" LINK="Intermediaci&#xf3;n%20de%20datos.mm" MODIFIED="1383940804504" TEXT="Intermediaci&#xf3;n de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383944983085" ID="ID_1825466903" LINK="../SARA.mm" MODIFIED="1384192163211" TEXT="SARA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1383938406560" FOLDED="true" ID="ID_546604900" LINK="Pol&#xed;tica%20de%20Gesti&#xf3;n%20de%20Documentos%20Electr&#xf3;nicos.mm" MODIFIED="1397554824219" POSITION="right" TEXT="Pol&#xed;tica de Gesti&#xf3;n de documentos electr&#xf3;nicos">
<cloud/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383938373861" ID="ID_968188627" LINK="Documento%20electr&#xf3;nico.mm" MODIFIED="1383945725468" TEXT="Documento electr&#xf3;nico">
<arrowlink DESTINATION="ID_490272369" ENDARROW="Default" ENDINCLINATION="529;0;" ID="Arrow_ID_775444726" STARTARROW="None" STARTINCLINATION="529;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383938383018" ID="ID_986372914" LINK="Expediente%20electr&#xf3;nico.mm" MODIFIED="1383944893624" TEXT="Expediente electr&#xf3;nico">
<arrowlink DESTINATION="ID_490272369" ENDARROW="Default" ENDINCLINATION="523;0;" ID="Arrow_ID_516769640" STARTARROW="None" STARTINCLINATION="523;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383940897168" ID="ID_1215277223" LINK="Digitalizaci&#xf3;n%20de%20documentos.mm" MODIFIED="1383944893626" TEXT="Digitalizaci&#xf3;n de documentos">
<arrowlink DESTINATION="ID_490272369" ENDARROW="Default" ENDINCLINATION="555;0;" ID="Arrow_ID_1744437815" STARTARROW="None" STARTINCLINATION="555;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383940882512" ID="ID_1454921075" LINK="Copiado%20aut&#xe9;ntico%20y%20conversi&#xf3;n.mm" MODIFIED="1383944893628" TEXT="Copiado aut&#xe9;ntico y conversi&#xf3;n">
<arrowlink DESTINATION="ID_490272369" ENDARROW="Default" ENDINCLINATION="779;0;" ID="Arrow_ID_1381830715" STARTARROW="None" STARTINCLINATION="779;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1383938389431" ID="ID_267026157" LINK="Modelos%20de%20datos.mm" MODIFIED="1383946009350" TEXT="Modelos de Datos">
<arrowlink DESTINATION="ID_490272369" ENDARROW="Default" ENDINCLINATION="173;0;" ID="Arrow_ID_725787666" STARTARROW="None" STARTINCLINATION="173;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1383938334977" ID="ID_490272369" LINK="Cat&#xe1;logo%20de%20est&#xe1;ndares.mm" MODIFIED="1383944893628" POSITION="right" TEXT="Cat&#xe1;logo de est&#xe1;ndares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383942353892" ID="ID_387274527" LINK="Pol&#xed;tica%20de%20firma%20electr&#xf3;nica.mm" MODIFIED="1384192178858" POSITION="right" TEXT="Pol&#xed;tica de Firma Electr&#xf3;nica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</map>
