<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1379531231534" ID="ID_1871311751" MODIFIED="1383917194443" TEXT="Intermediaci&#xf3;n de datos">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se refiere normalmente a los datos que una AP pone a disposici&#243;n de otra a trav&#233;s de determinados servicios (normalmente servicios Web). El MHAP ofrece la plataforma de intermediaci&#243;n.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379531333945" ID="ID_1219386837" MODIFIED="1385114071423" POSITION="right" TEXT="Actores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379531453484" ID="ID_1743024338" MODIFIED="1380651349249" TEXT="Cedente y Emisor (tecnol&#xf3;gico)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379531419660" ID="ID_609500900" MODIFIED="1380651359897" TEXT="Cesionario y Requirente (tecnol&#xf3;gico)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379531479937" ID="ID_928956749" MODIFIED="1381835817924" POSITION="right" TEXT="MHAP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379531494976" ID="ID_401558998" MODIFIED="1381834078484" TEXT="Nodo de interoperabilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379531526823" ID="ID_1874293648" MODIFIED="1397572231248" TEXT="Rol">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379531530679" ID="ID_1546913260" MODIFIED="1379581556834" TEXT="Gestionar&#xe1; Cesionarios y requirentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379531555353" ID="ID_1804972846" MODIFIED="1379581391886" TEXT="No almacenar&#xe1; informaci&#xf3;n personal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379531565702" ID="ID_1297802862" MODIFIED="1379581620797" TEXT="Asegurar&#xe1; confidencialidad e integridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379531586884" ID="ID_1290309295" MODIFIED="1385114261790" TEXT="Mantendr&#xe1; un portal web informativo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379531601385" ID="ID_793063390" MODIFIED="1379531609140" TEXT="Cat&#xe1;logo de servicios"/>
<node CREATED="1379531609729" ID="ID_1816519229" MODIFIED="1379531620333" TEXT="Formularios de solicitud de acceso a los servicios"/>
<node CREATED="1379531620842" ID="ID_1692561169" MODIFIED="1379531629346" TEXT="Acuerdos de prestaci&#xf3;n de servicios"/>
<node CREATED="1379531629948" ID="ID_1651849708" MODIFIED="1379531633109" TEXT="Novedades"/>
</node>
<node CREATED="1379531637457" ID="ID_834337840" MODIFIED="1383906251771" TEXT="Mantendr&#xe1; el sistema 24x7">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379531649409" ID="ID_137217744" MODIFIED="1383906252682" TEXT="Dar&#xe1; soporte a las organizaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379531661122" ID="ID_189835481" MODIFIED="1379581401382" TEXT="Mantendr&#xe1; un centro de atenci&#xf3;n a usuarios e integradores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379531686829" ID="ID_30005718" MODIFIED="1383906255082" TEXT="Informes de actividad y uso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379531698035" ID="ID_1919677278" MODIFIED="1383906255982" TEXT="Evolucionar&#xe1; y mantendr&#xe1; sus sistemas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379531708179" ID="ID_1559430877" MODIFIED="1383906256983" TEXT="Colaborar&#xe1; con las labores de auditor&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379531893927" ID="ID_1191575711" MODIFIED="1397572239100" TEXT="Requisitos t&#xe9;cnicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379531913212" ID="ID_999351810" MODIFIED="1379581547716" TEXT="Garantizar&#xe1; la interoperabilidad, disponibilidad, fiabilidad y seguridad de la informaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379531953142" ID="ID_1625058395" MODIFIED="1379581416767" TEXT="Usar&#xe1; SARA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379531962907" ID="ID_1578912596" MODIFIED="1385114291867" TEXT="Seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379531971757" FOLDED="true" ID="ID_650185681" MODIFIED="1385114413344" TEXT="Autenticidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379531988077" ID="ID_983231740" MODIFIED="1383906781278" TEXT="Marco operacional">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379532008669" ID="ID_1883833505" MODIFIED="1379581462689" TEXT="Control de acceso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379532018262" ID="ID_336590623" MODIFIED="1379581463537" TEXT="Protecci&#xf3;n de la informaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379532029248" FOLDED="true" ID="ID_177532801" MODIFIED="1385114414701" TEXT="Confidencialidad e integridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379532041432" ID="ID_1026142325" MODIFIED="1383906778897" TEXT="Medidas de protecci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379532054155" ID="ID_432154178" MODIFIED="1379581467625" TEXT="Protecci&#xf3;n de las comunicaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379532067856" ID="ID_690447096" MODIFIED="1379581469089" TEXT="Protecci&#xf3;n de la informaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379532136749" FOLDED="true" ID="ID_755861251" MODIFIED="1385114416402" TEXT="Disponibilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379532145759" ID="ID_1854753368" MODIFIED="1383906783278" TEXT="Medidas de protecci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379532157505" ID="ID_1170620056" MODIFIED="1379581473929" TEXT="Protecci&#xf3;n de los servicios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379532169960" ID="ID_634039626" MODIFIED="1383916299685" TEXT="Trazabilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379532430309" ID="ID_494164622" MODIFIED="1383907176398" TEXT="No almacenar&#xe1; informaci&#xf3;n sobre el contenido del intercambio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="closed"/>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1379532487927" FOLDED="true" ID="ID_76661608" MODIFIED="1385115069949" TEXT="Informaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379532492830" ID="ID_350626496" MODIFIED="1383907171236" TEXT="ID de transacci&#xf3;n &#xfa;nico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379532501037" ID="ID_1992229142" MODIFIED="1379581442240" TEXT="Cesionario">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379532505428" ID="ID_299152589" MODIFIED="1379581443160" TEXT="Tipo de informaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379532521573" ID="ID_729816654" MODIFIED="1379581444016" TEXT="Fecha y hora de realizaci&#xf3;n de la consulta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379532842657" FOLDED="true" ID="ID_1037386008" MODIFIED="1385117708728" TEXT="Medidas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379532359507" ID="ID_541324219" MODIFIED="1379581455657" TEXT="Protecci&#xf3;n de los registros de actividad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379532382865" ID="ID_705330173" MODIFIED="1379581454120" TEXT="Registros de actividad de los usuarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379532394231" ID="ID_227176283" MODIFIED="1379581457993" TEXT="Sellos de tiempo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1379532200215" ID="ID_1299749182" MODIFIED="1385115081306" TEXT="Tecnolog&#xed;as y est&#xe1;ndares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379532221007" FOLDED="true" ID="ID_1291937685" MODIFIED="1385117740318" TEXT="Servicios Web">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379532230760" ID="ID_1836747980" MODIFIED="1379581542523" TEXT="Definidos mediante WSDL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379532244286" ID="ID_1097848165" MODIFIED="1379581550568" TEXT="Mensajes en formato XML">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379532259794" ID="ID_160197497" MODIFIED="1379581552371" TEXT="Seguridad en las comunicaciones mediante TLS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379532272595" ID="ID_921805819" MODIFIED="1379532326633" TEXT="Servicios de intercambio SCSP 3.0 (Sustituci&#xf3;n de Certificados en Soporte Papel)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379532563622" ID="ID_581496184" MODIFIED="1385114473545" TEXT="Cat&#xe1;logo de servicios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379532622507" FOLDED="true" ID="ID_1309962499" MODIFIED="1385117715592" TEXT="Disponible en ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379532627954" ID="ID_1629514246" MODIFIED="1381835596499" TEXT="Punto informativo del Cedente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379532639452" ID="ID_311363281" MODIFIED="1381835598840" TEXT="Plataforma de Intermediaci&#xf3;n del MHAP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379533007029" ID="ID_1875509165" MODIFIED="1381835581366" TEXT="ACTUALMENTE: SERVICIOS DE VERIFICACI&#xd3;N DE DATOS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379532793841" FOLDED="true" ID="ID_111866574" MODIFIED="1385117716154" TEXT="Para la publicaci&#xf3;n de nuevos servicios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379532813565" ID="ID_330739522" MODIFIED="1379581688098" TEXT="UDDI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379532816586" ID="ID_1689109340" MODIFIED="1379581690622" TEXT="Servicio de directorio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379532658579" FOLDED="true" ID="ID_148035001" MODIFIED="1385117717012" TEXT="Instrumentos para la interoperabilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379532694188" ID="ID_1905248160" MODIFIED="1381835590079" TEXT="Inventario de procedimientos administrativos y servicios prestados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379532714288" ID="ID_614962351" MODIFIED="1383939935976" TEXT="Centro de Interoperabilidad Sem&#xe1;ntica de la Administraci&#xf3;n (CISE)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</map>
