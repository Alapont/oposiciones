<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1379515524273" ID="ID_944284197" MODIFIED="1379516732199" TEXT="Copiado aut&#xe9;ntico y conversi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379515600075" ID="ID_1474140212" MODIFIED="1395417650468" POSITION="right" TEXT="Copias electr&#xf3;nicas aut&#xe9;nticas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379515664995" ID="ID_1184717999" MODIFIED="1379515701298" TEXT="Si no hay cambio de formato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
<node CREATED="1379515674333" ID="ID_317405235" MODIFIED="1379515705619" TEXT="Eficacia jur&#xed;dica del documento original">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1379515710586" ID="ID_1124407792" MODIFIED="1380651975685" TEXT="A partir de originales o copia aut&#xe9;ntica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379515741341" ID="ID_44509824" MODIFIED="1380651980705" TEXT="Ser&#xe1;n nuevos documentos electr&#xf3;nicos (cumpliran NTI)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379515778634" ID="ID_1054759150" MODIFIED="1383896717778" TEXT="Metadatos m&#xed;nimo obligatorios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379515801326" ID="ID_621704495" MODIFIED="1383896719884" TEXT="En funci&#xf3;n de las caracter&#xed;sticas propias de cada metadato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379515821846" ID="ID_720219074" MODIFIED="1383896721023" TEXT="Relaci&#xf3;n con el original">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379515841137" ID="ID_430608840" MODIFIED="1379515869588" TEXT="Metadato &quot;Identificador de documento origen&quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1379515874820" ID="ID_407773552" MODIFIED="1384195028844" STYLE="bubble" TEXT="Firmadas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379515885984" ID="ID_902006870" MODIFIED="1384195028844" TEXT="Sistemas de firma art. 18 y 19 LAESCP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1379515908063" ID="ID_1748457009" MODIFIED="1385112201179" POSITION="right" TEXT="Copias aut&#xe9;nticas con cambio de formato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379515953132" ID="ID_1940318280" MODIFIED="1379580483477" TEXT="Metadato obligatorio &quot;Estado de elaboraci&#xf3;n&quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379515982891" ID="ID_1815406165" MODIFIED="1379516003405" TEXT="&quot;Copia electr&#xf3;nica aut&#xe9;ntica con cambio de formato&quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379516462168" ID="ID_716620209" MODIFIED="1383897052282" TEXT="Conversi&#xf3;n">
<arrowlink DESTINATION="ID_379025290" ENDARROW="Default" ENDINCLINATION="215;0;" ID="Arrow_ID_458143467" STARTARROW="None" STARTINCLINATION="215;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379516454703" ID="ID_1543882399" MODIFIED="1385112204315" POSITION="right" TEXT="Copia aut&#xe9;ntica de documentos papel">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379515953132" ID="ID_1901502972" MODIFIED="1379580485157" TEXT="Metadato obligatorio &quot;Estado de elaboraci&#xf3;n&quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379515982891" ID="ID_882615390" MODIFIED="1379516554366" TEXT="&quot;Copia electr&#xf3;nica aut&#xe9;ntica de documento papel&quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379516557164" ID="ID_16724907" LINK="Digitalizaci&#xf3;n%20de%20documentos.mm" MODIFIED="1379580790792" TEXT="Digitalizaci&#xf3;n segun NTI Digitalizaci&#xf3;n de documentos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379516393528" ID="ID_841864133" MODIFIED="1385112208886" POSITION="right" TEXT="Copia electr&#xf3;nica parcial aut&#xe9;ntica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379515953132" ID="ID_395179490" MODIFIED="1379580487285" TEXT="Metadato obligatorio &quot;Estado de elaboraci&#xf3;n&quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379515982891" ID="ID_1416827082" MODIFIED="1379580503358" TEXT="&quot;Copia electr&#xf3;nica parcial aut&#xe9;ntica&quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379516640757" ID="ID_770413781" MODIFIED="1379580795440" TEXT="Se obtendr&#xe1;n mediante extractos de contenido del origen que permitan mantener la confidencialidad del resto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379515943813" ID="ID_379025290" MODIFIED="1385112262503" POSITION="right" TEXT="Conversi&#xf3;n de documentos ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379516027202" ID="ID_422475008" MODIFIED="1379516049231" TEXT="Generaci&#xf3;n de un nuevo documento electr&#xf3;nico (NTI)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379516073651" ID="ID_1254920635" MODIFIED="1383898098622" TEXT="Conversi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379516088762" ID="ID_332369120" LINK="Pol&#xed;tica%20de%20Gesti&#xf3;n%20de%20Documentos%20Electr&#xf3;nicos.mm" MODIFIED="1379580787728" TEXT="Procedimiento seg&#xfa;n la NTI de Pol&#xed;tica de Gesti&#xf3;n de Documentos Electr&#xf3;nicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379516183258" ID="ID_1347656497" MODIFIED="1379581205934" TEXT="Tratamiento espec&#xed;fico seg&#xfa;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379516200162" ID="ID_1845888658" MODIFIED="1383945575490" TEXT="Conservaci&#xf3;n del contenido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379516212617" ID="ID_442659046" MODIFIED="1383945579095" TEXT="Contexto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379516226946" ID="ID_890898167" MODIFIED="1383945582724" TEXT="Estructura">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379516236532" ID="ID_1052407762" LINK="Cat&#xe1;logo%20de%20est&#xe1;ndares.mm" MODIFIED="1379580774881" TEXT="Formato del nuevo documento seleccionado seg&#xfa;n NTI del Cat&#xe1;logo de est&#xe1;ndares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379516296849" ID="ID_728530131" MODIFIED="1379580778865" TEXT="Permitir&#xe1; la reproducci&#xf3;n de la informaci&#xf3;n del documento original">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379516330569" ID="ID_790567145" MODIFIED="1379516373970" TEXT="Si adem&#xe1;s debe ser considerado copia aut&#xe9;ntica">
<arrowlink DESTINATION="ID_1474140212" ENDARROW="Default" ENDINCLINATION="683;0;" ID="Arrow_ID_562950897" STARTARROW="None" STARTINCLINATION="683;0;"/>
</node>
</node>
</node>
</node>
</map>
