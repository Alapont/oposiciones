<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1368339659739" ID="ID_1012216057" MODIFIED="1382990878334" TEXT="Data Warehouse">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368339663084" ID="ID_1880963843" MODIFIED="1391294310530" POSITION="right" TEXT="Almac&#xe9;n de datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368339772524" ID="ID_110255883" MODIFIED="1385555255933" TEXT="Caracter&#xed;sticas (Bill Inmon)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Hay que tener en cuenta que no se almacena TODA la informaci&#xf3;n, sino aquella integrada, consolidada y en algunos casos&#xa0;&#xa0;ya multidimensional que servir&#xe1; posteriormente para su explotaci&#xf3;n (Reporting, CMI, OLAP, Data Mining).
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368339779289" ID="ID_859117802" MODIFIED="1378197606852" TEXT="Orientado a un tema">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368339787686" ID="ID_1675864671" MODIFIED="1378197608346" TEXT="Integrado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368339793454" ID="ID_1784128622" MODIFIED="1378197609627" TEXT="Variante en el tiempo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368339799971" ID="ID_1687478122" MODIFIED="1378197611139" TEXT="No vol&#xe1;til">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368339869885" ID="ID_1693468600" MODIFIED="1385555257290" TEXT="Operaciones (ETL)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Procesos: Extracci&#xf3;n, elaboraci&#xf3;n, carga y explotaci&#xf3;n.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368339874814" ID="ID_1361869352" MODIFIED="1378025588634" TEXT="Extracci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368339993081" ID="ID_1057838996" MODIFIED="1368340011959" TEXT="An&#xe1;lisis de las fuentes de datos"/>
<node CREATED="1368340012184" ID="ID_1372418363" MODIFIED="1368340021091" TEXT="Dise&#xf1;o de los procesos de extracci&#xf3;n"/>
<node CREATED="1368340021321" ID="ID_384341693" MODIFIED="1368340029577" TEXT="Desarrollo e implementaci&#xf3;n"/>
</node>
<node CREATED="1380797429808" ID="ID_594731339" MODIFIED="1380797446279" TEXT="Elaboraci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368340073792" ID="ID_1202313255" MODIFIED="1378025590797" TEXT="Filtrado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368340083416" ID="ID_1737925572" MODIFIED="1368340092487" TEXT="Eliminaci&#xf3;n defectuosos"/>
<node CREATED="1368340092794" ID="ID_1085570598" MODIFIED="1368340099966" TEXT="Eliminaci&#xf3;n duplicados"/>
<node CREATED="1368340100235" ID="ID_1116615326" MODIFIED="1368340110005" TEXT="Verificaci&#xf3;n de la consistencia"/>
<node CREATED="1368340110256" ID="ID_672525727" MODIFIED="1368340118079" TEXT="Verificaci&#xf3;n de la coherencia"/>
</node>
<node CREATED="1368340124110" ID="ID_188914632" MODIFIED="1378025593358" TEXT="Transformaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368340184200" ID="ID_1403058840" MODIFIED="1368340484689" TEXT="Modelo que de coherencia l&#xf3;gica (multidimensional)"/>
</node>
</node>
<node CREATED="1368340505927" ID="ID_746025661" MODIFIED="1380797455375" TEXT="Agregaci&#xf3;n (Carga)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368340511423" ID="ID_1870415961" MODIFIED="1368340523746" TEXT="R&#xe1;pidez y potencia"/>
</node>
<node CREATED="1368339889677" ID="ID_1486270253" MODIFIED="1378051615923" TEXT="Explotaci&#xf3;n y gesti&#xf3;n de consultas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368340536145" ID="ID_1853407262" MODIFIED="1384445102603" TEXT="Query and reporting">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368340553547" ID="ID_133436297" MODIFIED="1368340563371" TEXT="Cuadro de mando anal&#xed;tico"/>
<node CREATED="1368340563703" ID="ID_979617827" MODIFIED="1378197614562" TEXT="Cuadro de mando integral">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368340573685" ID="ID_634884897" MODIFIED="1384445090342" TEXT="OLAP (On-line Analytical Processing)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368340576473" ID="ID_466852649" MODIFIED="1384445094784" TEXT="Data mining">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1378197622501" ID="ID_310143026" MODIFIED="1378197661197" TEXT="Definici&#xf3;n (Kimball)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="13px" face="sans-serif" color="rgb(0, 0, 0)">Una copia de las transacciones de datos espec&#xed;ficamente estructurada para la consulta y el an&#xe1;lisis.</font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1368339730419" ID="ID_326466871" MODIFIED="1391274476844" POSITION="right" TEXT="OLAP (Anal&#xed;tico)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368340765510" ID="ID_440984430" MODIFIED="1552641611990" TEXT="Modelo multidimensional">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368340854159" ID="ID_1152181236" MODIFIED="1384445575265" TEXT="Tipos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368340794432" ID="ID_1253579064" MODIFIED="1385555437427" TEXT="MOLAP (cubos)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376738584333" ID="ID_23214625" MODIFIED="1378025572116" TEXT="Mayor rendimiento en extracci&#xf3;n y an&#xe1;lisis">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376738651824" ID="ID_1692760494" MODIFIED="1378025577302" TEXT="Requieren un esfuerzo inicial de modelizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368340774650" ID="ID_585686043" MODIFIED="1385555443088" TEXT="ROLAP (SGDB)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376478727111" ID="ID_1133187686" MODIFIED="1378025557902" TEXT="Son capaces de almacenar m&#xe1;s informaci&#xf3;n que los MOLAP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376738749187" ID="ID_1665383243" MODIFIED="1378025561594" TEXT="Deben hacer el cambio del modelo relacional al multidimensional">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376738915347" ID="ID_1222700896" MODIFIED="1378025563628" TEXT="Optimizadas para la inserci&#xf3;n y el acceso concurrente de los usuarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376478744656" ID="ID_1382755230" MODIFIED="1378025566019" TEXT="Se usan normalmente en Data Mart cuando el tiempo es cr&#xed;tico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376738711817" ID="ID_910664833" MODIFIED="1378026358242" TEXT="Estructura">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376478683428" ID="ID_260496117" MODIFIED="1378025482781" TEXT="Estrellla">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378026223303" ID="ID_634750185" MODIFIED="1378026317561" TEXT="una tabla de hechos, y una tabla para cada dimensi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1376478700657" ID="ID_1902424265" MODIFIED="1378025486523" TEXT="Copo de nieve">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378026257142" ID="ID_175961780" MODIFIED="1378026316843" TEXT="Variante de estrella, que presenta tablas de dimensi&#xf3;n NORMALIZADAS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1376478706286" ID="ID_1171743265" MODIFIED="1378025490364" TEXT="Constelaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378026329084" ID="ID_958995006" MODIFIED="1378026348503" TEXT="Varios esquemas copo de nieve o estrella que comparten dimensiones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1368340811751" ID="ID_105701933" MODIFIED="1368340814029" TEXT="HOLAP">
<node CREATED="1552641619220" ID="ID_1353644069" MODIFIED="1552641629361" TEXT="h&#xed;brido de los anteriores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1368340975453" ID="ID_466549489" MODIFIED="1384597026321" TEXT="Arquitectura">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368340885378" ID="ID_934040226" MODIFIED="1378025493958" TEXT="Tabla de hechos: medidas num&#xe9;ricas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368340891254" ID="ID_1857017769" MODIFIED="1378025497171" TEXT="Tablas de dimensiones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376478634879" ID="ID_1216264346" MODIFIED="1384445532636" TEXT="Informaci&#xf3;n jer&#xe1;rquica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376478650548" ID="ID_378510327" MODIFIED="1384445533683" TEXT="Permite la agregaci&#xf3;n en dimensiones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1376739316031" ID="ID_840851626" MODIFIED="1384448142051" TEXT="Problemas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368340914142" ID="ID_468884116" MODIFIED="1378025544195" TEXT="data sparsity. Existencia de huecos en la tabla multidimensional">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368340924570" ID="ID_475081825" MODIFIED="1378025549338" TEXT="data explosion. Demasiadas dimensiones ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1368341013253" ID="ID_100918701" MODIFIED="1384448380457" TEXT="Caracter&#xed;sticas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368341022044" ID="ID_583647091" MODIFIED="1378025540446" TEXT="Visi&#xf3;n multidimensional y jerarquizada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368341035846" ID="ID_1101444551" MODIFIED="1368341049162" TEXT="Analizar tendencias en el tiempo"/>
<node CREATED="1368341049254" ID="ID_725111989" MODIFIED="1378025535680" TEXT="N&#xfa;mero de dimensiones debe ser  reducido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368341066742" ID="ID_900082298" MODIFIED="1368341080760" TEXT="interactivos y concurrentes"/>
<node CREATED="1374513248846" ID="ID_528865862" MODIFIED="1384445757805" TEXT="Consultas r&#xe1;pidas (tiempo real)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368341089047" ID="ID_285024009" MODIFIED="1384597024058" TEXT="Funcionalidades">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368341097053" ID="ID_437273999" MODIFIED="1378025499889" TEXT="Rotaci&#xf3;n. Cambiar las vistas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368341125468" ID="ID_1070670788" MODIFIED="1378025503352" TEXT="Roll-up. Subir a un mayor nivel de agregaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368341131041" ID="ID_462445462" MODIFIED="1378025507634" TEXT="Drill-Down. Descender a un mayor nivel de detalle">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368341137074" ID="ID_423438635" MODIFIED="1378025510500" TEXT="Drill-Across. Moverse entre cubos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368341144144" ID="ID_345981438" MODIFIED="1378025513781" TEXT="Drill-Through. Consultas SQL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368341152064" ID="ID_1067275517" MODIFIED="1378025516693" TEXT="Slice and dice. Presentaci&#xf3;n de vistas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1376739088458" ID="ID_928899268" MODIFIED="1376739165022" TEXT="Modelo acu&#xf1;ado por E.F. Codd">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      "S&#xed;ntesis, an&#xe1;lisis y consolodaci&#xf3;n din&#xe1;mica de grandes vol&#xfa;menes de informaci&#xf3;n multidimensionales"
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1368340625154" ID="ID_1950379872" MODIFIED="1552641638568" POSITION="right" TEXT="OLTP (Transaccional)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368340628947" ID="ID_625553255" MODIFIED="1368340733275" TEXT="Optimizados para la inserci&#xf3;n"/>
<node CREATED="1368340733569" ID="ID_1862539182" MODIFIED="1369828479679" TEXT="Fuentes diversas (heterogeneidad)"/>
<node CREATED="1368340738444" ID="ID_1019916406" MODIFIED="1368340747996" TEXT="Falta de organizaci&#xf3;n"/>
<node CREATED="1368340750864" ID="ID_929596635" MODIFIED="1368340764068" TEXT="Modelo relacional"/>
</node>
<node CREATED="1368339746741" ID="ID_1210738916" MODIFIED="1552641640410" POSITION="right" TEXT="Data Mining">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368341712288" ID="ID_439525167" MODIFIED="1378198382220" TEXT="Objetivos: localizar patrones ocultos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378197935082" ID="ID_1129567354" MODIFIED="1384445982043" TEXT="Tareas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378197944840" ID="ID_1076063131" MODIFIED="1378198212717" TEXT="Detecci&#xf3;n de anomal&#xed;as">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1378197955387" ID="ID_1572013579" MODIFIED="1552641684983" TEXT="Asociar regla de aprendizaje">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1378198009286" ID="ID_564123505" MODIFIED="1378198025070" TEXT="B&#xfa;squeda de relaciones entre variables"/>
</node>
<node CREATED="1378197972700" ID="ID_662842003" MODIFIED="1552641682071" TEXT="Clustering">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1378197989229" ID="ID_196608710" MODIFIED="1380798116095" TEXT="Descubrimiento de grupos y estructuras"/>
</node>
<node CREATED="1378197982252" ID="ID_1069901818" MODIFIED="1552641683006" TEXT="Clasificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
<node CREATED="1378198101242" ID="ID_77728854" MODIFIED="1380798127502" TEXT="Asocia estructuras conocidas a los datos nuevos"/>
</node>
<node CREATED="1378198128474" ID="ID_1109252613" MODIFIED="1552641683614" TEXT="Regresi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-5"/>
<node CREATED="1378198135016" ID="ID_66551279" MODIFIED="1378198666794" TEXT="Asocia una funci&#xf3;n que modele el comportamiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378198168756" ID="ID_293805784" MODIFIED="1552641684158" TEXT="Resumen">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-6"/>
<node CREATED="1378198172402" ID="ID_528820561" MODIFIED="1378198189558" TEXT="Proporciona una representaci&#xf3;n m&#xe1;s compacta"/>
</node>
<node CREATED="1378198298986" ID="ID_1045066952" MODIFIED="1378198313456" TEXT="Mining de patrones secuenciales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368341561043" ID="ID_877948430" MODIFIED="1552641688183" TEXT="Modelos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368341573374" ID="ID_913254979" MODIFIED="1382025487393" TEXT="De verificaci&#xf3;n. Verifica la validez de la informaci&#xf3;n que se le presenta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368341579258" ID="ID_1800921886" MODIFIED="1382987480488" TEXT="De descubrimiento o NO SUPERVISADOS. Descubre nueva informaci&#xf3;n sin intervenci&#xf3;n del usuario  ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368341584903" ID="ID_639614877" MODIFIED="1382011586246" TEXT="Predictivo o SUPERVISADOS. Prev&#xe9; el modelo futuro m&#xe1;s probable">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376479062053" ID="ID_1139871932" MODIFIED="1376479122467" TEXT="Alternativas si no hay suficientes datos">
<node CREATED="1376479070127" ID="ID_975519341" MODIFIED="1384446547946" TEXT="Restringido. No obtener predicci&#xf3;n alguna">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376479090025" ID="ID_682248466" MODIFIED="1384446549133" TEXT="No restringido. Predicci&#xf3;n de menor probabilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1368341596180" ID="ID_264830897" MODIFIED="1384446556062" TEXT="T&#xe9;cnicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368341667285" ID="ID_1855538442" MODIFIED="1378198638374" TEXT="Redes neuronales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378198533036" ID="ID_146226585" MODIFIED="1378198640575" TEXT="Regresi&#xf3;n lineal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368341659287" ID="ID_784415868" MODIFIED="1382012651408" TEXT="Arboles de decisi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378198612756" ID="ID_517612653" MODIFIED="1384446563764" TEXT="ID3 (Quinlan)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se obtiene un conjunto de reglas a partir de un conjunto de ejemplos, por inducci&#xf3;n.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378198616581" ID="ID_1223855299" MODIFIED="1378198620974" TEXT="C4.5"/>
</node>
<node CREATED="1368341673827" ID="ID_635349718" MODIFIED="1378198644864" TEXT="M&#xe9;todos estad&#xed;sticos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368341600622" ID="ID_1821985936" MODIFIED="1382012655630" TEXT="Agrupamiento o clustering">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378198573700" ID="ID_1828662096" MODIFIED="1378198582240" TEXT="K-means"/>
<node CREATED="1378198582712" ID="ID_202852414" MODIFIED="1378198592386" TEXT="K-medoids"/>
</node>
<node CREATED="1368341680888" ID="ID_193278473" MODIFIED="1378198651184" TEXT="L&#xf3;gica borrosa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378198624623" ID="ID_472696676" MODIFIED="1378198653192" TEXT="Reglas de asociaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378136772293" ID="ID_112954976" MODIFIED="1384446641505" TEXT="Extensiones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378136778504" ID="ID_763473464" MODIFIED="1378198664978" TEXT="Web Mining">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378136789422" ID="ID_1926531722" MODIFIED="1378198671203" TEXT="Text Mining">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1378136927708" ID="ID_1549757253" MODIFIED="1552641771289" POSITION="right" TEXT="BI">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      La inteligencia de negocio (Business Intelligence) suele definirse como la transformaci&#243;n de los datos de la compa&#241;&#237;a en conocimiento para obtener una ventaja competitiva (Gartner Group)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384447831009" ID="ID_120083830" MODIFIED="1384447839513" TEXT="Usa estad&#xed;stica descriptiva">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1552641779097" ID="ID_238241503" MODIFIED="1552641794550" TEXT="Transformaci&#xf3;n de los datos de la compa&#xf1;&#xed;a en conocimiento para obtener una ventaja competitiva ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1552641804386" ID="ID_1350172231" MODIFIED="1552641838780" TEXT="Caracter&#xed;sticas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1552641817173" ID="ID_1446183793" MODIFIED="1552641839426" TEXT="Accesibilidad a la informaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1552641821948" ID="ID_1328000846" MODIFIED="1552641842277" TEXT="Apoyo en la toma de decisiones. ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1552641827094" ID="ID_297799696" MODIFIED="1552641841654" TEXT="Orientaci&#xf3;n al usuario final. ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</map>
