<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1410617868092" ID="ID_1477224111" MODIFIED="1410807748431" TEXT="Ley 9/2014 General de Telecomunicaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1410617968527" ID="ID_704685059" MODIFIED="1411631529349" POSITION="left" TEXT="Disposiciones transitorias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1410622572667" ID="ID_1332134919" MODIFIED="1410622589626" TEXT="1&#xaa; Continuaci&#xf3;n de la vigencia de las normas anteriores"/>
<node CREATED="1410622595583" ID="ID_1826929681" MODIFIED="1410674625231" TEXT="2&#xaa; Plazo de adaptaci&#xf3;n de los operadores controlados por las AAPP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1410622620467" ID="ID_223212070" MODIFIED="1410622650846" STYLE="bubble" TEXT="Un a&#xf1;o">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1410622853446" FOLDED="true" ID="ID_1851722569" MODIFIED="1410685142178" TEXT="3&#xaa; Concesiones de uso del dominio p&#xfa;blico radioel&#xe9;ctrico">
<node CREATED="1410622883898" ID="ID_78874717" MODIFIED="1410622909441" TEXT="Pasan a estar ligadas a concesiones de uso privativo"/>
</node>
<node CREATED="1410622927376" FOLDED="true" ID="ID_361921478" MODIFIED="1410685144170" TEXT="4&#xaa; Restricciones a los principios de neutralidad tecnol&#xf3;gica  y de servicios en los t&#xed;tullos habilitantes">
<node CREATED="1410622993484" ID="ID_1792063822" MODIFIED="1410674086061" STYLE="bubble" TEXT=" 25 de mayo de 2016">
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1792063822" ENDARROW="Default" ENDINCLINATION="429;87;" ID="Arrow_ID_385011644" SOURCE="ID_1602561779" STARTARROW="None" STARTINCLINATION="504;90;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1410673979774" ID="ID_1648186370" MODIFIED="1410674044129" TEXT="Se aplicar&#xe1;n a TODOS los t&#xed;tulos habilitantes a partir de esta fecha"/>
</node>
<node CREATED="1410623030109" ID="ID_1023797634" MODIFIED="1410623051278" TEXT="Si el periodo de vigencia se extiende m&#xe1;s all&#xe1;">
<node CREATED="1410623051278" ID="ID_734448413" MODIFIED="1410623080023" TEXT="Se puede solicitar a la SETSI una evaluaci&#xf3;n">
<node CREATED="1410673774186" ID="ID_1440164608" MODIFIED="1410673835993" TEXT="La SETSI notificar&#xe1; de su alcance al titular. "/>
<node CREATED="1410673836915" ID="ID_658816898" MODIFIED="1410673854142" TEXT="El titular la puede retirar en 15 d&#xed;as">
<node CREATED="1410673868200" ID="ID_1602561779" MODIFIED="1410674059283" TEXT="Si se retira, permanece hasta">
<arrowlink DESTINATION="ID_1792063822" ENDARROW="Default" ENDINCLINATION="429;87;" ID="Arrow_ID_385011644" STARTARROW="None" STARTINCLINATION="504;90;"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1410674106371" FOLDED="true" ID="ID_1882824607" MODIFIED="1410685146855" TEXT="5&#xaa; Servicios de tel&#xe9;x, telegr&#xe1;ficos y similares del art.28.2">
<node CREATED="1410674172576" ID="ID_625370101" MODIFIED="1410674207972" TEXT="Se prestar&#xe1;n por Correos y Telegr&#xe1;fos S.A."/>
</node>
<node CREATED="1410674214790" FOLDED="true" ID="ID_761250600" MODIFIED="1410685148936" TEXT="6&#xaa; Regimen transitorio para la fijaci&#xf3;n de tasas">
<node CREATED="1410675098231" ID="ID_1449850222" MODIFIED="1410675114163" TEXT="Vigentes hasta la inclusi&#xf3;n en la Ley de Presupuestos"/>
</node>
<node CREATED="1410674284913" FOLDED="true" ID="ID_313715512" MODIFIED="1410685139055" TEXT="7&#xaa; Licencias y autorizaciones realizadas con anterioridad">
<node CREATED="1410674338392" ID="ID_1553513872" MODIFIED="1410674368836" TEXT="Se tramitar&#xe1;n por la normativa vigente en el momento de presentaci&#xf3;n de la solicitud"/>
<node CREATED="1410685122688" ID="ID_183961393" MODIFIED="1410685134755" TEXT="A menos que el inetresado desista"/>
</node>
<node CREATED="1410674482918" ID="ID_265715699" MODIFIED="1410674498889" TEXT="8&#xaa; Registro de operadores">
<node CREATED="1410674498890" ID="ID_1246870568" MODIFIED="1410674525429" TEXT="Se mantiene la inscripci&#xf3;n de los datos que ya figuren en el mismo"/>
</node>
<node CREATED="1410674526701" ID="ID_1077049085" MODIFIED="1410675127206" TEXT="9&#xaa;  Plazo de adaptaci&#xf3;n de la normativa y los instrumentos de planificaci&#xf3;n territorial y urban&#xed;stica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1410622620467" ID="ID_28579652" MODIFIED="1410622650846" STYLE="bubble" TEXT="Un a&#xf1;o">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1410674657701" FOLDED="true" ID="ID_1690405558" MODIFIED="1410685151537" TEXT="10&#xaa; Desempe&#xf1;o transitorio de funciones por la CNMC">
<node CREATED="1410674869679" ID="ID_1718435937" MODIFIED="1410674988779" TEXT="Hasta la fecha que se determine (D.A. 15&#xaa;)">
<arrowlink COLOR="#b0b0b0" DESTINATION="ID_813735954" ENDARROW="Default" ENDINCLINATION="1918;441;" ID="Arrow_ID_1504954786" STARTARROW="None" STARTINCLINATION="1789;0;"/>
</node>
</node>
<node CREATED="1410674677590" FOLDED="true" ID="ID_865829356" MODIFIED="1410685153010" TEXT="11&#xaa; Procedimientos anteriores a la ley sobre funciones de la CNMC">
<node CREATED="1410674760361" ID="ID_899083527" MODIFIED="1410674790103" TEXT="Si son ahora competencia del MINETUR, continuar&#xe1;n tramit&#xe1;ndose por este Ministerio"/>
</node>
<node CREATED="1410674802043" FOLDED="true" ID="ID_695992239" MODIFIED="1410685154484" TEXT="12&#xaa; Regimen de las estaciones para las que se hubiera presentado licencia o autorizaci&#xf3;n">
<node CREATED="1410675050722" ID="ID_17412338" MODIFIED="1410675072856" TEXT="Podr&#xe1;n continuar instaladas y en funcionamiento"/>
</node>
</node>
<node CREATED="1410675165822" FOLDED="true" ID="ID_279710639" MODIFIED="1410807759896" POSITION="left">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Disposici&#243;n derogatoria &#250;nica: quedan derogadas
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1410675182239" ID="ID_1037209120" MODIFIED="1410675214667" TEXT="Ley 11/1998, de 24 de abril, General de Telecomunicaciones"/>
<node CREATED="1410675215001" ID="ID_1470935187" MODIFIED="1410675236893" TEXT="Ley 32/2003, de 3 de noviembre, General de Telecomunicaciones"/>
<node CREATED="1410675276926" ID="ID_1902379358" MODIFIED="1410675300089" TEXT="Cuantas disposiciones de igual o inferior rango se opongan a lo dispuesto"/>
</node>
<node CREATED="1410807774146" FOLDED="true" ID="ID_936581209" MODIFIED="1410810105375" POSITION="right" TEXT="Pre&#xe1;mbulo">
<node CREATED="1410808005622" FOLDED="true" ID="ID_1874374343" MODIFIED="1410808586261" TEXT="I">
<node CREATED="1410808022006" ID="ID_306054320" MODIFIED="1410808027592" TEXT="Sustituye a la anterior Ley 32/2003"/>
<node CREATED="1410808075328" ID="ID_10532060" MODIFIED="1410808388072" TEXT="Introduce el marco regulador europeo en comunicaciones electr&#xf3;nicas de 2009">
<node CREATED="1410808426190" ID="ID_1020710575" MODIFIED="1410808435034" TEXT="Directiva 2009/136/CE: Derechos de los usuarios"/>
<node CREATED="1410808444206" ID="ID_1175785090" MODIFIED="1410808451313" TEXT="Directiva 2009/140/CE: Mejor regulaci&#xf3;n"/>
</node>
</node>
<node CREATED="1410808592311" FOLDED="true" ID="ID_242765584" MODIFIED="1410810104365" TEXT="II">
<node CREATED="1410808607110" ID="ID_1338404512" MODIFIED="1410808621764" TEXT="Agenda Digital para Europa (Europa 2020)"/>
<node CREATED="1410808641925" ID="ID_1480761714" MODIFIED="1410808670922" TEXT="Agenda Digital para Espa&#xf1;a"/>
</node>
<node CREATED="1410808700486" FOLDED="true" ID="ID_1726342414" MODIFIED="1410810101749" TEXT="III">
<node CREATED="1410808712358" ID="ID_1064832146" MODIFIED="1410808719115" TEXT="Toma como base la competencia exclusiva estatal en materia de telecomunicaciones del art&#xed;culo 149.1.21.&#xaa; de la Constituci&#xf3;n y en las competencias de car&#xe1;cter transversal de los art&#xed;culos 149.1.1.&#xaa; y 149.1.13.&#xaa;"/>
<node CREATED="1410808727430" ID="ID_1403626288" MODIFIED="1410808736326" TEXT="Objetivos">
<node CREATED="1410808748110" ID="ID_223899471" MODIFIED="1410808750539" TEXT="asegurar un marco regulatorio claro y estable que fomente la inversi&#xf3;n, proporcione seguridad jur&#xed;dica y elimine las barreras que han dificultado el despliegue de redes, y un mayor grado de competencia en el mercado"/>
<node CREATED="1410808764703" ID="ID_1325794997" MODIFIED="1410808776048" TEXT="recuperar la unidad de mercado"/>
<node CREATED="1410810056390" ID="ID_1816208016" MODIFIED="1410810058987" TEXT="simplificaci&#xf3;n administrativa, eliminando y revisando licencias y autorizaciones. Reducci&#xf3;n de cargas administrativas"/>
<node CREATED="1410810071246" ID="ID_1770265310" MODIFIED="1410810073393" TEXT="se refuerza el control del dominio p&#xfa;blico radioel&#xe9;ctrico"/>
</node>
<node CREATED="1410810094816" ID="ID_1352765499" MODIFIED="1410810097031" TEXT="se incorporan las previsiones recogidas en la Ley 3/2013, de creaci&#xf3;n de la Comisi&#xf3;n Nacional de los Mercados y de la Competencia "/>
</node>
</node>
<node CREATED="1410810111583" FOLDED="true" ID="ID_1816108678" MODIFIED="1410990295804" POSITION="right" TEXT="T&#xed;tulo I: Disposiciones Generales">
<node CREATED="1410810168222" FOLDED="true" ID="ID_1194967977" MODIFIED="1410810566006" TEXT="Art.1: Objeto y &#xe1;mbito de aplicaci&#xf3;n de la Ley.">
<node CREATED="1410810246486" ID="ID_83636620" MODIFIED="1410810249931" TEXT="1.&#xc1;mbito:  la regulaci&#xf3;n de las telecomunicaciones (explotaci&#xf3;n de las redes y la prestaci&#xf3;n de los servicios de comunicaciones electr&#xf3;nicas; los recursos asociados) seg&#xfa;nel art&#xed;culo 149.1.21.&#xaa; de la Constituci&#xf3;n"/>
<node CREATED="1410810292894" ID="ID_1209726154" MODIFIED="1410810305793" TEXT="2. Exclusiones">
<node CREATED="1410810344254" ID="ID_1066535507" MODIFIED="1410810377276" TEXT="los contenidos audiovisuales transmitidos por las redes"/>
<node CREATED="1410810415078" ID="ID_1653019204" MODIFIED="1410810417433" TEXT="el r&#xe9;gimen b&#xe1;sico de los medios de comunicaci&#xf3;n social de naturaleza audiovisual "/>
<node CREATED="1410810421390" ID="ID_1849221918" MODIFIED="1410810465531" TEXT="los  Servicios regulados por la LSSICE que no consistan  en el transporte de se&#xf1;ales a trav&#xe9;s de redes de comunicaciones electr&#xf3;nicas"/>
</node>
</node>
<node CREATED="1410810506286" FOLDED="true" ID="ID_1424476841" MODIFIED="1410810660878" TEXT="Art.2: Las telecomunicaciones como servicios de inter&#xe9;s general.">
<node CREATED="1410810543272" ID="ID_1350402415" MODIFIED="1410810556952" TEXT="1. R&#xe9;gimen de libre competencia."/>
<node CREATED="1410810612143" ID="ID_1407332402" MODIFIED="1410810613709" TEXT="2. S&#xf3;lo tienen la consideraci&#xf3;n de servicio p&#xfa;blico la defensa nacional, la seguridad p&#xfa;blica, la seguridad vial y la protecci&#xf3;n civil. La imposici&#xf3;n de obligaciones de servicio p&#xfa;blico podr&#xe1; recaer sobre los operadores que obtengan derechos de ocupaci&#xf3;n del dominio p&#xfa;blico o de la propiedad privada, de derechos de uso del dominio p&#xfa;blico radioel&#xe9;ctrico, de derechos de uso de recursos p&#xfa;blicos de numeraci&#xf3;n, direccionamiento o de denominaci&#xf3;n o que ostenten la condici&#xf3;n de operador con poder significativo en un determinado mercado de referencia"/>
</node>
<node CREATED="1410810700584" FOLDED="true" ID="ID_1797781168" MODIFIED="1410814816342" TEXT="Art.3: Objetivos y principios de la Ley.">
<node CREATED="1410810700584" MODIFIED="1410810700584" TEXT="a) Fomentar la competencia"/>
<node CREATED="1410810700584" MODIFIED="1410810700584" TEXT="b) Desarrollar la econom&#xed;a y el empleo digital."/>
<node CREATED="1410810700584" MODIFIED="1410810700584" TEXT="c) Promover el despliegue de redes y la prestaci&#xf3;n de servicios, fomentando la conectividad y la interoperabilidad."/>
<node CREATED="1410810700585" MODIFIED="1410810700585" TEXT="d) Promover el desarrollo de la industria de productos y equipos de telecomunicaciones."/>
<node CREATED="1410810700585" MODIFIED="1410810700585" TEXT="e) Contribuir al desarrollo del mercado interior de servicios de comunicaciones electr&#xf3;nicas en la Uni&#xf3;n Europea."/>
<node CREATED="1410810700585" MODIFIED="1410810700585" TEXT="f) Promover la inversi&#xf3;n en infraestructuras"/>
<node CREATED="1410810700585" ID="ID_1964968657" MODIFIED="1410812186891" TEXT="g) Uso eficaz de los recursos limitados de telecomunicaciones (numeraci&#xf3;n, espectro y acceso a los derechos de ocupaci&#xf3;n de la propiedad p&#xfa;blica y privada)"/>
<node CREATED="1410810700585" MODIFIED="1410810700585" TEXT="h) Fomentar  la neutralidad tecnol&#xf3;gica."/>
<node CREATED="1410810700585" MODIFIED="1410810700585" TEXT="i) Cumplimiento de las obligaciones de servicio p&#xfa;blico."/>
<node CREATED="1410810700585" MODIFIED="1410810700585" TEXT="j) Defender los intereses de los usuarios."/>
<node CREATED="1410810700585" MODIFIED="1410810700585" TEXT="k) Salvaguardar y proteger en los mercados de telecomunicaciones las necesidades de grupos sociales espec&#xed;ficos (discapacitados,  dependientes...)"/>
<node CREATED="1410810700585" MODIFIED="1410810700585" TEXT="l) Facilitar el acceso de los usuarios discapacitados."/>
</node>
<node CREATED="1410810729910" FOLDED="true" ID="ID_1884861923" MODIFIED="1410990292245" TEXT="Art.4: Servicios de telecomunicaciones para la defensa nacional, la seguridad p&#xfa;blica, la seguridad vial y la protecci&#xf3;n civil.">
<node CREATED="1410812247084" MODIFIED="1410812247084" TEXT="1. S&#xf3;lo tienen la consideraci&#xf3;n de servicio p&#xfa;blico estos servicios ."/>
<node CREATED="1410812247086" MODIFIED="1410812247086" TEXT="2. La defensa nacional se reserva al Estado y se rige por su normativa espec&#xed;fica."/>
<node CREATED="1410812247087" ID="ID_1916440154" MODIFIED="1410812297972" TEXT="3. El Minetur es el competente para ejecutar la pol&#xed;tica de defensa nacional en el sector de las telecomunicaciones (con coordinaci&#xf3;n con el Ministerio de Defensa y siguiendo los criterios fijados por &#xe9;ste)"/>
<node CREATED="1410812247090" ID="ID_1634669632" MODIFIED="1410812316123" TEXT="4. El Minetur  cooperar&#xe1; con el Ministerio del Interior y con los &#xf3;rganos responsables de las comunidades aut&#xf3;nomas con competencias sobre seguridad p&#xfa;blica, seguridad vial y de la protecci&#xf3;n civil."/>
<node CREATED="1410812247093" ID="ID_629172246" MODIFIED="1410812416347" TEXT="5. Los bienes muebles o inmuebles vinculados a la explotaci&#xf3;n de las redes y a la prestaci&#xf3;n de los servicios de telecomunicaciones dispondr&#xe1;n de las medidas y sistemas de seguridad, vigilancia, difusi&#xf3;n de informaci&#xf3;n, prevenci&#xf3;n de riesgos y protecci&#xf3;n que se determinen por el Gobierno, a propuesta de los Ministerios de Defensa, del Interior o Minetur."/>
<node CREATED="1410812247097" MODIFIED="1410812247097" TEXT="6. El Gobierno, con car&#xe1;cter excepcional y transitorio, podr&#xe1; acordar la asunci&#xf3;n por la AGE de la gesti&#xf3;n directa de determinados servicios o de la explotaci&#xf3;n de ciertas redes de comunicaciones electr&#xf3;nicas, de acuerdo con el texto refundido de la Ley de Contratos del Sector P&#xfa;blico."/>
</node>
</node>
<node CREATED="1410812456281" FOLDED="true" ID="ID_1333800487" MODIFIED="1410991545542" POSITION="right" TEXT="T&#xcd;TULO II: Explotaci&#xf3;n de redes y prestaci&#xf3;n de servicios de comunicaciones electr&#xf3;nicas en r&#xe9;gimen de libre competencia">
<node CREATED="1410812605209" FOLDED="true" ID="ID_1491406461" MODIFIED="1410990308245" TEXT="CAP&#xcd;TULO I: Disposiciones generales">
<node CREATED="1410812605210" FOLDED="true" ID="ID_1399690256" MODIFIED="1410815076062" TEXT="Art&#xed;culo 5. Principios aplicables.">
<node CREATED="1410812605210" MODIFIED="1410812605210" TEXT="1. R&#xe9;gimen de libre competencia"/>
<node CREATED="1410812605211" ID="ID_223529303" MODIFIED="1410812605211" TEXT="2. La adquisici&#xf3;n de los derechos de uso del dominio p&#xfa;blico radioel&#xe9;ctrico, de ocupaci&#xf3;n del dominio p&#xfa;blico o de la propiedad privada y de los recursos de numeraci&#xf3;n, direccionamiento y denominaci&#xf3;n necesarios para la explotaci&#xf3;n de redes y para la prestaci&#xf3;n de servicios de comunicaciones electr&#xf3;nicas deber&#xe1; realizarse conforme a lo dispuesto en esta Ley y en su normativa espec&#xed;fica."/>
<node CREATED="1410812605214" ID="ID_1917765483" MODIFIED="1410814984035" TEXT="3. Las medidas que se adopten en relaci&#xf3;n al acceso o al uso por parte de los usuarios finales respetar&#xe1;n el Convenio Europeo para la Protecci&#xf3;n de los Derechos Humanos y de las Libertades Fundamentales, en la Carta de Derechos Fundamentales de la Uni&#xf3;n Europea, en los principios generales del Derecho comunitario y en la Constituci&#xf3;n Espa&#xf1;ola."/>
</node>
<node CREATED="1410812605217" FOLDED="true" ID="ID_162760653" MODIFIED="1410815234006" TEXT="Art&#xed;culo 6. Requisitos exigibles">
<node CREATED="1410812605217" MODIFIED="1410812605217" TEXT="1. Personas f&#xed;sicas o jur&#xed;dicas nacionales de un Estado miembro de la UE o de otra nacionalidad si as&#xed; est&#xe1; previsto en los acuerdos internacionales que vinculen a Espa&#xf1;a. El Gobierno podr&#xe1; autorizar excepciones."/>
<node CREATED="1410812605220" MODIFIED="1410812605220" TEXT="2.Los interesados deber&#xe1;n, con anterioridad al inicio de la actividad, comunicarlo previamente al Registro de operadores en los t&#xe9;rminos que se determinen mediante RD.  (excepto r&#xe9;gimen de autoprestaci&#xf3;n)."/>
</node>
<node CREATED="1410812605221" FOLDED="true" ID="ID_309619280" MODIFIED="1410815236327" TEXT="Art&#xed;culo 7. Registro de operadores.">
<node CREATED="1410812605222" ID="ID_282068476" MODIFIED="1410812605222" TEXT="1. Dependiente del Minetur. Su regulaci&#xf3;n se har&#xe1; por RD. Se garantizar&#xe1; que el acceso a dicho Registro por medios electr&#xf3;nicos. En &#xe9;l deber&#xe1;n inscribirse los datos relativos a las personas f&#xed;sicas o jur&#xed;dicas que hayan notificado su intenci&#xf3;n de explotar redes o prestar servicios de comunicaciones electr&#xf3;nicas, las condiciones para desarrollar la actividad y sus modificaciones."/>
<node CREATED="1410812605224" MODIFIED="1410812605224" TEXT="2. Cuando el Registro de operadores constate que la notificaci&#xf3;n no re&#xfa;ne los requisitos dictar&#xe1; resoluci&#xf3;n motivada en un plazo m&#xe1;ximo de 15 d&#xed;as h&#xe1;biles, no teniendo por realizada aqu&#xe9;lla."/>
<node CREATED="1410812605225" ID="ID_515278120" MODIFIED="1410815119571" TEXT="3. Las AAPP comunicar&#xe1;n al Registro de operadores todo proyecto de instalaci&#xf3;n o explotaci&#xf3;n de redes de comunicaciones electr&#xf3;nicas en r&#xe9;gimen de autoprestaci&#xf3;n que haga uso del dominio p&#xfa;blico. "/>
<node CREATED="1410812605227" ID="ID_1501839430" MODIFIED="1410812605227" TEXT="4. Los seleccionados para la prestaci&#xf3;n de servicios de comunicaciones electr&#xf3;nicas armonizados en procedimientos de licitaci&#xf3;n convocados por las instituciones de la UE ser&#xe1;n inscritos de oficio."/>
<node CREATED="1410812605229" MODIFIED="1410812605229" TEXT="5. No ser&#xe1; preciso el consentimiento del interesado para el tratamiento de los datos de car&#xe1;cter personal que haya de contener el Registro ni para la comunicaci&#xf3;n de dichos datos que se derive de su publicidad."/>
</node>
<node CREATED="1410812605231" FOLDED="true" ID="ID_1298212098" MODIFIED="1410815459518" TEXT="Art&#xed;culo 8. Condiciones para la prestaci&#xf3;n de servicios o la explotaci&#xf3;n de redes de comunicaciones electr&#xf3;nicas.">
<node CREATED="1410812605232" ID="ID_1190537881" MODIFIED="1410815316587" TEXT="1. Las previstas en esta Ley y su normativa de desarrollo."/>
<node CREATED="1410812605234" ID="ID_1239644018" MODIFIED="1410815338420" TEXT="2. El Gobierno podr&#xe1; modificar las condiciones impuestas previa audiencia de los interesados, del Consejo de Consumidores y Usuarios y de las asociaciones m&#xe1;s representativas de los restantes usuarios, e informe de la CNMC. La modificaci&#xf3;n se realizar&#xe1; mediante RD"/>
<node CREATED="1410812605235" ID="ID_745527034" MODIFIED="1410815454427" TEXT="3. Las entidades que tengan derechos especiales o exclusivos para la prestaci&#xf3;n de servicios en otro sector econ&#xf3;mico y que exploten redes p&#xfa;blicas o presten servicios de comunicaciones electr&#xf3;nicas disponibles al p&#xfa;blico deber&#xe1;n llevar cuentas separadas y auditadas o establecer una separaci&#xf3;n estructural para las actividades asociadas con la explotaci&#xf3;n de redes o la prestaci&#xf3;n de servicios de comunicaciones electr&#xf3;nicas. Mediante RD podr&#xe1; establecerse la exenci&#xf3;n de esta obligaci&#xf3;n para las entidades cuyos ingresos brutos de explotaci&#xf3;n anuales por actividades asociadas con las redes o servicios de comunicaciones electr&#xf3;nicas sea inferior a 50 millones de euros."/>
</node>
<node CREATED="1410812605237" FOLDED="true" ID="ID_881871657" MODIFIED="1410816298870" TEXT="Art&#xed;culo 9. Instalaci&#xf3;n y explotaci&#xf3;n de redes p&#xfa;blicas y prestaci&#xf3;n de servicios de comunicaciones electr&#xf3;nicas en r&#xe9;gimen de prestaci&#xf3;n a terceros por las AAPP.">
<node CREATED="1410812605239" MODIFIED="1410812605239" TEXT="1. Se regir&#xe1; de manera espec&#xed;fica por lo dispuesto en el presente art&#xed;culo."/>
<node CREATED="1410812605240" ID="ID_502012818" MODIFIED="1410815543411" TEXT="2. Por operadores controlados directa o indirectamente por  AAPP se realizar&#xe1; dando cumplimiento al principio de inversor privado, con la debida separaci&#xf3;n de cuentas. Las condiciones se determinar&#xe1;n mediante RD, previo informe de la CNMC."/>
<node CREATED="1410812605241" ID="ID_388948215" MODIFIED="1410815576467" TEXT="3. Una AP s&#xf3;lo podr&#xe1; instalar y explotar redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas o prestar servicios de comunicaciones electr&#xf3;nicas en r&#xe9;gimen de prestaci&#xf3;n a terceros a trav&#xe9;s de entidades o sociedades que tengan entre su objeto social o finalidad la instalaci&#xf3;n y explotaci&#xf3;n de redes o la prestaci&#xf3;n de servicios de comunicaciones electr&#xf3;nicas. "/>
<node CREATED="1410812605243" FOLDED="true" ID="ID_1648394083" MODIFIED="1410816293734" TEXT="4. Los operadores controlados directa o indirectamente por AAPP deber&#xe1;n hacerlo en las condiciones establecidas en el art&#xed;culo 8 y, en particular:">
<node CREATED="1410812605245" ID="ID_556836686" MODIFIED="1410816261587" TEXT="a) Los operadores tienen reconocido directamente el derecho a acceder en condiciones neutrales, objetivas, transparentes, equitativas y no discriminatorias a las infraestructuras y recursos asociados utilizados por los operadores controlados directa o indirectamente por AAPP."/>
<node CREATED="1410812605247" ID="ID_611709743" MODIFIED="1410812605247" TEXT="b) Los operadores tienen reconocido directamente el derecho de uso compartido de las infraestructuras de red de comunicaciones electr&#xf3;nicas y sus recursos asociados instaladas por los operadores controlados directa o indirectamente por AAPP."/>
<node CREATED="1410812605248" ID="ID_324652573" MODIFIED="1410812605248" TEXT="c) Si las AAPP reguladoras o titulares del dominio p&#xfa;blico ostentan la propieda o ejercen el control directo o indirecto de operadores que explotan redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas o servicios de comunicaciones electr&#xf3;nicas disponibles para el p&#xfa;blico, deber&#xe1;n mantener una separaci&#xf3;n estructural entre dichos operadores y los &#xf3;rganos encargados de la regulaci&#xf3;n y gesti&#xf3;n de los derechos de utilizaci&#xf3;n del dominio p&#xfa;blico correspondiente."/>
</node>
</node>
<node CREATED="1410812605250" FOLDED="true" ID="ID_136151425" MODIFIED="1410816445711" TEXT="Art&#xed;culo 10. Obligaciones de suministro de informaci&#xf3;n.">
<node CREATED="1410812605251" FOLDED="true" ID="ID_1027389421" MODIFIED="1410816343366" TEXT="1. Las Autoridades Nacionales de Reglamentaci&#xf3;n de Telecomunicaciones (ANRT) podr&#xe1;n requerir a las personas f&#xed;sicas o jur&#xed;dicas que exploten redes o presten servicios de comunicaciones electr&#xf3;nicas, as&#xed; como a otros agentes que intervengan en este mercado, la informaci&#xf3;n necesaria para el cumplimiento de alguna de las siguientes finalidades. Excepto la referida a d) y o), no podr&#xe1; exigirse antes del inicio de la actividado. Las ANRT garantizar&#xe1;n la confidencialidad de la informaci&#xf3;n suministrada que pueda afectar a la seguridad e integridad de las redes y de los servicios de comunicaciones electr&#xf3;nicas o al secreto comercial o industrial.">
<node CREATED="1410812605253" MODIFIED="1410812605253" TEXT="a) estad&#xed;sticas o an&#xe1;lisis y para informes de seguimiento sectoriales."/>
<node CREATED="1410812605253" MODIFIED="1410812605253" TEXT="b) Comprobar el cumplimiento de las condiciones establecidas en particular cuando la explotaci&#xf3;n de las redes conlleve emisiones radioel&#xe9;ctricas."/>
<node CREATED="1410812605254" MODIFIED="1410812605254" TEXT="c) Comprobar que los operadores controlados por AAPP cumplen las condiciones."/>
<node CREATED="1410812605255" MODIFIED="1410812605255" TEXT="d) Evaluar la procedencia de las solicitudes de derechos de uso del dominio p&#xfa;blico radioel&#xe9;ctrico y de la numeraci&#xf3;n."/>
<node CREATED="1410812605256" MODIFIED="1410812605256" TEXT="e) Comprobar el uso efectivo y eficiente de frecuencias y n&#xfa;meros."/>
<node CREATED="1410812605256" MODIFIED="1410812605256" TEXT="f) Elaborar an&#xe1;lisis que permitan la definici&#xf3;n de los mercados de referencia, el establecimiento de condiciones espec&#xed;ficas a los operadores con poder significativo y conocer el modo en que la futura evoluci&#xf3;n de las redes o los servicios puede repercutir en los servicios mayoristas"/>
<node CREATED="1410812605258" MODIFIED="1410812605258" TEXT="g) Comprobar el cumplimiento de las obligaciones espec&#xed;ficas impuestas en el marco de la regulaci&#xf3;n ex ante y el cumplimiento de las resoluciones dictadas para resolver conflictos entre operadores."/>
<node CREATED="1410812605259" MODIFIED="1410812605259" TEXT="h) Comprobar el cumplimiento de las obligaciones de servicio p&#xfa;blico y obligaciones de car&#xe1;cter p&#xfa;blico; determinar los operadores encargados de prestar el servicio universal."/>
<node CREATED="1410812605260" MODIFIED="1410812605260" TEXT="i) Comprobar el cumplimiento de las obligaciones para garantizar un acceso equivalente para los usuarios finales con discapacidad."/>
<node CREATED="1410812605262" MODIFIED="1410812605262" TEXT="j) La puesta a disposici&#xf3;n de los ciudadanos de informaci&#xf3;n o aplicaciones que posibiliten realizar comparativas sobre precios, cobertura y calidad."/>
<node CREATED="1410812605263" MODIFIED="1410812605263" TEXT="k) La adopci&#xf3;n de medidas destinadas a facilitar la coubicaci&#xf3;n o el uso compartido de elementos de redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas y recursos asociados."/>
<node CREATED="1410812605264" MODIFIED="1410812605264" TEXT="l) Evaluar la integridad y la seguridad de las redes y servicios de comunicaciones electr&#xf3;nicas."/>
<node CREATED="1410812605265" MODIFIED="1410812605265" TEXT="m) Cumplir los requerimientos que vengan impuestos en el ordenamiento jur&#xed;dico."/>
<node CREATED="1410812605265" MODIFIED="1410812605265" TEXT="n) Comprobar el cumplimiento del resto de obligaciones nacidas de esta Ley."/>
<node CREATED="1410812605266" MODIFIED="1410812605266" TEXT="o) Planificar de manera eficiente el uso de fondos p&#xfa;blicos destinados al despliegue de infraestructuras de telecomunicaciones."/>
</node>
<node CREATED="1410812605267" ID="ID_1607617009" MODIFIED="1410816440492" TEXT="2. Las AAPP podr&#xe1;n solicitar la informaci&#xf3;n que sea necesaria en el ejercicio de sus competencias. Antes de hacerlo deber&#xe1;n recabarla de las Autoridades Nacionales de Reglamentaci&#xf3;n. "/>
<node CREATED="1410812605269" MODIFIED="1410812605269" TEXT="3. Las solicitudes de informaci&#xf3;n que se realicen de conformidad con los apartados anteriores habr&#xe1;n de ser motivadas y proporcionadas al fin."/>
</node>
<node CREATED="1410812605270" ID="ID_323226285" MODIFIED="1410990306579" TEXT="Art&#xed;culo 11. Normas t&#xe9;cnicas.">
<node CREATED="1410812605270" ID="ID_1503848031" MODIFIED="1410816546220" TEXT="1. El Minetur fomentar&#xe1; el uso de las normas o especificaciones t&#xe9;cnicas que la Comisi&#xf3;n Europea elabore. En su ausencia promover&#xe1; la aplicaci&#xf3;n de las normas de la Uni&#xf3;n Internacional de Telecomunicaciones (UIT), la Conferencia Europea de Administraciones de Correos y Telecomunicaciones (CEPT), la Comisi&#xf3;n Internacional de Normalizaci&#xf3;n (ISO) y la Comisi&#xf3;n Electrot&#xe9;cnica Internacional (CEI). Mediante RD se podr&#xe1;n determinar las formas de elaboraci&#xf3;n y, en su caso, de adopci&#xf3;n de las especificaciones t&#xe9;cnicas aplicables. Mediante RD se establecer&#xe1; el procedimiento de comunicaci&#xf3;n de las citadas especificaciones a la CE."/>
<node CREATED="1410812605273" ID="ID_1568916491" MODIFIED="1410816589955" TEXT="2. La CNMC fomentar&#xe1; y garantizar&#xe1; el uso de las normas o especificaciones t&#xe9;cnicas en la regulaci&#xf3;n ex ante y resoluci&#xf3;n de conflictos entre operadores."/>
</node>
</node>
<node CREATED="1410812639297" FOLDED="true" ID="ID_710595167" MODIFIED="1410990315965" TEXT="CAP&#xcd;TULO II: Acceso a las redes y recursos asociados e interconexi&#xf3;n">
<node CREATED="1410812639298" FOLDED="true" ID="ID_1635668498" MODIFIED="1410990313852" TEXT="Art&#xed;culo 12. Principios generales aplicables">
<node CREATED="1410812639299" ID="ID_559002479" MODIFIED="1410816614211" TEXT="1. Este cap&#xed;tulo ser&#xe1; aplicable salvo que el beneficiario del acceso sea un usuario fina."/>
<node CREATED="1410812639301" ID="ID_1648547485" MODIFIED="1410816633012" TEXT="2. Los operadores de redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas tendr&#xe1;n el derecho y, cuando se solicite por otros operadores, la obligaci&#xf3;n de negociar la interconexi&#xf3;n mutua."/>
<node CREATED="1410812639303" MODIFIED="1410812639303" TEXT="3. No existir&#xe1;n restricciones que impidan que los operadores negocien entre s&#xed; acuerdos de acceso e interconexi&#xf3;n."/>
<node CREATED="1410812639305" MODIFIED="1410812639305" TEXT="4. La persona f&#xed;sica o jur&#xed;dica que explote redes o preste servicios en otro Estado miembro de la UE que solicite acceso o interconexi&#xf3;n en Espa&#xf1;a no necesitar&#xe1; llevar a cabo la notificaci&#xf3;n  del art&#xed;culo 6 cuando no explote redes ni preste servicios de comunicaciones electr&#xf3;nicas en el territorio nacional."/>
<node CREATED="1410812639307" ID="ID_1151055540" MODIFIED="1410816678555" TEXT="5. La CNMC podr&#xe1; intervenir en las relaciones entre operadores o entre operadores y otras entidades que se beneficien de las obligaciones de acceso e interconexi&#xf3;n, a petici&#xf3;n de cualquiera de las partes, o de oficio. La decisi&#xf3;n de la CNMC ser&#xe1; vinculante y se adoptar&#xe1; en el plazo indicado en la Ley 3/2013."/>
<node CREATED="1410812639310" MODIFIED="1410812639310" TEXT="6. Las obligaciones y condiciones que se impongan de conformidad con este cap&#xed;tulo ser&#xe1;n objetivas, transparentes, proporcionadas y no discriminatorias."/>
<node CREATED="1410812639312" ID="ID_1605053792" MODIFIED="1410816720468" TEXT="7. Los operadores que obtengan informaci&#xf3;n de otros respetar&#xe1;n en todo momento la confidencialidad"/>
</node>
</node>
<node CREATED="1410812673300" FOLDED="true" ID="ID_236623030" MODIFIED="1410990410750" TEXT="CAP&#xcd;TULO III: Regulaci&#xf3;n ex ante de los mercados y resoluci&#xf3;n de conflictos">
<node CREATED="1410812673301" FOLDED="true" ID="ID_806409691" MODIFIED="1410990392374" TEXT="Art&#xed;culo 13. Mercados de referencia y operadores con poder significativo en el mercado.">
<node CREATED="1410812673302" MODIFIED="1410812673302" TEXT="1. La CNMC , teniendo en cuenta la Recomendaci&#xf3;n de la CE sobre mercados relevantes, las Directrices de la CE para el an&#xe1;lisis de mercados y determinaci&#xf3;n de operadores con poder significativo en el mercado y los dict&#xe1;menes y posiciones comunes pertinentes adoptados por el Organismo de Reguladores Europeos de las Comunicaciones Electr&#xf3;nicas (ORECE), definir&#xe1;, previo informe del MINETUR  y del MEC (Ministerio de Econom&#xed;a y Competitividad) y mediante resoluci&#xf3;n publicada en el BOE, los mercados de referencia. En todo caso, la CNMC, en aplicaci&#xf3;n de la normativa en materia de competencia, en especial, de la Ley 15/2007, de Defensa de la Competencia, de los art&#xed;culos 101 y 102 del Tratado de Funcionamiento de la Uni&#xf3;n Europea, y de la Ley 3/2013, de creaci&#xf3;n de la Comisi&#xf3;n, deber&#xe1; supervisar el funcionamiento de los distintos mercados de comunicaciones electr&#xf3;nicas."/>
<node CREATED="1410812673305" FOLDED="true" ID="ID_1541561051" MODIFIED="1410990390662" TEXT="2. La CNMC  llevar&#xe1; a cabo un an&#xe1;lisis de los citados mercados en los siguientes plazos. Si la CNMC no hubiera concluido su an&#xe1;lisis de un mercado relevante que figura en la Recomendaci&#xf3;n de Mercados  Relevantes dentro de los plazos establecidos, el ORECE le prestar&#xe1; asistencia, a petici&#xf3;n de la propia Comisi&#xf3;n. La CNMC notificar&#xe1; el proyecto de medida a la Comisi&#xf3;n Europea en 6 meses. El MINETUR  podr&#xe1; solicitar a la CNMC para que realice el an&#xe1;lisis de un mercado determinado por razones de inter&#xe9;s general, o por indicios de falta de competencia efectiva. La CNMC, en los planes anuales o plurianuales de actuaci&#xf3;n deber&#xe1; identificar los mercados relevantes que vaya a analizar. ">
<node CREATED="1410812673309" ID="ID_314449369" MODIFIED="1410990372997" TEXT="a) En un plazo m&#xe1;ximo de 3 a&#xf1;os contado desde la adopci&#xf3;n de una medida anterior. Excepcionalmente, podr&#xe1; ampliarse a un m&#xe1;ximo de 3 a&#xf1;os suplementarios cuando las ANRT hayan notificado una propuesta de ampliaci&#xf3;n a la CE y esta no haya hecho ninguna objeci&#xf3;n en el plazo de un mes respecto de la ampliaci&#xf3;n notificada.">
<icon BUILTIN="idea"/>
</node>
<node CREATED="1410812673312" ID="ID_1962910411" MODIFIED="1410990368224" TEXT="b) En el plazo m&#xe1;ximo de dos a&#xf1;os desde la adopci&#xf3;n de una recomendaci&#xf3;n sobre mercados relevantes revisada, para los mercados no notificados previamente a la Comisi&#xf3;n Europea.">
<icon BUILTIN="idea"/>
</node>
</node>
<node CREATED="1410812673314" ID="ID_1257439181" MODIFIED="1410816965540" TEXT="3. El an&#xe1;lisis tendr&#xe1; como finalidad determinar si los distintos mercados de referencia se desarrollan en un entorno de competencia efectiva. En caso contrario, la CNMC  previo informe del MINETUR y del MEC, identificar&#xe1; y har&#xe1; p&#xfa;blicos el operador u operadores que poseen un poder significativo en cada mercado considerado. Cuando un operador u operadores tengan poder significativo en un mercado de referencia (mercado primario), la CNMC podr&#xe1; declarar que lo tienen tambi&#xe9;n en otro mercado de referencia estrechamente relacionado con el anterior (mercado secundario). En este supuesto, podr&#xe1;n imponerse obligaciones espec&#xed;ficas adecuadas en el mercado secundario."/>
<node CREATED="1410812673316" ID="ID_236570821" MODIFIED="1410816996899" TEXT="4. En aquellos mercados en que se constate la inexistencia de un entorno de competencia efectiva, la CNMC, previo informe del MINETUR y del MEC, impondr&#xe1; las obligaciones espec&#xed;ficas apropiadas a los operadores con poder significativo."/>
<node CREATED="1410812673318" MODIFIED="1410812673318" TEXT="5. En los mercados en los que se constate la existencia de competencia efectiva, la CNMC suprimir&#xe1; las obligaciones espec&#xed;ficas que tuvieran impuestas los operadores por haber sido declarados con poder significativo en dichos mercados."/>
</node>
<node CREATED="1410812673319" FOLDED="true" ID="ID_395015821" MODIFIED="1410817370343" TEXT="Art&#xed;culo 14. Obligaciones espec&#xed;ficas aplicables a los operadores con poder significativo en mercados de referencia.">
<node CREATED="1410812673320" MODIFIED="1410812673320" TEXT="1. La CNMC podr&#xe1; imponer a los operadores que hayan sido declarados con poder significativo en el mercado obligaciones espec&#xed;ficas en materia de:">
<node CREATED="1410812673322" ID="ID_1106921729" MODIFIED="1410817129435" TEXT="a) Transparencia. En particular, cuando se impongan a un operador obligaciones de no discriminaci&#xf3;n, se le podr&#xe1; exigir que publique una oferta de referencia. Mediante RD se establecer&#xe1; el contenido m&#xed;nimo de dicha oferta."/>
<node CREATED="1410812673324" ID="ID_1396142217" MODIFIED="1410817094099" TEXT="b) No discriminaci&#xf3;n"/>
<node CREATED="1410812673326" MODIFIED="1410812673326" TEXT="c) Separaci&#xf3;n de cuentas"/>
<node CREATED="1410812673326" MODIFIED="1410812673326" TEXT="d) Acceso a elementos o a recursos espec&#xed;ficos de las redes y a su utilizaci&#xf3;n, as&#xed; como a recursos y a servicios asociados tales como servicios de identidad, localizaci&#xf3;n y presencia."/>
<node CREATED="1410812673327" ID="ID_1859639654" MODIFIED="1410817079235" TEXT="e) Control de precios"/>
</node>
<node CREATED="1410812673329" ID="ID_807681161" MODIFIED="1410812673329" TEXT="2. En circunstancias excepcionales, la CNMC, previo sometimiento al mecanismo de consulta previsto en la disposici&#xf3;n adicional octava, podr&#xe1; imponer obligaciones espec&#xed;ficas relativas al acceso o a la interconexi&#xf3;n que no se limiten a las materias enumeradas en el apartado anterior."/>
<node CREATED="1410812673330" ID="ID_693211281" MODIFIED="1410812673330" TEXT="3. Cuando la CNMC  estudie la conveniencia de imponer las obligaciones espec&#xed;ficas de acceso previstas en la letra d) del apartado 1 habr&#xe1; de considerar los siguientes elementos:">
<node CREATED="1410812673331" MODIFIED="1410812673331" TEXT="a) la viabilidad t&#xe9;cnica y econ&#xf3;mica de utilizar o instalar recursos que compitan entre s&#xed;."/>
<node CREATED="1410812673332" MODIFIED="1410812673332" TEXT="b) la posibilidad de proporcionar el acceso propuesto, en relaci&#xf3;n con la capacidad disponible,"/>
<node CREATED="1410812673332" MODIFIED="1410812673332" TEXT="c) la inversi&#xf3;n inicial del propietario de los recursos, las inversiones p&#xfa;blicas realizadas y los riesgos inherentes a las inversiones,"/>
<node CREATED="1410812673334" MODIFIED="1410812673334" TEXT="d) la necesidad de salvaguardar la competencia a largo plazo"/>
<node CREATED="1410812673334" MODIFIED="1410812673334" TEXT="e) los derechos de propiedad intelectual,"/>
<node CREATED="1410812673335" MODIFIED="1410812673335" TEXT="f) el suministro de servicios paneuropeos."/>
</node>
<node CREATED="1410812673335" MODIFIED="1410812673335" TEXT="4. Cuando la CNMC  imponga obligaciones espec&#xed;ficas a un operador para que facilite acceso podr&#xe1; establecer determinadas condiciones t&#xe9;cnicas u operativas al citado operador o a los beneficiarios de dicho acceso, conforme se establezca mediante RD. Las obligaciones estar&#xe1;n de acuerdo con las normas a que se refiere el art&#xed;culo 11."/>
<node CREATED="1410812673337" ID="ID_520293031" MODIFIED="1410817363635" TEXT="5. Mediante RD  el Gobierno identificar&#xe1; las obligaciones espec&#xed;ficas que la CNMC podr&#xe1; imponer en los mercados de referencia y determinar&#xe1; las condiciones para su imposici&#xf3;n, modificaci&#xf3;n o supresi&#xf3;n."/>
</node>
<node CREATED="1410812673339" FOLDED="true" ID="ID_1069144974" MODIFIED="1410990408997" TEXT="Art&#xed;culo 15. Resoluci&#xf3;n de conflictos.">
<node CREATED="1410812673340" ID="ID_1866029439" MODIFIED="1410817419756" TEXT="1. La CNMC resolver&#xe1; los conflictos de acceso e interconexi&#xf3;n. La CNMC, previa audiencia de las partes, dictar&#xe1; resoluci&#xf3;n vinculante, en el plazo indicado en la Ley de creaci&#xf3;n de esta Comisi&#xf3;n, sin perjuicio de que puedan adoptarse medidas provisionales."/>
<node CREATED="1410812673343" ID="ID_523907031" MODIFIED="1410817457604" TEXT="2. En caso de producirse un conflicto transfronterizo en el que una de las partes est&#xe9; radicada en otro Estado miembro de la UE, la CNMC, en caso de que cualquiera de las partes as&#xed; lo solicite, coordinar&#xe1;, en los t&#xe9;rminos que se establezcan mediante RD, con la otra u otras autoridades nacionales de reglamentaci&#xf3;n afectadas. La CNMC podr&#xe1; solicitar que el ORECE adopte un dictamen sobre las medidas para resolver el litigio. Cuando se haya transmitido al ORECE tal solicitud, la CNMC deber&#xe1; esperar el dictamen del ORECE antes de tomar medidas para resolver el litigio. Ello no constituir&#xe1; un obst&#xe1;culo para que la CNMC adopte medidas urgentes. "/>
</node>
</node>
<node CREATED="1410812980797" FOLDED="true" ID="ID_50663449" MODIFIED="1410990983318" TEXT="CAP&#xcd;TULO IV: Separaci&#xf3;n funcional">
<node CREATED="1410812980797" FOLDED="true" ID="ID_600008680" MODIFIED="1410990963613" TEXT="Art&#xed;culo 16. Separaci&#xf3;n funcional obligatoria.">
<node CREATED="1410812980797" ID="ID_844787711" MODIFIED="1410990461702" TEXT="1. Cuando la CNMC llegue a la conclusi&#xf3;n de que las obligaciones espec&#xed;ficas impuestas no han bastado para conseguir una competencia efectiva podr&#xe1; decidir la imposici&#xf3;n, como medida excepcional, a los operadores con poder significativo en el mercado integrados verticalmente, de la obligaci&#xf3;n de traspasar las actividades relacionadas con el suministro al por mayor de productos de acceso a una unidad empresarial que act&#xfa;e independientemente."/>
<node CREATED="1410812980797" FOLDED="true" ID="ID_16189058" MODIFIED="1410990541824" TEXT="2. Cuando la CNMC se proponga imponer una obligaci&#xf3;n de separaci&#xf3;n funcional, elaborar&#xe1; una propuesta que incluya:">
<node CREATED="1410812980798" ID="ID_711344128" MODIFIED="1410990488386" TEXT="a) motivos"/>
<node CREATED="1410812980798" ID="ID_1914232670" MODIFIED="1410990510811" TEXT="b) razones por las que hay pocas posibilidade de competencia basada en la infraestructura "/>
<node CREATED="1410812980798" ID="ID_27847010" MODIFIED="1410812980798" TEXT="c) un an&#xe1;lisis del impacto previsto sobre la autoridad reguladora, sobre la empresa, particularmente los trabajadores, y al sector de las comunicaciones electr&#xf3;nicas en su conjunto, sobre los incentivos para invertir en el sector en su conjunto, en especial por lo que respecta a la necesidad de garantizar la cohesi&#xf3;n social y territorial, as&#xed; como sobre otras partes interesadas, incluido el impacto previsto sobre la competencia en infraestructuras y cualquier efecto negativo potencial sobre los consumidores,"/>
<node CREATED="1410812980798" MODIFIED="1410812980798" TEXT="d) un an&#xe1;lisis de las razones que justifiquen que esta obligaci&#xf3;n es el medio m&#xe1;s adecuado."/>
</node>
<node CREATED="1410812980798" FOLDED="true" ID="ID_1561944048" MODIFIED="1410990719957" TEXT="3. El proyecto de medida incluir&#xe1; los elementos siguientes:">
<node CREATED="1410812980798" ID="ID_275243226" MODIFIED="1410990564811" TEXT="a) la naturaleza y el grado precisos de la separaci&#xf3;n, especificando el estatuto jur&#xed;dico de la entidad empresarial separada,"/>
<node CREATED="1410812980799" ID="ID_1523522392" MODIFIED="1410990592410" TEXT="b) activos de la entidad empresarial separada y de los productos o servicios que debe suministrar,"/>
<node CREATED="1410812980799" ID="ID_74153145" MODIFIED="1410990613235" TEXT="c) los mecanismos de gobernanza para garantizar la independencia del personal empleado por la entidad empresarial separada,"/>
<node CREATED="1410812980799" MODIFIED="1410812980799" TEXT="d) las normas para garantizar el cumplimiento de las obligaciones,"/>
<node CREATED="1410812980799" MODIFIED="1410812980799" TEXT="e) las normas para garantizar la transparencia de los procedimientos operativos,"/>
<node CREATED="1410812980799" ID="ID_418123093" MODIFIED="1410990629187" TEXT="f) un programa de seguimiento, incluida la publicaci&#xf3;n de un informe anual."/>
</node>
<node CREATED="1410812980799" ID="ID_1624690822" MODIFIED="1410990674402" TEXT="4. La propuesta de imposici&#xf3;n de la obligaci&#xf3;n de separaci&#xf3;n funcional, una vez que el MINETUR y el MEC hayan emitido informe, se presentar&#xe1; a la Comisi&#xf3;n Europea."/>
<node CREATED="1410812980799" ID="ID_1720876237" MODIFIED="1410990715538" TEXT="5. Tras la decisi&#xf3;n de la CE, la CNMC  llevar&#xe1; a cabo un an&#xe1;lisis coordinado de los distintos mercados relacionados con la red de acceso. Sobre la base de su evaluaci&#xf3;n, previo informe del MINETUR y del MEC, la CNMC impondr&#xe1;, mantendr&#xe1;, modificar&#xe1; o suprimir&#xe1; las obligaciones espec&#xed;ficas correspondientes."/>
</node>
<node CREATED="1410812980800" FOLDED="true" ID="ID_738173810" MODIFIED="1410990960197" TEXT="Art&#xed;culo 17. Separaci&#xf3;n funcional voluntaria.">
<node CREATED="1410812980800" ID="ID_441560128" MODIFIED="1410990886298" TEXT="1. Si una empresa designada como poseedora de poder significativo se propone  transferir sus activos de red de acceso local a una persona jur&#xed;dica separada deber&#xe1; informar con anterioridad al MINETUR, al MEC y a la CNMC. "/>
<node CREATED="1410812980800" ID="ID_1233166007" MODIFIED="1410990957602" TEXT="2. Si se realiza la separaci&#xf3;n funcional voluntaria, la CNMC evaluar&#xe1; el efecto de la transacci&#xf3;n prevista sobre las obligaciones reglamentarias impuestas a esa entidad, llevando a cabo un an&#xe1;lisis coordinado de los distintos mercados relacionados con la red de acceso. Sobre la base de su evaluaci&#xf3;n, previo informe del MINETUR, la CNMC mantendr&#xe1;, modificar&#xe1; o suprimir&#xe1; las obligaciones espec&#xed;ficas correspondientes."/>
</node>
<node CREATED="1410812980801" FOLDED="true" ID="ID_1755615316" MODIFIED="1410990980213" TEXT="Art&#xed;culo 18. Obligaciones espec&#xed;ficas adicionales a la separaci&#xf3;n funcional.">
<node CREATED="1410812980801" MODIFIED="1410812980801" TEXT="Las empresas podr&#xe1;n estar sujetas a cualquiera de las obligaciones espec&#xed;ficas enumeradas en el art&#xed;culo 14 en cualquier mercado de referencia en que hayan sido designadas como poseedoras de poder significativo en el mercado."/>
</node>
</node>
<node CREATED="1410813024635" FOLDED="true" ID="ID_238866536" MODIFIED="1410991543894" TEXT="CAP&#xcd;TULO V: Numeraci&#xf3;n, direccionamiento y denominaci&#xf3;n">
<node CREATED="1410813024636" FOLDED="true" ID="ID_554799443" MODIFIED="1410991355222" TEXT="Art&#xed;culo 19. Principios generales.">
<node CREATED="1410813024637" MODIFIED="1410813024637" TEXT="1. Para los servicios de comunicaciones electr&#xf3;nicas disponibles al p&#xfa;blico se proporcionar&#xe1;n los n&#xfa;meros, direcciones y nombres precisos para su efectiva prestaci&#xf3;n."/>
<node CREATED="1410813024640" MODIFIED="1410813024640" TEXT="2. La regulaci&#xf3;n de los nombres de dominio de internet (&#xab;.es&#xbb;) se regir&#xe1; por su normativa espec&#xed;fica."/>
<node CREATED="1410813024642" ID="ID_1551656773" MODIFIED="1410991209725" TEXT="3. Corresponde al Gobierno la aprobaci&#xf3;n por RD de los planes nacionales de numeraci&#xf3;n, direccionamiento y denominaci&#xf3;n.">
<icon BUILTIN="idea"/>
</node>
<node CREATED="1410813024644" MODIFIED="1410813024644" TEXT="4. Corresponde al MINETUR la elaboraci&#xf3;n de las propuestas de planes nacionales para su elevaci&#xf3;n al Gobierno, y el desarrollo normativo de estos planes"/>
<node CREATED="1410813024645" ID="ID_1389590387" MODIFIED="1410813024645" TEXT="5. Corresponde al MINETUR el otorgamiento de los derechos de uso de los recursos p&#xfa;blicos regulados en los planes nacionales de numeraci&#xf3;n, direccionamiento y denominaci&#xf3;n. Los procedimientos para el otorgamiento de estos derechos ser&#xe1;n abiertos, objetivos, no discriminatorios, proporcionados y transparentes y se establecer&#xe1;n mediante RD. Las decisiones relativas a los otorgamientos de derechos de uso se adoptar&#xe1;n, comunicar&#xe1;n y har&#xe1;n p&#xfa;blicas en el plazo m&#xe1;ximo de tres semanas desde la recepci&#xf3;n de la solicitud completa, salvo cuando se apliquen procedimientos de selecci&#xf3;n comparativa o competitiva, en cuyo caso, el plazo m&#xe1;ximo ser&#xe1; de seis semanas. Transcurrido el plazo m&#xe1;ximo sin haberse notificado la resoluci&#xf3;n expresa, se podr&#xe1; entender desestimada la solicitud. Tambi&#xe9;n se har&#xe1;n p&#xfa;blicas las decisiones relativas a la cancelaci&#xf3;n de derechos de uso."/>
<node CREATED="1410813024648" ID="ID_1745726239" MODIFIED="1410813024648" TEXT="6. Los operadores que presten servicios telef&#xf3;nicos disponibles al p&#xfa;blico deber&#xe1;n cursar las llamadas que se efect&#xfa;en a los rangos de numeraci&#xf3;n telef&#xf3;nica nacional y llamadas internacionales al espacio europeo de numeraci&#xf3;n telef&#xf3;nica y a otros rangos de numeraci&#xf3;n internacional, en los t&#xe9;rminos que se especifiquen en los planes nacionales de numeraci&#xf3;n o en sus disposiciones de desarrollo. Adoptar&#xe1;n las medidas oportunas para que sean cursadas cuantas llamadas se efect&#xfa;en procedentes de y con destino al espacio europeo de numeraci&#xf3;n telef&#xf3;nica, a tarifas similares a las que se aplican a las llamadas con origen o destino en otros pa&#xed;ses comunitarios."/>
<node CREATED="1410813024650" MODIFIED="1410813024650" TEXT="7. El otorgamiento de derechos de uso regulados en los planes nacionales no supondr&#xe1; el otorgamiento de m&#xe1;s derechos que los de su utilizaci&#xf3;n conforme a esta Ley."/>
<node CREATED="1410813024651" MODIFIED="1410813024651" TEXT="8. Los operadores a los que se haya otorgado el derecho de uso de una serie de n&#xfa;meros no podr&#xe1;n discriminar a otros operadores en lo que se refiere a las secuencias de n&#xfa;meros utilizadas para dar acceso a los servicios de &#xe9;stos."/>
<node CREATED="1410813024652" MODIFIED="1410813024652" TEXT="9. Todos los operadores y, en su caso, los fabricantes y los comerciantes deber&#xe1;n tomar las medidas necesarias para el cumplimiento de las decisiones que se adopten por el MINETUR en materia de numeraci&#xf3;n, direccionamiento y denominaci&#xf3;n."/>
<node CREATED="1410813024653" MODIFIED="1410813024653" TEXT="10. Los usuarios finales tendr&#xe1;n acceso a los recursos p&#xfa;blicos regulados en los planes nacionales. Esta normativa podr&#xe1; prever, cuando est&#xe9; justificado, el otorgamiento de derechos de uso de n&#xfa;meros, nombres o direcciones a los usuarios finales para determinados rangos que a tal efecto se definan en los planes nacionales o en sus disposiciones de desarrollo."/>
<node CREATED="1410813024655" MODIFIED="1410813024655" TEXT="11. Los operadores adoptar&#xe1;n las medidas para que los usuarios finales puedan tener acceso a los servicios utilizando n&#xfa;meros no geogr&#xe1;ficos en la Uni&#xf3;n Europea, y que puedan tener acceso a todos los n&#xfa;meros proporcionados en la UE, incluidos los de los planes nacionales de numeraci&#xf3;n de los Estados miembros, los del espacio europeo de numeraci&#xf3;n telef&#xf3;nica, y los N&#xfa;meros Universales Internacionales de Llamada Gratuita."/>
<node CREATED="1410813024656" MODIFIED="1410813024656" TEXT="12. El Gobierno apoyar&#xe1; la armonizaci&#xf3;n de determinados n&#xfa;meros o series de n&#xfa;meros concretos dentro de la Uni&#xf3;n Europea cuando ello promueva al mismo tiempo el funcionamiento del mercado interior y el desarrollo de servicios paneuropeos."/>
</node>
<node CREATED="1410813024658" FOLDED="true" ID="ID_957516376" MODIFIED="1410991356365" TEXT="Art&#xed;culo 20. Planes nacionales.">
<node CREATED="1410813024658" MODIFIED="1410813024658" TEXT="1. Los planes nacionales designar&#xe1;n los servicios para los que puedan utilizarse los n&#xfa;meros, direcciones y nombres correspondientes. Los planes nacionales podr&#xe1;n incluir los principios de fijaci&#xf3;n de precios y los precios m&#xe1;ximos para garantizar la protecci&#xf3;n de los consumidores."/>
<node CREATED="1410813024660" MODIFIED="1410813024660" TEXT="2. El contenido de los citados planes y su gesti&#xf3;n ser&#xe1;n p&#xfa;blicos, salvo en lo relativo a la seguridad nacional."/>
<node CREATED="1410813024661" MODIFIED="1410813024661" TEXT="3. A fin de cumplir con las obligaciones y recomendaciones internacionales o para garantizar la disponibilidad suficiente de n&#xfa;meros, direcciones y nombres, el MINETUR podr&#xe1;, mediante orden y previo informe preceptivo de la CNMC, modificar la estructura y la organizaci&#xf3;n de los planes nacionales o planes espec&#xed;ficos para cada servicio, establecer medidas sobre la utilizaci&#xf3;n de los recursos num&#xe9;ricos y alfanum&#xe9;ricos necesarios para la prestaci&#xf3;n de los servicios."/>
<node CREATED="1410813024662" MODIFIED="1410813024662" TEXT="4. Los planes nacionales podr&#xe1;n establecer procedimientos de selecci&#xf3;n competitiva o comparativa para el otorgamiento de derechos de uso de n&#xfa;meros y nombres con valor econ&#xf3;mico excepcional o que sean particularmente apropiados para la prestaci&#xf3;n de determinados servicios de inter&#xe9;s general. Estos procedimientos respetar&#xe1;n los principios de publicidad, concurrencia y no discriminaci&#xf3;n para todas las partes interesadas."/>
</node>
<node CREATED="1410813024664" FOLDED="true" ID="ID_1249775917" MODIFIED="1410991540069" TEXT="Art&#xed;culo 21. Conservaci&#xf3;n de los n&#xfa;m telef&#xf3;nicos por los abonados.">
<node CREATED="1410813024664" MODIFIED="1410813024664" TEXT="1. Los operadores garantizar&#xe1;n, de conformidad con lo establecido en el art&#xed;culo 47, que los abonados con n&#xfa;meros del plan nacional de numeraci&#xf3;n telef&#xf3;nica puedan conservar, previa solicitud, los n&#xfa;meros que les hayan sido asignados, con independencia del operador. Mediante RD se fijar&#xe1;n los supuestos a los que sea de aplicaci&#xf3;n la conservaci&#xf3;n de n&#xfa;meros, as&#xed; como los aspectos t&#xe9;cnicos y administrativos necesarios. En aplicaci&#xf3;n de este RD y su normativa de desarrollo, la CNMC podr&#xe1; fijar, mediante circular, caracter&#xed;sticas y condiciones para la conservaci&#xf3;n de los n&#xfa;meros."/>
<node CREATED="1410813024666" ID="ID_1612621064" MODIFIED="1410991414210" TEXT="2. Los costes derivados de la actualizaci&#xf3;n de los elementos de la red y de los sistemas necesarios para hacer posible la conservaci&#xf3;n de los n&#xfa;meros deber&#xe1;n ser sufragados por cada operador. Los dem&#xe1;s costes que produzca la conservaci&#xf3;n de los n&#xfa;meros telef&#xf3;nicos se repartir&#xe1;n entre los operadores afectados por el cambio. A falta de acuerdo, resolver&#xe1; la CNMC."/>
</node>
<node CREATED="1410813024668" FOLDED="true" ID="ID_1499194166" MODIFIED="1410991539094" TEXT="Art&#xed;culo 22. N&#xfa;meros armonizados para los servicios armonizados europeos de valor social.">
<node CREATED="1410813024668" ID="ID_1242874248" MODIFIED="1410991444818" TEXT="1. El MINETUR promover&#xe1; el conocimiento de los n&#xfa;meros armonizados europeos que comienzan por 116."/>
<node CREATED="1410813024670" ID="ID_1686440912" MODIFIED="1410991484842" TEXT="2. El Minetur adoptar&#xe1; las iniciativas pertinentes para que los usuarios finales con discapacidad puedan tener el mejor acceso posible a estos servicios."/>
<node CREATED="1410813024671" ID="ID_158307290" MODIFIED="1410991535442" TEXT="3. Las AAPP  competentes en la regulaci&#xf3;n o supervisi&#xf3;n de cada uno de estos servicios velar&#xe1;n por que los ciudadanos reciban una informaci&#xf3;n adecuada."/>
</node>
</node>
</node>
<node CREATED="1410617968442" FOLDED="true" ID="ID_1078315885" MODIFIED="1410807751750" POSITION="left" TEXT="Disposiciones adicionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1410685816721" FOLDED="true" ID="ID_1588306459" MODIFIED="1410685832774" TEXT="1&#xaa; Significado de los t&#xe9;rminos">
<node CREATED="1410685827135" ID="ID_1504001238" MODIFIED="1410685830766" TEXT="Anexo II"/>
</node>
<node CREATED="1410618044104" FOLDED="true" ID="ID_26724373" MODIFIED="1410685839995" TEXT="2&#xaa; Limitaciones a la propiedad y servidumbre">
<node CREATED="1410618194824" ID="ID_815050873" MODIFIED="1410618273043" TEXT=" Altura m&#xe1;xima de los edificios"/>
<node CREATED="1410618212618" ID="ID_1126964691" MODIFIED="1410618278294" TEXT=" Distancia m&#xed;nima ">
<node CREATED="1410618231051" ID="ID_1730605500" MODIFIED="1410618565277" TEXT="Industrias e instalaciones el&#xe9;ctricas alta tensi&#xf3;n">
<icon BUILTIN="idea"/>
</node>
<node CREATED="1410618252029" ID="ID_1651525131" MODIFIED="1410618266142" TEXT="L&#xed;neas f&#xe9;rreas electrificadas"/>
<node CREATED="1410618280882" ID="ID_793955390" MODIFIED="1410618299015" TEXT="Transmisores radioel&#xe9;ctricos"/>
</node>
<node CREATED="1410618323766" ID="ID_1870370393" MODIFIED="1410618335290" TEXT="Condiciones m&#xe1;s gravosas">
<node CREATED="1410618335291" ID="ID_1127481884" MODIFIED="1410618633822" TEXT="Distancias inferiores a 1000 m">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1410618352532" ID="ID_1932852603" MODIFIED="1410618380374" TEXT="Angulo">
<node CREATED="1410618380374" ID="ID_1583193145" MODIFIED="1410618537851" TEXT="Sobre la horizontal">
<icon BUILTIN="down"/>
</node>
<node CREATED="1410618389161" ID="ID_1874025370" MODIFIED="1410618541168" TEXT="Desde la parte superior  de una estaci&#xf3;n">
<icon BUILTIN="down"/>
</node>
<node CREATED="1410618418031" ID="ID_1542901624" MODIFIED="1410618544084" TEXT="El punto m&#xe1;s elevado del edificio">
<icon BUILTIN="down"/>
</node>
<node CREATED="1410618428731" ID="ID_1674287190" MODIFIED="1410618647690" STYLE="bubble" TEXT="inferior a 3&#xba;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1410618451560" ID="ID_871225152" MODIFIED="1410618637022" TEXT="M&#xe1;xima separaci&#xf3;n exigible entre">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1410618506932" ID="ID_1072407080" MODIFIED="1410618547017" TEXT="Industria">
<icon BUILTIN="down"/>
</node>
<node CREATED="1410618511262" ID="ID_418823481" MODIFIED="1410618560562" TEXT="L&#xed;nea de alta tensi&#xf3;n ">
<icon BUILTIN="down"/>
<icon BUILTIN="idea"/>
</node>
<node CREATED="1410618522927" ID="ID_765145502" MODIFIED="1410618552154" TEXT="Ferrocarril">
<icon BUILTIN="down"/>
</node>
<node CREATED="1410618592892" ID="ID_1708620854" MODIFIED="1410618604641" TEXT="y antenas receptoras">
<node CREATED="1410618615099" ID="ID_1207498560" MODIFIED="1410618653319" STYLE="bubble" TEXT="1000 m">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1410618768955" FOLDED="true" ID="ID_1150020655" MODIFIED="1410685887843" TEXT="3&#xaa; ICT ">
<node CREATED="1410618783045" ID="ID_341545515" MODIFIED="1410685883002" TEXT="RD 1/1998 (modificado en la DF 5&#xaa;)">
<arrowlink COLOR="#b0b0b0" DESTINATION="ID_1332696020" ENDARROW="Default" ENDINCLINATION="483;0;" ID="Arrow_ID_827554432" STARTARROW="None" STARTINCLINATION="483;0;"/>
</node>
</node>
<node CREATED="1410618814850" FOLDED="true" ID="ID_840137959" MODIFIED="1410685979425" TEXT="4&#xaa; Informaci&#xf3;n confidencial de los datos para las ANR">
<node CREATED="1410685945568" ID="ID_106059140" MODIFIED="1410685976783" TEXT="Justificada por las personas que la aporten"/>
</node>
<node CREATED="1410618829869" FOLDED="true" ID="ID_1712659919" MODIFIED="1410686236569" TEXT="5&#xaa; Consejo Asesor de las Telecomunicaciones y de la Sociedad de la Informaci&#xf3;n">
<node CREATED="1410686010304" ID="ID_357443118" MODIFIED="1410686020938" TEXT="Presidido por el Ministro de Industria"/>
<node CREATED="1410686030468" ID="ID_344197496" MODIFIED="1410686042060" TEXT="Funciones">
<node CREATED="1410686042060" ID="ID_302906459" MODIFIED="1410686061928" TEXT="Estudio, deliberaci&#xf3;n y propuesta en telecomunicaciones"/>
<node CREATED="1410686072231" ID="ID_308369821" MODIFIED="1410686085301" TEXT="Informar al Gobierno sobre los asuntos que determine"/>
</node>
<node CREATED="1410686107131" FOLDED="true" ID="ID_1366591078" MODIFIED="1410686230382" TEXT="Composici&#xf3;n (mediante RD)">
<node CREATED="1410686138630" ID="ID_34228182" MODIFIED="1410686146886" TEXT="En todo caso">
<node CREATED="1410686146886" ID="ID_640609758" MODIFIED="1410686159292" TEXT="Personas con discapacidad"/>
<node CREATED="1410686159696" ID="ID_1426237084" MODIFIED="1410686164997" TEXT="Operadores"/>
<node CREATED="1410686169265" ID="ID_631999000" MODIFIED="1410686183836" TEXT="Servicios de comunicaci&#xf3;n audiovisual"/>
<node CREATED="1410686186605" ID="ID_869880798" MODIFIED="1410686204719" TEXT="Prestadores de servicios de sociedad de la informaci&#xf3;n"/>
<node CREATED="1410686205066" ID="ID_1824227069" MODIFIED="1410686217786" TEXT="Industrias fabricantes de equipos de telecomunicaciones"/>
<node CREATED="1410686218233" ID="ID_1856469839" MODIFIED="1410686221337" TEXT="Sindicatos"/>
<node CREATED="1410686221810" ID="ID_723498404" MODIFIED="1410686227244" TEXT="Colegios oficiales"/>
</node>
</node>
</node>
<node CREATED="1410619029195" FOLDED="true" ID="ID_1114426271" MODIFIED="1410686263470" TEXT="6&#xaa; Multas coercitivas">
<node CREATED="1410619080323" ID="ID_1701528134" MODIFIED="1410619096789" TEXT="Para el cumplimiento de resoluciones o requerimientos de informaci&#xf3;n"/>
<node CREATED="1410619058773" ID="ID_1104579551" MODIFIED="1410619076718" TEXT="Por importe diario de 125 a 30.000 &#x20ac;"/>
<node CREATED="1410686252641" ID="ID_1175909731" MODIFIED="1410686259795" TEXT="Independientes de las sanciones"/>
</node>
<node CREATED="1410619802519" FOLDED="true" ID="ID_106243469" MODIFIED="1410686306111" TEXT="7&#xaa; Obligaciones">
<node CREATED="1410619834713" ID="ID_1803197072" MODIFIED="1410619876014" TEXT="Acceso condicional a los servicios de televisi&#xf3;n y radio (mediante RD)"/>
<node CREATED="1410620134380" ID="ID_1071466380" MODIFIED="1410620153525" TEXT="Acceso a las API y EPG"/>
<node CREATED="1410620167672" ID="ID_732945669" MODIFIED="1410620178262" TEXT="Distribuci&#xf3;n de programas en formato ancho"/>
<node CREATED="1410620195286" ID="ID_1262266046" MODIFIED="1410620225103" TEXT="Usuarios con discapacidad">
<node CREATED="1410620225104" ID="ID_743120174" MODIFIED="1410620235692" TEXT="Canales determinados"/>
<node CREATED="1410620236192" ID="ID_412793683" MODIFIED="1410620243399" TEXT="Servicios complementarios"/>
<node CREATED="1410620270519" ID="ID_1615191612" MODIFIED="1410620289842" TEXT="Cooperaci&#xf3;n en la prestaci&#xf3;n de servicios interoperables"/>
</node>
<node CREATED="1410620467329" ID="ID_1858729005" MODIFIED="1410620603526" TEXT="TDT">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mediante OM
    </p>
  </body>
</html></richcontent>
<node CREATED="1410620471605" ID="ID_648982046" MODIFIED="1410620502271" TEXT="Obligaciones y requisitos de los gestores de multiplex "/>
<node CREATED="1410620503121" ID="ID_170234291" MODIFIED="1410620586254" TEXT="Registro de los par&#xe1;metros de informaci&#xf3;n de los servicios de TDT">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      La llevanza corresponde al Ministerio de Industria, Energ&#xed;a y Turismo
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1410620752704" FOLDED="true" ID="ID_163625015" MODIFIED="1410686360178" TEXT="8&#xaa; Mecanismos de notificaci&#xf3;n">
<node CREATED="1410686328490" ID="ID_220444407" MODIFIED="1410686354981" TEXT="art. 7, 7 bis y 7 ter de la Directiva 2002/21/CE"/>
</node>
<node CREATED="1410620789596" FOLDED="true" ID="ID_1974464363" MODIFIED="1410685932503" TEXT="9&#xaa; Obligaciones a imponer a los operadores">
<node CREATED="1410620803492" ID="ID_1110636011" MODIFIED="1410620898198" TEXT="Informe preceptivo del Ministerio de Industria, Energ&#xed;a y Turismo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
</node>
<node CREATED="1410620864279" ID="ID_1273036535" MODIFIED="1410620885686" TEXT="Deber&#xe1; contemplar los mecanismos de financiaci&#xf3;n de los costes"/>
</node>
<node CREATED="1410620913392" FOLDED="true" ID="ID_1847781717" MODIFIED="1410686366888" TEXT="10&#xaa; Comisi&#xf3;n Interministerial sobre radiofrecuencias y salud">
<node CREATED="1410620958220" ID="ID_1594448394" MODIFIED="1410621032687" TEXT="Ministerio de Industria, Energ&#xed;a y Turismo"/>
<node CREATED="1410620968571" ID="ID_189967800" MODIFIED="1410620998367" TEXT="Ministerio de Sanidad, Servicios Sociales e Igualdad"/>
<node CREATED="1410620999748" ID="ID_606579658" MODIFIED="1410621193696" TEXT="Instituto de Salud Carlos III (Ministerio de Econom&#xed;a)"/>
<node CREATED="1410621048720" ID="ID_613729404" MODIFIED="1410621057678" TEXT="Grupo asesor">
<node CREATED="1410621057679" ID="ID_233831432" MODIFIED="1410621063713" TEXT="CCAA"/>
<node CREATED="1410621068643" ID="ID_919850300" MODIFIED="1410621078696" TEXT="Asociaci&#xf3;n de EELL"/>
<node CREATED="1410621099206" ID="ID_1244381317" MODIFIED="1410621109582" TEXT="Grupo de expertos independientes"/>
<node CREATED="1410621128610" ID="ID_552964862" MODIFIED="1410621137433" TEXT="Sociedades cient&#xed;ficas"/>
<node CREATED="1410621137911" ID="ID_1817423698" MODIFIED="1410621144294" TEXT="Representantes de los ciudadanos"/>
</node>
</node>
<node CREATED="1410621218919" FOLDED="true" ID="ID_1624548449" MODIFIED="1410686438768" TEXT="11&#xaa; Par&#xe1;metros y requrimientos t&#xe9;cnicos esenciales">
<node CREATED="1410686413291" ID="ID_1830408974" MODIFIED="1410686435190" TEXT="Se establecer&#xe1;n por RD aprobado en Consejo de Ministros"/>
</node>
<node CREATED="1410621333042" ID="ID_180206078" MODIFIED="1410621349008" TEXT="12&#xaa; Aplicaci&#xf3;n de la Ley General Tributaria"/>
<node CREATED="1410621301899" FOLDED="true" ID="ID_261814859" MODIFIED="1410686469239" TEXT="13&#xaa; Publicaci&#xf3;n de actos">
<node CREATED="1410686464440" ID="ID_1427752160" MODIFIED="1410686466646" TEXT="BOE"/>
</node>
<node CREATED="1410621310647" ID="ID_892318496" MODIFIED="1410621360388" TEXT="14&#xaa; Coordinaci&#xf3;n de ayudas p&#xfa;blicas a la banda ancha"/>
<node CREATED="1410621461651" FOLDED="true" ID="ID_791098480" MODIFIED="1410686757983" TEXT="15&#xaa; Integraci&#xf3;n de personal en la CNMC">
<node CREATED="1410686596140" ID="ID_472695056" MODIFIED="1410686729446" TEXT="Fecha de ejercicio efectivo de las nuevas funciones por el MINETUR ">
<node CREATED="1410686634628" ID="ID_1362392925" MODIFIED="1410686645972" TEXT="Orden del Ministro de Presidencia"/>
<node CREATED="1410686646621" ID="ID_144421324" MODIFIED="1410686670409" TEXT="A propuesta de">
<node CREATED="1410686670410" ID="ID_1106200596" MODIFIED="1410686682884" TEXT="Ministro de Industria"/>
<node CREATED="1410686683277" ID="ID_539796591" MODIFIED="1410686703161" TEXT="Ministro de Econom&#xed;a"/>
<node CREATED="1410686689792" ID="ID_1813352815" MODIFIED="1410686695816" TEXT="Ministro de Hacienda"/>
</node>
</node>
<node CREATED="1410621528005" ID="ID_813735954" MODIFIED="1410686748200" STYLE="bubble" TEXT="En todo caso, plazo de actuaciones: 4 meses desde la entrada en vigor (11 de mayo de 2014)">
<linktarget COLOR="#b0b0b0" DESTINATION="ID_813735954" ENDARROW="Default" ENDINCLINATION="1918;441;" ID="Arrow_ID_1504954786" SOURCE="ID_1718435937" STARTARROW="None" STARTINCLINATION="1789;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1410621570566" FOLDED="true" ID="ID_957035008" MODIFIED="1410686770551" TEXT="16&#xaa; Red.es">
<node CREATED="1410621692709" ID="ID_1718026266" MODIFIED="1410621719850" TEXT="Gesti&#xf3;n de los nombres de dominio .es"/>
<node CREATED="1410621720893" ID="ID_167322042" MODIFIED="1410621808398" TEXT="Participaci&#xf3;n en la ICANN y asesoramiento al Ministerio en el GAC"/>
<node CREATED="1410621808811" ID="ID_1261082457" MODIFIED="1410621819174" TEXT="Asesoramiento de la AGE"/>
<node CREATED="1410621819498" ID="ID_1274535547" MODIFIED="1410621826870" TEXT="ONTSI"/>
<node CREATED="1410621828830" ID="ID_1513233994" MODIFIED="1410621841412" TEXT="Elaboraci&#xf3;n de estudios e informes"/>
<node CREATED="1410621843684" ID="ID_725473016" MODIFIED="1410621862851" TEXT="Fomento y desarrollo de la Sociedad de la informaci&#xf3;n"/>
</node>
<node CREATED="1410621894475" FOLDED="true" ID="ID_71322796" MODIFIED="1410686798758" TEXT="17&#xaa; Plan de medidas para potenciar la innovaci&#xf3;n">
<node CREATED="1410621985236" ID="ID_47542374" MODIFIED="1410622003382" TEXT="a) Foro de colaboraci&#xf3;n entre los operadores y la industria"/>
<node CREATED="1410622006302" ID="ID_1525864761" MODIFIED="1410622055538" TEXT="b) Estimular las pol&#xed;ticas de innovaci&#xf3;n en el sector en colaboraci&#xf3;n con todos lloas agentes"/>
<node CREATED="1410622056289" ID="ID_1628750706" MODIFIED="1410622105037" TEXT="c) Potenciar compras innovadoras y mercado de demanda temprana"/>
</node>
<node CREATED="1410622130013" FOLDED="true" ID="ID_1057189062" MODIFIED="1410622537716" TEXT="18&#xaa; Universalizaci&#xf3;n de la banda ancha ultrar&#xe1;pida">
<node CREATED="1410622164351" FOLDED="true" ID="ID_1940803549" MODIFIED="1410622534193" TEXT="Estrategia Nacional de Redes Ultrar&#xe1;pidas">
<node CREATED="1410622274017" ID="ID_446385397" MODIFIED="1410622281380" TEXT="Objetivos">
<node CREATED="1410622281381" ID="ID_1581475037" MODIFIED="1410622288896" TEXT="Agenda Digital para Europa"/>
<node CREATED="1410622289249" ID="ID_807774802" MODIFIED="1410622298184" TEXT="Agenda Digital para Espa&#xf1;a"/>
<node CREATED="1410622328893" ID="ID_1733379599" MODIFIED="1410622492607" TEXT="A&#xf1;o 2016: 30 Mbps para ">
<node CREATED="1410622359595" ID="ID_1212095755" MODIFIED="1410622367565" TEXT="Centros de Salud"/>
<node CREATED="1410622367933" ID="ID_1133440122" MODIFIED="1410622380449" TEXT="Centros de Secundaria"/>
<node CREATED="1410622383467" ID="ID_655632660" MODIFIED="1410622390546" TEXT="Bibliotecas p&#xfa;blicas"/>
</node>
<node CREATED="1410622193451" ID="ID_1151102043" MODIFIED="1410622206569" TEXT="A&#xf1;o 2017">
<node CREATED="1410622206570" ID="ID_982259291" MODIFIED="1410622217010" TEXT="Velocidad m&#xed;nima 10 Mbps"/>
</node>
<node CREATED="1410622220951" ID="ID_278348384" MODIFIED="1410622231496" TEXT="Antes de 2020">
<node CREATED="1410622231497" ID="ID_796991037" MODIFIED="1410622248006" TEXT="Todos los usuarios con velocidad minima de 30 Mbps"/>
<node CREATED="1410622248470" ID="ID_1750050985" MODIFIED="1410622509725" TEXT="50% de los hogares con velocidades superiores a 100 Mbps, as&#xed; como:">
<node CREATED="1410622359595" ID="ID_664203267" MODIFIED="1410622367565" TEXT="Centros de Salud"/>
<node CREATED="1410622367933" ID="ID_1636397032" MODIFIED="1410622380449" TEXT="Centros de Secundaria"/>
<node CREATED="1410622383467" ID="ID_1490977723" MODIFIED="1410622390546" TEXT="Bibliotecas p&#xfa;blicas"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1410622538654" ID="ID_673768398" MODIFIED="1410622551721" TEXT="19&#xaa; Estaciones de radioaficionado"/>
</node>
<node CREATED="1410618033837" FOLDED="true" ID="ID_1315494577" MODIFIED="1410807755457" POSITION="left" TEXT="Disposiciones finales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1410675374105" FOLDED="true" ID="ID_1958140318" MODIFIED="1410677298232" TEXT="1&#xaa; Modificaci&#xf3;n de la Ley 13/2011, de regulaci&#xf3;n del juego">
<node CREATED="1410676977284" ID="ID_86992992" MODIFIED="1410677065169" TEXT="Se incluye el concepto de RED PUBLICITARIA dentro de los medios que difunden publicidad de juegos"/>
<node CREATED="1410677068134" ID="ID_1540123686" MODIFIED="1410677088588" TEXT="Se le aplican a las REDES PUBLICITARIAS las obligaciones del resto de medios"/>
<node CREATED="1410677174463" ID="ID_591353664" MODIFIED="1410677190366" TEXT="Se da la potestad sancionadora a la CNMC">
<node CREATED="1410677250843" ID="ID_207707580" MODIFIED="1410677286018" TEXT="Regimen sancionador previsto en la ley 7/2010 de Comunicaci&#xf3;n Audiovisual"/>
</node>
</node>
<node CREATED="1410675443806" FOLDED="true" ID="ID_1914248330" MODIFIED="1410683953580" TEXT="2&#xaa; Modificaci&#xf3;n de la Ley 34/2002 (LSSI)">
<node CREATED="1410677574885" FOLDED="true" ID="ID_1419509174" MODIFIED="1410683494974" TEXT="Oblicaciones de informaci&#xf3;n general">
<node CREATED="1410677584535" ID="ID_750540021" MODIFIED="1410677604403" TEXT="Elimina la competencia de CCAA"/>
</node>
<node CREATED="1410677630295" FOLDED="true" ID="ID_340012851" MODIFIED="1410683498434" TEXT="C&#xf3;digos de conducta">
<node CREATED="1410677661778" ID="ID_1004188940" MODIFIED="1410677690370" TEXT="Sujetos a la ley 3/1991 de competencia desleal "/>
</node>
<node CREATED="1410677695816" ID="ID_1368544247" MODIFIED="1410683684091" TEXT="Comunicaciones comerciales">
<node CREATED="1410677717948" ID="ID_1114928752" MODIFIED="1410683951208" TEXT="Elimina la exigencia de incluir &quot;publi&quot; o &quot;publicidad&quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1410677809102" ID="ID_354170576" MODIFIED="1410677848126" TEXT="Elimina las competencias de CCAA comercio electr&#xf3;nico o publicidad"/>
</node>
<node CREATED="1410677870685" FOLDED="true" ID="ID_1935250711" MODIFIED="1410678947248" TEXT="Consentimiento de cookies">
<node CREATED="1410677936379" ID="ID_831156728" MODIFIED="1410677964120" TEXT="Paramentros del navegador"/>
<node CREATED="1410677964843" ID="ID_339832150" MODIFIED="1410677970676" TEXT="Otras aplicaciones"/>
</node>
<node CREATED="1410678009209" ID="ID_1774889156" MODIFIED="1410678045619" TEXT="Se incluye en el tipo sancionador el uso de cookies sin informaci&#xf3;n o consentimiento del usuario"/>
<node CREATED="1410678363302" ID="ID_1331064159" MODIFIED="1410678813229" TEXT="Responsabilidad (art.37)">
<node CREATED="1410678400361" ID="ID_376437928" MODIFIED="1410678448315" TEXT="Incluye como responsable al agente o red publicitaria que gestione la colocaci&#xf3;n de anuncios"/>
</node>
<node CREATED="1410678552894" FOLDED="true" ID="ID_61091591" MODIFIED="1410683942406" TEXT="Nueva redacci&#xf3;n del concepto de SPAM">
<node CREATED="1410683689738" ID="ID_1475385889" MODIFIED="1410683934853" TEXT="Ya NO es el env&#xed;o de 3 o m&#xe1;s en el plazo de un a&#xf1;o">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1410683786252" ID="ID_336122991" MODIFIED="1410683808184" TEXT="&quot;Env&#xed;o insistente y sistem&#xe1;tico&quot; "/>
</node>
<node CREATED="1410678568582" FOLDED="true" ID="ID_1048055118" MODIFIED="1410678863288" TEXT="Moderaci&#xf3;n de las sanciones (art. 39 bis)">
<node CREATED="1410678661045" ID="ID_1874977188" MODIFIED="1410678675299" TEXT="Concurrencia de varios criterios del art. 40"/>
<node CREATED="1410678677520" ID="ID_1959047619" MODIFIED="1410678692292" TEXT="Regularizaci&#xf3;n de forma diligente"/>
<node CREATED="1410678701827" ID="ID_182032538" MODIFIED="1410678717196" TEXT="Inducci&#xf3;n por la conducta del afectado"/>
<node CREATED="1410678717501" ID="ID_378588386" MODIFIED="1410678742487" TEXT="Reconocimiento espont&#xe1;neo de la culpabilidad"/>
<node CREATED="1410678751569" ID="ID_1122206508" MODIFIED="1410678760251" TEXT="Proceso de fusi&#xf3;n por absorci&#xf3;n"/>
</node>
<node CREATED="1410678612730" FOLDED="true" ID="ID_363032955" MODIFIED="1410683483836" TEXT="Graduaci&#xf3;n de la cuant&#xed;a de las sanciones (art.40)">
<node CREATED="1410678877394" ID="ID_261174766" MODIFIED="1410678885827" TEXT="Existencia de intencionalidad"/>
<node CREATED="1410678886541" ID="ID_1006697542" MODIFIED="1410678891554" TEXT="Plazo de tiempo"/>
<node CREATED="1410678892709" ID="ID_545555110" MODIFIED="1410678897090" TEXT="Reincidencia"/>
<node CREATED="1410678897372" ID="ID_727951899" MODIFIED="1410678908714" TEXT="Naturaleza y cuant&#xed;a de los prejuicios"/>
<node CREATED="1410678909339" ID="ID_1556592059" MODIFIED="1410678917624" TEXT="Beneficios obtenidos"/>
<node CREATED="1410678918923" ID="ID_1908833641" MODIFIED="1410678928160" TEXT="Volumen de facturaci&#xf3;n"/>
<node CREATED="1410678929685" ID="ID_955965980" MODIFIED="1410678939269" TEXT="Adhesi&#xf3;n a un c&#xf3;digo de conducta"/>
</node>
<node CREATED="1410682927775" FOLDED="true" ID="ID_1235362770" MODIFIED="1410683485586" TEXT="Potestad sancionadora (art. 43)">
<node CREATED="1410682968751" ID="ID_1822892799" MODIFIED="1410682975815" TEXT="Infracciones muy graves">
<node CREATED="1410682975816" ID="ID_699383050" MODIFIED="1410683004614" TEXT="Ministro de Industria, Energ&#xed;a y Turismo"/>
</node>
<node CREATED="1410683006251" ID="ID_815671333" MODIFIED="1410683017037" TEXT="Graves y leves">
<node CREATED="1410683017038" ID="ID_256734844" MODIFIED="1410683047362" TEXT="Secretario de Estado de Telecomunicaciones y Sociedad de la Informaci&#xf3;n"/>
</node>
<node CREATED="1410683066929" ID="ID_1885361439" MODIFIED="1410683087884" TEXT="Plazo del procedimiento simplificado">
<node CREATED="1410683087885" ID="ID_12797679" MODIFIED="1410683101276" STYLE="bubble" TEXT="3 meses">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1410682697413" FOLDED="true" ID="ID_1945080688" MODIFIED="1410683486839" TEXT="Entidades de registros de nombres de dominio">
<node CREATED="1410682715745" ID="ID_38630204" MODIFIED="1410683167399" TEXT="Posibilidad de suspensi&#xf3;n de servicio (D.A. 6&#xaa;)"/>
<node CREATED="1410682733117" ID="ID_299100985" MODIFIED="1410683172745" TEXT="Obligaci&#xf3;n de facilitar datos cuando los soliciten las autoridades p&#xfa;blicas (D.A.8&#xaa;)"/>
</node>
<node CREATED="1410682577357" FOLDED="true" ID="ID_44179800" MODIFIED="1410683488552" TEXT="Gesti&#xf3;n de la ciberseguridad (D.A. 9&#xaa;)">
<node CREATED="1410683185239" ID="ID_1386222585" MODIFIED="1410683215515" TEXT="Obligaci&#xf3;n de colaborar y suministrar informaci&#xf3;n al CERT"/>
<node CREATED="1410683220608" ID="ID_944120723" MODIFIED="1410683260739" TEXT="Programa de cooperaci&#xf3;n p&#xfa;blica para mitigar los ataques de ciberseguridad">
<node CREATED="1410683265867" ID="ID_100992933" MODIFIED="1410683287505" STYLE="bubble" TEXT="Plazo: 6 meses">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1410683307407" ID="ID_1562651551" MODIFIED="1410683320579" TEXT="Elaboraci&#xf3;n de c&#xf3;digos de conducta">
<node CREATED="1410683420326" ID="ID_1673117882" MODIFIED="1410683442517" TEXT="Identificar a los usuarios afectados"/>
<node CREATED="1410683459002" ID="ID_1401216981" MODIFIED="1410683467483" TEXT="Indicarles las acciones a llevar a cabo"/>
</node>
</node>
</node>
</node>
<node CREATED="1410675556871" FOLDED="true" ID="ID_1599630327" MODIFIED="1410684540865" TEXT="3&#xaa; Modificaci&#xf3;n de la Ley 38/1999, de Ordenaci&#xf3;n de la Edificaci&#xf3;n">
<node CREATED="1410684003770" ID="ID_1531366050" MODIFIED="1410684033222" TEXT="DA 8&#xaa;: Obligaci&#xf3;n de una declaraci&#xf3;n responsable de la existencia de un proyecto o memoria t&#xe9;cnica"/>
</node>
<node CREATED="1410675607179" FOLDED="true" ID="ID_1841164754" MODIFIED="1410684535078" TEXT="4&#xaa; Modificaci&#xf3;n de la Ley 25/2007, de conservaci&#xf3;n de datos relativos a las comunicaciones">
<node CREATED="1410684096796" FOLDED="true" ID="ID_1842096636" MODIFIED="1410684315421" TEXT="Plazo de ejecuci&#xf3;n de la orden de cesi&#xf3;n">
<node CREATED="1410684112613" ID="ID_1862200427" MODIFIED="1410684124277" TEXT="Fijado en la reoluci&#xf3;n judicial"/>
<node CREATED="1410684128691" ID="ID_1670120139" MODIFIED="1410684191541" TEXT="Si no se establece">
<node CREATED="1410684136399" ID="ID_1872814094" MODIFIED="1410684204457" STYLE="bubble" TEXT="7 d&#xed;as naturales a partir de las 8:00 del natural siguiente en que el sujeto obligado reciba la orden">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1410684319330" ID="ID_687093289" MODIFIED="1410684339678" TEXT="Infracciones (D.A. UNICA)">
<node CREATED="1410684340997" ID="ID_801321606" MODIFIED="1410684355223" TEXT="Muy grave">
<node CREATED="1410684355224" ID="ID_93117047" MODIFIED="1410684372023" TEXT="Incumplimiento de la llevanza del libro-registro"/>
</node>
<node CREATED="1410684377220" ID="ID_22088694" MODIFIED="1410684450064" TEXT="Graves">
<node CREATED="1410684386534" ID="ID_1492288651" MODIFIED="1410684411480" TEXT="llevanza reiterada o sistemanticamente incompleta del libro-registro"/>
<node CREATED="1410684424766" ID="ID_1194261740" MODIFIED="1410684444889" TEXT="Incumplimiento deliberado de la cesi&#xf3;n y entrega de los datos"/>
</node>
<node CREATED="1410684454051" ID="ID_465565946" MODIFIED="1410684463188" TEXT="Leves">
<node CREATED="1410684463189" ID="ID_1488212499" MODIFIED="1410684477885" TEXT="Llevanza incompleta del libro-registro"/>
<node CREATED="1410684488856" ID="ID_355123939" MODIFIED="1410684510823" TEXT="Incumplimiento de la cesi&#xf3;n y entrega de los datos"/>
</node>
</node>
</node>
<node CREATED="1410675648852" FOLDED="true" ID="ID_1332696020" MODIFIED="1410685883001" TEXT="5&#xaa; Modificaci&#xf3;n del RD-ley 1/1998 de ICT">
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1332696020" ENDARROW="Default" ENDINCLINATION="483;0;" ID="Arrow_ID_827554432" SOURCE="ID_341545515" STARTARROW="None" STARTINCLINATION="483;0;"/>
<node CREATED="1410684984221" ID="ID_1526921985" MODIFIED="1410685010297" TEXT="Se consideran t&#xe9;cnicos competentes seg&#xfa;n el Plan de Estudios"/>
</node>
<node CREATED="1410675944576" FOLDED="true" ID="ID_262372306" MODIFIED="1410685100730" TEXT="6&#xaa; Modificaci&#xf3;n de la ley 59/2003 de firma electr&#xf3;nica">
<node CREATED="1410685048699" ID="ID_570817908" MODIFIED="1410685097356" STYLE="bubble" TEXT="Se eleva a 5 a&#xf1;os la validez m&#xe1;xima de los certificados reconocidos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1410675982569" FOLDED="true" ID="ID_1440579764" MODIFIED="1410685555605" TEXT="7&#xaa; Modificaci&#xf3;n de la ley 7/2010 General de Comunicaci&#xf3;n Audiovisual">
<node CREATED="1410685209678" ID="ID_1950635023" MODIFIED="1410685274117" TEXT="art. 5.2. la exclusi&#xf3;n del tiempo dedicado a informaci&#xf3;n, publicidad, televenta, etc afectar&#xe1; a todas las limitaciones, no s&#xf3;lo a las obras europeas "/>
<node CREATED="1410685276420" ID="ID_1319210160" MODIFIED="1410685323468" TEXT="art. 17 se permite el emplazamiento de bienes o servicios, si tienen un valor significativo"/>
<node CREATED="1410685340075" ID="ID_1112581430" MODIFIED="1410685381976" TEXT="art. 38 se puede limitar la recepci&#xf3;n de servicios de la UE">
<node CREATED="1410685381977" ID="ID_314768680" MODIFIED="1410685429897" TEXT="Infrinjan la normativa espa&#xf1;olasobre menores"/>
<node CREATED="1410685430488" ID="ID_1630121889" MODIFIED="1410685468557" TEXT="Incitaciones al odio, por raza, sexo, religi&#xf3;n o nacionalidad"/>
<node CREATED="1410685469176" ID="ID_1872992612" MODIFIED="1410685487041" TEXT="De manera reiterada (dos veces en 12 meses)"/>
</node>
<node CREATED="1410685502174" ID="ID_180387762" MODIFIED="1410685549292" TEXT="art. 39 Las autoridades de cualquier EM pueden solventar los problemas planteados por la autoridad espa&#xf1;ola"/>
</node>
<node CREATED="1410676055654" FOLDED="true" ID="ID_349387577" MODIFIED="1410685565538" TEXT="8&#xaa; Regulaci&#xf3;n de las condiciones de ocupaci&#xf3;n del dominio p&#xfa;blico de los &#xf3;rganos gestores de infraestructuras de transporte de competencia estatal">
<node CREATED="1410676127130" ID="ID_1899983884" MODIFIED="1410676153067" TEXT="Acceso efectivo a dichos bienes"/>
<node CREATED="1410676153354" ID="ID_7962694" MODIFIED="1410676160729" TEXT="Reducci&#xf3;n de cargas"/>
<node CREATED="1410676161194" ID="ID_1391723600" MODIFIED="1410676174009" TEXT="Simplificaci&#xf3;n administrativa"/>
<node CREATED="1410676174311" ID="ID_15403592" MODIFIED="1410676210343" TEXT="Condiciones equitativas, no discriminatorias, objetivas y neutrales"/>
</node>
<node CREATED="1410676217636" FOLDED="true" ID="ID_1948049442" MODIFIED="1410685569675" TEXT="9&#xaa; Fundamento constitucional">
<node CREATED="1410676246545" ID="ID_1705145907" MODIFIED="1410676263971" TEXT="art. 149.1.21&#xaa;"/>
<node CREATED="1410676264743" ID="ID_1372211517" MODIFIED="1410676274267" TEXT="art. 149.1.1&#xaa;"/>
<node CREATED="1410676275329" ID="ID_970753825" MODIFIED="1410676284826" TEXT="art. 149.1.13&#xaa;"/>
</node>
<node CREATED="1410676299503" FOLDED="true" ID="ID_271869873" MODIFIED="1410676419042" TEXT="10&#xaa; Competencias de desarrollo">
<node CREATED="1410676319895" ID="ID_1983697880" MODIFIED="1410676323465" TEXT="Gobierno"/>
<node CREATED="1410676324173" ID="ID_939332004" MODIFIED="1410676328909" TEXT="MINETUR"/>
</node>
<node CREATED="1410676336320" FOLDED="true" ID="ID_1053183652" MODIFIED="1410676421406" TEXT="11&#xaa; Entrada en vigor">
<node CREATED="1410676351728" ID="ID_1554386296" MODIFIED="1410676399799" STYLE="bubble" TEXT="11 de mayo de 2014">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1410813315303" FOLDED="true" ID="ID_1358398892" MODIFIED="1411591164008" POSITION="right" TEXT="T&#xcd;TULO III: Obligaciones de servicio p&#xfa;blico y derechos y obligaciones de car&#xe1;cter p&#xfa;blico en la explotaci&#xf3;n de redes y en la prestaci&#xf3;n de servicios de comunicaciones electr&#xf3;nicas">
<node CREATED="1410813315309" FOLDED="true" ID="ID_950193976" MODIFIED="1410992745046" TEXT="CAP&#xcd;TULO I: Obligaciones de servicio p&#xfa;blico">
<node CREATED="1410813315313" FOLDED="true" ID="ID_1161890105" MODIFIED="1410992411804" TEXT="Secci&#xf3;n 1.&#xaa; Delimitaci&#xf3;n">
<node CREATED="1410813315314" FOLDED="true" ID="ID_1048835360" MODIFIED="1410991683542" TEXT="Art&#xed;culo 23. Delimitaci&#xf3;n de las obligaciones de servicio p&#xfa;blico.">
<node CREATED="1410813315315" MODIFIED="1410813315315" TEXT="1. Est&#xe1; garantizada la existencia de servicios de comunicaciones electr&#xf3;nicas disponibles al p&#xfa;blico, de adecuada calidad en todo el territorio nacional a trav&#xe9;s de una competencia y una libertad de elecci&#xf3;n reales."/>
<node CREATED="1410813315317" MODIFIED="1410813315317" TEXT="2. Cuando se impongan obligaciones de servicio p&#xfa;blico, se aplicar&#xe1; con car&#xe1;cter supletorio el r&#xe9;gimen establecido para la concesi&#xf3;n de servicio p&#xfa;blico determinado por el texto refundido de la Ley de Contratos del Sector P&#xfa;blico"/>
<node CREATED="1410813315319" ID="ID_53238082" MODIFIED="1410991642739" TEXT="3. El cumplimiento de las obligaciones de servicio p&#xfa;blico se efectuar&#xe1; con respeto a los principios de igualdad, transparencia, no discriminaci&#xf3;n, continuidad, adaptabilidad, disponibilidad y permanencia y conforme a los t&#xe9;rminos y condiciones que mediante RD se determinen."/>
<node CREATED="1410813315321" MODIFIED="1410813315321" TEXT="4. Corresponde al MINETUR el control y el ejercicio de las facultades de la Administraci&#xf3;n relativas a las obligaciones de servicio p&#xfa;blico."/>
<node CREATED="1410813315323" ID="ID_1523853504" MODIFIED="1410991678803" TEXT="5. Cuando el MINETUR constate que cualquiera de los servicios a que se refiere este art&#xed;culo se est&#xe1; prestando en competencia, en condiciones de precio, cobertura y calidad de servicio similares a aquellas en que los operadores designados deben prestarlas, podr&#xe1;, previo informe de la CNMC y audiencia a los interesados, determinar el cese de su prestaci&#xf3;n como obligaci&#xf3;n de servicio p&#xfa;blico."/>
</node>
<node CREATED="1410813315325" FOLDED="true" ID="ID_425514586" MODIFIED="1410991787077" TEXT="Art&#xed;culo 24. Categor&#xed;as de obligaciones de servicio p&#xfa;blico.">
<node CREATED="1410813315326" MODIFIED="1410813315326" TEXT="a) El servicio universal en los t&#xe9;rminos contenidos en la secci&#xf3;n 2.&#xaa; de este cap&#xed;tulo."/>
<node CREATED="1410813315327" MODIFIED="1410813315327" TEXT="b) Otras obligaciones de servicio p&#xfa;blico impuestas por razones de inter&#xe9;s general, en la forma y con las condiciones establecidas en la secci&#xf3;n 3.&#xaa; de este cap&#xed;tulo."/>
</node>
</node>
<node CREATED="1410813315329" FOLDED="true" ID="ID_1760919372" MODIFIED="1410992410773" TEXT="Secci&#xf3;n 2.&#xaa; El servicio universal">
<node CREATED="1410813315329" FOLDED="true" ID="ID_560455374" MODIFIED="1410992038478" TEXT="Art&#xed;culo 25. Concepto y &#xe1;mbito de aplicaci&#xf3;n.">
<node CREATED="1410813315331" FOLDED="true" ID="ID_1651872077" MODIFIED="1410992019549" TEXT="1. Servicio universal es el conjunto  de servicios cuya prestaci&#xf3;n se garantiza para todos los usuarios con independencia de su localizaci&#xf3;n geogr&#xe1;fica, con una calidad determinada y a un precio asequible. El MINETUR supervisar&#xe1; la evoluci&#xf3;n y el nivel de la tarificaci&#xf3;n. Bajo el concepto de servicio universal se deber&#xe1; garantizar, en los t&#xe9;rminos y condiciones que se determinen mediante RD que:">
<node CREATED="1410813315334" MODIFIED="1410813315334" TEXT="a) Todos los usuarios finales puedan obtener una conexi&#xf3;n a la red p&#xfa;blica de comunicaciones electr&#xf3;nicas desde una ubicaci&#xf3;n fija siempre que sus solicitudes se consideren razonables en los t&#xe9;rminos que mediante RD se determinen y que, incluir&#xe1;n, el coste de su provisi&#xf3;n. La conexi&#xf3;n debe permitir comunicaciones de voz, fax y datos, a velocidad suficiente para acceder de forma funcional a Internet (en sentido descendente 1 Mbps). El Gobierno podr&#xe1; actualizar esta velocidad."/>
<node CREATED="1410813315337" MODIFIED="1410813315337" TEXT="b) Prestaci&#xf3;n de un servicio telef&#xf3;nico disponible al p&#xfa;blico a trav&#xe9;s de la conexi&#xf3;n a Internet."/>
<node CREATED="1410813315338" MODIFIED="1410813315338" TEXT="c) Los abonados al servicio telef&#xf3;nico disponible al p&#xfa;blico contar&#xe1;n con una gu&#xed;a de n&#xfa;meros de abonados, ya sea impresa o electr&#xf3;nica, o ambas, que se actualice, como m&#xed;nimo, una vez al a&#xf1;o. Mediante RD se determinar&#xe1;n los colectivos de abonados que pueden solicitar que se le entregue  impresa, y al menos un servicio de informaci&#xf3;n general sobre n&#xfa;meros de abonados. Todos los abonados al servicio telef&#xf3;nico disponible al p&#xfa;blico tendr&#xe1;n derecho a figurar en la gu&#xed;a, sin perjuicio, de la protecci&#xf3;n de los datos personales y el derecho a la intimidad."/>
<node CREATED="1410813315340" MODIFIED="1410813315340" TEXT="d) Oferta suficiente de tel&#xe9;fonos p&#xfa;blicos de pago."/>
<node CREATED="1410813315342" MODIFIED="1410813315342" TEXT="e) Los usuarios finales con discapacidad tengan acceso a los servicios incluidos en los p&#xe1;rrafos b), c) y d) de este apartado, a un nivel equivalente al que disfrutan otros usuarios finales."/>
<node CREATED="1410813315343" MODIFIED="1410813315343" TEXT="f) Se ofrezcan a los consumidores que sean personas f&#xed;sicas con necesidades sociales especiales. Podr&#xe1;n aplicarse, cuando proceda, limitaciones de precios, tarifas comunes, equiparaci&#xf3;n geogr&#xe1;fica u otros reg&#xed;menes similares a las prestaciones incluidas en este art&#xed;culo."/>
</node>
<node CREATED="1410813315345" MODIFIED="1410813315345" TEXT="2. Mediante RD se podr&#xe1;n adoptar medidas a para garantizar que los usuarios con discapacidad puedan beneficiarse de la capacidad de elecci&#xf3;n de operadores. Podr&#xe1;n establecerse sistemas de ayuda directa a las personas f&#xed;sicas con rentas bajas o con necesidades sociales especiales."/>
<node CREATED="1410813315347" MODIFIED="1410813315347" TEXT="3. Todas las obligaciones que se incluyen en el servicio universal estar&#xe1;n sujetas a los mecanismos de financiaci&#xf3;n que del art&#xed;culo 27."/>
<node CREATED="1410813315349" MODIFIED="1410813315349" TEXT="4. El Gobierno, de conformidad con la normativa comunitaria, podr&#xe1; revisar el alcance de las obligaciones de servicio universal."/>
</node>
<node CREATED="1410813315351" FOLDED="true" ID="ID_1348073895" MODIFIED="1410992146184" TEXT="Art&#xed;culo 26. Designaci&#xf3;n de los operadores encargados de la prestaci&#xf3;n del servicio universal.">
<node CREATED="1410813315351" MODIFIED="1410813315351" TEXT="1. Cuando la prestaci&#xf3;n del servicio universal no quede garantizada por el libre mercado, el MINETUR  designar&#xe1; uno o m&#xe1;s operadores."/>
<node CREATED="1410813315353" MODIFIED="1410813315353" TEXT="2. El sistema de designaci&#xf3;n de operadores encargados de garantizar la prestaci&#xf3;n de los servicios del servicio universal se establecer&#xe1; mediante RD con sujeci&#xf3;n a los principios de eficiencia, objetividad, transparencia y no discriminaci&#xf3;n sin excluir a priori la designaci&#xf3;n de ninguna empresa. Licitaci&#xf3;n p&#xfa;blica para dichos servicios, prestaciones y ofertas."/>
<node CREATED="1410813315355" MODIFIED="1410813315355" TEXT="3. Cuando el operador designado para la prestaci&#xf3;n del servicio universal se proponga entregar una parte o la totalidad de sus activos de red de acceso local a una persona jur&#xed;dica separada de distinta propiedad, informar&#xe1; con la debida antelaci&#xf3;n al Minetur a fin de evaluar las repercusiones. El Minetur podr&#xe1; imponer, modificar o suprimir obligaciones al operador designado."/>
<node CREATED="1410813315357" ID="ID_1035838886" MODIFIED="1410992129018" TEXT="4. El Minetur  podr&#xe1; establecer objetivos de rendimiento."/>
<node CREATED="1410813315359" ID="ID_418662428" MODIFIED="1410992143914" TEXT="5. El Minetur notificar&#xe1; a la Comisi&#xf3;n Europea las obligaciones de servicio universal impuestas."/>
</node>
<node CREATED="1410813315361" FOLDED="true" ID="ID_376914407" MODIFIED="1410992409138" TEXT="Art&#xed;culo 27. Coste y financiaci&#xf3;n del servicio universal.">
<node CREATED="1410813315362" MODIFIED="1410813315362" TEXT="1. La CNMC determinar&#xe1; si la obligaci&#xf3;n de la prestaci&#xf3;n del servicio universal puede implicar una carga injustificada. El coste neto de prestaci&#xf3;n del servicio universal ser&#xe1; determinado peri&#xf3;dicamente por la CNMC de acuerdo con los procedimientos de designaci&#xf3;n previstos en el art&#xed;culo 26.2 o en funci&#xf3;n del ahorro neto que el operador conseguir&#xed;a si no tuviera la obligaci&#xf3;n de prestar el servicio universal. Para la determinaci&#xf3;n de este ahorro neto la CNMC desarrollar&#xe1; y publicar&#xe1; una metodolog&#xed;a de acuerdo con los criterios que se establezcan mediante RD."/>
<node CREATED="1410813315365" ID="ID_406616220" MODIFIED="1410992232930" TEXT="2. El coste neto de la obligaci&#xf3;n de prestaci&#xf3;n del servicio universal ser&#xe1; financiado por un mecanismo de reparto, por aquellos operadores que obtengan por la explotaci&#xf3;n de redes o la prestaci&#xf3;n de servicios de comunicaciones electr&#xf3;nicas unos ingresos brutos de explotaci&#xf3;n anuales superiores a 100 millones de euros. Esta cifra podr&#xe1; ser actualizada o modificada mediante RD acordado en Consejo de Ministros, previo informe de la CNMC."/>
<node CREATED="1410813315367" ID="ID_1928807245" MODIFIED="1410992260499" TEXT="3. La CNMC determinar&#xe1; las aportaciones que correspondan a cada uno de los operadores- Las aportaciones recibidas se depositar&#xe1;n en el Fondo nacional del servicio universal, que se crea por esta Ley."/>
<node CREATED="1410813315369" MODIFIED="1410813315369" TEXT="4. Los gastos de gesti&#xf3;n de su cuenta ser&#xe1;n deducidos de su saldo, y los rendimientos minorar&#xe1;n la contribuci&#xf3;n de los aportantes. Podr&#xe1;n depositarse aportaciones realizadas por cualquier persona f&#xed;sica o jur&#xed;dica que desee contribuir. Los operadores sujetos a obligaciones de prestaci&#xf3;n del servicio universal recibir&#xe1;n de este fondo la cantidad correspondiente al coste neto. La CNMC se encargar&#xe1; de la gesti&#xf3;n del Fondo, mediante RD se determinar&#xe1; su estructura, organizaci&#xf3;n, plazos en los que se realizar&#xe1;n las aportaciones..."/>
<node CREATED="1410813315372" MODIFIED="1410813315372" TEXT="5. Mediante RD podr&#xe1; preverse la existencia de un mecanismo de compensaci&#xf3;n directa entre operadores para aquellos casos en que la magnitud del coste no justifique los costes de gesti&#xf3;n del fondo nacional del servicio universal."/>
</node>
</node>
<node CREATED="1410813315374" FOLDED="true" ID="ID_737850109" MODIFIED="1410992744077" TEXT="Secci&#xf3;n 3.&#xaa; Otras obligaciones de servicio p&#xfa;blico">
<node CREATED="1410813315375" ID="ID_105934751" MODIFIED="1410992413419" TEXT="Art&#xed;culo 28. Otras obligaciones de servicio p&#xfa;blico.">
<node CREATED="1410813315376" MODIFIED="1410813315376" TEXT="1. El Gobierno podr&#xe1;, por necesidades de la defensa nacional, de la seguridad p&#xfa;blica, seguridad vial o de los servicios que afecten a la seguridad de las personas o a la protecci&#xf3;n civil, imponer otras obligaciones de servicio p&#xfa;blico distintas de las de servicio universal a los operadores."/>
<node CREATED="1410813315378" FOLDED="true" ID="ID_923062384" MODIFIED="1410992663271" TEXT="2. El Gobierno podr&#xe1;, imponer otras obligaciones de servicio p&#xfa;blico, previo informe de la CNMC, as&#xed; como de la administraci&#xf3;n territorial competente, motivadas por: ">
<node CREATED="1410992507807" ID="ID_522274236" MODIFIED="1410992575141" TEXT="a) cohesi&#xf3;n territorial"/>
<node CREATED="1410992519374" ID="ID_1839735641" MODIFIED="1410992624700" TEXT="b)extensi&#xf3;n del uso de nuevos servicios y tecnolog&#xed;as"/>
<node CREATED="1410992525646" ID="ID_1259663590" MODIFIED="1410992635342" TEXT="c) Por la necesidad de facilitar la comunicaci&#xf3;n entre determinados colectivos que se encuentren en circunstancias especiales y est&#xe9;n insuficientemente atendidos."/>
<node CREATED="1410992528942" ID="ID_1560075690" MODIFIED="1410992646934" TEXT="d) Por la necesidad de facilitar la disponibilidad de servicios que comporten la acreditaci&#xf3;n de fehaciencia del contenido del mensaje remitido o de su remisi&#xf3;n o recepci&#xf3;n."/>
</node>
<node CREATED="1410813315381" MODIFIED="1410813315381" TEXT="3. Mediante RD se regular&#xe1; el procedimiento de imposici&#xf3;n de las obligaciones a las que se refiere el apartado anterior y su forma de financiaci&#xf3;n."/>
<node CREATED="1410813315382" ID="ID_279903795" MODIFIED="1410992738787" TEXT="4. La obligaci&#xf3;n de encaminar las llamadas a los servicios de emergencia sin derecho a contraprestaci&#xf3;n econ&#xf3;mica de ning&#xfa;n tipo debe ser asumida tanto por los operadores, respecto de las llamadas dirigidas al 112 de atenci&#xf3;n a emergencias y a otros que se determinen mediante RD, incluidas aquellas que se efect&#xfa;en desde tel&#xe9;fonos p&#xfa;blicos de pago.  El servicio de llamadas de emergencia ser&#xe1; gratuito para los usuarios,  los operadores pondr&#xe1;n gratuitamente a disposici&#xf3;n de las autoridades receptoras de dichas llamadas la informaci&#xf3;n que mediante RD se determine relativa a la ubicaci&#xf3;n de su procedencia. "/>
</node>
</node>
</node>
<node CREATED="1410813315384" FOLDED="true" ID="ID_1911887606" MODIFIED="1411584545386" TEXT="CAP&#xcd;TULO II: Derechos de los operadores y despliegue de redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas">
<node CREATED="1410813315385" FOLDED="true" ID="ID_208484106" MODIFIED="1411507714716" TEXT="Secci&#xf3;n 1.&#xaa; Derechos de los operadores a la ocupaci&#xf3;n del dominio p&#xfa;blico, a ser beneficiarios en el procedimiento de expropiaci&#xf3;n forzosa y al establecimiento a su favor de servidumbres y de limitaciones a la propiedad">
<node CREATED="1410813315386" FOLDED="true" ID="ID_287955072" MODIFIED="1411507540532" TEXT="Art&#xed;culo 29. Derecho de ocupaci&#xf3;n de la propiedad privada.">
<node CREATED="1410813315387" ID="ID_242612545" MODIFIED="1411507402556" TEXT="1. Los operadores tendr&#xe1;n derecho a la ocupaci&#xf3;n de la propiedad privada cuando resulte estrictamente necesario para la instalaci&#xf3;n de la red en la medida prevista en el proyecto t&#xe9;cnico presentado y siempre que no existan otras alternativas, ya sea a trav&#xe9;s de su expropiaci&#xf3;n forzosa o mediante la declaraci&#xf3;n de servidumbre forzosa de paso.  Los operadores asumir&#xe1;n los costes a los que hubiera lugar por esta ocupaci&#xf3;n. La ocupaci&#xf3;n de la propiedad privada se llevar&#xe1; a cabo tras la instrucci&#xf3;n y resoluci&#xf3;n por el Minetur del oportuno procedimiento seg&#xfa;n la legislaci&#xf3;n de expropiaci&#xf3;n forzosa."/>
<node CREATED="1410813315390" ID="ID_679900562" MODIFIED="1410992799795" TEXT="2. La aprobaci&#xf3;n por el Minetur del proyecto t&#xe9;cnico para la ocupaci&#xf3;n de propiedad privada llevar&#xe1; impl&#xed;cita la declaraci&#xf3;n de utilidad p&#xfa;blica."/>
<node CREATED="1410813315391" ID="ID_931401945" MODIFIED="1411507516049" TEXT="3. Con car&#xe1;cter previo a la aprobaci&#xf3;n del proyecto t&#xe9;cnico, se recabar&#xe1; informe del &#xf3;rgano de la comunidad aut&#xf3;noma competente en materia de ordenaci&#xf3;n del territorio, que habr&#xe1; de ser emitido en el plazo m&#xe1;ximo de 30 d&#xed;as h&#xe1;biles. Si el proyecto afecta a un &#xe1;rea geogr&#xe1;fica relevante o pudiera tener afecciones ambientales, este plazo ser&#xe1; ampliado hasta tres meses. Asimismo, se recabar&#xe1; informe de los Ayuntamientos afectados sobre compatibilidad del proyecto t&#xe9;cnico con la ordenaci&#xf3;n urban&#xed;stica vigente, que deber&#xe1; ser emitido en el plazo de 30 d&#xed;as."/>
<node CREATED="1410813315393" ID="ID_1331792485" MODIFIED="1411507538224" TEXT="4. En las expropiaciones ligadas al cumplimiento de obligaciones de servicio p&#xfa;blico se seguir&#xe1; el procedimiento especial de urgencia establecido en la Ley de Expropiaci&#xf3;n Forzosa, cuando as&#xed; se haga constar en la resoluci&#xf3;n del Minetur que apruebe el oportuno proyecto t&#xe9;cnico."/>
</node>
<node CREATED="1410813315394" FOLDED="true" ID="ID_1240400670" MODIFIED="1411507568851" TEXT="Art&#xed;culo 30. Derecho de ocupaci&#xf3;n del dominio p&#xfa;blico:">
<node CREATED="1410813315395" ID="ID_1258944165" MODIFIED="1410813315395" TEXT="Los titulares del dominio p&#xfa;blico garantizar&#xe1;n el acceso de todos los operadores a dicho dominio en condiciones neutrales, objetivas, transparentes, equitativas y no discriminatorias. La ocupaci&#xf3;n o el derecho de uso de dominio p&#xfa;blico para la instalaci&#xf3;n o explotaci&#xf3;n de una red no podr&#xe1; ser otorgado o asignado mediante procedimientos de licitaci&#xf3;n."/>
</node>
<node CREATED="1410813315396" FOLDED="true" ID="ID_354596111" MODIFIED="1411507619668" TEXT="Art&#xed;culo 31. Normativa aplicable a la ocupaci&#xf3;n del dominio p&#xfa;blico y la propiedad privada.">
<node CREATED="1410813315397" ID="ID_616571277" MODIFIED="1410992886131" TEXT="1. La normativa dictada por cualquier Adm&#xf3;n P&#xfa;blica deber&#xe1; reconocer el derecho de ocupaci&#xf3;n del dominio p&#xfa;blico o la propiedad privada para el despliegue de las redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas.."/>
<node CREATED="1410813315399" FOLDED="true" ID="ID_1564324470" MODIFIED="1411507608107" TEXT="2. Las normas que se dicten por las correspondientes Administraciones deber&#xe1;n:">
<node CREATED="1410813315400" MODIFIED="1410813315400" TEXT="a) Ser publicadas en un diario oficial del &#xe1;mbito de la Administraci&#xf3;n competente y en su p&#xe1;gina web"/>
<node CREATED="1410813315400" MODIFIED="1410813315400" TEXT="b) Prever un procedimiento r&#xe1;pido, sencillo, eficiente y no discriminatorio de resoluci&#xf3;n de las solicitudes de ocupaci&#xf3;n, que no podr&#xe1; exceder de seis meses contados a partir de la presentaci&#xf3;n de la solicitud, salvo en caso de expropiaci&#xf3;n."/>
<node CREATED="1410813315402" ID="ID_222878619" MODIFIED="1411507600796" TEXT="c) Garantizar la transparencia de los procedimientos y que las normas aplicables fomenten la competencia."/>
<node CREATED="1410813315402" ID="ID_978267921" MODIFIED="1410992918602" TEXT="d) Garantizar el respeto de los l&#xed;mites impuestos a la intervenci&#xf3;n administrativa en esta Ley en protecci&#xf3;n de los derechos de los operadores."/>
</node>
<node CREATED="1410813315404" ID="ID_536360996" MODIFIED="1410813315404" TEXT="3. Si las AAPP  o titulares del dominio p&#xfa;blico ostentan la propiedad, total o parcial, o ejercen el control directo o indirecto de operadores deber&#xe1;n mantener una separaci&#xf3;n estructural entre dichos operadores y los &#xf3;rganos encargados de la regulaci&#xf3;n y gesti&#xf3;n de los derechos de utilizaci&#xf3;n del dominio p&#xfa;blico correspondiente."/>
</node>
<node CREATED="1410813315405" FOLDED="true" ID="ID_355980223" MODIFIED="1411507692066" TEXT="Art&#xed;culo 32. Ubicaci&#xf3;n compartida y uso compartido de la propiedad p&#xfa;blica o privada.">
<node CREATED="1410813315406" ID="ID_871128027" MODIFIED="1410992965882" TEXT="1. Las AAPP fomentar&#xe1;n la celebraci&#xf3;n de acuerdos voluntarios entre operadores para la ubicaci&#xf3;n compartida y el uso compartido de infraestructuras situadas en bienes de titularidad p&#xfa;blica o privada."/>
<node CREATED="1410813315408" ID="ID_1874804877" MODIFIED="1410993027787" TEXT="2. El MINETUR, mediante RD, previo tr&#xe1;mite de audiencia a los operadores afectados podr&#xe1; imponer la utilizaci&#xf3;n compartida del dominio p&#xfa;blico o la propiedad privada.   Cuando una Administraci&#xf3;n p&#xfa;blica competente considere que por razones de medio ambiente, salud p&#xfa;blica, seguridad p&#xfa;blica u ordenaci&#xf3;n urbana y territorial procede la imposici&#xf3;n de la utilizaci&#xf3;n compartida podr&#xe1; instar de manera motivada al MINETUR el inicio del procedimiento. El MINETUR antes de imponer la utilizaci&#xf3;n compartida  deber&#xe1; realizar un tr&#xe1;mite para que la Administraci&#xf3;n p&#xfa;blica competente que ha instado el procedimiento pueda efectuar alegaciones por un plazo de 15 d&#xed;as h&#xe1;biles."/>
<node CREATED="1410813315410" MODIFIED="1410813315410" TEXT="3. Las medidas deber&#xe1;n ser objetivas, transparentes, no discriminatorias y proporcionadas."/>
</node>
<node CREATED="1410813315411" FOLDED="true" ID="ID_233394848" MODIFIED="1411507710890" TEXT="Art&#xed;culo 33. Otras servidumbres y limitaciones a la propiedad.">
<node CREATED="1410813315411" MODIFIED="1410813315411" TEXT="1. Podr&#xe1;n establecerse las limitaciones a la propiedad y a la intensidad de campo el&#xe9;ctrico y las servidumbres necesarias para la protecci&#xf3;n radioel&#xe9;ctrica de determinadas instalaciones."/>
<node CREATED="1410813315412" ID="ID_756771581" MODIFIED="1410993057066" TEXT="2. Podr&#xe1;n imponerse l&#xed;mites a los derechos de uso del dominio p&#xfa;blico radioel&#xe9;ctrico para la protecci&#xf3;n de otros bienes jur&#xed;dicamente protegidos o de servicios p&#xfa;blicos que puedan verse afectados, en los t&#xe9;rminos que mediante RD se determinen. "/>
</node>
</node>
<node CREATED="1410813315414" FOLDED="true" ID="ID_1588570136" MODIFIED="1411510780123" TEXT="Secci&#xf3;n 2.&#xaa; Normativa de las AAPP que afecte al despliegue de redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas">
<node CREATED="1410993565988" FOLDED="true" ID="ID_1919367681" MODIFIED="1411508526339" TEXT="Art&#xed;culo 34. Colaboraci&#xf3;n entre AAPP en el despliegue de las redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas.">
<node CREATED="1410993565990" MODIFIED="1410993565990" TEXT="1. Las AAPP deber&#xe1;n colaborar a fin de hacer efectivo el derecho de los operadores de  de ocupar la propiedad p&#xfa;blica y privada para realizar el despliegue de redes p&#xfa;blicas."/>
<node CREATED="1410993565991" MODIFIED="1410993565991" TEXT="2. Las redes p&#xfa;blicas de comunicaciones son equipamiento de car&#xe1;cter b&#xe1;sico y su previsi&#xf3;n en los instrumentos de planificaci&#xf3;n urban&#xed;stica tiene el car&#xe1;cter de determinaciones estructurantes. Su instalaci&#xf3;n y despliegue constituyen obras de inter&#xe9;s general."/>
<node CREATED="1410993565992" ID="ID_1580426358" MODIFIED="1411507909017" TEXT="3. La normativa elaborada por las AAPP que afecte al despliegue de las redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas y los instrumentos de planificaci&#xf3;n territorial o urban&#xed;stica deber&#xe1;n impulsar o facilitar el despliegue en su &#xe1;mbito territorial."/>
<node CREATED="1410993565994" MODIFIED="1410993565994" TEXT="4. La normativa elaborada por las AAPP deber&#xe1; cumplir con la normativa sectorial de telecom. Los operadores no tendr&#xe1;n obligaci&#xf3;n de aportar la documentaci&#xf3;n o informaci&#xf3;n  que ya obre en poder de la Administraci&#xf3;n. El MINETUR establecer&#xe1;, mediante RD, la forma en que se facilitar&#xe1; a las AAPP la informaci&#xf3;n que precisen para el ejercicio de sus propias competencias."/>
<node CREATED="1410993565995" ID="ID_579793017" MODIFIED="1410993565995" TEXT="5. Los operadores deber&#xe1;n hacer uso de las canalizaciones subterr&#xe1;neas o en el interior de las edificaciones. En los casos en los que no existan  o no sea posible podr&#xe1;n efectuar despliegues a&#xe9;reos siguiendo los previamente existentes. Los operadores podr&#xe1;n efectuar por fachadas despliegue de cables y equipos utilizando, en la medida de lo posible, los ya previamente instalados. Los despliegues a&#xe9;reos y por fachadas no podr&#xe1;n realizarse en casos de edificaciones del patrimonio hist&#xf3;rico-art&#xed;stico o que puedan afectar a la seguridad p&#xfa;blica."/>
<node CREATED="1410993565997" ID="ID_1588253786" MODIFIED="1411508274913" TEXT="6. Para la instalaci&#xf3;n de las estaciones o infraestructuras radioel&#xe9;ctricas a las que se refiere la disposici&#xf3;n adicional tercera de la Ley 12/2012, de medidas urgentes de liberalizaci&#xf3;n del comercio y de determinados servicios, no podr&#xe1; exigirse la obtenci&#xf3;n de licencia previa. Para la instalaci&#xf3;n de redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas o de estaciones radioel&#xe9;ctricas en dominio privado distintas de las se&#xf1;aladas en el p&#xe1;rrafo anterior, no podr&#xe1; exigirse por parte de las AAPP la obtenci&#xf3;n de licencia, en el caso de que el operador haya presentado a la administraci&#xf3;n p&#xfa;blica competente para el otorgamiento de la licencia o autorizaci&#xf3;n un plan de despliegue en el que se contemplen dichas infraestructuras o estaciones, y siempre que el citado plan haya sido aprobado. En el Plan de despliegue o instalaci&#xf3;n se sujetar&#xe1; al contenido y condiciones t&#xe9;cnicas exigidas mediante RD acordado en Consejo de Ministros. Se entender&#xe1; aprobado si, transcurridos dos meses desde su presentaci&#xf3;n, la administraci&#xf3;n p&#xfa;blica competente no ha dictado resoluci&#xf3;n expresa. Las licencias o autorizaciones previas ser&#xe1;n sustituidas por declaraciones responsables."/>
<node CREATED="1410993566001" ID="ID_575143304" MODIFIED="1410993566001" TEXT="7. Si sobre una infraestructura de red p&#xfa;blica de comunicaciones electr&#xf3;nicas, fija o m&#xf3;vil, incluidas las estaciones radioel&#xe9;ctricas de comunicaciones electr&#xf3;nicas, ya est&#xe9; ubicada en dominio p&#xfa;blico o privado, se realicen actuaciones de innovaci&#xf3;n tecnol&#xf3;gica o adaptaci&#xf3;n t&#xe9;cnica que supongan la incorporaci&#xf3;n de nuevo equipamiento o la realizaci&#xf3;n de emisiones radioel&#xe9;ctricas en nuevas bandas de frecuencias o con otras tecnolog&#xed;as, sin variar los elementos de obra civil y m&#xe1;stil, no se requerir&#xe1; ning&#xfa;n tipo de concesi&#xf3;n, autorizaci&#xf3;n o licencia nueva o modificaci&#xf3;n de la existente o declaraci&#xf3;n responsable o comunicaci&#xf3;n previa a las administraciones p&#xfa;blicas competentes por razones de ordenaci&#xf3;n del territorio, urbanismo o medioambientales."/>
<node CREATED="1410993566002" ID="ID_1867935525" MODIFIED="1411508425428" TEXT="8. Cuando las AAPP elaboren proyectos que impliquen la variaci&#xf3;n en la ubicaci&#xf3;n de una infraestructura o un elemento de la red de transmisi&#xf3;n de comunicaciones electr&#xf3;nicas, deber&#xe1;n dar audiencia previa al operador titular de la infraestructura afectada, a fin de que realice las alegaciones."/>
</node>
<node CREATED="1410993566004" FOLDED="true" ID="ID_1192634631" MODIFIED="1411510674731" TEXT="Art&#xed;culo 35. Mecanismos de colaboraci&#xf3;n entre el MINETUR y las AAPP para el despliegue de las redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas.">
<node CREATED="1410993566005" ID="ID_969166537" MODIFIED="1411508566439" TEXT="1. El MINETUR y las AAPP tienen los deberes de rec&#xed;proca informaci&#xf3;n y de colaboraci&#xf3;n y cooperaci&#xf3;n mutuas en el ejercicio de sus actuaciones de regulaci&#xf3;n y que puedan afectar a las telecomunicaciones. "/>
<node CREATED="1410993566007" ID="ID_1583277724" MODIFIED="1411508747313" TEXT="2. Los &#xf3;rganos encargados de planificaci&#xf3;n territorial o urban&#xed;stica que afecten al despliegue de las redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas deber&#xe1;n recabar el oportuno informe del MINETUR. El informe preceptivo ser&#xe1; previo a la aprobaci&#xf3;n del instrumento de planificaci&#xf3;n y tendr&#xe1; car&#xe1;cter vinculante. El MINETUR emitir&#xe1; el informe en un plazo m&#xe1;ximo de tres meses. Transcurrido dicho plazo, el informe se entender&#xe1; emitido con car&#xe1;cter favorable y podr&#xe1; continuarse con la tramitaci&#xf3;n del instrumento de planificaci&#xf3;n. En el caso de que el informe no sea favorable, los &#xf3;rganos encargados de la tramitaci&#xf3;n de los procedimientos de aprobaci&#xf3;n, modificaci&#xf3;n o revisi&#xf3;n de los instrumentos de planificaci&#xf3;n territorial o urban&#xed;stica dispondr&#xe1;n de un plazo m&#xe1;ximo de un mes, a contar desde la recepci&#xf3;n del informe, para remitir al MINETUR sus alegaciones al informel. Sobre ellas el MINETUR emitir&#xe1; un nuevo informe en el plazo m&#xe1;ximo de un mes a contar desde la recepci&#xf3;n de las alegaciones. "/>
<node CREATED="1410993566010" ID="ID_701439617" MODIFIED="1411508781272" TEXT="3. Mediante orden, el MINETUR podr&#xe1; establecer la forma en que han de solicitarse los informes a que se refiere el apartado anterior y la informaci&#xf3;n a facilitar por parte del &#xf3;rgano solicitante pudiendo exigirse a las AAPP competentes su tramitaci&#xf3;n por v&#xed;a electr&#xf3;nica."/>
<node CREATED="1410993566011" ID="ID_1837752781" MODIFIED="1411510510321" TEXT="4.  En defecto de acuerdo entre las AAPP el Consejo de Ministros podr&#xe1; autorizar la ubicaci&#xf3;n o el itinerario concreto de una infraestructura de red de comunicaciones electr&#xf3;nicas, en cuyo caso la administraci&#xf3;n p&#xfa;blica competente deber&#xe1; incorporar necesariamente en sus respectivos instrumentos de ordenaci&#xf3;n las rectificaciones imprescindibles para acomodar sus determinaciones a aqu&#xe9;llas."/>
<node CREATED="1410993566013" ID="ID_1581494888" MODIFIED="1411510589784" TEXT="5. La tramitaci&#xf3;n por la administraci&#xf3;n p&#xfa;blica competente de una medida cautelar que impida o paralice o de una resoluci&#xf3;n que deniegue la instalaci&#xf3;n de la infraestructura de red que cumpla los par&#xe1;metros y requerimientos t&#xe9;cnicos esenciales, excepto en edificaciones del patrimonio hist&#xf3;rico-art&#xed;stico, ser&#xe1; objeto de previo informe preceptivo del MINETUR que dispone del plazo m&#xe1;ximo de un mes para su emisi&#xf3;n y que ser&#xe1; evacuado tras, en su caso, los intentos que procedan de encontrar una soluci&#xf3;n negociada con los &#xf3;rganos encargados de la tramitaci&#xf3;n de la citada medida o resoluci&#xf3;n. Transcurrido dicho plazo, el informe se entender&#xe1; emitido con car&#xe1;cter favorable y podr&#xe1; continuarse con la tramitaci&#xf3;n de la medida o resoluci&#xf3;n. A falta de solicitud del preceptivo informe, as&#xed; como en el supuesto de que el informe no sea favorable, no se podr&#xe1; aprobar la medida o resoluci&#xf3;n."/>
<node CREATED="1410993566015" MODIFIED="1410993566015" TEXT="6. El MINETUR promover&#xe1; con la asociaci&#xf3;n de entidades locales de &#xe1;mbito estatal con mayor implantaci&#xf3;n la elaboraci&#xf3;n de un modelo tipo de declaraci&#xf3;n responsable a que se refiere el apartado 6 del art&#xed;culo anterior."/>
<node CREATED="1410993566016" MODIFIED="1410993566016" TEXT="7. Igualmente, el MINETUR aprobar&#xe1; recomendaciones para la elaboraci&#xf3;n por parte de las AAPP competentes de las normas o instrumentos contemplados en la presente secci&#xf3;n, que podr&#xe1;n contener modelos de ordenanzas municipales elaborados conjuntamente con la asociaci&#xf3;n de entidades locales de &#xe1;mbito estatal con mayor implantaci&#xf3;n. En el caso de municipios se podr&#xe1; reemplazar la solicitud de informe a que se refiere el apartado 2 de este art&#xed;culo por la presentaci&#xf3;n al MINETUR del proyecto de instrumento acompa&#xf1;ado de la declaraci&#xf3;n del Alcalde del municipio acreditando el cumplimiento de dichas recomendaciones."/>
<node CREATED="1410993566018" ID="ID_1761676949" MODIFIED="1411510672649" TEXT="8. El MINETUR podr&#xe1; crear, mediante RD, un punto de informaci&#xf3;n &#xfa;nico a trav&#xe9;s del cual los operadores de comunicaciones electr&#xf3;nicas acceder&#xe1;n por v&#xed;a electr&#xf3;nica a toda la informaci&#xf3;n relativa sobre las condiciones y procedimientos aplicables para la instalaci&#xf3;n y despliegue de redes de comunicaciones electr&#xf3;nicas y sus recursos asociados. Las Comunidades Aut&#xf3;nomas y las Corporaciones Locales podr&#xe1;n, mediante la suscripci&#xf3;n del oportuno convenio de colaboraci&#xf3;n con el MINETUR adherirse al punto de informaci&#xf3;n &#xfa;nico, en cuyo caso, los operadores de comunicaciones electr&#xf3;nicas deber&#xe1;n presentar en formato electr&#xf3;nico a trav&#xe9;s de dicho punto las declaraciones responsables a que se refiere el apartado 6 del art&#xed;culo anterior y permisos de toda &#xed;ndole para ocupar dominio p&#xfa;blico y privado necesario para el despliegue de dichas redes que vayan dirigidas a la respectiva Comunidad Aut&#xf3;noma o Corporaci&#xf3;n Local. El punto de informaci&#xf3;n &#xfa;nico ser&#xe1; gestionado por el MINETUR"/>
</node>
<node CREATED="1410993566020" MODIFIED="1410993566020" TEXT="Art&#xed;culo 36. Previsi&#xf3;n de infraestructuras de comunicaciones electr&#xf3;nicas en proyectos de urbanizaci&#xf3;n y en obras civiles financiadas con recursos p&#xfa;blicos.">
<node CREATED="1410993566021" ID="ID_1723569318" MODIFIED="1411510776833" TEXT="1. Cuando se acometan proyectos de urbanizaci&#xf3;n, el proyecto t&#xe9;cnico de urbanizaci&#xf3;n deber&#xe1; prever la instalaci&#xf3;n de infraestructura de obra civil para facilitar el despliegue de las redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas. Las infraestructuras que se instalen para facilitar el despliegue de las redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas  formar&#xe1;n parte del conjunto resultante de las obras de urbanizaci&#xf3;n y pasar&#xe1;n a integrarse en el dominio p&#xfa;blico municipal. La administraci&#xf3;n p&#xfa;blica titular de dicho dominio p&#xfa;blico pondr&#xe1; tales infraestructuras a disposici&#xf3;n de los operadores interesados. Mediante RD se establecer&#xe1; el dimensionamiento y caracter&#xed;sticas t&#xe9;cnicas m&#xed;nimas que habr&#xe1;n de reunir estas infraestructuras."/>
<node CREATED="1410993566023" ID="ID_273566192" MODIFIED="1411510735272" TEXT="2. En las obras civiles financiadas total o parcialmente con recursos p&#xfa;blicos se prever&#xe1;, en los supuestos y condiciones que se determinen mediante RD, la instalaci&#xf3;n de recursos asociados y otras infraestructuras de obra civil para facilitar el despliegue de las redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas, que se pondr&#xe1;n a disposici&#xf3;n de los operadores interesados"/>
</node>
</node>
<node CREATED="1410813315435" FOLDED="true" ID="ID_689998539" MODIFIED="1411584541793" TEXT="Secci&#xf3;n 3.&#xaa; Acceso a infraestructuras susceptibles de alojar redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas">
<node CREATED="1410813315437" FOLDED="true" ID="ID_297119135" MODIFIED="1411583535124" TEXT="Art&#xed;culo 37. Acceso a las infraestructuras susceptibles de alojar redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas.">
<node CREATED="1410813315438" ID="ID_275323805" MODIFIED="1411582862776" TEXT="1. Las AAPP titulares de infraestructuras susceptibles de ser utilizadas para el despliegue de redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas facilitar&#xe1;n el acceso a dichas infraestructuras, siempre que dicho acceso no comprometa la continuidad y seguridad de la prestaci&#xf3;n de los servicios de car&#xe1;cter p&#xfa;blico que en dichas infraestructuras realiza su titular. El acceso no podr&#xe1; ser otorgado o reconocido mediante procedimientos de licitaci&#xf3;n."/>
<node CREATED="1410813315440" ID="ID_677012233" MODIFIED="1411583142614" TEXT="2. Las entidades o sociedades encargadas de la gesti&#xf3;n de infraestructuras de transporte de competencia estatal, as&#xed; como las empresas y operadores de otros sectores distintos al de las comunicaciones electr&#xf3;nicas que sean titulares o gestoras de infraestructuras en el dominio p&#xfa;blico del Estado, de las Comunidades Aut&#xf3;nomas o de las Entidades Locales o beneficiarias de expropiaciones forzosas y que sean susceptibles de ser utilizadas para el despliegue de redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas facilitar&#xe1;n el acceso. En particular, infraestructuras viarias, ferroviarias, puertos, aeropuertos, abastecimiento de agua, saneamiento, y del transporte y la distribuci&#xf3;n de gas y electricidad."/>
<node CREATED="1410813315442" ID="ID_433463445" MODIFIED="1411583166222" TEXT="3. Por infraestructuras susceptibles de ser utilizadas para el despliegue de redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas se entender&#xe1;n tubos, postes, conductos, cajas, c&#xe1;maras, armarios..."/>
<node CREATED="1410813315444" ID="ID_81284083" MODIFIED="1411583205038" TEXT="4. Mediante RD se determinar&#xe1;n los procedimientos, plazos, requisitos y condiciones en los que se facilitar&#xe1; el acceso."/>
<node CREATED="1410813315445" ID="ID_298054522" MODIFIED="1411583239806" TEXT="5. El MINETUR podr&#xe1; exigir a las AAPP y sus entidades y sociedades, as&#xed; como a las empresas y operadores a que se refieren los dos primeros apartados de este art&#xed;culo, que suministren la informaci&#xf3;n necesaria para elaborar de forma coordinada un inventario detallado que se facilitar&#xe1; a los operadores de redes y servicios de comunicaciones electr&#xf3;nicas."/>
<node CREATED="1410813315447" ID="ID_790507383" MODIFIED="1411583500269" TEXT="6. Las partes negociar&#xe1;n libremente los acuerdos del acceso y sus condiciones, incluidas las contraprestaciones econ&#xf3;micas. Cualquiera de las partes podr&#xe1; presentar un conflicto sobre el acceso y sus condiciones ante la CNMC, la cual, previa audiencia de las partes, dictar&#xe1; resoluci&#xf3;n vinculante en el plazo indicado en la Ley 3/2013, sin perjuicio de que puedan adoptarse medidas provisionales hasta que se dicte la resoluci&#xf3;n definitiva."/>
<node CREATED="1410813315448" ID="ID_799385044" MODIFIED="1411583531269" TEXT="7. Las AAPP titulares de las infraestructuras a las que se hace referencia en este art&#xed;culo tendr&#xe1;n derecho a establecer las compensaciones econ&#xf3;micas que correspondan."/>
</node>
<node CREATED="1410813315449" FOLDED="true" ID="ID_1486396251" MODIFIED="1411584540666" TEXT="Art&#xed;culo 38. Acceso o uso de las redes de comunicaciones electr&#xf3;nicas titularidad de los &#xf3;rganos o entes gestores de infraestructuras de transporte de competencia estatal.">
<node CREATED="1410813315450" ID="ID_1481096228" MODIFIED="1411584207454" TEXT="1. Los &#xf3;rganos o entes pertenecientes a la AGE encargados de la gesti&#xf3;n de infraestructuras de transporte de competencia estatal que presten servicios de comunicaciones electr&#xf3;nicas o comercialicen la explotaci&#xf3;n de redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas, negociar&#xe1;n con los operadores de redes y servicios de comunicaciones electr&#xf3;nicas interesados en el acceso o uso de las redes de comunicaciones electr&#xf3;nicas de las que aquellos sean titulares."/>
<node CREATED="1410813315452" ID="ID_837018737" MODIFIED="1411584487150" TEXT="2. Las condiciones para el acceso o uso de estas redes han de ser equitativas, no discriminatorias, objetivas, transparentes, neutrales y a precios de mercado, siempre que se garantice al menos la recuperaci&#xf3;n de coste de las inversiones y su operaci&#xf3;n y mantenimiento. "/>
<node CREATED="1410813315454" ID="ID_800966245" MODIFIED="1411584535094" TEXT="3. Las partes acordar&#xe1;n libremente los acuerdos del acceso o uso a que se refiere este art&#xed;culo, a partir de las condiciones establecidas en el apartado anterior. Cualquiera de las partes podr&#xe1; presentar un conflicto sobre el acceso y sus condiciones ante la CNMC, la cual, previa audiencia de las partes, dictar&#xe1; resoluci&#xf3;n vinculante."/>
</node>
</node>
</node>
<node CREATED="1410813315455" FOLDED="true" ID="ID_1358771946" MODIFIED="1411589323817" TEXT="CAP&#xcd;TULO III: Secreto de las comunicaciones y protecci&#xf3;n de los datos personales y derechos y obligaciones de car&#xe1;cter p&#xfa;blico vinculados con las redes y servicios de comunicaciones electr&#xf3;nicas">
<node CREATED="1410813315456" FOLDED="true" ID="ID_789954222" MODIFIED="1411586661025" TEXT="Art&#xed;culo 39. Secreto de las comunicaciones.">
<node CREATED="1410813315457" ID="ID_703253979" MODIFIED="1411585022007" TEXT="1. Los operadores  deber&#xe1;n garantizar el secreto de las comunicaciones de conformidad con los art&#xed;culos 18.3 y 55.2 de la Constituci&#xf3;n."/>
<node CREATED="1410813315458" ID="ID_414429288" MODIFIED="1411585207190" TEXT="2. Los operadores est&#xe1;n obligados a realizar las interceptaciones que se autoricen de acuerdo con  la Ley de Enjuiciamiento Criminal, en la Ley Org&#xe1;nica 2/2002 y en otras normas con rango de ley org&#xe1;nica. "/>
<node CREATED="1410813315460" ID="ID_1711063215" MODIFIED="1411585449270" TEXT="3. La interceptaci&#xf3;n deber&#xe1; facilitarse para cualquier comunicaci&#xf3;n que tenga como origen o destino el punto de terminaci&#xf3;n de red o el terminal espec&#xed;fico que se determine a partir de la orden de interceptaci&#xf3;n legal."/>
<node CREATED="1410813315461" ID="ID_1028291167" MODIFIED="1411585614789" TEXT="4. El acceso se facilitar&#xe1; para todo tipo de comunicaciones electr&#xf3;nicas. El acceso facilitado servir&#xe1; tanto para la supervisi&#xf3;n como para la transmisi&#xf3;n a los centros de recepci&#xf3;n de las interceptaciones y permitir&#xe1; obtener la se&#xf1;al con la que se realiza la comunicaci&#xf3;n."/>
<node CREATED="1410813315463" FOLDED="true" ID="ID_1229265148" MODIFIED="1411585997155" TEXT="5. Los sujetos obligados deber&#xe1;n facilitar al agente facultado, los datos indicados en la orden de interceptaci&#xf3;n legal, de entre los que se relacionan a continuaci&#xf3;n:">
<node CREATED="1410813315464" ID="ID_748903144" MODIFIED="1411585918886" TEXT="a) Identidad o identidades del sujeto objeto de la medida de la interceptaci&#xf3;n. Se entiende por identidad: etiqueta t&#xe9;cnica que puede representar el origen o el destino de cualquier tr&#xe1;fico de comunicaciones electr&#xf3;nicas."/>
<node CREATED="1410813315465" MODIFIED="1410813315465" TEXT="b) Identidad o identidades de las otras partes involucradas en la comunicaci&#xf3;n electr&#xf3;nica."/>
<node CREATED="1410813315466" MODIFIED="1410813315466" TEXT="c) Servicios b&#xe1;sicos utilizados."/>
<node CREATED="1410813315466" MODIFIED="1410813315466" TEXT="d) Servicios suplementarios utilizados."/>
<node CREATED="1410813315467" MODIFIED="1410813315467" TEXT="e) Direcci&#xf3;n de la comunicaci&#xf3;n."/>
<node CREATED="1410813315467" MODIFIED="1410813315467" TEXT="f) Indicaci&#xf3;n de respuesta."/>
<node CREATED="1410813315467" MODIFIED="1410813315467" TEXT="g) Causa de finalizaci&#xf3;n."/>
<node CREATED="1410813315468" MODIFIED="1410813315468" TEXT="h) Marcas temporales."/>
<node CREATED="1410813315468" MODIFIED="1410813315468" TEXT="i) Informaci&#xf3;n de localizaci&#xf3;n."/>
<node CREATED="1410813315469" MODIFIED="1410813315469" TEXT="j) Informaci&#xf3;n intercambiada a trav&#xe9;s del canal de control o se&#xf1;alizaci&#xf3;n."/>
</node>
<node CREATED="1410813315469" FOLDED="true" ID="ID_228352352" MODIFIED="1411586127962" TEXT="6. Adem&#xe1;s  los sujetos obligados deber&#xe1;n facilitar al agente facultado:">
<node CREATED="1410813315470" MODIFIED="1410813315470" TEXT="a) Identificaci&#xf3;n de la persona f&#xed;sica o jur&#xed;dica."/>
<node CREATED="1410813315471" ID="ID_1371462179" MODIFIED="1410813315471" TEXT="b) Domicilio en el que el proveedor realiza las notificaciones."/>
<node CREATED="1410813315471" MODIFIED="1410813315471" TEXT="aunque no sea abonado, si el servicio de que se trata permite disponer de alguno de los siguientes:">
<node CREATED="1410813315472" MODIFIED="1410813315472" TEXT="c) N&#xfa;mero de titular de servicio (tanto el n&#xfa;mero de directorio como todas las identificaciones de comunicaciones electr&#xf3;nicas del abonado)."/>
<node CREATED="1410813315474" MODIFIED="1410813315474" TEXT="d) N&#xfa;mero de identificaci&#xf3;n del terminal."/>
<node CREATED="1410813315474" MODIFIED="1410813315474" TEXT="e) N&#xfa;mero de cuenta asignada por el proveedor de servicios Internet."/>
<node CREATED="1410813315474" MODIFIED="1410813315474" TEXT="f) Direcci&#xf3;n de correo electr&#xf3;nico."/>
</node>
</node>
<node CREATED="1410813315475" ID="ID_569656793" MODIFIED="1411586266145" TEXT="7. los sujetos obligados deber&#xe1;n facilita rinformaci&#xf3;n de la situaci&#xf3;n geogr&#xe1;fica del terminal o punto de terminaci&#xf3;n de red origen y destino. En caso de servicios m&#xf3;viles, se proporcionar&#xe1; una posici&#xf3;n lo m&#xe1;s exacta posible y,  la identificaci&#xf3;n, localizaci&#xf3;n y tipo de la estaci&#xf3;n base afectada."/>
<node CREATED="1410813315476" ID="ID_1651823980" MODIFIED="1410813315476" TEXT="8. Los sujetos obligados deber&#xe1;n facilitar al agente facultado, de entre los datos previstos en los apartados 5, 6 y 7 de este art&#xed;culo, s&#xf3;lo aqu&#xe9;llos que est&#xe9;n incluidos en la orden de interceptaci&#xf3;n legal."/>
<node CREATED="1410813315477" ID="ID_1006145615" MODIFIED="1411586337311" TEXT="9. Con car&#xe1;cter previo a la ejecuci&#xf3;n de la orden de interceptaci&#xf3;n legal, los sujetos obligados deber&#xe1;n facilitar al agente facultado informaci&#xf3;n sobre los servicios y caracter&#xed;sticas del sistema de telecomunicaci&#xf3;n que utilizan los sujetos objeto de la medida de la interceptaci&#xf3;n y, si obran en su poder, los correspondientes nombres de los abonados con sus DNI, NIE o NIF."/>
<node CREATED="1410813315478" ID="ID_728640040" MODIFIED="1411586613318" TEXT="10. Los sujetos obligados deber&#xe1;n tener preparadas una o m&#xe1;s interfaces a trav&#xe9;s de las cuales las comunicaciones electr&#xf3;nicas interceptadas y la informaci&#xf3;n relativa a la interceptaci&#xf3;n se transmitir&#xe1;n a los centros de recepci&#xf3;n de las interceptaciones. Las caracter&#xed;sticas de estas interfaces estar&#xe1;n sujetas a las especificaciones t&#xe9;cnicas que se establezcan por el MINETUR."/>
<node CREATED="1410813315480" ID="ID_847917424" MODIFIED="1411586658430" TEXT="11. En el caso de que los sujetos obligados apliquen a las comunicaciones objeto de interceptaci&#xf3;n legal alg&#xfa;n procedimiento de compresi&#xf3;n, cifrado, digitalizaci&#xf3;n o cualquier otro tipo de codificaci&#xf3;n, deber&#xe1;n entregar aquellas desprovistas de los efectos de tales procedimientos."/>
</node>
<node CREATED="1410813315482" FOLDED="true" ID="ID_1642368573" MODIFIED="1411587556561" TEXT="Art&#xed;culo 40. Interceptaci&#xf3;n de las comunicaciones electr&#xf3;nicas por los servicios t&#xe9;cnicos.">
<node CREATED="1410813315483" FOLDED="true" ID="ID_1991640325" MODIFIED="1411587542523" TEXT="1. Cuando para la realizaci&#xf3;n de las tareas de control para la eficaz utilizaci&#xf3;n del dominio p&#xfa;blico radioel&#xe9;ctrico o para la localizaci&#xf3;n de interferencias perjudiciales sea necesaria la utilizaci&#xf3;n de equipos, infraestructuras e instalaciones t&#xe9;cnicas de interceptaci&#xf3;n de se&#xf1;ales no dirigidas al p&#xfa;blico en general, ser&#xe1; de aplicaci&#xf3;n lo siguiente">
<node CREATED="1410813315484" MODIFIED="1410813315484" TEXT="a) La Administraci&#xf3;n de las telecomunicaciones deber&#xe1; dise&#xf1;ar y establecer sus sistemas t&#xe9;cnicos de interceptaci&#xf3;n de se&#xf1;ales en forma tal que se reduzca al m&#xed;nimo el riesgo de afectar a los contenidos de las comunicaciones."/>
<node CREATED="1410813315486" ID="ID_198113743" MODIFIED="1411587541054" TEXT="b) Cuando, como consecuencia de las interceptaciones t&#xe9;cnicas efectuadas, quede constancia de los contenidos, los soportes en los que &#xe9;stos aparezcan deber&#xe1;n ser custodiados hasta la finalizaci&#xf3;n, en su caso, del expediente sancionador que hubiera lugar o, en otro caso, destruidos inmediatamente."/>
</node>
<node CREATED="1410813315487" MODIFIED="1410813315487" TEXT="2. Las mismas reglas se aplicar&#xe1;n para la vigilancia del adecuado empleo de las redes y la correcta prestaci&#xf3;n de los servicios de comunicaciones electr&#xf3;nicas."/>
<node CREATED="1410813315488" MODIFIED="1410813315488" TEXT="3. Lo establecido en este art&#xed;culo se entiende sin perjuicio de las facultades que a la Administraci&#xf3;n atribuye el art&#xed;culo 60."/>
</node>
<node CREATED="1410813315490" FOLDED="true" ID="ID_909842505" MODIFIED="1411587780721" TEXT="Art&#xed;culo 41. Protecci&#xf3;n de los datos de car&#xe1;cter personal.">
<node CREATED="1410813315490" FOLDED="true" ID="ID_1661141790" MODIFIED="1411587678088" TEXT="1. La Agencia Espa&#xf1;ola de Protecci&#xf3;n de Datos, en el ejercicio de su competencia de garant&#xed;a de la seguridad en el tratamiento de datos de car&#xe1;cter personal, podr&#xe1; examinar las medidas adoptadas">
<node CREATED="1410813315492" MODIFIED="1410813315492" TEXT="a) La garant&#xed;a de que s&#xf3;lo el personal autorizado tenga acceso a los datos personales para fines autorizados por la Ley."/>
<node CREATED="1410813315494" MODIFIED="1410813315494" TEXT="b) La protecci&#xf3;n de los datos personales almacenados o transmitidos de la destrucci&#xf3;n accidental o il&#xed;cita, la p&#xe9;rdida o alteraci&#xf3;n accidentales o el almacenamiento, tratamiento, acceso o revelaci&#xf3;n no autorizados o il&#xed;citos."/>
<node CREATED="1410813315495" MODIFIED="1410813315495" TEXT="c) La garant&#xed;a de la aplicaci&#xf3;n efectiva de una pol&#xed;tica de seguridad con respecto al tratamiento de datos personales."/>
</node>
<node CREATED="1410813315496" MODIFIED="1410813315496" TEXT="2. En caso de que exista un riesgo particular de violaci&#xf3;n de la seguridad de la red p&#xfa;blica o del servicio de comunicaciones electr&#xf3;nicas, el operador que explote dicha red o preste el servicio de comunicaciones electr&#xf3;nicas informar&#xe1; a los abonados sobre dicho riesgo y sobre las medidas a adoptar."/>
<node CREATED="1410813315497" ID="ID_311824016" MODIFIED="1411587779006" TEXT="3. En caso de violaci&#xf3;n de los datos personales, el operador de servicios de comunicaciones electr&#xf3;nicas disponibles al p&#xfa;blico notificar&#xe1; sin dilaciones indebidas dicha violaci&#xf3;n a la Agencia Espa&#xf1;ola de Protecci&#xf3;n de Datos. Si la violaci&#xf3;n de los datos pudiera afectar negativamente a la intimidad o a los datos personales de un abonado o particular, el operador notificar&#xe1; tambi&#xe9;n la violaci&#xf3;n al abonado o particular sin dilaciones indebidas.  Los operadores deber&#xe1;n llevar un inventario de las violaciones de los datos personales."/>
<node CREATED="1410813315501" MODIFIED="1410813315501" TEXT="4. Lo dispuesto en el presente art&#xed;culo ser&#xe1; sin perjuicio de la aplicaci&#xf3;n de la Ley Org&#xe1;nica 15/1999 y su normativa de desarrollo."/>
</node>
<node CREATED="1410813315502" FOLDED="true" ID="ID_896649014" MODIFIED="1411587451287" TEXT="Art&#xed;culo 42. Conservaci&#xf3;n y cesi&#xf3;n de datos relativos a las comunicaciones electr&#xf3;nicas y a las redes p&#xfa;blicas de comunicaciones.">
<node CREATED="1410813315503" ID="ID_933120398" MODIFIED="1411586839221" TEXT="Se rige por Ley 25/2007 de conservaci&#xf3;n de datos relativos a las comunicaciones electr&#xf3;nicas y a las redes p&#xfa;blicas de comunicaciones."/>
</node>
<node CREATED="1410813315505" FOLDED="true" ID="ID_467430280" MODIFIED="1411587406889" TEXT="Art&#xed;culo 43. Cifrado en las redes y servicios de comunicaciones electr&#xf3;nicas.">
<node CREATED="1410813315505" ID="ID_1508783203" MODIFIED="1410813315505" TEXT="1. Cualquier tipo de informaci&#xf3;n que se transmita por redes de comunicaciones electr&#xf3;nicas podr&#xe1; ser protegida mediante procedimientos de cifrado."/>
<node CREATED="1410813315506" ID="ID_1073756882" MODIFIED="1411587370360" TEXT="2. Condiciones de uso: facilitar a un &#xf3;rgano de la AGE o a un organismo p&#xfa;blico, los algoritmos o  procedimiento de cifrado utilizado, as&#xed; como la obligaci&#xf3;n de facilitar sin coste alguno los aparatos de cifra."/>
</node>
<node CREATED="1410813315508" FOLDED="true" ID="ID_1595183208" MODIFIED="1411587427104" TEXT="Art&#xed;culo 44. Integridad y seguridad de las redes y de los servicios de comunicaciones electr&#xf3;nicas.">
<node CREATED="1410798331582" ID="ID_1012057505" MODIFIED="1410798740732" TEXT="Los operadores gestionar&#xe1;n riesgos de seguridad y garantizar&#xe1;n la integridad de las comunicaciones."/>
<node CREATED="1410798770112" ID="ID_893089241" MODIFIED="1410798881485" TEXT="Los operadores notificar&#xe1;n al MIET las violaciones de la seguridad o p&#xe9;rdidas de integridad que hayan tenido un impacto significativo en la explotaci&#xf3;n de las redes o los servicios. Una vez al a&#xf1;o, el MIET presentar&#xe1; a la Comisi&#xf3;n y a la ENISA un informe sobre las notificaciones recibidas y las medidas adoptadas."/>
<node CREATED="1410799302693" ID="ID_565346181" MODIFIED="1410799488936" TEXT="El MIET comunicar&#xe1; a la Secretar&#xed;a de Estado de Seguridad del Ministerio del Interior los incidentes que sean de inter&#xe9;s para la mejora de la protecci&#xf3;n de infraestructuras cr&#xed;ticas, en el marco de la Ley 8/2011. El MIET comunicar&#xe1; a la CNMC las violaciones de la seguridad o p&#xe9;rdidas de integridad que afecten a las obligaciones  impuestas por dila CNMC en los mercados de referencia"/>
<node CREATED="1410799598366" ID="ID_511632996" MODIFIED="1410799628960" TEXT="El MIET podr&#xe1; imponer:  a) La obligaci&#xf3;n de facilitar la informaci&#xf3;n necesaria para evaluar la seguridad y la integridad de sus servicios y redes, incluidos los documentos sobre las pol&#xed;ticas de seguridad.  b) La obligaci&#xf3;n de someterse a una auditor&#xed;a de seguridad realizada por un organismo independiente o por una autoridad competente. "/>
</node>
</node>
<node CREATED="1410813315519" FOLDED="true" ID="ID_797277125" MODIFIED="1411589320936" TEXT="CAP&#xcd;TULO IV: Infraestructuras comunes y redes de comunicaciones electr&#xf3;nicas en los edificios">
<node CREATED="1411588291015" ID="ID_1947343695" MODIFIED="1411588291015" TEXT="Art&#xed;culo 45. Infraestructuras comunes y redes de comunicaciones electr&#xf3;nicas en los edificios.">
<node CREATED="1410799701111" ID="ID_229009154" MODIFIED="1410799783257" TEXT="Mediante RD se determinar&#xe1; el punto de interconexi&#xf3;n de la red interior con las redes p&#xfa;blicas, y las condiciones aplicables a la red interior."/>
<node CREATED="1410799696584" ID="ID_1697666145" MODIFIED="1410799867871" TEXT="La infraestructura de obra civil dispondr&#xe1; de capacidad suficiente para permitir el paso de las redes de los distintos operadores y facilitar&#xe1; la posibilidad de uso compartido de infraestructuras. "/>
<node CREATED="1410801900160" ID="ID_1249904919" MODIFIED="1410802854422" TEXT="Las ICCEs promover&#xe1;n la implantaci&#xf3;n del hogar digital en Espa&#xf1;a."/>
<node CREATED="1410802855244" ID="ID_289144112" MODIFIED="1410806446905" TEXT="Redes fijas de comunicaciones electr&#xf3;nicas de acceso ultrarr&#xe1;pido:si no hay ICCEs se podr&#xe1;n emplear los elemento comunes de la edificaci&#xf3;n. Si no fuera posible la instalaci&#xf3;n podr&#xe1; realizarse utilizando las fachadas de las edificaciones. El operador deber&#xe1; comunicarlo por escrito a la comunidad de propietarios o al propietario del edificio junto con el proyecto de actuaci&#xf3;n (formato, contenido, y plazos formales de presentaci&#xf3;n se determinar&#xe1;n reglamentariamente). (Ver procedimiento de inicio de instalaci&#xf3;n en la Ley). "/>
<node CREATED="1410806460107" ID="ID_1284520189" MODIFIED="1410806504559" TEXT="El MIET podr&#xe1; imponer a los operadores obligaciones objetivas, transparentes, proporcionadas y no discriminatorias relativas a la utilizaci&#xf3;n compartida de los tramos finales de las redes de acceso cuando la duplicaci&#xf3;n de esta infraestructura sea econ&#xf3;micamente ineficiente o f&#xed;sicamente inviable."/>
</node>
</node>
<node CREATED="1410813315520" FOLDED="true" ID="ID_1200856520" MODIFIED="1411590558929" TEXT="CAP&#xcd;TULO V:  Derechos de los usuarios finales">
<node CREATED="1411588678877" FOLDED="true" ID="ID_144711912" MODIFIED="1411589317576" TEXT="Art&#xed;culo 46. Derechos de los usuarios finales de servicios de comunicaciones electr&#xf3;nicas.">
<node CREATED="1411588678878" ID="ID_1786952957" MODIFIED="1411588777822" TEXT="1. Son titulares de los derechos espec&#xed;ficos reconocidos en este Cap&#xed;tulo, los usuarios finales de servicios de comunicaciones electr&#xf3;nicas sin perjuicio de los derechos que otorga a los consumidores el texto refundido de la Ley General para la Defensa de los Consumidores y Usuarios aprobado por el real decreto Legislativo 1/200."/>
<node CREATED="1411588678880" ID="ID_439898689" MODIFIED="1411588821358" TEXT="2. En aquellos aspectos expresamente previstos en las disposiciones del derecho de la UE ser&#xe1;n de aplicaci&#xf3;n preferente"/>
</node>
<node CREATED="1411588842882" FOLDED="true" ID="ID_1577904541" MODIFIED="1411589689160" TEXT="Art&#xed;culo 47. Derechos espec&#xed;ficos de los usuarios finales de redes y servicios de comunicaciones electr&#xf3;nicas disponibles al p&#xfa;blico.">
<node CREATED="1411588842883" FOLDED="true" ID="ID_966023119" MODIFIED="1411588999528" TEXT="1. Se establecer&#xe1;n por RD que regular&#xe1;:">
<node CREATED="1410807302117" ID="ID_1956109769" MODIFIED="1410807362310" TEXT="a) El derecho a celebrar contratos as&#xed; como el contenido m&#xed;nimo de dichos contratos."/>
<node CREATED="1410807364099" ID="ID_1355372216" MODIFIED="1410807460929" TEXT="b) El derecho a resolver el contrato en cualquier momento (incluye resolverlo anticipadamente y sin penalizaci&#xf3;n en el supuesto de modificaci&#xf3;n de las condiciones contractuales)."/>
<node CREATED="1410807462366" ID="ID_1070184863" MODIFIED="1410807763592" TEXT=" c) El derecho al cambio de operador, con conservaci&#xf3;n de los n&#xfa;meros del plan nacional de numeraci&#xf3;n telef&#xf3;nica en los supuestos en que as&#xed; se contemple en el plazo m&#xe1;ximo de un d&#xed;a laborable. No se podr&#xe1; transferir a los usuarios finales a otro operador en contra de su voluntad.  El proceso de cambio es dirigido por el operador receptor (ampliar con la Ley). "/>
<node CREATED="1410807766524" ID="ID_694681459" MODIFIED="1410807851042" TEXT="d) El derecho a la informaci&#xf3;n, que deber&#xe1; ser veraz, eficaz, suficiente, transparente, comparable, sobre los servicios de comunicaciones electr&#xf3;nicas disponibles al p&#xfa;blico. "/>
<node CREATED="1410807856432" ID="ID_169873648" MODIFIED="1410807888627" TEXT=" e) Los supuestos, plazos y condiciones en que el usuario, previa solicitud, podr&#xe1; ejercer el derecho de desconexi&#xf3;n de determinados servicios, contempl&#xe1;ndose la necesidad de petici&#xf3;n expresa para el acceso a servicios de distinta consideraci&#xf3;n. "/>
<node CREATED="1410807890105" ID="ID_1548326117" MODIFIED="1410807908940" TEXT=" f) El derecho a la continuidad del servicio, y a obtener una compensaci&#xf3;n autom&#xe1;tica por su interrupci&#xf3;n, en los supuestos que se determinen mediante RD.  "/>
<node CREATED="1410807910265" ID="ID_1517373639" MODIFIED="1410807981761" TEXT="g) Los supuestos de aprobaci&#xf3;n por parte del MIET de las condiciones generales de los contratos, entre los que se incluir&#xe1;n los celebrados con operadores con obligaciones de servicio p&#xfa;blico. "/>
<node CREATED="1410807982527" ID="ID_1612146365" MODIFIED="1410808066020" TEXT="h) El derecho a recibir informaci&#xf3;n completa, comparable, pertinente, fiable, actualizada y de f&#xe1;cil consulta sobre la calidad de los servicios de comunicaciones electr&#xf3;nicas disponibles al p&#xfa;blico y sobre las medidas adoptadas para garantizar un acceso equivalente para los usuarios finales con discapacidad."/>
<node CREATED="1410808067514" ID="ID_954417332" MODIFIED="1410808088525" TEXT=" i) El derecho a elegir un medio de pago entre los com&#xfa;nmente utilizados en el tr&#xe1;fico comercial. "/>
<node CREATED="1410808089538" ID="ID_1400497056" MODIFIED="1410808098988" TEXT=" j) El derecho a acceder a los servicios de emergencias de forma gratuita sin tener que utilizar ning&#xfa;n medio de pago. "/>
<node CREATED="1410808100129" ID="ID_636422465" MODIFIED="1410808111621" TEXT=" k) El derecho a la facturaci&#xf3;n detallada, clara y sin errores, sin perjuicio del derecho a recibir facturas no desglosadas a petici&#xf3;n del usuario.  "/>
<node CREATED="1410808112370" ID="ID_127618877" MODIFIED="1410808124941" TEXT="l) El derecho a detener el desv&#xed;o autom&#xe1;tico de llamadas efectuado a su terminal por parte de un tercero.  "/>
<node CREATED="1410808125410" ID="ID_280053026" MODIFIED="1410808172468" TEXT="m) El derecho a impedir, mediante un procedimiento sencillo y gratuito, la presentaci&#xf3;n de la identificaci&#xf3;n de su l&#xed;nea en las llamadas que genere o la presentaci&#xf3;n de la identificaci&#xf3;n de su l&#xed;nea al usuario que le realice una llamada (no aplica para llamadas al 112). Por un per&#xed;odo de tiempo limitado, los usuarios finales no podr&#xe1;n ejercer este derecho cuando el abonado a la l&#xed;nea de destino haya solicitado la identificaci&#xf3;n de las llamadas maliciosas o molestas realizadas a su l&#xed;nea.  "/>
<node CREATED="1410808173425" ID="ID_630999994" MODIFIED="1410808221025" TEXT="n) El derecho a impedir, mediante un procedimiento sencillo y gratuito, la presentaci&#xf3;n de la identificaci&#xf3;n de la l&#xed;nea de origen en las llamadas entrantes y a rechazar las llamadas entrantes en que dicha l&#xed;nea no aparezca identificada. "/>
</node>
<node CREATED="1411588842887" ID="ID_347615743" MODIFIED="1411588842887" TEXT="2. Los operadores deber&#xe1;n disponer de un servicio de atenci&#xf3;n al cliente, gratuito para los usuarios, que tenga por objeto facilitar informaci&#xf3;n y atender y resolver las quejas y reclamaciones de sus clientes. Los servicios de atenci&#xf3;n al cliente mediante el canal telef&#xf3;nico deber&#xe1;n garantizar una atenci&#xf3;n personal directa, m&#xe1;s all&#xe1; de la posibilidad de utilizar complementariamente otros medios t&#xe9;cnicos a su alcance para mejorar dicha atenci&#xf3;n. Los operadores pondr&#xe1;n a disposici&#xf3;n de sus clientes m&#xe9;todos para la acreditaci&#xf3;n documental de las gestiones o reclamaciones realizadas, como el otorgamiento de un n&#xfa;mero de referencia o la posibilidad de enviar al cliente un documento en soporte duradero."/>
</node>
<node CREATED="1411589073298" FOLDED="true" ID="ID_173233996" MODIFIED="1411589685936" TEXT="Art&#xed;culo 48. Derecho a la protecci&#xf3;n de datos personales y la privacidad en relaci&#xf3;n con las comunicaciones no solicitadas, con los datos de tr&#xe1;fico y de localizaci&#xf3;n y con las gu&#xed;as de abonados.">
<node CREATED="1411589073302" ID="ID_1043971600" MODIFIED="1411589243710" TEXT="1. En relaci&#xf3;n con las comunicaciones no solicitadas:">
<node CREATED="1411589073305" MODIFIED="1411589073305" TEXT="a) A no recibir llamadas autom&#xe1;ticas sin intervenci&#xf3;n humana o mensajes de fax, con fines de comunicaci&#xf3;n comercial sin haber prestado su consentimiento previo e informado para ello."/>
<node CREATED="1411589073307" MODIFIED="1411589073307" TEXT="b) A oponerse a recibir llamadas no deseadas con fines de comunicaci&#xf3;n comercial que se efect&#xfa;en mediante sistemas distintos de los establecidos en la letra anterior y a ser informado de este derecho."/>
</node>
<node CREATED="1411589073308" FOLDED="true" ID="ID_1403519770" MODIFIED="1411589234285" TEXT="2.en relaci&#xf3;n con los datos de tr&#xe1;fico y los datos de localizaci&#xf3;n distintos de los datos de tr&#xe1;fico:">
<node CREATED="1410808539262" ID="ID_56010246" MODIFIED="1411502297122" TEXT="a) A que se hagan an&#xf3;nimos o se cancelen sus datos de tr&#xe1;fico cuando ya no sean necesarios a los efectos de la transmisi&#xf3;n de una comunicaci&#xf3;n (los necesarios a efectos de facturaci&#xf3;n hasta que haya expirado el plazo para la impugnaci&#xf3;n de la factura del servicio, para la devoluci&#xf3;n del cargo efectuado por el operador, para el pago de la factura o para que el operador pueda exigir su pago). (Sin perjuicio de las obligaciones establecidas en la Ley 25/2007, de 18 de octubre, de conservaci&#xf3;n de datos relativos a las comunicaciones electr&#xf3;nicas y a las redes p&#xfa;blicas de comunicaciones)."/>
<node CREATED="1410808633904" ID="ID_1352599485" MODIFIED="1410808768247" TEXT=" b) A que sus datos de tr&#xe1;fico sean utilizados para promoci&#xf3;n comercial de servicios de comunicaciones electr&#xf3;nicas o para la prestaci&#xf3;n de servicios de valor a&#xf1;adido &#xfa;nicamente cuando hubieran prestado su consentimiento informado para ello. Los usuarios finales dispondr&#xe1;n del derecho de retirar su consentimiento para el tratamiento de los datos de tr&#xe1;fico en cualquier momento y con efecto inmediato.  "/>
<node CREATED="1410808649049" ID="ID_1438565207" MODIFIED="1411502311228" TEXT="c) A que s&#xf3;lo se proceda al tratamiento de sus datos de localizaci&#xf3;n distintos a los datos de tr&#xe1;fico cuando se hayan hecho an&#xf3;nimos o previo su consentimiento informado y &#xfa;nicamente en la medida y por el tiempo necesarios para la prestaci&#xf3;n (servicios de valor a&#xf1;adido). Derecho de retirar su consentimiento en cualquier momento y con efecto inmediato  excepto para 112 o comunicaciones efectuadas a entidades que presten servicios de llamadas de urgencia que se determinen por el MIET.  (Sin perjuicio de las obligaciones establecidas en la Ley 25/2007, de 18 de octubre, de conservaci&#xf3;n de datos relativos a las comunicaciones electr&#xf3;nicas y a las redes p&#xfa;blicas de comunicaciones)."/>
</node>
<node CREATED="1411589073315" ID="ID_226062008" MODIFIED="1411589261846" TEXT="3. en relaci&#xf3;n con las gu&#xed;as de abonados:">
<node CREATED="1411502127610" ID="ID_588136426" MODIFIED="1411502166168" TEXT="a) A figurar en las gu&#xed;as de abonados."/>
<node CREATED="1411502167780" ID="ID_564413327" MODIFIED="1411502181616" TEXT=" b) A ser informados gratuitamente de la inclusi&#xf3;n de sus datos en las gu&#xed;as, as&#xed; como de la finalidad de las mismas, con car&#xe1;cter previo a dicha inclusi&#xf3;n. "/>
<node CREATED="1411502182900" ID="ID_726347122" MODIFIED="1411502279957" TEXT=" c) A no figurar en las gu&#xed;as o a solicitar la omisi&#xf3;n de algunos de sus datos."/>
</node>
<node CREATED="1411589073320" ID="ID_709800853" MODIFIED="1411589310894" TEXT="4. Lo establecido en las letras a) y c) del apartado 2 de este art&#xed;culo se entiende sin perjuicio de las obligaciones establecidas en la Ley 25/2007."/>
</node>
<node CREATED="1411589712521" FOLDED="true" ID="ID_1730251723" MODIFIED="1411589773784" TEXT="Art&#xed;culo 49. Gu&#xed;as de abonados.">
<node CREATED="1411502344905" ID="ID_826945324" MODIFIED="1411502420085" TEXT="1. La elaboraci&#xf3;n y comercializaci&#xf3;n en r&#xe9;gimen de libre competencia.  "/>
<node CREATED="1411502421866" ID="ID_213784123" MODIFIED="1411502494613" TEXT="2. Se garantiza el acceso de los usuarios finales a los servicios de informaci&#xf3;n sobre n&#xfa;meros de abonados, para cuya consecuci&#xf3;n el MIET podr&#xe1; imponer obligaciones y condiciones objetivas, equitativas, no discriminatorias y transparentes a las empresas que prestan de servicios de informaci&#xf3;n sobre n&#xfa;meros de abonado.  "/>
<node CREATED="1411502495674" ID="ID_961922272" MODIFIED="1411502520031" TEXT="3. El MIET adoptar&#xe1;, siempre que sea t&#xe9;cnica y econ&#xf3;micamente posible, medidas para garantizar el acceso directo de los usuarios finales al servicio de informaci&#xf3;n sobre n&#xfa;meros de abonados de otro pa&#xed;s comunitario mediante llamada vocal o SMS."/>
</node>
<node CREATED="1411589712522" FOLDED="true" ID="ID_210671717" MODIFIED="1411589774928" TEXT="Art&#xed;culo 50. Calidad de servicio.">
<node CREATED="1411502543050" ID="ID_1741497598" MODIFIED="1411502677406" TEXT="1. Por Orden del MIET se podr&#xe1;n fijar requisitos m&#xed;nimos de QoS de acuerdo con los procedimientos que se establezcan mediante RD.  El MIET facilitar&#xe1; los requisitos a la Comisi&#xf3;n Europea y al ORECE (Organismo de Reguladores Europeos de las Comunicaciones Electr&#xf3;nicas).  "/>
<node CREATED="1411502612992" ID="ID_1864139292" MODIFIED="1411502721620" TEXT="2. Asimismo, se podr&#xe1;n establecer: los par&#xe1;metros de calidad que habr&#xe1;n de cuantificarse y los mecanismos de certificaci&#xf3;n de la calidad."/>
</node>
<node CREATED="1411589712522" FOLDED="true" ID="ID_1225686097" MODIFIED="1411589809727" TEXT="Art&#xed;culo 51. Acceso a n&#xfa;meros o servicios.">
<node CREATED="1411502749768" ID="ID_343431783" MODIFIED="1411502809705" TEXT=" 1. Mediante RD  o en los Planes Nacionales de numeraci&#xf3;n, direccionamiento y denominaci&#xf3;n y sus disposiciones de desarrollo, podr&#xe1;n establecerse requisitos sobre capacidades o funcionalidades m&#xed;nimas que deber&#xe1;n cumplir determinados tipos de servicios.  "/>
<node CREATED="1411502810518" ID="ID_1021391842" MODIFIED="1411503041951" TEXT="2. Mediante RD, previo informe de la CNMC se establecer&#xe1;n las condiciones en las que los operadores lleven a cabo el bloqueo de acceso a n&#xfa;meros o servicios (por tr&#xe1;fico no permitido o irregular con fines fraudulentos). La CNMC podr&#xe1; ordenar el bloqueo en caso de conflicto entre operadores en materia de acceso o interconexi&#xf3;n. (Esto NO aplica a servicios de la Sociedad de la Informaci&#xf3;n regulados en la Ley 34/2002).  "/>
<node CREATED="1411503043876" ID="ID_415307972" MODIFIED="1411503139689" TEXT="3. Mediante Resoluci&#xf3;n el Secretario de Estado de Telecomunicaciones y para la Sociedad de la Informaci&#xf3;n podr&#xe1; establecer que, algunos n&#xfa;meros o rangos de numeraci&#xf3;n s&#xf3;lo sean accesibles previa petici&#xf3;n expresa del usuario (protecci&#xf3;n de los derechos de los usuarios sobre facturaci&#xf3;n y  tarifas que se aplican en la prestaci&#xf3;n de determinados servicios). "/>
</node>
<node CREATED="1411589712523" FOLDED="true" ID="ID_759690995" MODIFIED="1411589884649" TEXT="Art&#xed;culo 52. Regulaci&#xf3;n de las condiciones b&#xe1;sicas de acceso por personas con discapacidad.">
<node CREATED="1411589712523" FOLDED="true" ID="ID_965360019" MODIFIED="1411589883240" TEXT="Mediante RD se podr&#xe1;n establecer las condiciones b&#xe1;sicas para el acceso de las personas con discapacidad:">
<node CREATED="1411589712524" MODIFIED="1411589712524" TEXT="a) Puedan tener un acceso a servicios de comunicaciones electr&#xf3;nicas equivalente al que disfrutan la mayor&#xed;a de los usuarios finales."/>
<node CREATED="1411589712524" MODIFIED="1411589712524" TEXT="b) Se beneficien de la posibilidad de elecci&#xf3;n de empresa y servicios disponible para la mayor&#xed;a de usuarios finales."/>
</node>
</node>
<node CREATED="1411589712524" FOLDED="true" ID="ID_525225146" MODIFIED="1411590350216" TEXT="Art&#xed;culo 53. Contratos.">
<node CREATED="1411589712524" FOLDED="true" ID="ID_1264599607" MODIFIED="1411590348536" TEXT="1. Antes de la celebraci&#xf3;n de un contrato entre usuarios finales y los operadores estos proporcionar&#xe1;n a los usuarios finales al menos la informaci&#xf3;n que a estos efectos se establece en el Texto Refundido de la Ley General para la Defensa de los Consumidores y Usuarios aprobado por el RDL 1/2007. Adem&#xe1;s los operadores proporcionar&#xe1;n, antes de la celebraci&#xf3;n del contrato, la informaci&#xf3;n espec&#xed;fica que se establezca mediante RD, y al menos:">
<node CREATED="1411589712524" MODIFIED="1411589712524" TEXT="a) Descripci&#xf3;n de los servicios a proveer y posibles limitaciones en su uso."/>
<node CREATED="1411589712524" MODIFIED="1411589712524" TEXT="b) Los precios y tarifas aplicables, con los conceptos y detalles que se establezcan mediante real decreto."/>
<node CREATED="1411589712524" MODIFIED="1411589712524" TEXT="c) Duraci&#xf3;n de los contratos y causas para su resoluci&#xf3;n."/>
<node CREATED="1411589712524" MODIFIED="1411589712524" TEXT="d) Informaci&#xf3;n sobre restricciones impuestas en cuanto a las posibilidades de utilizar el equipo terminal suministrado."/>
<node CREATED="1411589712524" MODIFIED="1411589712524" TEXT="e) Condiciones aplicables en relaci&#xf3;n con la conservaci&#xf3;n de n&#xfa;meros."/>
</node>
<node CREATED="1411589712524" FOLDED="true" ID="ID_1648148283" MODIFIED="1411590347145" TEXT="2. El contenido de los contratos disponibles al p&#xfa;blico se regular&#xe1; mediante RD, e incluir&#xe1;:">
<node CREATED="1411589712524" ID="ID_1570257760" MODIFIED="1411589712524" TEXT="a) Los servicios prestados, incluyendo, en particular:">
<node CREATED="1411506597600" ID="ID_620940354" MODIFIED="1411506770653" TEXT=" i) Si se facilita o no el acceso a los servicios de emergencia e informaci&#xf3;n la ubicaci&#xf3;n de las personas que efect&#xfa;an la llamada.  &#xa;"/>
<node CREATED="1411506612160" ID="ID_54823422" MODIFIED="1411506798051" TEXT="ii) Cualquier condici&#xf3;n que limite el acceso o la utilizaci&#xf3;n de los servicios y las aplicaciones.  "/>
<node CREATED="1411506621016" ID="ID_721454936" MODIFIED="1411506831409" TEXT="iii) Los niveles m&#xed;nimos de calidad de servicio que se ofrecen (plazo para la conexi&#xf3;n inicial u otros)."/>
<node CREATED="1411506660390" ID="ID_1637915188" MODIFIED="1411507097584" TEXT="iv) Informaci&#xf3;n sobre procedimiento para medir y gestionar el tr&#xe1;fico y c&#xf3;mo puede afectar a la QoS.  "/>
<node CREATED="1411506668741" ID="ID_1279809342" MODIFIED="1411507167767" TEXT=" vi)  Restricciones del proveedor sobre el uso del equipo terminal suministrado.  "/>
<node CREATED="1411506840477" ID="ID_1308590028" MODIFIED="1411507759894" TEXT="v) Los tipos de mantenimiento ofrecidos y los servicios de apoyo facilitados al cliente, as&#xed; como los medios para entrar en contacto con dichos servicios. "/>
</node>
<node CREATED="1411589712525" MODIFIED="1411589712525" TEXT="b) La decisi&#xf3;n del abonado acerca de la posibilidad de incluir o no sus datos personales en una gu&#xed;a determinada y los datos de que se trate."/>
<node CREATED="1411589712525" MODIFIED="1411589712525" TEXT="c) La duraci&#xf3;n del contrato y las condiciones para su renovaci&#xf3;n y para la terminaci&#xf3;n de los servicios y la resoluci&#xf3;n del contrato, incluidos:">
<node CREATED="1411589712525" MODIFIED="1411589712525" TEXT="i) Cualquier uso o duraci&#xf3;n m&#xed;nimos u otros requisitos requeridos para aprovechar las promociones."/>
<node CREATED="1411589712525" MODIFIED="1411589712525" TEXT="ii) Todos los gastos relacionados con la conservaci&#xf3;n del n&#xfa;mero y otros identificadores."/>
<node CREATED="1411589712525" MODIFIED="1411589712525" TEXT="iii) Todos los gastos relacionados con la resoluci&#xf3;n del contrato, incluida la recuperaci&#xf3;n de costes relacionada con los equipos terminales."/>
<node CREATED="1411589712525" MODIFIED="1411589712525" TEXT="iv) Las condiciones en las que en los supuestos de cambio de operador con conservaci&#xf3;n de n&#xfa;meros, el operador cedente se comprometa, en su caso, a reembolsar cualquier cr&#xe9;dito restante en las tarjetas prepago."/>
</node>
<node CREATED="1411589712525" MODIFIED="1411589712525" TEXT="d) El modo de iniciar los procedimientos de resoluci&#xf3;n de litigios, de conformidad con el art&#xed;culo 55."/>
<node CREATED="1411589712526" MODIFIED="1411589712526" TEXT="e) Los tipos de medidas que podr&#xed;a tomar la empresa en caso de incidentes de seguridad o integridad o de amenazas y vulnerabilidad."/>
</node>
<node CREATED="1411589712526" ID="ID_1311709832" MODIFIED="1411590343094" TEXT="3. Mediante RD podr&#xe1; establecerse la obligatoriedad de que los contratos incluyan informaci&#xf3;n sobre uso de las redes y servicios para desarrollar actividades il&#xed;citas o para difundir contenidos nocivos, medios de protecci&#xf3;n frente a riesgos para la seguridad personal, la privacidad y los datos personales. "/>
<node CREATED="1411589712526" MODIFIED="1411589712526" TEXT="4. Los operadores deber&#xe1;n entregar o remitir a los usuarios por escrito o en cualquier otro soporte duradero el contrato celebrado."/>
</node>
<node CREATED="1411589712526" FOLDED="true" ID="ID_1716529048" MODIFIED="1411590554243" TEXT="Art&#xed;culo 54. Transparencia y publicaci&#xf3;n de informaci&#xf3;n.">
<node CREATED="1411589712526" ID="ID_960225003" MODIFIED="1411590392846" TEXT="  1. Operadores publicar&#xe1;n informaci&#xf3;n transparente, comparable, adecuada y actualizada sobre los precios y tarifas, gastos y condiciones de la terminaci&#xf3;n de los contratos, acceso y la utilizaci&#xf3;n de los servicios (condiciones establecidas mediante RD). "/>
<node CREATED="1411589712526" ID="ID_885423015" MODIFIED="1411590413910" TEXT="2. El MIET fomentar&#xe1; la divulgaci&#xf3;n de informaci&#xf3;n comparable para que los usuarios finales puedan hacer una evaluaci&#xf3;n del coste de las alternativas y regular&#xe1; las condiciones para que la informaci&#xf3;n publicada por los operadores pueda ser utilizada gratuitamente por terceros, con el fin de vender o permitir la utilizaci&#xf3;n de estas gu&#xed;as interactivas o t&#xe9;cnicas similares. "/>
<node CREATED="1411589712527" ID="ID_94168553" MODIFIED="1411589712527" TEXT="3. Mediante RD se regular&#xe1;n las condiciones para garantizar que los operadores:">
<node CREATED="1411510503683" ID="ID_135429225" MODIFIED="1411510673897" TEXT="a) tarifas de n&#xfa;mero o servicio sujetos a condiciones de precios espec&#xed;ficas, pudi&#xe9;ndose exigir que dicha informaci&#xf3;n se facilite inmediatamente antes de efectuar las llamadas. "/>
<node CREATED="1411510511811" ID="ID_1067884695" MODIFIED="1411510685319" TEXT="b) todo cambio de acceso a los servicios de emergencia o a la informaci&#xf3;n relativa a la ubicaci&#xf3;n de las personas que efect&#xfa;an las llamadas."/>
<node CREATED="1411510519931" ID="ID_780703856" MODIFIED="1411510658260" TEXT="c) los cambios en las condiciones que limiten el acceso o la utilizaci&#xf3;n de los servicios y las aplicaciones.  "/>
<node CREATED="1411510526506" ID="ID_1216322872" MODIFIED="1411510722731" TEXT="d) cualquier procedimiento establecido por el proveedor para medir y gestionar el tr&#xe1;fico de forma que se evite agotar o saturar el enlace de la red y sobre la manera en que esos procedimientos pueden afectar la QoS. "/>
<node CREATED="1411510536074" ID="ID_1517692025" MODIFIED="1411510736184" TEXT="e) su derecho a decidir si incluyen sus datos personales en una gu&#xed;a y los tipos de datos de que se trata. "/>
<node CREATED="1411510543114" ID="ID_870316634" MODIFIED="1411510747174" TEXT=" f) de forma peri&#xf3;dica y detallada a los abonados con discapacidad de los productos y servicios dirigidos a ellos.  "/>
</node>
<node CREATED="1411589712527" ID="ID_1044406571" MODIFIED="1411590509701" TEXT="4. El MINETUR podr&#xe1; exigir a los operadores que difundan de forma gratuita, y en un determinado formato, informaci&#xf3;n de inter&#xe9;s p&#xfa;blico a los antiguos y nuevos abonados sobre::">
<node CREATED="1411510449354" ID="ID_1838417950" MODIFIED="1411510841488" TEXT="a) Los usos m&#xe1;s comunes de los servicios para desarrollar actividades il&#xed;citas o para difundir contenidos nocivos (que atenten contra los derechos y libertades de terceros y los derechos de autor y derechos afines), as&#xed; como sus consecuencias jur&#xed;dicas. "/>
<node CREATED="1411510463154" ID="ID_1009029954" MODIFIED="1411510464453" TEXT="b) Los medios de protecci&#xf3;n contra los riesgos para la seguridad personal, la privacidad, y los datos de car&#xe1;cter personal en el uso de los servicios de comunicaciones electr&#xf3;nicas. "/>
</node>
<node CREATED="1411589712528" ID="ID_1485148581" MODIFIED="1411590552806" TEXT="5. El MINETUR publicar&#xe1; peri&#xf3;dicamente los datos resultantes de la gesti&#xf3;n del procedimiento de resoluci&#xf3;n de controversias con un  nivel de desagregaci&#xf3;n que permita obtener informaci&#xf3;n acerca de los servicios, materias y operadores sobre los que versan las reclamaciones recibidas."/>
</node>
<node CREATED="1411589712528" FOLDED="true" ID="ID_1845162787" MODIFIED="1411590555776" TEXT="Art&#xed;culo 55. Resoluci&#xf3;n de controversias">
<node CREATED="1411507676704" ID="ID_17402492" MODIFIED="1411511023008" TEXT=" 1. Los usuarios finales que sean personas f&#xed;sicas tendr&#xe1;n derecho a disponer de un procedimiento extrajudicial, transparente, no discriminatorio, sencillo y gratuito para resolver sus controversias con los operadores (procedimiento establecido por el MIET mediante orden). Los operadores estar&#xe1;n obligados a someterse al procedimiento, as&#xed; como a cumplir la resoluci&#xf3;n. El silencio administrativo implica que se desestima la reclamaci&#xf3;n. La resoluci&#xf3;n que se dicte podr&#xe1; impugnarse ante la jurisdicci&#xf3;n contencioso-administrativa."/>
<node CREATED="1411511024813" ID="ID_15322772" MODIFIED="1411511099882" TEXT="2. Alternativa al procedimiento del MEIT: los usuarios finales podr&#xe1;n someter las controversias al conocimiento de las Juntas arbitrales de consumo. Si las Juntas arbitrales de consumo acuerdan conocer sobre la controversia, no ser&#xe1; posible acudir al procedimiento del apartado anterior."/>
</node>
</node>
</node>
<node CREATED="1411589471145" FOLDED="true" ID="ID_452608771" MODIFIED="1411591313873" POSITION="right" TEXT="T&#xcd;TULO IV: Evaluaci&#xf3;n de la conformidad de equipos y aparatos">
<node CREATED="1411591186760" FOLDED="true" ID="ID_508084466" MODIFIED="1411591249265" TEXT="Art&#xed;culo 56. Normalizaci&#xf3;n t&#xe9;cnica.">
<node CREATED="1411507880780" ID="ID_1771953567" MODIFIED="1411511722267" TEXT="1. Condiciones de publicaci&#xf3;n de especificaciones t&#xe9;cnicas precisas y adecuadas de las interfaces ofrecidas --&gt; establecidas mediante RD.">
<icon BUILTIN="ksmiletris"/>
</node>
<node CREATED="1411511113894" ID="ID_333981403" MODIFIED="1411511679608" TEXT="2. Elaboraci&#xf3;n de especificaciones t&#xe9;cnicas aplicables a los equipos y aparatos de telecomunicaciones y equipos exceptuados de evaluaci&#xf3;n de conformidad--&gt; establecida mediante RD."/>
</node>
<node CREATED="1411591186764" FOLDED="true" ID="ID_1128957505" MODIFIED="1411591250146" TEXT="Art&#xed;culo 57. Evaluaci&#xf3;n de la conformidad.">
<node CREATED="1411507890802" ID="ID_459235409" MODIFIED="1411582311586" TEXT="1. Los aparatos de telecomunicaci&#xf3;n (equipo radioel&#xe9;ctrico y/o terminal de telecomunicaci&#xf3;n) deber&#xe1;n evaluar su conformidad e incorporar el marcado correspondiente como consecuencia de la evaluaci&#xf3;n realizada. (Podr&#xe1;n exceptuarse equipos como de radioaficionado por RD)."/>
<node CREATED="1411582312903" ID="ID_1954385174" MODIFIED="1411583901098" TEXT="2. Para la importaci&#xf3;n desde terceros pa&#xed;ses no pertenecientes a la UE el agente econ&#xf3;mico establecido en la UE o el usuario hayan verificado previamente la conformidad.  "/>
<node CREATED="1411582329594" ID="ID_1514769030" MODIFIED="1411584287481" TEXT="4. El MIET podr&#xe1; promover procedimientos complementarios de certificaci&#xf3;n voluntaria para los aparatos de telecomunicaci&#xf3;n que incluir&#xe1;n, al menos, la evaluaci&#xf3;n de la conformidad.  "/>
<node CREATED="1411582337690" ID="ID_1378651775" MODIFIED="1411584310138" TEXT="5. El MIET podr&#xe1; realizar los controles adecuados para asegurar que los equipos puestos en el mercado han evaluado su conformidad."/>
</node>
<node CREATED="1411591186777" FOLDED="true" ID="ID_264259836" MODIFIED="1411591287040" TEXT="Art&#xed;culo 58. Reconocimiento mutuo.">
<node CREATED="1411507899650" ID="ID_1982496774" MODIFIED="1411511431698" TEXT="1. Evaluaci&#xf3;n de conformidad en un EEMM de la UE o en pa&#xed;ses con acuerdo de reconocimiento m&#xfa;tuo con la UE equivalente a evaluaci&#xf3;n de conformidad en Espa&#xf1;a."/>
<node CREATED="1411511202310" ID="ID_1672281883" MODIFIED="1411511318363" TEXT="3. Los aparatos que utilicen el espectro radioel&#xe9;ctrico con par&#xe1;metros de radio no armonizados en la UE: autorizados por el MIET + evaluaci&#xf3;n de conformidad."/>
</node>
<node CREATED="1411591186786" FOLDED="true" ID="ID_1698718705" MODIFIED="1411591312768" TEXT="Art&#xed;culo 59. Condiciones que deben cumplir las instalaciones e instaladores.">
<node CREATED="1411507914178" ID="ID_240250517" MODIFIED="1411584447469" TEXT="1. La instalaci&#xf3;n de los aparatos de telecomunicaci&#xf3;n deber&#xe1; ser realizada siguiendo las instrucciones proporcionadas por el agente econ&#xf3;mico, manteniendo las condiciones bajo las cuales se ha verificado su conformidad."/>
<node CREATED="1411584329295" ID="ID_1297719847" MODIFIED="1411585604830" TEXT="2. La prestaci&#xf3;n de servicios de instalaci&#xf3;n o mantenimiento de equipos o sistemas de telecomunicaci&#xf3;n personas f&#xed;sicas o jur&#xed;dicas nacionales de un EEMM de la UE o con otra nacionalidad si hay acuerdo internacional, en r&#xe9;gimen de libre competencia (requisitos establecidos mediante RD).   "/>
<node CREATED="1411585071396" ID="ID_1773571215" MODIFIED="1411585676651" TEXT="Declaraci&#xf3;n responsable en el Registro de empresas instaladoras de telecomunicaci&#xf3;n, por medios electr&#xf3;nicos o telem&#xe1;ticos con anterioridad al inicio de la actividad (inscripci&#xf3;n de oficio y habilitaci&#xf3;n en todo el territorio espa&#xf1;ol y con  duraci&#xf3;n indefinida)."/>
<node CREATED="1411584341334" ID="ID_615388711" MODIFIED="1411585508042" TEXT="3. El Registro de empresas instaladoras de telecomunicaci&#xf3;n ser&#xe1; de car&#xe1;cter p&#xfa;blico y su regulaci&#xf3;n se har&#xe1; mediante RD. "/>
</node>
</node>
<node CREATED="1411589471146" FOLDED="true" ID="ID_1801629856" MODIFIED="1411631877760" POSITION="right" TEXT="T&#xcd;TULO V : Dominio p&#xfa;blico radioel&#xe9;ctrico">
<node CREATED="1411591378764" FOLDED="true" ID="ID_1317603049" MODIFIED="1411592560762" TEXT="Art&#xed;culo 60. De la administraci&#xf3;n del dominio p&#xfa;blico radioel&#xe9;ctrico.">
<node CREATED="1411591378765" ID="ID_1497950356" MODIFIED="1411592412486" TEXT="1. Espectro radioel&#xe9;ctrico: bien de dominio p&#xfa;blico, cuya titularidad y administraci&#xf3;n corresponden al Estado (conforme a Ley 9/2014, tratados y acuerdos internacionales en los que Espa&#xf1;a sea parte, atendiendo a la normativa aplicable en la UE y a las resoluciones y recomendaciones de la UIT y  otros organismos internacionales). &#xa;"/>
<node CREATED="1411591378768" ID="ID_1579265628" MODIFIED="1411592429566" TEXT="2. Administraci&#xf3;n teniendo en cuenta su importante valor social, cultural y econ&#xf3;mico y la necesaria cooperaci&#xf3;n con otros EEMM de la UE y con la Comisi&#xf3;n Europea en la planificaci&#xf3;n estrat&#xe9;gica, la coordinaci&#xf3;n y la armonizaci&#xf3;n del uso del espectro radioel&#xe9;ctrico en la UE Europea. "/>
<node CREATED="1411591378772" ID="ID_402590453" MODIFIED="1411592521823" TEXT="3. Principios aplicables a la administraci&#xf3;n del dominio p&#xfa;blico radioel&#xe9;ctrico ">
<node CREATED="1411591378774" MODIFIED="1411591378774" TEXT="a) Garantizar un uso eficaz y eficiente de este recurso."/>
<node CREATED="1411591378775" MODIFIED="1411591378775" TEXT="b) Fomentar la neutralidad tecnol&#xf3;gica y de los servicios, y el mercado secundario del espectro."/>
<node CREATED="1411591378775" MODIFIED="1411591378775" TEXT="c) Fomentar una mayor competencia en el mercado de las comunicaciones electr&#xf3;nicas."/>
</node>
<node CREATED="1411591378776" ID="ID_1063720993" MODIFIED="1411592494782" TEXT="4. La administraci&#xf3;n del DPR incluyen las siguientes:">
<node CREATED="1411512110915" ID="ID_1135527162" MODIFIED="1411512120333" TEXT="a) Planificaci&#xf3;n: Elaboraci&#xf3;n y aprobaci&#xf3;n de los planes de utilizaci&#xf3;n. "/>
<node CREATED="1411512120995" ID="ID_676746461" MODIFIED="1411512139661" TEXT="b) Gesti&#xf3;n: Establecimiento de las condiciones t&#xe9;cnicas de explotaci&#xf3;n y otorgamiento de los derechos de uso. "/>
<node CREATED="1411512140154" ID="ID_1740915201" MODIFIED="1411512194459" TEXT="c) Control: Comprobaci&#xf3;n t&#xe9;cnica de las emisiones, detecci&#xf3;n y eliminaci&#xf3;n de interferencias, inspecci&#xf3;n t&#xe9;cnica de instalaciones, equipos y aparatos radioel&#xe9;ctricos, as&#xed; como el control de la puesta en el mercado de &#xe9;stos &#xfa;ltimos, protecci&#xf3;n del DPR."/>
<node CREATED="1411512152242" ID="ID_1712160621" MODIFIED="1411512153644" TEXT=" d) Aplicaci&#xf3;n del r&#xe9;gimen sancionador. "/>
</node>
<node CREATED="1411591378784" ID="ID_358974966" MODIFIED="1411592557223" TEXT="5. La utilizaci&#xf3;n de frecuencias radioel&#xe9;ctricas mediante redes de sat&#xe9;lites se incluye dentro de la administraci&#xf3;n del dominio p&#xfa;blico radioel&#xe9;ctrico. La utilizaci&#xf3;n del DPR necesaria para la utilizaci&#xf3;n de los recursos &#xf3;rbita-espectro en el &#xe1;mbito de la soberan&#xed;a espa&#xf1;ola y mediante sat&#xe9;lites de comunicaciones queda reservada al Estado (explotaci&#xf3;n sometida al derecho internacional mediante su gesti&#xf3;n directa por el Estado o mediante concesi&#xf3;n seg&#xfa;n RD)."/>
</node>
<node CREATED="1411591378786" ID="ID_1245597914" MODIFIED="1411631347994" TEXT="Art&#xed;culo 61. Facultades del Gobierno para la administraci&#xf3;n del dominio p&#xfa;blico radioel&#xe9;ctrico. El Gobierno desarrollar&#xe1; mediante RD las condiciones para la adecuada administraci&#xf3;n del dominio p&#xfa;blico radioel&#xe9;ctrico. En dicho real decreto se regular&#xe1;, como m&#xed;nimo, lo siguiente:">
<node CREATED="1411585751639" ID="ID_1906143376" MODIFIED="1411585768937" TEXT="a) El procedimiento para la elaboraci&#xf3;n de los planes de utilizaci&#xf3;n del espectro radioel&#xe9;ctrico, que incluyen el Cuadro Nacional de Atribuci&#xf3;n de Frecuencias, los planes t&#xe9;cnicos nacionales de radiodifusi&#xf3;n y televisi&#xf3;n, cuya aprobaci&#xf3;n corresponder&#xe1; al Gobierno, y las necesidades de espectro radioel&#xe9;ctrico para la defensa nacional. Los datos relativos a esta &#xfa;ltima materia tendr&#xe1;n el car&#xe1;cter de reservados.  "/>
<node CREATED="1411585769710" ID="ID_981971543" MODIFIED="1411585974391" TEXT="b) El procedimiento de determinaci&#xf3;n, control e inspecci&#xf3;n de los niveles &#xfa;nicos de emisi&#xf3;n radioel&#xe9;ctrica tolerable y que no supongan un peligro para la salud p&#xfa;blica."/>
<node CREATED="1411585782622" ID="ID_155405933" MODIFIED="1411586201759" TEXT="c) Los procedimientos, plazos y condiciones para la habilitaci&#xf3;n del ejercicio de los derechos de uso del DPR, que revestir&#xe1; la forma de autorizaci&#xf3;n general, autorizaci&#xf3;n individual, afectaci&#xf3;n o concesi&#xf3;n administrativas.  Procedimiento abierto de otorgamiento de derechos de uso del DPR."/>
<node CREATED="1411585792829" ID="ID_603072574" MODIFIED="1411586247128" TEXT="d) El procedimiento para la reasignaci&#xf3;n del uso de bandas de frecuencias con el objetivo de alcanzar un uso m&#xe1;s eficiente del espectro radioel&#xe9;ctrico."/>
<node CREATED="1411585802109" ID="ID_1112036106" MODIFIED="1411586278056" TEXT="e) Las condiciones asociadas a los t&#xed;tulos habilitantes para el uso del DPR."/>
<node CREATED="1411585813285" ID="ID_944156684" MODIFIED="1411586296060" TEXT="f) Las condiciones de otorgamiento de t&#xed;tulos habilitantes para el uso del DPR para fines experimentales o eventos de corta duraci&#xf3;n.  "/>
<node CREATED="1411585803853" ID="ID_383840645" MODIFIED="1411585826343" TEXT="g) La adecuada utilizaci&#xf3;n del espectro radioel&#xe9;ctrico mediante el empleo de equipos y aparatos."/>
</node>
<node CREATED="1411619090570" FOLDED="true" ID="ID_1823932041" MODIFIED="1411631447939" TEXT="Art&#xed;culo 62. T&#xed;tulos habilitantes">
<node CREATED="1411619161199" ID="ID_772998650" MODIFIED="1411620166050" TEXT="Usos">
<node CREATED="1411619168100" ID="ID_1333564340" MODIFIED="1411619230048" TEXT="Com&#xfa;n: ning&#xfa;n t&#xed;tulo habilitante"/>
<node CREATED="1411619190446" ID="ID_45098798" MODIFIED="1411619254696" TEXT="Especial: explotaci&#xf3;n compartida, sin limitaci&#xf3;n de n&#xfa;mero"/>
<node CREATED="1411619194635" ID="ID_1739107643" MODIFIED="1411619280920" TEXT="Privativo: explotaci&#xf3;n en exclusiva o por un n&#xfa;mero limitado"/>
</node>
<node CREATED="1411619289194" ID="ID_1134755159" MODIFIED="1411619295365" TEXT="Tipos">
<node CREATED="1411619295365" ID="ID_300762115" MODIFIED="1411619307510" TEXT="Autorizaci&#xf3;n general">
<node CREATED="1411619307510" ID="ID_937821056" MODIFIED="1411619336339" TEXT="Uso especial de las bandas">
<node CREATED="1411621211005" ID="ID_1478454232" MODIFIED="1411621231208" TEXT="S&#xf3;lo con la notificaci&#xf3;n a la SETSI">
<node CREATED="1411625692049" ID="ID_1491723014" MODIFIED="1411625719310" STYLE="bubble" TEXT="Desestimaci&#xf3;n en 15 d&#xed;as"/>
</node>
</node>
</node>
<node CREATED="1411619337500" ID="ID_815963638" MODIFIED="1411619362484" TEXT="Autorizaci&#xf3;n individual">
<node CREATED="1411619351393" ID="ID_1351350583" MODIFIED="1411619388739" TEXT="Uso especial por radioaficionados u otros sin contenido econ&#xf3;mico"/>
<node CREATED="1411619388855" ID="ID_564101878" MODIFIED="1411619418824" TEXT="Uso privativo para autoprestaci&#xf3;n"/>
<node CREATED="1411619423760" ID="ID_854103264" MODIFIED="1411619439229" TEXT="AAPP">
<node CREATED="1411619439229" ID="ID_1580632736" MODIFIED="1411619444938" TEXT="afectaci&#xf3;n demanial"/>
</node>
</node>
<node CREATED="1411619448112" ID="ID_1084155495" MODIFIED="1411619459571" TEXT="Afectaci&#xf3;n o concesiones administrativas">
<node CREATED="1411619459571" ID="ID_479357249" MODIFIED="1411619466841" TEXT="Resto">
<node CREATED="1411625658868" ID="ID_304657943" MODIFIED="1411625664487" TEXT="Deben ser operadores"/>
<node CREATED="1411625664937" ID="ID_816819311" MODIFIED="1411625671797" TEXT="Deben poder contratatar"/>
</node>
</node>
</node>
<node CREATED="1411621158746" ID="ID_1611566393" MODIFIED="1411621172831" TEXT="Requiere la aprobaci&#xf3;n de proyecto t&#xe9;cnico">
<node CREATED="1411621172832" ID="ID_533171843" MODIFIED="1411621195896" TEXT="Puede sustituirse por declaraci&#xf3;n responsable"/>
</node>
</node>
<node CREATED="1411619471278" FOLDED="true" ID="ID_1362674100" MODIFIED="1411631445961" TEXT="Art&#xed;culo 63. T&#xed;tulos habilitantes mediante licitaci&#xf3;n">
<node CREATED="1411619494015" ID="ID_1110438114" MODIFIED="1411619524265" TEXT="Casos en que el MINETUR quiera limitar el n&#xfa;mero de concesiones demaniales"/>
<node CREATED="1411621051338" ID="ID_1278862977" MODIFIED="1411621067722" TEXT="Competencia de la SETSI"/>
<node CREATED="1411621281143" ID="ID_904372614" MODIFIED="1411621295490" STYLE="bubble" TEXT="Resoluci&#xf3;n en 8 MESES"/>
</node>
<node CREATED="1411619535862" FOLDED="true" ID="ID_1833733460" MODIFIED="1411631465800" TEXT="Art&#xed;culo 64. Duraci&#xf3;n, modificaci&#xf3;n, extinci&#xf3;n y revocaci&#xf3;n de los t&#xed;tulos habilitantes">
<node CREATED="1411619595635" ID="ID_1776293868" MODIFIED="1411619621094" TEXT="Uso privativo sin limitaci&#xf3;n de n&#xfa;mero">
<node CREATED="1411619626280" ID="ID_1902626405" MODIFIED="1411619652536" TEXT="31 de diciembre del a&#xf1;o natural del 5&#xba; a&#xf1;o de vigencia"/>
<node CREATED="1411619653182" ID="ID_482838009" MODIFIED="1411619664212" TEXT="Renovables cada 5 a&#xf1;os"/>
</node>
<node CREATED="1411619666902" ID="ID_652454724" MODIFIED="1411619687183" TEXT="Uso privativo con limitaci&#xf3;n de n&#xfa;mero">
<node CREATED="1411619688734" ID="ID_501240900" MODIFIED="1411619703181" TEXT="Prevista en la licitaci&#xf3;n"/>
<node CREATED="1411619704904" ID="ID_355053856" MODIFIED="1411619717697" TEXT="M&#xe1;ximo de 20 a&#xf1;os, sin renovaci&#xf3;n autom&#xe1;tica"/>
</node>
<node CREATED="1411625820409" ID="ID_1981410161" MODIFIED="1411625828649" TEXT="Causas de extinci&#xf3;n">
<node CREATED="1411625828650" ID="ID_1307735457" MODIFIED="1411625856565" TEXT="a) Rese&#xf1;adas en art. 100 de la Ley 33/2003"/>
<node CREATED="1411625858281" ID="ID_1819172369" MODIFIED="1411625872063" TEXT="b) Muerte del titular"/>
<node CREATED="1411625875888" ID="ID_1455394262" MODIFIED="1411625884885" TEXT="c) Renuncia del titular"/>
<node CREATED="1411625888208" ID="ID_471730840" MODIFIED="1411625900572" TEXT="d) P&#xe9;rdida de la condici&#xf3;n de operadosr"/>
<node CREATED="1411625901456" ID="ID_461820456" MODIFIED="1411625922700" TEXT="e) Falta de pago de la tasa por reserva de DPR"/>
<node CREATED="1411625928336" ID="ID_704591797" MODIFIED="1411625962781" TEXT="f) P&#xe9;rdida de adecuaci&#xf3;n de las caracter&#xed;sticas t&#xe9;cnicas de la red al CNR"/>
<node CREATED="1411625972752" ID="ID_609590457" MODIFIED="1411625992844" TEXT="g) Mutuo acuerdo entre el titular y el MINETUR"/>
<node CREATED="1411626005552" ID="ID_1672696070" MODIFIED="1411626024181" TEXT="h) Transcurso del tiempo para el que se acordaron"/>
<node CREATED="1411626030960" ID="ID_1138424546" MODIFIED="1411626065565" TEXT="i) Incumplimiento grave y reiterado de las obligaciones del titular"/>
</node>
<node CREATED="1411626093920" ID="ID_1271206600" MODIFIED="1411626102457" TEXT="Causas de revocaci&#xf3;n">
<node CREATED="1411626102458" ID="ID_1990941638" MODIFIED="1411626132804" TEXT="a) Incumplimiento de las condiciones y requisitos t&#xe9;cnicos"/>
<node CREATED="1411626133367" ID="ID_1486033403" MODIFIED="1411626162205" TEXT="b) No pagar el impuesto de Transmisiones Patrimoniales y Actor Jur&#xed;dicos Documentados"/>
<node CREATED="1411626165560" ID="ID_1911742834" MODIFIED="1411626181077" TEXT="c) No efectuar un uso eficaz y eficiente del DPR"/>
<node CREATED="1411626185055" ID="ID_152175688" MODIFIED="1411626283493" TEXT="d) La revocaci&#xf3;n sucesiva de dos autorizaciones administrativas de transferencia de t&#xed;tulo en un a&#xf1;o"/>
<node CREATED="1411626225248" ID="ID_1452235508" MODIFIED="1411626254332" TEXT="e) La utilizaci&#xf3;n de frecuencias con fines distintos a los que motivaron su asignaci&#xf3;n"/>
</node>
</node>
<node CREATED="1411619724459" FOLDED="true" ID="ID_132714267" MODIFIED="1411631486598" TEXT="Art&#xed;culo 65. Protecci&#xf3;n activa">
<node CREATED="1411619747368" ID="ID_1638115531" MODIFIED="1411619753733" TEXT="Encargada la SETSI">
<node CREATED="1411626507800" ID="ID_996905494" MODIFIED="1411626529973" TEXT="Podr&#xe1; realizar emisiones sin contenidos sustantivos"/>
<node CREATED="1411626530376" ID="ID_276332074" MODIFIED="1411626552300" TEXT="En canales con derechos de uso no otorgados "/>
</node>
<node CREATED="1411619754083" ID="ID_1187111702" MODIFIED="1411619764926" TEXT="Procedimiento mediante RD">
<node CREATED="1411626332288" ID="ID_1681695097" MODIFIED="1411626339649" TEXT="Normas">
<node CREATED="1411626339650" ID="ID_1348212733" MODIFIED="1411626390277" TEXT="a) Se constatar&#xe1; la ocupaci&#xf3;n de una frecuencia sin titulo habilitante"/>
<node CREATED="1411626391608" ID="ID_1819667353" MODIFIED="1411626423717" TEXT="b) Tramite de audiencia para alegaciones (10 d&#xed;as)"/>
<node CREATED="1411626424168" ID="ID_1077081574" MODIFIED="1411626453005" TEXT="c) Se requerir&#xe1; al cese de emisiones en 8 d&#xed;as h&#xe1;biles"/>
<node CREATED="1411626467849" ID="ID_1244237638" MODIFIED="1411626501580" TEXT="d) Si no se procede, la SETSI iniciar&#xe1; sus emisiones en dicha frecuencia"/>
</node>
</node>
</node>
<node CREATED="1411619772437" FOLDED="true" ID="ID_1906980754" MODIFIED="1411631511383" TEXT="Art&#xed;culo 66. Neutralidad tecnol&#xf3;gica">
<node CREATED="1411619805237" ID="ID_776884039" MODIFIED="1411619829986" TEXT="Se podr&#xe1; emplear cualquier tipo de tecnolog&#xed;a">
<node CREATED="1411619829986" ID="ID_726881497" MODIFIED="1411619841483" TEXT="De conformidad con el derecho de la UE"/>
</node>
<node CREATED="1411619842644" ID="ID_1085992203" MODIFIED="1411619874867" TEXT="Podr&#xe1;n preveerse restricciones">
<node CREATED="1411619874867" ID="ID_348616348" MODIFIED="1411619894601" TEXT="Proporcionadas y no discriminatorias"/>
<node CREATED="1411619896527" ID="ID_1200709690" MODIFIED="1411620066600" TEXT="A los tipos de tecnolog&#xed;a de acceso inal&#xe1;mbrico o red, para">
<node CREATED="1411619923789" ID="ID_720668482" MODIFIED="1411619997172" TEXT="a) Evitar interferencias perjudiciales">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1411619938678" ID="ID_368726839" MODIFIED="1411619963390" TEXT="b) Proteger la salud p&#xfa;blica frente a los campos electromagn&#xe9;ticos"/>
<node CREATED="1411619964192" ID="ID_1426298975" MODIFIED="1411619974162" TEXT="c) Asegurar la calidad t&#xe9;cnica"/>
<node CREATED="1411620001656" ID="ID_508472277" MODIFIED="1411620019643" TEXT="d) Garantizar un uso compartido m&#xe1;ximo de las frecuencias"/>
<node CREATED="1411620020321" ID="ID_517203706" MODIFIED="1411620038044" TEXT="e) Garantizar un uso eficiente del espectro"/>
<node CREATED="1411620039626" ID="ID_609399331" MODIFIED="1411620051686" TEXT="f) Por un objetivo de inter&#xe9;s general"/>
</node>
</node>
<node CREATED="1411626615296" ID="ID_1479349526" MODIFIED="1411626630985" TEXT="Exigencia de una frecuencia determinada">
<node CREATED="1411626630986" ID="ID_207896489" MODIFIED="1411626648209" TEXT="justificada por inter&#xe9;s general">
<node CREATED="1411626648210" ID="ID_346532000" MODIFIED="1411626656028" TEXT="a) Seguridad de la vida"/>
<node CREATED="1411626656279" ID="ID_95789720" MODIFIED="1411626675852" TEXT="b) Promoci&#xf3;n de la cohesi&#xf3;n social, regional o territorial"/>
<node CREATED="1411626676199" ID="ID_459668197" MODIFIED="1411626701285" TEXT="c) Evitaci&#xf3;n del uso ineficiente de las radiofrecuencias"/>
<node CREATED="1411626701928" ID="ID_1115580969" MODIFIED="1411626726988" TEXT="d) Promoci&#xf3;n de la diversidad cultural y lingu&#xed;stica "/>
</node>
<node CREATED="1411619829986" ID="ID_487026969" MODIFIED="1411619841483" TEXT="De conformidad con el derecho de la UE"/>
</node>
</node>
<node CREATED="1411620072004" ID="ID_1260038539" MODIFIED="1411631581668" TEXT="Art&#xed;culo 67. Mercado secundario">
<node CREATED="1411620191000" ID="ID_1525915874" MODIFIED="1411620215432" TEXT="Los t&#xed;tulos habilitantes podr&#xe1;n ser transferidos"/>
<node CREATED="1411620215890" ID="ID_1582992282" MODIFIED="1411620237981" TEXT="Los derechos de uso podr&#xe1;n ser cedidos"/>
<node CREATED="1411620242652" ID="ID_977502347" MODIFIED="1411620247022" TEXT="Mediante RD"/>
</node>
</node>
<node CREATED="1411589471147" FOLDED="true" ID="ID_664226644" MODIFIED="1411631871481" POSITION="right" TEXT="T&#xcd;TULO VI: La administraci&#xf3;n de las telecomunicaciones">
<node CREATED="1411620294477" FOLDED="true" ID="ID_289838655" MODIFIED="1411631715105" TEXT="Art&#xed;culo 68. Competencias de la AGE y sus organismos p&#xfa;blicos">
<node CREATED="1411620321052" ID="ID_1850397538" MODIFIED="1411627418393" TEXT="ANRs">
<node CREATED="1411620326441" ID="ID_1340863515" MODIFIED="1411620335911" TEXT="a) El Gobierno"/>
<node CREATED="1411620336509" ID="ID_764168937" MODIFIED="1411620362512" TEXT="b) Organos superiores y directivos del MINETUR">
<node CREATED="1411620362512" ID="ID_517859954" MODIFIED="1411631674371" TEXT="Que asuman las competencias asignadas a este Ministerio (art. 69)">
<arrowlink DESTINATION="ID_1198253453" ENDARROW="Default" ENDINCLINATION="834;0;" ID="Arrow_ID_138743315" STARTARROW="None" STARTINCLINATION="834;0;"/>
</node>
</node>
<node CREATED="1411620403141" ID="ID_1581141609" MODIFIED="1411620428937" TEXT="c) Organos superiores y directivos del Ministerio de Econom&#xed;a y Competitividad">
<node CREATED="1411620449123" ID="ID_1894784941" MODIFIED="1411620449123" TEXT="Que asuman las competencias asignadas a este Ministerio"/>
</node>
<node CREATED="1411620461563" ID="ID_1060598701" MODIFIED="1411631691286" TEXT="d) CNMC (art. 70)">
<arrowlink DESTINATION="ID_1175784359" ENDARROW="Default" ENDINCLINATION="191;0;" ID="Arrow_ID_1005870496" STARTARROW="None" STARTINCLINATION="191;0;"/>
</node>
</node>
<node CREATED="1411626859760" FOLDED="true" ID="ID_1554197699" MODIFIED="1411627149881" TEXT="Principios reguladores">
<node CREATED="1411626866633" ID="ID_74228364" MODIFIED="1411626880109" TEXT="a) Promover un entorno regulador previsible"/>
<node CREATED="1411626880728" ID="ID_92802572" MODIFIED="1411626899501" TEXT="b) Fomentar la inversi&#xf3;n eficiente"/>
<node CREATED="1411626911297" ID="ID_153977795" MODIFIED="1411626942997" TEXT="c) Imponer obligaciones espec&#xed;ficas unicamente cuando no exista una competencia efectiva"/>
<node CREATED="1411626943864" ID="ID_1966899741" MODIFIED="1411626998669" TEXT="d) Garantizar un trato no discriminatorio a las empresas"/>
<node CREATED="1411626962848" ID="ID_1559465039" MODIFIED="1411627013213" TEXT="e) Salvaguardar la competencia en beneficio de los consumidores"/>
<node CREATED="1411627014544" ID="ID_1506311462" MODIFIED="1411627063237" TEXT="f) Tener en cuenta la variedad de condiciones en cuanto a la competencia y consumidores"/>
<node CREATED="1411627088944" ID="ID_983798126" MODIFIED="1411627118117" TEXT="g) Promover la eficiencia, la competencia sostenible y el m&#xe1;ximo beneficio para los usuarios"/>
</node>
</node>
<node CREATED="1411591495694" FOLDED="true" ID="ID_1198253453" MODIFIED="1411631712923" TEXT="Art&#xed;culo 69. Ministerio de Industria, Energ&#xed;a y Turismo.">
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1198253453" ENDARROW="Default" ENDINCLINATION="834;0;" ID="Arrow_ID_138743315" SOURCE="ID_517859954" STARTARROW="None" STARTINCLINATION="834;0;"/>
<node CREATED="1411591495695" FOLDED="true" ID="ID_407681959" MODIFIED="1411631653618" TEXT="Los &#xf3;rganos superiores y directivos del Ministerio de Industria, Energ&#xed;a y Turismo que, de conformidad con la estructura org&#xe1;nica del departamento, asuman las competencias asignadas a este ministerio, ejercer&#xe1;n las siguientes funciones:">
<node CREATED="1411591495697" MODIFIED="1411591495697" TEXT="a) Ejecutar la pol&#xed;tica adoptada por el Gobierno en los servicios de telecomunicaciones para la defensa nacional y la protecci&#xf3;n civil a los que se refiere el art&#xed;culo 4 de la presente Ley."/>
<node CREATED="1411591495699" MODIFIED="1411591495699" TEXT="b) Gestionar el Registro de Operadores"/>
<node CREATED="1411591495700" MODIFIED="1411591495700" TEXT="c) Ejercer las competencias que en materia de acceso a las redes y recursos asociados, interoperabilidad e interconexi&#xf3;n le atribuye la presente Ley y su desarrollo reglamentario, en particular, en los siguientes supuestos:">
<node CREATED="1411591495702" MODIFIED="1411591495702" TEXT="1. En los procedimientos de licitaci&#xf3;n para la obtenci&#xf3;n de derechos de uso del dominio p&#xfa;blico radioel&#xe9;ctrico"/>
<node CREATED="1411591495704" MODIFIED="1411591495704" TEXT="2. Cuando se haga necesario para garantizar el cumplimiento de la normativa sobre datos personales y protecci&#xf3;n de la intimidad en el sector de las comunicaciones electr&#xf3;nicas."/>
<node CREATED="1411591495706" MODIFIED="1411591495706" TEXT="3. Cuando resulte preciso para garantizar el cumplimiento de compromisos internacionales en materia de telecomunicaciones."/>
</node>
<node CREATED="1411591495708" MODIFIED="1411591495708" TEXT="d) Proponer al Gobierno la aprobaci&#xf3;n de los planes nacionales de numeraci&#xf3;n, direccionamiento y denominaci&#xf3;n, el otorgamiento de los derechos de uso de los recursos p&#xfa;blicos regulados en dichos planes y ejercer las dem&#xe1;s competencias que le atribuye el cap&#xed;tulo V del T&#xed;tulo II de la presente Ley."/>
<node CREATED="1411591495710" MODIFIED="1411591495710" TEXT="e) Proponer al Gobierno la pol&#xed;tica a seguir para facilitar el desarrollo y la evoluci&#xf3;n de las obligaciones de servicio p&#xfa;blico a las que se hace referencia en el cap&#xed;tulo I del T&#xed;tulo III y la desarrollar&#xe1; asumiendo la competencia de control y seguimiento de las obligaciones de servicio p&#xfa;blico que correspondan a los distintos operadores en la explotaci&#xf3;n de redes o la prestaci&#xf3;n de servicios de comunicaciones electr&#xf3;nicas."/>
<node CREATED="1411591495712" MODIFIED="1411591495712" TEXT="f) Proponer al Gobierno la pol&#xed;tica a seguir para reconocer y garantizar los derechos y obligaciones de car&#xe1;cter p&#xfa;blico en la explotaci&#xf3;n de redes y en la prestaci&#xf3;n de servicios de comunicaciones electr&#xf3;nicas as&#xed; como los derechos de los usuarios finales a los que se hace referencia en los cap&#xed;tulos II, III y V del T&#xed;tulo III."/>
<node CREATED="1411591495714" MODIFIED="1411591495714" TEXT="g) Gestionar el Registro de empresas instaladoras de telecomunicaci&#xf3;n."/>
<node CREATED="1411591495715" MODIFIED="1411591495715" TEXT="h) Formular las propuestas para la elaboraci&#xf3;n de normativa relativa a las infraestructuras comunes de comunicaciones electr&#xf3;nicas en el interior de edificios y conjuntos inmobiliarios, y el seguimiento de su implantaci&#xf3;n en Espa&#xf1;a."/>
<node CREATED="1411591495716" MODIFIED="1411591495716" TEXT="i) Ejercer las funciones en materia de la evaluaci&#xf3;n de la conformidad de equipos y aparatos a las que se refiere el T&#xed;tulo IV."/>
<node CREATED="1411591495718" MODIFIED="1411591495718" TEXT="j) Ejercer las funciones en materia de administraci&#xf3;n del dominio p&#xfa;blico radioel&#xe9;ctrico a las que se refiere el T&#xed;tulo V. En particular, ejercer&#xe1; las siguientes funciones:">
<node CREATED="1411591495720" MODIFIED="1411591495720" TEXT="1. La propuesta de planificaci&#xf3;n, la gesti&#xf3;n y el control del dominio p&#xfa;blico radioel&#xe9;ctrico, as&#xed; como la tramitaci&#xf3;n y el otorgamiento de los t&#xed;tulos habilitantes para su utilizaci&#xf3;n"/>
<node CREATED="1411591495722" MODIFIED="1411591495722" TEXT="2. El ejercicio de las funciones atribuidas a la Administraci&#xf3;n General del Estado en materia de autorizaci&#xf3;n e inspecci&#xf3;n de instalaciones radioel&#xe9;ctricas en relaci&#xf3;n con los niveles &#xfa;nicos de emisi&#xf3;n radioel&#xe9;ctrica permitidos a que se refiere el art&#xed;culo 61 de esta Ley"/>
<node CREATED="1411591495724" MODIFIED="1411591495724" TEXT="3. La gesti&#xf3;n de un registro p&#xfa;blico de radiofrecuencias, accesible a trav&#xe9;s de Internet, en el que constar&#xe1;n los titulares de concesiones administrativas para el uso privativo del dominio p&#xfa;blico radioel&#xe9;ctrico."/>
<node CREATED="1411591495727" MODIFIED="1411591495727" TEXT="4. La elaboraci&#xf3;n de proyectos y desarrollo de los planes t&#xe9;cnicos nacionales de radiodifusi&#xf3;n y televisi&#xf3;n."/>
<node CREATED="1411591495727" MODIFIED="1411591495727" TEXT="5. La comprobaci&#xf3;n t&#xe9;cnica de emisiones radioel&#xe9;ctricas para la identificaci&#xf3;n, localizaci&#xf3;n y eliminaci&#xf3;n de interferencias perjudiciales, infracciones, irregularidades y perturbaciones de los sistemas de radiocomunicaci&#xf3;n, y la verificaci&#xf3;n del uso efectivo y eficiente del dominio p&#xfa;blico radioel&#xe9;ctrico por parte de los titulares de derechos de uso"/>
<node CREATED="1411591495730" MODIFIED="1411591495730" TEXT="6. La protecci&#xf3;n del dominio p&#xfa;blico radioel&#xe9;ctrico, para lo cual podr&#xe1;, entre otras actuaciones, realizar emisiones en aquellas frecuencias y canales radioel&#xe9;ctricos cuyos derechos de uso, en el &#xe1;mbito territorial correspondiente, no hayan sido otorgados."/>
<node CREATED="1411591495732" MODIFIED="1411591495732" TEXT="7. La gesti&#xf3;n de la asignaci&#xf3;n de los recursos &#xf3;rbita-espectro para comunicaciones por sat&#xe9;lite."/>
<node CREATED="1411591495733" MODIFIED="1411591495733" TEXT="8. La elaboraci&#xf3;n de estudios e informes y, en general, el asesoramiento de la Administraci&#xf3;n General del Estado en todo lo relativo a la administraci&#xf3;n del dominio p&#xfa;blico radioel&#xe9;ctrico."/>
<node CREATED="1411591495734" MODIFIED="1411591495734" TEXT="9. La participaci&#xf3;n en los organismos internacionales relacionados con la planificaci&#xf3;n del espectro radioel&#xe9;ctrico."/>
</node>
<node CREATED="1411591495736" MODIFIED="1411591495736" TEXT="k) Gestionar en per&#xed;odo voluntario las tasas en materia de telecomunicaciones a que se refiere la presente Ley."/>
<node CREATED="1411591495738" MODIFIED="1411591495738" TEXT="l) Ejercer las funciones de gesti&#xf3;n, liquidaci&#xf3;n, inspecci&#xf3;n y recaudaci&#xf3;n en periodo voluntario de las aportaciones a realizar por los operadores de telecomunicaciones y por los prestadores privados del servicio de comunicaci&#xf3;n audiovisual televisiva, de &#xe1;mbito geogr&#xe1;fico estatal o superior al de una Comunidad Aut&#xf3;noma, reguladas en los art&#xed;culos 5 y 6 de la Ley 8/2009, de 28 de agosto, de financiaci&#xf3;n de la Corporaci&#xf3;n Radio y Televisi&#xf3;n Espa&#xf1;ola."/>
<node CREATED="1411591495740" MODIFIED="1411591495740" TEXT="m) Realizar las funciones atribuidas de manera expresa por la normativa comunitaria, la presente Ley y su normativa de desarrollo."/>
<node CREATED="1411591495742" MODIFIED="1411591495742" TEXT="n) Realizar cualesquiera otras funciones que le sean atribuidas por ley o por real decreto."/>
</node>
</node>
<node CREATED="1411591495743" FOLDED="true" ID="ID_1175784359" MODIFIED="1411631705482" TEXT="Art&#xed;culo 70. La Comisi&#xf3;n Nacional de los Mercados y la Competencia.">
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1175784359" ENDARROW="Default" ENDINCLINATION="191;0;" ID="Arrow_ID_1005870496" SOURCE="ID_1060598701" STARTARROW="None" STARTINCLINATION="191;0;"/>
<node CREATED="1411591495743" MODIFIED="1411591495743" TEXT="1. La naturaleza, funciones, estructura, personal, presupuesto y dem&#xe1;s materias que configuran la Comisi&#xf3;n Nacional de los Mercados y la Competencia est&#xe1;n reguladas en la Ley de creaci&#xf3;n de la Comisi&#xf3;n Nacional de los Mercados y la Competencia."/>
<node CREATED="1411591495745" FOLDED="true" ID="ID_870867445" MODIFIED="1411631702300" TEXT="2. En particular, en las materias reguladas por la presente Ley, la Comisi&#xf3;n Nacional de los Mercados y la Competencia ejercer&#xe1; las siguientes funciones:">
<node CREATED="1411591495746" MODIFIED="1411591495746" TEXT="a) Definir y analizar los mercados de referencia relativos a redes y servicios de comunicaciones electr&#xf3;nicas, entre los que se incluir&#xe1;n los correspondientes mercados de referencia al por mayor y al por menor, y el &#xe1;mbito geogr&#xe1;fico de los mismos, cuyas caracter&#xed;sticas pueden justificar la imposici&#xf3;n de obligaciones espec&#xed;ficas, en los t&#xe9;rminos establecidos en el art&#xed;culo 13 de la presente Ley y su normativa de desarrollo."/>
<node CREATED="1411591495749" MODIFIED="1411591495749" TEXT="b) Identificar el operador u operadores que poseen un poder significativo en el mercado cuando del an&#xe1;lisis de los mercados de referencia se constate que no se desarrollan en un entorno de competencia efectiva."/>
<node CREATED="1411591495750" MODIFIED="1411591495750" TEXT="c) Establecer, cuando proceda, las obligaciones espec&#xed;ficas que correspondan a los operadores con poder significativo en mercados de referencia, en los t&#xe9;rminos establecidos en el art&#xed;culo 14 de la presente Ley y su normativa de desarrollo."/>
<node CREATED="1411591495751" MODIFIED="1411591495751" TEXT="d) Resolver los conflictos en los mercados de comunicaciones electr&#xf3;nicas a los que se refiere el art&#xed;culo 15 de la presente Ley. En particular, le corresponder&#xe1; resolver conflictos entre operadores relativos a la determinaci&#xf3;n de las condiciones concretas para la puesta en pr&#xe1;ctica de la obligaci&#xf3;n impuesta por el Ministerio de Industria, Energ&#xed;a y Turismo de la utilizaci&#xf3;n compartida del dominio p&#xfa;blico o la propiedad privada, o de la ubicaci&#xf3;n compartida de infraestructuras y recursos asociados, de acuerdo con el procedimiento regulado en el art&#xed;culo 32 de la presente Ley, as&#xed; como resolver conflictos sobre el acceso a infraestructuras susceptibles de alojar redes p&#xfa;blicas de comunicaciones electr&#xf3;nicas y el acceso a las redes de comunicaciones electr&#xf3;nicas titularidad de los &#xf3;rganos o entes gestores de infraestructuras de transporte de competencia estatal, en los t&#xe9;rminos establecidos por los art&#xed;culos 37 y 38 de la presente Ley."/>
<node CREATED="1411591495752" MODIFIED="1411591495752" TEXT="e) Decidir la imposici&#xf3;n, como medida excepcional, a los operadores con poder significativo en el mercado integrados verticalmente, de la obligaci&#xf3;n de separaci&#xf3;n funcional de acuerdo con los requisitos y procedimientos indicados en el art&#xed;culo 16 de la presente Ley."/>
<node CREATED="1411591495753" MODIFIED="1411591495753" TEXT="f) Fijar las caracter&#xed;sticas y condiciones para la conservaci&#xf3;n de los n&#xfa;meros en aplicaci&#xf3;n de los aspectos t&#xe9;cnicos y administrativos que mediante real decreto se establezcan para que &#xe9;sta se lleve a cabo."/>
<node CREATED="1411591495754" MODIFIED="1411591495754" TEXT="g) Intervenir en las relaciones entre operadores o entre operadores y otras entidades que se beneficien de las obligaciones de acceso e interconexi&#xf3;n, con objeto de fomentar y, en su caso, garantizar la adecuaci&#xf3;n del acceso, la interconexi&#xf3;n y la interoperabilidad de los servicios, en los t&#xe9;rminos establecidos en el art&#xed;culo 12 de la presente Ley y su normativa de desarrollo"/>
<node CREATED="1411591495756" MODIFIED="1411591495756" TEXT="h) Determinar la cuant&#xed;a que supone el coste neto en la prestaci&#xf3;n del servicio universal, a que se refiere al art&#xed;culo 27 de la presente Ley."/>
<node CREATED="1411591495757" MODIFIED="1411591495757" TEXT="i) Definir y revisar la metodolog&#xed;a para determinar el coste neto del servicio universal, tanto en lo que respecta a la imputaci&#xf3;n de costes como a la atribuci&#xf3;n de ingresos, que deber&#xe1; basarse en procedimientos y criterios objetivos, transparentes, no discriminatorios y proporcionales y tener car&#xe1;cter p&#xfa;blico."/>
<node CREATED="1411591495758" MODIFIED="1411591495758" TEXT="j) Establecer el procedimiento para cuantificar los beneficios no monetarios obtenidos por el operador u operadores encargados de la prestaci&#xf3;n del servicio universal."/>
<node CREATED="1411591495759" MODIFIED="1411591495759" TEXT="k) Decidir la imposici&#xf3;n de obligaciones a los operadores que dispongan de interfaces de programa de aplicaciones (API) y gu&#xed;as electr&#xf3;nicas de programaci&#xf3;n (EPG) para que faciliten el acceso a estos recursos, en la medida que sea necesario para garantizar el acceso de los usuarios finales a determinados servicios digitales de radiodifusi&#xf3;n y televisi&#xf3;n."/>
<node CREATED="1411591495760" MODIFIED="1411591495760" TEXT="l) Ser consultada por el Gobierno y el Ministerio de Industria, Energ&#xed;a y Turismo en materia de comunicaciones electr&#xf3;nicas, particularmente en aquellas materias que puedan afectar al desarrollo libre y competitivo del mercado. Igualmente podr&#xe1; ser consultada en materia de comunicaciones electr&#xf3;nicas por las comunidades aut&#xf3;nomas y las corporaciones locales.  En el ejercicio de esta funci&#xf3;n, participar&#xe1;, mediante informe, en el proceso de elaboraci&#xf3;n de normas que afecten a su &#xe1;mbito de competencias en materia de comunicaciones electr&#xf3;nicas."/>
<node CREATED="1411591495761" MODIFIED="1411591495761" TEXT="m) Realizar las funciones de arbitraje, tanto de derecho como de equidad, que le sean sometidas por los operadores de comunicaciones electr&#xf3;nicas en aplicaci&#xf3;n de la Ley 60/2003, de 23 de diciembre, de Arbitraje."/>
<node CREATED="1411591495762" MODIFIED="1411591495762" TEXT="n) Realizar las funciones atribuidas de manera expresa por la normativa comunitaria, la presente Ley y su normativa de desarrollo."/>
<node CREATED="1411591495763" MODIFIED="1411591495763" TEXT="&#xf1;) Realizar cualesquiera otras funciones que le sean atribuidas por ley o por real decreto."/>
</node>
</node>
</node>
<node CREATED="1411589471147" ID="ID_201438935" MODIFIED="1411631866123" POSITION="right" TEXT="T&#xcd;TULO VII: Tasas en materia de telecomunicaciones">
<node CREATED="1411591534464" FOLDED="true" ID="ID_1605197116" MODIFIED="1411631868745" TEXT="Art&#xed;culo 71. Tasas en materia de telecomunicaciones.">
<node CREATED="1411620559704" ID="ID_752698562" MODIFIED="1411627267025" TEXT="En general, para cubrir los gastos">
<node CREATED="1411627267026" ID="ID_1856284133" MODIFIED="1411627273861" TEXT="Administrativos"/>
<node CREATED="1411627274264" ID="ID_1632368561" MODIFIED="1411627293481" TEXT="Gesti&#xf3;n, control y ejecuci&#xf3;n">
<node CREATED="1411627293482" ID="ID_1532622134" MODIFIED="1411627301780" TEXT="Regimen establecido por la ley"/>
<node CREATED="1411627306007" ID="ID_1605426631" MODIFIED="1411627324981" TEXT="Derechos de uso">
<node CREATED="1411627325936" ID="ID_804906313" MODIFIED="1411627335901" TEXT="Dominio p&#xfa;blico"/>
<node CREATED="1411627336296" ID="ID_818124231" MODIFIED="1411627350389" TEXT="Dominio p&#xfa;blico radioel&#xe9;ctrico"/>
<node CREATED="1411627350824" ID="ID_125311564" MODIFIED="1411627359677" TEXT="Numeraci&#xf3;n"/>
</node>
</node>
<node CREATED="1411627361336" ID="ID_1432256479" MODIFIED="1411627372197" TEXT="Notificaciones"/>
<node CREATED="1411627372656" ID="ID_1814888634" MODIFIED="1411627399341" TEXT="Cooperaci&#xf3;n internacional, normalizaci&#xf3;n y armonizaci&#xf3;n"/>
</node>
</node>
</node>
<node CREATED="1375284095401" FOLDED="true" ID="ID_209314566" MODIFIED="1411631838966" POSITION="right" TEXT="T&#xed;tulo VIII: Inspecci&#xf3;n y r&#xe9;gimen sancionador">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375284122222" ID="ID_539731910" MODIFIED="1411631785773" TEXT="Funci&#xf3;n inspectora">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375284352747" ID="ID_1987377493" MODIFIED="1411501549840" TEXT="MINETUR">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375284377599" ID="ID_72117496" MODIFIED="1411509788648" TEXT="Servicios y redes de telecomunicaciones">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1411501614314" ID="ID_360336710" MODIFIED="1411501628215" TEXT="Incluyendo servicios de tarificaci&#xf3;n especial"/>
</node>
<node CREATED="1411501582731" ID="ID_1965659953" MODIFIED="1411509759961" TEXT="Equipos y aparatos">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384015914193" ID="ID_1912342852" MODIFIED="1411509759961" TEXT="Espectro radioel&#xe9;ctrico">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375284348121" ID="ID_1392646853" MODIFIED="1384015861371" TEXT="CNMC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375284400123" ID="ID_460809323" MODIFIED="1411509797091" TEXT="Actividades de los operadores de telecomunicaciones">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375284509732" ID="ID_1377840869" MODIFIED="1411631795179" TEXT="Infracciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375284517683" ID="ID_445459689" MODIFIED="1384967794172" TEXT="Muy graves">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1411502010423" ID="ID_73208900" MODIFIED="1411509748756" TEXT="No disponer de la habilitaci&#xf3;n">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1411502057046" ID="ID_1394068403" MODIFIED="1411509748763" TEXT="Utilizaci&#xf3;n del EPR sin concesi&#xf3;n, o sin seguir el CNAF">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1411502227451" ID="ID_1681389569" MODIFIED="1411509748763" TEXT="Emisiones radioel&#xe9;ctricas que perjudiquen el desarrollo del EPR o el CNAF">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375284776876" ID="ID_1717530707" MODIFIED="1411509748763" TEXT="Interferencias deliberadas">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1411502352723" ID="ID_1447136871" MODIFIED="1411509748762" TEXT="Importaci&#xf3;n o venta al por mayor de equipos no autorizados">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375284618861" ID="ID_414182705" MODIFIED="1411509748762" TEXT="Interceptaci&#xf3;n de comunicaciones, as&#xed; como su divulgaci&#xf3;n">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1411502433610" ID="ID_1674748054" MODIFIED="1411509748762" TEXT="Incumplimiento de resoluciones firmes en v&#xed;a administrativa">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1411502461681" ID="ID_106708895" MODIFIED="1411509748757" TEXT="Incumplimiento grave de obligaciones en materia de acceso, interconexi&#xf3;n e interoperabilidad de servicios">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1411503715443" ID="ID_1909317215" MODIFIED="1411509748757" TEXT="Incumplimiento reiterado de infracciones graves">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375284522405" ID="ID_1427145376" MODIFIED="1384967762337" TEXT="Graves">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383677941593" ID="ID_615458689" MODIFIED="1411509751881" TEXT="Instalaci&#xf3;n de estaciones radioel&#xe9;ctricas sin autorizaci&#xf3;n">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1411502812702" ID="ID_506224305" MODIFIED="1411502900824" TEXT="Utilizaci&#xf3;n del EPR de forma no autorizada cuando afecte a otros operadores"/>
<node CREATED="1383677893104" ID="ID_751770028" MODIFIED="1411509751882" TEXT="Emisi&#xf3;n de se&#xf1;ales de identificaci&#xf3;n falsas o enga&#xf1;osas">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1411502978427" ID="ID_217334797" MODIFIED="1411503038336" TEXT="Emisi&#xf3;n de se&#xf1;ales que superen los l&#xed;mites de exposici&#xf3;n, o incumplir medidas de seguridad"/>
<node CREATED="1411503047010" ID="ID_1173874334" MODIFIED="1411503073502" TEXT="Transferencia o cesi&#xf3;n no autorizada de t&#xed;tulos habilitantes"/>
<node CREATED="1411503100114" ID="ID_1825623987" MODIFIED="1411503123654" TEXT="Instalaci&#xf3;n y utilizaci&#xf3;n de equipos no sujetos a conformidad"/>
<node CREATED="1411502352723" ID="ID_1423951974" MODIFIED="1411503184398" TEXT="Importaci&#xf3;n o venta de equipos no autorizados"/>
<node CREATED="1411503195561" ID="ID_651565520" MODIFIED="1411503200734" TEXT="Negativa a ser inspeccionado"/>
<node CREATED="1411503222713" ID="ID_749912454" MODIFIED="1411503263916" TEXT="Instalaci&#xf3;n y mantenimiento de equipos y sistemas sin declaraci&#xf3;n responsable"/>
<node CREATED="1411503308280" ID="ID_1027711033" MODIFIED="1411503436354" TEXT="Alteraci&#xf3;n de equipos e ICT (n&#xfa;mero de identificaci&#xf3;n, manuales, omisi&#xf3;n de caracter&#xed;sticas)"/>
<node CREATED="1375284824926" ID="ID_1926284187" MODIFIED="1411510474492" TEXT="Incumplimiento de obligaciones">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1411510511695" ID="ID_1656957992" MODIFIED="1411510517683" TEXT="Conformidad y acreditaci&#xf3;n de equipos"/>
<node CREATED="1411510518111" ID="ID_1424783962" MODIFIED="1411510556011" TEXT="Negativa a cumplir condiciones prestaci&#xf3;n servicio"/>
<node CREATED="1411510556631" ID="ID_1946662386" MODIFIED="1411510591194" TEXT="Relativas a integridad y seguridad de comunicaciones"/>
<node CREATED="1411510619638" ID="ID_920833574" MODIFIED="1411510659250" TEXT="Negativa a uso compartido de infraestructuras y recursos"/>
<node CREATED="1411510686628" ID="ID_1002604414" MODIFIED="1411510705457" TEXT="Derivadas de normas de la Comisi&#xf3;n Europea"/>
</node>
<node CREATED="1411503450767" ID="ID_211321259" MODIFIED="1411503468059" TEXT="Incumplimiento tard&#xed;o de resoluci&#xf3;n firmes en v&#xed;a administrativa"/>
<node CREATED="1411503505326" ID="ID_1193386453" MODIFIED="1411503511907" TEXT="No realizar notificaci&#xf3;n de prestaci&#xf3;n de servicios"/>
<node CREATED="1411510262474" ID="ID_1816280832" MODIFIED="1411510278838" TEXT="Expedir certificados de instalaciones falsos"/>
<node CREATED="1411510290722" ID="ID_497972555" MODIFIED="1411510307126" TEXT="Incumplimiento deliberado de intercepci&#xf3;n legal de comunicaciones"/>
<node CREATED="1411503542085" ID="ID_892056693" MODIFIED="1411503586817" TEXT="Cursar tr&#xe1;fico contrario a planes de numeraci&#xf3;n o con fines fraudulentos"/>
<node CREATED="1383677955886" ID="ID_533207966" MODIFIED="1411509742097" TEXT="No facilitar informaci&#xf3;n requerida a la Admon. pasados 3 meses el plazo">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1411503663333" ID="ID_1013765069" MODIFIED="1411503671440" TEXT="Incumplimiento de obligaciones de conservaci&#xf3;n de n&#xfa;meros"/>
<node CREATED="1411503683795" ID="ID_834640870" MODIFIED="1411503692687" TEXT="Incumplimiento reiterado de infracciones leves"/>
</node>
<node CREATED="1375284525917" ID="ID_1884709394" MODIFIED="1411503742456" TEXT="Leves">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375284869773" ID="ID_1739996694" MODIFIED="1411509739098" TEXT="Establecer comunicaciones no autorizadas">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1411503765163" ID="ID_907717050" MODIFIED="1411503795990" TEXT="No facilitar datos requeridos por la Admon."/>
<node CREATED="1411503858898" ID="ID_1042787387" MODIFIED="1411503886733" TEXT="Utilizaci&#xf3;n del EPR sin autorizaci&#xf3;n general, autorizaci&#xf3;n individual o afecci&#xf3;n demanial"/>
<node CREATED="1411503914265" ID="ID_1033042327" MODIFIED="1411503940804" TEXT="Instalaci&#xf3;n de estaciones de radioaficionado sin autorizaci&#xf3;n"/>
<node CREATED="1411503979992" ID="ID_869372543" MODIFIED="1411503999517" TEXT="Incumplimiento de intercepci&#xf3;n legal de comunicaciones, cuando no sea materia grave o muy grave"/>
<node CREATED="1411504040080" ID="ID_520632978" MODIFIED="1411510727754" TEXT="Incumplimiento de obligaciones">
<node CREATED="1411510755204" ID="ID_1023025767" MODIFIED="1411510762089" TEXT="Vulneraci&#xf3;n de derechos de usuarios finales"/>
<node CREATED="1411510729605" ID="ID_1861476543" MODIFIED="1411510733648" TEXT="En materia de calidad"/>
</node>
<node CREATED="1411504077160" ID="ID_1215931437" MODIFIED="1411504094915" TEXT="No presentaci&#xf3;n de documentaci&#xf3;n de ICTs cuando sea requerido"/>
</node>
</node>
<node CREATED="1375284896041" FOLDED="true" ID="ID_861877167" MODIFIED="1411631804019" TEXT="Sanciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375284905641" ID="ID_1149140992" MODIFIED="1411507961865" TEXT="Muy graves">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1411507967860" ID="ID_1107257750" MODIFIED="1411507980584" TEXT="Max: 20 mill. &#x20ac;"/>
<node CREATED="1411507964316" ID="ID_1730522087" MODIFIED="1411509383361" TEXT="En caso CNMC: no inferior al tanto, ni superior al quintuplo del beneficio bruto obtenido, &#xf3; 20 mill. &#x20ac;"/>
<node CREATED="1375284974171" ID="ID_1408130164" MODIFIED="1411509713839" TEXT="Inhabilitaci&#xf3;n de hasta 5 a&#xf1;os">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375284964478" ID="ID_1050414457" MODIFIED="1411507933819" TEXT="Graves">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375285005400" ID="ID_200084589" MODIFIED="1411509719182" TEXT="Max: 2 mill. &#x20ac;">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375284995093" ID="ID_1698510540" MODIFIED="1411509719183" TEXT="En caso CNMC: duplo del beneficio bruto, &#xf3; 2 mill. &#x20ac;">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375285018643" ID="ID_1705762921" MODIFIED="1411509406801" TEXT="Leves">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1411509407613" ID="ID_1783314397" MODIFIED="1411509411759" TEXT="Max. 50.000 &#x20ac;"/>
</node>
<node CREATED="1375285048188" ID="ID_58768311" MODIFIED="1411509501095" TEXT="Multa de hasta 60.000 &#x20ac; a los representantes legales o miembros &#xf3;rganos directivos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375285070497" ID="ID_405410188" MODIFIED="1411509610646" TEXT="Prescripci&#xf3;n de infracciones y sanciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375285090042" ID="ID_1863212629" MODIFIED="1411509725922" TEXT="Muy graves: 3 a&#xf1;os">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375285102354" ID="ID_1345805016" MODIFIED="1411509725924" TEXT="Graves: 2 a&#xf1;os">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375285109364" ID="ID_118650798" MODIFIED="1411509725923" TEXT="Leves: 1 a&#xf1;o">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375285163650" ID="ID_1810117705" MODIFIED="1384967487917" TEXT="Competencias sancionadoras">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375285179160" ID="ID_396170808" MODIFIED="1411509729926" TEXT="CNMC">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375285182473" ID="ID_1071588866" MODIFIED="1411509729927" TEXT="Agencia de Protecci&#xf3;n de datos">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375285199371" ID="ID_650941115" MODIFIED="1411509729927" TEXT="Secretario de Estado de Telecomunicaciones y para la Sociedad de la Informaci&#xf3;n">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</map>
