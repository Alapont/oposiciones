<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1429871660005" ID="ID_1141591445" MODIFIED="1445086915553">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T110 LGT
    </p>
    <p style="text-align: center">
      9/2014
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1429872848837" ID="ID_447409551" MODIFIED="1433929697440" POSITION="right" TEXT="1. Disps generales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445093145691" ID="ID_980795472" MODIFIED="1445093305624">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b><i>Telecos</i></b><i>:</i>&#160;servs de <u>inter&#233;s general</u>&#160;en libre competencia&#8594; <i>Servs p&#250;b</i>: Defensa, Seg p&#250;b/vial y Prot Civil
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1429872423816" ID="ID_1111730281" MODIFIED="1445093878436">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1998 (<u>libre compet</u>)&#8594; Ley 32/2003&#8594; <u>2&#186; Paq</u>Telecom (EU hacia Mercado &#218;nico)&#8594;LGT
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429873328542" ID="ID_121952526" MODIFIED="1429873332313" TEXT="&#xc1;mbito">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429873332543" ID="ID_461313747" MODIFIED="1445093366419" TEXT="Habilitaci&#xf3;n para operador, dchos y obligs, interceptaci&#xf3;n legal, conserv datos, conformidad equipos">
<node CREATED="1429873151249" ID="ID_1737596720" MODIFIED="1445093505965">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Contenidos</u>, Com AV, Soc Info y Servs P&#250;b (breve ref)&#8594; normativa propia
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="stop-sign"/>
</node>
</node>
</node>
<node CREATED="1429872872395" ID="ID_1721450547" MODIFIED="1445093595197" TEXT="Objetivos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1429872877810" ID="ID_886595094" MODIFIED="1445093838041">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Explot redes</u>&#160;y <u>prest servs</u>&#160;de com elect (obligs de serv p&#250;b), compet efectiva, uso <u>recursos</u>&#160;(espectro), desarr industria (inversi&#243;n efic, innov), ocup propiedad, neutralidad tecn, prot&#160;usuarios, mercado int EU
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1429873439682" ID="ID_353540277" MODIFIED="1433929693983" POSITION="right" TEXT="2. Explot redes y servs">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429873588172" ID="ID_1329826626" MODIFIED="1445094627388">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b><i>Reqs</i></b>: pers fis/jur <u>UE u otros</u>&#160;(acuerdos/exceps de Gob), notific a <b>Reg Operadores</b>&#160;(Minetur, por RD)
    </p>
  </body>
</html></richcontent>
<node CREATED="1445093950806" ID="ID_1601033098" MODIFIED="1445094282689">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Autoprestaci&#243;n (pero si es AP s&#237; debe comunicarlo, sin registrarse)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="stop-sign"/>
</node>
<node CREATED="1429874042039" ID="ID_1925512759" MODIFIED="1445094313636">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si no cumple reqs&#8594; resol en 15 d&#237;as h&#225;biles. No req consentim para tratar/com datos pers
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="calendar"/>
</node>
</node>
<node CREATED="1429873948938" ID="ID_487212967" MODIFIED="1429874193812" TEXT="Separaci&#xf3;n estructural">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429873955347" ID="ID_1733645641" MODIFIED="1429895099969" TEXT="Entidades que operen en otro sector adem&#xe1;s del sector teleco &#x2192; Cuentas separadas y auditadas">
<node CREATED="1429874221613" ID="ID_58224179" MODIFIED="1429874238602" TEXT="Ingresos brutos &lt;50M&#x20ac; (por RD)">
<icon BUILTIN="stop-sign"/>
</node>
</node>
</node>
<node CREATED="1429874250428" ID="ID_393275419" MODIFIED="1429874257245" TEXT="Prestaci&#xf3;n por AP">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429874259532" ID="ID_1225181148" MODIFIED="1445094571136">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ppio de inversor priv:</i>&#160;<u>sep cuentas</u>&#160;y <u>estructural</u>&#160;(opers-regulaci&#243;n), neutral, transp y cumplim normativa
    </p>
  </body>
</html></richcontent>
<node CREATED="1429874381390" ID="ID_700216573" MODIFIED="1445094527779">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Otros opers con dcho de acceso y&#160;<u>uso compartido</u>&#160;de infraestr
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1429874771782" ID="ID_195847420" MODIFIED="1445094687830">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b><i>Interconex</i></b><i>&#160;<b>de redes</b></i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1445094687836" ID="ID_1534581047" MODIFIED="1445094694567">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Opers de redes p&#250;b<u>obligados</u>&#160;a negociarla sin restriccs y con confid ( <u>CNMC</u>&#160;interviene si conflicto)
    </p>
  </body>
</html></richcontent>
<node CREATED="1429874842395" ID="ID_1131796218" MODIFIED="1429874898431">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Oper fuera de UE que solicite acceso <u>no debe notificar a Registro</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1429875036417" ID="ID_1976894791" MODIFIED="1445094937924">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Regul de mercados
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429875136833" ID="ID_501941987" MODIFIED="1445094805674">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>CNMC</u>&#160;def y analiza mercados de ref para determinar si existe <u>competencia efectiva</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1445094807532" ID="ID_1640213014" MODIFIED="1445094911858">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Seg&#250;n recos CE/ORECE y previo informe de Minetur y MinEco. Resol en BOE
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1429875247085" ID="ID_425495483" MODIFIED="1445095789780">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Opers con <u>poder significativo</u>&#160;&#8594;&#160;Obligs espec&#237;ficas
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1429875331594" ID="ID_1947460374" MODIFIED="1445094960182">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Transp (contab, redes), no discrim (=condics), sep cuentas, acceso a redes y ctrl precios
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429875779716" ID="ID_306516491" MODIFIED="1445095036107">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si no es suficiente&#8594;
    </p>
  </body>
</html></richcontent>
<node CREATED="1445095036113" ID="ID_1802347710" MODIFIED="1445097966715">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Sep funcional:</i>&#160;&#160;traspaso de suministro de prods de acceso a empresa indep (puede ser volunt)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1429875990950" ID="ID_997022714" MODIFIED="1429876066925" TEXT="N&#xba;, direcc y nombre">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429876028141" ID="ID_1109150619" MODIFIED="1445095295607">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Planes nacionales</i>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1445095295611" ID="ID_800754424" MODIFIED="1445095464285">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Servs para los q pueden utilizarse, reqs y precios
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1429876110890" ID="ID_204368123" MODIFIED="1445095363899">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Minetur</u>&#160;propone y modif plan (previo informe de CNMC) y&#160; <u>Gobierno</u>&#160;aprueba por RD
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1429876145325" ID="ID_568290665" MODIFIED="1445095391210">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Otorg dchos de uso de recs p&#250;b
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429876167654" ID="ID_1763586367" MODIFIED="1445095395939" TEXT="Minetur, 3 semanas desde solicitud o 6 si selecci&#xf3;n comparativa/competitiva (sil neg) ">
<icon BUILTIN="calendar"/>
</node>
</node>
</node>
<node CREATED="1429876317022" ID="ID_481047143" MODIFIED="1445095514088">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Conserv de n&#186; abonado:</i>&#160;garantizado <u>previa solicitud</u>, indep del operador (coste a cargo de oper)
    </p>
  </body>
</html></richcontent>
<node CREATED="1429895644767" ID="ID_157067392" MODIFIED="1445095597139">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si cambia de domicilio (tfn fijo).&#160;Condics por RD seg&#250;n&#160; <u>CNMC</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="stop-sign"/>
</node>
</node>
<node CREATED="1429876334756" ID="ID_850670071" MODIFIED="1429876359285" TEXT="N&#xba; para servs sociales armonizados (EU)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429876347908" ID="ID_1063142963" MODIFIED="1429876357875">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comienzan por <u>116</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1433928934101" ID="ID_1161637203" MODIFIED="1445095639731">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>RD 381/2015:</i>Medidas contra tr&#225;fico no permitido/irreg con fines fraud&#160;
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1433929029689" ID="ID_621355074" MODIFIED="1445095803411">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>No permitido: </i>usa numeraci&#243;n no autorizada o hace uso indebido de num
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1433929067282" ID="ID_60504277" MODIFIED="1445097964125">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si opers detectan&#8594; <u>bloquearlo y/o retener pagos</u>&#160;y notificar a SETSI y opers afectados (&lt;2 d&#237;as)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1429876464758" ID="ID_524598303" MODIFIED="1433929699062" POSITION="right" TEXT="3. Obligs y dchos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429876615800" ID="ID_48662834" MODIFIED="1429876636221" TEXT="Obligs de serv p&#xfa;b">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429883336057" ID="ID_435383242" MODIFIED="1429883677768">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Garantizar servs de com el&#233;ctr disponibles en todo el territorio nacional, con calidad y precio asequible
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1429883415383" ID="ID_1472463580" MODIFIED="1445096522323">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ctrl de precios por Minetur y evisi&#243;n de alcance por Gobierno
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1429876622766" ID="ID_1165968908" MODIFIED="1445096105956">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Serv Universal
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429883387807" ID="ID_1580847050" MODIFIED="1445095871898">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tfn <u>fijo</u>/fax, Internet-<u>1Mbps</u>, gu&#237;a, cabinas (emerg gratis) y acceso a discapacs (tarifas especials)
    </p>
  </body>
</html></richcontent>
<node CREATED="1433928663186" ID="ID_1057591315" MODIFIED="1433928719976">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Agenda DigEU: </i>10 Mbps (2017), 30 Mbps (2020)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429883693516" ID="ID_487222210" MODIFIED="1445096033336">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Designaci&#243;n de
    </p>
    <p>
      opers (1 &#243; +)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429883699740" ID="ID_1204707238" MODIFIED="1445096544604">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Por <u>Minetur</u>&#160;(RD), si no est&#225; garantizado por libre mercado (posible licitaci&#243;n)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429883770454" ID="ID_815110118" MODIFIED="1445096074857">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si CNMC determina&#160; <u>carga injustificada</u>&#160;&#8594;
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1445096074861" ID="ID_482809551" MODIFIED="1445097961216">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Financ por reparto entre opers con ingrs brutos &gt;100M&#8364; (dep&#243;sito en Fondo Nac de Serv Univ)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1429884150925" ID="ID_874867867" MODIFIED="1445096380581">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Otras impuestas por el Gobierno:</i>&#160;por Defensa, Seg p&#250;b/vial y Protec Civil +&#8594;
    </p>
  </body>
</html></richcontent>
<node CREATED="1445096198181" ID="ID_1211162686" MODIFIED="1445096370554">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Previo informe CNMC y AP territ:&#160; </i>por cohesi&#243;n, nuevos servs, colectivos especiales y fehaciencia
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1429876636725" ID="ID_1472170440" MODIFIED="1429885413930" TEXT="Dchos de opers">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429884736022" ID="ID_1930062516" MODIFIED="1429884804171" TEXT="Ocupaci&#xf3;n">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429884754339" ID="ID_20894077" MODIFIED="1445096606598">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Prop Priv:</i>&#160;solo si es necesario y no hay alternativas,&#160; <u>exprop</u>&#160;(urgente) o paso forzoso
    </p>
  </body>
</html></richcontent>
<node CREATED="1429884782868" ID="ID_1104042839" MODIFIED="1429884797096" TEXT="Resuelve Minetur y opers asumen costes">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1429884824084" ID="ID_1945015956" MODIFIED="1445096591424">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Dominio p&#250;b:</i>&#160;si es necesario, titulares garantizan acceso en condiciones &#243;ptimas
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429885071852" ID="ID_1526318924" MODIFIED="1445096639822">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ubic y uso compartido:</i>&#160;opers <u>pueden</u>&#160;acordar condiciones (puede ser impuesto por Minetur)
    </p>
  </body>
</html></richcontent>
<node CREATED="1429886168729" ID="ID_874314720" MODIFIED="1445097960776">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      AP, transp p&#250;b y otros (pe ADIF) facilitar&#225;n acceso a sus recs (negoc libre)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1429876659740" ID="ID_1991043965" MODIFIED="1429885417410" TEXT="Despliegue redes">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429885167767" ID="ID_1106527607" MODIFIED="1429886858986" TEXT="Colab AP-AGE para facilitarlo">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429885301992" ID="ID_1042685441" MODIFIED="1429885452539" TEXT="Uso de canaliz subterr&#xe1;neas o interior edificios. Si no, a&#xe9;reo o fachadas">
<node CREATED="1429885383401" ID="ID_207584075" MODIFIED="1429885395681" TEXT="Protec hist&#xf3;rico-art&#xed;stica">
<icon BUILTIN="stop-sign"/>
</node>
</node>
<node CREATED="1429885345187" ID="ID_1383117710" MODIFIED="1445097960576">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No pedir doc en poder de AP y <u>no exigir licencia</u>&#160;&#160;para urgencia, planes aprobados o innov/adapt tecn
    </p>
  </body>
</html></richcontent>
<node CREATED="1445096797720" ID="ID_898402254" MODIFIED="1445096843846">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Solo declar&#160;resonsable
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1429885748016" ID="ID_646099989" MODIFIED="1429886859369" TEXT="Colab AP-Minetur">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429885758908" ID="ID_649324951" MODIFIED="1445096979811">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#211;rgs que aprueben instrumentos urban&#237;sticos (pe Ayunts)&#8594; previo&#160; <u>informe</u>&#160;de Minetur
    </p>
  </body>
</html></richcontent>
<node CREATED="1429885863835" ID="ID_884790819" MODIFIED="1429885919285" TEXT="Emisi&#xf3;n en &lt;3 meses (sil pos)">
<icon BUILTIN="calendar"/>
</node>
</node>
<node CREATED="1429885969146" ID="ID_727784461" MODIFIED="1445097067362">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Punto de Info &#218;nico:</i>&#160;&#160;(por Minetur) AP pueden adherirse
    </p>
  </body>
</html></richcontent>
<node CREATED="1445097055307" ID="ID_651864995" MODIFIED="1445097055310">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Opers solicitan info y presentan permisos y declar para obtener licencias
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1429886061688" ID="ID_512531548" MODIFIED="1445097109649">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Previsi&#243;n&#160;de infraestruct:</i>&#160;instal de obra civil y recs en proys de urbaniz y obras civiles con financ p&#250;b&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1429876671227" ID="ID_1074147636" MODIFIED="1429886415351" TEXT="Obligs de opers">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429886565470" ID="ID_561788866" MODIFIED="1445097205649">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Secretos de coms:</i>&#160;garantizado y seg&#250;n Ley de Enjuiciam (condics de&#160; <u>Interceptaci&#243;n</u>&#160;en RD 424/2005)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429886593839" ID="ID_875436987" MODIFIED="1445097217904">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Prot de datos personales
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429886640156" ID="ID_1389822853" MODIFIED="1445097302949">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Medidas&#160;m&#237;n:</i>&#160;solo personal autorizado accede, protecci&#243;n contra modifics, app de pol&#237;tica de seg
    </p>
  </body>
</html></richcontent>
<node CREATED="1429886718541" ID="ID_1650836488" MODIFIED="1445097312379">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Conserv datos de tr&#225;fico:&#160;<u>1 a&#241;o</u>&#160;(Ley 25/2007)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1429886600751" ID="ID_811465552" MODIFIED="1429886714496" TEXT="Si hay violaci&#xf3;n, notificar a AEPD y abonado (si datos no cifrados) ">
<node CREATED="1445097276965" ID="ID_65648798" MODIFIED="1445097294600" TEXT="AEPD puede examinar las medidas y dar recos"/>
</node>
</node>
<node CREATED="1429886887816" ID="ID_812049645" MODIFIED="1445097362517">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Seg en redes y servs:</i>&#160;si violaci&#243;n, notificar a Minetur, que realiza&#8594;
    </p>
  </body>
</html></richcontent>
<node CREATED="1429886901856" ID="ID_1880335352" MODIFIED="1429887142370" TEXT="Informe anual a ENISA, comunic incidentes a Secret de Seg (MinInt) y supervisi&#xf3;n a opers (audit) "/>
</node>
</node>
<node CREATED="1429876681778" ID="ID_1912703962" MODIFIED="1429876683725" TEXT="ICTs">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429887153613" ID="ID_1124323745" MODIFIED="1429887202213">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Opers pueden instalar tramos finales de acceso ultrarr&#225;pido en prop horz o arrendams &gt;1 a&#241;o
    </p>
  </body>
</html></richcontent>
<node CREATED="1429887204582" ID="ID_374978530" MODIFIED="1445097390065" TEXT="= RD 346/2011 (ICTs)"/>
</node>
<node CREATED="1429887245770" ID="ID_1883045876" MODIFIED="1445097406629">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Instal:</i>&#160;com escrita a comunidad. Si en &lt;1mes ninguno est&#225; interesado, no puede realizarse
    </p>
  </body>
</html></richcontent>
<node CREATED="1429887305902" ID="ID_989970886" MODIFIED="1429894051226">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Minetur</u>&#160;fija aspectos t&#233;cnicos, impone obligs y posee inventario de ICTs
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1429876688133" ID="ID_1604715350" MODIFIED="1429876691039" TEXT="Dchos de usuarios">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429887525018" ID="ID_1081274073" MODIFIED="1429887527426" TEXT="Espec&#xed;ficos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429887527887" ID="ID_430563777" MODIFIED="1445097503972">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Resol de&#160;contrato sin penaliz, cambio de oper en&#160; <u>1 d&#237;a lab</u>&#160;y&#160;conserv n&#186; (mvl y fijo), continuidad de serv (compens), factura detallada o no, ocultar id (salvo 112), acceso equiv con discapac, info de servs, emergs gratis, att cliente, detener desv&#237;os a su tfn
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429887788538" ID="ID_294660868" MODIFIED="1429887803899" TEXT="Protecci&#xf3;n datos y privacidad">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429887805297" ID="ID_649343034" MODIFIED="1445097559719">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No llamada comercial sin consentim,&#160;datos localiz an&#243;nimos y cancelados al final, figurar o no en gu&#237;as
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429888336702" ID="ID_25790896" MODIFIED="1429888361975" TEXT="Resoluci&#xf3;n de controversias">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429888342085" ID="ID_838891571" MODIFIED="1429888368463">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Proced <u>extrajudicial</u>, transp, no discrim, sencillo y gratuito
    </p>
  </body>
</html></richcontent>
<node CREATED="1429888353678" ID="ID_1397254956" MODIFIED="1429888506056" TEXT="Establecido por Minetur, al igual que los reqs m&#xed;n de QoS">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1429876699780" ID="ID_816113564" MODIFIED="1433929630763" POSITION="right" TEXT="4. Conformidad de equipos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429889005816" ID="ID_45032257" MODIFIED="1429889572686">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cualquier equipo debe <u>evaluar</u>&#160;su conformidad con reqs de normativa (por RD) e incorporar&#160; <u>marcado</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1429889143708" ID="ID_1702837672" MODIFIED="1429894108155" TEXT="Control por Minetur">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1429889166619" ID="ID_1517578799" MODIFIED="1445098091012">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Equipos con reqs de otro <u>pa&#237;s UE u otro</u>&#160;de fuera (con <u>reconocimiento mutuo</u>&#160;&#160;acordado)&#8594; v&#225;lidos&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1429889308373" ID="ID_1327010465" MODIFIED="1433927023385">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Equipos con par&#225;ms radio <u>no armonizados</u>&#160;en UE&#8594; previa autoriz del Minetur y eval
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429889403647" ID="ID_1892625748" MODIFIED="1445098098281">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Prestaci&#243;n de instal y mantenim
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429889437249" ID="ID_213315085" MODIFIED="1429889621035">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Libre competencia&#8594; Personas f&#237;s/jur UE u otro (seg&#250;n acuerdos y excepciones del Gob)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429889466960" ID="ID_24048458" MODIFIED="1445098156523">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Previa declaraci&#243;n responsable de cumplim de reqs al <u>Reg&#160;de Empresas Instaladoras</u>&#160;(por SETSI)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1429876719060" ID="ID_670716323" MODIFIED="1433929631796" POSITION="right" TEXT="5. Dominio P&#xfa;b Radioel&#xe9;ctrico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429889850375" ID="ID_705626954" MODIFIED="1429889872528" TEXT="Espectro: 9 kHz- 3000 GHz">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1429889876674" ID="ID_1035643937" MODIFIED="1445098124419">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Control del&#160; <u>Estado</u>. Coop con CE y pa&#237;ses UE y seg&#250;n recos internac (UIT)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1433921393696" ID="ID_1269559242" MODIFIED="1445098234135">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>RD 805/2014: </i>Plan TDT y dividendo dig (parte espectro liberada tras implantaci&#243;n de TDT)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1433928883571" ID="ID_530441797" MODIFIED="1445098223741">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      470-790MHz&#8594; posible interf con 4G (800MHz)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1429889928279" ID="ID_1689597421" MODIFIED="1429890122211" TEXT="Ppios de adm">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429889941568" ID="ID_1513635788" MODIFIED="1445098335603">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Uso eficaz y eficiente y +competencia
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429890052726" ID="ID_796751880" MODIFIED="1445098342461">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Espectro finito&#8594; requiere t&#237;tulo habilitante
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1429890033332" ID="ID_804597116" MODIFIED="1445098330405" TEXT="Neutralidad tecnol&#xf3;gica">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429890060577" ID="ID_462878383" MODIFIED="1445098404865">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Restrics en inal&#225;mb (no interfs perj, prot salud, QoS y uso compart m&#225;x) y ToS (seg, cohesi&#243;n social)&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1429890138737" ID="ID_1422737052" MODIFIED="1429890176708" TEXT="Mercado secundario">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429890146048" ID="ID_1637003916" MODIFIED="1429890163001" TEXT="T&#xed;tulos transferibles y dchos de uso cedidos total/parcialmente"/>
</node>
</node>
<node CREATED="1429890373646" ID="ID_1311505397" MODIFIED="1429890379743" TEXT="Facultades del Gobierno">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429890380126" ID="ID_1642052336" MODIFIED="1445098518659">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Elaborar y aprobar planes de utiliz (<u>CNAF</u>&#160;),&#160;ctrl niveles emisi&#243;n y uso adecuado del espectro, proceds y condics de dchos de uso y de t&#237;tulos (incluye &#160;<u>experimentos&#160;o eventos</u>), reasign de freqs
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1429890588315" ID="ID_1745617577" MODIFIED="1429890669952" TEXT="Tipos de uso del espectro">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429890640294" ID="ID_828260921" MODIFIED="1429890668855" TEXT="Com&#xfa;n ">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429890661895" ID="ID_78126869" MODIFIED="1429890952692" TEXT="No requiere t&#xed;tulo. pe Wifi">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1429890670589" ID="ID_1740509585" MODIFIED="1429890706668" TEXT="Especial">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429890673030" ID="ID_1770847341" MODIFIED="1445098561878">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Explot <u>compartida</u>&#160;de bandas, sin l&#237;mite de usuarios. pe taxi, barcos, aviones
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1429890692148" ID="ID_1233147069" MODIFIED="1429890707044" TEXT="Privativo">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429890695323" ID="ID_803578610" MODIFIED="1445098543899">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Explot <u>exclusiva por un l&#237;mite</u>&#160;&#160;de usuarios (limit f&#237;sica del BW). pe TDT
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1429890735642" ID="ID_1757700236" MODIFIED="1429891618218" TEXT="T&#xed;tulos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1429891597403" ID="ID_1896870959" MODIFIED="1429891599644" TEXT="Tipos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429890799926" ID="ID_124719930" MODIFIED="1429890845090" TEXT="Autoriz General">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429890804825" ID="ID_1584563631" MODIFIED="1429890920302">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Uso <u>especial</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1429890812946" ID="ID_21618403" MODIFIED="1429890832572" TEXT="Solo requiere notific a SETSI  (y pago tasas)"/>
</node>
</node>
<node CREATED="1429890845440" ID="ID_183771440" MODIFIED="1429890852538" TEXT="Autoriz Individual">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429890857769" ID="ID_1998512794" MODIFIED="1429890929389">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Uso <u>especial</u>&#160;por <u>radioaficionados</u>&#160;u otros sin contenido eco
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429890873968" ID="ID_536445659" MODIFIED="1429890936992">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Uso <u>privativo</u>&#160;para <u>autoprestaci&#243;n</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1429890887944" ID="ID_603456720" MODIFIED="1429890913648" TEXT="Salvo AP&#x2192; requieren Afectaci&#xf3;n Demanial (otro tipo de t&#xed;tulo)"/>
</node>
</node>
<node CREATED="1429890971027" ID="ID_640679035" MODIFIED="1429890975828" TEXT="Concesi&#xf3;n Admin">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429890976682" ID="ID_668703070" MODIFIED="1429890991491">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Uso <u>privativo</u>&#160;en resto de casos (la +com&#250;n)
    </p>
  </body>
</html></richcontent>
<node CREATED="1429891003032" ID="ID_195108616" MODIFIED="1445098590911">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si se limita el n&#186; de concesiones (freqs muy deseadas)&#8594; proced abierto de <b>licitaci&#243;n</b>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1429891068471" ID="ID_1829104946" MODIFIED="1429891626257" TEXT="Otorgamiento">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429891185876" ID="ID_1842945894" MODIFIED="1445098637419">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Competencia de <b>SETSI </b>(Minetur solo si es por licitaci&#243;n)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1429891972074" ID="ID_1171624375" MODIFIED="1429891997203" TEXT="SETSI tb protege el dominio (ocupaci&#xf3;n en freqs no otorgadas)"/>
</node>
<node CREATED="1429891080037" ID="ID_1684823347" MODIFIED="1429891305878">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>6 semanas</u>&#160;desde solicitud en Registro en &#243;rg competente
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="calendar"/>
<node CREATED="1429891090493" ID="ID_1242955736" MODIFIED="1429891400545">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Licitaci&#243;n ( <u>8 meses</u>), requiere coord internac o afecta a posiciones orbitales
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="stop-sign"/>
</node>
</node>
<node CREATED="1429891408494" ID="ID_760148743" MODIFIED="1429891444567" TEXT="Duraci&#xf3;n dchos de uso privativo">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429891413359" ID="ID_1049723609" MODIFIED="1429891490450" TEXT="Sin l&#xed;mite de usuarios">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429891457468" ID="ID_1523046373" MODIFIED="1429891544925">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      31 diciembre del <u>5&#186; a&#241;o</u>&#160;(prorrogable por +5)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429891478305" ID="ID_137031282" MODIFIED="1429891490777" TEXT="Con l&#xed;mite de usuarios">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429891492217" ID="ID_1433976077" MODIFIED="1445098654490">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Seg&#250;n proced <u>licitaci&#243;n</u>, pero <u>&lt;20 a&#241;os</u>&#160;(incluye pr&#243;rrogas) y sin renov autom&#225;tica
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1429891578973" ID="ID_252948568" MODIFIED="1429891646774" TEXT="Causas de revocaci&#xf3;n">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429891651620" ID="ID_913752012" MODIFIED="1429891832862" TEXT="Incumpl condics, no pago impuesto, uso ineficaz, revoc sucesiva (2 autoriz/a&#xf1;o), uso fines distintos  ">
<node CREATED="1429891763433" ID="ID_532829916" MODIFIED="1429891772171" TEXT="Revoc por Minetur"/>
</node>
</node>
<node CREATED="1429891774312" ID="ID_1154695644" MODIFIED="1429891776978" TEXT="Extinci&#xf3;n">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429891777688" ID="ID_625652649" MODIFIED="1429891921380" TEXT="Muerte/renuncia, p&#xe9;rdida condic oper, impago tasas, incumpl grave, acuerdo o no adecuac a CNAF"/>
</node>
</node>
</node>
<node CREATED="1429876737109" ID="ID_178293243" MODIFIED="1433929633191" POSITION="right" TEXT="6. Adm de las telecos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429893033164" ID="ID_1236198747" MODIFIED="1429893036869" TEXT="ORECE">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429872544141" ID="ID_93237809" MODIFIED="1429893046545">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Organismos Reguladores EU de Comunics Electr&#243;nicas
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429893048571" ID="ID_1206891240" MODIFIED="1429893052452" TEXT="ANRs">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429872566595" ID="ID_1232482548" MODIFIED="1429894342526">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Autoridades Nacionales de Reglamentaci&#243;n (Gobierno, Minetur, CNMC y MinEco)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1429874575396" ID="ID_1028517825" MODIFIED="1429895175315" TEXT="Requieren info a opers para comprobar cumplim Ley, coop con UE y apoyo de objs de ORECE"/>
</node>
</node>
<node CREATED="1429893172073" ID="ID_1908619617" MODIFIED="1429893174769" TEXT="CNMC">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429875447919" ID="ID_1212412160" MODIFIED="1433926967949">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comisi&#243;n&#160;Nac de Mercado y Competencia (integra CMT)
    </p>
  </body>
</html></richcontent>
<node CREATED="1429875460015" ID="ID_1782989811" MODIFIED="1433926842610">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Analiza mercados de ref, resuelve conflictos, asesora, informes, inspecci&#243;n
    </p>
  </body>
</html></richcontent>
<node CREATED="1433926507832" ID="ID_1540986824" MODIFIED="1433926572840">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#211;rg p&#250;b <u>indep</u>&#160;adscrito a MinEco
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1433926693189" ID="ID_1964405138" MODIFIED="1433926958910">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Consejo (cada <u>6 a&#241;os</u>), Presi y &#211;rgs Dir
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1429876762736" ID="ID_1750153128" MODIFIED="1433929634249" POSITION="right" TEXT="7. Tasas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429893336314" ID="ID_1707703258" MODIFIED="1433926900107">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cubrir gastos (recauda <u>Minetur</u>)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1429893342648" ID="ID_348517957" MODIFIED="1429893586414">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Admin:</i>&#160;regulaci&#243;n y actos adm, coop internac, armoniz, normaliz y an&#225;lisis de mercado
    </p>
  </body>
</html></richcontent>
<node CREATED="1429893544349" ID="ID_1608836587" MODIFIED="1433921377978">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>De gesti&#243;n:</i>&#160;del r&#233;gimen establecido, dchos de ocup y uso, num&#160;y notifics
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1429893628448" ID="ID_379205720" MODIFIED="1429893631050" TEXT="Tipos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429893631744" ID="ID_1148090292" MODIFIED="1445098738233">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>General de operadores: </i>app de r&#233;gimen jur por ANRs (gesti&#243;n ctrl y ejec)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1429893682873" ID="ID_1484289748" MODIFIED="1433921319398">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Por num, dir y nombre:</i>&#160;otorg dcho de uso de num/dir/nombre por Minetur
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429893720518" ID="ID_428561721" MODIFIED="1433921327609">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Por reserva del dominio p&#250;blico radioel&#233;ctrico: </i>para uso privativo o especial
    </p>
  </body>
</html></richcontent>
<node CREATED="1429893748476" ID="ID_1307906154" MODIFIED="1445098782593">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>De telecos:</i>&#160;certificaciones registrales, rx de proyecto, inspeccs, concesiones...
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1433927108952" ID="ID_1645495146" MODIFIED="1433929636847" POSITION="right" TEXT="8. Infracciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433927278497" ID="ID_430192764" MODIFIED="1445100087333">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sanciona Minetur (SETSI), CNMC o AEPD (si es de prot datos)
    </p>
  </body>
</html></richcontent>
<node CREATED="1433927326560" ID="ID_683066378" MODIFIED="1445098822847">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Inspecciones: </i>opers obligs a facilitarlas
    </p>
  </body>
</html></richcontent>
<node CREATED="1445098823540" ID="ID_311668503" MODIFIED="1445098862437">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>SETSI</u>&#160;(DPR, servs y redes, equipos e instals) y <u>CNMC</u>&#160; (activs de opers, docum)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1445098875690" ID="ID_106005077" MODIFIED="1445098888951" TEXT="Personal de inspecci&#xf3;n puede precintar y hacer mediciones"/>
</node>
</node>
<node CREATED="1433927602841" ID="ID_1482185352" MODIFIED="1433927605101" TEXT="Muy graves">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433927605498" ID="ID_740816973" MODIFIED="1445098935003">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Emisiones perj CNAF, interf deliberadas,&#160;uso de dom sin concesi&#243;n o no CNAF, incump reqs de explot, venta de equips sin eval, incump resols, incump grave de obligs acceso/interop, incump grave de conserv n&#186;, &gt;2 graves= en 3a&#241;os, activs sin habilit, no att solicitud de cesaci&#243;n, intercept sin autoriz
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1433928219449" ID="ID_210906833" MODIFIED="1433928224797" TEXT="Sanciones">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1433928234054" ID="ID_1060290133" MODIFIED="1445098985571">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Muy graves:</i>&#160;&lt;20M&#8364; o &lt; 5&#183;beneficio bruto si sanciona CNMC (opc&#160;inhab 5 a&#241;os) / Presc: 3 a&#241;os
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1433928347010" ID="ID_612985483" MODIFIED="1445100090881">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Graves: </i>&lt;2M&#8364; o &lt;2&#183;bb si sanciona CNMC / Presc: 2 a&#241;os
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1433928375907" ID="ID_1380701318" MODIFIED="1445100091323">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Leves: &lt;50k&#8364;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1433928429668" ID="ID_1884169749" MODIFIED="1433928484882">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Repre/&#243;rg dir de persona jur:</i>&#160;60k&#8364; (muy graves), 30k&#8364; (graves) o 5k&#8364; (leves)
    </p>
  </body>
</html></richcontent>
<node CREATED="1433928494321" ID="ID_251145437" MODIFIED="1433928505593" TEXT="No asistentes o votaron en contra">
<icon BUILTIN="stop-sign"/>
</node>
</node>
</node>
</node>
</node>
</map>
