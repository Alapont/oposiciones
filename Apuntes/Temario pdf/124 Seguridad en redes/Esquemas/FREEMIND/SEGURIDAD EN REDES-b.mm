<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1426529738177" ID="ID_1050499203" MODIFIED="1441281042663">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T111b SEGURIDAD
    </p>
    <p style="text-align: center">
      EN REDES
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426530302247" ID="ID_524841519" MODIFIED="1426530320914" POSITION="right" TEXT="Cortafuegos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426530314993" ID="ID_561595624" MODIFIED="1441281432845">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      T&#233;cns, pol&#237;ticas y tecnolog&#237;as para&#160; <u>centralizar seg</u>&#160;en la red&#8594; 1 pto de acceso
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1426530318617" ID="ID_656090943" MODIFIED="1441281441070">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Control</u>&#160;de tr&#225;fico y accesos, <u>registro</u>&#160;de eventos y cifrado de info (para VPNs)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1426593234267" ID="ID_1103939442" MODIFIED="1441281110493" TEXT="Tipos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426593235524" ID="ID_293949706" MODIFIED="1441281237020">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Filtrado de paqs: </i>routers con <u>reglas</u>&#160;(crits: prots, dirs IP y puertos), capas&#160; <u>transp y red</u>&#160;
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426593298552" ID="ID_1124620437" MODIFIED="1426593321544" TEXT="No protege capas sup, posibles cuellos botella">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1426593333377" ID="ID_1345329638" MODIFIED="1441281230705">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Servs proxy:</i>&#160;sw que <u>filtra conexs</u>&#160;entre c/s
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426593504109" ID="ID_226931739" MODIFIED="1441281287657">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Gateway de app:</i>(basti&#243;n host) m&#225;q donde se ejec proxy
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426765501754" ID="ID_870172897" MODIFIED="1441281750224">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Proxy web:</i>&#160;cuando no es posible comunic dir, o para guardar web en cach&#233;
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426765566245" ID="ID_931397062" MODIFIED="1441281738554">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Reverse proxy:</i>&#160;protege <u>servidores web</u>&#160;expuestos al ext. Encripta, distrib carga y guarda en cach&#233;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1426765723223" ID="ID_1645629479" MODIFIED="1441281383312">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Inspector de paqs: </i><u>detecta intrusos</u>&#160;(en apps muy complejas), capas desde <u>app hasta red</u>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1426765950585" ID="ID_316604037" MODIFIED="1441281466461" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Redes Priv Virtuales (VPN)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426765989343" ID="ID_1112941210" MODIFIED="1441281470775" TEXT="Tunneling">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426766019636" ID="ID_1609487350" MODIFIED="1441281494418">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Usar Internet para tx datos de una red <u>priv</u>&#160;a otra de forma seg
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="xmag"/>
</node>
<node CREATED="1426766158304" ID="ID_453551292" MODIFIED="1441281496149" TEXT="Protocolos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426766161331" ID="ID_1211876701" MODIFIED="1441281549351">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>PPPT y L2TP:</i>(capa enlace) encapsulan datos en trama PPP (Point to Point)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441281520199" ID="ID_1027229638" MODIFIED="1441281535533">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      L2TP va realmente sobre <u>UDP</u>
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1426766306030" ID="ID_298995501" MODIFIED="1441281573366">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>IPSec:</i>&#160;(capa red): encapsula paqs IP con una cab IP adicional (info routing)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1426766557179" ID="ID_658817916" MODIFIED="1441281472111" TEXT="Infraestructura">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426766834611" ID="ID_1834489370" MODIFIED="1441281782825">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Implem:</i>&#160;<u>routers y FWs</u>&#160;como base, y por encima mecs de seg (ACID)
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426766895285" ID="ID_1687520969" MODIFIED="1441281789055" TEXT="Elementos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426766910661" ID="ID_1800608653" MODIFIED="1441281904657">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Serv VPN: </i>ofrece conexs VPN&#8594; De acceso remoto (PC a red priv) o Router-Rout (porciones de red priv)
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426766928121" ID="ID_1360895999" MODIFIED="1441281922189">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cliente VPN:</i>&#160;debe ser<u>autenticado</u>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426767039844" ID="ID_349805396" MODIFIED="1441281972104">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>T&#250;nel VPN: </i>porci&#243;n de conexi&#243;n donde se&#160; <u>encapsulan</u>&#160;datos <u>cifrados</u>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1426767099780" ID="ID_1572813502" MODIFIED="1441281940396">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Red de tr&#225;nsito: </i>red IP <u>intermedia</u>&#160;para tx datos (internet o intranet alquilada)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1426767247836" ID="ID_556752868" MODIFIED="1441281473318" TEXT="Funcionamiento">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426767250684" ID="ID_586697149" MODIFIED="1426767271585" TEXT="1. Usuario conecta con red de su ISP (proveedor)"/>
<node CREATED="1426767279655" ID="ID_247503228" MODIFIED="1426767299906" TEXT="2. Para conectar con red corporativa, solicita t&#xfa;nel al Servidor VPN"/>
<node CREATED="1426767312265" ID="ID_650211264" MODIFIED="1426767487600" TEXT="3. Servidor autentica a usuario y crea el otro extremo del t&#xfa;nel"/>
<node CREATED="1426767338988" ID="ID_688128643" MODIFIED="1441281997819">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      4. Usuario tx datos, <u>cifrados por su sw VPN</u>
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1426767358407" ID="ID_337918939" MODIFIED="1426767385974" TEXT="5. En destino, Servidor VPN descifra y propaga datos a red corporativa"/>
</node>
<node CREATED="1441281585925" ID="ID_1779900514" MODIFIED="1441281610999" TEXT="VPLS">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441281611517" ID="ID_1397437644" MODIFIED="1441281697369">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Virtual Priv LAN Service) serv de VPN, permite <u>Eth multipto a multipto</u>&#160;&#160;(tramas L2 entre sedes rem)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1426767530251" ID="ID_628914477" MODIFIED="1426767534846" POSITION="right" TEXT="Amenazas l&#xf3;gicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426767547879" ID="ID_1817333773" MODIFIED="1441282064072">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Deneg de serv (DoS, interrup)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426767586106" ID="ID_1550847382" MODIFIED="1426769250397">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Saturar</b>&#160;recursos (flood) o impedir comunicaci&#243;n
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1426767598843" ID="ID_1191776017" MODIFIED="1426769335950" TEXT="Atenta contra A y D">
<icon BUILTIN="broken-line"/>
</node>
<node CREATED="1426767696587" ID="ID_1876530720" MODIFIED="1441282112956">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Distributed DoS: </i>involucra a&#160; <u>varias m&#225;qs</u>&#160;para&#160;saturar
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426767751270" ID="ID_67437923" MODIFIED="1441282116198">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Disributed Reflection DoS:</i>&#160;utiliza un&#160; <u>serv</u>&#160;para saturar
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1426767780927" ID="ID_750821520" MODIFIED="1441282120800" TEXT="Ataques">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426767784016" ID="ID_206077388" MODIFIED="1441282384520" TEXT="Ransomware (cifrar y cobrar), connection flood, IP spoofing (falso origen), ping muerte (falso tama&#xf1;o), email bombing, broadcast storm (multicast con falso origen), supernuke (UDP), hoaxes (bulos)"/>
</node>
</node>
<node CREATED="1426768022606" ID="ID_955676738" MODIFIED="1441282055002" TEXT="Escucha (intercepci&#xf3;n)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426768071404" ID="ID_1814624635" MODIFIED="1426769264510">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tercero no autorizado accede y <b>se apropia </b>de info
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1426768096669" ID="ID_645243576" MODIFIED="1426769328805" TEXT="Atenta contra C">
<icon BUILTIN="broken-line"/>
</node>
</node>
<node CREATED="1426768210321" ID="ID_972333064" MODIFIED="1426768212058" TEXT="Ataques">
<node CREATED="1426768476731" ID="ID_1667652178" MODIFIED="1426768516054" TEXT="Decoy (interfaz roba contrase&#xf1;a), keyloggers (guardan acciones), TCP scanning (registro puerto), eavesdropping-sniffing (roba paquetes), snoopong-download (copia info)"/>
</node>
</node>
<node CREATED="1426768633992" ID="ID_149878523" MODIFIED="1441282056274" TEXT="Suplantaci&#xf3;n (impostura o fabricaci&#xf3;n)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426768071404" ID="ID_1893418044" MODIFIED="1426769277002">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tercero no autorizado accede<b>&#160;suplantando a usuario o sistema</b>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1426768096669" ID="ID_1437417580" MODIFIED="1426769322019" TEXT="Atenta contra A y C">
<icon BUILTIN="broken-line"/>
</node>
</node>
<node CREATED="1426768809532" ID="ID_1287162" MODIFIED="1426768811726" TEXT="Ataques">
<node CREATED="1426769105278" ID="ID_686674453" MODIFIED="1441282519905" TEXT="DNS Spoof-Pharming (falso dominio-IP), mail spoof-Phishing (correo falso), man in the middle (simula c/s), IP splice-hijack (se adue&#xf1;a de sesi&#xf3;n), browser hijack, spoof-loop (a varios s&#xaa;), ing social"/>
</node>
</node>
<node CREATED="1426769190337" ID="ID_1680835433" MODIFIED="1441282072582">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modific (manipulaci&#243;n)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426768071404" ID="ID_54752495" MODIFIED="1427218131398">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tercero no autorizado accede y <b>modifica</b>&#160;la info&#160;(tampering o data diddling)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1426768096669" ID="ID_1851219361" MODIFIED="1426769338292" TEXT="Atenta contra I y D">
<icon BUILTIN="broken-line"/>
</node>
</node>
<node CREATED="1426769403868" ID="ID_1565397155" MODIFIED="1426769405579" TEXT="Ataques">
<node CREATED="1426769405580" ID="ID_804699219" MODIFIED="1427281802054">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Virus, gusanos (crean copias), troyanos (parece &#250;til), bombas l&#243;gicas (por evento), tabnabbing (falsean tab inactiva), defacement (modifican portal web)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1426769489960" ID="ID_1993729896" MODIFIED="1426769494346" POSITION="right" TEXT="Proyecto OWASP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426769494839" ID="ID_636025995" MODIFIED="1441282667252">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Open Web App Sec Project) 10 mayores vulnerabs en apps web (cada 3 a&#241;os, &#250;ltima 2013)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
</node>
<node CREATED="1426769666282" ID="ID_982780282" MODIFIED="1426770252327">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Injection (enga&#241;ar a int&#233;rprete serv), broken authentic and session manag, cross-site script (datos no fiables a navegador),&#160;insecure direct object ref (manipular refs), security misconfig, sensitive data exposure, missing function level access control, cross-site request forgery, using components with known vulnerabilities, unvalidated redirects and forwards
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</map>
