<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1426529738177" ID="ID_1050499203" MODIFIED="1441277110587">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T111 SEGURIDAD
    </p>
    <p style="text-align: center">
      EN REDES
    </p>
  </body>
</html></richcontent>
<node CREATED="1426529964582" ID="ID_397485806" MODIFIED="1441277126374" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      SSL (Sec Socket Layer)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426529979492" ID="ID_277581610" MODIFIED="1441280909941">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (RFC 6101) Prots <u>sobre TCP</u>&#160;de pila TCP/IP, <u>transpar</u>&#160;a user/app y da servs a prots de app (pe HTTPS)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1441277658412" ID="ID_1530129260" MODIFIED="1441279710427">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>S-HTTP: </i>seg a HTTP, compat con SSL, extensi&#243;n .shtml
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1441279770704" ID="ID_1417194743" MODIFIED="1441279799651">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Declarado <u>obsoleto</u>, sustituido por TLS
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node CREATED="1441277486164" ID="ID_1383262349" MODIFIED="1441277798343">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Funciones
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1426585754797" ID="ID_1123794493" MODIFIED="1441277254449">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Autentic servidor:&#160; </i>(opc cliente) mediante certifs dig X.509 (incluyen claves)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426585776169" ID="ID_317550875" MODIFIED="1441277430637">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Confid:</i>&#160;cript <u>sim&#233;trica</u>&#160;(clave priv) y <u>asim</u>&#160;(clave p&#250;b para intercambiar priv por sobre dig o D-H)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426585822838" ID="ID_1514498877" MODIFIED="1441277453324">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Integr:</i>&#160;firmas dig (funci&#243;n hash&#8594; huella&#160; <u>MAC</u>: C&#243;d Aut Msj)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426588020929" ID="ID_866823675" MODIFIED="1441277639954">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estados de comunic
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426586084883" ID="ID_1179601104" MODIFIED="1441277359002">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Sesi&#243;n:</i>&#160;asociaci&#243;n&#160;c-s (Handshake)&#8594; id sesi&#243;n, certif, algs compr y cifr, clave maestra (48 bits) y flags
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426586114861" ID="ID_255744203" MODIFIED="1441277813599">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Conex:&#160;</i>transporte&#8594; n&#186;aleat c/s, n&#186;secretos MAC, claves secretas (cifr), vector inic (bloques), n&#186;seq
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426530158268" ID="ID_1615439606" MODIFIED="1441278828404" TEXT="Protocolos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426585888097" ID="ID_1034145120" MODIFIED="1441277831308" TEXT="Handshake">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426586278143" ID="ID_1281963443" MODIFIED="1441279890168">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Handshake: </i>negoc seg de <u>claves y algs</u>
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1426586364555" ID="ID_979507085" MODIFIED="1441277912706">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>1&#186; hello: </i>cliente conecta con serv, acuerdan algs y versi&#243;n SSL
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426586445032" ID="ID_1135731473" MODIFIED="1441278055297">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2&#186; certif o key_exch (si no tiene): </i>&#160;serv se autentica e intercambian claves p&#250;b
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426586664968" ID="ID_1449057480" MODIFIED="1441277940122">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>3&#186; key_exch:</i>&#160;cliente genera secreto pre-maestro (para cifrar info) y lo tx con su autentic
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426586750807" ID="ID_1146634172" MODIFIED="1441278021550">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>4&#186; change_cipher_spec y finished:</i>&#160;fin sesi&#243;n&#160;confirmando q intercambio y autentic ha sido ok&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426530182070" ID="ID_1665817111" MODIFIED="1441278010054">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Change Cipher Spec: </i>notifica&#160; <u>cambios</u>&#160;en estrategias de cifrado. Msjs posteriores van cifrados
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1426586275567" ID="ID_1634719829" MODIFIED="1441277849102" TEXT="Alert">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426587386715" ID="ID_720038173" MODIFIED="1441279958035">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cierre/close_notify:</i>&#160;&#160;aviso de conexi&#243;n terminando
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426587407309" ID="ID_1344626273" MODIFIED="1441278093375" TEXT="Error">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426587428978" ID="ID_655408097" MODIFIED="1441278120993">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Fatal (cierre conex):</i>&#160;unexpected_msg, bad_record_mac, decompr_fail, hshake_fail, illegal_param
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426587485205" ID="ID_1163174479" MODIFIED="1441278139858">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Otros (de certif):</i>&#160;no_/bad_/unsuported_certif, certif_revoked/_expired/_unknown
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1426585890895" ID="ID_317720494" MODIFIED="1441279951756">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Record:</i>&#160;<u>encapsula</u>&#160;prots sup
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1426588234366" ID="ID_967276477" MODIFIED="1441278186197">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Fragm (bloques &lt;214B)&#8594; Compr (alg acordado)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426588306995" ID="ID_180015911" MODIFIED="1441278407753">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>C&#243;digo MAC:&#160; </i>func hash con&#160; <u>n&#186;seq + n&#186;secreto MAC + datos</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1441278407759" ID="ID_1713393688" MODIFIED="1441278407773">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se incluye en msj y se compara en rx
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426588626080" ID="ID_1192716612" MODIFIED="1441278431674">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cifrado: </i>bloque + MAC (alg sim y clave acordados)
    </p>
  </body>
</html></richcontent>
<node CREATED="1426588681779" ID="ID_39977470" MODIFIED="1441278341486">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      En bloque (CRC 64bits, req VI) o Flujo (OR-Exc con info)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1441279751437" ID="ID_1838300627" MODIFIED="1441279760970" POSITION="right" TEXT="TLS (Transport Layer Sec)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441280015559" ID="ID_1277501612" MODIFIED="1441280937134">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (RFC 4256 y 6176, v1.2,<u>abierto</u>)&#160;Mejora vulnerabs de SSLv3 y es <u>compatible</u>&#160;con &#233;l (pero no al rev&#233;s)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1441280316932" ID="ID_1753816627" MODIFIED="1441280768784">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mismo func, cambian algs
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441280330035" ID="ID_1649647093" MODIFIED="1441280397144" TEXT="Handshake">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441280398710" ID="ID_1185808388" MODIFIED="1441280567854">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Clave maestra: </i>(para cifrar) m&#233;todos de acuerdo&#8594; <u>RSA, D-H y PSK</u>&#160;(pre-shared keys)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441280444684" ID="ID_1289716525" MODIFIED="1441280560977">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Solo D-H ef&#237;mero y de curva el&#237;pt garantizan secreto
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1441280575947" ID="ID_836740397" MODIFIED="1441280581412" TEXT="Record">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441280591027" ID="ID_1091598796" MODIFIED="1441280801592">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Funcs HMAC:&#160; </i>MD5 y SHA1 (no recomendables), <u>SHA256</u>&#160;Y SHA384
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1441280852189" ID="ID_1389425379" MODIFIED="1441280858723" TEXT="Tama&#xf1;o hash: 96 bits"/>
</node>
<node CREATED="1441280642384" ID="ID_1899872192" MODIFIED="1441280824865">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Algs de cifr sim:&#160;</i><u>AES</u>, Camellia, SEED, ChaCha20 con Poly1305 e IDEA. No seg&#8594; 3DES, DES, RC2 y RC4
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1441282702992" ID="ID_1409108976" MODIFIED="1441282705755" TEXT="Ataques">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1441282706815" ID="ID_955691229" MODIFIED="1441288336547">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reneg (inyecta peticiones), rev de versiones (modif cifr), beast (applet viola restriccs), crime (recup autentic de cookie de compr), breach (recup login/correo/datos), poodle (rellenos en CBS), contra RC4 (recup txt con gran n&#186;cifrados), truncam (bloq pet desconex) y heartbleed (lee claves de memo)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1426530199734" ID="ID_849479125" MODIFIED="1426530205823" POSITION="right" TEXT="IPSec">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426589279011" ID="ID_1263634704" MODIFIED="1441278493644">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (RFC 2401, <u>abierto</u>&#160;e interop) Prots en capa <u>Internet</u>&#160;de TCP/IP, ofrecen servs de seg (seg&#250;n AH o ESP)&#160;
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
</node>
<node CREATED="1426589932977" ID="ID_1224326721" MODIFIED="1441278591184">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sec Association (SA)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426589944178" ID="ID_635898926" MODIFIED="1426593650866" TEXT="Conexi&#xf3;n unidir segura usada por AH o ESP">
<node CREATED="1426590008124" ID="ID_569045120" MODIFIED="1441278619454">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Grupos de SA: </i>seq de SAs para satisfacer una pol&#237;tica de seg (comb de servicios)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426590044475" ID="ID_941190616" MODIFIED="1441278627742" TEXT="BBDD">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426590047604" ID="ID_246610843" MODIFIED="1441278642299">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Pol&#237;ticas de seg (SPD):</i>&#160;tratam de tr&#225;fico
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426590063819" ID="ID_866020680" MODIFIED="1441279165278">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Asociaci&#243;n de seg (SAD):</i>&#160;id un&#237;voca&#8594; Dir IP <u>destino + Prot</u>&#160;IPSec (AH o ESP)
    </p>
  </body>
</html></richcontent>
<node CREATED="1426590090931" ID="ID_173633316" MODIFIED="1441278744809">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#205;ndice de par&#225;ms de seg (SPI): diferencia SAs con valores anteriores&#160;&#160;=
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1426589485067" ID="ID_1305887916" MODIFIED="1441278860450" TEXT="Protocolos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426589527671" ID="ID_1053797109" MODIFIED="1441278928196">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Internet Key Exchange (IKE):</i>&#160; <u>acuerdo</u>&#160;de claves y par&#225;ms de conexi&#243;n AH o ESP (similar a Handshake)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1426589502647" ID="ID_1516886109" MODIFIED="1441278895685" TEXT="IP Authentication Header (AH)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426590528897" ID="ID_1089117662" MODIFIED="1441279249347">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Autentic</u>&#160;(protege cab IP, excepto campos var) e <u>Integr</u>&#160;(+extendida que ESP)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
<node CREATED="1426590995452" ID="ID_1550675585" MODIFIED="1441278995910">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      NO Confid
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1426590699347" ID="ID_1066090170" MODIFIED="1441279258894">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Valor Prot (en cab IP): <u>51</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1426590616302" ID="ID_1943587380" MODIFIED="1441279403433">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cabecera: </i>(entre cab IP-datos) sig cab, long carga, SPI, n&#186;seq y <u>autentic</u>&#160;(Valor Comprob Integr=MAC)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426590865683" ID="ID_1073618019" MODIFIED="1441279192831">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Func:</i>&#160;alg HMAC&#8594; rx recalcula y compara huella para verif integr y autentic
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426589511502" ID="ID_316293925" MODIFIED="1441278896573" TEXT="IP Encapsulating Sec Payload (ESP)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426590909764" ID="ID_1246790986" MODIFIED="1441279296489">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Confid</u>&#160;e <u>Integr</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
<node CREATED="1426592621636" ID="ID_1224517267" MODIFIED="1441279294802">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Autentic&#160; <u>no protege cab IP</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1426590699347" ID="ID_876677387" MODIFIED="1441279313147">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Valor Prot (en cab IP): <u>50</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1426591573058" ID="ID_1059963154" MODIFIED="1441279380307">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cabecera: </i>(entre cab IP-cab prot sup) <u>carga &#250;til cifrada</u>, sig cab, SPI, n&#186;seq e ICV
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426591708997" ID="ID_1535998448" MODIFIED="1441279424903">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Func: </i>cifra msj y lo incluye en paq IP. Rx aplica alg cifrado y recupera datos
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1426530286846" ID="ID_1302718959" MODIFIED="1441279430261" TEXT="Modos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426530294472" ID="ID_395643236" MODIFIED="1441279471699">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Transporte: </i>datagrama AH/ESP son datos de capa transporte&#8594; solo <u>carga &#250;til</u>&#160;es cifrada
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1426592014406" ID="ID_864359620" MODIFIED="1441279594973">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Comunic ord a ord:</i>&#160;req que ambos extremos entiendan IPSec
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1426530296974" ID="ID_1854722740" MODIFIED="1441279543847">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>T&#250;nel: </i>datagr AH/ESP es un datagr IP (encaps en otra cab IP)&#8594; <u>todo el paq</u>&#160;es cifrado
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1426592053263" ID="ID_1816342781" MODIFIED="1441279620595">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Com red a red: </i><u>VPNs</u>, destino no conoce IPSec, gateways, seg centraliz, ocultar nodos
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
</node>
</map>
