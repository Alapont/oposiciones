<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1436516523078" ID="ID_1144495843" MODIFIED="1436517610656">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T87/88 CALIDAD
    </p>
    <p style="text-align: center">
      (Tradicional)
    </p>
  </body>
</html></richcontent>
<node CREATED="1436518206057" ID="ID_1644190288" MODIFIED="1436518216084" POSITION="right" TEXT="Calidad de sw">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436518394430" ID="ID_501267218" MODIFIED="1436518438461">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Creaci&#243;n de prods que, eficaz y eficientemente, den <u>completa satisf</u>&#160;al usuario
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1436518479223" ID="ID_469853001" MODIFIED="1436518528406">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>SW:</i>&#160;abstracto, se desarrolla (errores en dise&#241;o, se asumen) y no se deteriora
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1436518554182" ID="ID_1914494978" MODIFIED="1436518614069">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Que coincidan las visiones de calidad&#8594; req (cliente), pgmada y realizada
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1436518444863" ID="ID_1351975233" MODIFIED="1436523843221">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Eval de cal:&#160;</i><u>compara&#160;reqs</u>&#160;preestablecidos y <u>prod</u>&#160;realmente desarrollado
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1436518359297" ID="ID_1911659575" MODIFIED="1436523743901" TEXT="Costes">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436524168877" ID="ID_92066783" MODIFIED="1436524218609">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Teo.Lawler:</i>&#160;coste dism exp con cal&#8594; cte&#8594; aumenta exp con cal
    </p>
  </body>
</html></richcontent>
<node CREATED="1436523618400" ID="ID_331264911" MODIFIED="1436524249032">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>C. de impantaci&#243;n: </i>dise&#241;ar org, formaci&#243;n, metod y herrams y +esfuerzo en DSI
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1436523662844" ID="ID_1856069944" MODIFIED="1436524255091">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>C. de 'no calidad': </i>correcci&#243;n de fallos int y ext
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1436518363810" ID="ID_1468830204" MODIFIED="1436523736807" TEXT="Beneficios">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1436523706358" ID="ID_1592513609" MODIFIED="1436524064462">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Asegura objs en plazos y presup, mejor sol t&#233;cn, productiv y&#160; <u>-mantenim</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1436518217011" ID="ID_1638973777" MODIFIED="1436518261028" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modelos de Cal
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436518669766" ID="ID_1860956963" MODIFIED="1436518908284">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ayudan a def y comprobar cal sw de forma <u>jer&#225;rquica</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1436518695046" ID="ID_635000397" MODIFIED="1439822626883">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Factores:</i>&#160;atrib <u>ext</u>, pto vista&#160;<u>usuario</u>. Se descomponen&#160;en&#8594; <i>Criterios:</i>&#160;atrib <u>int</u>, pto vista prod sw
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1436518736649" ID="ID_822847498" MODIFIED="1436519163400">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>M&#233;tricas: </i><u>grado</u>&#160;en q prod prosee un crit. Cal del proceso de <u>desarrollo</u>&#160;(todo el CV) y del <u>prod</u>&#160;sw
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1436518988726" ID="ID_980472770" MODIFIED="1436520199637" TEXT="Estrategia">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="idea"/>
<node CREATED="1436518993528" ID="ID_236050178" MODIFIED="1436519103350" TEXT="Elegir modelo (est&#xe1;ndar o partic)&#x2192; Seleccionar y priorizar Factores&#x2192; ">
<node CREATED="1436519103351" ID="ID_898771402" MODIFIED="1436519103352" TEXT="Elegir m&#xe9;tricas por criterio&#x2192; Valores deseables y m&#xed;n por criterio"/>
</node>
</node>
<node CREATED="1436518224197" ID="ID_1300468188" MODIFIED="1436518227607" TEXT="McCALL">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436519282349" ID="ID_1980281268" MODIFIED="1436519299515" TEXT="11 Factores, 23 Criterios y 41 M&#xe9;tricas">
<node CREATED="1436519301649" ID="ID_1160292012" MODIFIED="1438931752944" TEXT="Factores">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436519369678" ID="ID_1290162766" MODIFIED="1436519425089">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>De op:</i>&#160;correcc (reqs), fiab (no error), eficiencia (pocos recs), integr (ctrl accesos) y usab
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1436519426598" ID="ID_1531011038" MODIFIED="1436519464560">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>De revisi&#243;n:</i>&#160;mantenib (coste error), verificab (comprob reqs) y flexib (coste modif)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1436519475793" ID="ID_842808461" MODIFIED="1436519512395">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>De transici&#243;n:</i>&#160;portab (cambio de hw), reusab (otras apps) e interop (acople con otro sw)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1436519542534" ID="ID_72566638" MODIFIED="1436519547318" TEXT="M&#xe9;tricas">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436519547319" ID="ID_131755169" MODIFIED="1436519587651" TEXT="F=r1&#xb7;m1+...+rn&#xb7;mn&#x2192; rj: coefs de regresi&#xf3;n (tablas) y mj: m&#xe9;tricas por crit"/>
<node CREATED="1436519605812" ID="ID_1485342374" MODIFIED="1436519699215">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Eficacia de Elim Defectos:</i>&#160;EED=E/(E+D)&#8594; E: errores pre-entrega; D: tras entrega. Ideal=1
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1436518233405" ID="ID_1722142601" MODIFIED="1436518240203" TEXT="ISO 9126">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436519971675" ID="ID_971484252" MODIFIED="1436520152550" TEXT="6 Factores y 25 subcaracter&#xed;sticas">
<node CREATED="1436519987592" ID="ID_1494966925" MODIFIED="1436520013528">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Factores: </i>funcionalidad, fiab, usab, eficiencia, mantenib y portab
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1436518244140" ID="ID_638660220" MODIFIED="1436520140364" TEXT="MOSCA">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436520140350" ID="ID_957179050" MODIFIED="1436520150442" TEXT="Modelo Sist&#xe9;mico de Cal">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436520083990" ID="ID_1777754276" MODIFIED="1436520136833">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      4 niveles (dims, categ, caracts y 715 m&#233;tricas) en base de 3 ramas (prod, proceso y humana)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1436518250147" ID="ID_294004833" MODIFIED="1436518255384" POSITION="right" TEXT="Garant&#xed;a de Cal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436520495062" ID="ID_1065645541" MODIFIED="1439823702676">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Planif, estim y superv de activs de <u>desarrollo</u>&#160;para asegurar que prods cumplen reqs
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1439823591865" ID="ID_558134992" MODIFIED="1439823653535">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>En fases: </i>Espec de reqs, Dise&#241;o, Codif y Pruebas
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1436520587988" ID="ID_1468758496" MODIFIED="1436520591173" TEXT="Tareas">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436520591174" ID="ID_1706025729" MODIFIED="1436524017482">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Planif cal (id reqs y mecs cntrl)&#8594;&#160;Supervisar cal (Control de Cal)&#8594;&#160; Construir cal (activs prev de errores)
    </p>
  </body>
</html></richcontent>
<node CREATED="1439823686537" ID="ID_609362841" MODIFIED="1439823694950">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Indep</u>&#160;al equipo de desarrollo
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
<node CREATED="1436518275014" ID="ID_710541404" MODIFIED="1436520703307" POSITION="right" TEXT="Control de Cal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436520802108" ID="ID_1576220829" MODIFIED="1436520912960">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Comprueba</u>&#160;q prod posee caracts de calidad en el grado req&#8594; id y corrige<u>defectos</u>&#160;
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1436520942055" ID="ID_1020161036" MODIFIED="1439822792873">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se centra en el <u>prod</u>&#160;sw, no en el desarrollo como la GCal
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1436518283671" ID="ID_76312232" MODIFIED="1436521697917">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>An&#225;lisis est&#225;ticos</b>
    </p>
    <p>
      (no req ejec sw)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436518288474" ID="ID_271746576" MODIFIED="1436518298505" TEXT="Auditor&#xed;as">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436521036085" ID="ID_486243197" MODIFIED="1436521086216">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Investig</u>&#160;para determinar grado de<u>cumplim de reqs</u>&#160;y efectividad de implementaci&#243;n realizada
    </p>
  </body>
</html></richcontent>
<node CREATED="1436521619060" ID="ID_595764238" MODIFIED="1436521627985" TEXT="&#xda;ltimas fases de desarr"/>
</node>
<node CREATED="1436521098896" ID="ID_1992837919" MODIFIED="1436521160735">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Tipos:</i>&#160;del prod, del proceso (proy y gesti&#243;n) o del propio s&#170; de garant&#237;a de cal
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1436518301240" ID="ID_1817026078" MODIFIED="1436518302993" TEXT="Revisiones">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436521514367" ID="ID_1495685856" MODIFIED="1436521602570">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Reuni&#243;n</u>&#160;con usuario sobre &#160;<u>estado actual</u>&#160;de resultados del proy y se analizan
    </p>
  </body>
</html></richcontent>
<node CREATED="1436521610817" ID="ID_773963600" MODIFIED="1436521616050" TEXT="Primeras fases de desarrollo"/>
<node CREATED="1436521880804" ID="ID_464016300" MODIFIED="1436521884441" TEXT="Informe final con resumen"/>
</node>
<node CREATED="1436521667432" ID="ID_643146728" MODIFIED="1436523341004">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tipos
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436523341006" ID="ID_420309142" MODIFIED="1436523345120">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      PGCC
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436521676609" ID="ID_1516431327" MODIFIED="1436521733120">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Rev M&#237;n (RM):</i>&#160;solo caracts +visibles de prods
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1436521734587" ID="ID_792391208" MODIFIED="1436521826638">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Rev T&#233;cn Formales (RTF):</i>&#160;eval sistem&#225;tica para&#160;verif cumplim de <u>reqs</u>&#160;y adecuaci&#243;n&#160;a <u>est&#225;ndares</u>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1436521779925" ID="ID_1224026679" MODIFIED="1436524299706">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Inspecc Detalladas (ID):</i>&#160;+exhaustivo, busca tb <u>fallos</u>&#160;y deficiencias y se&#241;alan desviaciones
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1436523353849" ID="ID_1468203720" MODIFIED="1436523358735" TEXT="IAC">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436523439310" ID="ID_573334945" MODIFIED="1438880461816">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>T&#233;cn:</i>=RTF, participan&#8594; Jefe Proy y resp del GAC
    </p>
  </body>
</html></richcontent>
<node CREATED="1436523361067" ID="ID_171653305" MODIFIED="1436523505754">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Formales:</i>&#160;=ID, participan&#8594; GAC, eq DSI y usario
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1436518305970" ID="ID_17476538" MODIFIED="1436518308853" TEXT="Verific formal">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436521908350" ID="ID_64725990" MODIFIED="1436521932384">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Demostraci&#243;n <u>matem&#225;tica</u>&#160;de correcci&#243;n de un pgm respecto a reqs. Muy costoso (solo s&#170; cr&#237;ticos)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1436518309682" ID="ID_1187004669" MODIFIED="1436522110282">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Pruebas din&#225;micas</b>
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436522100365" ID="ID_1581058899" MODIFIED="1436522105629" TEXT="(req ejec sw)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436521963246" ID="ID_476295864" MODIFIED="1436522096268">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mod (unit), de integr (interfs), del s&#170; (reqs usuario), de acept (entorno real) y de regresi&#243;n (versiones)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1436518318693" ID="ID_1687200069" MODIFIED="1436518322215" POSITION="right" TEXT="Sistema de Cal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436522120073" ID="ID_515192717" MODIFIED="1439823784256">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Instrum q define la&#160; <u>estrategia de implantaci&#243;n</u>&#160;de cal en una org
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1436522176428" ID="ID_1394055536" MODIFIED="1436522207557">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Manual de garant&#237;a (termin, ppios, resps, est&#225;ndares),&#160;modelo, proceds e integr en DSI
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1436518331280" ID="ID_926868544" MODIFIED="1436523205869">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Plan Gen de Garant&#237;a de Cal (CSAE)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436524544416" ID="ID_764556314" MODIFIED="1436524807019">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Marco de ref y metod para elab de Planes de Garant&#237;a de Cal<u>Espec&#237;ficos</u>&#160;(para cada proy)
    </p>
  </body>
</html></richcontent>
<node CREATED="1436525238988" ID="ID_1947178206" MODIFIED="1439823308227" TEXT="Sigue ISO 9000 y IEEE">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1436524574565" ID="ID_1489466487" MODIFIED="1436524682857">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>1. Gu&#237;a de elab de PGC:</i>agentes q intervienen y metod para Planes Espec&#237;ficos
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1436524584400" ID="ID_1811191172" MODIFIED="1439822118834">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2. Esquema Formal</i>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1439822111841" ID="ID_271159046" MODIFIED="1439822111845">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Diagr Caract (<u>clasif</u>&#160;proy)&#8594; Modelo ref (fases)&#8594; Perfil riesgos (7 tipos)&#8594;Foco inter&#233;s por fase
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1436524593083" ID="ID_13086972" MODIFIED="1439823440907">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>3. Proceds de Control de Cal</i>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1439823440911" ID="ID_215378261" MODIFIED="1439823498226">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Audit (proceds extraord), revisiones, pruebas y&#160;proceds particuales (eval prototipos)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1436524599629" ID="ID_997537790" MODIFIED="1436525064098">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>4. Instrums de control: </i>listas de ctrl, gu&#237;as con recos y aux (hojas de coments, correcciones...)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1436518336139" ID="ID_637177709" MODIFIED="1436523256481">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Interfaz de Aseg de Cal (M&#233;trica v3)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436522255026" ID="ID_1653264312" MODIFIED="1439823001640">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Activs para <u>verif cal</u>&#160;de prods, realizadas por Grupo de Aseg de Cal (GAC, indep del eq desarr)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1436524353655" ID="ID_464143134" MODIFIED="1436524381282" TEXT="Desde EVS a MSI">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1436523256472" ID="ID_743070762" MODIFIED="1439823136296">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Plan de Aseg de Cal (en EVS):&#160;</i>activs, est&#225;ndrs, prods, proceds y normas
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1436523222909" ID="ID_790539530" MODIFIED="1439823297452">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Prevenir defics (reco: <u>ISO 9000:2000</u>)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
</node>
</node>
</node>
</map>
