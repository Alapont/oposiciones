<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1375800625583" ID="ID_1995838940" MODIFIED="1428132905597">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      RD 3/2011, de 14nov
    </p>
    <p>
      &#160;-- TRLCSP --
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ojo: los d&#237;as se computar&#225;n como HABILES
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375804283298" ID="ID_232126948" MODIFIED="1428133816447" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Selecci&#243;n de proveedor
    </p>
    <p>
      y adjudicaci&#243;n
    </p>
    <p>
      (II)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
<node CREATED="1375866341314" ID="ID_62195280" MODIFIED="1428133823957" TEXT="Proposiciones de los candidatos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428075533376" ID="ID_845679170" MODIFIED="1428075567235" TEXT="Su contenido se ajustar&#xe1; al PCAP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428075520762" ID="ID_298572350" MODIFIED="1428075567235" TEXT="Secretas hasta la apertura">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428075558784" ID="ID_1262466768" MODIFIED="1428075567235" TEXT="1 proposici&#xf3;n por licitador">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428075695876" ID="ID_561276501" MODIFIED="1428133990123">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Valoraci&#243;n
    </p>
    <p>
      de ofertas
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428075736857" ID="ID_1620651796" MODIFIED="1428076083450" TEXT="Criterios">
<node CREATED="1428075739108" ID="ID_584614158" MODIFIED="1428075740547" TEXT="Precio">
<node CREATED="1428075740548" ID="ID_697354672" MODIFIED="1428075752707" TEXT="Obligatoriamente utilizado en caso de utilizarse un &#xfa;nico criterio"/>
</node>
<node CREATED="1428075789958" ID="ID_1059741961" MODIFIED="1428075978717">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      En el &#225;mbito TIC obligatorio
    </p>
    <p>
      utilizar varios criterios
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428075823825" ID="ID_1702625219" MODIFIED="1428133862715" TEXT="Cuando el presupuesto no haya podido ser establecido &#xa;previamente y los licitadores deban presentar los suyos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428075841599" ID="ID_1881833853" MODIFIED="1428133877210" TEXT="En aquellos casos en que se contemple la presentaci&#xf3;n de variantes &#xa;o mejoras por los licitadores, o reducciones del plazo de ejecuci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428075859479" ID="ID_395580277" MODIFIED="1428133881787" TEXT="En caso de emplear tecnolog&#xed;a especialmente avanzada &#xa;o que la ejecuci&#xf3;n sea particularmente compleja">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428075874746" ID="ID_1982477314" MODIFIED="1428133908122" TEXT="Suministros y servicios: salvo productos o servicios perfectamente definidos (normalizados) y no sea posible variar los plazos de entrega, introducir variaciones ni mejoras de ninguna clase">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428075882706" ID="ID_28433463" MODIFIED="1428133925297" TEXT="Contratos con impacto en el medio ambiente: obligatorio&#xa;contemplar la inclusi&#xf3;n de criterios medioambientales.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428075932784" ID="ID_369272398" MODIFIED="1428075961644" TEXT="Ponderaci&#xf3;n de cada criterio en el anuncio de licitaci&#xf3;n y en el PCAP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
</node>
<node CREATED="1428076093967" ID="ID_1978364301" MODIFIED="1428076214132">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si predominan los criterios de juicio
    </p>
    <p>
      de valor sobre las f&#243;rmulas
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1428076112998" ID="ID_1014006503" MODIFIED="1428076165209" TEXT="Comit&#xe9; de expertos para la valoraci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1428076198999" ID="ID_361489867" MODIFIED="1428076204639" TEXT="No podr&#xe1;n pertenecer al &#xf3;rgano proponente"/>
</node>
</node>
</node>
<node CREATED="1428076300330" ID="ID_870075518" MODIFIED="1428076365239">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Posibilidad de contemplar VARIANTES y MEJORAS
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1428076304650" ID="ID_765754239" MODIFIED="1428076325865" TEXT="Si se consideran criterios distintos al precio"/>
<node CREATED="1428076326100" ID="ID_1462014217" MODIFIED="1428076333136" TEXT="Se indicar&#xe1; en el anuncio de licitaci&#xf3;n"/>
</node>
<node CREATED="1428076430671" ID="ID_40582112" MODIFIED="1428076559354" TEXT="El &#xf3;rgano de contrataci&#xf3;n clasificar&#xe1;, por orden DECRECIENTE, las proposiciones presentadas y que no hayan sido declaradas DESPROPORCIONADAS o ANORMALES">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="info"/>
</node>
<node CREATED="1428076521503" ID="ID_946672305" MODIFIED="1428076532015" TEXT="Si existe al menos una oferta admisible">
<node CREATED="1428076532016" ID="ID_661984053" MODIFIED="1428076547485" TEXT="La licitaci&#xf3;n no podr&#xe1; declararse DESIERTA"/>
</node>
</node>
<node CREATED="1375866541433" ID="ID_693772364" MODIFIED="1428133994930">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Adjudicaci&#243;n
    </p>
    <p>
      del contrato
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378810009717" ID="ID_515561860" MODIFIED="1428077798353" TEXT="Se requiere a la m&#xe1;s ventajosa econ&#xf3;micamente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375805512992" ID="ID_1058589170" MODIFIED="1428078038747" TEXT="Presentaci&#xf3;n de documentaci&#xf3;n acreditativa de hallarse &#xa;al corriente de obligaciones tributarias y con la SS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1428077985337" ID="ID_1340812397" MODIFIED="1428078039771" TEXT="Si presento&#x301; declaracio&#x301;n responsable, los documentos que se listan en tal declaracio&#x301;n ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1428077886839" ID="ID_1091212814" MODIFIED="1428078040843" TEXT="Disponer de los medios que se hubiese comprometido a dedicar">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1375866505989" ID="ID_291335993" MODIFIED="1428078042251" TEXT="Constituci&#xf3;n de la garant&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1375805525074" ID="ID_1305477086" MODIFIED="1384076778340" STYLE="bubble" TEXT="En 10 d&#xed;as h&#xe1;biles">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378810061712" ID="ID_87347441" MODIFIED="1381586588795" TEXT="Adjudicaci&#xf3;n tras la recepci&#xf3;n de la documentaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378810094312" ID="ID_1911665187" MODIFIED="1384076778340" STYLE="bubble" TEXT="5 d&#xed;as h&#xe1;biles">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428078281706" ID="ID_600785879" MODIFIED="1428078399911">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      La adjudicaci&#243;n se realizar&#225; mediante resoluci&#243;n motivada
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428078304776" ID="ID_1804824286" MODIFIED="1428078327281" TEXT="Se enviar&#xe1; a todos los licitadores (posibilidad por eCorreo)"/>
<node CREATED="1428078335497" ID="ID_14283060" MODIFIED="1428078368131" TEXT="Se publicar&#xe1; en el Perfil del Contratante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375866589933" ID="ID_789450634" MODIFIED="1385018960817" TEXT="Formalizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428078715868" ID="ID_872076667" MODIFIED="1428078731757" TEXT="Requisito indispensable para iniciar la ejecuci&#xf3;n del contrato (salvo en caso de emergencia)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
</node>
<node CREATED="1375866598367" ID="ID_734559743" MODIFIED="1428078760164" TEXT="Los contratos p&#xfa;blicos deben formalizarse en DOCUMENTO ADMINISTRATIVO">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375866607187" ID="ID_1036106275" MODIFIED="1428134002503" STYLE="bubble" TEXT="NO ANTES DE de 15 d&#xed;as h&#xe1;biles desde la adjudicaci&#xf3;n&#xa;si es susceptible de RECURSO ESPECIAL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428078879975" ID="ID_600914746" MODIFIED="1428133944449" TEXT="Pasado el plazo, requerimiento al adjudicatario &#xa;para formalizaci&#xf3;n en plazo NO SUPERIOR a 5 d&#xed;as h&#xe1;biles">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375866637615" ID="ID_396287536" MODIFIED="1428079051946" STYLE="bubble" TEXT="NO M&#xc1;S TARDE de 15 d&#xed;as h&#xe1;biles en el RESTO de casos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375866659197" ID="ID_1172294084" MODIFIED="1377532332519" TEXT="Publicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375866670283" ID="ID_824977407" MODIFIED="1377532320384" TEXT="Perfil del contratante, no obligatorio para los contratos menores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428079421556" ID="ID_1373897346" MODIFIED="1428079457310" TEXT="=&gt; 18.000&#x20ac;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428079426667" ID="ID_919109481" MODIFIED="1428079459877" TEXT="Perfil del Contratante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428079315739" ID="ID_1002151413" MODIFIED="1428079316736" TEXT="48 d&#xed;as tras el contrato"/>
</node>
</node>
<node CREATED="1375866696715" ID="ID_1285526966" MODIFIED="1428079309924">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      =&gt; 100.000 &#8364;
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428079310433" ID="ID_1369052378" MODIFIED="1428079462557">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      BOE o DO + Perfil del contratante
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428079315739" ID="ID_976620987" MODIFIED="1428079316736" TEXT="48 d&#xed;as tras el contrato"/>
</node>
</node>
<node CREATED="1375866708922" ID="ID_961048928" MODIFIED="1428079323258" TEXT="SARA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428079323259" ID="ID_95679887" MODIFIED="1428079464798">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      DOUE + BOE + Perfil del Contratate
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428079315739" ID="ID_973122536" MODIFIED="1428079316736" TEXT="48 d&#xed;as tras el contrato"/>
</node>
</node>
</node>
</node>
<node CREATED="1428078464958" ID="ID_163627436" MODIFIED="1428079149784">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Renuncia a la celebraci&#243;n del contrato
    </p>
    <p>
      y desistimiento del procedimiento
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428078480924" ID="ID_1792065691" MODIFIED="1428078522198" TEXT="Podr&#xe1; hacerse por parte de la Adm&#xf3;n. siempre antes de la adjudicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
</node>
<node CREATED="1428078533347" ID="ID_1802143072" MODIFIED="1428078679683" TEXT="Renuncia, &#xfa;nicamente por razones de inter&#xe9;s p&#xfa;blico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428078551118" ID="ID_205478164" MODIFIED="1428078556186" TEXT="Se justificar&#xe1;n en el expediente"/>
<node CREATED="1428078580182" ID="ID_336462996" MODIFIED="1428078645017" TEXT="No podr&#xe1; iniciarse nuevo licitaci&#xf3;n hasta que se subsanen"/>
</node>
<node CREATED="1428078592561" ID="ID_1378094476" MODIFIED="1428079154944">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Desistimiento, &#250;nicamente por infracci&#243;n
    </p>
    <p>
      no subsanable en el procedimiento de contrataci&#243;n
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428078619999" ID="ID_1879415764" MODIFIED="1428078665537" TEXT="Podr&#xe1; iniciarse otra licitaci&#xf3;n inmediatamente"/>
</node>
</node>
</node>
<node CREATED="1375806918769" ID="ID_1446734889" MODIFIED="1428133841432" POSITION="right" TEXT="Ejecuci&#xf3;n y Recepci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-5"/>
<node CREATED="1375807032190" ID="ID_826046627" MODIFIED="1377534967860" TEXT="Principio de riesgo y ventura">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428089038723" ID="ID_1150672406" MODIFIED="1428089043841" TEXT="Los contratos se realizar&#xe1;n a riesgo y ventura del contratista"/>
</node>
<node CREATED="1375806926486" ID="ID_399259174" MODIFIED="1428089070762" TEXT="Penalidades por ejecuci&#xf3;n defectuosa o demora">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375806932063" ID="ID_835720124" MODIFIED="1428089371770" STYLE="bubble" TEXT="&lt;= 10% presupuesto del contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428089346396" ID="ID_796771989" MODIFIED="1428089371770" STYLE="bubble" TEXT="Por demora: penalidades diarias de 0,20&#x20ac; por cada 1.000&#x20ac; del precio del contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428089167098" ID="ID_757512780" MODIFIED="1428089179954" TEXT="Acumulaci&#xf3;n de penalidades puede suponer Resoluci&#xf3;n del contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375806980983" ID="ID_1811736442" MODIFIED="1428089265666" TEXT="Pueden incluirse distintas penalidades en los pliegos y en el contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378811618634" ID="ID_779034427" MODIFIED="1378890805244" TEXT="Si es por causa imputable al contratista no es necesario incluirlas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375807043243" ID="ID_1209371354" MODIFIED="1428089053482" TEXT="Pago del precio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375807048666" ID="ID_2419354" MODIFIED="1428089828070" STYLE="bubble" TEXT="30 d&#xed;as desde acreditaci&#xf3;n de conformidad de bienes o servicios entregados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428089599922" ID="ID_818056670" MODIFIED="1428089833871" STYLE="bubble" TEXT="En 30 d&#xed;as, el contratista debe presentar la factura ante un registro administrativo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428091124197" ID="ID_450789225" MODIFIED="1428091228809" TEXT="Desde 15/01/2015 Obligaci&#xf3;n de presentar eFactura &#xa;ante el Punto General de Entrada de eFacturas (FACe)"/>
</node>
<node CREATED="1375807069076" ID="ID_561852544" MODIFIED="1428089778634">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Derecho a los intereses de demora e indemnizaci&#243;n por costes de cobro
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375865078401" ID="ID_795347472" MODIFIED="1377534978620" TEXT="Se prohibe el pago aplazado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375807106712" FOLDED="true" ID="ID_169082629" MODIFIED="1428091254141">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Demora de m&#225;s de 4 meses en el pago - Suspensi&#243;n
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428090988042" ID="ID_1974447547" MODIFIED="1428090994406" TEXT="Comunic&#xe1;ndolo con 1 mes de antelaci&#xf3;n"/>
</node>
<node CREATED="1375807120931" ID="ID_918635522" MODIFIED="1428091259686" TEXT="Demora de m&#xe1;s de 6 meses en el pago - Resoluci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375867000495" ID="ID_1874541598" MODIFIED="1428089886963" TEXT="Modificaci&#xf3;n del contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428089850510" ID="ID_339033687" MODIFIED="1428089885035" TEXT="S&#xf3;lo cabe por razones de inter&#xe9;s p&#xfa;blico y ante causas imprevistas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428090343323" ID="ID_873151052" MODIFIED="1428090357326" TEXT="La posibilidad debe quedar recogida en pliegos y en el contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375867011189" ID="ID_879291258" MODIFIED="1428090324052" TEXT="Suspensi&#xf3;n del contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375867018341" ID="ID_969471515" MODIFIED="1385037594791" TEXT="Cesi&#xf3;n y subcontrataci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375807181687" ID="ID_234806178" MODIFIED="1385037595938" TEXT="Cesi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375807185332" ID="ID_628664042" MODIFIED="1428090043042" STYLE="bubble" TEXT="Necesidad de ejecuci&#xf3;n de un 20 %">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428089904304" ID="ID_1708843735" MODIFIED="1428089940732" TEXT="Subcontrataci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428089907600" ID="ID_799500105" MODIFIED="1428090040403" STYLE="bubble" TEXT="&lt;= 60% del importe de adjudicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428090023956" ID="ID_479670858" MODIFIED="1428090040403" STYLE="bubble" TEXT="Deber&#xe1; indicarlo el licitador en la oferta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375867048660" ID="ID_432160509" MODIFIED="1428090114729" TEXT="Extinci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375867053818" ID="ID_211709519" MODIFIED="1428090189361" STYLE="bubble" TEXT="Cumplimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428090191200" ID="ID_89777945" MODIFIED="1428090727918" TEXT="Acto formal y positivo de RECEPCI&#xd3;N">
<node CREATED="1428090203106" ID="ID_1368669411" MODIFIED="1428090217755" TEXT="1 mes tras la entrega del objeto del contrato"/>
<node CREATED="1428090729036" ID="ID_1440540218" MODIFIED="1428090927146" TEXT="Acudir&#xe1;n">
<node CREATED="1428094681561" ID="ID_948232565" MODIFIED="1428094684474" TEXT="Necesariamente">
<node CREATED="1428090750814" ID="ID_348551625" MODIFIED="1428090914251" TEXT="Representante de la Adm&#xf3;n.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428090780472" ID="ID_151123428" MODIFIED="1428090914252" TEXT="Contratista">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1428094703553" ID="ID_868045368" MODIFIED="1428134017682">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      De forma
    </p>
    <p>
      potestativa
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1428090784445" ID="ID_615313871" MODIFIED="1428094742027" TEXT="Interventor (IGAE)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428090794815" ID="ID_432042989" MODIFIED="1428090917653" TEXT="Inversi&#xf3;n &gt; 50.000&#x20ac;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428090888440" ID="ID_1453633810" MODIFIED="1428090909371">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Verificar&#225; materialmente la efectiva realizaci&#243;n
    </p>
    <p>
      de las obras, servicios y adquisiciones
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428094729202" ID="ID_1168819208" MODIFIED="1428094787578" TEXT="Asesor del Interventor (si lo considera necesario)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428094787844" ID="ID_655547567" MODIFIED="1428094810567" TEXT="Asesor del contratista (si lo considera conveniente)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1375867062388" ID="ID_143317025" MODIFIED="1428090189841" STYLE="bubble" TEXT="o Resoluci&#xf3;n del contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428090222752" ID="ID_1907946367" MODIFIED="1428090239260" TEXT="Incumplimiento de obligaciones contractuales"/>
<node CREATED="1428090239480" ID="ID_1500100975" MODIFIED="1428090273938" TEXT="Mutuo acuerdo entre las partes (no si causa atribuible al contratista)"/>
<node CREATED="1428090280018" ID="ID_784510038" MODIFIED="1428090286767" TEXT="Otras previstas en el contrato"/>
</node>
</node>
<node CREATED="1375867076560" ID="ID_1205283300" MODIFIED="1377534992389" TEXT="Se fijar&#xe1; un plazo de garant&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428090297633" ID="ID_71019563" MODIFIED="1428090308971" TEXT="TRLCSP no determina plazos concretos"/>
</node>
</node>
</node>
</map>
