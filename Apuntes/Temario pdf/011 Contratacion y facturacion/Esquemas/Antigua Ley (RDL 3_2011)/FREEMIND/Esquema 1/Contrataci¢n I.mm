<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1375800625583" ID="ID_1995838940" MODIFIED="1479119066696">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      RD 3/2011, de 14nov
    </p>
    <p>
      &#160;-- TRLCSP --
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ojo: los d&#237;as se computar&#225;n como HABILES
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427991568298" ID="ID_749069934" MODIFIED="1428128326993" POSITION="right" TEXT="Legislaci&#xf3;n aplicable">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428096307871" ID="ID_1073429157" MODIFIED="1428128377645">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Directiva 2014/24/UE, de 26Feb2014,
    </p>
    <p>
      sobre contrataci&#243;n p&#250;blica
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428096352863" ID="ID_1009211529" MODIFIED="1428096378684" TEXT="Impulsa la Contrataci&#xf3;n Electr&#xf3;nica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428096413459" ID="ID_924392127" MODIFIED="1428128385300">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      En 4 a&#241;os y medio TODAS las fases
    </p>
    <p>
      del procedimiento ser&#225;n electr&#243;nicas
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428096310567" ID="ID_337643443" MODIFIED="1428096325456" TEXT="Deroga Directiva 2004/18/CE"/>
<node CREATED="1428096326848" ID="ID_1855531751" MODIFIED="1428096375654" TEXT="Trasposici&#xf3;n antes de febrero 2016"/>
</node>
<node CREATED="1427991573115" ID="ID_1940335742" MODIFIED="1428040750113" TEXT="Directiva 2004/18/CE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428040779264" ID="ID_871184850" MODIFIED="1428040800586" TEXT="Coordinaci&#xf3;n de los procedimientos de adjudicaci&#xf3;n &#xa;de los contratos p&#xfa;blicos de obras, de suministro y de servicios"/>
</node>
<node CREATED="1427991840137" ID="ID_1359440687" MODIFIED="1428040835193" TEXT="RD 3/2011 TRLCSP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427986628047" ID="ID_870826951" MODIFIED="1428128407421">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Texto Refundido de la Ley
    </p>
    <p>
      de Contratos del Sector P&#250;blico
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427986580042" ID="ID_164874849" MODIFIED="1427986584563" TEXT="Integra en un texto &#xfa;nico">
<node CREATED="1427986474358" ID="ID_1345699020" MODIFIED="1427986646066" TEXT="Ley 30/2007, de 30 de octubre, de Contratos del Sector P&#xfa;blico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427986577151" ID="ID_220635489" MODIFIED="1427986648089" TEXT="Modificaciones la Ley 30/2007 a trav&#xe9;s de diversas Leyes modificatorias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1427986684305" ID="ID_824426038" MODIFIED="1427986766817" TEXT="Deroga Ley 30/2007">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427986727360" ID="ID_507406547" MODIFIED="1427986730208" TEXT="Entrada en vigor">
<node CREATED="1427986730209" ID="ID_1229134941" MODIFIED="1428040860489" TEXT="Al mes de la publicaci&#xf3;n en BOE (16 de diciembre de 2011)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1427994979051" ID="ID_697254220" MODIFIED="1427995423096" TEXT="Orden HAP/2425/2013 (Cambio de l&#xed;mites a partir de 01/01/14)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427991854135" ID="ID_595339752" MODIFIED="1427991904316" TEXT="Orden EHA/3479/2011 (Cambio de l&#xed;mites)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427991871312" ID="ID_1239412385" MODIFIED="1427991975305" TEXT="Orden EHA/1049/2008 (Contrataci&#xf3;n centralizada)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1427985545088" ID="ID_1020029995" MODIFIED="1428128438846" POSITION="right" TEXT="Principios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427985548334" ID="ID_1559919919" MODIFIED="1427992353694" TEXT="Libre acceso a las licitaciones,&#xa;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1427992176101" ID="ID_976020522" MODIFIED="1427992356189" TEXT="Publicidad y transparencia de los procedimientos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1427992180628" ID="ID_1728114459" MODIFIED="1427992357685" TEXT="No discriminaci&#xf3;n e igualdad de trato entre los candidatos&#xa;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1427992278315" ID="ID_450997741" MODIFIED="1427992359605" TEXT="Asegurar una eficiente utilizaci&#xf3;n de los fondos p&#xfa;blicos mediante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1427992299948" ID="ID_500411702" MODIFIED="1427992362534" TEXT="Definici&#xf3;n previa de las necesidades">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1427992235309" ID="ID_1034663329" MODIFIED="1427992364110" TEXT="Salvaguarda de la libre competencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1427992333503" ID="ID_835171525" MODIFIED="1427992364510" TEXT="Selecci&#xf3;n de la oferta econ&#xf3;micamente m&#xe1;s ventajosa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1375800632720" ID="ID_1707568446" MODIFIED="1428128442409" POSITION="right" TEXT="&#xc1;mbito: contratos ONEROSOS del sector p&#xfa;blico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375800686980" ID="ID_131644942" MODIFIED="1427990967264">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      AAPP
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427990967266" ID="ID_1309131300" MODIFIED="1427990967268">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      AGE, CCAA, EELL
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375800698483" ID="ID_954213036" MODIFIED="1427988008043" TEXT="Entidades gestoras y servicios comunes de la Seguridad Social">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375800707320" ID="ID_1533590079" MODIFIED="1427990952579" TEXT="Organismos aut&#xf3;nomos, Universidades P&#xfa;blicas, Agencias Estatales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1427990286328" ID="ID_411028384" MODIFIED="1427990314520" TEXT="Mutuas de accidentes de trabajo y enfermedades profesionales de la Seguridad Social">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427990956018" ID="ID_642540932" MODIFIED="1427990978037" TEXT="Entidades P&#xfa;blicas Empresariales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427989181472" ID="ID_1479557518" MODIFIED="1427991020821" TEXT="Entidades de derecho p&#xfa;blico con personalidad jur&#xed;dica propia vinculadas a 1 sujeto del sector p&#xfa;blico o dependiente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427990130995" ID="ID_1566132605" MODIFIED="1427990229246" TEXT="Consorcios con personalidad jur&#xed;dica propia (32/1992)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375800734831" ID="ID_613471894" MODIFIED="1427990248534" TEXT="Sociedades mercantiles participadas &gt; 50%">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375800750860" ID="ID_1250719346" MODIFIED="1427990263862" TEXT="Fundaciones participadas &gt; 50 %">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375864571106" ID="ID_203152947" MODIFIED="1427990561790" TEXT="Cualquier otro ente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427990357022" ID="ID_216822831" MODIFIED="1427990573034" TEXT="Con personalidad jur&#xed;dica propia,">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427990362206" ID="ID_511708834" MODIFIED="1427990579018" TEXT="creado para satisfacer necesidades de inter&#xe9;s general no industrial o mercantil">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427989591226" ID="ID_369956494" MODIFIED="1428128482611" TEXT="y que 1 o m&#xe1;s sujetos del sector p&#xfa;blico financien &#xa;mayoritariamente su actividad o controlen su gesti&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1427990710726" ID="ID_1180211851" MODIFIED="1427991043734" TEXT="Sector P&#xfa;blico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="idea"/>
<node CREATED="1427990717428" ID="ID_351123256" MODIFIED="1427991126107">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Poderes adjudicadores
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427990714806" ID="ID_303269766" MODIFIED="1427991128449" TEXT="AAPP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427990806866" ID="ID_1772190872" MODIFIED="1427991130625" TEXT="PAs que no son AAPP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375864571106" ID="ID_184470778" MODIFIED="1427990561790" TEXT="Cualquier otro ente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427990357022" ID="ID_655725220" MODIFIED="1427990573034" TEXT="Con personalidad jur&#xed;dica propia,">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427990362206" ID="ID_1421091954" MODIFIED="1428128496247">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      creado para satisfacer necesidades
    </p>
    <p>
      de inter&#233;s general no industrial o mercantil
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427989591226" ID="ID_816102266" MODIFIED="1428128489195" TEXT="y que 1 o m&#xe1;s sujetos con condici&#xf3;n de PA financien &#xa;mayoritariamente su actividad o controlen su gesti&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1427990730846" ID="ID_1789940621" MODIFIED="1427991173130" TEXT="Resto del sector p&#xfa;blico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1427991297539" ID="ID_120882557" MODIFIED="1427992383768" TEXT="Excluidos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="closed"/>
<node CREATED="1427991300116" ID="ID_1447865249" MODIFIED="1427992383767" TEXT="Contratos laborales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427991365901" ID="ID_1282724336" MODIFIED="1427992383768" TEXT="Convenios de colaboraci&#xf3;n entre AAPP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428041644523" ID="ID_58095457" MODIFIED="1428041933650" TEXT="Prestaci&#xf3;n de un servicio p&#xfa;blico cuyo uso por los usuarios requiera el pago de una tasa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428041772330" ID="ID_527906011" MODIFIED="1428041933650" TEXT="Contratos de servicios financieros relativos a la gesti&#xf3;n financiera del Estado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428041774994" ID="ID_534152248" MODIFIED="1428041933650" TEXT="Compraventa, donaci&#xf3;n, arrendamiento de bienes inmuebles">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428041867698" ID="ID_1449905540" MODIFIED="1428041933646" TEXT="Contratos de servicios y suministros celebrados por OOPP de Investigaci&#xf3;n para la ejecuci&#xf3;n de proyectos de investigaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428042007851" ID="ID_776615562" MODIFIED="1428042106749" TEXT="Contratos de I+D siempre que el &#xf3;rgano de contrataci&#xf3;n comparta con los adjudicatarios &#xed;ntegramente los riesgos y beneficios de la investigaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375800951906" ID="ID_1402377216" MODIFIED="1428128505533" POSITION="right" TEXT="Tipos de contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428000043169" ID="ID_929667791" MODIFIED="1428000048657" TEXT="Seg&#xfa;n el objeto">
<node CREATED="1375800959561" ID="ID_1423005207" MODIFIED="1385013937363" TEXT="Obras">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375801024159" ID="ID_1063885149" MODIFIED="1375801033833" TEXT="Trabajos de construcci&#xf3;n o ingenier&#xed;a civil"/>
</node>
<node CREATED="1375800962016" ID="ID_478969231" MODIFIED="1378741462328" TEXT="Concesi&#xf3;n de obras p&#xfa;blicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427992553127" ID="ID_946572628" MODIFIED="1427992599952" TEXT="Realizaci&#xf3;n por el consesionario de prestaciones relativas al contrato de obras"/>
</node>
<node CREATED="1375800973210" ID="ID_760171350" MODIFIED="1378741464028" TEXT="Gesti&#xf3;n de servicios p&#xfa;blicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375800979909" ID="ID_125284141" MODIFIED="1427987942030" STYLE="bubble" TEXT="Suministro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375801056981" ID="ID_1093720325" MODIFIED="1427993216780">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Adquisici&#243;n,
    </p>
    <p>
      Arrendamiento financiero,
    </p>
    <p>
      Arrendamiento c/s opci&#243;n a compra
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375865597028" ID="ID_381533904" MODIFIED="1427993261889" TEXT="Equipos y sistemas de telecomunicaciones o para el tratamiento de informaci&#xf3;n"/>
<node CREATED="1375865608487" ID="ID_1675619232" MODIFIED="1427993299809" TEXT="Sus dispositivos y programas"/>
<node CREATED="1427993302514" ID="ID_835273759" MODIFIED="1427993413190">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cesi&#243;n de derecho de uso de los programas
    </p>
  </body>
</html></richcontent>
<node CREATED="1427986233005" ID="ID_625204178" MODIFIED="1428128899584">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Programas desarrollados a medida: Contratos de servicios
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="closed"/>
</node>
</node>
</node>
<node CREATED="1375801109748" ID="ID_588850466" MODIFIED="1427987942031" TEXT="Fabricaci&#xf3;n, aunque el contratatante se obligue a portar los materiales precisos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375800983053" ID="ID_945331404" MODIFIED="1427987933978" STYLE="bubble" TEXT="Servicios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427993454842" ID="ID_1921916475" MODIFIED="1427993506502" TEXT="Aquellos con objeto de &quot;hacer&quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427993475861" ID="ID_1523172444" MODIFIED="1427993506502" TEXT="Desarrollo de una actividad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427993480353" ID="ID_1746274544" MODIFIED="1427993506502" TEXT="Obtenci&#xf3;n de un resultado distinto de una obra o suministro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375801082378" ID="ID_1206016407" MODIFIED="1427993820432" TEXT="Entre otros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427993822959" ID="ID_594701090" MODIFIED="1428041321920" TEXT="Servicios de telecomunicaci&#xf3;n (Anexo II - Cat. 5)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427993848465" ID="ID_1762716522" MODIFIED="1428041321922" TEXT="Servicios de inform&#xe1;tica y servicios conexos (Anexo II - Cat. 7)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427993966185" ID="ID_1995800481" MODIFIED="1428041321920">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Incluyen
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427993832519" ID="ID_1551422390" MODIFIED="1428041321922" TEXT="Mantenimiento, conservaci&#xf3;n, limpieza y reparaci&#xf3;n de bienes, equipos e instalaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427993916602" ID="ID_1929643550" MODIFIED="1428041321925" TEXT="Adquisici&#xf3;n de programas de ordenador desarrollados a medida">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427993929290" ID="ID_1755671593" MODIFIED="1428041321925" TEXT="Adquisici&#xf3;n de servicios de telecomunicaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427993934587" ID="ID_1017318963" MODIFIED="1428041321924">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Servicios de consultor&#237;a en general
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427993942704" ID="ID_908765597" MODIFIED="1428041321922">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contratos de formaci&#243;n del personal de la AGE
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375807314428" ID="ID_57804635" MODIFIED="1427987933979" TEXT="Duraci&#xf3;n inferior a 4 a&#xf1;os">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375807334187" ID="ID_1652181423" MODIFIED="1427987933980" STYLE="bubble" TEXT="No m&#xe1;s de 6 a&#xf1;os con pr&#xf3;rrogas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375800989659" ID="ID_499959198" MODIFIED="1427994073090" STYLE="bubble" TEXT="Colaboraci&#xf3;n entre el sector p&#xfa;blico y el privado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427994196773" ID="ID_354698163" MODIFIED="1428128863272">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Realizaci&#243;n de una actuaci&#243;n global
    </p>
    <p>
      durante un periodo determinado
    </p>
  </body>
</html></richcontent>
<node CREATED="1427994302657" ID="ID_1656720775" MODIFIED="1428128653783" TEXT="Construcci&#xf3;n, instalaci&#xf3;n, transformaci&#xf3;n, mantenimiento &#xa;o explotaci&#xf3;n de obras o productos COMPLEJOS"/>
<node CREATED="1427994391792" ID="ID_1563862770" MODIFIED="1428128659955">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Fabricaci&#243;n de bienes y prestaci&#243;n de servicios
    </p>
    <p>
      que incorporen tecnolog&#237;a AVANZADA
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1375801206110" ID="ID_103811732" MODIFIED="1427994073091" TEXT="S&#xf3;lo cuando no son posibles otras formas de contrataci&#xf3;n"/>
<node CREATED="1375807403847" ID="ID_191383733" MODIFIED="1427994073091" STYLE="bubble" TEXT="No ser&#xe1;n superiores a 20 a&#xf1;os">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375801231842" ID="ID_639924854" MODIFIED="1427994533808" STYLE="bubble" TEXT="Mixtos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427994541490" ID="ID_1320339072" MODIFIED="1427994573855" TEXT="Incluyen prestaciones que corresponden a m&#xe1;s de un tipo de contrato">
<node CREATED="1427994575893" ID="ID_421317983" MODIFIED="1427994610596" TEXT="Ej: adquirir licencias SW (suministro) y su implantaci&#xf3;n (servicio)"/>
</node>
<node CREATED="1427994767168" ID="ID_1130435178" MODIFIED="1427994786492" TEXT="S&#xf3;lo si est&#xe1;n relacionadas y se complementan para satisfacer una necesidad "/>
<node CREATED="1427994654649" ID="ID_692665554" MODIFIED="1427994794159" TEXT="Se considerar&#xe1; del tipo de contrato al que perteneza la prestaci&#xf3;n mayoritaria econ&#xf3;micamente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1427994732082" ID="ID_478143440" MODIFIED="1428128874784">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      El contrato Mixto como tal NO EXISTE
    </p>
    <p>
      --&gt; Debe determinarse el tipo
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1428000060377" ID="ID_1756295301" MODIFIED="1428128519376" TEXT="Seg&#xfa;n su cuant&#xed;a">
<node CREATED="1375801246795" ID="ID_482464798" MODIFIED="1428128630834">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sujetos a una regulaci&#243;n
    </p>
    <p>
      armonizada (SARA)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375864649697" ID="ID_842647811" MODIFIED="1427995357535" TEXT="Contratos que por su tipo y cuant&#xed;a se encuentran sometidos a las directrices europeas sobre contrataci&#xf3;n p&#xfa;blica (2004/18/CE)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375801308228" ID="ID_673678648" MODIFIED="1375801316006" TEXT="No se consideran">
<node CREATED="1375801316006" ID="ID_197901282" MODIFIED="1427996996166" TEXT="Programas destinados a radiodifusi&#xf3;n"/>
<node CREATED="1375801341237" ID="ID_1460924283" MODIFIED="1375801344032" TEXT="I+D"/>
<node CREATED="1375801344349" ID="ID_675404200" MODIFIED="1375864755982" TEXT="Tratado de funcionamiento de la UE: Defensa"/>
<node CREATED="1427996967204" ID="ID_1178882282" MODIFIED="1427996970877" TEXT="Secretos o reservados"/>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1375801268393" ID="ID_911097802" MODIFIED="1427996347473" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Colaboraci&#243;n entre el sector p&#250;blico y privado: en todo caso ser&#225;n de regul. armoniz.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375801411242" ID="ID_11445492" MODIFIED="1479119186691">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Umbrales (HAP 2943/2015)
    </p>
    <p>
      (Valor Estimado = importe total sin IVA)
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modificados por la Orden HAP 2943/2015
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384080389104" ID="ID_1188786344" MODIFIED="1427996370070">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Suministros y Servicios
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1375801414949" ID="ID_1919959373" MODIFIED="1479119084488" STYLE="bubble" TEXT=" =&gt;135.000 &#x20ac;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375801424957" ID="ID_124373787" MODIFIED="1376903609897" TEXT="AGE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375801430235" ID="ID_746421630" MODIFIED="1427995571882" TEXT="EEGG y SSCC de la SS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1375801434486" ID="ID_1056008789" MODIFIED="1479119088239" STYLE="bubble" TEXT="=&gt;209.000 &#x20ac;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376903602731" ID="ID_893175788" MODIFIED="1376903613449" TEXT="Resto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1384080427819" ID="ID_939346114" MODIFIED="1479119203728" TEXT="Obras &gt;= 5.225.000 &#x20ac;"/>
<node CREATED="1375865737008" ID="ID_620064644" MODIFIED="1375865749537" TEXT="Umbrales sujetos a revisi&#xf3;n peri&#xf3;dica"/>
<node CREATED="1375801482160" ID="ID_1253657763" MODIFIED="1428128856968">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contratos por
    </p>
    <p>
      lotes separados
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1427995965790" ID="ID_1195601920" MODIFIED="1427996449513" TEXT="Se calcular&#xe1; el umbral sumando los lotes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1427996055782" ID="ID_1358935077" MODIFIED="1427996117270" TEXT="Si sobrepasa --&gt; regulaci&#xf3;n armonizada a cada lote "/>
<node CREATED="1427996473260" ID="ID_437141206" MODIFIED="1428128640875">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se podr&#225;n exceptuar
    </p>
    <p>
      de la regulaci&#243;n armonizada
    </p>
  </body>
</html></richcontent>
<node CREATED="1375801495836" ID="ID_759949051" MODIFIED="1427996226269" TEXT="Los lotes que cuyo v.e. &lt; 80.000&#x20ac;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375801506964" ID="ID_1706598993" MODIFIED="1428128647050">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Siempre que importe de lotes
    </p>
    <p>
      exceptuados no sobrepase el 20% del total
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1428000097688" ID="ID_1861193500" MODIFIED="1428000480670" TEXT="Contratos Menores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428000500563" ID="ID_994746505" MODIFIED="1428042579565" TEXT="Podr&#xe1;n adjudicarse directamente a cualquier empresario con capacidad de obrar y que cuente con la habilitaci&#xf3;n profesional necesaria">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428000596739" ID="ID_1804326081" MODIFIED="1428000631094" TEXT="Obras">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1428000600424" ID="ID_747000113" MODIFIED="1428000638480" STYLE="bubble" TEXT="&lt; 50.000&#x20ac;">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428000598681" ID="ID_739720457" MODIFIED="1428000631095" TEXT="Resto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1428000618233" ID="ID_1279878095" MODIFIED="1428000638478" STYLE="bubble" TEXT="&lt; 18.000&#x20ac;">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428042655042" ID="ID_1227925858" MODIFIED="1428042676397" TEXT="La tramitaci&#xf3;n del expediente s&#xf3;lo exigir&#xe1; la aprobaci&#xf3;n del gasto y la incorporaci&#xf3;n de la factura correspondiente"/>
<node CREATED="1428042608140" ID="ID_1518439715" MODIFIED="1428042608565" TEXT="Duraci&#xf3;n m&#xe1;xima 1 a&#xf1;o no prorrogable"/>
<node CREATED="1428042617625" ID="ID_623606607" MODIFIED="1428042619045" TEXT="No cabe revisi&#xf3;n de precios"/>
</node>
</node>
<node CREATED="1428000701958" ID="ID_1272256612" MODIFIED="1428000705997" TEXT="Seg&#xfa;n su naturaleza">
<node CREATED="1428000706163" ID="ID_1358754864" MODIFIED="1428000725327" TEXT="Contratos administrativos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428000743092" ID="ID_1819770292" MODIFIED="1428000749523" TEXT="Cualquiera celebrado por una AP"/>
</node>
<node CREATED="1428000719390" ID="ID_18193745" MODIFIED="1428000725327" TEXT="Contratos privados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428000750628" ID="ID_613690728" MODIFIED="1428000769341" TEXT="Cualquiera celebrado por un ente del sector p&#xfa;blico distinto a una AP"/>
</node>
</node>
</node>
</node>
</map>
