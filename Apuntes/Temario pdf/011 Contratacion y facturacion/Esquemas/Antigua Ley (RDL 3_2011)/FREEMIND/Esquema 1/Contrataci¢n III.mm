<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1375800625583" ID="ID_1995838940" MODIFIED="1479119570044">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      RD 3/2011, de 14nov
    </p>
    <p>
      &#160;-- TRLCSP --
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ojo: los d&#237;as se computar&#225;n como HABILES
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375803054614" ID="ID_1381916922" MODIFIED="1428131257479" POSITION="right" TEXT="Clasificaci&#xf3;n del contratista">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428045382046" ID="ID_408830901" MODIFIED="1428064008973" TEXT="Exigible">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384080932448" ID="ID_1432962407" MODIFIED="1384080941855" TEXT="Obras &gt;= 500.000 &#x20ac;"/>
</node>
<node CREATED="1428063912072" ID="ID_830501446" MODIFIED="1428063998525" TEXT="Lo har&#xe1;n las Comisiones Calificadoras de la Junta Consultiva de Contrataci&#xf3;n Administrativa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428045510313" ID="ID_369603507" MODIFIED="1428063976651" TEXT="Examinando solvencia econ&#xf3;mica, financiera y t&#xe9;cnica"/>
<node CREATED="1428063991042" ID="ID_1543371264" MODIFIED="1428063994715" TEXT="Ser&#xe1; v&#xe1;lido frente a cualquier &#xf3;rgano de contrataci&#xf3;n"/>
</node>
<node CREATED="1375803142738" ID="ID_1815215030" MODIFIED="1428063286859" TEXT="No es necesaria para empresarios no espa&#xf1;oles miembros de la UE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="closed"/>
<node CREATED="1428063305128" ID="ID_1085768741" MODIFIED="1428063310134" TEXT="Deber&#xe1;n acreditar su solvencia"/>
</node>
<node CREATED="1428063465686" ID="ID_1824638176" MODIFIED="1428064182148">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tendr&#225; vigencia INDEFINIDA
    </p>
    <p>
      <font size="2">(mientras se mantengan </font>
    </p>
    <p>
      <font size="2">condiciones y circunstancias)</font>
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428045608374" ID="ID_474912789" MODIFIED="1428063464864" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Justificaci&#243;n de la solvencia
    </p>
    <p>
      para conservaci&#243;n de la clasificaci&#243;n
    </p>
  </body>
</html></richcontent>
<node CREATED="1428045645183" ID="ID_1640116341" MODIFIED="1428064208186" TEXT="Anualmente, econ&#xf3;mica y financiera">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428045655029" ID="ID_988721951" MODIFIED="1428064210178" TEXT="Cada 3 a&#xf1;os, profesional o t&#xe9;cnica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428064063324" ID="ID_1764314358" MODIFIED="1428131413337">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      En los contratos de servicios, el plazo de c&#243;mputo
    </p>
    <p>
      de la experiencia ser&#225; de 5 a&#241;os
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1375801976938" ID="ID_1523058473" MODIFIED="1428131260498" POSITION="right" TEXT="Recurso especial de revisi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375802012046" ID="ID_1235560591" MODIFIED="1427997678162" TEXT="Recurso administrativo que se puede interponer previamente al contencioso-administrativo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428046227146" ID="ID_1261698223" MODIFIED="1428046283461" TEXT="Por PF o PJ cuyos dchos. o intereses leg&#xed;timos sean perjudicados o puedan resultar afectados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1381592718353" ID="ID_1731539734" MODIFIED="1385155224239" TEXT="Se puede aplicar a los contratos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1375802035704" ID="ID_924649542" MODIFIED="1428046364292" STYLE="bubble" TEXT="En todo caso, SARA">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1377531692040" ID="ID_455058424" MODIFIED="1428046517722" STYLE="bubble" TEXT="Servicios v.e. =&gt; 207.000 &#x20ac;">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384081548670" ID="ID_1328385554" MODIFIED="1428046349931" STYLE="bubble" TEXT="Gesti&#xf3;n de servicios p&#xfa;blicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384081566198" ID="ID_1880180333" MODIFIED="1428046349931" TEXT="&gt;= 500.000 &#x20ac;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384081576972" ID="ID_1600497653" MODIFIED="1428046349932" TEXT="Duraci&#xf3;n superior a 5 a&#xf1;os">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375802064158" ID="ID_1970793025" MODIFIED="1428046349932" STYLE="bubble" TEXT="Contratos subvencionados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1383047533933" ID="ID_1179526522" MODIFIED="1427983413557" TEXT="Son susceptibles de recurso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383047550853" ID="ID_214545551" MODIFIED="1383047637819" TEXT="Anuncios de licitaci&#xf3;n, pliegos y documentos contractuales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383047577603" ID="ID_564540050" MODIFIED="1428084246374" TEXT="Actos de tramite adoptados en el procedimiento de adjudicaci&#xf3;n &#xa;que DECIDAN DIRECTA O INDIRECTAMENTE SOBRE LA ADJUDICACI&#xd3;N">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383047598830" ID="ID_1246787244" MODIFIED="1383047635369" TEXT="Acuerdos de adjudicaci&#xf3;n adoptados por los &#xf3;rganos adjudicadores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1383047652571" ID="ID_670710408" MODIFIED="1427983416580" TEXT="NO SON SUSCEPTIBLES  ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383047662049" ID="ID_699787672" MODIFIED="1383047679071" TEXT="Acuerdos de modificaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428083891001" ID="ID_573708561" MODIFIED="1428084270512" TEXT="Actos de tr&#xe1;mite que no resuelven la licitaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428083910964" ID="ID_708922057" MODIFIED="1428084274559" TEXT="Penalizaciones impuestas por ejecuci&#xf3;n defectuosa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375802091341" ID="ID_958968823" MODIFIED="1428084277734" TEXT="Se excluye el tr&#xe1;mite de emergencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="closed"/>
</node>
</node>
<node CREATED="1375802126060" ID="ID_68616101" MODIFIED="1428046383859" STYLE="bubble" TEXT="Ante el Tribunal Administrativo Central de Recursos Contractuales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1380449538428" ID="ID_1807298949" MODIFIED="1428046383860" TEXT="Adscrito al MHAP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1381576292664" ID="ID_761298583" MODIFIED="1428046383860" TEXT="Presidente y dos vocales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375802282411" ID="ID_1353802935" MODIFIED="1377529960419" TEXT="Se pueden solicitar medidas provisionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383654026564" ID="ID_1420709397" MODIFIED="1385017804527" TEXT="Plazos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1375802336021" ID="ID_1349732466" MODIFIED="1377531815446" STYLE="bubble" TEXT="15 DIAS HABILES a partir de la notificaci&#xf3;n del acto impugnado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375802478598" ID="ID_984322020" MODIFIED="1377529965012" TEXT="Alegaciones: 5 d&#xed;as h&#xe1;biles">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375802490462" ID="ID_1450199730" MODIFIED="1377529967389" TEXT="Resoluci&#xf3;n: 5 d&#xed;as h&#xe1;biles, a partir de las alegaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375802556474" ID="ID_1351589276" MODIFIED="1428046406604" TEXT="Se puede multar si se contempla mala f&#xe9; en la interposici&#xf3;n de este recurso (1.000 y 15.000 &#x20ac;)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375803473070" ID="ID_347176319" MODIFIED="1428131262032" POSITION="right" STYLE="bubble" TEXT="Garant&#xed;as">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375803479477" ID="ID_335518810" MODIFIED="1428047662151" TEXT="GARANT&#xcd;A DEFINITIVA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428046803120" ID="ID_656370817" MODIFIED="1428046815082" TEXT="Deber&#xe1;n depositarla los licitadores que presenten las ofertas econ&#xf3;micamente m&#xe1;s ventajosas, como requisito para la adjudicaci&#xf3;n"/>
<node CREATED="1428047294589" ID="ID_1047103553" MODIFIED="1428047297830" TEXT="Responden">
<node CREATED="1428047319464" ID="ID_294312635" MODIFIED="1428047320742" TEXT="Penalizaciones impuestas por ejecuci&#xf3;n defectuosa o demoras"/>
<node CREATED="1428047322544" ID="ID_1422122871" MODIFIED="1428047336415" TEXT="Incorrecta ejecuci&#xf3;n de las prestaciones del contrato"/>
<node CREATED="1428047359294" ID="ID_804199414" MODIFIED="1428047406189" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div class="page" title="Page 31">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="SansSerif" size="3">Gastos causados a la Administracio&#769;n, por demoras del contratista</font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node CREATED="1428047337624" ID="ID_639846924" MODIFIED="1428047431309" TEXT="Da&#xf1;os y perjuicios a la Administraci&#xf3;n derivados de la ejecuci&#xf3;n del contrato o de su incumplimiento, cuando no proceda la resoluci&#xf3;n del mismo"/>
<node CREATED="1428047433027" ID="ID_1103479534" MODIFIED="1428047465815" TEXT="De la incautaci&#xf3;n que puede decretarse en los casos de resoluci&#xf3;n del contrato"/>
<node CREATED="1428047467561" ID="ID_734705141" MODIFIED="1428047517868" TEXT="Defectos de los bienes suministrados durante el plazo de garant&#xed;a (contratos de suministros)"/>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1428046752623" ID="ID_1812961095" MODIFIED="1428046772096" STYLE="bubble" TEXT="5% del importe de adjudicaci&#xf3;n, IVA excluido">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375803536384" ID="ID_850665569" MODIFIED="1384081417968" TEXT="Puede existir una COMPLEMENTARIA de otro 5% hasta el 10 % en total el importe del contrato ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1384081227979" ID="ID_1257875734" MODIFIED="1428047551784">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="3">Se puede eximir esta garant&#237;a, justific&#225;ndolo en los pliegos</font>
    </p>
    <p>
      <font size="2">(en especial caso de suministro de consumibles, cuya recepci&#243;n se deba hacer antes del pago)</font>
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<icon BUILTIN="yes"/>
<node CREATED="1384081301879" ID="ID_1444266909" MODIFIED="1384081328759" TEXT=" Salvo en los contratos de Obras y concesi&#xf3;n de obras p&#xfa;blicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
<icon BUILTIN="yes"/>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1428047074889" ID="ID_1939818966" MODIFIED="1428047078464" TEXT="Admitidas">
<node CREATED="1428047078821" ID="ID_863482263" MODIFIED="1428048416183" TEXT="Efectivo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428047100484" ID="ID_1117952270" MODIFIED="1428048416183" TEXT="Valores de Deuda P&#xfa;blica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428047104928" ID="ID_1198988515" MODIFIED="1428048416183" TEXT="Aval">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428047125472" ID="ID_1365643133" MODIFIED="1428048416183" TEXT="Contrato de seguro de cauci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375803667385" ID="ID_697301184" MODIFIED="1428131625849" TEXT="Acuerdo de devoluci&#xf3;n, deber&#xe1; adoptarse y notificarse en 2 meses DESDE EL PLAZO DE FINALIZACI&#xd3;N DE LA GARANT&#xcd;A">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384074053754" ID="ID_1545179314" MODIFIED="1428131708018">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si no se devuelve en ese plazo, habr&#225; que
    </p>
    <p>
      incrementar cantidad abonada con el inter&#233;s legal del dinero
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1428132149214" ID="ID_884712817" MODIFIED="1428132203314" STYLE="fork" TEXT="En el caso de recepci&#xf3;n parcial del contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428132155747" ID="ID_1015941926" MODIFIED="1428132203315" TEXT="El contratista s&#xf3;lo podr&#xe1; solicitar devoluci&#xf3;n o cancelaci&#xf3;n de la parte proporcional ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1382945393741" ID="ID_1279442949" MODIFIED="1428132081177">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Transcurrido 1 a&#241;o desde la terminaci&#243;n del contrato, si no se ha
    </p>
    <p>
      producido la&#160;&#160;recepci&#243;n&#160;formal ni liquidaci&#243;n&#160;del contrato
    </p>
    <p>
      (por causas no imputables al contratista)
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Por causas no imputables al contratista.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1382945398822" ID="ID_590116894" MODIFIED="1428131855278">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se proceder&#225; a la devoluci&#243;n o cancelaci&#243;n de las garant&#237;as sin m&#225;s demora
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Con la depuraci&#xf3;n de responsabilidades
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382945405643" ID="ID_1435641746" MODIFIED="1428131511821" TEXT="6 meses si el contrato &lt; 100.000 &#x20ac;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382945424213" ID="ID_412548529" MODIFIED="1382945439007" TEXT="6 meses si la empresa es una PYME">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428047618679" ID="ID_1076635620" MODIFIED="1428048386410" TEXT="GARANT&#xcd;A GLOBAL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428047623317" ID="ID_1525205886" MODIFIED="1428047645247" TEXT="Para responder ante responsabilidades de TODOS los contratos que celebre una empresa con una Administraci&#xf3;n P&#xfa;blica o con uno o varios &#xf3;rganos de contrataci&#xf3;n"/>
<node CREATED="1428047740752" ID="ID_484844465" MODIFIED="1428047866237" TEXT="Parte que vaya afectando a cada contrato queda INMOVILIZADA y ligada al mismo mientras sea preciso"/>
</node>
</node>
<node CREATED="1375803493242" ID="ID_1401147053" MODIFIED="1428047924473">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      GARANT&#205;A PROVISIONAL
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428047949384" ID="ID_368429207" MODIFIED="1428048410388" TEXT="La PODR&#xc1;N EXIGIR &#xf3;rganos de contrataci&#xf3;n para responder del mantenimiento de las ofertas hasta la adjudicaci&#xf3;n del contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428048043202" ID="ID_783667973" MODIFIED="1428048410392" TEXT="Debe quedar suficientemente JUSTIFICADA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1428047925669" ID="ID_129001324" MODIFIED="1428048134775" STYLE="bubble" TEXT="No superior al 3% del importe de adjudicaci&#xf3;n, IVA excluido">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382945526083" ID="ID_1970627689" MODIFIED="1428048163331" TEXT="Devoluci&#xf3;n: inmediatamente tras la adjudicaci&#xf3;n del contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384081341809" ID="ID_299325802" MODIFIED="1428048303656" TEXT="Adjudicatario: se retendr&#xe1; hasta que constituya la garant&#xed;a definitiva">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428048334031" ID="ID_1511572310" MODIFIED="1428048357852" TEXT="Se puede utilizar el importe depositado como garant&#xed;a &#xa;provisional para la constituci&#xf3;n de la definitiva (o no)"/>
</node>
</node>
<node CREATED="1375865197181" ID="ID_511820850" MODIFIED="1383648077904" TEXT="Se podr&#xe1;n constituir por medios TIC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375865272427" ID="ID_1840559598" MODIFIED="1385026290093" TEXT="Se">
<node CREATED="1375865276602" ID="ID_1059631133" MODIFIED="1428048493844" TEXT="Reponen, si se ha usado total o parcialmente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1428048494060" ID="ID_488446019" MODIFIED="1428048587515" STYLE="bubble" TEXT="A los 15 d&#xed;as desde la ejecuci&#xf3;n">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375865307777" ID="ID_540360091" MODIFIED="1428048563046" TEXT="Reajustan, por variaci&#xf3;n del precio del contrato debido a una MODIFICACI&#xd3;N">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1428048563272" ID="ID_1427326744" MODIFIED="1428048587512" STYLE="bubble" TEXT="A los 15 d&#xed;as de la notificaci&#xf3;n de la modificaci&#xf3;n">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375865323369" ID="ID_1176631965" MODIFIED="1428048658282" TEXT="Devuelven o cancelan">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428048635144" ID="ID_878053703" MODIFIED="1428048650143" TEXT="Vence el per&#xed;odo de garant&#xed;a del contrato y &#xe9;ste se ha cumplido de forma satisfactoria"/>
<node CREATED="1428048642362" ID="ID_1662948203" MODIFIED="1428048653705" TEXT="Cuando se resuelve el contrato, sin que exista culpa por parte del contratista"/>
</node>
</node>
</node>
</node>
</map>
