<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1375800625583" ID="ID_1995838940" MODIFIED="1479119829510">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      RD 3/2011, de 14nov
    </p>
    <p>
      &#160;-- TRLCSP --
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ojo: los d&#237;as se computar&#225;n como HABILES
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375805008841" ID="ID_268121546" MODIFIED="1428134227588" POSITION="right" TEXT="Confidencialidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428079614835" ID="ID_745170614" MODIFIED="1428079723958">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 69" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font size="3" face="SansSerif">&#211;rganos de contratacio&#769;n no divulgar informacio&#769;n facilitada por </font>
          </p>
          <p>
            <font size="3" face="SansSerif">empresarios que e&#769;stos hayan designado confidencial</font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375805013827" ID="ID_354658138" MODIFIED="1428079595487" STYLE="bubble" TEXT=" 5 a&#xf1;os, salvo plazo mayor en los pliegos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375805458795" ID="ID_1154930121" MODIFIED="1428134308347" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Racionalizaci&#243;n
    </p>
    <p>
      de la contrataci&#243;n
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375806550845" ID="ID_1849408957" MODIFIED="1428079736750" TEXT="Acuerdo Marco">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428084600236" ID="ID_737030220" MODIFIED="1428084637179" TEXT="Establecen un &#xe1;mbito de condiciones, para formalizaci&#xf3;n posterior de contratos basados en el mismo">
<arrowlink DESTINATION="ID_737030220" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_634568058" STARTARROW="None" STARTINCLINATION="0;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_737030220" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_634568058" SOURCE="ID_737030220" STARTARROW="None" STARTINCLINATION="0;0;"/>
</node>
<node CREATED="1428084655261" ID="ID_1033326494" MODIFIED="1428084744058" TEXT="Se concluir&#xe1;n (resolver&#xe1;n) con 1 o varios empresarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375806559805" ID="ID_1481633454" MODIFIED="1428084737054" STYLE="bubble" TEXT="Al menos tres empresarios si concluye con varios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375806567784" ID="ID_1537172513" MODIFIED="1428085032831" STYLE="bubble" TEXT="M&#xe1;ximo 4 a&#xf1;os, salvo casos excepcionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375867151199" ID="ID_287493293" MODIFIED="1383651327650" TEXT="Etapas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375867154968" ID="ID_1056313796" MODIFIED="1428085351398">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Adjudicaci&#243;n del
    </p>
    <p>
      acuerdo marco
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1428084809225" ID="ID_731845055" MODIFIED="1428084814431" TEXT="Reglas del procedimiento general"/>
<node CREATED="1428084814673" ID="ID_1712139394" MODIFIED="1428085216525" TEXT="Publicaci&#xf3;n de la licitaci&#xf3;n en el Perfil del Contratante + BOE o DO + DOUE (SARA)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428085061679" ID="ID_1470332568" MODIFIED="1428085206091" TEXT="Publicaci&#xf3;n en los diarios oficiales m&#xe1;x. 48 d&#xed;as desde contrataci&#xf3;n"/>
</node>
<node CREATED="1375867169607" ID="ID_1322585839" MODIFIED="1428085352863">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Adjudicaci&#243;n sucesiva de contratos
    </p>
    <p>
      basados en el acuerdo marco
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1428085235794" ID="ID_1324722978" MODIFIED="1428085348816" TEXT="Suscritos con 1 empresario">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428085241685" ID="ID_1248898105" MODIFIED="1428085348817" TEXT="Emplear los t&#xe9;rminos del AM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428085254998" ID="ID_1172748709" MODIFIED="1428085348817" TEXT="Suscritos con varios empresarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428085259724" ID="ID_909560151" MODIFIED="1428085613253">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Todos los t&#233;rminos del contrato
    </p>
    <p>
      est&#225;n definidos en el AM
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1428085323038" ID="ID_650046816" MODIFIED="1428085343771" TEXT="Adjudicaci&#xf3;n directa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428085378222" ID="ID_1765486735" MODIFIED="1428085621309">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Necesario a&#241;adir o
    </p>
    <p>
      detallar alg&#250;n t&#233;rmino
    </p>
    <p>
      <font size="2">(adjudicaci&#243;n simplificada)</font>
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<icon BUILTIN="yes"/>
<node CREATED="1428085388311" ID="ID_1183429262" MODIFIED="1428085609576" TEXT="Consulta a las empresas del AM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1428085457396" ID="ID_435378037" MODIFIED="1428085505573" TEXT="No SARA, podr&#xe1; solicitar ofertas s&#xf3;lo a 3"/>
</node>
<node CREATED="1428085423967" ID="ID_428359230" MODIFIED="1428085609575" TEXT="Recepci&#xf3;n de ofertas (no especifica plazo)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1428086228867" ID="ID_926181629" MODIFIED="1428086242195" TEXT="Se puede usar la subasta electr&#xf3;nica"/>
</node>
<node CREATED="1428085534460" ID="ID_1080302498" MODIFIED="1428085609575" TEXT="Elecci&#xf3;n de la mejor oferta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1428085594099" ID="ID_1081545020" MODIFIED="1428085599384" TEXT="Los criterios deben figurar en el AM"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1375806572566" ID="ID_1204193474" MODIFIED="1428079737662" TEXT="Sistema din&#xe1;mico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375867215650" ID="ID_802760294" MODIFIED="1428086134089" TEXT="Instrumento similar al Acuerdo Marco, EXCLUSIVAMENTE con medios TIC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428086635174" ID="ID_1137938714" MODIFIED="1428086646661" TEXT="Al AM se &quot;concluye&quot;, el SD se &quot;articula&quot;"/>
</node>
<node CREATED="1428085830689" ID="ID_1410160145" MODIFIED="1428085853243" TEXT="Limitado a obras, servicios y suministros de USO CORRIENTE, con caracter&#xed;sticas generalmente disponibles en el mercado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="info"/>
</node>
<node CREATED="1375806567784" ID="ID_1864867921" MODIFIED="1428085032831" STYLE="bubble" TEXT="M&#xe1;ximo 4 a&#xf1;os, salvo casos excepcionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375806596303" ID="ID_699494104" MODIFIED="1428086148341">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Seguir&#225; las normas del procedimiento ABIERTO en todas sus fases
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428085897709" ID="ID_1177085651" MODIFIED="1428085902516" TEXT="Diferencias">
<node CREATED="1428085984356" ID="ID_724871538" MODIFIED="1428086028367" TEXT="Los licitadores presentar&#xe1;n una OFERTA INDICATIVA que cumpla criterios de selecci&#xf3;n"/>
<node CREATED="1428085902712" ID="ID_1531861818" MODIFIED="1428086033320" TEXT="La adjudicaci&#xf3;n se realizar&#xe1; a TODOS los licitadores que cumplan los criterios de selecci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375806603797" ID="ID_123118524" MODIFIED="1428086090272" TEXT="Se pueden incorporar nuevos adjudicatarios una vez el sistema din&#xe1;mico est&#xe9; vigente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428086039230" ID="ID_1485921550" MODIFIED="1428086316197" TEXT="Adjudicaci&#xf3;n">
<node CREATED="1428086316407" ID="ID_1076131174" MODIFIED="1428086409121" TEXT="Licitaci&#xf3;n, convocando a todos los licitadores del sistema din&#xe1;mico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428086344597" ID="ID_335537742" MODIFIED="1428086355186" TEXT="Publicidad en el caso de SARA"/>
<node CREATED="1375806641182" ID="ID_1083515268" MODIFIED="1428086454749" STYLE="bubble" TEXT="15 d&#xed;as para la evaluaci&#xf3;n de ofertas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375806666946" ID="ID_123374935" MODIFIED="1428086481081" TEXT="Se puede usar la subasta electr&#xf3;nica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="info"/>
</node>
</node>
<node CREATED="1428086355595" ID="ID_1019161595" MODIFIED="1428086381500" TEXT="Publicidad para la adjudicaci&#xf3;n">
<node CREATED="1428086400230" ID="ID_990331468" MODIFIED="1428086405436" TEXT="Se podr&#xe1; agrupar trimestralmente"/>
</node>
</node>
</node>
<node CREATED="1375867287059" ID="ID_6967284" MODIFIED="1383651351582" TEXT="Participaci&#xf3;n gratuita de las empresas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375806691087" ID="ID_520753249" MODIFIED="1428095009132" TEXT="Centrales de contrataci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428086546275" ID="ID_104771702" MODIFIED="1428087013838" TEXT="Las entidades del SP podr&#xe1;n centralizar la contrataci&#xf3;n de obras, &#xa;servicios y suministros, atribuy&#xe9;ndola a organizaciones especializadas">
<node CREATED="1428086669523" ID="ID_763999052" MODIFIED="1428086778942">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Podr&#225;n, con destino a
    </p>
    <p>
      otros &#243;rganos de contrataci&#243;n
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428086682469" ID="ID_62502606" MODIFIED="1428086778942" TEXT="Concluir acuerdos marco">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428086729624" ID="ID_25830136" MODIFIED="1428086778940">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Adjudicar contratos
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428086736214" ID="ID_1019371960" MODIFIED="1428086778938">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Realizar adquisiciones
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1428086857561" ID="ID_1894771767" MODIFIED="1428086874428" TEXT="La mayor&#xed;a de contrataci&#xf3;n de bienes y servicios TIC se realiza por esta v&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="idea"/>
</node>
<node CREATED="1375806717164" ID="ID_1757185901" MODIFIED="1428094417479">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sistema de Contrataci&#243;n
    </p>
    <p>
      Centralizada Estatal
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428087039293" ID="ID_1828948658" MODIFIED="1428087043182" TEXT="Organismos">
<node CREATED="1375806728326" ID="ID_747868337" MODIFIED="1428087090560" TEXT="Junta de Contratacion Centralizada (JCC)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428087111807" ID="ID_1295592037" MODIFIED="1428093500072" TEXT="&#xd3;rgano de contrataci&#xf3;n del sistema estatal de contrataci&#xf3;n centralizada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428087119776" ID="ID_1007232638" MODIFIED="1428087901740" TEXT="Desconcentra en la DGRCC todas sus competencias, excepto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428087158998" ID="ID_291070301" MODIFIED="1428093857141" STYLE="bubble" TEXT="Adjudicaci&#xf3;n de procedimientos especiales de adopci&#xf3;n de tipo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1428087167707" ID="ID_750414407" MODIFIED="1428087181900" TEXT="Adjudicaci&#xf3;n de contratos que no se deriven de un acuerdo marco">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1428087046549" ID="ID_1169455993" MODIFIED="1428087093609" TEXT="Direcci&#xf3;n General de Racionalizaci&#xf3;n y Centralizaci&#xf3;n de la Contrataci&#xf3;n (DGRCC)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384084529856" ID="ID_587595542" MODIFIED="1384096443033" TEXT="Preparaci&#xf3;n y tramitaci&#xf3;n de los expedientes de contrataci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384084584061" ID="ID_1076091281" MODIFIED="1385024053462" TEXT="Coordinaci&#xf3;n y elaboraci&#xf3;n de los PCAP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1428087290923" ID="ID_1124538240" MODIFIED="1428087293564" TEXT="Funcionamiento">
<node CREATED="1428087293722" ID="ID_1853373960" MODIFIED="1428088068047" TEXT="El Ministro HAP declara los suministros y servicios de contrataci&#xf3;n centralizada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1428087344572" ID="ID_1474517247" MODIFIED="1428093602402" TEXT="Bienes y servicios de caracter&#xed;sticas homog&#xe9;neas y que se contraten de forma generalizada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428088690588" ID="ID_36866177" MODIFIED="1428094018355" TEXT="Informe preceptivo de la DTIC de la declaraci&#xf3;n &#xa;de contrataci&#xf3;n centralizada de suministros y servicios TIC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="info"/>
</node>
</node>
<node CREATED="1428087431577" ID="ID_1530691560" MODIFIED="1428094149561">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contrataci&#243;n de suministros y servicios
    </p>
    <p>
      CENTRALIZADOS por la DGRCC (Modalidades)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1428087442583" ID="ID_793734436" MODIFIED="1428094263576" TEXT="Licitar directamente el contrato (arreglo a normas generales)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1428087449065" ID="ID_1591182142" MODIFIED="1428093784569">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      A trav&#233;s del procedimiento especial
    </p>
    <p>
      de adopci&#243;n de tipo. Fases:
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1428087626491" ID="ID_12986285" MODIFIED="1428087679567">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Conclusi&#243;n de un acuerdo marco
    </p>
    <p>
      o apertura de un sistema din&#225;mico
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1428087665580" ID="ID_912638483" MODIFIED="1428087677809">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Adopci&#243;n de los tipos contratables
    </p>
    <p>
      para cada clase de bienes o servicios
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1428087721072" ID="ID_1673703254" MODIFIED="1428087744744" TEXT="Contrataci&#xf3;n espec&#xed;fica basada en el AM o SD">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
</node>
</node>
<node CREATED="1428088604712" ID="ID_1056410304" MODIFIED="1428094199370">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      La contrataci&#243;n se har&#225; a trav&#233;s de&#160;la DGRCC
    </p>
    <p>
      (central de contrataci&#243;n &#250;nica)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1428088667500" ID="ID_1452272439" MODIFIED="1428088684711" TEXT="Los &#xf3;rganos de la AGE contratar&#xe1;n estos bienes y servicios a trav&#xe9;s de la DGRCC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428088775945" ID="ID_1529396854" MODIFIED="1428088915579" TEXT="Cuando sea necesaria LICITACI&#xd3;N, ser&#xe1; responsabilidad &#xa;del &#xf3;rgano interesado, no de la DGRCC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428088885801" ID="ID_876112640" MODIFIED="1428088917796" TEXT="Gesti&#xf3;n presupuestaria de los contratos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428088896099" ID="ID_779849886" MODIFIED="1428088920132" TEXT="Responsabilidad del &#xf3;rgano interesado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428087442583" ID="ID_1618191596" MODIFIED="1428094263576" TEXT="Licitar directamente el contrato (arreglo a normas generales)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1383808435888" ID="ID_949209399" MODIFIED="1428091380061" TEXT="EXCEPCION">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383808454772" ID="ID_345358853" MODIFIED="1428091380061" TEXT="Podr&#xe1; hacerse por el &#xf3;rgano interesado (no centralizado)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383808473750" ID="ID_625998742" MODIFIED="1428091380061" TEXT="Se justifique por las caracter&#xed;sticas del servicio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383808500200" ID="ID_1727324250" MODIFIED="1428091380061" TEXT="Previo informe favorable de la DGRCC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1375868870221" ID="ID_1822423367" MODIFIED="1384097544299" TEXT="Podr&#xe1;n adherirse CCAA  y entes locales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375950790923" ID="ID_1963728865" MODIFIED="1479119845651">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Orden EHA 1049/2008 de declaraci&#243;n de bienes
    </p>
    <p>
      y servicios de contrataci&#243;n centralizada
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375950816210" ID="ID_835341197" MODIFIED="1428094426480">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Suministros de
    </p>
    <p>
      contrataci&#243;n centralizada
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375952510904" ID="ID_213807973" MODIFIED="1479119882899" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Equipos para el tratamiento de la info. y sus programas
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428095211230" ID="ID_364757795" MODIFIED="1428095224663" TEXT="No podr&#xe1;n incluirse por importe mayor en los AM"/>
</node>
<node CREATED="1375952578918" ID="ID_771915990" MODIFIED="1428095494382" TEXT="Los AM de suministros TIC suelen contener todos los t&#xe9;rminos del contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1382341739589" ID="ID_1264160420" MODIFIED="1428095124544" TEXT="En ese caso, no es necesaria la licitaci&#xf3;n --&gt; adjudicaci&#xf3;n directa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375952542399" ID="ID_622257838" MODIFIED="1382341734790" TEXT="Sin l&#xed;mite en el importe del contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428092886503" ID="ID_1414859950" MODIFIED="1428094431231">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Servicios de
    </p>
    <p>
      contrataci&#243;n centralizada
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1383638295636" ID="ID_1279529812" MODIFIED="1428095444279">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Servicios dirigidos al desarrollo
    </p>
    <p>
      de la eAdmin &lt;= 862.000&#8364;
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1383808152947" ID="ID_1841321197" MODIFIED="1428091380061" TEXT="Consultor&#xed;a, planificaci&#xf3;n, analisis, dise&#xf1;o...y mantenimientos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383808316388" ID="ID_1705650551" MODIFIED="1428091380061" TEXT="Alojamientos y servicios remotos para dar soporte">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428095385034" ID="ID_1915540310" MODIFIED="1479119856187">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      AM 26/2015 (Servicios de desarrollo
    </p>
    <p>
      de sistemas de informaci&#243;n)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1428095736730" ID="ID_959854815" MODIFIED="1428095764507" TEXT="Consultor&#xed;a, Desarrollo, Mantenimiento"/>
<node CREATED="1428095740274" ID="ID_1727058312" MODIFIED="1428095792773" TEXT="Establece experiencia, formaci&#xf3;n, precios/hora"/>
</node>
<node CREATED="1428095431507" ID="ID_1201578704" MODIFIED="1428095731102">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      AM 27/2012 Servicios de alojamiento
    </p>
    <p>
      de sistemas de informaci&#243;n (hosting y housing)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1383808372199" ID="ID_1303304727" MODIFIED="1479119896795">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contratos de servicios de comunicaciones
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1375952221021" ID="ID_825931720" MODIFIED="1428092658198">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      4 a&#241;os, SIN PR&#211;RROGA
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375952262086" ID="ID_1699596960" MODIFIED="1428095688378">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Siempre es necesario a&#241;adir t&#233;rminos y condiciones a las del AM
    </p>
    <p>
      <font size="2">(plazos de ejecuci&#243;n, definici&#243;n de objeto concreto a contratar)</font>
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1384097151238" ID="ID_1386218186" MODIFIED="1428095682671" TEXT="Es necesario una nueva licitaci&#xf3;n en el &#xe1;mbito del AM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384096900846" ID="ID_1036493934" MODIFIED="1428095667830">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      LICITACION POR EL ORGANISMO INTERESADO
    </p>
    <div class="page" title="Page 49">
      <div class="layoutArea">
        <div class="column">
          <p>
            <span style="font-size: 10.000000pt; font-family: Helvetica"><font size="3" face="SansSerif">(Presentara&#769; una propuesta de adjudicacio&#769;n)</font><font size="10.000000pt" face="Helvetica">&#160;</font></span>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1383808435888" ID="ID_1839710120" MODIFIED="1428091380061" TEXT="EXCEPCION">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383808454772" ID="ID_159290948" MODIFIED="1428091380061" TEXT="Podr&#xe1; hacerse por el &#xf3;rgano interesado (no centralizado)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383808473750" ID="ID_140285987" MODIFIED="1428091380061" TEXT="Se justifique por las caracter&#xed;sticas del servicio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383808500200" ID="ID_1475079668" MODIFIED="1428091380061" TEXT="Previo informe favorable de la DGRCC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
