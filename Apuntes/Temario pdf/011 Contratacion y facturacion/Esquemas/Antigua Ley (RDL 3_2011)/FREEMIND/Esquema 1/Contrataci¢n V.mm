<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1375800625583" ID="ID_1995838940" MODIFIED="1428132464959">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      RD 3/2011, de 14nov
    </p>
    <p>
      &#160;-- TRLCSP --
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ojo: los d&#237;as se computar&#225;n como HABILES
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375804283298" ID="ID_232126948" MODIFIED="1428133202839" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Selecci&#243;n de proveedor
    </p>
    <p>
      y adjudicaci&#243;n
    </p>
    <p>
      (I)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
<node CREATED="1428057385069" ID="ID_1154739815" MODIFIED="1428132476113" TEXT="Principios">
<node CREATED="1428057387444" ID="ID_743175936" MODIFIED="1428057422484" TEXT="IGUALDAD en el tratamiento a los licitadores"/>
<node CREATED="1428057404123" ID="ID_378070034" MODIFIED="1428057426805" TEXT="TRANSPARENCIA en las actuaciones del &#xf3;rgano de contrataci&#xf3;n"/>
</node>
<node CREATED="1375805028684" ID="ID_341635921" MODIFIED="1428132476119" TEXT="Publicidad y Transparencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375808230483" ID="ID_962094978" MODIFIED="1428133168504">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Anuncio previo
    </p>
    <p>
      (opcional/potestativo)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="attach"/>
<node CREATED="1375808239811" ID="ID_109970133" MODIFIED="1428057649524" TEXT="Contratos que se prev&#xe9; adjudicar los 12 meses siguientes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378744237380" ID="ID_207718273" MODIFIED="1428132477032" TEXT="Contenido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378744243924" ID="ID_1828510123" MODIFIED="1428057840019" STYLE="bubble" TEXT="Suministros y Servicios con valor total estimado superior a 750.000 &#x20ac;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428057847934" ID="ID_9688220" MODIFIED="1428132477034" TEXT="Publicaci&#xf3;n en uno de los siguientes">
<node CREATED="1375866175619" ID="ID_1572765279" MODIFIED="1428058753806" TEXT="DOUE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1378744292512" ID="ID_1435159799" MODIFIED="1428132477905" TEXT="Perfil del contratante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1378744309743" ID="ID_786500171" MODIFIED="1428132478121" TEXT="Debe comunicarse previamente a ">
<node CREATED="1378744318331" ID="ID_364370928" MODIFIED="1378744320812" TEXT="BOE"/>
<node CREATED="1378744321101" ID="ID_1643180736" MODIFIED="1378744335952" TEXT="Comisi&#xf3;n Europea"/>
</node>
<node CREATED="1428060206066" ID="ID_376523997" MODIFIED="1428060213468" TEXT="Indicar en el perfil la fecha de env&#xed;o de las comunicaciones"/>
</node>
</node>
<node CREATED="1428057923648" ID="ID_1247879498" MODIFIED="1428132477035" STYLE="bubble" TEXT="Permitir&#xe1; REDUCCI&#xd3;N de PLAZOS para la presentaci&#xf3;n de las OFERTAS">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ojo: los d&#237;as se computar&#225;n como HABILES
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428057977393" ID="ID_1870405066" MODIFIED="1428058001636" TEXT="En procedimientos ABIERTOS y RESTRINGIDOS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428058023028" ID="ID_1356993058" MODIFIED="1428058149512" TEXT="Publicaci&#xf3;n &lt; 52 d&#xed;as del env&#xed;o del anuncio de licitaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1378812806428" ID="ID_607357588" MODIFIED="1428132476520" TEXT="Anuncio de licitaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="attach"/>
<node CREATED="1383048977969" ID="ID_464422142" MODIFIED="1428132477035" TEXT="Contratos menores (&lt; 18.000 servicios, &lt;50.000 obras)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383125092088" ID="ID_1116751534" MODIFIED="1383125259066" TEXT="No requieren publicidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1383124921505" ID="ID_1937877263" MODIFIED="1428132477039" TEXT="Negociados que requieren publicidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383124954282" ID="ID_605306904" MODIFIED="1428059520955" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      60.000 &lt; v.e. &lt; 100.000&#8364;&#160;&#160;servicios
    </p>
    <p>
      200.000 &lt; v.e. &lt; 1.000.000&#8364; obras
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377535365119" ID="ID_79603664" MODIFIED="1428058764382" TEXT="Perfil del contratante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1428059581601" ID="ID_829325934" MODIFIED="1428059610098" TEXT="BOE o DO si no se hace a trav&#xe9;s del Perfil del contratante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node CREATED="1378812821364" ID="ID_109634933" MODIFIED="1428132477040" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Todos los dem&#225;s NO SARA
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428058525543" ID="ID_1649552773" MODIFIED="1428058761094" TEXT="BOE o DO">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1377535365119" ID="ID_1147600926" MODIFIED="1428058764382" TEXT="Perfil del contratante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1428058414591" ID="ID_909954535" MODIFIED="1428058765030" TEXT="DOUE cuando lo estime conveniente el &#xf3;rgano de contrataci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node CREATED="1375805076515" ID="ID_1875151900" MODIFIED="1428132477041" STYLE="bubble" TEXT="SARA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428058525543" ID="ID_1311272879" MODIFIED="1428058761094" TEXT="BOE o DO">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1377535365119" ID="ID_640618522" MODIFIED="1428058764382" TEXT="Perfil del contratante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1428058414591" ID="ID_1663008622" MODIFIED="1428058896793" TEXT="DOUE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1428058824486" ID="ID_76185212" MODIFIED="1428059646718" TEXT="El env&#xed;o al DOUE preceder&#xe1; a cualquier otra publicidad, donde se indicar&#xe1; la fecha de ese env&#xed;o">
<arrowlink DESTINATION="ID_76185212" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_1305545832" STARTARROW="None" STARTINCLINATION="0;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_76185212" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_1305545832" SOURCE="ID_76185212" STARTARROW="None" STARTINCLINATION="0;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375808251650" ID="ID_857157669" MODIFIED="1428132476522">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Perfil del Contratante de
    </p>
    <p>
      cada &#243;rgano de contrataci&#243;n
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="gohome"/>
<node CREATED="1375802785615" ID="ID_1089069317" MODIFIED="1428132477045" TEXT="Deber&#xe1; contener">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375802792083" ID="ID_1403990289" MODIFIED="1377534416301" TEXT="EN TODO CASO la adjudicaci&#xf3;n de contratos, excepto los menores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1378744718181" ID="ID_317120232" MODIFIED="1428059937982">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Anuncios de licitaci&#243;n y anuncios previos
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375802872234" ID="ID_1474774192" MODIFIED="1383655449839" TEXT="Cualesquiera datos referentes a la actividad contractual">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375802799436" ID="ID_1840559285" MODIFIED="1428060564378" TEXT="DEBE tener un dispositivo para acreditar fehacientemente &#xa;MOMENTO de inicio de DIFUSION de la informaci&#xf3;n p&#xfa;blica     ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1381311873824" ID="ID_807736498" MODIFIED="1428132477046">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      De obligada publicaci&#243;n en Internet
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377533625260" ID="ID_727611645" MODIFIED="1428060825144" TEXT="Se integrar&#xe1;n TODOS en la PCSP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1428060263169" ID="ID_1038230562" MODIFIED="1428133175543">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      La Plataforma de Contrataci&#243;n
    </p>
    <p>
      del Sector P&#250;blico
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="gohome"/>
<node CREATED="1428060335349" ID="ID_1508969672" MODIFIED="1428060420064" TEXT="En el &#xe1;mbito de la Junta Consultiva de Contrataci&#xf3;n Administrativa del Estado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428060375444" ID="ID_1408766979" MODIFIED="1428132477047" TEXT="Plataforma electr&#xf3;nica para dar publicidad a trav&#xe9;s de Internet">
<node CREATED="1428060384846" ID="ID_1358957512" MODIFIED="1428060469058" TEXT="Convocatorias de licitaciones y sus resultados"/>
<node CREATED="1428060454685" ID="ID_1705356868" MODIFIED="1428060458207" TEXT="Servicios complementarios asociados al tratamiento inform&#xe1;tico de estos datos"/>
<node CREATED="1428060402997" ID="ID_1339250724" MODIFIED="1428060414467" TEXT="Cualquier otra info de inter&#xe9;s"/>
</node>
<node CREATED="1375802799436" ID="ID_581162566" MODIFIED="1428060578122" TEXT="DEBE tener un dispositivo para acreditar fehacientemente&#xa;MOMENTO de inicio de DIFUSION de la informaci&#xf3;n p&#xfa;blica     ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428060647931" ID="ID_1259005566" MODIFIED="1428132477049" TEXT="Los perfiles de contratante de TODOS los &#xf3;rganos de contrataci&#xf3;n &#xa;del sector p&#xfa;blico deber&#xe1;n integrarse en esta plataforma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428060683164" ID="ID_1129466049" MODIFIED="1428060745071" TEXT="Las sedes electr&#xf3;nicas de estos &#xf3;rganos incluir&#xe1;n enlace &#xa;a su perfil del contratante situado en la PCSP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1428062301269" ID="ID_1085215342" MODIFIED="1428133180471">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ley 19/2013, de 19dic,
    </p>
    <p>
      Ley de Transparencia
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428062348036" ID="ID_160083552" MODIFIED="1428132477054" TEXT="Informaci&#xf3;n m&#xed;nima que se debe publicar de TODOS los contratos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428062365444" ID="ID_1828532005" MODIFIED="1428062404878" TEXT="Objeto&#xa;">
<arrowlink DESTINATION="ID_1828532005" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_1265891064" STARTARROW="None" STARTINCLINATION="0;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1828532005" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_1265891064" SOURCE="ID_1828532005" STARTARROW="None" STARTINCLINATION="0;0;"/>
</node>
<node CREATED="1428062404879" ID="ID_1755381424" MODIFIED="1428062404881" TEXT="Duraci&#xf3;n&#xa;"/>
<node CREATED="1428062399850" ID="ID_790014475" MODIFIED="1428062413319" TEXT="Importes de licitaci&#xf3;n y adjudicaci&#xf3;n&#xa;"/>
<node CREATED="1428062413320" ID="ID_427141268" MODIFIED="1428062417711" TEXT="El procedimiento de adjudicaci&#xf3;n (abierto, restringido, negociado, ...)&#xa;"/>
<node CREATED="1428062417712" ID="ID_1376481421" MODIFIED="1428062432327" TEXT="Instrumentos utilizados para su publicidad (DOUE, BOE, perfil)&#xa;"/>
<node CREATED="1428062432328" ID="ID_398607486" MODIFIED="1428062432329" TEXT="N&#xfa;mero de licitadores&#xa;"/>
<node CREATED="1428062429282" ID="ID_1741102809" MODIFIED="1428062429283" TEXT="Identidad del adjudicatario&#xa;"/>
<node CREATED="1428062425698" ID="ID_1656483463" MODIFIED="1428062425699" TEXT="Modificaciones del contrato&#xa;"/>
<node CREATED="1428062421978" ID="ID_1174161192" MODIFIED="1428062421979" TEXT="Decisiones de desistimiento o renuncia del contrato"/>
</node>
<node CREATED="1428062515686" ID="ID_968582133" MODIFIED="1428062642852" TEXT="Sobre los contratos menores, indica que se podr&#xe1; hacer de forma trimestral">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428062478220" ID="ID_482398831" MODIFIED="1428062545099" TEXT="Se publicar&#xe1;n datos estad&#xed;sticos que incluyan el porcentaje de contratos, en volumen presupuestario, adjudicados a trav&#xe9;s de cada tipo de procedimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375866820931" ID="ID_1172580958" MODIFIED="1428133218294">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Procedimientos
    </p>
    <p>
      de adjudicaci&#243;n
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428070925869" ID="ID_17871947" MODIFIED="1428133224190">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ordinarios
    </p>
    <p>
      (No se negocian
    </p>
    <p>
      los t&#233;rminos)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375804304883" ID="ID_1763452665" MODIFIED="1428132477057">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Abierto: presentaci&#243;n de ofertas
    </p>
    <p>
      (El m&#225;s usado en contrataci&#243;n TIC)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1428071373079" ID="ID_1102141558" MODIFIED="1428071397484">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Podr&#225; presentar proposici&#243;n cualquier empresario, quedando excluida toda negociaci&#243;n
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1428071563236" ID="ID_27779214" MODIFIED="1428133255557">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Presentaci&#243;n de
    </p>
    <p>
      proposiciones
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1375805638706" ID="ID_1341787961" MODIFIED="1428132478123" TEXT="SARA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375805644033" ID="ID_1847749058" MODIFIED="1428132479328" STYLE="bubble" TEXT="52 d&#xed;as desde el env&#xed;o al DOUE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378745195840" ID="ID_952989048" MODIFIED="1428133276205">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Podr&#225; reducirse 5 d&#237;as por acceso
    </p>
    <p>
      con mmee a la documentaci&#243;n
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375805697368" ID="ID_650698294" MODIFIED="1428132479329" STYLE="bubble" TEXT="36 d&#xed;as si hay anuncio previo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378745087575" ID="ID_1916186917" MODIFIED="1383648920709" TEXT="22 d&#xed;as en casos excepcionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1378745213867" ID="ID_707300261" MODIFIED="1428071710817" TEXT="Todos estos plazos podr&#xe1;n reducirse 7 d&#xed;as cuando los anuncios se env&#xed;en por mmee">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375805721464" ID="ID_483990668" MODIFIED="1428132478123" TEXT="Resto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375805732706" ID="ID_1863095765" MODIFIED="1378808752384" STYLE="bubble" TEXT="15 d&#xed;as desde la publicaci&#xf3;n del anuncio en BOE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375805777033" ID="ID_403008882" MODIFIED="1428132477908" TEXT="Apertura de proposiciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1375805789586" ID="ID_203493405" MODIFIED="1378745387696" TEXT="1 mes desde fin de plazo de presentaci&#xf3;n de ofertas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375805814567" ID="ID_174908674" MODIFIED="1428132477909" TEXT="Adjudicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1375805819538" ID="ID_645815720" MODIFIED="1428073130641" TEXT="15 d&#xed;as si &#xfa;nico criterio es el precio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375805830448" ID="ID_158451399" MODIFIED="1428132478124" TEXT="2 meses con otros factores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428071919955" ID="ID_1605214244" MODIFIED="1428071928815" TEXT="Salvo se indique otra cosa en PCAP"/>
</node>
<node CREATED="1428071932348" ID="ID_1365678613" MODIFIED="1428072012723" TEXT="+15 d&#xed;as en caso de oferta desproporcionada (audiencia al licitador)"/>
</node>
</node>
<node CREATED="1375804311612" ID="ID_52971640" MODIFIED="1428132477063">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Restringido
    </p>
    <p>
      (Uso muy residual)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1428072094831" ID="ID_412919071" MODIFIED="1428132477910">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Podr&#225;n presentar proposici&#243;n&#160;&#250;nicamente los empresarios que lo SOLICITEN
    </p>
    <p>
      y sean SELECCIONADOS atendiendo a su SOLVENCIA,
    </p>
    <p>
      quedando excluida toda negociaci&#243;n
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375805859583" ID="ID_233026517" MODIFIED="1428133281220" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Empresarios invitados &gt;= 5 (se indicar&#225; el
    </p>
    <p>
      m&#237;nimo y opcionalmente el m&#225;ximo)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375806022812" ID="ID_1042558659" MODIFIED="1428132477912" TEXT="Recepci&#xf3;n de solicitudes de participaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1375805925203" ID="ID_1545185108" MODIFIED="1428132478125" TEXT="SARA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375805933699" ID="ID_1727433756" MODIFIED="1428132479332" STYLE="bubble" TEXT="37 d&#xed;as desde env&#xed;o de anuncio al DOUE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375805939570" ID="ID_1828077325" MODIFIED="1428133287396">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Podr&#225; reducirse 7 d&#237;as por
    </p>
    <p>
      env&#237;o de anuncios por mmee
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375805950373" ID="ID_1902941304" MODIFIED="1428132478125" TEXT="Resto ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375805956093" ID="ID_1017112882" MODIFIED="1378810309876" STYLE="bubble" TEXT="10 d&#xed;as desde la publicaci&#xf3;n del anuncio en BOE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375805979748" ID="ID_213189939" MODIFIED="1428132477912" TEXT="Se invitar&#xe1; a presentar oferta al menos al m&#xed;nimo fijado previamente (&gt;=5)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1428072424624" ID="ID_1708168318" MODIFIED="1428072433923" TEXT="Si no llegan al m&#xed;nimo, puede continuar el procedimiento"/>
</node>
<node CREATED="1375806039887" ID="ID_621052912" MODIFIED="1428132477914" TEXT="Presentaci&#xf3;n de Ofertas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1375806047929" ID="ID_1311743042" MODIFIED="1428132478127" TEXT="SARA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375806051558" ID="ID_1065471603" MODIFIED="1428132479332" STYLE="bubble" TEXT="40 d&#xed;as desde el env&#xed;o de la invitaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378812420847" ID="ID_224982686" MODIFIED="1428133293100" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se reduce en 5 d&#237;as si hay
    </p>
    <p>
      acceso electr&#243;nico a los Pliegos
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375805697368" ID="ID_613552817" MODIFIED="1428132479334" STYLE="bubble" TEXT="36 d&#xed;as si hay anuncio previo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1378745087575" ID="ID_655144961" MODIFIED="1428072880305" TEXT="22 d&#xed;as en casos excepcionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1378808770098" ID="ID_324226028" MODIFIED="1428132478128" TEXT="Resto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428072628253" ID="ID_1397248454" MODIFIED="1428073069361" STYLE="bubble" TEXT="15 d&#xed;as desde el env&#xed;o de la invitaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375805814567" ID="ID_669559943" MODIFIED="1428132477915" TEXT="Adjudicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
<node CREATED="1375805819538" ID="ID_953960593" MODIFIED="1428073136307" TEXT="15 d&#xed;as si &#xfa;nico criterio es el precio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375805830448" ID="ID_487110521" MODIFIED="1428132478128" TEXT="2 meses con otros factores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428071919955" ID="ID_1943268472" MODIFIED="1428071928815" TEXT="Salvo se indique otra cosa en PCAP"/>
</node>
<node CREATED="1428071932348" ID="ID_859039104" MODIFIED="1428072012723" TEXT="+15 d&#xed;as en caso de oferta desproporcionada (audiencia al licitador)"/>
</node>
</node>
</node>
<node CREATED="1428070938357" ID="ID_552739633" MODIFIED="1428133245389">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No ordinarios
    </p>
    <p>
      (Se negocian las
    </p>
    <p>
      condiciones del contrato)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375804315536" ID="ID_328532916" MODIFIED="1428132477067" TEXT="Negociado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1428073502889" ID="ID_1972121113" MODIFIED="1428073518514" TEXT="Recaer&#xe1; en el licitador JUSTIFICADAMENTE ELEGIDO tras efectuar consultas con diversos candidatos y NEGOCIAR las condiciones del contrato con uno o varios de ellos"/>
<node CREATED="1375866797008" ID="ID_1279634675" MODIFIED="1428074348841" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se invitar&#225;n al menos tres ofertas, cuando sea POSIBLE
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375806094856" ID="ID_56733378" MODIFIED="1428132477919" TEXT="Casos de aplicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428073601387" ID="ID_418587552" MODIFIED="1428074014553">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      En procedimientos abiertos, restringidos o de di&#225;logo competitivo realizados
    </p>
    <p>
      donde las ofertas son NO ADECUADAS o IRREGULARES&#160;
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1428073757063" ID="ID_1671685165" MODIFIED="1428073767319" TEXT="Cuando no se pueda determinar el precio global "/>
<node CREATED="1428073790122" ID="ID_1477714477" MODIFIED="1428074167497" TEXT="El contrato s&#xf3;lo pueda encomendarse a un empresario determinado (especificidad t&#xe9;cnica)"/>
<node CREATED="1428073811520" ID="ID_1003925717" MODIFIED="1428073813945" TEXT="Contrato declarado secreto o reservado"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1428073930547" ID="ID_1388850187" MODIFIED="1428073981309" STYLE="bubble" TEXT="Contratos de suministros y servicios con v.e. &lt; 100.000&#x20ac;">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375806125469" ID="ID_1236346104" MODIFIED="1428132477920" TEXT="Tipos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428074069640" ID="ID_1422663286" MODIFIED="1428132478131" TEXT="Sin publicidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428074101239" ID="ID_982736129" MODIFIED="1428074471936" STYLE="bubble" TEXT="Suministros y servicios con v.e. &lt;= 60.000&#x20ac;"/>
<node CREATED="1428074631197" ID="ID_302284258" MODIFIED="1428074738975" TEXT="Por haberse presentado ofertas inaceptables o irregulares en otros procedimientos, &#xa;siempre que se incluyan a todos los licitadores en la negociaci&#xf3;n"/>
</node>
<node CREATED="1428074066809" ID="ID_278125548" MODIFIED="1428132478132" TEXT="Con publicidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428074425142" ID="ID_1270121394" MODIFIED="1428074537808" STYLE="bubble" TEXT="Suministros y servicios 60.000&#x20ac; &lt; v.e. &lt; 100.000&#x20ac;"/>
<node CREATED="1428074503202" ID="ID_809027478" MODIFIED="1428074525138" TEXT="Todos los dem&#xe1;s casos donde se emplea el Negociado por razones distintas de la cuant&#xed;a">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
</node>
</node>
<node CREATED="1375804378591" ID="ID_417318894" MODIFIED="1428132477072" TEXT="Di&#xe1;logo competitivo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1428074817231" ID="ID_1185912867" MODIFIED="1428132477921">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      El &#243;rgano de contrataci&#243;n considere que
    </p>
    <p>
      no resultan adecuados abierto ni restringido
    </p>
  </body>
</html></richcontent>
<node CREATED="1428074896027" ID="ID_1154757701" MODIFIED="1428074912428" TEXT="Contratos particularmente complejos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375806365664" ID="ID_109389795" MODIFIED="1428075093735" TEXT="El &#xf3;rgano de contrataci&#xf3;n no se ve capacitado para configurar el PPTP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428075134471" ID="ID_1543316250" MODIFIED="1428132477922" STYLE="bubble" TEXT="Seleccionar al menos 3 candidatos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428075168295" ID="ID_783889988" MODIFIED="1428075174220" TEXT="Previa solicitud de participaci&#xf3;n de los mismos"/>
</node>
<node CREATED="1428075182792" ID="ID_1542991679" MODIFIED="1428132477922" TEXT="Di&#xe1;logo con los candidatos para ENCONTRAR SOLUCIONES">
<node CREATED="1428075217761" ID="ID_1540867798" MODIFIED="1428075396440" TEXT="Se pueden establecer compensaciones para los participantes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428075237273" ID="ID_1286019651" MODIFIED="1428132477923" TEXT="No se incluye PPT en el Expediente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1428075326412" ID="ID_1620670237" MODIFIED="1428075341990" TEXT="Sustituido por documento descriptivo de la soluci&#xf3;n"/>
</node>
<node CREATED="1428075034524" ID="ID_922830894" MODIFIED="1428075389423" STYLE="bubble" TEXT="Para adjudicaci&#xf3;n, valorar necesariamente varios criterios, NO S&#xd3;LO EL PRECIO">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1375806398664" ID="ID_1034311330" MODIFIED="1428074875003" STYLE="bubble" TEXT="Contratos de colaboraci&#xf3;n entre el sector p&#xfa;blico y el privado: en todo caso">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ser&#225; siempre el procedimiento de adjudicaci&#243;n de este tipo de contratos.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1375804338663" ID="ID_1570503672" MODIFIED="1428132476536" TEXT="Menores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375804341496" ID="ID_877980613" MODIFIED="1383648886513" TEXT="&lt; 18.000 &#x20ac; (servicios) o 50.000 &#x20ac; , obras">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375804366462" ID="ID_1117804106" MODIFIED="1377532196335" TEXT="Adjudicaci&#xf3;n directa, si habilitaci&#xf3;n profesional">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375866887270" ID="ID_917964732" MODIFIED="1377532268987" TEXT="M&#xe1;xima duraci&#xf3;n de un a&#xf1;o">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375806477000" ID="ID_376567487" MODIFIED="1377532524309" TEXT="Concursos de proyectos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428133389894" ID="ID_1319220237" MODIFIED="1428133396606" TEXT="Procedimientos encaminados a la obtenci&#xf3;n de planos o proyectos">
<node CREATED="1428133417439" ID="ID_726553127" MODIFIED="1428133432573" TEXT="Entre otros, en el &#xe1;mbito del procesamiento de datos"/>
</node>
<node CREATED="1428133446781" ID="ID_1181113928" MODIFIED="1428133447741" TEXT="Tipos">
<node CREATED="1428133447877" ID="ID_1561328791" MODIFIED="1428133512381" TEXT="Organizados en el marco de un proceso de adjudicaci&#xf3;n de contrato de SERVICIOS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428133471485" ID="ID_357973415" MODIFIED="1428133512381" TEXT="Concursos de proyectos con primas de participaci&#xf3;n o pagos a los participantes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428133554355" ID="ID_163739969" MODIFIED="1428133560173" TEXT="No requiere un m&#xed;nimo de participantes"/>
<node CREATED="1428133586607" ID="ID_1737901378" MODIFIED="1428133753471">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Jurado INDEPENDIENTE formado por personas f&#237;sicas
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428133626049" ID="ID_381586189" MODIFIED="1428133639183" TEXT="Elaborar&#xe1; un informe con la clasificaci&#xf3;n de los distintos proyectos"/>
<node CREATED="1428133706322" ID="ID_1189953475" MODIFIED="1428133747263">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 88" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font size="3" face="SansSerif">Cuando se exija cualificacio&#769;n profesional para participar en un concurso de proyectos, al menos un tercio de los miembros del jurado debera&#769; poseer dicha cualificacio&#769;n </font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
