<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1375800625583" ID="ID_1995838940" MODIFIED="1479119221168">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      RD 3/2011, de 14nov
    </p>
    <p>
      &#160;-- TRLCSP --
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ojo: los d&#237;as se computar&#225;n como HABILES
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375801634377" ID="ID_158090269" MODIFIED="1428129019678" POSITION="right" TEXT="Racionalidad y consistencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375864768670" ID="ID_1647710049" MODIFIED="1427985822328" TEXT="Necesidad e idoneidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427997953948" ID="ID_427365070" MODIFIED="1427998004110" TEXT="S&#xf3;lo contratos necesarios para cumplimiento y realizaci&#xf3;n de los fines institucionales de la organizaci&#xf3;n"/>
<node CREATED="1427998046315" ID="ID_927377878" MODIFIED="1427998286325" TEXT="TODO EXPEDIENTE, en la doc preparatoria, previa al inicio del procedimiento de adj. del contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1375864791144" ID="ID_1815264200" MODIFIED="1427998154401" TEXT="Necesidades a satisfacer">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
</node>
<node CREATED="1375864801617" ID="ID_894794797" MODIFIED="1428130456296">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Declaraci&#243;n de idoneidad del
    </p>
    <p>
      contrato para satisfacerlas
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
</node>
</node>
</node>
<node CREATED="1375801646555" ID="ID_870734047" MODIFIED="1428070649134" TEXT="Plazos de duraci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428000884771" ID="ID_578366440" MODIFIED="1428000887134" TEXT="En base a">
<node CREATED="1428000887365" ID="ID_1436094814" MODIFIED="1428000916862" TEXT="Naturaleza de las prestaciones&#xa;"/>
<node CREATED="1428000916863" ID="ID_445735689" MODIFIED="1428000920982" TEXT="Caracter&#xed;sticas de su financiaci&#xf3;n&#xa;"/>
<node CREATED="1428000920983" ID="ID_1140587918" MODIFIED="1428000920984" TEXT="Necesidad de someter peri&#xf3;dicamente a concurrencia su realizaci&#xf3;n"/>
</node>
<node CREATED="1427998524773" ID="ID_466307496" MODIFIED="1427998757615" TEXT="Podr&#xe1; prever pr&#xf3;rrogas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1427998643506" ID="ID_366462039" MODIFIED="1427998750347" TEXT="Siempre que en la licitaci&#xf3;n se haya considerado la duraci&#xf3;n m&#xe1;xima, inc. todas las pr&#xf3;rrogas"/>
<node CREATED="1427998592697" ID="ID_451350059" MODIFIED="1427998643265" TEXT="El contrato mantendr&#xe1; sus caracter&#xed;sticas inalterables durante el per&#xed;odo de duraci&#xf3;n de las mismas"/>
<node CREATED="1427998789113" ID="ID_1357059762" MODIFIED="1427998811319" TEXT="Se acordar&#xe1; por el &#xf3;rgano de contrataci&#xf3;n y ser&#xe1; obligatoria para la empresa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428000932853" ID="ID_1216646775" MODIFIED="1428000936150" TEXT="Plazos">
<node CREATED="1383646090078" ID="ID_151910971" MODIFIED="1428000226090" TEXT="Servicios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1428000227539" ID="ID_397956056" MODIFIED="1428000249894" STYLE="bubble" TEXT="4 a&#xf1;os + 2 pr&#xf3;rroga">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383646214265" ID="ID_1684568643" MODIFIED="1383646232673" TEXT="Ampliaci&#xf3;n del plazo por Acuerdo de Ministros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1385025487127" ID="ID_1007172176" MODIFIED="1428000372955" TEXT="Servicios de la Sociedad de la Informaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1428000373636" ID="ID_681687742" MODIFIED="1428000386269" STYLE="bubble" TEXT="Contrataci&#xf3;n centralizada, 4 a&#xf1;os">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1383646482848" ID="ID_1857184862" MODIFIED="1428000276618" TEXT="Acuerdos Marco">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428000276860" ID="ID_333248012" MODIFIED="1428000282732" TEXT="4 a&#xf1;os">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1383646431278" ID="ID_998332927" MODIFIED="1428000987935" TEXT="Colaboraci&#xf3;n p&#xfa;blico-privado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428000988936" ID="ID_1447748641" MODIFIED="1428000993936" TEXT="20 a&#xf1;os">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383646452656" ID="ID_1143376289" MODIFIED="1383646479502" TEXT="40 a&#xf1;os si se asimilan a los de concesi&#xf3;n de obra p&#xfa;blica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1383646496895" ID="ID_989293506" MODIFIED="1428001000462" TEXT="Sistema Din&#xe1;mico de Adquisici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428001000463" ID="ID_548530127" MODIFIED="1428001003681" TEXT="4 a&#xf1;os">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375801657941" ID="ID_326303015" MODIFIED="1428001014150" TEXT="Contratos menores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428001014151" ID="ID_1251627112" MODIFIED="1428001024361" TEXT="&lt; 1 a&#xf1;o y sin pr&#xf3;rroga">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1383646234771" ID="ID_765146249" MODIFIED="1428000304669" TEXT="Obra publica">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1428000290783" ID="ID_1156892890" MODIFIED="1428000307933" TEXT="40 a&#xf1;os">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383646250052" ID="ID_1850327672" MODIFIED="1428000307933" TEXT="75 a&#xf1;os en caso de obras hidraulicas">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383646312655" ID="ID_1868469169" MODIFIED="1428000307933" TEXT="Ampliable en un 15 %">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1383646305268" ID="ID_285756436" MODIFIED="1428000304669" TEXT="Gesti&#xf3;n de servicio p&#xfa;blico">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1383646358290" ID="ID_724930996" MODIFIED="1383646365902" TEXT="50 a&#xf1;os con ejecuci&#xf3;n de obras"/>
<node CREATED="1383646366316" ID="ID_512545029" MODIFIED="1383646379512" TEXT="60 a&#xf1;os con ejecuci&#xf3;n de obras en mercados mayoristas"/>
<node CREATED="1383646379941" ID="ID_1025660325" MODIFIED="1383646405539" TEXT="25 a&#xf1;os con explotaci&#xf3;n de Servicios P&#xfa;blicos distintos de los Sanitarios"/>
<node CREATED="1383646405859" ID="ID_1665983731" MODIFIED="1383646424172" TEXT="10 a&#xf1;os, explotaci&#xf3;n de Servicios Sanitarios"/>
</node>
</node>
<node CREATED="1427999316137" ID="ID_946849307" MODIFIED="1427999457887">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Instrucci&#243;n de la Subsecretar&#237;a de Hacienda
    </p>
    <p>
      y Administraciones P&#250;blicas (5 jul 2013)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1427999329834" ID="ID_916134142" MODIFIED="1428130476781" TEXT="NO iniciar contratos, por m&#xe1;s de 1a&#xf1;o, prorrogable m&#xe1;x. otro a&#xf1;o, &#xa;de bienes y servicios TIC al margen del SISTEMA CENTRALIZADO">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
</node>
<node CREATED="1427999433127" ID="ID_14738118" MODIFIED="1428130486365" TEXT="Duraci&#xf3;n prevista mayor --&gt; Recabar informe favorable de la &#xa;Subsecretar&#xed;a de Hacienda y Administraciones P&#xfa;blicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
</node>
</node>
</node>
<node CREATED="1375864825415" ID="ID_530569305" MODIFIED="1427999755896" TEXT="Libertad de pactos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375864841117" ID="ID_636263802" MODIFIED="1427999782175" TEXT="Cualquier pacto,cla&#xfa;sula o condici&#xf3;n NO CONTRARIO al inter&#xe9;s p&#xfa;blico"/>
</node>
<node CREATED="1375801696855" ID="ID_1335833720" MODIFIED="1428129044788">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contenido m&#237;nimo del contrato
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375801702512" ID="ID_457817377" MODIFIED="1428001168905" TEXT="Identificaci&#xf3;n de las partes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1375801710552" ID="ID_812446416" MODIFIED="1428001192366" TEXT="Acreditaci&#xf3;n de la capacidad de los firmantes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1375801717845" ID="ID_808136054" MODIFIED="1428001172617" TEXT="Definici&#xf3;n del objeto del contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1375801727900" ID="ID_1877690429" MODIFIED="1428001200442" TEXT="Referencia a la legislaci&#xf3;n aplicable">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1375801738921" ID="ID_234692169" MODIFIED="1428001203066" TEXT="Enumeraci&#xf3;n de los documentos que la integran">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1375801753889" ID="ID_329539521" MODIFIED="1428001176313" TEXT="El precio cierto, o modo de determinarlo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-6"/>
</node>
<node CREATED="1375801766876" ID="ID_1870675536" MODIFIED="1428001226563" TEXT="La duraci&#xf3;n del contrato y pr&#xf3;rrogas en su caso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-7"/>
</node>
<node CREATED="1375801778288" ID="ID_408393728" MODIFIED="1428001253268" TEXT="Condiciones de recepci&#xf3;n, entrega o admisi&#xf3;n de las prestaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-8"/>
</node>
<node CREATED="1375801785939" ID="ID_56692824" MODIFIED="1428001256315" TEXT="Condiciones de pago">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-9"/>
</node>
<node CREATED="1375801791236" ID="ID_1890693025" MODIFIED="1428001263371" TEXT="Supuestos de resoluci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<icon BUILTIN="full-0"/>
</node>
<node CREATED="1375801800494" ID="ID_1887276301" MODIFIED="1428001296350" TEXT="Cr&#xe9;dito presupuestario al que se abonar&#xe1; el precio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1375801806165" ID="ID_1646594480" MODIFIED="1428001298987" TEXT="Extensi&#xf3;n del deber de confidencialidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<icon BUILTIN="full-2"/>
</node>
</node>
<node CREATED="1382945684004" ID="ID_1029563224" MODIFIED="1428130419885" TEXT="IMPORTANTE: La documentacion podr&#xe1; sustituirse por una declaraci&#xf3;n responsable &#xa;(Ley 14/2013, de 27 sept, Ley de Emprendedores)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Introducido por la Ley de Emprendedores
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375801880647" ID="ID_77597435" MODIFIED="1428001813114" TEXT="Causas de nulidad de derecho administrativo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375801895446" ID="ID_157919294" MODIFIED="1428130605172">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Falta de capacidad de obrar o solvencia (econ&#243;mica/t&#233;cnica/profesional)
    </p>
    <p>
      o incurso en una prohibici&#243;n para contratar
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375801905124" ID="ID_1729211976" MODIFIED="1377529804926" TEXT="Carencia o suficiencia de cr&#xe9;dito">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382945589794" ID="ID_1656698242" MODIFIED="1428130717538">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Disposiciones o resoluciones que otorguen ventaja directa o indirecta
    </p>
    <p>
      a empresas que hayan contratado previamente con cualquier Adm&#243;n.
    </p>
    <p>
      <font size="2">(Ley 14/2013, de 27sept, Ley de Emprendedores)</font>
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Introducido por la Ley de Emprendedores
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375803326249" ID="ID_1720584025" MODIFIED="1428129046806" POSITION="right" TEXT="Precios ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375864969216" ID="ID_1472340166" MODIFIED="1428002557948" TEXT="No podr&#xe1; fraccionarse 1 contrato con finalidad de disminuir su cuant&#xed;a y eludir requisitos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375864917604" ID="ID_1873365294" MODIFIED="1428002633252" TEXT="La retribuci&#xf3;n del contratista consistir&#xe1; en un precio cierto, en euros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375803331672" ID="ID_859989472" MODIFIED="1377529842778" TEXT="IVA excluido, deber&#xe1; indicarse de forma desglosada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375865027397" ID="ID_1646165182" MODIFIED="1428129057183" STYLE="bubble" TEXT="Valor estimado = Importe total incluidas las pr&#xf3;rrogas, SIN IVA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428130187989" ID="ID_1186772084" MODIFIED="1479119277811" TEXT="Ley 2/2015 de desindexaci&#xf3;n de la econom&#xed;a espa&#xf1;ola">
<node CREATED="1479119482154" ID="ID_649787974" MODIFIED="1479119527363" TEXT="Los valores monetarios no podr&#xe1;n ser objeto de revisi&#xf3;n peri&#xf3;dica y &#xa;predeterminada en funci&#xf3;n de precios, &#xed;ndices de precios o f&#xf3;rmulas"/>
</node>
<node CREATED="1375803405529" ID="ID_1149094320" MODIFIED="1428003118476" STYLE="bubble" TEXT="No se podr&#xe1;n revisar">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375803414774" ID="ID_905169231" MODIFIED="1428003118477" TEXT="Arrendamiento financiero">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375803420707" ID="ID_389647935" MODIFIED="1428003118477" TEXT="Contratos menores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428002728650" ID="ID_1209444366" MODIFIED="1428130041639" TEXT="Podr&#xe1;n incluir cl&#xe1;usulas de variaci&#xf3;n de precios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1428002737604" ID="ID_928596164" MODIFIED="1428002770013" TEXT="Por objetivo de cumplimiento de plazos"/>
<node CREATED="1428002770673" ID="ID_465008606" MODIFIED="1428002775942" TEXT="Penalizaciones por incumplimientos"/>
</node>
<node CREATED="1428002810887" ID="ID_713645059" MODIFIED="1428129996872" TEXT="Pueden celebrarse contratos con precios provisionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1428002822972" ID="ID_1313226255" MODIFIED="1428129999601">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      S&#243;lo en procedimiento negociado o di&#225;logo competitivo
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428002843883" ID="ID_1128285622" MODIFIED="1428129974277" TEXT="Cuando la ejecuci&#xf3;n debe comenzar antes de poder determinar el precio"/>
</node>
</node>
<node CREATED="1428130090317" ID="ID_350354075" MODIFIED="1428130100711" TEXT="Se proh&#xed;be el pago aplazado del precio en los contratos de las Administraciones P&#xfa;blicas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="closed"/>
<node CREATED="1428130104110" ID="ID_166390000" MODIFIED="1428130126198" TEXT="Salvo arrendamiento financiero o con opci&#xf3;n a compra">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375803384976" ID="ID_1793042893" MODIFIED="1377529847236" TEXT="En el PCA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375865397652" ID="ID_462507453" MODIFIED="1428129113050" POSITION="right" TEXT="Aptitud de la empresa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428043802964" ID="ID_1758547476" MODIFIED="1428044023177" TEXT="Personas naturales o jur&#xed;dicas, espa&#xf1;olas o extranjeras">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1428062976676" ID="ID_315591766" MODIFIED="1428062978760" TEXT="Las empresas extranjeras deber&#xe1;n declarar el sometimiento a la jurisdicci&#xf3;n espa&#xf1;ola"/>
</node>
<node CREATED="1375865409580" ID="ID_1849980635" MODIFIED="1428044024792" TEXT="Plena capacidad de obrar">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1375865421865" ID="ID_813512434" MODIFIED="1428044205765" TEXT="Escritura o documento de constituci&#xf3;n inscrito en el Registro P&#xfa;blico que corresponda">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428062825799" ID="ID_1875110121" MODIFIED="1428062845640" TEXT="UTEs">
<node CREATED="1428062845791" ID="ID_1793916909" MODIFIED="1428062880895" TEXT="Pueden no estar formalizadas antes de la adjudicaci&#xf3;n del contrato"/>
<node CREATED="1428062881105" ID="ID_1324237054" MODIFIED="1428062902770" TEXT="Basta con Acreditaci&#xf3;n del compromiso de tal formalizaci&#xf3;n"/>
</node>
</node>
<node CREATED="1428043672943" ID="ID_1750924348" MODIFIED="1428044026200" TEXT="No incursa en una prohibicaci&#xf3;n para contratar">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1428044186634" ID="ID_1274457894" MODIFIED="1428044202153" TEXT="Mediante testimonio judicial o certificaci&#xf3;n administrativa"/>
</node>
<node CREATED="1375865444148" ID="ID_958107388" MODIFIED="1428063184016" TEXT="Habilitaci&#xf3;n empresarial o profesional que en su caso sea exigible">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1375865476651" ID="ID_582307510" MODIFIED="1428130422373" TEXT="Acreditada solvencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-5"/>
<node CREATED="1428044286551" ID="ID_44762250" MODIFIED="1428044581974" TEXT="Econ&#xf3;mica y Financiera">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428044305333" ID="ID_1854870421" MODIFIED="1428044326727" TEXT="Declaraciones de entidades financieras"/>
<node CREATED="1428044327384" ID="ID_1701762075" MODIFIED="1428044904916" TEXT="Cuentas anuales en Registro mercantil o libros de contabilidad (si no obligado a presentar cuentas)"/>
<node CREATED="1428044372182" ID="ID_316413946" MODIFIED="1428044417833" TEXT="Volumen de negocio de los 3 &#xfa;ltimos ejercicios en actividades relacionadas al objeto del contrato"/>
</node>
<node CREATED="1428044296549" ID="ID_871521011" MODIFIED="1428044581975" TEXT="T&#xe9;cnica o Profesional">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428044617644" ID="ID_766883072" MODIFIED="1428044630289" TEXT="Contratos de suministros">
<node CREATED="1428044642053" ID="ID_298683766" MODIFIED="1428063111800" TEXT="Suministros realizados en los 3 &#xfa;ltimos a&#xf1;os">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428044688280" ID="ID_1097648294" MODIFIED="1428063110016" TEXT="Personal t&#xe9;cnico disponible para la ejecuci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428044715889" ID="ID_1048191928" MODIFIED="1428063107208" TEXT="Instalaciones t&#xe9;cnicas, medidas para garantizar la calidad y medios de investigaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428044754826" ID="ID_1566142591" MODIFIED="1428044983750" TEXT="Caso de productos complejos, control efectuado por la entidad contratante"/>
<node CREATED="1428044780272" ID="ID_1931395524" MODIFIED="1428044792884" TEXT="Muestras, descripciones y fotograf&#xed;as de los productos a suministrar"/>
<node CREATED="1428044811654" ID="ID_228450528" MODIFIED="1428063116792" TEXT="Certificados oficiales de control de calidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428044621893" ID="ID_544872743" MODIFIED="1428044626366" TEXT="Contratos de servicios">
<node CREATED="1428044912274" ID="ID_460654417" MODIFIED="1428063019702" TEXT="Relaci&#xf3;n de principales servicios en los 3 &#xfa;ltimos a&#xf1;os">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428044688280" ID="ID_1357687385" MODIFIED="1428063022286" TEXT="Personal t&#xe9;cnico participante en el contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428044715889" ID="ID_1090321925" MODIFIED="1428063096791" TEXT="Instalaciones t&#xe9;cnicas, medidas para garantizar la calidad y medios de investigaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428044754826" ID="ID_1229389584" MODIFIED="1428045107649" TEXT="Caso de trabajos complejos, control efectuado por la entidad contratante"/>
<node CREATED="1428045121471" ID="ID_516961282" MODIFIED="1428063026838" TEXT="Titulaciones acad&#xe9;micas y profesionales de directivos y responsables de la ejecuci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428045142947" ID="ID_1816022879" MODIFIED="1428045178731" TEXT="Plantilla media anual"/>
<node CREATED="1428045178938" ID="ID_1307894568" MODIFIED="1428045206289" TEXT="Maquinaria, material y equip t&#xe9;cnico del que se dispondr&#xe1; para la ejecuci&#xf3;n"/>
<node CREATED="1428045206482" ID="ID_1981845755" MODIFIED="1428045229596" TEXT="Indicaci&#xf3;n de la parte del contrato que se pretenda subcontratar"/>
</node>
</node>
<node CREATED="1428043915158" ID="ID_629845918" MODIFIED="1428129100812" TEXT="Este requisito ser&#xe1; sustituido por la exigencia de Clasificaci&#xf3;n cuando corresponda">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
</node>
</node>
<node CREATED="1375803010941" ID="ID_814089964" MODIFIED="1428070297655" TEXT="Prohibiciones de contratar (v)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1381576610944" ID="ID_264960712" MODIFIED="1428003452144" TEXT="Condenadas mediante sentencia firme">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1381576628425" ID="ID_1460344587" MODIFIED="1381576633762" TEXT="asociaci&#xf3;n il&#xed;cita"/>
<node CREATED="1381576634174" ID="ID_147114579" MODIFIED="1428003388296" TEXT="corrupci&#xf3;n en transacciones econ&#xf3;micas internacionales"/>
<node CREATED="1381576648268" ID="ID_888105921" MODIFIED="1381576652857" TEXT="cohecho"/>
<node CREATED="1381576710559" ID="ID_1805114324" MODIFIED="1381576716084" TEXT="malversaci&#xf3;n"/>
<node CREATED="1381576653174" ID="ID_1166020016" MODIFIED="1381576659338" TEXT="tr&#xe1;fico de influencias"/>
<node CREATED="1381576661903" ID="ID_1630884995" MODIFIED="1381576665594" TEXT="delitos">
<node CREATED="1381576665594" ID="ID_286722817" MODIFIED="1381576670992" TEXT="Hacienda P&#xfa;blica"/>
<node CREATED="1381576671325" ID="ID_1213165474" MODIFIED="1381576672981" TEXT="SS"/>
<node CREATED="1381576673767" ID="ID_824096061" MODIFIED="1428003498411" TEXT="Derechos de los trabajadores"/>
<node CREATED="1381576684726" ID="ID_1168882356" MODIFIED="1428003503715" TEXT="Medio ambiente"/>
</node>
</node>
<node CREATED="1381576695201" ID="ID_159707449" MODIFIED="1428003627493" TEXT="Solicitado concurso voluntario, hallarse en Concurso,&#xa;declarada insolvente o sujeto a intervenci&#xf3;n judicial">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1381576737118" ID="ID_1142112510" MODIFIED="1385016942938" TEXT="Sancionadas con car&#xe1;cter grave">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1381576761103" ID="ID_1869447500" MODIFIED="1381576770808" TEXT="Disciplina de mercado"/>
<node CREATED="1381576771563" ID="ID_1909356118" MODIFIED="1428003636832" TEXT="Materia profesional"/>
<node CREATED="1381576781087" ID="ID_1888278249" MODIFIED="1428003639045" TEXT="Integraci&#xf3;n laboral"/>
<node CREATED="1381576786867" ID="ID_901279432" MODIFIED="1428003642516" TEXT="Igualdad de oportunidades"/>
</node>
<node CREATED="1381576799199" ID="ID_28255982" MODIFIED="1428003688341" TEXT="No estar al corriente de cumplimiento con Hacienda o SS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1381576810891" ID="ID_276131767" MODIFIED="1428003760061" TEXT="Falsedad en la Declaraci&#xf3;n responsable o en cualquier dato relativo a su capacidad y solvencia ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1381576826327" ID="ID_1080814344" MODIFIED="1428003949144" TEXT="Ley de incompatibilidad o cargos electivos (R&#xe9;gimen Electoral) para los administradores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428003830805" ID="ID_635341062" MODIFIED="1428003874862" TEXT="Cualquier PJ en cuyo capital participe personal de las AAPP"/>
</node>
<node CREATED="1381576845757" ID="ID_1381923887" MODIFIED="1383646765551" TEXT="Conflictos de Intereses de los Miembros del Gobierno">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1381576876809" ID="ID_1063394353" MODIFIED="1383646766596" TEXT="Declarados culpables de resoluci&#xf3;n de contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1381576894679" ID="ID_132102299" MODIFIED="1428004087930" TEXT="Retirado indebidamente una proposici&#xf3;n o candidatura en un procedim. de adjudicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1381576930707" ID="ID_654486395" MODIFIED="1383646770481" TEXT="Incumplido las condiciones de ejecuci&#xf3;n del contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428070117298" ID="ID_1052240363" MODIFIED="1428130902806">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="3">Declaraci&#243;n responsable de </font>
    </p>
    <p>
      <font size="3">cumplimiento de condiciones</font>
    </p>
    <p>
      <font size="2">(Ley 14/2013, 27sept, Ley de Emprendedores)</font>
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1428070198973" ID="ID_664285298" MODIFIED="1428130914100">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Puede sustituir a toda la documentaci&#243;n acreditativa
    </p>
    <p>
      de la aptitud de la empresa para licitar
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428070419193" ID="ID_792546968" MODIFIED="1428070587216" TEXT="En todo caso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1428070234005" ID="ID_1924580058" MODIFIED="1428070335779" STYLE="bubble" TEXT="Suministros o servicios v.e. &lt; 90.000&#x20ac;">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428070259150" ID="ID_158902404" MODIFIED="1428070285577" TEXT="Obras v.e. &lt; 1.000.000&#x20ac;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428070433866" ID="ID_1949855462" MODIFIED="1428070561801">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      El &#243;rgano contratante podr&#225; aceptar la DR en licitaciones de mayor importe
    </p>
  </body>
</html></richcontent>
<node CREATED="1428070545080" ID="ID_1998705069" MODIFIED="1428070607887" TEXT="Deber&#xe1; indicarse en el PCAP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428131028944" ID="ID_1343807151" MODIFIED="1428131097879">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      El &#243;rgano contratante podr&#225;&#160;requerir la documentaci&#243;n acreditativa
    </p>
    <p>
      en cualquier momento, incluso antes de la adjudicaci&#243;n
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</map>
