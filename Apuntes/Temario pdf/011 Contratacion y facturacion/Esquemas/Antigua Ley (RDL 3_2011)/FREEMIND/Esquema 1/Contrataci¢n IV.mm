<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1375800625583" ID="ID_1995838940" MODIFIED="1428132324878">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      RD 3/2011, de 14nov
    </p>
    <p>
      &#160;-- TRLCSP --
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ojo: los d&#237;as se computar&#225;n como HABILES
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428049065626" ID="ID_751715578" MODIFIED="1428132339686" POSITION="right" TEXT="Preparaci&#xf3;n del Expediente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1428049075410" ID="ID_1331233029" MODIFIED="1428049455367">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Inicio del expediente por
    </p>
    <p>
      el &#243;rgano de contrataci&#243;n
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1428049614184" ID="ID_544491941" MODIFIED="1428132395647" TEXT="La celebraci&#xf3;n de contratos por las AAPP requerir&#xe1; la &#xa;previa tramitaci&#xf3;n del correspondiente expediente"/>
<node CREATED="1428049218000" ID="ID_1513597515" MODIFIED="1428049360773" TEXT="Contenido del Expediente">
<node CREATED="1428049220294" ID="ID_1401680235" MODIFIED="1428049428487" TEXT="Motivaci&#xf3;n de su necesidad (ver necesidad e idoneidad)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428049270608" ID="ID_860218580" MODIFIED="1428049428491" TEXT="Pliegos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428049473239" ID="ID_567285126" MODIFIED="1428049660814">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Documento descriptivo en caso de Di&#225;logo Competitivo
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="closed"/>
</node>
</node>
<node CREATED="1428049272010" ID="ID_1648226745" MODIFIED="1428049428491" TEXT="Certificado de existencia de cr&#xe9;dito">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428049296290" ID="ID_1851938987" MODIFIED="1428049428491" TEXT="Fiscalizaci&#xf3;n previa, cuando lo prevea la legislaci&#xf3;n correspondiente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428049344873" ID="ID_1706549306" MODIFIED="1428049428487" TEXT="Justificaci&#xf3;n de la elecci&#xf3;n del procedimiento de contrataci&#xf3;n &#xa;y de los criterios para adjudicaci&#xf3;n del contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375804025788" ID="ID_277140127" MODIFIED="1428049445165" TEXT="Pliegos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1428048910990" ID="ID_1778634691" MODIFIED="1428048923027" TEXT="Describen">
<node CREATED="1428048916503" ID="ID_387949065" MODIFIED="1428048928872" TEXT="Necesidades a cubrir"/>
<node CREATED="1428048929127" ID="ID_1614739028" MODIFIED="1428048936522" TEXT="Caracter&#xed;sticas de los productos o prestaciones"/>
<node CREATED="1428048937513" ID="ID_636762450" MODIFIED="1428048940385" TEXT="Requisitos de las ofertas"/>
<node CREATED="1428048941103" ID="ID_107393241" MODIFIED="1428048962454" TEXT="Criterios de valoraci&#xf3;n en la selecci&#xf3;n del contratista"/>
</node>
<node CREATED="1375804030665" ID="ID_823292559" MODIFIED="1428132578674">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div class="page" title="Page 56">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="SansSerif" size="3">Pliegos de cla&#769;usulas </font>
          </p>
          <p>
            <font face="SansSerif" size="3">administrativas particulares </font>
          </p>
        </div>
      </div>
    </div>
    <p>
      <font size="3">(PCAP)</font>
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1428049883099" ID="ID_1557066471" MODIFIED="1428049886304" TEXT="Contenido">
<node CREATED="1428049886524" ID="ID_222017796" MODIFIED="1428051013086">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pactos y condiciones que establecen los
    </p>
    <p>
      DERECHOS y OBLIGACIONES de las PARTES del contrato
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428050069050" ID="ID_794178347" MODIFIED="1428050145143" TEXT="Se considera parte integrante del contenido de los contratos (los contratos se ajustar&#xe1;n a este contenido)"/>
<node CREATED="1428050145382" ID="ID_307246413" MODIFIED="1428050177592" TEXT="Instrucciones sobre buenas pr&#xe1;cticas para la gesti&#xf3;n &#xa;de las contrataciones de servicios y encomiendas de gesti&#xf3;n &#xa;a fin de evitar incurrir en supuestos de cesi&#xf3;n ilegal de trabajadores&#xa;(MINHAP)">
<node CREATED="1428050195703" ID="ID_1398973775" MODIFIED="1428050254922" TEXT="Necesaria cla&#xfa;sula exhaustiva sobre c&#xf3;mo debe ser &#xa;la relaci&#xf3;n entre AP y personal laboral de la empresa contratista"/>
</node>
</node>
<node CREATED="1428050269483" ID="ID_378534182" MODIFIED="1428051009798" TEXT="Aprobaci&#xf3;n por el &#xf3;rgano de contrataci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428050307634" ID="ID_1005147691" MODIFIED="1428050314987" TEXT="Momento de aprobaci&#xf3;n">
<node CREATED="1428050315266" ID="ID_1225850285" MODIFIED="1428050913188" TEXT="Previa o conjunta con la aprobaci&#xf3;n del gasto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428050331010" ID="ID_1029882320" MODIFIED="1428050913188" TEXT="Simpre antes de la licitaci&#xf3;n o, si no hay, antes de la adjudicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428050866016" ID="ID_329638976" MODIFIED="1428050909276" TEXT="Modelos de PCAP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428050901506" ID="ID_1168357039" MODIFIED="1428050902578" TEXT="Los &#xf3;rganos de contrataci&#xf3;n pueden aprobar modelos de PCAP para contratos de naturaleza an&#xe1;loga"/>
</node>
<node CREATED="1428050419101" ID="ID_670964323" MODIFIED="1428050925012">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pliegos de cla&#250;sulas
    </p>
    <p>
      administrativas generales
    </p>
    <p>
      (PCAG)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428050446941" ID="ID_545622038" MODIFIED="1428050454376" TEXT="Podr&#xe1;n tomarlas como modelo los PCAP"/>
<node CREATED="1428050467424" ID="ID_1802054628" MODIFIED="1428050722081">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#193;mbito de las compras TIC en la AGE
    </p>
  </body>
</html></richcontent>
<node CREATED="1428050579873" ID="ID_1296136023" MODIFIED="1428051457840" TEXT="Aprobadas por Consejo de Ministros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428050653229" ID="ID_417560623" MODIFIED="1428050932133" TEXT="A iniciativa de los Ministerios interesados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428050599017" ID="ID_1383922373" MODIFIED="1428050737384" TEXT="A propuesta de MINHAP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428050625866" ID="ID_1600247102" MODIFIED="1428050932133" TEXT="Aprobadas por Consejo de Estado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1428050960448" ID="ID_71579099" MODIFIED="1428050977614" TEXT="Introducir en los PCAP estipulaciones contrarias a las de los PCAG">
<node CREATED="1428050977773" ID="ID_1716205419" MODIFIED="1428052408304" TEXT="Informe de la Junta Consultiva de Contrataci&#xf3;n Administrativa.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="list"/>
</node>
</node>
</node>
<node CREATED="1375804034961" ID="ID_271418829" MODIFIED="1428132639690">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pliegos de Prescripciones
    </p>
    <p>
      T&#233;cnicas Particulares
    </p>
    <p>
      (PPTP)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1428051096030" ID="ID_1768673293" MODIFIED="1428051098647" TEXT="Contenido">
<node CREATED="1428051131735" ID="ID_1001947925" MODIFIED="1428051145740" TEXT="Descripci&#xf3;n de la prestaci&#xf3;n/caracter&#xed;sticas t&#xe9;cnicas/calidades de los bienes a contratar"/>
<node CREATED="1428051146809" ID="ID_668029858" MODIFIED="1428051154010" TEXT="Condiciones de ejecuci&#xf3;n del contrato"/>
</node>
<node CREATED="1428051331962" ID="ID_416955173" MODIFIED="1428132641222">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reglas para
    </p>
    <p>
      su establecimiento
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1375804043535" ID="ID_1478466933" MODIFIED="1428051610140" TEXT="Consideraci&#xf3;n de criterios de Accesibilidad Universal y dise&#xf1;o para todos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375867443629" ID="ID_1530857013" MODIFIED="1428051649008">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Caso de poder afectar al medio ambiente
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375865828627" ID="ID_1232493045" MODIFIED="1428051672821" TEXT="Criterios de sostenibilidad y protecci&#xf3;n ambiental"/>
</node>
<node CREATED="1428051715173" ID="ID_1137892613" MODIFIED="1428051853324" TEXT="Permitir acceso en CONDICIONES DE IGUALDAD de los licitadores &#xa;y no suponer obst&#xe1;culo a la COMPETENCIA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428052048902" ID="ID_582698270" MODIFIED="1428132641220">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Definici&#243;n del contenido
    </p>
    <p>
      (se pueden combinar)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1375865858509" ID="ID_1658644085" MODIFIED="1428132531087">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Haciendo referencia a especificaciones t&#233;cnicas
    </p>
    <p>
      (en el siguiente orden de prelaci&#243;n)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375865866584" ID="ID_1875870042" MODIFIED="1428132639689">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Normas nacionales que
    </p>
    <p>
      incorporen normas europeas
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<icon BUILTIN="bookmark"/>
</node>
<node CREATED="1375865887291" ID="ID_313369556" MODIFIED="1428052242339" TEXT="Normas internacionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1428052167923" ID="ID_841632688" MODIFIED="1428132670923">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Normas estrictamente nacionales
    </p>
    <p>
      (acompa&#241;adas de la menci&#243;n &#171;o equivalente&#187;)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
</node>
<node CREATED="1428052286270" ID="ID_1747357930" MODIFIED="1428052318670" TEXT="T&#xe9;rminos de rendimiento o exigencias funcionales, &#xa;mediante par&#xe1;metros lo suficientemente precisos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428052343285" ID="ID_638971562" MODIFIED="1428132641219">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contemplar caracter&#237;sticas medioambientales
    </p>
    <p>
      si puede afectar al medio ambiente
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
</node>
</node>
</node>
<node CREATED="1375865921058" ID="ID_616432684" MODIFIED="1428132520591">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No se podr&#225;n mencionar marcas, patentes,
    </p>
    <p>
      procedimientos o fabricaci&#243;n determinada
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428051989788" ID="ID_1822961408" MODIFIED="1428052004401">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      S&#243;lo si lo justifica el objeto del contrato,
    </p>
    <p>
      &#250;nicamente con car&#225;cter excepcional
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1428052501215" ID="ID_692367023" MODIFIED="1428132677953" TEXT="Si se menciona, deber&#xe1; ir acompa&#xf1;ada de la menci&#xf3;n &#xab;o equivalente&#xbb;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1428050269483" ID="ID_1113627904" MODIFIED="1428051009798" TEXT="Aprobaci&#xf3;n por el &#xf3;rgano de contrataci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428050307634" ID="ID_1688069432" MODIFIED="1428050314987" TEXT="Momento de aprobaci&#xf3;n">
<node CREATED="1428050315266" ID="ID_1573295481" MODIFIED="1428050913188" TEXT="Previa o conjunta con la aprobaci&#xf3;n del gasto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428050331010" ID="ID_1286586366" MODIFIED="1428050913188" TEXT="Simpre antes de la licitaci&#xf3;n o, si no hay, antes de la adjudicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428051360887" ID="ID_1423593327" MODIFIED="1428051384792">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pliegos de Prescripciones
    </p>
    <p>
      T&#233;cnicas Generales (PPTG)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428051386997" ID="ID_1841885097" MODIFIED="1428051393507" TEXT="Podr&#xe1;n tomarlas como modelo los PPT"/>
<node CREATED="1428051432238" ID="ID_1886098574" MODIFIED="1428051520268" TEXT="Aprobadas por Consejo de Ministros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428051461165" ID="ID_127199749" MODIFIED="1428051520268" TEXT="A propuesta del Ministro correspondiente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428051497632" ID="ID_553282312" MODIFIED="1428052415264" TEXT="Previo informe de la Junta Consultiva de Contrataci&#xf3;n Administrativa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="list"/>
</node>
</node>
</node>
</node>
<node CREATED="1428052563253" ID="ID_185824312" MODIFIED="1428052567860" TEXT="Controles">
<icon BUILTIN="full-3"/>
<node CREATED="1428054911538" ID="ID_216248910" MODIFIED="1428054968001" TEXT="Informe Jur&#xed;dico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428054942548" ID="ID_715884120" MODIFIED="1428054955337" TEXT="Informe favorable sobre el contenido del PCAP">
<node CREATED="1428054923367" ID="ID_1978055841" MODIFIED="1428054942334" TEXT="En la AGE se requiere del Servicio Jur&#xed;dico (Abogac&#xed;a del Estado)"/>
</node>
<node CREATED="1428055002255" ID="ID_1628221591" MODIFIED="1428055010189" TEXT="Excepto si se ajusta a PCAG"/>
</node>
<node CREATED="1428054915935" ID="ID_51618274" MODIFIED="1428055937363">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Informe T&#233;cnico de la DTIC
    </p>
    <p>
      (Memoria y PPT)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428055036944" ID="ID_444050991" MODIFIED="1428055039590" TEXT="Real Decreto 806/2014">
<node CREATED="1428055087865" ID="ID_1544675811" MODIFIED="1428055391666" TEXT="Toda adquisici&#xf3;n de bienes y servicios inform&#xe1;ticos en el &#xe1;mbito de la AGE &#xa;y sus OOPP, debe ser informada preceptivamente por la DTIC">
<node CREATED="1428055744250" ID="ID_1826883064" MODIFIED="1428055749888" TEXT="Independientemente del importe"/>
</node>
<node CREATED="1428055772869" ID="ID_1786842271" MODIFIED="1428055783836" TEXT="Todo &#xf3;rgano de contrataci&#xf3;n que desee adquirir bienes o servicios inform&#xe1;ticos deber&#xe1; recabar el informe favorable de la DTIC"/>
<node CREATED="1428055249781" ID="ID_1722087714" MODIFIED="1428055400068" TEXT="Informar&#xe1; la declaraci&#xf3;n de contrataci&#xf3;n centralizada &#xa;de los contratos de suministros, obras y servicios TIC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428055289678" ID="ID_1127487170" MODIFIED="1428055318419">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      A la Direcci&#243;n General de Racionalizaci&#243;n y&#160;Centralizaci&#243;n de la Contrataci&#243;n
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1428055350232" ID="ID_328044110" MODIFIED="1428132639690">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Realizar&#225; informe t&#233;cnico de la
    </p>
    <p>
      memoria y PPT de la contrataci&#243;n TIC
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1428055432820" ID="ID_1567187312" MODIFIED="1428055450587" TEXT="Suministro de equipos y programas para tto. de info."/>
<node CREATED="1428055450794" ID="ID_1693495130" MODIFIED="1428055453819" TEXT="Contratos de servicios"/>
<node CREATED="1428055453986" ID="ID_1260352726" MODIFIED="1428055463576" TEXT="Procedimientos especiales de adopci&#xf3;n de tipo"/>
<node CREATED="1428055463789" ID="ID_623775351" MODIFIED="1428055474816" TEXT="Convenios de colaboraci&#xf3;n y encomiendas de gesti&#xf3;n"/>
<node CREATED="1428055608960" ID="ID_372376827" MODIFIED="1428055644592" TEXT="Excepciones">
<icon BUILTIN="closed"/>
<node CREATED="1428055612816" ID="ID_540702840" MODIFIED="1428055650826" TEXT="Defensa, seguridad, contratos secretos o reservados">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428055526714" ID="ID_1081034871" MODIFIED="1428055602119" STYLE="bubble" TEXT="10 d&#xed;as h&#xe1;biles desde el registro del expediente"/>
</node>
</node>
</node>
<node CREATED="1428055819603" ID="ID_436059499" MODIFIED="1428055822134" TEXT="Fiscalizaci&#xf3;n Previa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428055822798" ID="ID_852116025" MODIFIED="1428055897039" TEXT="Recabados los informes anteriores, el &#xf3;rgano de contrataci&#xf3;n enviar&#xe1; &#xa;el expediente a su Intervenci&#xf3;n Delegada para su fiscalizaci&#xf3;n"/>
</node>
</node>
</node>
<node CREATED="1428055972552" ID="ID_1950366958" MODIFIED="1428132345801" POSITION="right" TEXT="Aprobaci&#xf3;n del Expediente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1428055989562" ID="ID_536852759" MODIFIED="1428056082741" TEXT="Completado el expediente y revisado por la Intervenci&#xf3;n Delegada: &#xa;Aprobaci&#xf3;n mediante resoluci&#xf3;n motivada por el &#xf3;rgano de contrataci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428056130795" ID="ID_491124163" MODIFIED="1428132348177" POSITION="right" TEXT="Apertura del procedimiento de adjudicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1375803789868" ID="ID_1163290267" MODIFIED="1428132352830" TEXT="Tramitaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375803794043" ID="ID_1867582256" MODIFIED="1428057115669" TEXT="Ordinaria">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1428056152308" ID="ID_1115165628" MODIFIED="1428056166699" TEXT="Incliye los pasos anteriores, caso general"/>
</node>
<node CREATED="1375803801038" ID="ID_639671468" MODIFIED="1428057117517" TEXT="Abreviada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1375865956883" ID="ID_1537281429" MODIFIED="1428057152742" TEXT="Urgencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1428056206892" ID="ID_591482979" MODIFIED="1428056551759" TEXT="Necesidad inaplazable o cuya adjudicaci&#xf3;n se deba acelerar por inter&#xe9;s p&#xfa;blico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
</node>
<node CREATED="1375803846405" ID="ID_115520204" MODIFIED="1428056340101" STYLE="bubble" TEXT="Declaraci&#xf3;n motivada de urgencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428056243367" ID="ID_1188677310" MODIFIED="1428056340101" TEXT="La incluir&#xe1; el &#xf3;rgano de contrataci&#xf3;n en el expediente"/>
</node>
<node CREATED="1375803869951" ID="ID_1450214442" MODIFIED="1428056409211" STYLE="bubble" TEXT="Plazos de licitaci&#xf3;n, adjudicaci&#xf3;n y formalizaci&#xf3;n se reducen a la mitad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428056369828" ID="ID_1611025211" MODIFIED="1428056384531" TEXT="Mismo procedimiento que tramitaci&#xf3;n ordinaria"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1375803921720" ID="ID_759891570" MODIFIED="1428056498212" STYLE="bubble" TEXT="15 d&#xed;as para el inicio de la ejecuci&#xf3;n tras la formalizaci&#xf3;n">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428070744049" ID="ID_1122065346" MODIFIED="1428070766837" TEXT="En otro caso podr&#xe1; ser RESUELTO"/>
</node>
<node CREATED="1428056418586" ID="ID_64488795" MODIFIED="1428056455780" TEXT="Sigue estando presente la necesidad de constituci&#xf3;n previa de la garant&#xed;a"/>
</node>
<node CREATED="1375803938732" ID="ID_1635099146" MODIFIED="1428057154806" TEXT="Emergencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1428056542448" ID="ID_1500421118" MODIFIED="1428056648265" TEXT="Acontecimientos catastr&#xf3;ficos, situaciones de grave peligro o necesidades de la defensa nacional">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
</node>
<node CREATED="1428056757955" ID="ID_1706987241" MODIFIED="1428056773530" TEXT="No est&#xe1; sujeta a los requisitos formales del TRLCSP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428056715074" ID="ID_743140294" MODIFIED="1428056808735" TEXT="No requiere existencia de Expediente ni pliegos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375866057901" ID="ID_1019007155" MODIFIED="1428056787340" TEXT="No es necesario cr&#xe9;dito suficiente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428056827060" ID="ID_42271342" MODIFIED="1428056841919" STYLE="bubble" TEXT="Tan solo requiere Acuerdo de inicio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1375865988582" ID="ID_882126889" MODIFIED="1428057049932" STYLE="bubble" TEXT="1 mes para el inicio de la prestaci&#xf3;n desde el acuerdo de inicio">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428057006385" ID="ID_1244885292" MODIFIED="1428070773643" TEXT="En otro caso se ir&#xe1; a tramitaci&#xf3;n ORDINARIA"/>
</node>
<node CREATED="1375865976937" ID="ID_107467197" MODIFIED="1428057080037" TEXT="Unico caso donde es posible CONTRATACI&#xd3;N VERBAL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375803818130" ID="ID_382356947" MODIFIED="1428057247744" TEXT="Contratos menores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1428057248190" ID="ID_431573642" MODIFIED="1428057258410" TEXT="&#xda;nicamente requieren">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375866024769" ID="ID_1691074819" MODIFIED="1377530253045" TEXT="Aprobaci&#xf3;n del gasto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375866030749" ID="ID_84848905" MODIFIED="1428057229508" TEXT="Incorporaci&#xf3;n de la factura al expediente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375866153381" ID="ID_653062397" MODIFIED="1428057234206" TEXT="Adjudicaci&#xf3;n directa de proveedor">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</map>
