<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1375800625583" ID="ID_1995838940" MODIFIED="1428134420178">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      RD 3/2011, de 14nov
    </p>
    <p>
      &#160;-- TRLCSP --
    </p>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ojo: los d&#237;as se computar&#225;n como HABILES
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375805232618" ID="ID_77076722" MODIFIED="1428134438178" POSITION="right" TEXT="Subasta electr&#xf3;nica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375805256657" ID="ID_717599795" MODIFIED="1428076672211" TEXT="Puede emplearse en procedimientos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428076658181" ID="ID_1457875843" MODIFIED="1428077047606" TEXT="Abiertos y retringidos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428076654820" ID="ID_2467028" MODIFIED="1428077047606" TEXT="Negociados, tras abiertos o restringidos donde las ofertas no sean aceptables">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428076986313" ID="ID_718145165" MODIFIED="1428077034804" TEXT="T&#xc9;CNICA que se puede usar cuando se prevea que las ofertas de los licitadores pueden ser mejoradas">
<node CREATED="1428077036621" ID="ID_1919642272" MODIFIED="1428077043333" TEXT="No es un procedimiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="stop-sign"/>
</node>
</node>
<node CREATED="1375805282873" ID="ID_68768488" MODIFIED="1428077761949" TEXT="Basado EXCLUSIVAMENTE en medios electr&#xf3;nicos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1428076934088" ID="ID_1198352375" MODIFIED="1428076939713" TEXT="Evaluaci&#xf3;n autom&#xe1;tica de las ofertas"/>
</node>
<node CREATED="1375805243717" ID="ID_1673455376" MODIFIED="1428077308175" TEXT="Proceso iterativo para la adjudicaci&#xf3;n de un contrato">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428076882409" ID="ID_870510820" MODIFIED="1428076909609" TEXT="Subasta en la que se van presentando mejoras &#xa;en los precios u otros elementos de las ofertas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428077238951" ID="ID_332260127" MODIFIED="1428077267363" TEXT="Premisas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428077240370" ID="ID_308296238" MODIFIED="1428077266931" TEXT="Que las especificaciones del contrato se puedan establecer de forma precisa&#xa;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1428077258842" ID="ID_1360012771" MODIFIED="1428077266483" TEXT="Que las prestaciones a contratar no sean de car&#xe1;cter intelectual">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
</node>
<node CREATED="1428077309481" ID="ID_1081276044" MODIFIED="1428077311162" TEXT="Proceso">
<node CREATED="1428077590448" ID="ID_805893495" MODIFIED="1428077643834" TEXT="Motivaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1428077594950" ID="ID_785374558" MODIFIED="1428077636089" TEXT="Se estima que las ofertas pueden ser mejoradas y se cumplen las premisas"/>
</node>
<node CREATED="1428077311377" ID="ID_1229740448" MODIFIED="1428077717532" TEXT="Primera evaluaci&#xf3;n de ofertas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1428077320463" ID="ID_776755523" MODIFIED="1428077646106">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Invitaci&#243;n por mmee a licitadores cuyas ofertas se hayan aceptado
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1375805349626" ID="ID_1662059065" MODIFIED="1381594115322" TEXT="Entre la fecha de la invitaci&#xf3;n y el inicio de la subasta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375805365236" ID="ID_1774036703" MODIFIED="1376903679293" STYLE="bubble" TEXT="2 d&#xed;as h&#xe1;biles">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1428077333938" ID="ID_1876049710" MODIFIED="1428077647178" TEXT="Inicio de la subasta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
<node CREATED="1428077480639" ID="ID_220971243" MODIFIED="1428077725940" TEXT="Se informar&#xe1;, de forma continua e instant&#xe1;nea, sobre la respectiva clasificaci&#xf3;n&#xa;">
<icon BUILTIN="info"/>
</node>
<node CREATED="1428077501687" ID="ID_366746883" MODIFIED="1428077727452" TEXT="Se podr&#xe1; informar sobre los precios o valores de los restantes licitadores, &#xa;sin poder divulgar su identidad">
<icon BUILTIN="info"/>
</node>
</node>
<node CREATED="1428077368677" ID="ID_526780863" MODIFIED="1428077648218" TEXT="Fin de la subasta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-5"/>
<node CREATED="1428077427820" ID="ID_1072487846" MODIFIED="1428077428901" TEXT="Por">
<node CREATED="1428077388528" ID="ID_483803558" MODIFIED="1428077437285">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 26" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font size="3" face="SansSerif">Sen&#771;alamiento de fecha y hora de cierre</font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1428077437291" ID="ID_659348039" MODIFIED="1428077451338">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 26" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font size="3" face="SansSerif">Falta de presentacio&#769;n de nuevos precios durante un determinado plazo</font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1428077451343" ID="ID_198097567" MODIFIED="1428077451346">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 26" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font size="3" face="SansSerif">Finalizacio&#769;n del calendario de fases</font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1428081353927" ID="ID_1559262295" MODIFIED="1428134450439" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Organizaci&#243;n
    </p>
    <p>
      administrativa
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375807463884" ID="ID_611877231" MODIFIED="1428134456871">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#211;rganos
    </p>
    <p>
      de contrataci&#243;n
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375807471117" ID="ID_1403431922" MODIFIED="1428081402443" TEXT="Ministros y Secretarios de Estado en la AGE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1375807488720" ID="ID_200156504" MODIFIED="1428081403034" TEXT="Presidentes y directores de los OOAA, Agencias, EPES">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1375807516730" ID="ID_1493799222" MODIFIED="1428081403370" TEXT="Directores Generales de la SS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1375808376806" ID="ID_1224569980" MODIFIED="1428134518251" TEXT="En los organismos podr&#xe1;n constituirse Juntas de Contrataci&#xf3;n, &#xa;que actuar&#xe1;n en los contratos de">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1382341902790" ID="ID_383608168" MODIFIED="1428081424796" TEXT="Suministros de bienes consumibles o de f&#xe1;cil deterioro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382341924398" ID="ID_1982361558" MODIFIED="1428081424797" TEXT="Contratos de servicios, salvo los que sean de contrataci&#xf3;n centralizada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382341931038" ID="ID_1998345078" MODIFIED="1428134536837">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contratos de suministros y servicios, cuando
    </p>
    <p>
      afecten a m&#225;s de un &#243;rgano de contrataci&#243;n
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375807539124" ID="ID_1096404636" MODIFIED="1428081404178" TEXT="Junta de Contrataci&#xf3;n Centralizada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1428080285534" ID="ID_1541616471" MODIFIED="1428081376123" TEXT="Celebraci&#xf3;n del procedimiento especial de adopci&#xf3;n de tipo">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428080303678" ID="ID_1221152245" MODIFIED="1428081376124" TEXT="Adjudicaci&#xf3;n de contratos de contrataci&#xf3;n centralizada que no se deriven de un Acuerdo Marco">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428080427960" ID="ID_862192771" MODIFIED="1428081376124" TEXT="Excepci&#xf3;n">
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="closed"/>
<node CREATED="1428080431093" ID="ID_1365146590" MODIFIED="1428081393493" TEXT="No basados en AM que s&#xf3;lo se puedan encomendar a un empresario (negociado sin publicidad)">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1428080482139" ID="ID_1057536967" MODIFIED="1428134552956">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Director/a General de Racionalizaci&#243;n
    </p>
    <p>
      y Centralizaci&#243;n de la Contrataci&#243;n
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1428080492147" ID="ID_96479490" MODIFIED="1428081393490" TEXT="Contratos derivados de un Acuerdo Marco">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428080431093" ID="ID_1492018911" MODIFIED="1428081393491" TEXT="No basados en AM que s&#xf3;lo se puedan encomendar a un empresario (negociado sin publicidad)">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375807591049" ID="ID_1096473249" MODIFIED="1428134541085">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Autorizaciones previas
    </p>
    <p>
      necesarias para contratar
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="info"/>
<node CREATED="1375807600042" ID="ID_1409000498" MODIFIED="1428080721730" TEXT="Por Consejo de Ministros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375807607241" ID="ID_444466742" MODIFIED="1384080782568" TEXT=" Importe &gt;= 12 mill. &#x20ac;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375807641242" ID="ID_1003425262" MODIFIED="1428080703537" TEXT="Arrendamiento de cualquier tipo superior a 4 a&#xf1;os">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384080720639" ID="ID_1491667607" MODIFIED="1384080761348" TEXT="Contratos plurianuales que modifiquen el % o n&#xfa;mero de anualidades previstos art. 47">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1384080812331" ID="ID_905031198" MODIFIED="1384080856583" TEXT="Ministro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384080816294" ID="ID_1673721637" MODIFIED="1384080867366" TEXT="Pueden fijar la cuant&#xed;a a partir de la cual se precisa su autorizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384080831890" ID="ID_1517420568" MODIFIED="1384080869171" TEXT="Si no se fija, &gt;900.000 &#x20ac;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1428080794786" ID="ID_1641122410" MODIFIED="1428080880510" TEXT="Ojo con las 3 juntas">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1428080801258" ID="ID_1884270243" MODIFIED="1428080810692" TEXT="Juntas de Contrataci&#xf3;n (de los ministerios)"/>
<node CREATED="1428080810933" ID="ID_1996808327" MODIFIED="1428080866925" TEXT="Junta de Contrataci&#xf3;n Centralizada (del sistema de contrataci&#xf3;n centralizada estatal)"/>
<node CREATED="1428080835443" ID="ID_188140820" MODIFIED="1428080843788" TEXT="Junta Consultiva de Contrataci&#xf3;n Administrativa (&#xf3;rgano consultivo)"/>
</node>
</node>
<node CREATED="1375870699554" ID="ID_1161635418" MODIFIED="1428134464447">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#211;rganos
    </p>
    <p>
      de asistencia
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375870715659" ID="ID_999105516" MODIFIED="1383651611025" TEXT="Mesas de contrataci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375870725931" ID="ID_476140075" MODIFIED="1378891095297" TEXT="Mesa de contrataci&#xf3;n de di&#xe1;logo competitivo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428080979423" ID="ID_1365592499" MODIFIED="1428080987122" TEXT="Contar&#xe1; con expertos en la materia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375870742523" ID="ID_1283792139" MODIFIED="1428080975896" TEXT="Asiste tambi&#xe9;n en contratos de colaboraci&#xf3;n p&#xfa;blico-privada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375870763716" ID="ID_1246489504" MODIFIED="1428081182960">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mesa de contrataci&#243;n del sistema
    </p>
    <p>
      estatal de contrataci&#243;n centralizada
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428081028834" ID="ID_1506569607" MODIFIED="1428081037657" TEXT="&#xd3;rgano competente para la valoraci&#xf3;n de las ofertas"/>
<node CREATED="1428081184701" ID="ID_1405431737" MODIFIED="1428081311936" TEXT="Excepto">
<icon BUILTIN="closed"/>
<node CREATED="1428081187596" ID="ID_1115315111" MODIFIED="1428081268306">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      El &#243;rgano de contrataci&#243;n es una Junta de Contrataci&#243;n
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1428081195898" ID="ID_471110436" MODIFIED="1428081202989" TEXT="Abiertos y restringidos"/>
<node CREATED="1428081244693" ID="ID_930284580" MODIFIED="1428081248414" TEXT="Ciertos casos de los negociados que requieren publicidad"/>
</node>
<node CREATED="1428081292462" ID="ID_140542322" MODIFIED="1428081293942" TEXT="Opcional">
<node CREATED="1428081293943" ID="ID_556718767" MODIFIED="1428081300331" TEXT="Negociados que no requieren publicidad"/>
</node>
</node>
</node>
<node CREATED="1375870794952" ID="ID_221989228" MODIFIED="1383651612086" TEXT="Jurados de concursos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375870804472" ID="ID_986507319" MODIFIED="1383651614051" TEXT="Concursos de proyectos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1375870822090" ID="ID_1934691520" MODIFIED="1428134471630">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#211;rganos
    </p>
    <p>
      consultivos
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375870833746" ID="ID_59229400" MODIFIED="1378821295853" TEXT="Junta Consultiva de Contrataci&#xf3;n Administrativa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428081440890" ID="ID_204183131" MODIFIED="1428081444915" TEXT="Adscrita al MINHAP"/>
<node CREATED="1428081460169" ID="ID_1075165978" MODIFIED="1428081461379" TEXT="Aborda aspectos administrativos, t&#xe9;cnicos y econ&#xf3;micos, en materia de contrataci&#xf3;n"/>
</node>
<node CREATED="1375870846687" ID="ID_956904199" MODIFIED="1428080908550" TEXT="&#xd3;rganos consultivos de las CCAA"/>
</node>
<node CREATED="1428081476658" ID="ID_744304973" MODIFIED="1428081479685" TEXT="DTIC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428081492579" ID="ID_1084886201" MODIFIED="1428081506834" TEXT="Impulsa la racionalizaci&#xf3;n de la contrataci&#xf3;n TIC en la AGE y sus OOPP"/>
<node CREATED="1428081516051" ID="ID_1480961974" MODIFIED="1428081522115" TEXT="Rango de SUBSECRETAR&#xcd;A"/>
<node CREATED="1428081577375" ID="ID_1874657891" MODIFIED="1428081579960" TEXT="Divisi&#xf3;n de Inversiones TIC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428055249781" ID="ID_820570122" MODIFIED="1428055400068" TEXT="Informar&#xe1; la declaraci&#xf3;n de contrataci&#xf3;n centralizada &#xa;de los contratos de suministros, obras y servicios TIC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428055289678" ID="ID_513757369" MODIFIED="1428055318419">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      A la Direcci&#243;n General de Racionalizaci&#243;n y&#160;Centralizaci&#243;n de la Contrataci&#243;n
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1428081641384" ID="ID_1091964178" MODIFIED="1428083705300">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Informe preceptivo de la memoria y PPT
    </p>
    <p>
      de la contrataci&#243;n TIC
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428081723746" ID="ID_1186674350" MODIFIED="1428083646255" TEXT="Suministro de equipos y programas para tto. de info."/>
<node CREATED="1428083646447" ID="ID_1162009970" MODIFIED="1428083661015" TEXT="Contratos de servicios"/>
<node CREATED="1428083661203" ID="ID_1343228240" MODIFIED="1428083667493" TEXT="Procedimientos especiales de adopci&#xf3;n de tipo"/>
<node CREATED="1428083667695" ID="ID_1274067592" MODIFIED="1428083692845" TEXT="Convenios de colaboraci&#xf3;n y encomiendas de gesti&#xf3;n"/>
</node>
<node CREATED="1428081901844" ID="ID_1421992876" MODIFIED="1428081979713" TEXT="Informe anual detallado y desagregado de imputaci&#xf3;n de costes TIC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1375867901437" ID="ID_592331342" MODIFIED="1428134481838" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Registros
    </p>
    <p>
      Oficiales
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375867918919" ID="ID_888382700" MODIFIED="1428061387619" TEXT="ROs de Licitadores y Empresas clasificadas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428061290435" ID="ID_1919168517" MODIFIED="1428064264275">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contendr&#225; informaci&#243;n P&#218;BLICA&#160;sobre
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428061239764" ID="ID_852253365" MODIFIED="1428064262155">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Datos relativos a la capacidad de los empresarios calificados por la JCCAE
    </p>
    <p>
      (Capacidad de obrar, solvencia, prohibiciones para contratar, etc.)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428061371482" ID="ID_139254547" MODIFIED="1428061532013" TEXT="Inscripci&#xf3;n voluntaria de empresarios no calificados"/>
</node>
<node CREATED="1375867938214" ID="ID_384297136" MODIFIED="1428087246247">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      ROLEC del Estado
    </p>
    <p>
      (ROLECE)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428061426350" ID="ID_929212303" MODIFIED="1428061430466" TEXT="Dependiente del MINHAP">
<node CREATED="1428064323178" ID="ID_213066921" MODIFIED="1428064335324" TEXT="Gestionada por la Junta Consultiva de Contrataci&#xf3;n Administrativa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428064290057" ID="ID_60329734" MODIFIED="1428064297258" TEXT="Acredita frente a TODOS los &#xf3;rganos del SP"/>
</node>
<node CREATED="1375867941576" ID="ID_1512681353" MODIFIED="1428061438263" TEXT="ROLEC de las CCAA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428064344738" ID="ID_1626323886" MODIFIED="1428064358695" TEXT="Mismo efecto en sus &#xe1;mbitos territoriales"/>
</node>
</node>
<node CREATED="1428060984269" ID="ID_1691568442" MODIFIED="1428061002005" TEXT="Registro de Contratos del Sector P&#xfa;blico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="folder"/>
<node CREATED="1428061642691" ID="ID_773906467" MODIFIED="1428061657294" TEXT="Sistema oficial central de informaci&#xf3;n sobre la contrataci&#xf3;n p&#xfa;blica en Espa&#xf1;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428061564353" ID="ID_212595757" MODIFIED="1428061659023" TEXT="Dependiente del MIHAP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1428061576180" ID="ID_1806755924" MODIFIED="1428061713127">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Los &#243;rganos de contrataci&#243;n comunicar&#225;n al RCSP
    </p>
    <p>
      los DATOS B&#193;SICOS&#160;de los contratos ADJUDICADOS
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428061728179" ID="ID_1923076170" MODIFIED="1428061737470" TEXT="Obligatoriamente por medios telem&#xe1;ticos"/>
<node CREATED="1428061902220" ID="ID_1276313968" MODIFIED="1428134487430">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      AAPP con registros propios --&gt; Podr&#225;n sustituir
    </p>
    <p>
      comunicaci&#243;n por la interconexi&#243;n de los registros
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1428061853816" ID="ID_696805551" MODIFIED="1428061875842" TEXT="Se prev&#xe9; la habilitaci&#xf3;n del acceso a los datos telem&#xe1;ticamente &#xa;a todos los &#xf3;rganos de la Administraci&#xf3;n"/>
<node CREATED="1428061659999" ID="ID_1323283536" MODIFIED="1428062054713" TEXT="Identificaci&#xf3;n de contratos">
<node CREATED="1428062054909" ID="ID_1440581162" MODIFIED="1428062103991" TEXT="C&#xf3;digos &#xfa;nicos asignados por la AP comunicante"/>
<node CREATED="1428062104270" ID="ID_957892355" MODIFIED="1428062111634" TEXT="En base a reglas que estableza MINHAP"/>
</node>
</node>
</node>
<node CREATED="1375868430939" ID="ID_1663374055" MODIFIED="1428134438233" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      RD. 589/2005. Contrataci&#243;n NO centralizada
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428091660635" ID="ID_949531970" MODIFIED="1428091679547" TEXT="Los servicios y suministros TIC no centralizados, se contratan seg&#xfa;n reglas del r&#xe9;gimen general"/>
<node CREATED="1428091711741" ID="ID_173053206" MODIFIED="1428091724158" TEXT="Los pliegos SIEMPRE los elabora el organismo interesado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375868669690" ID="ID_779588791" MODIFIED="1377535216662" TEXT="Servicios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375868482562" ID="ID_1268407117" MODIFIED="1428091840463" STYLE="bubble" TEXT="&lt;= 1.000.000 &#x20ac; (IVA EXCLUIDO)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375868658622" ID="ID_1447639387" MODIFIED="1428091840463" TEXT="Los pliegos los informa la DTIC (CMAD hasta 01/01/2015)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375868658622" ID="ID_1431366084" MODIFIED="1428091840463" TEXT="Los pliegos los informa la DTIC (CMAD hasta 01/01/2015)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375868565521" ID="ID_102049633" MODIFIED="1428091840463" STYLE="bubble" TEXT="&gt; 1.000.000 &#x20ac; (IVA EXCLUIDO)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375868658622" ID="ID_51672771" MODIFIED="1428091840463" TEXT="Los pliegos los informa la DTIC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375868776769" ID="ID_1612158711" MODIFIED="1428091883998" STYLE="bubble" TEXT="Duraci&#xf3;n m&#xe1;xima de 4 a&#xf1;os + 2 pr&#xf3;rroga">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375868509963" ID="ID_778035382" MODIFIED="1383656252181" TEXT="Organo de contrataci&#xf3;n: Ministerio u organismo del interesado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375868472679" ID="ID_637753701" MODIFIED="1377535214238" TEXT="Suministros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ff9999" CREATED="1375868482562" ID="ID_649852517" MODIFIED="1428091840461" STYLE="bubble" TEXT="&lt;= 1.000.000 &#x20ac; (IVA EXCLUIDO)">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375868509963" ID="ID_379420051" MODIFIED="1428091840462" TEXT="Organo de contrataci&#xf3;n: Ministerio u organismo del interesado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375868658622" ID="ID_298130121" MODIFIED="1428091840462" TEXT="Los pliegos los informa la DTIC (CMAD hasta 01/01/2015)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node BACKGROUND_COLOR="#ff9999" CREATED="1375868565521" ID="ID_468043749" MODIFIED="1428091840462" STYLE="bubble" TEXT="&gt; 1.000.000 &#x20ac; (IVA EXCLUIDO)">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375868595530" ID="ID_69226191" MODIFIED="1428091895625" TEXT="Organo de contrataci&#xf3;n: JCC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
</node>
<node CREATED="1380457289079" ID="ID_808054126" MODIFIED="1428091840462" TEXT="Los pliegos los informa la DTIC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1428095955455" ID="ID_1207318220" MODIFIED="1428134438239" POSITION="right" TEXT="Condiciones para el uso de mmee en la contrataci&#xf3;n">
<node CREATED="1428095964991" ID="ID_644694526" MODIFIED="1428096015966" TEXT="Todos los actos y manifestaciones de voluntad">
<node CREATED="1428096017363" ID="ID_357369613" MODIFIED="1428096120876" TEXT="Autenticados mediante firma electr&#xf3;nica avanzada reconocida">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="edit"/>
</node>
</node>
<node CREATED="1428096041673" ID="ID_688040106" MODIFIED="1428096105117" TEXT="Presentaci&#xf3;n de ofertas en 2 fases">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428096045578" ID="ID_1475334405" MODIFIED="1428096113054" TEXT="Presentaci&#xf3;n de la eFirma de la oferta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1428096072056" ID="ID_766496743" MODIFIED="1428096113054" TEXT="Presentaci&#xf3;n de la oferta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1428096077850" ID="ID_1477947128" MODIFIED="1428096100523" TEXT="plazo m&#xe1;ximo de 24h (si no, se entiende retirada)"/>
</node>
</node>
<node CREATED="1375898252807" ID="ID_681446723" MODIFIED="1428095984012" TEXT="Notificaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375898257220" ID="ID_1848384100" MODIFIED="1377711562641" TEXT="Fecha y hora de emisi&#xf3;n o recepci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375898267503" ID="ID_921886885" MODIFIED="1377711565755" TEXT="Integridad de su contenido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375898277539" ID="ID_1924707258" MODIFIED="1377711569074" TEXT="Remitente y destinatario de las mismas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1428096141027" ID="ID_1625879343" MODIFIED="1428096185700" TEXT="Copias electr&#xf3;nicas de documentos del expediente de contrataci&#xf3;n">
<node CREATED="1428096179663" ID="ID_566875896" MODIFIED="1428134493075" TEXT="Copia compulsada si incluyen eFirma reconocida &#xa;del &#xf3;rgano administrativo habilitado para su recepci&#xf3;n"/>
</node>
</node>
<node CREATED="1428095884061" ID="ID_327524739" MODIFIED="1428134438245" POSITION="right" TEXT="CONECTA-CENTRALIZACI&#xd3;N">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428096232321" ID="ID_1672485031" MODIFIED="1428096237479" TEXT="Plataforma electr&#xf3;nica del sistema estatal de contrataci&#xf3;n centralizada"/>
<node CREATED="1428096237918" ID="ID_1930235558" MODIFIED="1428096242847" TEXT="Gestionada por DGRCC"/>
<node CREATED="1428096255188" ID="ID_218874487" MODIFIED="1428096256863" TEXT="Servicios">
<node CREATED="1428096257078" ID="ID_608765802" MODIFIED="1428096258151" TEXT="Revisi&#xf3;n de los productos y servicios incluidos en los acuerdos marco"/>
<node CREATED="1428096259502" ID="ID_1118108525" MODIFIED="1428096263651" TEXT="Peticiones de contrataci&#xf3;n">
<node CREATED="1428096273815" ID="ID_644633502" MODIFIED="1428096279589" TEXT="Por medios TIC y con eFirma"/>
</node>
</node>
<node CREATED="1428096560389" ID="ID_1453111932" MODIFIED="1428096562110" TEXT="Orden EHA/1049/2008">
<node CREATED="1428096602128" ID="ID_16804165" MODIFIED="1428096695071" STYLE="bubble" TEXT="Solicitudes de contratos basados en AM o en el marco del SD de contrataci&#xf3;n &#xa;se tramitar&#xe1;n OBLIGATORIAMENTE a trav&#xe9;s de CONTECTA-CENTRALIZACI&#xd3;N">
<node BACKGROUND_COLOR="#ff9999" CREATED="1428096562349" ID="ID_1920335468" MODIFIED="1428096703024" STYLE="bubble" TEXT="Desde 01/01/2014">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1375950749858" ID="ID_109148885" MODIFIED="1428096214712" POSITION="right" TEXT="SSD-APP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375950755060" ID="ID_1192362883" MODIFIED="1384080201036" TEXT="Ayuda a la toma de decisiones entre alternativas en las AAPP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375950775845" ID="ID_1196660812" MODIFIED="1384080204502" TEXT="Sustituye a las gu&#xed;as SILICE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</map>
