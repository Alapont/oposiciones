<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1426681384580" ID="ID_1990084009" MODIFIED="1446050849645">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T41 LCSP I
    </p>
    <p style="text-align: center">
      RD 3/2011
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426677703698" ID="ID_401864265" MODIFIED="1446051572828" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>1. Actuaciones</b>
    </p>
    <p>
      <b>preparatorias</b>
    </p>
    <p>
      (exped de contrat)
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426677735648" ID="ID_1271860865" MODIFIED="1446052421287" TEXT="1.1 An&#xe1;lisis de necesidades">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426677795619" ID="ID_103602751" MODIFIED="1446052230161">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Memoria&#8594; Planif de Capac (qu&#233; se necesita), Decisiones hw-sw (c&#243;mo) y Rentab Eco
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1426678040450" ID="ID_1806975896" MODIFIED="1446050995106">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1.2&#160;&#160;Especs t&#233;cnico-funcionales
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426678067808" ID="ID_213214557" MODIFIED="1446051089017" TEXT="PPT">
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1426678080468" ID="ID_455088086" MODIFIED="1446051036587" TEXT="1.3 Orden de inicio del exped y certif de existencia de cr&#xe9;dito">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1426678231566" ID="ID_356830133" MODIFIED="1446051251593">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1.4 Redacci&#243;n Pliegos
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426678351697" ID="ID_587845422" MODIFIED="1446051413433">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cl&#225;sulas Adm (PCAP): </i>pactos, dchos y obligs
    </p>
  </body>
</html></richcontent>
<node CREATED="1426678363338" ID="ID_144184585" MODIFIED="1446051226388">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Objeto (lotes), presu licitaci&#243;n, plazo, pr&#243;rrogs, adjudic, docs, garant&#237;as, pagos (&lt;50% en bienes)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1446051169899" ID="ID_846752352" MODIFIED="1446051310606">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Criterios:</i>&#160;ordenados de + a - ponderaci&#243;n (asign por <u>DMD</u>)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1426678410203" ID="ID_205608768" MODIFIED="1446051467634">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Informe previo del Serv Jur&#237;dico&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1426678426066" ID="ID_129641739" MODIFIED="1446051425363">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Prescripcs T&#233;cn (PPTP):</i>&#160;calidades
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1426678479134" ID="ID_204761322" MODIFIED="1446051695095">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Reqs</u>, entorno, funcionals, cl&#225;usulas (pe MABER), valoraci&#243;n criterios (+detalle), conform con normas&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426678846914" ID="ID_1759798677" MODIFIED="1446051349029">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>PITER: </i>app modelo de ref&#8594; m&#243;ds: presenta, alcance, gu&#237;a-uso
    </p>
  </body>
</html></richcontent>
<node CREATED="1426678898707" ID="ID_761931320" MODIFIED="1446051456420">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Informe previo de la <u>DTIC</u>
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
<node CREATED="1426678931605" ID="ID_611639300" MODIFIED="1446052447530">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>1.5 Informe de fiscalizaci&#243;n:</i>&#160;&#160;Intervenci&#243;n delegada
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426678961547" ID="ID_911676480" MODIFIED="1446051525351">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>1.6 Aprobaci&#243;n exped y gasto:</i>&#160;&#243;rg de contrataci&#243;n
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1426679051786" ID="ID_688882987" MODIFIED="1446051644117" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      2. Proced de
    </p>
    <p>
      adjudicaci&#243;n
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426609529526" ID="ID_1711107365" MODIFIED="1446052431218" TEXT="2.1 Petici&#xf3;n de ofertas">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426610341443" ID="ID_1226262694" MODIFIED="1446051712758" TEXT="Publicidad">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426610918966" ID="ID_601271377" MODIFIED="1446051830776">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Anuncio previo:</i>&#160;&#160;(opc, previsi&#243;n 12 meses) en DOUE o perfil contratante, TIC &#8805; <u>750k</u>&#8364;
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426610988700" ID="ID_1835453193" MODIFIED="1446051745615" TEXT="Reduce plazo de presentaci&#xf3;n">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1426611029557" ID="ID_1967119048" MODIFIED="1446051825968">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Anuncio licitaci&#243;n:&#160; </i>(convocatoria) en perfil contratante, BOE y DOUE (si SARA)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426611055129" ID="ID_1331498837" MODIFIED="1446051817168">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Proceds <u>negoc</u>&#160;VE&lt;<u>60k</u>&#8364;
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="stop-sign"/>
</node>
</node>
</node>
<node CREATED="1426679716198" ID="ID_1405937375" MODIFIED="1446051916929">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Proceds
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1426609534271" ID="ID_1409191231" MODIFIED="1446051869896">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Adjudicaci&#243;n directa:</i>&#160;contratos menores &lt;18k&#8364;
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426609569680" ID="ID_1494010916" MODIFIED="1446051881001">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Abierto: </i>cualquier interesado
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426611323679" ID="ID_1199002257" MODIFIED="1446052062074">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      SARA: 52 d&#237;as desde anuncio (36 si anuncio previo) / NO SARA: 15 d&#237;as
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="calendar"/>
</node>
</node>
<node CREATED="1426609576062" ID="ID_1935273425" MODIFIED="1446051912892">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Restringido:</i>&#160;selecci&#243;n previa solicitud (m&#237;n 5)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426611342623" ID="ID_86702119" MODIFIED="1446052071177">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      SARA: 37 d&#237;as para solicitud, 40 ofertas (36 si ap) / NO SARA: 10 y 15 d&#237;as
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="calendar"/>
</node>
</node>
<node CREATED="1426609593583" ID="ID_1002033017" MODIFIED="1446051941815">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Negociado:</i>&#160;selecci&#243;n (m&#237;n 3)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426609623596" ID="ID_23123289" MODIFIED="1446051993495">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Bienes o servs con VE&lt;<u>100k</u>&#8364;, CC, urgencia, no se presentan, &#250;nico proveedor, complems
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1426611424065" ID="ID_649192705" MODIFIED="1446053172621">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      SARA: 37 d&#237;as para ofertas / NO SARA: 10 d&#237;as
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="calendar"/>
</node>
</node>
<node CREATED="1426609675743" ID="ID_1827364458" MODIFIED="1446052101775">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Di&#225;logo compet:</i>&#160;selecci&#243;n previa solicitud (m&#237;n 5)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1446052101779" ID="ID_530011850" MODIFIED="1446052101782">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#211;rg contrat&#160; <u>no capacitado</u>&#160;para def necesidads
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426611424065" ID="ID_649111069" MODIFIED="1446052117606">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      SARA: 37 d&#237;as para solicitud / NO SARA: 10 d&#237;as
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="calendar"/>
</node>
</node>
<node CREATED="1426609748997" ID="ID_653830916" MODIFIED="1446052152850">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Subasta-e:</i>&#160;opc en abierto, restringido o negoc para mejorar ofertas o nuevos valores a criterios
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1446052153562" ID="ID_1877766846" MODIFIED="1446052162957" TEXT="Indicado en anuncio licitaci&#xf3;n">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1426610435974" ID="ID_1744915217" MODIFIED="1446052560364">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Garant&#237;a Provisional:</i>&#160;(opc) para poder licitar y mantener proposiciones&#8594; &#8804; <u>3%</u>&#160;del presup de licit
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1426679568641" ID="ID_1876932212" MODIFIED="1446052253969" TEXT="2.2 Recepci&#xf3;n de ofertas">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426679624156" ID="ID_508214657" MODIFIED="1446052287268">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Mesa de Contrataci&#243;n:</i>&#160;apertura de plicas (cumplim reqs) y resol admitidas a CMAD (q asigna Ponencia)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426700307061" ID="ID_43490144" MODIFIED="1426700311159" TEXT="5 d&#xed;as">
<icon BUILTIN="calendar"/>
</node>
</node>
</node>
<node CREATED="1426679987809" ID="ID_723882929" MODIFIED="1446052304044" TEXT="2.3 Evaluaci&#xf3;n de ofertas">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426680013792" ID="ID_1754098304" MODIFIED="1446052378304">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ponencia T&#233;cn:</i>&#160;<u>DMD</u>&#160;e informe de calific a CMAD&#8594; Propuesta a MC&#8594; Propuesta a &#211;rg Contrat&#8594; Resol
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1426700433935" ID="ID_1406729761" MODIFIED="1426700437604" TEXT="2 meses">
<icon BUILTIN="calendar"/>
</node>
</node>
</node>
<node CREATED="1426680234420" ID="ID_504319318" MODIFIED="1446052432417">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      2.4 Adjudic del contrato
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426609824418" ID="ID_1059576343" MODIFIED="1446052415145" TEXT="Ordinaria">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426609836416" ID="ID_1743832796" MODIFIED="1446052581947">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1. Oferta <u>eco +ventajosa</u>&#8594; req docum y garant&#237;a def a licitador
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426610480239" ID="ID_245188424" MODIFIED="1446052630463">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Garant&#237;a Definitiva: </i>no defectos&#8594; &#8804; <u>5%</u>&#160;presup adjudic (sin IVA)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1426610521252" ID="ID_30961352" MODIFIED="1446052870357">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Complem:</i>&#160;(opc) &#8804; +5% presup adjudic (sin IVA)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426610202241" ID="ID_1621911635" MODIFIED="1426610209324" TEXT="10 d&#xed;as">
<icon BUILTIN="calendar"/>
</node>
</node>
</node>
<node CREATED="1426610211080" ID="ID_1740396149" MODIFIED="1446052674786">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      2. Adjudic, <u>notificaci&#243;n</u>&#160;&#160;a licitadores y public en <u>perfil</u>&#160;contratante&#160;
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426610261518" ID="ID_343862880" MODIFIED="1426610267017" TEXT="5 d&#xed;as">
<icon BUILTIN="calendar"/>
</node>
</node>
<node CREATED="1426610268700" ID="ID_1450327747" MODIFIED="1446052710113">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      3. Posibilidad de <u>recurso</u>&#8594; impugn del acuerdo de adjudic (previa a rec cont-adm)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426610279428" ID="ID_78656663" MODIFIED="1426610283171" TEXT="15 d&#xed;as">
<icon BUILTIN="calendar"/>
</node>
</node>
<node CREATED="1426610286483" ID="ID_206625540" MODIFIED="1446052729617">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      4. Formalizaci&#243;n: en doc adm (o notar&#237;a si lo costea el licitador)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426610320147" ID="ID_987066538" MODIFIED="1426610331463" TEXT="5 d&#xed;as (naturales)">
<icon BUILTIN="calendar"/>
</node>
</node>
<node CREATED="1426610910959" ID="ID_1494424304" MODIFIED="1446052814643">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      5. Publicidad de formalizaci&#243;n&#8594; en perfil contratante si &#8805; 18k&#8364;, BOE si &#8805; <u>100k</u>&#8364; y DOUE si SARA
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1426610378945" ID="ID_1449572278" MODIFIED="1426610382415" TEXT="48 d&#xed;as">
<icon BUILTIN="calendar"/>
</node>
</node>
</node>
<node CREATED="1426610780675" ID="ID_239443332" MODIFIED="1446052839361">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Urgente: </i>inter&#233;s p&#250;blico o necesidad inaplazable&#8594; plazos a mitad (excepto recurso)
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426610833382" ID="ID_1104822019" MODIFIED="1446052851034">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Emergencia: </i>cat&#225;strofe, defensa&#8594; proced verbal y sin plazos (inmediato)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1426680519838" ID="ID_71237518" MODIFIED="1446052455816">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2.5 Fiscalizaci&#243;n del contrato:&#160; </i>IGAE
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426680539270" ID="ID_31045231" MODIFIED="1446052480747">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2.6 Aprob compromiso de gasto:</i>&#160;&#243;rg de contrat
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1426680681815" ID="ID_1542164156" MODIFIED="1446052890778" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      3. Ejecuci&#243;n y
    </p>
    <p>
      liquidaci&#243;n
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426680833259" ID="ID_592158152" MODIFIED="1446052915065">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>3.1 Ejec del contrato:</i>&#160;suministro de bienes o prestaci&#243;n del servicio al centro gestor
    </p>
  </body>
</html></richcontent>
<node CREATED="1426680845750" ID="ID_116221378" MODIFIED="1446052953789">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si contratista se demora&#8594; <u>resol</u>&#160;contrato + penalidades diarias (0,2/1000&#8364;) variable cada 5%
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1426680919262" ID="ID_483174491" MODIFIED="1446052971417">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>3.2 Validaci&#243;n y pruebas: </i>serv t&#233;cnico del centro gestor comprueba cumplim
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426680938843" ID="ID_1113863242" MODIFIED="1446052991863" TEXT="3.3 Recepci&#xf3;n de la inversi&#xf3;n">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1446052991847" ID="ID_1525340750" MODIFIED="1446052996910" TEXT="Acto de comprob material">
<node CREATED="1426762326590" ID="ID_206998079" MODIFIED="1446053065357">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contrato &#8805; 50k&#8364;
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426762400647" ID="ID_86131762" MODIFIED="1446053031863" TEXT="Interventor IGAE (solicitado), repre centro gestor, contratista (+asesores)"/>
<node CREATED="1426762430743" ID="ID_766510658" MODIFIED="1446053046371" TEXT="Acta de Recepci&#xf3;n">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1426680961057" ID="ID_397691756" MODIFIED="1426681763877" TEXT="Bienes o servs pasan a ser propiedad de la AP"/>
<node CREATED="1426680969423" ID="ID_1331415132" MODIFIED="1426681000237" TEXT="Comienza plazo de garant&#xed;a definitiva y de pago al contratista"/>
</node>
</node>
<node CREATED="1426762445991" ID="ID_1720049214" MODIFIED="1446053069405">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contrato &lt; 50k&#8364;
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1426762400647" ID="ID_1772617346" MODIFIED="1426762527249" TEXT="Repre centro gestor, contratista (+asesores)">
<node CREATED="1426762430743" ID="ID_1987316638" MODIFIED="1446053047268" TEXT="Certificado de Aceptaci&#xf3;n">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1426681020880" ID="ID_1292534609" MODIFIED="1446053136524">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>3.4 Liquidaci&#243;n del contrato: </i>AP reconoce obligaci&#243;n y paga&#8594; <u>devol</u>&#160;de garant&#237;a definitiva (tras plazo)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</map>
