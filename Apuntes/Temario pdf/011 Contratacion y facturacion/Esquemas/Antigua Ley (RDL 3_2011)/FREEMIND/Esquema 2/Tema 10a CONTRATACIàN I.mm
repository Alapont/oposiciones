<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1426681384580" ID="ID_1990084009" MODIFIED="1446048171669">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T41 LCSP I
    </p>
    <p style="text-align: center">
      RD 3/2011
    </p>
  </body>
</html></richcontent>
<node CREATED="1426681252851" ID="ID_623654800" MODIFIED="1446059964879" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ley de Contratos del SP
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1446059964857" ID="ID_985652751" MODIFIED="1446060021880">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      RD 3/2011&#8594; texto refundido de la Ley 30/2007
    </p>
  </body>
</html></richcontent>
<node CREATED="1426607376037" ID="ID_1594724935" MODIFIED="1446048195083">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;Acceso a licitaciones, transpar, igualdad, uso de fondos, libre competencia, elecci&#243;n de oferta
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1436352518219" ID="ID_1098547113" MODIFIED="1446048373486">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Sujeto A Reg Armonizada:</i>&#160;(SARA) seg&#250;n Directiva UE
    </p>
  </body>
</html></richcontent>
<node CREATED="1436352551964" ID="ID_1036729454" MODIFIED="1446048385756">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Valor Estimado: </i>precio de licitaci&#243;n (sin IVA) + pr&#243;rrogas
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1426607476883" ID="ID_1552498916" MODIFIED="1426676152780" POSITION="right" TEXT="&#xc1;mbito">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426607487253" ID="ID_1293722271" MODIFIED="1426607545967" TEXT="SP y subvencionados">
<node CREATED="1426607731299" ID="ID_1884947582" MODIFIED="1446048254572" TEXT="Servs de tarifa o financ, convenios internac, expropiaciones, compraventa o arrendam de inmuebles">
<icon BUILTIN="stop-sign"/>
</node>
</node>
</node>
<node CREATED="1426607833601" ID="ID_691695108" MODIFIED="1426676152164" POSITION="right" TEXT="Tipos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426607856210" ID="ID_1841831120" MODIFIED="1462177614392">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Obras y concesi&#243;n de obras (SARA si VE&gt;5,225M&#8364;), Gesti&#243;n de servs p&#250;b, Colab p&#250;b-priv
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426608115698" ID="ID_1638692158" MODIFIED="1462177600673">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b><i>Suministros&#160;y servs (TIC)</i></b><i>: </i>SARA si VE &#8805; <u>135k</u>&#8364; (AGE, OOAA, SS) o &#8805; <u>209k</u>&#8364; (resto)
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1426608010994" ID="ID_830145720" MODIFIED="1446048471034">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Suministros:&#160; </i>equipos (compra/alquiler), pgms (o COTS) y licencias
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426608026375" ID="ID_1964566998" MODIFIED="1436352722453" TEXT="Servicios">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426608306464" ID="ID_507712865" MODIFIED="1436352739038">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mantenimiento, pgms&#160; <u>a medida</u>, consultor&#237;a y formaci&#243;n
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426608308879" ID="ID_1590883241" MODIFIED="1426609393062" TEXT="Hasta 4 a&#xf1;os (+2 pr&#xf3;rroga). 1 a&#xf1;o si &lt;18k&#x20ac; (contratos menores)"/>
</node>
</node>
</node>
<node CREATED="1426608528249" ID="ID_1902047424" MODIFIED="1446048509730" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#211;rganos de
    </p>
    <p>
      contrataci&#243;n
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426608533650" ID="ID_603896059" MODIFIED="1446048624194">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      M&#225;x autoridad de cada &#225;rea (pueden delegar)
    </p>
  </body>
</html></richcontent>
<node CREATED="1426608546649" ID="ID_1722783958" MODIFIED="1446049937561">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Consejo Ministros debe autorizar si VE &#8805; <u>12M</u>&#8364;
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1426608571749" ID="ID_1406029286" MODIFIED="1446048750762">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b><i>RD 806/2014</i></b>
    </p>
    <p>
      (Gobernanza TIC)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426608582513" ID="ID_1818857612" MODIFIED="1446048572708" TEXT="Suministros">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426608595013" ID="ID_1124429168" MODIFIED="1446050390807">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si precio de licitaci&#243;n &gt; <u>1M</u>&#8364;&#8594; <u>DGRCC</u>&#160;(Dir Gen de Racionaliz y Contrat Centraliz, del MinHAP)
    </p>
  </body>
</html></richcontent>
<node CREATED="1446050272526" ID="ID_14565504" MODIFIED="1446050277502" TEXT="Reco del CORA">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1426608641514" ID="ID_138410181" MODIFIED="1446050403192">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si licitaci&#243;n &#8804; <u>1M&#8364;, MDef o SS</u>&#8594; propios &#243;rganos
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426608704702" ID="ID_1567904913" MODIFIED="1446048573428" TEXT="Servicios">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426608734601" ID="ID_146433263" MODIFIED="1436352770810" TEXT="Propios &#xf3;rganos"/>
</node>
<node CREATED="1426673578193" ID="ID_497974264" MODIFIED="1446050271137">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contrataci&#243;n
    </p>
    <p>
      Centralizada
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1426673601236" ID="ID_685582958" MODIFIED="1446048722945">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contrat general y homog&#233;nea con plazos muy breves
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
<node CREATED="1426673676873" ID="ID_1582203790" MODIFIED="1462177573879">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Suministros:</i>&#160;&#160;equipos unit, pgms, impresoras, seg, ciberseg
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1426673711417" ID="ID_87803753" MODIFIED="1446049091070">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Servs: </i>rels con Adm-e con licitaci&#243;n&#160;&#8804; <u>862k</u>&#8364; y telecos
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426674245479" ID="ID_1150336479" MODIFIED="1446050568205">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Conecta Centraliz:</i>&#160;&#160;plataf-e con <u>cat&#225;logo</u>&#160;de bienes y servs de CC (gesti&#243;n&#160;por DGRCC)
    </p>
  </body>
</html></richcontent>
<node CREATED="1446050538431" ID="ID_750507567" MODIFIED="1446050546424" TEXT="catalogocentralizado.minhap.es">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1426673781494" ID="ID_1431416174" MODIFIED="1446049028328">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1. Adopci&#243;n de tipo
    </p>
  </body>
</html></richcontent>
<node CREATED="1446049030211" ID="ID_365839485" MODIFIED="1446050296640">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Junta&#160;CC</u>&#160;(&#243;rg de contrat interministerial, adscrito a la DGRCC)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1426673794355" ID="ID_289431432" MODIFIED="1446049109545" TEXT="Acuerdo Marco">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426673995740" ID="ID_99744563" MODIFIED="1446049120599" TEXT="Cat&#xe1;logo con varios proveedores (m&#xed;n 3)"/>
<node CREATED="1426674010015" ID="ID_830319840" MODIFIED="1426674022015" TEXT="Durante &lt;4 a&#xf1;os no se puede modificar y no se convocan licitaciones"/>
</node>
<node CREATED="1426673844126" ID="ID_980038836" MODIFIED="1446049132219" TEXT="S&#xaa; din&#xe1;mico de contrataci&#xf3;n">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426674028340" ID="ID_1450146945" MODIFIED="1446049141274" TEXT=" Proc abierto, en &lt;4a&#xf1;os se pueden presentar y mejorar ofertas"/>
</node>
</node>
<node CREATED="1426674081279" ID="ID_322531915" MODIFIED="1426674207368" TEXT="2. Adquisici&#xf3;n">
<node CREATED="1426674199854" ID="ID_254214996" MODIFIED="1446049161540">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Dentro del tipo: DGRCC mediante proc <u>negociado</u>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426674220501" ID="ID_1168879214" MODIFIED="1426674227679" TEXT="Fuera: Junta CC"/>
</node>
</node>
</node>
</node>
<node CREATED="1426608886348" ID="ID_1498346600" MODIFIED="1436352790108" POSITION="right" TEXT="Contratistas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436352790082" ID="ID_1907050628" MODIFIED="1436352800412" TEXT="Deben demostrar">
<node CREATED="1426608968970" ID="ID_812476454" MODIFIED="1446049223447">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Capacidad de obrar: inscrip en Reg Mercantil
    </p>
  </body>
</html></richcontent>
<node CREATED="1426609189282" ID="ID_195116440" MODIFIED="1446050685062" TEXT="Insolventes, resol anterior, condenados, sancionados, no al corriente en SS ">
<icon BUILTIN="stop-sign"/>
</node>
</node>
<node CREATED="1426608981783" ID="ID_558199087" MODIFIED="1426608991413" TEXT="Acreditaci&#xf3;n de solvencia">
<node CREATED="1426608991413" ID="ID_433223694" MODIFIED="1446049236133">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Eco-fin:</i>&#160;informes, cuentas o trabajos de 3 a&#241;os
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426609005005" ID="ID_1530100497" MODIFIED="1446049240523">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>T&#233;cn-prof: </i>equipo, medidas calidad o trabajos de 3 a&#241;os
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1426609038129" ID="ID_1332126788" MODIFIED="1446049587891" TEXT="Clasificaci&#xf3;n">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426609069041" ID="ID_1135531314" MODIFIED="1446049264715">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Categor&#237;as de contratos a optar seg&#250;n capacidades
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1446049264719" ID="ID_793158581" MODIFIED="1446049272413">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Inscrip en <u>ROLEC</u>&#160;(Reg Of de Licitadores y Emprs Clasif)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426609045439" ID="ID_1234769581" MODIFIED="1426609174524" TEXT="Obligatorio obras VE&gt;500k&#x20ac;">
<node CREATED="1426609174525" ID="ID_1298666072" MODIFIED="1446049343462">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Voluntario</u>&#160;&#160;para el resto (convalida acreditaci&#243;n)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node CREATED="1426609115829" ID="ID_1128174952" MODIFIED="1446049326493">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Vigencia indef, mantenimiento&#8594; eco-fin (1 a&#241;o), t&#233;cn-prof (3 a&#241;os)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1426674596906" ID="ID_218507503" MODIFIED="1446050479625" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Plataf de Contrat-e
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426674743505" ID="ID_1205427587" MODIFIED="1426674887272" TEXT="Nodo central de intercambio de info y publicidad contractual">
<icon BUILTIN="info"/>
<node CREATED="1426674772831" ID="ID_4365203" MODIFIED="1446050486505" TEXT="Gestionado por la Junta Consultiva de Contrat Adm (JCCA)">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1446050486501" ID="ID_1886947190" MODIFIED="1446050490779" TEXT="contrataciondelestado.es">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1426674784918" ID="ID_1754658862" MODIFIED="1446049384446" TEXT="Conectado con diarios oficiciales (BOE, DOUE...) y ROLEC"/>
<node CREATED="1426674820344" ID="ID_434084170" MODIFIED="1446049441805">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Perfil&#160;de contratante: </i>cada &#243;rg de contrat&#8594; publicado en Inet, forma de acceso especificada
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426674607465" ID="ID_377989121" MODIFIED="1446049719829" TEXT="Uso de medios EIT">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426674615015" ID="ID_1678207983" MODIFIED="1446049470651">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Est&#225;ndares abiertos o uso general, que garanticen ACIDT y firmaE
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426674645046" ID="ID_175202467" MODIFIED="1426674655561" TEXT="Especificado en pliegos">
<node CREATED="1426674655982" ID="ID_454698695" MODIFIED="1446050731291">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Potestativo para licitadores pero le <u>vincula</u>&#160;para todo el proceso
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
</node>
<node CREATED="1426675318357" ID="ID_975447243" MODIFIED="1446049590524" TEXT="CODICE">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426675319973" ID="ID_1664217148" MODIFIED="1446049519247">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (COmps y Docs Interop de Contrat-E) idioma para comunicarnos con la plataf
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1426675371033" ID="ID_487481263" MODIFIED="1446049534237">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Gestionado por Dir Gen de Patrimonio del Estado
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426675432160" ID="ID_1973806344" MODIFIED="1446050761784">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Libs:</i>&#160;la plataf trabaja a nivel de <u>comps reut</u>&#8594; 'Espacio virtual de licitaci&#243;n'
    </p>
  </body>
</html></richcontent>
<node CREATED="1426675459619" ID="ID_851178161" MODIFIED="1446049576221">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Partes se comunican mediante docs <u>XML</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1426675809386" ID="ID_770571137" MODIFIED="1446049615991">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tipos de acceso
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426675819627" ID="ID_1221826308" MODIFIED="1446049634574">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Sist&#233;mico: </i>&#243;rg contrat o licitador NO usan portal (trabajan con sus sistemas)&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1426675853073" ID="ID_536829772" MODIFIED="1446049700904">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se comunican por&#160; <u>WS</u>&#160;y reciben docs en modo <u>B2B</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426675819627" ID="ID_1804555854" MODIFIED="1446049684716">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Web: </i>&#243;rg contrat o licitador S&#205; usan el portal
    </p>
  </body>
</html></richcontent>
<node CREATED="1426675925030" ID="ID_7322260" MODIFIED="1446050775074">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pueden trabajar simult&#225;neamente en <u>portal</u>&#160;y sus sistemas (exportan info en CODICE)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
</map>
