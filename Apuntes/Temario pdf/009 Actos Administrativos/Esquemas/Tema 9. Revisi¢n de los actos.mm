<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1386873509510" ID="ID_900994694" MODIFIED="1386877108434" TEXT="Tema 8. Revisi&#xf3;n de los actos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1394272344303" ID="ID_1995143310" MODIFIED="1481278308659" POSITION="right" TEXT="Nulabilidad y anulabilidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1394272363079" ID="ID_1667037960" MODIFIED="1394272520262" TEXT="Actos nulos de pleno derecho">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1392875244312" ID="ID_470521700" MODIFIED="1481285000648" TEXT="Nulabilidad (47.1)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Un acto es nulo en si mismo, no tiene ning&#xfa;n efecto jur&#xed;dico, y lo deben declarar de oficio los juzgados y autoridades que tengan conocimiento del mismo. No es subsanable.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1392875291601" ID="ID_828885628" MODIFIED="1392875983128" TEXT="a) Lesionen DERECHOS Y LIBERTADES susceptibles de amparo constitucional">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1392875313829" ID="ID_719832071" MODIFIED="1392875984240" TEXT="b) Dictados por &#xf3;rgano manifiestamente INCOMPETENTE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1392875352647" ID="ID_415501712" MODIFIED="1393939900442" TEXT="c) Contenido IMPOSIBLE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1392875365237" ID="ID_1698897474" MODIFIED="1392875985985" TEXT="d) Constitutivos de infracci&#xf3;n PENAL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1392875391537" ID="ID_1002468925" MODIFIED="1392875986625" TEXT="e) Los dictados prescindiendo total y absolutamente del PROCEDIMIENTO legalmente establecido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1392875441649" ID="ID_696271002" MODIFIED="1394272308035" TEXT="f) Actos expresos o presuntos CONTRARIOS AL ORDENAMIENTO JURIDICO por los que se obtienen derechos o facultades cuando se carezca de los requisitos esenciales para su adquisici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1392875507165" ID="ID_882900416" MODIFIED="1392875988517" TEXT="g) Cualquier otro que aparezca expresamente en una disposici&#xf3;n legal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1389562619897" ID="ID_383873374" MODIFIED="1481284988170" TEXT="Nulidad de las disposiciones administrativas (47.2)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1394270726243" ID="ID_1539773150" MODIFIED="1394271272257" TEXT="Que vulneren la Constituci&#xf3;n, las leyes u otras disposiciones administrativas de orden superior">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1394270778195" ID="ID_1220894392" MODIFIED="1394271275779" TEXT="Regulen materias reservadas a la ley">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1394270797892" ID="ID_77151795" MODIFIED="1394271278833" TEXT="Establezcan la retroactividad de disposiciones sancionadoras no favorables o restrictivas de derechos individuales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1392875253867" ID="ID_292982869" MODIFIED="1481284981863" TEXT="Anulabilidad (48)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se diferencia de la nulabilidad en que puede ser subsanable y que el acto es v&#xe1;lido hasta que se declara su anulabilidad, y se declara a solicitud del interesado, por alguno de los motivos que de detallan.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1392875577811" ID="ID_1994837588" MODIFIED="1394270955336" TEXT="1. Cualquier infracci&#xf3;n del ordenamiento jur&#xed;dico, incluso la desviaci&#xf3;n de poder">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1392875608995" ID="ID_1070984787" MODIFIED="1393949754726" TEXT="2. El defecto de forma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1392875646927" ID="ID_1195234070" MODIFIED="1393939873084" TEXT="Carezca de los requisitos INDISPENSABLES"/>
<node CREATED="1392875662282" ID="ID_930839965" MODIFIED="1392875670130" TEXT="De lugar a indefensi&#xf3;n"/>
</node>
<node CREATED="1394270979598" ID="ID_1892244142" MODIFIED="1394271191684" TEXT="3. La realizaci&#xf3;n de una actuaci&#xf3;n fuera del tiempo establecido solo implica la anulabilidad cuando as&#xed; lo imponga la naturaleza de este plazo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1386874012137" FOLDED="true" ID="ID_47300558" MODIFIED="1394273180109" POSITION="right" TEXT="Anulaci&#xf3;n de oficio de los actos administrativos">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tener en cuenta que uno de los principios de actuaci&#xf3;n de la administraci&#xf3;n es su sometimiento pleno a la ley y al Derecho (art. 103), por lo que cualquier acto emanado de la misma que contraveniere el ordenamiento jur&#xed;dico puede ser anulado (art. 63), y son por tanto, susceptibles de recurso. Y ser&#xe1;n nulos de pleno derecho los de los supuestos del art&#xed;culo 62.1.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386874130926" ID="ID_491468640" MODIFIED="1393949050806" TEXT="Organos competentes para la revisi&#xf3;n de oficio de los actos administrativos nulos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386874152622" ID="ID_1611807749" MODIFIED="1386874166932" TEXT="a) Consejo de Ministros"/>
<node CREATED="1386874167643" ID="ID_1351617827" MODIFIED="1386874178610" TEXT="b) En la AGE">
<node CREATED="1386874178611" ID="ID_668253461" MODIFIED="1386874189483" TEXT="Ministros"/>
<node CREATED="1386874189870" ID="ID_689301308" MODIFIED="1386874202928" TEXT="Secretarios de Estado"/>
</node>
<node CREATED="1386874209710" ID="ID_1828326082" MODIFIED="1386874220397" TEXT="c) En los Organismos P&#xfa;blicos">
<node CREATED="1386874224735" ID="ID_1531736842" MODIFIED="1386874236825" TEXT="Los &#xf3;rganos a los que est&#xe1;n adscritos"/>
<node CREATED="1386874238184" ID="ID_454731078" MODIFIED="1386874258148" TEXT="Los m&#xe1;ximos &#xf3;rganos rectores de los organismos"/>
</node>
</node>
<node CREATED="1386874274251" ID="ID_1666719832" MODIFIED="1389562814783" TEXT="Rectificaci&#xf3;n de errores materiales de hecho o aritmeticos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1386874301084" ID="ID_1284810980" MODIFIED="1481284786621" POSITION="right" TEXT="Recursos administrativos (art. 112 a 126)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1389561085465" ID="ID_1097750161" MODIFIED="1481284808096" TEXT="Tipos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386874363990" ID="ID_65286565" MODIFIED="1393951961429" TEXT="Recursos comunes o ordinarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386874395805" FOLDED="true" ID="ID_1628214497" MODIFIED="1481284854522" TEXT="Recurso de alzada (121 y 122)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386876777326" ID="ID_1028946333" MODIFIED="1386876799561" TEXT="Contra las resoluciones y actos de tr&#xe1;mite">
<node CREATED="1386876799562" ID="ID_1272889022" MODIFIED="1386876813637" TEXT="QUE NO PONGAN FIN A LA VIA ADMINISTRATIVA"/>
</node>
</node>
<node CREATED="1386874412206" ID="ID_1477414179" MODIFIED="1481284867532" TEXT="Recurso de reposici&#xf3;n (123 y 124)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386876829773" ID="ID_444247720" MODIFIED="1392876006816" TEXT="QUE PONGAN FIN A LA VIA ADMINISTRATIVA (que no sean firmes)  "/>
</node>
<node CREATED="1392875238838" ID="ID_812888078" MODIFIED="1394271595328" TEXT="Motivos para interponer cualquiera de los dos recursos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1394272425398" ID="ID_156386852" MODIFIED="1394272438580" TEXT="Nulabilidad o anulabilidad de los actos"/>
</node>
</node>
<node CREATED="1386874385924" ID="ID_363650473" MODIFIED="1394272474672" TEXT="Recursos especiales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386874470479" ID="ID_952978598" MODIFIED="1389559828227" TEXT="a) Las reclamaciones econ&#xf3;mico-administrativas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386874529637" ID="ID_504003025" MODIFIED="1389559830181" TEXT="b) Impugnaci&#xf3;n o reclamaci&#xf3;n, conciliaci&#xf3;n, mediaci&#xf3;n y arbitraje">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1386874634477" ID="ID_1678850995" MODIFIED="1481284885324" TEXT="Recurso extraordinario de revisi&#xf3;n (125 y 126)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386876908941" ID="ID_1573270661" MODIFIED="1386876919543" TEXT="Contra los actos firmes en v&#xed;a administrativa"/>
<node CREATED="1386876967403" ID="ID_1822375021" MODIFIED="1481284906545" TEXT="Motivos para interponer este recurso (125)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386876973428" ID="ID_871578636" MODIFIED="1392874859480" TEXT="1&#xba; Que al dictarlos se hubiera incurrido en errores de hecho">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_cancel"/>
<node CREATED="1392874818354" ID="ID_442486377" MODIFIED="1392891159604" TEXT="que resulte de los propios documentos incorporados al expediente">
<icon BUILTIN="folder"/>
</node>
</node>
<node CREATED="1386876979895" ID="ID_827709108" MODIFIED="1392874937650" TEXT="2&#xba; Que aparezcan documentos de valor esencial para la resoluci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="folder"/>
<node CREATED="1392874924843" ID="ID_334462069" MODIFIED="1392891168244" TEXT="que no hubieran aparecido antes">
<icon BUILTIN="calendar"/>
</node>
</node>
<node CREATED="1386876999427" ID="ID_837404044" MODIFIED="1392874975930" TEXT="3&#xba; Que en la resoluci&#xf3;n hayan influido documentos o testimonios falsos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="smiley-angry"/>
</node>
<node CREATED="1386877043510" ID="ID_1784346846" MODIFIED="1392875057322" TEXT="4&#xba; Que la resoluci&#xf3;n se haya dictado como consecuencia de conducta punible">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="stop-sign"/>
<node CREATED="1392875001526" ID="ID_1829819140" MODIFIED="1392875017102" TEXT="Y se haya declarado as&#xed; mediante sentencia judicial firme"/>
</node>
</node>
</node>
</node>
<node CREATED="1392845927633" ID="ID_854463210" MODIFIED="1481284801105" TEXT="Organo competente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1392845933771" ID="ID_414461466" MODIFIED="1392845949322" TEXT="De la misma administraci&#xf3;n">
<node CREATED="1392845949323" ID="ID_70039182" MODIFIED="1392845955786" TEXT="Recurso de alzada">
<node CREATED="1392845955788" ID="ID_1416764972" MODIFIED="1392845967882" TEXT="Organo superior al que dict&#xf3; el acto"/>
</node>
<node CREATED="1392845969503" ID="ID_363141325" MODIFIED="1392845981056" TEXT="Recurso de reposici&#xf3;n">
<node CREATED="1392845981057" ID="ID_438107099" MODIFIED="1392845989336" TEXT="Mismo &#xf3;rgano que dict&#xf3; el acto"/>
</node>
<node CREATED="1392845991794" ID="ID_1219539791" MODIFIED="1392846002784" TEXT="Recurso especial de revisi&#xf3;n">
<node CREATED="1392846002785" ID="ID_840197718" MODIFIED="1392846012964" TEXT="Mismo &#xf3;rgano que dict&#xf3; el acto"/>
</node>
</node>
<node CREATED="1393951814506" ID="ID_1451741749" MODIFIED="1393951822565" TEXT="Recursos especiales">
<node CREATED="1393951822565" ID="ID_1593541818" MODIFIED="1393951840209" TEXT="Organo especial (Ej: Tribunal Econ&#xf3;mico-administrativo)"/>
</node>
</node>
<node CREATED="1386874742049" ID="ID_1995252656" MODIFIED="1481284797674" TEXT="Materia recurrible">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386874767910" ID="ID_64094591" MODIFIED="1392889155262" TEXT="a) Las resoluciones (pongan o no fin a la v&#xed;a administrativa)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386874802566" ID="ID_758451025" MODIFIED="1392889157353" TEXT="b) Los actos de tr&#xe1;mite si deciden directa o indirectamente el fondo del asunto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386874837647" FOLDED="true" ID="ID_1418942402" MODIFIED="1394274314215" TEXT="c) Los actos firmes en v&#xed;a administrativa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386875624171" ID="ID_619148815" MODIFIED="1392891753811" TEXT="Actos que ponen fin a la v&#xed;a administrativa (109)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386875639102" ID="ID_1144134047" MODIFIED="1392846039904" TEXT="a) Las resoluciones de los recursos de alzada (son firmes)"/>
<node CREATED="1386875679618" ID="ID_1304199914" MODIFIED="1392889285179" TEXT="b) Procedimientos de impugnaci&#xf3;n del art. 107.2"/>
<node CREATED="1386875711597" ID="ID_1459348387" MODIFIED="1386875737369" TEXT="c) Resoluciones de &#xf3;rganos administrativos que carezcan de superior jer&#xe1;rquico"/>
<node CREATED="1386875769730" ID="ID_1696393839" MODIFIED="1386875786105" TEXT="Seg&#xfa;n la LOFAGE">
<node CREATED="1386875786106" ID="ID_1794465022" MODIFIED="1386875797810" TEXT="Actos de los miembros del Gobierno"/>
<node CREATED="1386875798595" ID="ID_1934070194" MODIFIED="1386875820022" TEXT="Emanados de Ministros y Secretarios de Estado"/>
<node CREATED="1386875821107" ID="ID_1437838099" MODIFIED="1386875833222" TEXT="Organos de nivel de Director general o superior"/>
<node CREATED="1386875837720" ID="ID_1269706527" MODIFIED="1386875847178" TEXT="En los organismos p&#xfa;blicos">
<node CREATED="1386875847179" ID="ID_1036878443" MODIFIED="1386875866260" TEXT="M&#xe1;ximos &#xf3;rganos de direcci&#xf3;n unipersonales o colegiados"/>
</node>
</node>
</node>
</node>
<node CREATED="1392846050583" ID="ID_207347407" MODIFIED="1392846065292" TEXT="d) Dem&#xe1;s resoluciones, cuando una ley as&#xed; lo establezca"/>
<node CREATED="1386874860737" ID="ID_1519283086" MODIFIED="1394274332313" TEXT="No cabr&#xe1; recurso en v&#xed;a administrativa contra las disposiciones administrativas de car&#xe1;cter general">
<arrowlink DESTINATION="ID_461157817" ENDARROW="Default" ENDINCLINATION="373;0;" ID="Arrow_ID_352583190" STARTARROW="None" STARTINCLINATION="373;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1393951875549" ID="ID_1633386479" MODIFIED="1394271529812" STYLE="bubble" TEXT="SI que pueden revisarse de oficio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
</node>
</node>
</node>
</node>
<node CREATED="1386877109578" FOLDED="true" ID="ID_869085542" MODIFIED="1481284756786" POSITION="right" TEXT="Contencioso-administrativo (LJCA)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386882461646" ID="ID_1911465557" MODIFIED="1394271245023" TEXT="Capacidad para acudir a este recurso">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386882491010" ID="ID_379390190" MODIFIED="1393965262900" TEXT="Capacidad de ser parte">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Aptitud para ser titular de derechos y obligaciones
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386882508667" ID="ID_1621214985" MODIFIED="1386882551222" TEXT="Capacidad procesal">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Capacidad de obrar
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1386882574333" FOLDED="true" ID="ID_1082089638" MODIFIED="1394274098353" TEXT="Legitimaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1393951589250" ID="ID_1346294267" MODIFIED="1393951593073" TEXT="Activa">
<node CREATED="1386883123856" ID="ID_1968642175" MODIFIED="1386883186825" TEXT="Personas f&#xed;sicas o jur&#xed;dicas  con derecho o inter&#xe9;s leg&#xed;timo"/>
<node CREATED="1386883152151" ID="ID_534517243" MODIFIED="1386883166209" TEXT="Grupos que resulten afectados"/>
<node CREATED="1386883189953" ID="ID_1288278168" MODIFIED="1386883222290" TEXT="AGE, para impugnar actos de las CCAA, as&#xed; como de las Entidades Locales"/>
<node CREATED="1386883234109" ID="ID_665220522" MODIFIED="1386883236215" TEXT="CCAA"/>
<node CREATED="1386883236598" ID="ID_486015171" MODIFIED="1386883251857" TEXT="Entidades locales territoriales"/>
<node CREATED="1386883252247" ID="ID_339369805" MODIFIED="1386883259754" TEXT="El Ministerio Fiscal"/>
<node CREATED="1386883260340" ID="ID_665178175" MODIFIED="1386883297279" TEXT="Entidades de Derecho P&#xfa;blico con personalidad jur&#xed;dica propia"/>
</node>
<node CREATED="1386883318314" ID="ID_1007671429" MODIFIED="1393951625053" TEXT="Pasiva (parte demandada)">
<node CREATED="1386883332097" ID="ID_1364730115" MODIFIED="1392892272023" TEXT="AAPP y sus aseguradoras"/>
<node CREATED="1386883338705" ID="ID_272403920" MODIFIED="1386883362435" TEXT="Personas o entidades cuyos derechos pudieran quedar afectados"/>
</node>
</node>
<node CREATED="1386882596919" FOLDED="true" ID="ID_1176954828" MODIFIED="1393965891816" TEXT="Postulaci&#xf3;n (23)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386883436024" ID="ID_1580597087" MODIFIED="1386883448032" TEXT="Organos unipersonales">
<node CREATED="1386883448034" ID="ID_200033975" MODIFIED="1386883463493" TEXT="PUEDEN representarse por Procurador"/>
<node CREATED="1386883463974" ID="ID_1199179454" MODIFIED="1386883472540" TEXT="Asistidas por un Abogado"/>
</node>
<node CREATED="1386883483282" ID="ID_639856740" MODIFIED="1386883492075" TEXT="Organos colegiados">
<node CREATED="1386883493669" ID="ID_1303847047" MODIFIED="1386883515730" TEXT="DEBER&#xc1;N representarse por Procurador"/>
<node CREATED="1386883516098" ID="ID_1404051606" MODIFIED="1386883522616" TEXT="Asistidas por un Abogado"/>
</node>
</node>
<node CREATED="1386882608913" FOLDED="true" ID="ID_822811761" MODIFIED="1394274729421" TEXT="Actos impugnables">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386883542377" ID="ID_1729938863" MODIFIED="1393951666440" TEXT="a) Actos administrativos (25.1)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386883683569" ID="ID_805220718" MODIFIED="1389619177251" TEXT="Que sea definitivo">
<node CREATED="1392895579862" ID="ID_723673804" MODIFIED="1393966390097" TEXT="Ponga fin al procedimiento">
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1386883693849" ID="ID_351899512" MODIFIED="1386883706053" TEXT="o de tr&#xe1;mite cualificado">
<node CREATED="1392895540404" ID="ID_129853484" MODIFIED="1393966393825" TEXT="Decide directa o indirectamente sobre el fondo del asunto">
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1386883706404" ID="ID_1735963845" MODIFIED="1392895597662" TEXT="Haya agotado o puesto fin a la v&#xed;a administrativa (109)"/>
</node>
<node CREATED="1386883562819" ID="ID_461157817" MODIFIED="1394274332312" TEXT="b) Disposiciones de car&#xe1;cter general (25.1, 26 y 27)">
<linktarget COLOR="#b0b0b0" DESTINATION="ID_461157817" ENDARROW="Default" ENDINCLINATION="373;0;" ID="Arrow_ID_352583190" SOURCE="ID_1519283086" STARTARROW="None" STARTINCLINATION="373;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1389619274119" ID="ID_1915855750" MODIFIED="1389619296560" TEXT="As&#xed; como los actos que se produzcan en aplicaci&#xf3;n de la disposici&#xf3;n general"/>
</node>
<node CREATED="1386883591005" ID="ID_1316416512" MODIFIED="1392892849564" TEXT="c) Inactividad material de la Administraci&#xf3;n (25.2, 29 y 32.1)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386883619291" ID="ID_1721893617" MODIFIED="1393966361268" TEXT="d) &quot;V&#xed;a de hecho&quot; (25.2, 30 y 32.2)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mediante este recurso se pueden combatir aquellas &#xab;actuaciones materiales&#xbb; de la Administraci&#xf3;n que carecen de la necesaria cobertura jur&#xed;dica
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1386882629793" FOLDED="true" ID="ID_146694734" MODIFIED="1394275193645" TEXT="Fases">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386882639895" ID="ID_1532173006" MODIFIED="1394274950384" TEXT="Interposici&#xf3;n del recurso y reclamaci&#xf3;n del expediente (45 a 48)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1389619554284" ID="ID_1912896464" MODIFIED="1392894972333" TEXT="Se cita el ACTO que se impugna">
<icon BUILTIN="list"/>
</node>
<node CREATED="1389619562484" ID="ID_1565509131" MODIFIED="1392894984525" TEXT="Se solicita se tenga por interpuesto el RECURSO">
<icon BUILTIN="edit"/>
</node>
<node CREATED="1389619617951" ID="ID_1948078150" MODIFIED="1392894993454" TEXT="Se reclama el EXPEDIENTE al &#xf3;rgano autor del acto">
<icon BUILTIN="korn"/>
</node>
</node>
<node CREATED="1386882662259" ID="ID_1925098503" MODIFIED="1394274943381" TEXT="Emplazamiento de los demandados y admisi&#xf3;n del recurso (49 a 51)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1389619687213" ID="ID_293600295" MODIFIED="1392893109200" TEXT="Se notifica a los interesados.">
<icon BUILTIN="Mail"/>
</node>
<node CREATED="1389619702802" ID="ID_949637711" MODIFIED="1392893092148" TEXT="La reclamaci&#xf3;n del expediente ya da por notificada a la Administraci&#xf3;n">
<icon BUILTIN="folder"/>
</node>
<node CREATED="1392895741646" ID="ID_1338263658" MODIFIED="1392895774127" TEXT="El Juzgado o sala puede declarar la inadmisibilidad del recurso">
<icon BUILTIN="stop-sign"/>
</node>
</node>
<node CREATED="1386882698604" ID="ID_1518018797" MODIFIED="1394274893164" TEXT="Demanda y contestaci&#xf3;n (52 a 57)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1389619764173" ID="ID_760124083" MODIFIED="1392893082924" TEXT="Se ordena la entrega del expediente al recurrente">
<icon BUILTIN="folder"/>
<icon BUILTIN="forward"/>
<node CREATED="1389619788942" ID="ID_1120035677" MODIFIED="1392901366660" TEXT="Para que deduzca del mismo la correspondiente DEMANDA">
<icon BUILTIN="edit"/>
</node>
</node>
<node CREATED="1392901367990" ID="ID_210714442" MODIFIED="1392901412357" TEXT="Se dar&#xe1; traslado de la misma a las partes demandadas">
<icon BUILTIN="Mail"/>
</node>
</node>
<node CREATED="1386882712153" ID="ID_296213574" MODIFIED="1394274892315" TEXT="Alegaciones previas (58 y 59)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
<node CREATED="1389619829516" ID="ID_1973284091" MODIFIED="1389631151090" TEXT="Contra la competencia del &#xf3;rgano jurisdiccional"/>
<node CREATED="1389619868937" ID="ID_558494619" MODIFIED="1389619889577" TEXT="O la inadmisibilidad del recurso"/>
</node>
<node CREATED="1386882731747" ID="ID_1535108989" MODIFIED="1394274873426" TEXT="Prueba (60 y 61)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-5"/>
<node CREATED="1389620067934" ID="ID_402902951" MODIFIED="1389620108427" TEXT="Se solicitar&#xe1; en los escritos (demanda, contestaci&#xf3;n y alegaciones)"/>
<node CREATED="1389620125252" ID="ID_1526788599" MODIFIED="1392892938886" TEXT="Cuando exista disconformidad en los hechos"/>
<node CREATED="1392895835662" ID="ID_87670928" MODIFIED="1392895874401" TEXT="El organo judicial puede recabar informe de los organismos p&#xfa;blicos competentes"/>
</node>
<node CREATED="1386882756955" ID="ID_1435622401" MODIFIED="1394271348588" TEXT="Vista y conclusiones (62 a 66)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-6"/>
<node CREATED="1389620194963" ID="ID_855654232" MODIFIED="1389620233192" TEXT="Las partes podr&#xe1;n solicitar que se celebre vista, presenten conclusiones o que sea declarado concluso"/>
<node CREATED="1389632031439" ID="ID_1320320456" MODIFIED="1389632049934" TEXT="Las partes presentar&#xe1;n de forma sucinta sus alegaciones"/>
<node CREATED="1389620233873" ID="ID_51943389" MODIFIED="1389620289154" TEXT="Celebrada la vista o presentadas las conclusiones,">
<node CREATED="1389620289154" ID="ID_984028712" MODIFIED="1393953011701">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#xa0;el Juez o tribunal declarar&#xe1; el pleito ha concluido para <b>sentencia</b>
    </p>
  </body>
</html></richcontent>
<arrowlink DESTINATION="ID_515721496" ENDARROW="Default" ENDINCLINATION="907;0;" ID="Arrow_ID_1425901574" STARTARROW="None" STARTINCLINATION="907;0;"/>
</node>
</node>
<node CREATED="1386882771405" ID="ID_1670978867" MODIFIED="1389630863251" TEXT="Otros modos de terminaci&#xf3;n">
<node CREATED="1386882796993" ID="ID_109951348" MODIFIED="1386882820498" TEXT="a) Desistimiento del demandante"/>
<node CREATED="1386882808442" ID="ID_954516325" MODIFIED="1392898369436" TEXT="b) El allanamiento">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Aceptaci&#xf3;n y reconocimiento por el demandado de las pretensiones del demandante.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386882828964" ID="ID_1794403418" MODIFIED="1392895977020" TEXT="c) Satisfacci&#xf3;n extraprocesal">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      La Administraci&#xf3;n demandada reconoce totalmente en v&#xed;a administrativa las pretensiones del demandante
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386882844020" ID="ID_10074101" MODIFIED="1392898090021" TEXT="d) Conciliaci&#xf3;n judicial o transacci&#xf3;n procesal">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Posibilidad de alcanzar un acuerdo que ponga fin a la controversia.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1386882990515" FOLDED="true" ID="ID_515721496" MODIFIED="1394275607052" TEXT="La sentencia">
<linktarget COLOR="#b0b0b0" DESTINATION="ID_515721496" ENDARROW="Default" ENDINCLINATION="907;0;" ID="Arrow_ID_1425901574" SOURCE="ID_984028712" STARTARROW="None" STARTINCLINATION="907;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1389633007297" ID="ID_1520935861" MODIFIED="1389633043633" TEXT="Deber&#xe1; ser CONGRUENTE con la demanda y MOTIVADA   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386883029536" ID="ID_135676019" MODIFIED="1393953086643" TEXT="Concepto y clases">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386884308268" ID="ID_1143190295" MODIFIED="1386884348776" TEXT="a) Inadmisibilidad del recurso"/>
<node CREATED="1386884329759" ID="ID_682428292" MODIFIED="1386884354956" TEXT="b) Estimaci&#xf3;n o desistimaci&#xf3;n del recurso"/>
</node>
<node CREATED="1386883037883" ID="ID_346278133" MODIFIED="1393953092072" TEXT="Eficacia de las sentencias (72)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386884440105" ID="ID_1421711162" MODIFIED="1386884451153" TEXT="Para todas las personas afectadas">
<node CREATED="1386884427492" ID="ID_1518025570" MODIFIED="1392897885508" TEXT="Anulaci&#xf3;n de una disposici&#xf3;n o acto ">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1386884407668" ID="ID_264222023" MODIFIED="1386884424705" TEXT="S&#xf3;lo producir&#xe1; efectos entre las partes">
<node CREATED="1386884395130" ID="ID_1587293947" MODIFIED="1392897861533" TEXT="Inadmisibilidad o desestimaci&#xf3;n">
<icon BUILTIN="smily_bad"/>
</node>
<node CREATED="1386884455418" ID="ID_1904582670" MODIFIED="1392897865837" TEXT="Estimaci&#xf3;n de pretensiones de reconocimiento de situaci&#xf3;n jur&#xed;dica">
<icon BUILTIN="ksmiletris"/>
</node>
</node>
<node CREATED="1386884502142" ID="ID_1997439385" MODIFIED="1392897901013" TEXT="No afectar&#xe1;n a la eficacia de las sentencias o actos administrativos firmes anteriores (73)">
<icon BUILTIN="closed"/>
</node>
</node>
<node CREATED="1386883050941" ID="ID_968949067" MODIFIED="1393953102509" TEXT="Ejecuci&#xf3;n de las sentencias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386884565330" ID="ID_586059505" MODIFIED="1386884582488" TEXT="Las partes est&#xe1; obligadas a cumplir las sentencias"/>
<node CREATED="1386884583185" ID="ID_1946400703" MODIFIED="1386884610707" TEXT="Todas las personas y entidades deben colaborar (103.2 y 3)"/>
<node CREATED="1393954006630" ID="ID_150866716" MODIFIED="1393954024392" TEXT="En materia tributaria y de personal">
<node CREATED="1393954024392" ID="ID_207661537" MODIFIED="1393954047668" TEXT="se podr&#xe1; extender la sentencia a personas que se encuentran en una situaci&#xf3;n id&#xe9;ntica"/>
</node>
<node CREATED="1393954050232" ID="ID_1777389290" MODIFIED="1393954083961" TEXT="La conciliacion judicial tiene el mismo efecto de ejecuci&#xf3;n forzosa que la sentencia"/>
</node>
</node>
<node CREATED="1386882857428" FOLDED="true" ID="ID_217875933" MODIFIED="1394275604776" TEXT="El procedimiento abreviado (78)">
<node CREATED="1386884144262" ID="ID_1529095580" MODIFIED="1386884150709" TEXT="Personal de las AAPP"/>
<node CREATED="1386884151025" ID="ID_1396017630" MODIFIED="1386884158582" TEXT="Extranjer&#xed;a"/>
<node CREATED="1386884162132" ID="ID_1840074368" MODIFIED="1386884180274" TEXT="Inadmisi&#xf3;n de peticiones de asilo pol&#xed;tico"/>
<node CREATED="1386884180989" ID="ID_1117525415" MODIFIED="1386884189885" TEXT="Disciplina deportiva dopaje"/>
<node CREATED="1386884190745" ID="ID_1811067921" MODIFIED="1386884201138" TEXT="Cuant&#xed;a no superior a 30.000 &#x20ac;"/>
</node>
<node CREATED="1386882867437" FOLDED="true" ID="ID_811830473" MODIFIED="1394275603590" TEXT="Los procedimientos especiales">
<node CREATED="1386882895350" ID="ID_1566869149" MODIFIED="1386882912099" TEXT="a) Protecci&#xf3;n de los derechos fundamentales de la persona"/>
<node CREATED="1386882919976" ID="ID_1172734265" MODIFIED="1386882928542" TEXT="b) Cuesti&#xf3;n de ilegalidad"/>
<node CREATED="1386882929307" ID="ID_260212796" MODIFIED="1389633110259" TEXT="c) En caso de suspensi&#xf3;n administrativa previa de acuerdos"/>
</node>
</node>
</node>
</map>
