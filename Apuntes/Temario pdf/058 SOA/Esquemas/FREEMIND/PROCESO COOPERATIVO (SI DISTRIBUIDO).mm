<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1426243021833" ID="ID_164386252" MODIFIED="1480698097600">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      PROCESO
    </p>
    <p>
      COOPERATIVO
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1430469009365" ID="ID_11232167" MODIFIED="1430472230543" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      S&#170; Distribuido
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426243562484" ID="ID_1334429359" MODIFIED="1445961329327">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Computadoras diferentes/indep interact&#250;an en red con <u>obj com&#250;n</u>&#160;
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1426243773733" ID="ID_1383241300" MODIFIED="1430471517102" TEXT="-Coste (downsize), fiable (si uno falla, ok), escalable, flexible (varios SOs)">
<icon BUILTIN="button_ok"/>
<node CREATED="1426243827839" ID="ID_1869644088" MODIFIED="1445961257414">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Arq <u>abierta</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1426243843342" ID="ID_273634935" MODIFIED="1445961280624">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No todas las apps sirven, necesita SW adicional, no reloj com&#250;n (problemas de sync)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1426243675627" ID="ID_1279966644" MODIFIED="1445961352912">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Recs distrib y compartidos por interfaz, <u>aislados</u>&#160;para el usuario
    </p>
  </body>
</html></richcontent>
<node CREATED="1428773588392" ID="ID_1511358582" MODIFIED="1428773603203" TEXT="Acoplaci&#xf3;n +d&#xe9;bil que multicomputadores">
<icon BUILTIN="yes"/>
<node CREATED="1428773650461" ID="ID_1111052609" MODIFIED="1445961485848">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comunicaci&#243;n, no c&#243;mputo (pe red SARA)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
</node>
<node CREATED="1426243687698" ID="ID_158131051" MODIFIED="1445963656863">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Transparencia (visi&#243;n: <u>serv &#250;nico </u>)
    </p>
  </body>
</html></richcontent>
<node CREATED="1426244283804" ID="ID_1523184333" MODIFIED="1430478974332">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      de acceso (= a remoto q a local), ubic (sin conocerla), concurrencia (compartir sin interferir), r&#233;plica de recursos (+fiabilidad), ante fallos (rollback a momento previo), migraci&#243;n (por cambio de ubic), prestaciones (adapt seg&#250;n demanda = elasticidad), escalabilidad (crecer sin cambios)
    </p>
  </body>
</html></richcontent>
<node CREATED="1430469738122" ID="ID_7180144" MODIFIED="1445961381950">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Esc V: </i>migrar a serv mayores
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1430469747864" ID="ID_1775446839" MODIFIED="1445961385906">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Esc H: </i>a&#241;adir/quitar m&#225;qs
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1426244807346" ID="ID_292974769" MODIFIED="1430477057157" TEXT="Tipos de SO">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426244809700" ID="ID_1670327587" MODIFIED="1430470295110">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Centralizados: </i>monol&#237;ticos, solo gestionan 1 m&#225;q
    </p>
  </body>
</html></richcontent>
<node CREATED="1426244863874" ID="ID_1756743358" MODIFIED="1430470308421">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>En red (NOS):</i>&#160;visi&#243;n de varias m&#225;qs indep (no transp)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426244875729" ID="ID_173672134" MODIFIED="1430470341434">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b><i>Cooperativos:</i></b>&#160;visi&#243;n de <u>integraci&#243;n de servs</u>&#160;por middleware (transparente)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1426244924184" ID="ID_1466454242" MODIFIED="1445961552903">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Distrib: </i>&#160;visi&#243;n de uniproc virtual. No se usan mucho (Amoeba, Mach, Chorus)&#160;
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1426245849455" ID="ID_1310494854" MODIFIED="1430471974626" TEXT="Modelos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426245851704" ID="ID_1054621950" MODIFIED="1445961473320">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cliente/Serv
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426245867678" ID="ID_1234505168" MODIFIED="1445961461606">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Redimensionam, carga distrib, almac, compartici&#243;n recursos
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
<node CREATED="1426245897103" ID="ID_361547866" MODIFIED="1430471605733" TEXT="Concurrencia, gesti&#xf3;n, coste de red">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node CREATED="1426245961965" ID="ID_1875311758" MODIFIED="1430471722447">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Proxy:</i>&#160;intermediario entre c/s, filtra peticiones
    </p>
  </body>
</html></richcontent>
<node CREATED="1426245997676" ID="ID_765557306" MODIFIED="1430471728145">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cach&#233;:</i>&#160;memoriza respuestas a peticiones de cliente
    </p>
  </body>
</html></richcontent>
<node CREATED="1426245939065" ID="ID_1338423469" MODIFIED="1445961505951" TEXT="Rendimiento (-carga), rapidez y dispo">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1430471730444" ID="ID_1840166168" MODIFIED="1445961512408">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Reverse proxy:</i>&#160;cuando clientes acceden desde fuera, protege servicios
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426245921258" ID="ID_663717471" MODIFIED="1430472030134">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Peer to peer:</i>&#160;interacciones de 'igual a igual' (pe torrent)
    </p>
  </body>
</html></richcontent>
<node CREATED="1430471821476" ID="ID_462121875" MODIFIED="1445961525896">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Grid Computing:</i>&#160;&#160;proy BOINC (usuarios ofrecen sus recs para computaci&#243;n distrib)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1430471974609" ID="ID_1279861505" MODIFIED="1430471976193" TEXT="Otros">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426246156490" ID="ID_1863651747" MODIFIED="1430472003040">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      C&#243;digo m&#243;vil (s&#8594;c, applets), agente m&#243;vil (pgm se traslada recoge info, spider), cliente ligero (interfaz)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1426245093008" ID="ID_1001003791" MODIFIED="1426245589717" POSITION="right" TEXT="Middleware">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426245099178" ID="ID_874897212" MODIFIED="1445961573120">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Capa sw sobre SO, oculta red subyacente y protocolos&#8594;&#160;&#160;interop y transp
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1430470496825" ID="ID_66504727" MODIFIED="1445962124136">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Permite <u>heterog</u>&#160;(com entre sistemas diferentes) y une partes de apps distribuidas
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1426245224667" ID="ID_998273345" MODIFIED="1445962151778">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Prots de com </b>
    </p>
    <p>
      <b>entre recursos </b>
    </p>
    <p>
      (evol)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426245227196" ID="ID_758287009" MODIFIED="1430470682595">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>RPC:</i>&#160;(Remote Procedure Call) s&#237;ncrono, cliente llama a funci&#243;n de un serv
    </p>
  </body>
</html></richcontent>
<node CREATED="1426245805884" ID="ID_373516163" MODIFIED="1430470724247">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Stub (cliente):</i>&#160;convierte llamada local en remota a serv
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426245284000" ID="ID_62999229" MODIFIED="1430470733183">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Skeleton (serv):</i>&#160;invoca proceso local y devuelve resultado
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426245341397" ID="ID_1213072365" MODIFIED="1430470769118">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Conversaciones:</i>&#160;as&#237;ncrono, relaci&#243;n 'igual a igual'
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426245407482" ID="ID_408715243" MODIFIED="1445961836545">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Mensajer&#237;a ORB:</i>&#160;(Object Req Brokers) intercambio de msjs (primitivas)
    </p>
  </body>
</html></richcontent>
<node CREATED="1430470797891" ID="ID_1389576512" MODIFIED="1445961727401">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Utiliza&#160; <u>MQI</u>&#160;(Message Queue Interf), interfaz cola
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426245426837" ID="ID_743343822" MODIFIED="1445961785592">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>CORBA:</i>&#160;(Common ORB Arch, OMG) msjs/peticiones a objetos (pesado y complejo). Basado en GIOP
    </p>
  </body>
</html></richcontent>
<node CREATED="1445961973086" ID="ID_424799274" MODIFIED="1445963012782">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      C&#8594; Stub&#8594; ORB&#8594; Skeleton&#8594; S
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1426245488606" ID="ID_1145193177" MODIFIED="1445961958382">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>XML-RPC: </i>usa XML para codificar y <u>HTTP</u>&#160;para tx
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1426245517597" ID="ID_1542505657" MODIFIED="1445962079484">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>SOAP:</i>&#160;(Simple Obj Access, W3C)&#160; <u>empaqueta</u>&#160;msjs (sobre) para llamar a m&#233;todos de objs remotos
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1430471183412" ID="ID_1911088861" MODIFIED="1445962023956">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      DCE
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1426244019731" ID="ID_179507451" MODIFIED="1445962018751">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Distrib Computer Environm) est&#225;ndar de facto, incluye servs de com de recursos
    </p>
  </body>
</html></richcontent>
<node CREATED="1430471250876" ID="ID_664735600" MODIFIED="1445962034632">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      RPC, DDS (direct distrib), NTP (horario), Kerberos (seg, autentic), threads
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1426246455426" ID="ID_1974004296" MODIFIED="1445962138775">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tipos
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1430472572481" ID="ID_464208067" MODIFIED="1445962267913">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Message Oriented (MOM)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1430472523045" ID="ID_913848381" MODIFIED="1445962208184">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Estaciones de Mensajer&#237;a:</i>&#160;(EM) solo gestionan&#160; <u>comunic</u>&#160;entre elems (tx msjs)
    </p>
  </body>
</html></richcontent>
<node CREATED="1430472727175" ID="ID_307506691" MODIFIED="1430472739255" TEXT="Fiabilidad seg&#xfa;n SLA">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1430472668237" ID="ID_821466281" MODIFIED="1445962276775">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Nodos deben ser <u>homog</u>&#160;(=protoc y leng)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1430472534179" ID="ID_412660605" MODIFIED="1445962527895">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Motores de Integr:</i>&#160;(Mint, Messg Broker) EM + <u>traducci&#243;n</u>&#160;de msjs y <u>enrutam</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1430472777227" ID="ID_1374806769" MODIFIED="1430472784157" TEXT="Nodos heterog&#xe9;neos">
<icon BUILTIN="button_ok"/>
<node CREATED="1426246637679" ID="ID_394228065" MODIFIED="1426246640601" TEXT="&#xc1;mbito LAN"/>
</node>
</node>
</node>
<node CREATED="1430472546738" ID="ID_1835054817" MODIFIED="1445962386107">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Enterprise Service Bus:</i>&#160;(EBS) tb traduce y enruta, pero a <u>mayor escala</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1430479440063" ID="ID_1920729915" MODIFIED="1445962557958">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Maneja +par&#225;metros (no solo prots) y &#225;mbito <u>WAN</u>&#160;(orgs con sedes, pe Ministerios)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</node>
<node CREATED="1430475290017" ID="ID_1592683611" MODIFIED="1430475295106" POSITION="right" TEXT="Arq C/S">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1430472250019" ID="ID_528016983" MODIFIED="1430475301733" TEXT="HTML din&#xe1;micas">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1430473306935" ID="ID_1663694899" MODIFIED="1430475303362" TEXT="Cliente">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1430473607406" ID="ID_1597110076" MODIFIED="1445962622275">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comps
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1430473315502" ID="ID_217777241" MODIFIED="1445962638224">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Navegador:</i>&#160;UI con apps web (plug-ins: funcs extra)
    </p>
  </body>
</html></richcontent>
<node CREATED="1430473334014" ID="ID_313896512" MODIFIED="1430473378774">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      El 1&#186; fue <u>Lynx</u>&#160;(modo car&#225;cter) y luego <u>Mosaic</u>&#160;(modo gr&#225;fico)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node CREATED="1430473498292" ID="ID_756719872" MODIFIED="1445962659242">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>JVM:</i>&#160;(Java Virtual Machine): ejecuta&#160;<u>applets</u>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1430473540921" ID="ID_931251195" MODIFIED="1445962667476">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Int&#233;rprete de Script:</i>&#160;traduce <u>Javascript</u>, VBScript, JScript (MS)...
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1430473627209" ID="ID_486889581" MODIFIED="1430474047184" TEXT="Javascript 1.5">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1430473743771" ID="ID_1676513683" MODIFIED="1445962734727">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Est&#225;ndar&#160;ECMA262, leng interpretado, basado en <u>objetos</u>&#160;y eventos, embeb en HTML, case sensitive
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1430473631064" ID="ID_1995543028" MODIFIED="1430473633931" TEXT="Applets">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1430474097557" ID="ID_1106620588" MODIFIED="1445963096062">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Remotos (no embeb)&#8594;<u>localiz de c&#243;digo</u>&#160;a ejec en cliente (JVM, zona <u>sandbox</u>)
    </p>
  </body>
</html></richcontent>
<node CREATED="1430474220246" ID="ID_565727619" MODIFIED="1445962804228">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No accede a recs loc ni libs (salvo print/save)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
<node CREATED="1430473637711" ID="ID_1587790365" MODIFIED="1430475303977" TEXT="Servidor">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1430473671361" ID="ID_108009118" MODIFIED="1430473731743" TEXT="Servlets">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1430474612663" ID="ID_1342656052" MODIFIED="1445963414895">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Genera resp HTML <i>(HttpServletResponse)</i>&#160;<u>seg&#250;n</u>&#160;petici&#243;n de <u>cliente</u>&#160;<i>(HttpServletRequest)</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1430474728604" ID="ID_1933188842" MODIFIED="1445962876608">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Req <u>JRE</u>&#160;en serv app (incluye JVM)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1430473723342" ID="ID_1619170491" MODIFIED="1430474808000" TEXT="JSP (Java Server Page)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1430474808292" ID="ID_326384430" MODIFIED="1445962910981">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Evol de Servlet, facilita desarrollo y mantenim (actualiza todos los servlets con un solo cambio)&#160;&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1430473725620" ID="ID_1109501973" MODIFIED="1430474915757" TEXT="ASP (Active Server Page)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1430474917338" ID="ID_1408558078" MODIFIED="1445962937074">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Microsoft (compet de Servlet) solo funcionan sobre IIS&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1430474945810" ID="ID_1367799259" MODIFIED="1445962957208">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      3.0 (interpretado, VBScript, Jscript) o .NET (c&#243;digo CIL)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1430473674490" ID="ID_920432528" MODIFIED="1430473730718" TEXT="PHP">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1430475048552" ID="ID_401310471" MODIFIED="1445963476188">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      v5.6.14, integraci&#243;n sencilla con BD, libre e interpretado
    </p>
  </body>
</html></richcontent>
<node CREATED="1430475126032" ID="ID_1477158255" MODIFIED="1445963022943">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Serv Libre&#8594; <u>XAMPP</u>&#160;(Linux/Win/Mac-Apache-MySQL-PHP-Perl)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1430475305078" ID="ID_979796041" MODIFIED="1430475308777" TEXT="Web Services">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1430475471881" ID="ID_1757239664" MODIFIED="1445963156175">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tecn con prots y est&#225;ndares <u>abiertos</u>&#160;para intercambiar datos entre apps
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1430475495042" ID="ID_1195849634" MODIFIED="1445963187285">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Indep de leng y plat, publican servs ofrecidos, acceso homog, interop
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1445963284048" ID="ID_1936833383" MODIFIED="1445963425785">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>SOA: </i>(Serv Oriented Arch) paradigma de arq para dise&#241;ar y desarrollar s&#170; distrib
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1445963290213" ID="ID_1653899751" MODIFIED="1445963328909">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      UDDI (cat&#225;logo de WS), WSDL (descrip e interfaz) y SOAP (empaq y tx)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
</map>
