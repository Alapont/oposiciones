<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1367749097186" ID="ID_1806998452" MODIFIED="1378048471724" TEXT="SOA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367749162745" ID="ID_870313670" MODIFIED="1518437158570" POSITION="right" TEXT="Principios">
<node CREATED="1367749171893" ID="ID_321669189" MODIFIED="1385569123444" TEXT="Principios gu&#xed;a">
<node CREATED="1367749182992" ID="ID_67349834" MODIFIED="1367749190779" TEXT="Reutilizaci&#xf3;n"/>
<node CREATED="1367749191362" ID="ID_1066401296" MODIFIED="1367749195732" TEXT="Interoperabilidad"/>
<node CREATED="1367749197048" ID="ID_1204163897" MODIFIED="1367749200248" TEXT="Granularidad"/>
<node CREATED="1367749207414" ID="ID_575148391" MODIFIED="1367749212377" TEXT="Modularidad"/>
<node CREATED="1367749213038" ID="ID_1264436195" MODIFIED="1367749216534" TEXT="Composici&#xf3;n"/>
<node CREATED="1381311459078" ID="ID_569579157" MODIFIED="1381311469438" TEXT="Componentizaci&#xf3;n"/>
<node CREATED="1367749218865" ID="ID_1848549296" MODIFIED="1367749235278" TEXT="Conformidad con est&#xe1;ndares"/>
</node>
<node CREATED="1367749238326" ID="ID_1149662649" MODIFIED="1518437160979" TEXT="Principios arquit&#xe9;ctonicos">
<node CREATED="1367749267016" ID="ID_684390153" MODIFIED="1367749273521" TEXT="Encapsulaci&#xf3;n"/>
<node CREATED="1367749274650" ID="ID_1257445540" MODIFIED="1367749281875" TEXT="D&#xe9;bil acoplamiento"/>
<node CREATED="1367749290913" ID="ID_1967610930" MODIFIED="1367749298247" TEXT="Contrato"/>
<node CREATED="1367749298830" ID="ID_58143575" MODIFIED="1367749302186" TEXT="Abstracci&#xf3;n"/>
<node CREATED="1367749302598" ID="ID_1828387150" MODIFIED="1367749323987" TEXT="Autonom&#xed;a"/>
<node CREATED="1367749329391" ID="ID_614686251" MODIFIED="1367749341077" TEXT="Optimizaci&#xf3;n"/>
<node CREATED="1367749343392" ID="ID_933336891" MODIFIED="1367749348339" TEXT="Descubrimiento"/>
</node>
</node>
<node CREATED="1367749368070" ID="ID_1689692186" MODIFIED="1518437159944" POSITION="right" TEXT="Beneficios">
<node CREATED="1367749404288" ID="ID_326823050" MODIFIED="1367749414771" TEXT="Potencia activos preexistentes"/>
<node CREATED="1367749415557" ID="ID_928681632" MODIFIED="1367749421970" TEXT="Facilita la integraci&#xf3;n"/>
<node CREATED="1367749422413" ID="ID_1839180354" MODIFIED="1367749428109" TEXT="Mayor capacidad de respuesta"/>
<node CREATED="1367749428458" ID="ID_1276701856" MODIFIED="1367749435277" TEXT="Reducir costes"/>
</node>
<node CREATED="1367749485796" ID="ID_1433337825" MODIFIED="1518437162184" POSITION="right" TEXT="Dise&#xf1;o">
<node CREATED="1367749548587" ID="ID_1520053694" MODIFIED="1384590120593" TEXT="Inconvenientes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367749493762" ID="ID_1667827916" MODIFIED="1384590121818" TEXT="Tiempos no despreciables">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1367749502800" ID="ID_708304644" MODIFIED="1384590122426" TEXT="Respuesta depende de factores externos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1367749579941" ID="ID_454306700" MODIFIED="1384590127395" TEXT="Pasos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1367749602113" ID="ID_254360686" MODIFIED="1384590123532" TEXT="Identificaci&#xf3;n de servicios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1372178449070" ID="ID_1394760951" MODIFIED="1378047938764" TEXT="Enfoque top-down">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372178460735" ID="ID_1807836749" MODIFIED="1378047942789" TEXT="Enfoque bottom-up">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372178471031" ID="ID_859225828" MODIFIED="1378047947001" TEXT="Enfoque middle-out">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1367749609607" ID="ID_1556791658" MODIFIED="1384590124272" TEXT="An&#xe1;lisis de subsistemas. Especificamos interdependencias y relaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1367749618289" ID="ID_1765285202" MODIFIED="1384590124829" TEXT="Especificaci&#xf3;n de componentes que implementan los servicios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1367749634598" ID="ID_1947977655" MODIFIED="1384590125418" TEXT="Asignaci&#xf3;n de servicios a subsistemas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1367749641767" ID="ID_281523181" MODIFIED="1384590126068" TEXT="Implementaci&#xf3;n del servicio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-5"/>
</node>
</node>
</node>
<node CREATED="1367750423849" ID="ID_1968799956" MODIFIED="1518437163844" POSITION="right" TEXT="Arquitectura de referencia">
<node CREATED="1367750433170" ID="ID_579454066" MODIFIED="1367750454404" TEXT="Recursos"/>
<node CREATED="1367750454940" ID="ID_1399305817" MODIFIED="1367750458406" TEXT="Componentes"/>
<node CREATED="1367750482694" ID="ID_1176645202" MODIFIED="1367750486565" TEXT="Servicios"/>
<node CREATED="1367750491890" ID="ID_1605164664" MODIFIED="1367750499552" TEXT="Composici&#xf3;n de procesos de negocio"/>
<node CREATED="1367750500384" ID="ID_1600382858" MODIFIED="1367750504349" TEXT="Presentaci&#xf3;n"/>
<node CREATED="1367750544883" ID="ID_358048504" MODIFIED="1367750548224" TEXT="Calidad"/>
<node CREATED="1367750548916" ID="ID_982319715" MODIFIED="1367750553972" TEXT="Gobernanza"/>
</node>
<node CREATED="1369740588410" ID="ID_1755475737" MODIFIED="1391363168333" POSITION="right" TEXT="Servicios Web">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369740603258" ID="ID_1135450280" MODIFIED="1518437165513" TEXT="Previos: RPCs, CORBA, DCOM, RMI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369948457713" ID="ID_708977302" MODIFIED="1376986224193" TEXT="Tecnolog&#xed;as Java">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369948465984" ID="ID_1057010095" MODIFIED="1376986218017" TEXT="RMI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369948468632" ID="ID_1638462315" MODIFIED="1376986220049" TEXT="JINI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369948471989" ID="ID_1973406034" MODIFIED="1376986226481" TEXT="EJB">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1369948478582" ID="ID_791166717" MODIFIED="1385569418286" TEXT="DCOM (Distributed Component Objtect Model)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384432719070" ID="ID_249631928" MODIFIED="1384432730725" TEXT="Introducida por Microsoft">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369948775043" ID="ID_1530806490" MODIFIED="1384432773934" TEXT="Basada en DCE/RPC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369948781756" ID="ID_1922840890" MODIFIED="1376986229609" TEXT="Sustituida por .NET">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1369948480996" ID="ID_539476021" MODIFIED="1518437167404" TEXT="CORBA (OMG)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384432641371" ID="ID_1093565791" MODIFIED="1384432679987" TEXT="Definido por OMG (Object Management Group)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369948829036" ID="ID_654470677" MODIFIED="1377854257350" TEXT="Objetos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369948836776" ID="ID_286429546" MODIFIED="1369948894777">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="CORBA1.png"/>
  </body>
</html></richcontent>
<node CREATED="1369948910156" ID="ID_1268206219" MODIFIED="1381997268518" TEXT="Facilidades comunes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369948921018" ID="ID_1490074980" MODIFIED="1381997271904" TEXT="Interfaces de dominio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369948926778" ID="ID_693757242" MODIFIED="1381997275211" TEXT="Servicios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1369948997162" ID="ID_1028071290" MODIFIED="1369949004929">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="CORBA2.png"/>
  </body>
</html></richcontent>
<node CREATED="1369948262193" ID="ID_511807989" MODIFIED="1382975811834" TEXT="Middleware ORB">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369948351570" ID="ID_1394978259" MODIFIED="1382975815422" TEXT="Lenguaje IDL (Interface Description Language)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369948381914" ID="ID_1463747329" MODIFIED="1382975816530" TEXT="Cliente: Stub">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369948390316" ID="ID_1657023801" MODIFIED="1382975817435" TEXT="Servidor: Skeleton">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384433019163" ID="ID_540408531" MODIFIED="1384433034202" TEXT="Protocolo IIOP (sobre TCP/IP)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1369740625322" ID="ID_137825248" MODIFIED="1518437174287" TEXT="SOAP (Simple Object Access Protocol)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375519937444" ID="ID_22657263" MODIFIED="1378048576509" TEXT="Propuesto por Microsoft">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1391710681804" ID="ID_1950225405" MODIFIED="1391710835533" TEXT="Caracter&#xed;sticas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1391710691215" ID="ID_1468183439" MODIFIED="1391710707081" TEXT="Extensibilidad">
<node CREATED="1391710707083" ID="ID_1856508810" MODIFIED="1391710756755" TEXT="Puede implementar seguridad (WS-security) y enrutamiento (WS-routing)"/>
</node>
<node CREATED="1391710775574" ID="ID_1422501382" MODIFIED="1391710779174" TEXT="Neutralidad">
<node CREATED="1391710779176" ID="ID_1897229501" MODIFIED="1391710790715" TEXT="Puede ser usado sobre cualquier protocolo de transporte"/>
</node>
<node CREATED="1391710792154" ID="ID_1841134373" MODIFIED="1391710828687" TEXT="Independencia">
<node CREATED="1391710795592" ID="ID_1734353004" MODIFIED="1391710813339" TEXT="Puede ser programado en cualquier lenguaje"/>
</node>
</node>
<node CREATED="1369740692065" ID="ID_436808658" MODIFIED="1378048578428" TEXT="sobre SOAP: envelope + body">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369740700426" ID="ID_328084216" MODIFIED="1391708924067" TEXT="Implementa RPCs">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369740739474" ID="ID_1081571356" MODIFIED="1369740753685" TEXT="Petici&#xf3;n/respuesta"/>
</node>
</node>
<node CREATED="1369740775074" ID="ID_332402973" MODIFIED="1518437210019" TEXT="WSDL (Web Services Description Language)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369740808681" ID="ID_847736614" MODIFIED="1384433338632" TEXT="El objetivo es describir interfaz del servicio Web: operaciones, datos enviados y recibidos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375519913792" ID="ID_1899704279" MODIFIED="1382975845827" TEXT="Expone servicios gen&#xe9;ricos SOAP, puertos, enlaces y mensajes usados por cualquier cliente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375520006811" ID="ID_272414452" MODIFIED="1382975847543" TEXT="No contiene informaci&#xf3;n espec&#xed;fica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1372406478134" ID="ID_1303176948" MODIFIED="1384433306342" TEXT="Es un documento XML">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384676672198" ID="ID_80409372" MODIFIED="1384676713524" TEXT="Ultima versi&#xf3;n 2.0">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1369740834322" ID="ID_882454257" MODIFIED="1518437175602" TEXT="UDDI (Universal Description Discovery and Integration)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369740842142" ID="ID_479187884" MODIFIED="1378048570300" TEXT="Directorio de servicios descritos mediante WDSL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369740920906" ID="ID_1644989078" MODIFIED="1384676718126" TEXT="Servicio de p&#xe1;ginas amarillas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384676718803" ID="ID_1711672015" MODIFIED="1384676728056" TEXT="Ultima versi&#xf3;n 3.0">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1376985827226" ID="ID_709038773" MODIFIED="1518437211119" TEXT="MIP (Message Passing Interface)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376985862490" ID="ID_491606016" MODIFIED="1384433444756" TEXT="Librer&#xed;a para programaci&#xf3;n por paso de mensajes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1391363544483" ID="ID_249449995" MODIFIED="1391709723095" TEXT="Especificaciones de Web Service">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1391363365278" ID="ID_1892255447" MODIFIED="1518437185195" TEXT="WS- Addressing">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1391363384203" ID="ID_856135347" MODIFIED="1391363417518" TEXT="Informaci&#xf3;n relativa a la entrega de un mensaje a un web service"/>
<node CREATED="1391363419506" ID="ID_920883746" MODIFIED="1391363438714" TEXT="EPR (Endpoint Reference)">
<node CREATED="1391363447869" ID="ID_1676468502" MODIFIED="1391363461194" TEXT="Punto final de fuente"/>
<node CREATED="1391363461452" ID="ID_1581866341" MODIFIED="1391363471545" TEXT="Punto final para la respuesta"/>
<node CREATED="1391363471950" ID="ID_1470143874" MODIFIED="1391709746248" TEXT="Punto final para el fallo"/>
</node>
<node CREATED="1391711616109" ID="ID_648988066" MODIFIED="1391711836644" TEXT="MIH (Message Information Headers)">
<node CREATED="1391711723239" ID="ID_1009726433" MODIFIED="1391711737692" TEXT="Informaci&#xf3;n que caracteriza al mensaje y no se puede modificar"/>
</node>
</node>
<node CREATED="1391363519561" ID="ID_285117240" MODIFIED="1518437186434" TEXT="WS- Discovery">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1391363527801" ID="ID_815067445" MODIFIED="1391363601169" TEXT="Protocolo multicast para descubrir servicios Web en una red"/>
</node>
<node CREATED="1379262874744" ID="ID_125566744" MODIFIED="1518437188058" TEXT="Perfiles de WS- Interoperabilidad (WS-I)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379262938275" ID="ID_669239529" MODIFIED="1379263051854" TEXT="WS-I Basic Profile">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379262980700" ID="ID_1853867909" MODIFIED="1385569523639" TEXT="WS-I Basic Security Profile">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379263033227" ID="ID_1802002877" MODIFIED="1384435765071" TEXT="Extensi&#xf3;n del anterior">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384435677077" ID="ID_1079902697" MODIFIED="1384435766647" TEXT="Interoperabilidad en materia de seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379262998850" ID="ID_1739002736" MODIFIED="1384630813884" TEXT="WS-I Reliable Secure Profile">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384435709047" ID="ID_275607392" MODIFIED="1384435767535" TEXT="WS- Reliable Messaging">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384435717823" ID="ID_202051563" MODIFIED="1384435768327" TEXT="WS- Reliable Exchange">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384435744305" ID="ID_1179859584" MODIFIED="1384435769079" TEXT="WS - Secure Conversation">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1391363633453" ID="ID_680660796" MODIFIED="1518437191445" TEXT="WS - Security">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1391363643796" ID="ID_1990163830" MODIFIED="1391363701920" TEXT="Extensi&#xf3;n de SOAP para proporcionar informaci&#xf3;n a los Web Services">
<node CREATED="1391363668574" ID="ID_442237973" MODIFIED="1391363713609" TEXT="Como firmar los mensajes SOAP para asegurar la integridad"/>
<node CREATED="1391363714381" ID="ID_1178864719" MODIFIED="1391363742785" TEXT="Como encriptar los mensajes SOAP para asegurar la confidencialidad"/>
<node CREATED="1391363743045" ID="ID_1103996939" MODIFIED="1391363775396" TEXT="Como adjuntar los tokens de seguridad para la identidad del remitente"/>
</node>
<node CREATED="1391363798535" ID="ID_1230232919" MODIFIED="1391363803738" TEXT="Medios">
<node CREATED="1391363812215" ID="ID_1198912198" MODIFIED="1391363823163" TEXT="Certificados X.509"/>
<node CREATED="1391363823723" ID="ID_265722250" MODIFIED="1391363832396" TEXT="Tickets Kerberos"/>
<node CREATED="1391363832768" ID="ID_1461412607" MODIFIED="1391363857913" TEXT="Credenciales Usuario/passwords"/>
<node CREATED="1391363858253" ID="ID_1069157781" MODIFIED="1391363862956" TEXT="Listas SAM"/>
</node>
</node>
</node>
<node CREATED="1375520981292" ID="ID_1834971466" MODIFIED="1518437192617" TEXT="Interacciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375521016645" ID="ID_1306921972" MODIFIED="1384435013545" TEXT="Orquestaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384435013546" ID="ID_801834967" MODIFIED="1384435028148" TEXT="Controlado por un participante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384435068158" ID="ID_579129432" MODIFIED="1384435101416" TEXT="Solo la entidad que controla conoce el flujo de control y formaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375521007174" ID="ID_1637045238" MODIFIED="1384435118888" TEXT="Coreograf&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384434962677" ID="ID_1939027187" MODIFIED="1384435003166" TEXT="No es controlado por una sola entidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384434988049" ID="ID_1598849377" MODIFIED="1384435004862" TEXT="Puede verse como un proceso p&#xfa;blico y no ejecutable">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384435134936" ID="ID_56184531" MODIFIED="1384435148477" TEXT="Todos los participantes conocen el flujo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1369830865985" ID="ID_1058823584" MODIFIED="1518437193810" POSITION="right" TEXT="ESB (Enterprise Service Bus)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1369830912241" ID="ID_300570054" MODIFIED="1382989131910" TEXT="Compone procesos desde servicios SOA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369830928393" ID="ID_1232832477" MODIFIED="1378048299437" TEXT="No maneja actividades humanas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369831005225" ID="ID_874639823" MODIFIED="1378048301590" TEXT="S&#xf3;lo maneja tareas automatizadas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369831020489" ID="ID_415199377" MODIFIED="1382989142073" TEXT="Orientado a servicios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375520551865" ID="ID_1384512053" MODIFIED="1518437194743" POSITION="right" TEXT="Servicios de Web Sem&#xe1;ntica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375520035563" ID="ID_406738693" MODIFIED="1376986297076" TEXT="WSML (Web Services Modelling Language)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375520072748" ID="ID_131887627" MODIFIED="1384433995412" TEXT="Enlaza los mensajes gen&#xe9;ricos con m&#xe9;todos espec&#xed;ficos ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375520782144" ID="ID_1030619224" MODIFIED="1378048348327" TEXT="Describe el contenido sem&#xe1;ntico de los datos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375520833696" ID="ID_1809032293" MODIFIED="1379245288445" TEXT="Proporciona el significado de los datos descritos por WSDL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375521124304" ID="ID_972318256" MODIFIED="1384594470604" TEXT="Lenguajes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375521130184" ID="ID_373736033" MODIFIED="1378048364614" TEXT="OIL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375521133693" ID="ID_196565902" MODIFIED="1378048366408" TEXT="DAML">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375521137134" ID="ID_534134547" MODIFIED="1378048367297" TEXT="OWL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375521143118" ID="ID_1242576845" MODIFIED="1378048367968" TEXT="RDF">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375521145937" ID="ID_991533730" MODIFIED="1378048368576" TEXT="WSML">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375521158564" ID="ID_265789951" MODIFIED="1378048369497" TEXT="WDSL-S">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1375520571277" ID="ID_1995429319" MODIFIED="1384594486359" TEXT="WSMO (Web Service Modelling Ontology)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375519663376" ID="ID_1675723908" MODIFIED="1382980605782" TEXT="Puede usarse WSML (Web Service Modelling Language) para WSMO">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382980684120" ID="ID_648249664" MODIFIED="1382981189073" TEXT="Descrito en t&#xe9;rminos de MOF (Meta-Object Facility)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Define una estructura de metadatos
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1382980702344" ID="ID_614676628" MODIFIED="1384594496492" TEXT="Capas">
<node CREATED="1382980749050" ID="ID_1707352102" MODIFIED="1382980978463" TEXT="Capa de meta-meta-modelo">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Define la estructura y sem&#xe1;ntica del meta-modelo
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382980742729" ID="ID_1702695482" MODIFIED="1382980764491" TEXT="Capa de meta-modelo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382980735962" ID="ID_393628230" MODIFIED="1382980763743" TEXT="Capa de modelo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1382980724643" ID="ID_447603249" MODIFIED="1382981074504" TEXT="Capa de informaci&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Datos descritos por las ontolog&#xed;as e intercambiados por los servicios Web
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1376389210064" ID="ID_1416231885" MODIFIED="1382981213882" TEXT="Componentes (capa del modelo)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375520581397" ID="ID_282478696" MODIFIED="1382980608588" TEXT="Metas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375520591937" ID="ID_114039661" LINK="Inteligencia%20Artificial.mm" MODIFIED="1382980612587" TEXT="Ontolog&#xed;as">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375520596207" ID="ID_746508636" MODIFIED="1382980615040" TEXT="Mediadores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1375520601829" ID="ID_1300166575" MODIFIED="1382980618122" TEXT="Webservices">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1367749953349" ID="ID_1599421622" LINK="Workflow.%20BPM.mm" MODIFIED="1384432013567" POSITION="right" TEXT="Procesos de negocio (BPM)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</map>
