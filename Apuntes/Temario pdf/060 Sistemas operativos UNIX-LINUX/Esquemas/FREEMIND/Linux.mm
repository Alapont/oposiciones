<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1368292852370" ID="ID_1735962176" MODIFIED="1556793493889" TEXT="Sistema Operativo Linux">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1556791078821" ID="ID_1863174849" MODIFIED="1556804673048" POSITION="right" TEXT="Compuesto por">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1556791092436" ID="ID_560293924" MODIFIED="1556812453205" TEXT="Linux (n&#xfa;cleo, kernel)">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1376322389599" ID="ID_1281004990" MODIFIED="1556791347691" TEXT="Influenciado por">
<node CREATED="1376322395065" ID="ID_1925003844" MODIFIED="1556796150918" TEXT="UNIX y sus variantes (&quot;UNIX-like&quot;)."/>
<node CREATED="1376322413122" ID="ID_1795825929" MODIFIED="1556796148965" TEXT="MINIX (por Tanembaum)."/>
</node>
<node CREATED="1556791358932" ID="ID_1204965855" MODIFIED="1556792982458" TEXT="Desarrollado por">
<node CREATED="1556792965124" ID="ID_59417860" MODIFIED="1556796147157" TEXT="Iniciado por Linus Torvalds en 1996."/>
<node CREATED="1556792983653" ID="ID_355596507" MODIFIED="1556796145461" TEXT="Hoy en d&#xed;a miles de colaboradores."/>
</node>
</node>
<node CREATED="1556791188992" ID="ID_1863218783" MODIFIED="1556812453205" TEXT="Componentes b&#xe1;sicos">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1556794096531" ID="ID_943245678" MODIFIED="1556796141605" TEXT="Bibliotecas y herramientas b&#xe1;sicas esperadas en un &quot;UNIX-like&quot;."/>
<node CREATED="1556793639974" ID="ID_792326173" MODIFIED="1556796139749" TEXT="Desarrollados por GNU."/>
<node CREATED="1556793688932" ID="ID_1338005363" MODIFIED="1556794683859" TEXT="glibc, coreutils, gcc, binutils, bash, grub, gzip, grep, etc"/>
</node>
<node CREATED="1556791225398" ID="ID_110489099" MODIFIED="1556812432215" TEXT="Miles de otras aplicaciones y bibliotecas: nativas, portadas o comerciales (Firefox, Chrome, Libreoffice, Apache, MySql, VLC, Gimp, X11, Wayland, Skype, Dropbox, Spotify, virtualizaci&#xf3;n, etc)."/>
<node CREATED="1556807527145" ID="ID_131051353" MODIFIED="1556812453194" TEXT="Si es un sistema de escritorio&#xa;(no servidor), hay adem&#xe1;s:">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1556805517316" ID="ID_844249821" MODIFIED="1556805946306" TEXT="Sistema de ventanas">
<node CREATED="1556805804698" ID="ID_933191788" MODIFIED="1556806476931" TEXT="Se comunica con la tarjeta gr&#xe1;fica, rat&#xf3;n, etc, y ofrece una API de bajo nivel para que las aplicaciones puedan mostrar ventanas y usar otras funcionalidades gr&#xe1;ficas."/>
<node CREATED="1556805538132" ID="ID_703812943" MODIFIED="1556805631104" TEXT="X.Org (estable)."/>
<node CREATED="1556805578452" ID="ID_916416424" MODIFIED="1556805629088" TEXT="Wayland (sucesor; a&#xfa;n no muy extendido)."/>
</node>
<node CREATED="1368339325135" ID="ID_1311622029" MODIFIED="1556806951172" TEXT="Entornos de escritorio">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1556806063811" ID="ID_1367512768" MODIFIED="1556806605891" TEXT="Ofrecen al usuario un entorno gr&#xe1;fico c&#xf3;modo y amigable, accesible por rat&#xf3;n o teclado, con escritorios, men&#xfa;s, iconos, accesos a aplicaciones, explorador de ficheros, configuraci&#xf3;n gr&#xe1;fica de dispositivos, etc."/>
<node CREATED="1368339334362" ID="ID_570613677" MODIFIED="1556805421999" TEXT="KDE Plasma.">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1556805244834" ID="ID_917630265" MODIFIED="1556805423519" TEXT="GNOME."/>
<node CREATED="1556805313939" ID="ID_366660855" MODIFIED="1556806629892" TEXT="Otros entornos de escritorio: Cinnamon, Xfce, LXDE, Mate."/>
<node CREATED="1556806630968" ID="ID_1211507469" MODIFIED="1556806940068" TEXT="Alternativa: gestores de ventanas, que s&#xf3;lo manejan ventanas. Menos funcionalidad pero menor uso de recursos. Ejemplos: xfwm, i3, openbox, dwm, etc."/>
</node>
</node>
<node CREATED="1556791302623" ID="ID_1292354628" MODIFIED="1556812453194" TEXT="Distribuciones">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1556795140712" ID="ID_1763270323" MODIFIED="1556803709116" TEXT="Debian, Ubuntu, RedHat, centOS, openSUSE, Mint, Arch, Manjaro, Gentoo, Clear, Kali, etc"/>
<node CREATED="1556795047048" ID="ID_1812631081" MODIFIED="1556796027027" TEXT="Son &#xab;editores&#xbb;: seleccionan, integran y distribuyen los diferentes componentes (kernel, herramientas, aplicaciones)."/>
<node CREATED="1556795107832" ID="ID_1198857894" MODIFIED="1556796046276" TEXT="Por empresas (ej, RedHat, Canonical/Ubuntu) u organizaciones (Debian, Arch)."/>
<node CREATED="1556795697897" ID="ID_127466345" MODIFIED="1556795915077" TEXT="&#xbf;Por qu&#xe9; tantas? Diferentes filosof&#xed;as">
<node CREATED="1556795721689" ID="ID_998079117" MODIFIED="1556796057702" TEXT="S&#xf3;lo software libre."/>
<node CREATED="1556795735481" ID="ID_470282656" MODIFIED="1556796059173" TEXT="Enfoque comercial."/>
<node CREATED="1556795740217" ID="ID_270792477" MODIFIED="1556796060581" TEXT="Preferencia por estabilidad."/>
<node CREATED="1556795759288" ID="ID_59019920" MODIFIED="1556796061973" TEXT="Preferencia por innovaci&#xf3;n."/>
<node CREATED="1556795766489" ID="ID_1098237677" MODIFIED="1556796063685" TEXT="Basado en comunidades abiertas."/>
<node CREATED="1556795791017" ID="ID_1387517234" MODIFIED="1556796064981" TEXT="Controlado por una empresa."/>
<node CREATED="1556795813321" ID="ID_22475206" MODIFIED="1556796066293" TEXT="&#xc9;nfasis en seguridad."/>
<node CREATED="1556795801049" ID="ID_882312046" MODIFIED="1556796135446" TEXT="etc."/>
</node>
<node CREATED="1556796954988" ID="ID_1026353530" MODIFIED="1556804179341" TEXT="Otros &#xab;usos&#xbb; (t&#xe9;cnicamente no distribuciones): Android, WearOS, ChromeOS, Tesla, SpaceX, otros sistemas embebidos, etc."/>
</node>
</node>
<node CREATED="1368293079473" ID="ID_461772464" MODIFIED="1556796355174" POSITION="right" TEXT="Principios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368293087281" ID="ID_24482489" MODIFIED="1556804738639" TEXT="C&#xf3;digo abierto."/>
<node CREATED="1556796382890" ID="ID_1417913446" MODIFIED="1556804740014" TEXT="Gratuito."/>
<node CREATED="1368293103824" ID="ID_1440468774" MODIFIED="1556804742159" TEXT="Abierto a programadores que quieran colaborar."/>
<node CREATED="1368293117759" ID="ID_235226418" MODIFIED="1556804745455" TEXT="Multiplataforma (x86 64 y 32 bits, ARM, POWER, MIPS, System Z)."/>
<node CREATED="1368293122936" ID="ID_1509956608" MODIFIED="1556804291741" TEXT="Prop&#xf3;sito general (sistemas embebidos, de escritorio, servidores, etc)."/>
</node>
<node CREATED="1556808439339" ID="ID_1845589165" MODIFIED="1556812133672" POSITION="right" TEXT="Jerarqu&#xed;a de ficheros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1556809517694" ID="ID_1973812062" MODIFIED="1556810309267" TEXT="Un &#xfa;nico sistema de ficheros: todo &#xab;cuelga&#xbb; de la ra&#xed;z, &#xab;/&#xbb;."/>
<node CREATED="1556810313920" ID="ID_1242261769" MODIFIED="1556810354683" TEXT="Otros sistemas de ficheros (ej: disco externo) se montan en un directorio."/>
<node CREATED="1556809642382" ID="ID_84306147" MODIFIED="1556810113786" TEXT="/sys, /dev y /proc: ficheros virtuales para comunicarse con el kernel y el hardware"/>
<node CREATED="1556809712495" ID="ID_1941218968" MODIFIED="1556809728954" TEXT="/bin y /usr/bin: comandos de usuario"/>
<node CREATED="1556809730446" ID="ID_851854774" MODIFIED="1556809747019" TEXT="/sbin y /usr/sbin: comandos para root"/>
<node CREATED="1556812359508" ID="ID_1780262860" MODIFIED="1556812370769" TEXT="/lib y /usr/lib: bibliotecas"/>
<node CREATED="1556809775774" ID="ID_1534582577" MODIFIED="1556809825082" TEXT="/usr: miscel&#xe1;nea (programas, recursos, docs, etc)"/>
<node CREATED="1556809852622" ID="ID_1414509741" MODIFIED="1556809960970" TEXT="/var y /run: ficheros creados durante ejecuci&#xf3;n de servicios del sistema"/>
<node CREATED="1556809963423" ID="ID_286408906" MODIFIED="1556810382523" TEXT="/tmp: para ficheros temporales"/>
<node CREATED="1556809993343" ID="ID_1105143185" MODIFIED="1556810022731" TEXT="/home/usuario: directorio reservado a cada usuario"/>
<node CREATED="1556810033551" ID="ID_1725449961" MODIFIED="1556810065867" TEXT="/etc: ficheros de configuraci&#xf3;n del sistema"/>
<node CREATED="1556810076815" ID="ID_1502994990" MODIFIED="1556810088778" TEXT="/boot: ficheros necesarios durante el arranque del sistema"/>
<node CREATED="1556810195376" ID="ID_584939592" MODIFIED="1556810216955" TEXT="/media, /mnt: directorios donde montar otros sistemas de ficheros"/>
</node>
<node CREATED="1556812956981" ID="ID_1188994608" MODIFIED="1556812975290" POSITION="right" TEXT="Sistemas de ficheros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1556812983222" ID="ID_1221467789" MODIFIED="1556813099890" TEXT="Formatos de organizaci&#xf3;n de los ficheros en un disco y sus metadatos"/>
<node CREATED="1556813102357" ID="ID_1097164699" MODIFIED="1556814326773" TEXT="ext4 (m&#xe1;s frecuente con diferencia)"/>
<node CREATED="1556813550215" ID="ID_1968373323" MODIFIED="1556813562818" TEXT="XFS (similar a ext4, optimizado para paralelismo)"/>
<node CREATED="1556813162966" ID="ID_998675767" MODIFIED="1556813256610" TEXT="ZFS (de &#xab;nueva generaci&#xf3;n&#xbb;, gran uso de recursos, poco extendido)"/>
<node CREATED="1556813258023" ID="ID_694896507" MODIFIED="1556813300659" TEXT="btrfs (similar a ZFS)"/>
<node CREATED="1556813681368" ID="ID_777232737" MODIFIED="1556813720931" TEXT="Pueden montarse sistemas de ficheros de otros SSOO, ej: NTFS, HFS."/>
</node>
<node CREATED="1556807747642" ID="ID_1623113890" MODIFIED="1556812082944" POSITION="left" TEXT="Administraci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1556808231818" ID="ID_776853272" MODIFIED="1556808313111" TEXT="Hay algunas herramientas gr&#xe1;ficas pero t&#xed;picamente por l&#xed;nea de comandos"/>
<node CREATED="1556808787404" ID="ID_1323554178" MODIFIED="1556812125616" TEXT="Filosof&#xed;a UNIX: para realizar tareas complejas se combinan herramientas simples.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1556808960844" ID="ID_1724457359" MODIFIED="1556809001656" TEXT="Conectando entradas con salidas con &#xab;|&#xbb;: &#xab;ls | grep txt&#xbb;."/>
<node CREATED="1556809003324" ID="ID_901621980" MODIFIED="1556809021880" TEXT="Mediante scripts (ficheros con secuencias de comandos)."/>
<node CREATED="1556809094860" ID="ID_1664802554" MODIFIED="1556809124809" TEXT="Int&#xe9;rpretes de comandos pueden programarse (for, while, if, etc)."/>
</node>
<node CREATED="1556808254411" ID="ID_106625258" MODIFIED="1556812120912" TEXT="Niveles de privilegio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1556808274330" ID="ID_475156268" MODIFIED="1556808278982" TEXT="root (administrador)"/>
<node CREATED="1556808281914" ID="ID_1102732136" MODIFIED="1556808285607" TEXT="usuarios normales"/>
<node CREATED="1556808343163" ID="ID_363887703" MODIFIED="1556808360215" TEXT="Ejecutar comandos como root: su o sudo."/>
</node>
<node CREATED="1556810617728" ID="ID_1418670449" MODIFIED="1556812094825" TEXT="Secuencia de arranque">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1556810623600" ID="ID_1322303892" MODIFIED="1556810652492" TEXT="grub: iniciado por la BIOS, carga el kernel en memoria y lo ejecuta"/>
<node CREATED="1556810666273" ID="ID_1375068560" MODIFIED="1556810710963" TEXT="systemd o init: primer proceso en ejecutarse;&#xa;supervisa arranque y parada del resto de servicios"/>
<node CREATED="1556810737489" ID="ID_1894648314" MODIFIED="1556810751372" TEXT="Se arranca un daemon (demonio) o servidor para cada prop&#xf3;sito">
<node CREATED="1556810754576" ID="ID_154277801" MODIFIED="1556810760956" TEXT="apache: servidor web"/>
<node CREATED="1556810762881" ID="ID_209553122" MODIFIED="1556810777068" TEXT="sshd: administraci&#xf3;n remota por l&#xed;nea de comandos"/>
<node CREATED="1556810815713" ID="ID_1992905043" MODIFIED="1556810822476" TEXT="syslog: logs del sistema"/>
<node CREATED="1556810879137" ID="ID_456930541" MODIFIED="1556810883501" TEXT="exim: servidor correo"/>
<node CREATED="1556810898513" ID="ID_319144308" MODIFIED="1556810905548" TEXT="cron: ejecuciones peri&#xf3;dicas"/>
<node CREATED="1556810915168" ID="ID_59943693" MODIFIED="1556810940540" TEXT="udev: inicio de otros programas al activar hardware"/>
<node CREATED="1556810993840" ID="ID_1392291828" MODIFIED="1556811979678" TEXT="getty, login: acceso usuarios por consola">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1556811022049" ID="ID_1347475598" MODIFIED="1556811981862" TEXT="display managers: acceso usuarios gr&#xe1;ficamente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1556811180145" ID="ID_199371623" MODIFIED="1556811198876" TEXT="NetworkManager: gesti&#xf3;n de redes y dispositivos de red"/>
<node CREATED="1556811808610" ID="ID_1879528741" MODIFIED="1556811825374" TEXT="inetd, xinetd: servidores que escuchan por TCP o UDP."/>
<node CREATED="1556810959857" ID="ID_1116611321" MODIFIED="1556810961260" TEXT="etc."/>
</node>
<node CREATED="1556812040179" ID="ID_1902646328" MODIFIED="1556812070495" TEXT="Finalmente el usuario entra por consola local, display manager gr&#xe1;fico, o remotamente por ssh"/>
</node>
<node CREATED="1556808376907" ID="ID_1726507707" MODIFIED="1556812097264" TEXT="Comandos frecuentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1556808387595" ID="ID_86465594" MODIFIED="1556808545078" TEXT="cd: cambiar a directorio"/>
<node CREATED="1556808401723" ID="ID_1875978717" MODIFIED="1556808405094" TEXT="mkdir: crear directorio"/>
<node CREATED="1556808535292" ID="ID_1801818636" MODIFIED="1556808539559" TEXT="rmdir: borrar directorio"/>
<node CREATED="1556808406123" ID="ID_1912739579" MODIFIED="1556808412742" TEXT="ls: listar directorio"/>
<node CREATED="1556808494368" ID="ID_1980100512" MODIFIED="1556808501366" TEXT="mv: mover directorio o fichero"/>
<node CREATED="1556808418091" ID="ID_1525122411" MODIFIED="1556808424103" TEXT="rm: borrar fichero"/>
<node CREATED="1556808606587" ID="ID_1019292085" MODIFIED="1556808626759" TEXT="grep: buscar cadenas en texto"/>
<node CREATED="1556808629372" ID="ID_1471381476" MODIFIED="1556808645175" TEXT="find: buscar ficheros "/>
<node CREATED="1556808658876" ID="ID_1038676269" MODIFIED="1556808670743" TEXT="less, more: paginar texto en pantalla"/>
<node CREATED="1556808694892" ID="ID_994067974" MODIFIED="1556808703831" TEXT="sort: ordenar cadenas de texto"/>
<node CREATED="1556808738076" ID="ID_278443300" MODIFIED="1556808760311" TEXT="sed, awk: lenguajes de script para manipular texto"/>
<node CREATED="1556809197406" ID="ID_1892740369" MODIFIED="1556809200217" TEXT="man, info: ayuda"/>
<node CREATED="1556809225949" ID="ID_415362915" MODIFIED="1556809239544" TEXT="top, ps, kill: gestionar procesos"/>
<node CREATED="1556809254093" ID="ID_280946224" MODIFIED="1556809257944" TEXT="chown: cambiar propietario"/>
<node CREATED="1556809262590" ID="ID_363631763" MODIFIED="1556809270440" TEXT="chmod: cambiar permisos de acceso"/>
<node CREATED="1556809290381" ID="ID_253222717" MODIFIED="1556809302138" TEXT="ln: crear enlaces a otros ficheros"/>
<node CREATED="1556809343309" ID="ID_883462036" MODIFIED="1556809481179" TEXT="tar, gzip, xz, zip: crear vol&#xfa;menes y comprimir"/>
<node CREATED="1556810138496" ID="ID_1240658379" MODIFIED="1556810189499" TEXT="mount, umount: conectar un sistema de ficheros bajo un directorio"/>
<node CREATED="1556811153538" ID="ID_462903424" MODIFIED="1556811158493" TEXT="ip: configuraci&#xf3;n de red"/>
<node CREATED="1556812263076" ID="ID_24535324" MODIFIED="1556812290063" TEXT="ping: comprobar si otro sistema es accesible por red"/>
<node CREATED="1556812299588" ID="ID_208361238" MODIFIED="1556812309616" TEXT="ssh: ejecutar comandos remotamente"/>
<node CREATED="1556811882675" ID="ID_435099690" MODIFIED="1556811893854" TEXT="rsync: copiar directorios a trav&#xe9;s de red"/>
<node CREATED="1556812191108" ID="ID_666870238" MODIFIED="1556812209182" TEXT="df: espacio ocupado y libre en disco"/>
<node CREATED="1556812197140" ID="ID_177671031" MODIFIED="1556812203408" TEXT="free: memoria ocupada y libre"/>
<node CREATED="1556808433707" ID="ID_1079655992" MODIFIED="1556808438678" TEXT="Editores de texto">
<node CREATED="1556808443579" ID="ID_1012861061" MODIFIED="1556808450503" TEXT="vim"/>
<node CREATED="1556808462458" ID="ID_1268361798" MODIFIED="1556808464215" TEXT="emacs"/>
<node CREATED="1556808465179" ID="ID_66434140" MODIFIED="1556808465990" TEXT="nano"/>
</node>
<node CREATED="1556812218164" ID="ID_308556771" MODIFIED="1556812226144" TEXT="shells (int&#xe9;rpretes de comandos)">
<node CREATED="1556812232260" ID="ID_1739576680" MODIFIED="1556812238719" TEXT="bash (m&#xe1;s popular)"/>
<node CREATED="1556812240020" ID="ID_1878053298" MODIFIED="1556812241504" TEXT="tcsh"/>
</node>
</node>
<node CREATED="1556811220561" ID="ID_1681000264" MODIFIED="1556812138497" TEXT="Ficheros de configuraci&#xf3;n notables">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1556811284753" ID="ID_1100287482" MODIFIED="1556811309965" TEXT="/etc/passwd: usuarios del sistema"/>
<node CREATED="1556811311569" ID="ID_720771518" MODIFIED="1556811326076" TEXT="/etc/shadow: hashes de contrase&#xf1;as de usuarios"/>
<node CREATED="1556811522881" ID="ID_331775431" MODIFIED="1556811535693" TEXT="/etc/group: grupos y sus usuarios"/>
<node CREATED="1556811339265" ID="ID_1442060924" MODIFIED="1556811351789" TEXT="/etc/hostname: nombre del equipo"/>
<node CREATED="1556811357841" ID="ID_661518715" MODIFIED="1556811413741" TEXT="/etc/hosts: nombres de equipos y sus IPs, que se resuelven localmente"/>
<node CREATED="1556811572690" ID="ID_858132535" MODIFIED="1556811594157" TEXT="/etc/resolv.conf: servidores DNS para resolver IPs"/>
<node CREATED="1556811464498" ID="ID_1945698419" MODIFIED="1556811503485" TEXT="/etc/cron* (varios): comandos a ejecutar peri&#xf3;dicamente"/>
<node CREATED="1556811651714" ID="ID_248310869" MODIFIED="1556811659949" TEXT="/etc/ssh: configuraci&#xf3;n de sshd y ssh"/>
<node CREATED="1556811668226" ID="ID_330485880" MODIFIED="1556811678269" TEXT="/etc/sudoers: qu&#xe9; usuarios pueden ejecutar sudo"/>
<node CREATED="1556811716291" ID="ID_665836305" MODIFIED="1556811739118" TEXT="/etc/systemd: qu&#xe9; otros programas arranca systemd durante el inicio"/>
</node>
</node>
</node>
</map>
