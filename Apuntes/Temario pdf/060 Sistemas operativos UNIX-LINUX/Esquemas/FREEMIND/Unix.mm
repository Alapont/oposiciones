<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1368292722541" ID="ID_373372590" MODIFIED="1556816806212" TEXT="Unix">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368292777360" ID="ID_687635906" MODIFIED="1391699234993" POSITION="right" TEXT="Historia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376322547053" ID="ID_1865722663" MODIFIED="1384369361103" TEXT="MULTICS, abandonado y recuperado por Ken Thompson y Dennis Ritchie"/>
<node CREATED="1376322578458" ID="ID_560273679" MODIFIED="1556816674462" TEXT="Se pasa a crear UNIX (1969), basado en lenguaje B, m&#xe1;s tarde en C."/>
<node CREATED="1556816551026" ID="ID_615330806" MODIFIED="1556816624941" TEXT="El &#xe9;xito del sistema lleva a numerosos &#xab;clones&#xbb; y variantes, llamadas &#xab;UNIX-like&#xbb;."/>
<node CREATED="1556816075787" ID="ID_1781298333" MODIFIED="1556818140021" TEXT="Trademark UNIX hoy en propiedad de &quot;The Open Group&quot;, que certifica sistemas y otorga derecho de uso de la marca."/>
<node CREATED="1556816701298" ID="ID_1063207388" MODIFIED="1556816711183" TEXT="Linux es el &#xab;UNIX-like&#xbb; m&#xe1;s conocido hoy en d&#xed;a."/>
</node>
<node CREATED="1556816779795" ID="ID_512358404" MODIFIED="1556816808526" POSITION="right" TEXT="UNIX y sus variantes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1556816810946" ID="ID_1911229075" MODIFIED="1556818026880" TEXT="UNIX version 8 (hist&#xf3;rico)"/>
<node CREATED="1556817161379" ID="ID_1059086533" MODIFIED="1556817291951" TEXT="System V (hist&#xf3;rico)"/>
<node CREATED="1556817176611" ID="ID_844123310" MODIFIED="1556817300799" TEXT="BSD (hist&#xf3;rico)"/>
<node CREATED="1556817315315" ID="ID_294814535" MODIFIED="1556817545119" TEXT="FreeBSD, NetBSD, OpenBSD, DragonflyBSD"/>
<node CREATED="1556817894628" ID="ID_895182040" MODIFIED="1556817899360" TEXT="macOS"/>
<node CREATED="1556817303540" ID="ID_1464630363" MODIFIED="1556817305888" TEXT="HP-UX"/>
<node CREATED="1556817556004" ID="ID_1055511202" MODIFIED="1556817557503" TEXT="AIX"/>
<node CREATED="1556817225540" ID="ID_980509354" MODIFIED="1556817827664" TEXT="Oracle Solaris, illumos"/>
<node CREATED="1556817338643" ID="ID_627398287" MODIFIED="1556817346782" TEXT="Minix"/>
<node CREATED="1556817348755" ID="ID_99099276" MODIFIED="1556817349774" TEXT="Linux"/>
</node>
<node CREATED="1368292807428" ID="ID_765867136" MODIFIED="1556815994206" POSITION="right" TEXT="Estandarizaci&#xf3;n: IEEE intenta la estandarizaci&#xf3;n mediante POSIX">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368292928898" ID="ID_995049991" MODIFIED="1556815963135" POSITION="right" TEXT="Conceptos generales: simple, modular y flexible; principios:">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368293011699" ID="ID_616105609" MODIFIED="1368293015628" TEXT="Interactivo"/>
<node CREATED="1368293027994" ID="ID_453653714" MODIFIED="1377793161013" TEXT="Multiusuario">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368293032523" ID="ID_966577414" MODIFIED="1377793163791" TEXT="Multitarea">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368293051502" ID="ID_1541128065" MODIFIED="1377793166308" TEXT="Seguro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368293059241" ID="ID_895359882" MODIFIED="1368293062979" TEXT="Multiplataforma"/>
</node>
<node CREATED="1368293141396" ID="ID_1825173582" MODIFIED="1408308097458" POSITION="right" TEXT="Arquitectura">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376322654750" ID="ID_1509289239" MODIFIED="1377068010557" TEXT="Monol&#xed;tico y modular">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376322745726" ID="ID_1754716977" MODIFIED="1384369861333" TEXT="Basado en niveles que se pasan informaci&#xf3;n mediante traps">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368293173482" ID="ID_1844058122" MODIFIED="1391699252762" TEXT="kernel (modo kernel)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1368293267529" ID="ID_1161632368" MODIFIED="1385558752246" TEXT="N&#xfa;cleo dependiente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376322875109" ID="ID_1587988964" MODIFIED="1377793045566" TEXT="Interrupciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376322879562" ID="ID_1858962554" MODIFIED="1377793049518" TEXT="Drivers de dispositivos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376322885689" ID="ID_948562618" MODIFIED="1376322890169" TEXT="Parte de la memoria"/>
</node>
<node CREATED="1368293276727" ID="ID_621712585" MODIFIED="1385558755606" TEXT="N&#xfa;cleo independiente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376322899310" ID="ID_1091877969" MODIFIED="1376322908093" TEXT="Igual en todas la plataformas"/>
<node CREATED="1376322908696" ID="ID_1867966000" MODIFIED="1377792809933" TEXT="Scheduler">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376322915904" ID="ID_1990391900" MODIFIED="1377792818080" TEXT="Dispatcher">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376322944675" ID="ID_1069640394" MODIFIED="1377792823013" TEXT="Acceso controlado de los procesos a los perif&#xe9;ricos (recursos)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1376323031145" ID="ID_1926532990" MODIFIED="1384369953280" TEXT="Puede soportar varias CPUs (SMP)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376323048265" ID="ID_1384569702" MODIFIED="1384369954325" TEXT="Es configurable mediante par&#xe1;metros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376323063242" ID="ID_198268716" MODIFIED="1384369954918" TEXT="Se carga en memoria al arrancar el sistema">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376323425903" ID="ID_1283574520" MODIFIED="1377068015381" TEXT="LOS PROCESOS DEL KERNEL SIEMPRE EST&#xc1;N EN MEMORIA PRINCIPAL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368293186975" ID="ID_1334038633" MODIFIED="1384370073875" TEXT="biblioteca est&#xe1;ndar (modo usuario)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1368293207989" ID="ID_1373990731" MODIFIED="1384370075041" TEXT="programas y utilidades">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
</node>
<node CREATED="1368293331997" ID="ID_938979658" MODIFIED="1391262683162" POSITION="right" TEXT="Funcionamiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368293338256" ID="ID_547985744" MODIFIED="1408308472825" TEXT="Gesti&#xf3;n de procesos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368293420867" ID="ID_1478486110" MODIFIED="1382902650447" TEXT="comunicaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368293425930" ID="ID_1061171916" MODIFIED="1382881734467" TEXT="se&#xf1;alizaci&#xf3;n as&#xed;ncrona">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376323258041" ID="ID_368692740" MODIFIED="1377792841823" TEXT="signals">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368293437179" ID="ID_1360392329" MODIFIED="1377792844904" TEXT="transmisi&#xf3;n s&#xed;ncrona de mensajes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376323248672" ID="ID_1230788388" MODIFIED="1377792848742" TEXT="pipes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1368293457494" ID="ID_1506297353" MODIFIED="1384369393910" TEXT="scheduler (dispatcher)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376323155327" ID="ID_168684556" MODIFIED="1376323160871" TEXT="Cambio de contexto"/>
<node CREATED="1376323342565" ID="ID_469896438" MODIFIED="1376323363885" TEXT="Tiempo real">
<node CREATED="1376323346849" ID="ID_496933516" MODIFIED="1377792858768" TEXT="FIFO no-preemptive">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376323372763" ID="ID_1377318511" MODIFIED="1377792862381" TEXT="Round Robin preemptive">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1368293490638" ID="ID_1323075480" MODIFIED="1377792868960" TEXT="Los procesos pueden estar hasta en 9 estados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368293541684" ID="ID_1350742362" MODIFIED="1377793204151" TEXT="multiprogramaci&#xf3;n a tiempo compartido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368293563694" FOLDED="true" ID="ID_1664834699" MODIFIED="1391699342807" TEXT="Gesti&#xf3;n de memoria">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368293582616" ID="ID_445766611" MODIFIED="1377793214494" TEXT="paginaci&#xf3;n por demanda">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368293588376" ID="ID_1222430377" MODIFIED="1377792873331" TEXT="combinaci&#xf3;n de segmentos paginados">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368293607593" ID="ID_313498394" MODIFIED="1368293612298" TEXT="&#xe1;rea de swap"/>
</node>
<node CREATED="1368293654391" FOLDED="true" ID="ID_1990416282" MODIFIED="1391699441948" TEXT="Sistema de archivos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368293672860" ID="ID_1819337068" MODIFIED="1368293685685" TEXT="Estructura de una partici&#xf3;n">
<node CREATED="1368293685687" ID="ID_134048329" MODIFIED="1368293721343" TEXT="bloque de arranque"/>
<node CREATED="1368293692691" ID="ID_1962486808" MODIFIED="1368293697020" TEXT="superbloque"/>
<node CREATED="1368293697447" ID="ID_1057353421" MODIFIED="1377792885958" TEXT="i-nodes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376323471253" ID="ID_1975969611" MODIFIED="1377793219687" TEXT="NO contienen el nombre del fichero">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377861086299" ID="ID_392068149" MODIFIED="1377861390535" TEXT="Anteriormente ten&#xed;an  13 entradas (UNIX System V)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376323481731" ID="ID_1966963032" MODIFIED="1384540629614" TEXT="Los FS  actuales tienen hasta 15 entradas (punteros)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377861107515" ID="ID_67052944" MODIFIED="1377861113171" TEXT="12 punteros directos"/>
<node CREATED="1377861113571" ID="ID_458630205" MODIFIED="1384540633596" TEXT="1 indirecto simple"/>
<node CREATED="1377861117116" ID="ID_1556510872" MODIFIED="1377861122411" TEXT="1 indirecto doble"/>
<node CREATED="1377861122827" ID="ID_1576707117" MODIFIED="1377861127580" TEXT="1 indirecto triple"/>
</node>
</node>
<node CREATED="1368293709689" ID="ID_1360175386" MODIFIED="1368293715592" TEXT="bloques de datos"/>
</node>
</node>
<node CREATED="1368293740523" FOLDED="true" ID="ID_480498665" MODIFIED="1391699531461" TEXT="Gesti&#xf3;n de E/S">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368293753783" ID="ID_1239358068" MODIFIED="1384370905146" TEXT="Dispositivos de bloques">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368293820878" ID="ID_574198926" MODIFIED="1377792952039" TEXT="secuencias de bytes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368293826638" ID="ID_127063414" MODIFIED="1384370936417" TEXT="Usan buffercache">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368293768548" ID="ID_163265281" MODIFIED="1384370922963" TEXT="Dispositivos de caracteres">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368293850861" ID="ID_1766117761" MODIFIED="1377792915256" TEXT="sin estructura, son raw">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368293857608" ID="ID_1917844499" MODIFIED="1384370941142" TEXT="sin buffercache">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376323570407" ID="ID_1049022340" MODIFIED="1380716601902" TEXT="operaciones car&#xe1;cter a car&#xe1;cter"/>
</node>
<node CREATED="1368293866146" ID="ID_287008909" MODIFIED="1368293871135" TEXT="ficheros especiales"/>
</node>
<node CREATED="1368293966555" FOLDED="true" ID="ID_250057125" MODIFIED="1391290829547" TEXT="Interrupciones y excepciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376323636991" ID="ID_1350908867" MODIFIED="1376323643831" TEXT="SE guarda el contexto"/>
<node CREATED="1376323644680" ID="ID_1893308523" MODIFIED="1377068080850" TEXT="SE ejecuta la excepci&#xf3;n"/>
<node CREATED="1376323652342" ID="ID_1120819572" MODIFIED="1376323663568" TEXT="Se vuelve a cargar el contexto en memoria"/>
<node CREATED="1376323667094" ID="ID_617081196" MODIFIED="1377792938855" TEXT="Niveles tipicos">
<node CREATED="1376323674908" ID="ID_1176767323" MODIFIED="1376323683536" TEXT="Interrupciones por SW"/>
<node CREATED="1376323684825" ID="ID_256814042" MODIFIED="1376323694081" TEXT="Terminales"/>
<node CREATED="1376323694618" ID="ID_686788964" MODIFIED="1376323699385" TEXT="Dispositivos de red"/>
<node CREATED="1376323699987" ID="ID_225891395" MODIFIED="1376323702392" TEXT="Disco"/>
<node CREATED="1376323702671" ID="ID_1524989610" MODIFIED="1376323705478" TEXT="Reloj"/>
<node CREATED="1376323705884" ID="ID_1445207576" MODIFIED="1376323716104" TEXT="Errores de m&#xe1;quina"/>
</node>
</node>
</node>
</node>
</map>
