<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1368336256038" ID="ID_1637032498" MODIFIED="1378058271870" TEXT="Software Libre">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368336342178" ID="ID_829222657" MODIFIED="1391353678224" POSITION="right" TEXT="Historia">
<node CREATED="1368336662671" ID="ID_1385832185" MODIFIED="1368336672064" TEXT="RMS"/>
<node CREATED="1368336672646" ID="ID_1830083851" MODIFIED="1368336689860" TEXT="GNU (Hurd)"/>
<node CREATED="1368336676387" ID="ID_1870169493" MODIFIED="1368336681651" TEXT="Linux"/>
</node>
<node CREATED="1368336650220" ID="ID_595721868" MODIFIED="1391354350520" POSITION="right" TEXT="Modelos de explotaci&#xf3;n del software">
<node CREATED="1368336707954" ID="ID_1516551862" MODIFIED="1368336716089" TEXT="Sin distribuci&#xf3;n externa"/>
<node CREATED="1368336716360" ID="ID_1288541826" MODIFIED="1368336724600" TEXT="licencia"/>
<node CREATED="1368336724953" ID="ID_914407374" MODIFIED="1368336745197" TEXT="gratuito"/>
<node CREATED="1368336745474" ID="ID_1764004604" MODIFIED="1368336749273" TEXT="shareware"/>
<node CREATED="1368336749572" ID="ID_781337978" MODIFIED="1384698497935" TEXT="libre (FSF)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368336769153" ID="ID_1346069900" MODIFIED="1384698550928" TEXT="FSF">
<arrowlink DESTINATION="ID_661960663" ENDARROW="Default" ENDINCLINATION="146;0;" ID="Arrow_ID_401462727" STARTARROW="None" STARTINCLINATION="146;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384698502170" ID="ID_980585952" MODIFIED="1384698525810" TEXT="Creada por RMS para la Licencia GPL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368336775334" ID="ID_118116390" MODIFIED="1384698554936" TEXT="derechos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368336786775" ID="ID_1622428075" MODIFIED="1368336791265" TEXT="Uso"/>
<node CREATED="1368336792890" ID="ID_799926500" MODIFIED="1368336811112" TEXT="Estudiarlo y adaptarlo"/>
<node CREATED="1368336811557" ID="ID_870684914" MODIFIED="1368336818892" TEXT="Distribuci&#xf3;n"/>
<node CREATED="1368336819647" ID="ID_358808144" MODIFIED="1382010155659" TEXT="Es opcional hacer p&#xfa;blicas las adaptaciones o mantenerlas de forma privada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1368336902830" ID="ID_1055864669" MODIFIED="1385566067827" TEXT="c&#xf3;digo fuente abierto (OSI)">
<arrowlink DESTINATION="ID_717106770" ENDARROW="Default" ENDINCLINATION="234;0;" ID="Arrow_ID_383777958" STARTARROW="None" STARTINCLINATION="234;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368337115825" ID="ID_617356423" MODIFIED="1391719866479" TEXT="Incluye  la obligaci&#xf3;n a la distribuci&#xf3;n de modificaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1368337451759" ID="ID_209997962" MODIFIED="1391355074905" POSITION="right" TEXT="Tipos de licencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368337935026" ID="ID_661960663" MODIFIED="1385566049228" TEXT="GPL (General Public License)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368337949698" ID="ID_407146921" MODIFIED="1384421790202" TEXT="Conserva derechos de autor">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368337979074" ID="ID_491023125" MODIFIED="1384421792873" TEXT="Distribuci&#xf3;n en formato GPL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368338224564" ID="ID_752245945" MODIFIED="1384421795424" TEXT="M&#xe1;s del 60% del software">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384421797288" ID="ID_1964256084" MODIFIED="1384421824106" TEXT="Es el formato de distribuci&#xf3;n de GNU">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1391355082729" ID="ID_1962339575" MODIFIED="1391355085981" TEXT="Partes">
<node CREATED="1391355085981" ID="ID_1425080389" MODIFIED="1391355088945" TEXT="Pre&#xe1;mbulo"/>
<node CREATED="1391355098951" ID="ID_1333355934" MODIFIED="1391355127095" TEXT="Cla&#xfa;sulas">
<node CREATED="1391355127095" ID="ID_1795651254" MODIFIED="1391355135769" TEXT="Derecho de copia"/>
<node CREATED="1391355136087" ID="ID_1330245430" MODIFIED="1391355149317" TEXT="Descarga de la responsabilidad del autor y distribuidor"/>
</node>
<node CREATED="1391355106681" ID="ID_1349934955" MODIFIED="1391355163389" TEXT="Gu&#xed;a de uso"/>
</node>
</node>
<node CREATED="1368338804435" ID="ID_51552489" MODIFIED="1385566417941" TEXT="LGPL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368338809638" ID="ID_1085011285" MODIFIED="1378285003820" TEXT="No obliga a distribuir el software con GPL si es a&#xf1;adido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1369810824592" ID="ID_1496896491" MODIFIED="1378283517606" TEXT="Es la &#xfa;nica compatible con GPL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376473313934" ID="ID_893881513" MODIFIED="1378285002084" TEXT="Permite incorporar desarrollos GPL en productos propietarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368338041067" ID="ID_717106770" MODIFIED="1385566416346" TEXT="AGPL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368338046355" ID="ID_1795235959" MODIFIED="1384422000704" TEXT="Obligaci&#xf3;n de distribuir el software">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368338092004" ID="ID_1577611836" MODIFIED="1385566430205" TEXT="BSD">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368338132351" ID="ID_1029191441" MODIFIED="1378285012973" TEXT="Mantiene copyright para renuncia de garant&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368338173834" ID="ID_257969721" MODIFIED="1384421771511" TEXT="Mantiene la autor&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368338186170" ID="ID_1414358819" MODIFIED="1384421774841" TEXT="Libre distribuci&#xf3;n y modificaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368338272966" ID="ID_325031067" MODIFIED="1385566476984" TEXT="MPL (Mozilla)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368338350838" ID="ID_1690415239" MODIFIED="1378283656361" TEXT="Distribuci&#xf3;n no tiene que ser MPL">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1391365380994" ID="ID_930463584" MODIFIED="1391365439289" TEXT="Apache License">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1391365389134" ID="ID_1104376014" MODIFIED="1391365395405" TEXT="ASF"/>
<node CREATED="1391365395775" ID="ID_1524545754" MODIFIED="1391365403410" TEXT="Es parecido a LGPL">
<node CREATED="1391365403410" ID="ID_1677015181" MODIFIED="1391365422131" TEXT="Puede ustilizarse el c&#xf3;digo en productos propietarios"/>
<node CREATED="1391365422903" ID="ID_1374246747" MODIFIED="1391365434399" TEXT="Es compatible con GPL v3"/>
</node>
</node>
</node>
<node CREATED="1368338975298" ID="ID_1160054305" MODIFIED="1384539665472" POSITION="right" TEXT="Modelos de desarrollo (Eric Raymond)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1368338985157" ID="ID_856377305" MODIFIED="1384422432571" TEXT="Catedral">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modelo corporativo centralizado (GNU, Emacs)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368338989958" ID="ID_211239965" MODIFIED="1384422364892" TEXT="Bazar">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modelo distribuido
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1368339050672" FOLDED="true" ID="ID_1406861715" MODIFIED="1391353671953" POSITION="right" TEXT="Modos de financiaci&#xf3;n">
<node CREATED="1368339061554" ID="ID_1325367663" MODIFIED="1368339066183" TEXT="Patrocinio"/>
<node CREATED="1368339085670" ID="ID_85528088" MODIFIED="1368339090887" TEXT="Servicios y soporte"/>
<node CREATED="1368339091178" ID="ID_1733629494" MODIFIED="1368339098485" TEXT="Productos relacionados"/>
</node>
<node CREATED="1368339296581" FOLDED="true" ID="ID_410895413" MODIFIED="1391353676835" POSITION="right" TEXT="Productos">
<node CREATED="1368339303310" ID="ID_1605143171" MODIFIED="1376905482828" TEXT="Wine: Implementaci&#xf3;n en software libre de la API de Windows que funciona sobre X11">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      WINE no es un emulador, ya que muchas de las aplicaciones de Windows no funcionan sobre el
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1368339325135" ID="ID_1311622029" MODIFIED="1384421378696" TEXT="Escritorios">
<node CREATED="1368339334362" ID="ID_570613677" MODIFIED="1384421385419" TEXT="KDE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376634720077" FOLDED="true" ID="ID_1170060974" MODIFIED="1384421386480" TEXT="Gestor de ventanas">
<node CREATED="1376905449204" ID="ID_256307751" MODIFIED="1376905463900" TEXT="Tiene uno propio, pero puede funcionar con varios"/>
<node CREATED="1376635377540" ID="ID_1735884478" MODIFIED="1376635384485" TEXT="Kwin"/>
<node CREATED="1376635384925" ID="ID_469422065" MODIFIED="1378284828805" TEXT="Plasma (KDE 4.0)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376905228491" ID="ID_1332650863" MODIFIED="1376905249115" TEXT="Kicker: paneles de escritorio"/>
<node CREATED="1376905250844" ID="ID_20967092" MODIFIED="1376905262675" TEXT="Kdesktop: ventana raiz"/>
<node CREATED="1376905275908" ID="ID_493649118" MODIFIED="1376905285996" TEXT="Plasmoides: peque&#xf1;as aplicaciones"/>
</node>
</node>
<node CREATED="1376634988997" ID="ID_1837494710" MODIFIED="1376634995280" TEXT="Multiplataforma"/>
<node CREATED="1376634995601" ID="ID_1604143958" MODIFIED="1376635003389" TEXT="Programado en C++"/>
<node CREATED="1376904681665" ID="ID_1288488758" MODIFIED="1376904703873" TEXT="Kparts: sistema de componentes "/>
</node>
<node CREATED="1368339392997" FOLDED="true" ID="ID_1724911185" MODIFIED="1384422265331" TEXT="GNOME">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376634745540" ID="ID_1615839719" MODIFIED="1382010281281" TEXT="Primer gestor de ventanas Enlightment ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376634757227" ID="ID_434782100" MODIFIED="1378283560964" TEXT="Actual gestor de ventanas Metacity (GNOME2)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376634901841" ID="ID_795804533" MODIFIED="1382010283413" TEXT="Ultima versi&#xf3;n: GNOME 3.x">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376635033398" ID="ID_844068961" MODIFIED="1382010284473" TEXT="Programado en GTK+">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376635060767" ID="ID_354663626" MODIFIED="1382010285253" TEXT="Basado en CORBA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376904595725" ID="ID_678842848" MODIFIED="1378283562955" TEXT="Bonobo: sistema de componentes libre basado en CORBA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376904774333" ID="ID_688285384" MODIFIED="1382010286553" TEXT="DII: Interfaz de invocaci&#xf3;n din&#xe1;mica de objetos distribuidos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1376906953844" FOLDED="true" ID="ID_432516008" MODIFIED="1384422450011" TEXT="Glassfish">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1376906960329" ID="ID_1850995333" MODIFIED="1376906973004" TEXT="Servidor de aplicaciones de c&#xf3;digo abierto"/>
</node>
<node CREATED="1368339450366" ID="ID_713085560" MODIFIED="1368339458010" TEXT="Apache"/>
<node CREATED="1368339458381" ID="ID_1257589814" MODIFIED="1368339461838" TEXT="PHP"/>
<node CREATED="1368339462245" ID="ID_514419023" MODIFIED="1368339465806" TEXT="MySQL"/>
<node CREATED="1376667974623" ID="ID_231556852" MODIFIED="1376667981181" TEXT="Ubuntu">
<node CREATED="1376667981183" ID="ID_1358981435" MODIFIED="1378285036181" TEXT="LTS: 5 a&#xf1;os de soporte desde el lanzamiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1376739726170" ID="ID_124157747" LINK="http://en.wikipedia.org/wiki/MOSIX" MODIFIED="1376739949477" TEXT="Mosix">
<node CREATED="1376739735108" ID="ID_1033127053" MODIFIED="1384422249345" TEXT="Sistema operativo distribuido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1376739747823" ID="ID_1242641190" MODIFIED="1384422251315" TEXT="Usado para grid y cl&#xfa;steres Linux">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</map>
