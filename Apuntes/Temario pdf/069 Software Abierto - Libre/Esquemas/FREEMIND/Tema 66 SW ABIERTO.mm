<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1442391657371" ID="ID_1429638150" MODIFIED="1442391675397">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T61/62 SW
    </p>
    <p style="text-align: center">
      ABIERTO
    </p>
  </body>
</html></richcontent>
<node CREATED="1442391703587" ID="ID_228843541" MODIFIED="1442392080150" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      SW libre y
    </p>
    <p>
      propietario
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442392040939" ID="ID_1071376586" MODIFIED="1442392046173" TEXT="SW libre (GPL)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442392582918" ID="ID_1126876804" MODIFIED="1442397488003">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>(Gen Pub License, <u>GNU</u>-Free SW Found de R.Stallman)</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1442392835403" ID="ID_1182965442" MODIFIED="1445421259885">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>C&#243;d fuente</u>&#160;del pgm <u>p&#250;b</u>&#160;y <b>libertads</b>&#8594; ejec, adaptar, distrib copias y modif
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1442392698744" ID="ID_975983895" MODIFIED="1442392867376" TEXT="No implica gratis">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1442392729002" ID="ID_579753298" MODIFIED="1442398335791">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Copyleft</b>&#160;(persistencia)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1442392731033" ID="ID_1487800874" MODIFIED="1445421962938">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>No</u>&#160;permite a redistribs a&#241;adir <u>restriccs</u>&#160;a la licencia (privatizar)&#8594; GPL se <u>mantiene</u>&#160;en obras derivadas
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1442393071133" ID="ID_399630238" MODIFIED="1442393091966" TEXT="Benef de la comunidad">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</node>
<node CREATED="1442391739140" ID="ID_1826473641" MODIFIED="1445419971608">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      SW de c&#243;digo
    </p>
    <p>
      abierto (OSD)
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442393101314" ID="ID_493573175" MODIFIED="1445419933916">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Open Source Def) OS-Initivative def licencia&#160;&#160;<b>abierta</b>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1442393218146" ID="ID_1513195219" MODIFIED="1445420000669">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No es licencia&#8594; <b>directrices</b>&#160;q aseguran q licencia cumple libertades
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1445420021745" ID="ID_1555095733" MODIFIED="1445420034404">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Origen: </i>Debian Free SW Guide
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1442393263338" ID="ID_1452779210" MODIFIED="1442397024977">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>No es copyleft&#8594;</i>&#160;permite distrib y adapt comercial (privatizar obras derivadas)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1442393301903" ID="ID_499233797" MODIFIED="1442393315187">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Benef de grandes compa&#241;&#237;as
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1442391754549" ID="ID_750022675" MODIFIED="1442393602120">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Directrices
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442393454656" ID="ID_1612420312" MODIFIED="1445420907382">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Redistrib libre (sin tasa), c&#243;d fuente accesible, posibles obras derivads, integr c&#243;d fuente (o parches), no discrim, distrib licencia (cesi&#243;n dchos), no espec&#237;fica de un prod, no limitar a otro sw, tecn neutra
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1442393589581" ID="ID_1011527663" MODIFIED="1445420922317">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>OSI Certified</u>&#160;para lics q cumplan directs
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1442391751355" ID="ID_1243012411" MODIFIED="1442398222858" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Licencias
    </p>
    <p>
      libres
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442394482044" ID="ID_233038721" MODIFIED="1442398163641">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Instrum legal ( <u>contrato</u>&#160;) del titular del sw para otorgar <u>permisos y limits</u>&#160;a terceros (remun eco o no)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1442394538352" ID="ID_297283217" MODIFIED="1445420981658">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se pueden comb con otras y hacer distribs con dif lics (si no son copyleft)&#160;
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1442391779429" ID="ID_924241389" MODIFIED="1442391955408">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Copyleft
    </p>
    <p>
      robusto
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442391795856" ID="ID_1039591790" MODIFIED="1442394855915">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      GPL v2
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442394731403" ID="ID_776418974" MODIFIED="1442394733308" TEXT="Dchos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442394600013" ID="ID_1420478743" MODIFIED="1445423397028">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modif para uso pers (priv o comerc) y distrib con GPL sin notific autores (s&#237; a destinat)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1445423365377" ID="ID_1158237818" MODIFIED="1445423443422">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cobrar (por distrib copias, no por modifs)&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1445422781013" ID="ID_998822315" MODIFIED="1445422805365">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Distrib c&#243;d objeto (binario) con acceso a c&#243;d&#160;fuente (pe URL) por 3 a&#241;os
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1442394794544" ID="ID_607100194" MODIFIED="1442394797856" TEXT="Obligs">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442394797860" ID="ID_767042387" MODIFIED="1445420866949">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Incluir (autor&#237;a, modifics, garant&#237;a y URL licencia), usuario acepta GPL por el hecho de&#160;&#160; <u>modif/distrib</u>&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1442391801257" ID="ID_1248976940" MODIFIED="1442394859471">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      GPL v3
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442394914612" ID="ID_1508318173" MODIFIED="1442395061685">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Dchos (a&#241;adidos a v2)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442394919429" ID="ID_490995676" MODIFIED="1445421086540">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Distrib c&#243;d fuente cn copyright, a&#241;adir permisos, integr sw cn restriccs adic para compat con otra lic&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1442395062472" ID="ID_1031580800" MODIFIED="1442395064922" TEXT="Obligs">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442395065468" ID="ID_80009133" MODIFIED="1442398408044">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Usuario puede modif/retirar limits de un s&#170; <u>DRM</u>, si patente cede dchos a GPL no los puede reclamar
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1442395199635" ID="ID_1764080285" MODIFIED="1442398074736">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Compatib (restriccs similares)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442395202490" ID="ID_1562756709" MODIFIED="1445421113721">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Con GPL v2 solo si dicha&#160; <u>licencia lo indica</u>&#160;&#160;('con v2 o posteriores')
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1442395216730" ID="ID_1421526650" MODIFIED="1442398093653">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No con AGPL pero se puede combinar en un trabajo
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1442391806106" ID="ID_1562164157" MODIFIED="1445422005685">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      LGPL v3
    </p>
    <p>
      (Lesser)
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442395404076" ID="ID_1515131863" MODIFIED="1442395765469">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Permite <u>enlazar</u>&#160;pgms no libres con comps sw&#160; <u>sin afectar</u>&#160;al pgm final
    </p>
  </body>
</html></richcontent>
<node CREATED="1442395471381" ID="ID_178114297" MODIFIED="1445422032006">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Desarrollads pueden vincular pgms con comps libres sin copyleft (-liberts para users)
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1442395531731" ID="ID_1436378079" MODIFIED="1445422211012">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Si el comp enlazado&#160;fuera GPL y no LGPL, el pgm final tendr&#237;a que ser tb GPL
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1442391824062" ID="ID_1509772819" MODIFIED="1442396134051">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      AGPL v3 (Affero)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442396134847" ID="ID_1347256379" MODIFIED="1445421156873">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      = q GPL v3 + permite a usuario que interact&#250;e con <u>sw en red</u>&#160;recibir su fuente
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1442396267098" ID="ID_948696660" MODIFIED="1445421145589">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No compat cn GPL v2-3 (puede comb). Reco por FSF para servs
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1442391849418" ID="ID_533270687" MODIFIED="1442396316720">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      EUPL (EU Union Pub Lic)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442396317761" ID="ID_507491223" MODIFIED="1442396353140">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Licencia mundial, gratis y no exclusiva, la usan las AP para declarar fuentes abiertas
    </p>
  </body>
</html></richcontent>
<node CREATED="1442396396432" ID="ID_591271376" MODIFIED="1442396835704">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Compatible con GPL v2 (v3 en consulta)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1442396645165" ID="ID_1996514984" MODIFIED="1442396647059" TEXT="Dchos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442396647064" ID="ID_1745952164" MODIFIED="1445421237702">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Subcontr dchos, posibles defectos, no respons (salvo dolo a pers), user acepta al ejercer <u>cualq libertad</u>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1442396372037" ID="ID_1686913680" MODIFIED="1442396374092" TEXT="Obligs">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442396374097" ID="ID_1945484850" MODIFIED="1442396623402">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Menci&#243;n a dchos autor y patentes, derivs de lics compatibles pueden prevalecer, copia del c&#243;d fuente
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1442391857292" ID="ID_730592146" MODIFIED="1442391966683">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sin copyleft
    </p>
    <p>
      (permisivas)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442391876348" ID="ID_1475988992" MODIFIED="1442396865057">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      BSD (Berkeley SW Distrib)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442396867035" ID="ID_1467284402" MODIFIED="1442396874423" TEXT="Dchos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442396874428" ID="ID_1516734262" MODIFIED="1445420746895">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Distrib en c&#243;d objeto (bin) o fuente,&#160;privatiz obras deriv y comb
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1445420666467" ID="ID_1396832544" MODIFIED="1445420679957">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No garantiza correcto func del pgm y&#160;niega responsab
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1442398488660" ID="ID_776299400" MODIFIED="1445420834399">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Forking:&#160; </i>bifurc de c&#243;d&#8594; crear nueva versi&#243;n del pgm&#160;a partir del c&#243;d fuente
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1442397057902" ID="ID_961351413" MODIFIED="1442397059582" TEXT="Obligs">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442397059587" ID="ID_196348034" MODIFIED="1442397221374">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Copyright en c&#243;d fuente, condics de responsab, no usar nombre autor sin permiso (autor&#237;a eliminable)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1442397107558" ID="ID_1060140347" MODIFIED="1442397112080" TEXT="Compatib">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442397109981" ID="ID_1368097825" MODIFIED="1445423071694">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1.0 no compat con GPL v2
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1445422999336" ID="ID_43075839" MODIFIED="1445423100067">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>1.1</u>&#160;s&#237;, pero no viceversa&#8594; solo permite insertar c&#243;d BSD en sw GPL (distrib final GPL)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1442391904058" ID="ID_673128535" MODIFIED="1442397673016" TEXT="Apache 1.1">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442397673001" ID="ID_327578256" MODIFIED="1445421706738">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Variante BSD, no compat con GPL
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1442397629292" ID="ID_755994438" MODIFIED="1442397636364" TEXT="Obligs extras">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442397636369" ID="ID_266757347" MODIFIED="1445423699604">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Public autores</u>&#160;orig en redistrib (en doc o c&#243;d), obras deriv no pueden usar 'Apache' sin autoriz
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1442391907500" ID="ID_307545988" MODIFIED="1442391910626" TEXT="Apache 2.0">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442397711749" ID="ID_46960480" MODIFIED="1442397713149" TEXT="Dchos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442397713153" ID="ID_1003836768" MODIFIED="1442397748515">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sublicenciar en c&#243;d fuente o bin, otorga licencia patente (revocable)
    </p>
  </body>
</html></richcontent>
<node CREATED="1442397752070" ID="ID_600668576" MODIFIED="1442397773783" TEXT="Incompat con GPL2 v2, s&#xed; con v3">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1442397775044" ID="ID_715361381" MODIFIED="1442397778632" TEXT="Obligs">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442397778637" ID="ID_81544037" MODIFIED="1442397887904">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Indicar titularidad, modifs y 'notice.txt' (comentarios jur); conservar aviso de marcas (no usar nombre)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1442391911747" ID="ID_1628295896" MODIFIED="1442397905002">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      NPL y MPL 1.1 (Netscape y Mozilla)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442397905614" ID="ID_754437325" MODIFIED="1442397935620" TEXT="Equilibrio entre objs comerciales y desarrollo libre, no compat con GPL"/>
</node>
<node CREATED="1442391925212" ID="ID_1208495366" MODIFIED="1442391927741" TEXT="MPl 2.0">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442397943945" ID="ID_1139200579" MODIFIED="1442397971654">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Permite crear obra +grande combinando con otras lics de GNU
    </p>
  </body>
</html></richcontent>
<node CREATED="1442397972322" ID="ID_1453718088" MODIFIED="1442397982323" TEXT="Compat con GPL v2, LGPL v2.1 y AGPL v3"/>
</node>
</node>
<node CREATED="1442391929270" ID="ID_434075494" MODIFIED="1442391931389" TEXT="Otras">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442397991560" ID="ID_863435935" MODIFIED="1445421287867">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CeCILL v2 (compat con GPL), Zope PL (sin copyleft y compat con GPL), SCSL (Sun) y MSSI (pseudo-libres)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1442391973790" ID="ID_407939745" MODIFIED="1442399739056" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Proyectos
    </p>
    <p>
      de sw libre
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442392004033" ID="ID_919474050" MODIFIED="1442392010254" TEXT="KDE">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442399742713" ID="ID_1749158169" MODIFIED="1442400268927">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Escritorio</u>&#160;con <u>ventanas</u>&#160;para Unix abierto y personalizable
    </p>
  </body>
</html></richcontent>
<node CREATED="1442399874178" ID="ID_1421251776" MODIFIED="1445421311988">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>S&#170;:</i>&#160;KIO (E/S), D-BUS (com procesos&#160; <u>no CORBA</u>), Phonon (multim), Goya (UI) y KParts (compartir apps)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1442399762213" ID="ID_1454072639" MODIFIED="1442400305981">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Licencias:</i>&#160;<u>GPL y Qt</u>&#160;(libs propietarias)
    </p>
  </body>
</html></richcontent>
<node CREATED="1442399933071" ID="ID_856813632" MODIFIED="1442399995696">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>v4.14: </i>KDE Framework y Plasma 5.4.1
    </p>
  </body>
</html></richcontent>
<node CREATED="1442399995701" ID="ID_328908895" MODIFIED="1442400034343">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      =n&#186; mayor (no recompilar), 1 menor (nueva func y correcs), 2 menores (solo correccs)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1442400065298" ID="ID_703294890" MODIFIED="1442400142486">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Apps:&#160;</i>KWin, KMail, Kpete (msj inst)
    </p>
  </body>
</html></richcontent>
<node CREATED="1442400170556" ID="ID_781071554" MODIFIED="1445421332078">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Konqr</u>&#160;(nav), Word, Sheets, Stage (ppt), Flow (diagr), Karbon (vect), Krita (Ph), Kugar (inform), Plan(proy)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1442392005558" ID="ID_1235258364" MODIFIED="1442392009182" TEXT="GNOME">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442400227951" ID="ID_455261658" MODIFIED="1442400352784">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (GNU Netw Obj Model Environm, v3.16) entorno&#160; <u>escritorio</u>&#160;para Unix
    </p>
  </body>
</html></richcontent>
<node CREATED="1442400275722" ID="ID_81630444" MODIFIED="1442400570162">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Licencias:</i>&#160;<u>GPL/LGPL</u>&#160;(totalmente libre), libs&#160; <u>GTK+</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1442400386818" ID="ID_1155273361" MODIFIED="1442401397924">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Apps: </i>ORBit (<u>CORBA</u>), Glade (apps gr&#225;f), Nautilus (adm), HIG (usab), VFS (s&#170;fich virt), GConf
    </p>
  </body>
</html></richcontent>
<node CREATED="1442400486789" ID="ID_759536319" MODIFIED="1445421327687">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Galeon</u>&#160;(nav), AbiWord, Gnumeric, Agnubis (ppt), Gimp (gr&#225;f), Evol (mail), DB
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1442392011337" ID="ID_435182173" MODIFIED="1442392015937" TEXT="OpenOffice.org">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442400606770" ID="ID_83915684" MODIFIED="1442401150937">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (v4.1) suite ofim&#225;tica de sw libre y c&#243;d abierto <u>multiplat</u>&#160; (Win/Unix/BSD, Mac) compat con Micr Office&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1442401105365" ID="ID_835705606" MODIFIED="1442401133089">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Licencia:</i>&#160;<u>LGPL</u>&#160;y <u>Apache 2.0</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1442401156703" ID="ID_145814359" MODIFIED="1442401180014">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Apps:</i>&#160;Writer, Calc, Impress (ppt), Draw (vect), Base (BD), Math
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1442392017073" ID="ID_932268569" MODIFIED="1442392020448" TEXT="Serv Apache">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1442401184613" ID="ID_1775850487" MODIFIED="1442401292995">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (v2.4.16) serv HTTP 1.1 multiplat, modular, seguro (TLS), IPv6, compr info para naveg, msj error
    </p>
  </body>
</html></richcontent>
<node CREATED="1442401214744" ID="ID_1930938802" MODIFIED="1442401224222">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Licencia:</i>&#160;<u>Apache</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
</map>
