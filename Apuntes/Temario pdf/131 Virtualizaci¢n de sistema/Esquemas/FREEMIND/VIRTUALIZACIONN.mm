<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1428655210565" ID="ID_518144421" MODIFIED="1428655217461" TEXT="T119 VIRTUALIZACI&#xd3;N">
<node CREATED="1428655789318" ID="ID_1382998688" MODIFIED="1428655859458" POSITION="right" TEXT="Creaci&#xf3;n v&#xed;a sw de una versi&#xf3;n virtual de alg&#xfa;n recurso tecnol&#xf3;gico ">
<icon BUILTIN="info"/>
<node CREATED="1428655865676" ID="ID_59402138" MODIFIED="1428656090615" TEXT="Emulaci&#xf3;n: traducci&#xf3;n de instrucciones / Simulaci&#xf3;n: E/S predefinidas"/>
</node>
<node CREATED="1428656316009" ID="ID_688258787" MODIFIED="1428656367429" POSITION="right" TEXT="Causas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428656319801" ID="ID_1432394483" MODIFIED="1428662998476" TEXT="Infrauso hw, CPDs salen fuera (necesidad control), ahorro energ&#xe9;tico, - coste adm, uso de sw antiguo"/>
</node>
<node CREATED="1428656376442" ID="ID_738860632" MODIFIED="1428656529668" POSITION="right" TEXT="Popek y Goldberg">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428656382938" ID="ID_851902005" MODIFIED="1428656574627" TEXT="Reqs de arq para soportar virtualizaci&#xf3;n">
<node CREATED="1428656574627" ID="ID_263644241" MODIFIED="1428656575112" TEXT="Equivalencia, control de recursos y eficiencia"/>
</node>
</node>
<node CREATED="1428662319887" ID="ID_1416571944" MODIFIED="1428662326344" POSITION="right" TEXT="Infraestructura virtualizada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428662426515" ID="ID_296302307" MODIFIED="1428663059576" TEXT="Virtualizaci&#xf3;n total/integrada, basada en 3 pilares">
<icon BUILTIN="xmag"/>
<node CREATED="1428662441233" ID="ID_1148352978" MODIFIED="1428662462576" TEXT="Adaptarse al cambio, Aumentar capacidad de respuesta, Reducir costes">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1428662471041" ID="ID_331075838" MODIFIED="1428662474978" TEXT="Implantaci&#xf3;n">
<node CREATED="1428662474979" ID="ID_779713065" MODIFIED="1428662869144" TEXT="Fases: Consolidaci&#xf3;n, ahorro y mayor uso de hw &#x2192; Entrega de nuevos servicios y mejora de actuales"/>
<node CREATED="1428662796829" ID="ID_953976647" MODIFIED="1428662884113" TEXT="Virtualizar apps adecuadas (con poca E/S), estrategia de almacen, combinar VMs din&#xe1;micamente"/>
</node>
<node CREATED="1428662642542" ID="ID_491647238" MODIFIED="1428662644132" TEXT="Gesti&#xf3;n">
<node CREATED="1428662644133" ID="ID_269727852" MODIFIED="1428662722942" TEXT="Autom&#xe1;tica (no proliferaci&#xf3;n de VMs), monitorizar (mantener SLA), distribuir recursos din&#xe1;micamente "/>
</node>
</node>
<node CREATED="1428655924061" ID="ID_1488673866" MODIFIED="1428655981400" POSITION="right" TEXT="Hipervisor (VMM)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428655930486" ID="ID_1597203539" MODIFIED="1428663106030" TEXT="Capa de abstracci&#xf3;n (sw) entre hw-sw &#x2192; permite que VMs con SO distintos operen en mismo equipo "/>
<node CREATED="1428656107038" ID="ID_1814730188" MODIFIED="1428656109486" TEXT="Tipos">
<node CREATED="1428656109487" ID="ID_1997723018" MODIFIED="1428656263916" TEXT="1 (unhosted)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428656168380" ID="ID_721607577" MODIFIED="1428656289554">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se ejec directamente sobre <b>hw</b>&#160;(+r&#225;pido)
    </p>
  </body>
</html></richcontent>
<node CREATED="1428656228299" ID="ID_734131987" MODIFIED="1428656302015" TEXT="Paravirtualizaci&#xf3;n o Virt Total">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1428656143252" ID="ID_753926337" MODIFIED="1428656263316" TEXT="2 (hosted)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428656200317" ID="ID_221674801" MODIFIED="1428663121687">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se ejec sobre <b>SO</b>&#160;host (+lento)
    </p>
  </body>
</html></richcontent>
<node CREATED="1428656230595" ID="ID_8761353" MODIFIED="1428659121222">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Binary Translation:</i>&#160;instrucs privilegiadas son reemplazadas por c&#243;digo
    </p>
  </body>
</html></richcontent>
<node CREATED="1428656211828" ID="ID_220388412" MODIFIED="1428656302590" TEXT="Virt HW nativa">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1428656619659" ID="ID_1790329807" MODIFIED="1428656626317" POSITION="right" TEXT="Servidores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428656627933" ID="ID_470106831" MODIFIED="1428659247583" TEXT="Servidor dedicado para cada servicio es muy costoso (hw, sw, licencias, energ&#xed;a, gesti&#xf3;n)">
<icon BUILTIN="yes"/>
</node>
<node CREATED="1428656761015" ID="ID_1367143329" MODIFIED="1428656772654" TEXT="Virt del SO">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428656773189" ID="ID_407091655" MODIFIED="1428658063862">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Particiona con librer&#237;as el serv f&#237;sico en servs indep y aislados ( <u>contenedores</u>, VEs)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1428656801896" ID="ID_671573480" MODIFIED="1428656913310" TEXT="Instala SO guests sobre host"/>
</node>
<node CREATED="1428657094090" ID="ID_896403829" MODIFIED="1428657271941" TEXT="Un parche en host se instala en todos los guests, facilita Recovery (DRP), consolida serv, migraci&#xf3;n f&#xe1;cil">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1428657182304" ID="ID_1927664781" MODIFIED="1428657273505" TEXT="Todos los SOs deben ser iguales">
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1428656916172" ID="ID_519361847" MODIFIED="1428656923110" TEXT="App: Web Hosting">
<node CREATED="1428656923395" ID="ID_1785385320" MODIFIED="1428656930150" TEXT="Pe: Parallels Virtuozzo"/>
</node>
</node>
<node CREATED="1428657298959" ID="ID_849679446" MODIFIED="1428671391986" TEXT="Virt de HW (nativa)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428657723163" ID="ID_1216994477" MODIFIED="1428658298920">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Hipervisor <u>Tipo 2</u>&#160;aisla las VMs y les asigna recursos din&#225;micamente
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1428657675361" ID="ID_668210168" MODIFIED="1428658215247" TEXT="Emula recursos hw, en lugar de acceder a ellos directamente"/>
</node>
<node CREATED="1428657781869" ID="ID_867715637" MODIFIED="1428657864657" TEXT="Portabilidad de VMs, - coste, se pueden usar SOs diferentes">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1428657865277" ID="ID_1809889394" MODIFIED="1428657886988" TEXT="-Rendimiento, hipervisor debe tener todos los drivers de los recursos">
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1428657932919" ID="ID_223802772" MODIFIED="1428657942851" TEXT="App: desarrollo sw y contol de calidad">
<node CREATED="1428657942852" ID="ID_497030748" MODIFIED="1428659980195" TEXT="Pe: VMWare Server, VirtualBox, Microsoft Virtual Server"/>
</node>
</node>
<node CREATED="1428657960094" ID="ID_923674230" MODIFIED="1428658770977" TEXT="Paravirtualizaci&#xf3;n (PVM)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428658255311" ID="ID_381450829" MODIFIED="1428658845255">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Hipervisor <u>Tipo 1</u>&#160;multiplexa accesos de VMs a recursos
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1428658331911" ID="ID_776328706" MODIFIED="1428658503207">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ring-deprivileging: </i>modifica SOs guests para dejar libre ring 0 y&#160;controlar recursos
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1428658416698" ID="ID_1267551016" MODIFIED="1428658464454" TEXT="Mejor rendimiento, no necesita drivers (no emula hw)">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1428658434731" ID="ID_1701054879" MODIFIED="1428659181237" TEXT="SOs deben ser abiertos para poder modificar">
<icon BUILTIN="button_cancel"/>
<node CREATED="1428658675326" ID="ID_39621096" MODIFIED="1428659147295" TEXT="Pe: Xen, VMWare ESX, Linux KVM"/>
</node>
</node>
<node CREATED="1428658778577" ID="ID_381792766" MODIFIED="1428671432903" TEXT="Virt Total o asistida por HW (HVM)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428658786218" ID="ID_1299760760" MODIFIED="1428659100371">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Procesador Intel-VT o AMD-V crea nivel <u>ring -1</u>&#160;para el hipervisor
    </p>
  </body>
</html></richcontent>
<node CREATED="1428658966007" ID="ID_1158878596" MODIFIED="1428658993323" TEXT="No modifica SO, mejor rendimiento">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1428659210001" ID="ID_337793419" MODIFIED="1428659213642" POSITION="right" TEXT="Almacenamiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428659474231" ID="ID_498304814" MODIFIED="1428659511808" TEXT="Varios disps de almacen conectados en red dando sensaci&#xf3;n de uno solo compartido">
<node CREATED="1428659484969" ID="ID_1377983150" MODIFIED="1428659497043" TEXT="Storage Area Network (SAN)"/>
</node>
<node CREATED="1428659531184" ID="ID_1899345235" MODIFIED="1428659566363" TEXT="Rapidez, accesibilidad, seguridad (autenticaci&#xf3;n, redundancia) y escalabilidad">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1428659568159" ID="ID_1881870769" MODIFIED="1428659575009" POSITION="right" TEXT="Redes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428659573390" ID="ID_1089964815" MODIFIED="1428659860614" TEXT="Se gestionan recursos sw-hw de la red y se distibuyen mediante conexiones virtuales ">
<node CREATED="1428659730333" ID="ID_610120094" MODIFIED="1428659739085" TEXT="Se reparte el BW y se aprovecha mejor">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1428659747225" ID="ID_1378252035" MODIFIED="1428659822075" TEXT="En redes externas (VLAN y switch) o internas (switches para reorg VMs en un mismo ordenador)"/>
</node>
<node CREATED="1428659872342" ID="ID_13564932" MODIFIED="1428660343803" POSITION="right" TEXT="App">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428660138048" ID="ID_679470167" MODIFIED="1428660176441" TEXT="Entorno indep (porci&#xf3;n m&#xed;n de SO virtual) para ejecutar una app">
<node CREATED="1428660193452" ID="ID_265278436" MODIFIED="1428660214126" TEXT="App: pruebas sw (si app falla, no afecta al resto)">
<node CREATED="1428660308475" ID="ID_255129175" MODIFIED="1428660328223" TEXT="Pe: Microsoft App-V, XenApp, VMWare ThinApp, WINE"/>
</node>
</node>
<node CREATED="1428660217020" ID="ID_635053766" MODIFIED="1428660303435" TEXT="Portabilidad (sin necesidad de instalaci&#xf3;n), secuenciaci&#xf3;n de la app (apps indep)">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1428660339715" ID="ID_694574167" MODIFIED="1428660343109" POSITION="right" TEXT="Escritorio">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428660375981" ID="ID_794455188" MODIFIED="1428660399252" TEXT="Separa entorno sw del cliente del hw, para tener un acceso seguro a la red corporativa">
<node CREATED="1428661615377" ID="ID_563400088" MODIFIED="1428661625227" TEXT="Permite teletrabajo">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1428660399253" ID="ID_373720395" MODIFIED="1428661627251" TEXT="Requiere mantener sw-hw actual y en funcionamiento">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1428661237888" ID="ID_1277371794" MODIFIED="1428661333306" TEXT="En cliente">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428661244185" ID="ID_128921141" MODIFIED="1428661270290" TEXT="Crea entorno SO indep en hw de usuario, para que apps no compatibles con su SO funcionen">
<node CREATED="1428661279563" ID="ID_1611621276" MODIFIED="1428661314952" TEXT="Funciona oflline, despliegue flexible (pe en USB)">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1428661294506" ID="ID_797460939" MODIFIED="1428661314166" TEXT="Requiere equipo cliente potente, seguridad en riesgo">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node CREATED="1428661317633" ID="ID_265917868" MODIFIED="1428661343228" TEXT="En servidor (Virtual Desktop Infraestr, VDI)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428661331434" ID="ID_1499325384" MODIFIED="1428663278557" TEXT="Sw cliente se ejecuta en servs del CPD y usuario se conecta por remoto">
<node CREATED="1428661529295" ID="ID_495886763" MODIFIED="1428662143843" TEXT="VNC (Virtual Netw Comput) o RDP 6.0 (Remote Desktop)"/>
</node>
<node CREATED="1428661425845" ID="ID_1045215670" MODIFIED="1428661812301" TEXT="Control y aislam, cliente ligero (caja tonta)">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1428661489104" ID="ID_819003031" MODIFIED="1428663300649" TEXT="Componentes CPD muy caros, rentable si hay buena densidad de virtualiz">
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1428661674535" ID="ID_1222346189" MODIFIED="1428661689395" TEXT="Pe: VMWare ESX, XenServer de Citrix"/>
</node>
</node>
<node CREATED="1428661657402" ID="ID_1469099296" MODIFIED="1428661660303" POSITION="right" TEXT="Presentaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1428661862091" ID="ID_363661567" MODIFIED="1428662192676">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Terminal Services:</i>&#160;pgm simula funcionamiento de un ordenador por red, dando acceso a usuario
    </p>
  </body>
</html></richcontent>
<node CREATED="1428661921765" ID="ID_1383634639" MODIFIED="1428662282663" TEXT="Pe: Terminal Server (Windows) o Telnet (Linux)"/>
</node>
<node CREATED="1428661899245" ID="ID_1430074539" MODIFIED="1428661983129" TEXT="Mucho +barato que VDI, cliente ligero">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1428661983820" ID="ID_1116799865" MODIFIED="1428661992550" TEXT="Requiere conexi&#xf3;n fiable a servidores">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
</map>
