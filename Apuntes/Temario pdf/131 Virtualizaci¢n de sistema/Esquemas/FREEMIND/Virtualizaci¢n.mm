<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1377863373741" ID="ID_1114099028" MODIFIED="1379410742123" TEXT="Virtualizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377862735833" ID="ID_713441028" MODIFIED="1384879760847" POSITION="right" TEXT="Virtualizaci&#xf3;n">
<node CREATED="1377862791763" ID="ID_1001151828" MODIFIED="1384879830722" TEXT="Hardware">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377862839565" ID="ID_779141499" MODIFIED="1377862842518" TEXT="Tipos">
<node CREATED="1377862842519" ID="ID_1912526266" MODIFIED="1377862917433" TEXT="Virtualizaci&#xf3;n completa">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Simulaci&#243;n completa del hardware que permitir&#225; la instalaci&#243;n del software
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1377862920729" ID="ID_1654857769" MODIFIED="1377862977306" TEXT="Virtualizaci&#xf3;n parcial">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Virtualizaci&#243;n de parte del entorno. Algunos programas pueden requerir modificaciones.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1377862977299" ID="ID_570068811" MODIFIED="1377863060068" TEXT="Paravirtualizaci&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No se simula el entorno hardware. Los programas se ejecutan en entornos aislados, para que no afecten al rendimiento. los programas requieren modificaci&#243;n
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1377863093832" ID="ID_8166460" MODIFIED="1379410736076" TEXT="Hypervisor">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377863107369" ID="ID_1480849011" MODIFIED="1379410000980" TEXT="Puede ser software, firmware o hardware"/>
<node CREATED="1377863125962" ID="ID_861420079" MODIFIED="1377863443527" TEXT="Ejecuta las m&#xe1;quinas virtuales"/>
<node CREATED="1377863158635" ID="ID_1553161567" MODIFIED="1377863299105" TEXT="Clasificaci&#xf3;n">
<node CREATED="1377863163279" ID="ID_1869452955" MODIFIED="1391252169670" TEXT="&quot;bare metal&quot; (KVM, Hyper-V, VMware ESX, Xen)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se ejecuta directamente en el hardware del host para controlarlo y gestionar los sistemas operativos de los huespedes.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1377863244303" ID="ID_1359294423" MODIFIED="1379169372803" TEXT="hosted (Oracle Virtual Box,VMware Workstation)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se ejecutan dentro de un sistema operativo.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1377863300305" ID="ID_1701920387" MODIFIED="1377863305481">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Hyperviseur.png" />
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1379409935955" ID="ID_647985495" MODIFIED="1379409974506" TEXT="Popek y Godbergh">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379409637702" ID="ID_446040630" MODIFIED="1379410687945" TEXT="Propiedades de entorno virtualizado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379409649679" ID="ID_1178776871" MODIFIED="1379410689825" TEXT="Equivalencia/Fidelidad">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Un programa en una m&#225;quina virtual debe exhibir el mismo comportamiento que en una m&#225;quina equivalente.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379409677648" ID="ID_498134717" MODIFIED="1379410693129" TEXT="Control de recursos/Seguridad">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Un monitor de m&#225;quina virtual (VMM) debe tener el control completo de los recursos virtualizados.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1379409780380" ID="ID_1884070210" MODIFIED="1379410695570" TEXT="Eficiencia/Rendimiento">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Una fracci&#243;n dominante de las instrucciones debe poderse ejecutar sin la intervenci&#243;n del VMM
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1379409858600" ID="ID_595430676" MODIFIED="1383226103887" TEXT="Grupos del conjunto de instrucciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379409879673" ID="ID_1064306559" MODIFIED="1379410536775" TEXT="Privilegiadas">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Generan un trap si se encuentra en modo usuario, pero no lo generan si se encuentran en modo supervisor
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1379409894225" ID="ID_1558939566" MODIFIED="1379410076118" TEXT="Sensibles de control">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Intentan cambiar la configuraci&#243;n de los recursos en el sistema
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1379409915011" ID="ID_1571060776" MODIFIED="1379410425752" TEXT="Sensibles de comportamiento">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      El comportamiento o resultado depende de la configuraci&#243;n de los recursos
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1379410437385" ID="ID_1545463714" MODIFIED="1383226105886" TEXT="Teoremas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1379410445822" ID="ID_635179223" MODIFIED="1379410585792" TEXT="1">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <pre style="padding-left: 0; letter-spacing: normal; text-indent: 0px; color: rgb(0, 0, 0); padding-right: 0; text-transform: none; padding-top: 0; padding-bottom: 0; font-variant: normal; text-align: start; line-height: 1.3em; font-style: normal; font-weight: normal; word-spacing: 0px; font-size: 13px; font-family: monospace, Courier; background-color: rgb(249, 249, 249)"> <font size="3">Para cualquier computadora convencional de tercera generaci&#243;n, se puede construir un VMM <i>efectivo</i> si el conjunto de instrucciones sensibles es un subconjunto de las instrucciones privilegiadas.</font></pre>
    <p>
      
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1379410588263" ID="ID_735662488" MODIFIED="1379410598511" TEXT="2">
<node CREATED="1379410604479" ID="ID_377537240" MODIFIED="1379410622022" TEXT="Recursivamente virtualizable">
<node CREATED="1379410627521" ID="ID_1333932638" MODIFIED="1379410641999" TEXT="Es virtualizable">
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1379410643625" ID="ID_376556263" MODIFIED="1379410682953" TEXT="Se puede construir un VMM sin ninguna dependencia de virtualizaci&#xf3;n">
<icon BUILTIN="full-2"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
