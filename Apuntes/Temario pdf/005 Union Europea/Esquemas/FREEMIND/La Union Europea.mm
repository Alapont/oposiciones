<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1386405333127" ID="ID_393721616" MODIFIED="1386405355614" TEXT="Tema 5. la Uni&#xf3;n Europea">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386405352182" FOLDED="true" ID="ID_558557554" MODIFIED="1394396950303" POSITION="right" TEXT="Antecedentes, evoluci&#xf3;n y objetivos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386406484837" ID="ID_626500251" MODIFIED="1393847283087" TEXT="Primeros intentos">
<node CREATED="1386406446258" ID="ID_1666865129" MODIFIED="1386406499113" TEXT="Richard Coudenhove-Kalergi">
<node CREATED="1386406464364" ID="ID_693481866" MODIFIED="1386406515384" TEXT="Uni&#xf3;n Paneuropea (1923)"/>
</node>
<node CREATED="1386406537183" ID="ID_1488542059" MODIFIED="1386406548487" TEXT="Aristide Briand">
<node CREATED="1386406548487" ID="ID_652507311" MODIFIED="1386406556942" TEXT="Uni&#xf3;n Europea (1929)"/>
</node>
</node>
<node CREATED="1386405646587" FOLDED="true" ID="ID_451094184" MODIFIED="1394387898281" TEXT="Cooperaci&#xf3;n militar">
<node CREATED="1386405669369" ID="ID_345705065" MODIFIED="1386405746645" TEXT="Tratado de Dunkerque (1947)">
<node CREATED="1386405683775" ID="ID_1190947958" MODIFIED="1386405749968" TEXT="Francia y Gran Breta&#xf1;a "/>
<node CREATED="1386405833426" ID="ID_1440250015" MODIFIED="1386405853942" TEXT="Compromiso de asistencia mutua"/>
</node>
<node CREATED="1386405723727" ID="ID_67172564" MODIFIED="1386405764024" TEXT="Tratado de Bruselas (1948)">
<node CREATED="1386405766058" ID="ID_1524386777" MODIFIED="1386405778743" TEXT="Se suma el Benelux"/>
<node CREATED="1386405779700" ID="ID_667807278" MODIFIED="1386405821245" TEXT="Uni&#xf3;n Europea Occidental (UEO)  en 1954"/>
</node>
</node>
<node CREATED="1386405825400" ID="ID_359099892" MODIFIED="1386782507720" TEXT="Cooperaci&#xf3;n econ&#xf3;mica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386405871436" ID="ID_593557869" MODIFIED="1386782515770" TEXT="Plan Marshall">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386405944374" ID="ID_1875970342" MODIFIED="1386406028265" TEXT="Organizaci&#xf3;n Europea de Cooperaci&#xf3;n Econ&#xf3;mica (OECE) (1948)">
<node CREATED="1386405978782" ID="ID_343069472" MODIFIED="1389374034725" TEXT="Organizaci&#xf3;n para la Cooperaci&#xf3;n y el Desarrollo Econ&#xf3;mico (OCDE) (1961)"/>
</node>
</node>
</node>
<node CREATED="1386406077933" FOLDED="true" ID="ID_61895401" MODIFIED="1394388302713" TEXT="Iniciativas pol&#xed;ticas">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1386406088144" ID="ID_1305762141" MODIFIED="1386443556652" TEXT="Congreso de Europa de la Haya (1948)">
<node CREATED="1386406104378" ID="ID_486952395" MODIFIED="1386406117872" TEXT="Estatutos del Consejo de Europa"/>
<node CREATED="1386406642280" ID="ID_158515321" MODIFIED="1386406661689" TEXT="Organizaci&#xf3;n europea de cooperaci&#xf3;n pol&#xed;tica"/>
<node CREATED="1386406663348" ID="ID_1838965458" MODIFIED="1386406671384" TEXT="Cuestiones de inter&#xe9;s com&#xfa;n"/>
</node>
<node CREATED="1386406139452" ID="ID_985946517" MODIFIED="1386443578149" TEXT="Creaci&#xf3;n de la CECA">
<node CREATED="1386406147558" ID="ID_1334032560" MODIFIED="1386406180615" TEXT="Declaraci&#xf3;n Schuman (9 de mayo de 1950)">
<node CREATED="1386406194208" ID="ID_1558471906" MODIFIED="1386406217751" TEXT="Producci&#xf3;n franco-alemana de carb&#xf3;n y del acero"/>
<node CREATED="1386406218084" ID="ID_952068026" MODIFIED="1386406228663" TEXT="Alta Autoridad com&#xfa;n"/>
<node CREATED="1386406712917" ID="ID_743948922" MODIFIED="1386406728488" TEXT="Establecimiento de unas bases comunes de desarrollo econ&#xf3;mico"/>
<node CREATED="1386406750537" ID="ID_1495514276" MODIFIED="1386406768089" TEXT="Unificaci&#xf3;n de algunos sectores de la econom&#xed;a"/>
</node>
<node CREATED="1386406238419" ID="ID_1805974751" MODIFIED="1386406276079" TEXT="Acogido por Francia, Alemania, Italia y BENELUX"/>
<node CREATED="1386406281545" ID="ID_765579085" MODIFIED="1386406368502" TEXT="Tratado de Par&#xed;s de 18 de abril de 1951"/>
</node>
<node CREATED="1386406308447" ID="ID_1953543659" MODIFIED="1386443609068" TEXT="Comunidades Europeas">
<node CREATED="1386406333324" ID="ID_254954108" MODIFIED="1386406348674" TEXT="Tratados de Roma de 25 de marzo de 1957"/>
<node CREATED="1386406349663" ID="ID_184805323" MODIFIED="1386406353580" TEXT="CEE"/>
<node CREATED="1386406354600" ID="ID_690533941" MODIFIED="1386408148358" TEXT="CEEA (Euratom)"/>
</node>
</node>
<node CREATED="1386406801416" ID="ID_1021249742" MODIFIED="1394389269722" TEXT="Objetivos">
<node CREATED="1386406812734" ID="ID_762155480" MODIFIED="1386444537941" TEXT="ESPACIO de libertad, seguridad y justicia sin fronteras interiores">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1386406843910" FOLDED="true" ID="ID_1060109754" MODIFIED="1394396918932" TEXT="MERCADO interior">
<icon BUILTIN="button_ok"/>
<node CREATED="1386406862001" ID="ID_399128043" MODIFIED="1386443922129" TEXT="Desarrollo SOSTENIBLE"/>
<node CREATED="1386406869931" ID="ID_1968829004" MODIFIED="1386443930241" TEXT="CRECIMIENTO econ&#xf3;mico equilibrado"/>
<node CREATED="1386406880610" ID="ID_128672912" MODIFIED="1386443937573" TEXT="Estabilidad de los PRECIOS"/>
<node CREATED="1386406912129" ID="ID_959212834" MODIFIED="1386443944406" TEXT="Pleno EMPLEO y progreso social"/>
<node CREATED="1386406922511" ID="ID_1087792733" MODIFIED="1386443954827" TEXT="Protecci&#xf3;n del MEDIO AMBIENTE"/>
</node>
<node CREATED="1386406945124" FOLDED="true" ID="ID_692012169" MODIFIED="1394389287167" TEXT="Combatir&#xe1; la exclusi&#xf3;n social y la discriminaci&#xf3;n">
<icon BUILTIN="button_ok"/>
<node CREATED="1386406997736" ID="ID_1990760670" MODIFIED="1386407005179" TEXT="Justicia y protecci&#xf3;n sociales"/>
<node CREATED="1386407007072" ID="ID_1245487743" MODIFIED="1386407015919" TEXT="Igualdad entre hombres y mujeres"/>
<node CREATED="1386407100969" ID="ID_720436382" MODIFIED="1386407111002" TEXT="Protecci&#xf3;n de los derechos del ni&#xf1;o"/>
</node>
<node CREATED="1386407064285" ID="ID_1458982502" MODIFIED="1386444640044" TEXT="COHESION econ&#xf3;mica, social, territorial y la solidaridad">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1386407117279" ID="ID_780229348" MODIFIED="1386444644926" TEXT="Uni&#xf3;n Econ&#xf3;mica y Monetaria (UEM)">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1386407166738" FOLDED="true" ID="ID_1369785404" MODIFIED="1394389263952" TEXT="Actualidad">
<node CREATED="1386407178308" ID="ID_1057932672" MODIFIED="1386407416902" TEXT="28 Estados Miembros"/>
<node CREATED="1386407357989" ID="ID_66395748" MODIFIED="1386407458019" TEXT="Bulgar&#xed;a y Ruman&#xed;a el 1 de Enero de 2007 (Tratado de Luxemburgo)"/>
<node CREATED="1386407332257" ID="ID_1823485953" MODIFIED="1386407345566" TEXT="Croacia, el &#xfa;ltimo en incorporarse en julio de 2013"/>
</node>
</node>
<node CREATED="1386407439176" ID="ID_184556136" MODIFIED="1394435024860" POSITION="right" TEXT="Tratados originarios y modificativos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386407542206" ID="ID_1853177846" MODIFIED="1393797863441" TEXT="Tratados originarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386407562429" FOLDED="true" ID="ID_1403741273" MODIFIED="1394389230409" TEXT="Tratado de Par&#xed;s ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386407572808" ID="ID_543885042" MODIFIED="1386407582506" TEXT="18 de abril de 1951 (CECA)"/>
<node CREATED="1386408751765" FOLDED="true" ID="ID_1980312930" MODIFIED="1394388754730" TEXT="Objetivos">
<node CREATED="1386408764607" ID="ID_844259138" MODIFIED="1386408779889" TEXT="econ&#xf3;micos"/>
<node CREATED="1386408780535" ID="ID_559305780" MODIFIED="1386408783926" TEXT="pol&#xed;ticos"/>
</node>
<node CREATED="1386408785285" FOLDED="true" ID="ID_1171425028" MODIFIED="1394388752398" TEXT="Instituciones">
<node CREATED="1386408797366" ID="ID_1843356176" MODIFIED="1386408802057" TEXT="Alta Autoridad">
<node CREATED="1386444872193" ID="ID_1096664581" MODIFIED="1386444880042" TEXT="Personas independientes"/>
<node CREATED="1386444880859" ID="ID_287113165" MODIFIED="1386444892858" TEXT="Recursos financieros propios"/>
<node CREATED="1386444893144" ID="ID_137770399" MODIFIED="1386444904269" TEXT="Poderes vinculantes"/>
</node>
<node CREATED="1386408815749" ID="ID_1022405515" MODIFIED="1386408822314" TEXT="Consejo">
<node CREATED="1386444828071" ID="ID_1133371174" MODIFIED="1386444838772" TEXT="Representantes de los gobiernos"/>
<node CREATED="1386444839558" ID="ID_1883595196" MODIFIED="1386444854801" TEXT="Funciones de propuesta y consulta"/>
</node>
<node CREATED="1386408822701" ID="ID_1582009305" MODIFIED="1386408828554" TEXT="Asamblea Com&#xfa;n">
<node CREATED="1386444920717" ID="ID_143001364" MODIFIED="1386444930688" TEXT="Parlamentos nacionales"/>
<node CREATED="1386444930959" ID="ID_718138604" MODIFIED="1386444945048" TEXT="poderes de control pol&#xed;tico sobre la Alta Autoridad"/>
</node>
<node CREATED="1386408802448" ID="ID_1355583745" MODIFIED="1386408809815" TEXT="Tribunal de Justicia"/>
</node>
</node>
<node CREATED="1386407585791" FOLDED="true" ID="ID_7751752" MODIFIED="1394389229196" TEXT="Tratados de Roma">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386407596974" ID="ID_1704436639" MODIFIED="1392578713680" TEXT="25 de marzo de 1957 (en vigor el 1 de enero de 1958)"/>
<node CREATED="1386444782823" ID="ID_169068939" MODIFIED="1386444818032" TEXT="Francia, Italia, Alemania y el Benelux (mismos que la CECA)"/>
<node CREATED="1386407911273" ID="ID_506170714" MODIFIED="1393848385804" TEXT="TCEE (m&#xe1;s tarde TFUE)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386408881426" FOLDED="true" ID="ID_1699654797" MODIFIED="1394388748120" TEXT="Uni&#xf3;n aduanera, basada en ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386408898939" ID="ID_300019622" MODIFIED="1386444722131" TEXT="Libre circulaci&#xf3;n de los factores de producci&#xf3;n (capital, trabajadores, mercanc&#xed;as)"/>
<node CREATED="1386408915884" ID="ID_1886927602" MODIFIED="1386408919694" TEXT="Libre competencia"/>
</node>
<node CREATED="1386408930749" FOLDED="true" ID="ID_1800371609" MODIFIED="1394388747178" TEXT="Politicas sectoriales comunes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386408945707" ID="ID_1869569742" MODIFIED="1386408954257" TEXT="Agricultura"/>
<node CREATED="1386408954711" ID="ID_419330525" MODIFIED="1386408958270" TEXT="Transporte "/>
<node CREATED="1386408959089" ID="ID_1289109236" MODIFIED="1386408966988" TEXT="Relaciones comerciales"/>
</node>
<node CREATED="1386408980636" ID="ID_927950485" MODIFIED="1394389191632" TEXT="Instituciones de la CEE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386408985065" ID="ID_741887266" MODIFIED="1393833089538" TEXT="Consejo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386409049555" ID="ID_2793956" MODIFIED="1386409063051" TEXT="Representantes de los Gobiernos"/>
<node CREATED="1386409063302" ID="ID_1687019123" MODIFIED="1386409070326" TEXT="Decisiones por mayor&#xed;a simple"/>
</node>
<node CREATED="1386409025255" ID="ID_1331473382" MODIFIED="1393833091418" TEXT="Comisi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386409080811" ID="ID_1627480550" MODIFIED="1393832908518" TEXT="Poderes normativos de car&#xe1;cter ejecutivo y aplicativo"/>
<node CREATED="1386409103651" ID="ID_1598065952" MODIFIED="1386445071548" TEXT="Facultades de propuesta"/>
</node>
<node CREATED="1386409028756" ID="ID_1637932255" MODIFIED="1393833093649" TEXT="Asamblea (similar a la CECA)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386409040062" ID="ID_1014943127" MODIFIED="1393833095713" TEXT="Tribunal de justicia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1386407915297" ID="ID_1855006888" MODIFIED="1386444703629" TEXT="TCEEA (Euratom)"/>
</node>
</node>
<node CREATED="1386407548823" ID="ID_980649157" MODIFIED="1394395533661" TEXT="Tratados modificativos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386408017514" FOLDED="true" ID="ID_770021353" MODIFIED="1394533905968" TEXT="Acta Un&#xed;ca Europea (28 de febrero de 1986)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Con el Acta Unica Europea se pretende dar un mayor protagonismo a las Instituciones Comunitarias y a losrepresentantes de los pueblos que a los propios gobiernos, dotando de esta forma a las decisiones de una mayor legitimidad.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386409416462" ID="ID_777628356" MODIFIED="1386409432256" TEXT="La Haya, 28 de febrero de 1986"/>
<node CREATED="1386410484192" ID="ID_1175658555" MODIFIED="1386410502009" TEXT="Pasar del Mercado Com&#xfa;n a Un Mercado Interior">
<node CREATED="1392578958077" ID="ID_851359031" MODIFIED="1392578980058" TEXT="De eliminaci&#xf3;n de aranceles"/>
<node CREATED="1392578980469" ID="ID_153599531" MODIFIED="1392578991235" TEXT="A una verdadera circulaci&#xf3;n"/>
</node>
<node CREATED="1386409259695" ID="ID_583310454" MODIFIED="1386409266130" TEXT="Acciones">
<node CREATED="1386409266132" ID="ID_720800561" MODIFIED="1393851357591">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Introduce el procedimiento de<b>&#160;cooperaci&#243;n</b>&#160;en el Parlamento
    </p>
  </body>
</html></richcontent>
<node CREATED="1392579075887" ID="ID_217410420" MODIFIED="1392583151417" TEXT="El Consejo necesita unanimidad">
<icon BUILTIN="forward"/>
<node CREATED="1392579087503" ID="ID_1340172303" MODIFIED="1392583156424" TEXT="Para modificar un acta de la Comisi&#xf3;n con enmiendas del Parlamento">
<icon BUILTIN="forward"/>
</node>
</node>
</node>
<node CREATED="1386409320142" ID="ID_1417048756" MODIFIED="1386409335344" TEXT="Se reducen los &#xe1;mbitos de unanimidad en el Consejo">
<node CREATED="1392579131283" ID="ID_212374734" MODIFIED="1392583160543" TEXT="En beneficio de una mayor&#xed;a cualificada">
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1386409341442" ID="ID_1814560731" MODIFIED="1392579175511" TEXT="Se autoriza la delegaci&#xf3;n de competencias de ejecuci&#xf3;n del Consejo en la Comisi&#xf3;n"/>
<node CREATED="1386409394124" ID="ID_1260864100" MODIFIED="1386446109124" TEXT="Se crea el Tribunal de Primera Instancia"/>
</node>
</node>
<node CREATED="1386407709309" FOLDED="true" ID="ID_1115602753" MODIFIED="1394533898418" TEXT="Tratado de la Uni&#xf3;n Europea o TUE (Maastricht, 7 de febrero de 1992)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      El Tratado de la Uni&#243;n Europea pretende centrarse en lograr los siguientes objetivos:
    </p>
    <p>
      - Un Mercado interior.
    </p>
    <p>
      - La cohesi&#243;n econ&#243;mica, social y territorial, con la Ciudadan&#237;a de la Uni&#243;n.
    </p>
    <p>
      - La uni&#243;n econ&#243;mica y monetaria, con la creaci&#243;n de la moneda &#250;nica.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386409461824" ID="ID_791859857" MODIFIED="1386783754871" TEXT="Pilares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386409464729" ID="ID_168702107" MODIFIED="1386783756618" TEXT="TCECA, TCEE y TCEEA">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1386409541455" ID="ID_797890453" MODIFIED="1389452328814" STYLE="bubble" TEXT="UE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1386409486913" ID="ID_249988483" MODIFIED="1389376483265" TEXT="Pol&#xed;tica Exterior y Seguridad Com&#xfa;n (PESC)   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1386409507046" ID="ID_616136213" MODIFIED="1386783758007" TEXT="Cooperaci&#xf3;n en Asuntos de Justicia e Interior (CAJI)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
</node>
</node>
<node CREATED="1386409973072" ID="ID_124874549" MODIFIED="1394396315432" TEXT="Objetivos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386409992455" ID="ID_724578519" MODIFIED="1389466546984" TEXT="Uni&#xf3;n Econ&#xf3;mica y Monetaria (UEM)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386783087891" ID="ID_419867886" MODIFIED="1386783092916" TEXT="Econ&#xf3;mica">
<node CREATED="1392585894041" ID="ID_927079264" MODIFIED="1392585904136" TEXT="Creaci&#xf3;n del Mercado Interior"/>
</node>
<node CREATED="1386783094575" ID="ID_92741906" MODIFIED="1386783099437" TEXT="Monetaria">
<node CREATED="1386783099437" ID="ID_327433520" MODIFIED="1393082398655" TEXT="Se establecen los Criterios de Convergencia">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Son ( o eran) 4, b&#225;sicamente los que quedan es que el deficit p&#250;blico no debe superar el 3% del PIB y la deuda no debe superar el 60 %
    </p>
  </body>
</html></richcontent>
<arrowlink DESTINATION="ID_211305257" ENDARROW="Default" ENDINCLINATION="886;0;" ID="Arrow_ID_49080118" STARTARROW="None" STARTINCLINATION="886;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386783120518" ID="ID_304151391" MODIFIED="1389452321948" TEXT="Creaci&#xf3;n del BCE y el SEBC (Sistema Europeo de Bancos Centrales)"/>
</node>
</node>
<node CREATED="1386410011511" ID="ID_1383081588" MODIFIED="1392579243870" TEXT="Ciudadan&#xed;a de la Uni&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386410025623" ID="ID_507745501" MODIFIED="1394390483514" TEXT="Amplia libre circulaci&#xf3;n a las personas (no s&#xf3;lo a trabajadores)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386410038203" ID="ID_573260252" MODIFIED="1394390490191" TEXT="Derecho a votar y ser votados en los pa&#xed;ses"/>
<node CREATED="1386410068833" ID="ID_424843751" MODIFIED="1386451218238" TEXT="Petici&#xf3;n, protecci&#xf3;n diplom&#xe1;tica y Defensor del Pueblo Europeo"/>
</node>
<node CREATED="1386410121943" ID="ID_1705673321" MODIFIED="1389376674053" TEXT="Fortalecimiento de la Cohesi&#xf3;n Econ&#xf3;mica y Social  ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386410146786" ID="ID_256678029" MODIFIED="1393839443982" TEXT="Nuevas pol&#xed;ticas comunes">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cultura, salud p&#250;blica, educaci&#243;n, protecci&#243;n de los consumidores, pol&#237;tica industrial, transporte
    </p>
    <p>
      terrestre, mar&#237;timo y a&#233;reo, energ&#237;a, telecomunicaciones y cooperaci&#243;n al desarrollo.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386410179102" ID="ID_1873269858" MODIFIED="1392583112635" TEXT="Aumento de las competencias comunitarias ">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Medio ambiente, I+D+I, pol&#237;tica social
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1386783454129" ID="ID_217928528" MODIFIED="1394392038752" TEXT="Instituciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386783473046" ID="ID_1893625661" MODIFIED="1386783864306" TEXT="Parlamento Europeo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386783487031" ID="ID_443276894" MODIFIED="1386783505191" TEXT="Tiene el derecho de presentar iniciativas legislativas"/>
<node CREATED="1386783532723" ID="ID_593408773" MODIFIED="1389438371297" TEXT="Procedimiento de codecisi&#xf3;n (Consejo -Parlamento)">
<arrowlink DESTINATION="ID_1215082863" ENDARROW="Default" ENDINCLINATION="394;0;" ID="Arrow_ID_1201549521" STARTARROW="None" STARTINCLINATION="394;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386783548409" ID="ID_1241969293" MODIFIED="1386783559284" TEXT="Amplia sus poderes sobre la Comisi&#xf3;n"/>
</node>
<node CREATED="1386783563860" ID="ID_861851746" MODIFIED="1386783858690" TEXT="Se configura el Consejo Europeo como m&#xe1;ximo &#xf3;rgano pol&#xed;tico">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386783595505" ID="ID_1826308604" MODIFIED="1386783861062" TEXT="Se reconoce el Tribunal de Cuentas como instituci&#xf3;n europea">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386783658913" ID="ID_1579478121" MODIFIED="1386783861748" TEXT="Se crea el Comit&#xe9; de Regiones, con car&#xe1;cter consultivo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1386410245020" ID="ID_1605505042" MODIFIED="1386410248072" TEXT="Fases">
<node CREATED="1386410248072" ID="ID_959107204" MODIFIED="1392579917324" TEXT="Primera">
<icon BUILTIN="full-1"/>
<node CREATED="1386410255614" ID="ID_1800484708" MODIFIED="1386413644618" TEXT="Construcci&#xf3;n del Mercado Interior (31 de diciembre de 1993)"/>
</node>
<node CREATED="1386410300611" ID="ID_641620392" MODIFIED="1392579921631" TEXT="Segunda">
<icon BUILTIN="full-2"/>
<node CREATED="1386410318264" ID="ID_1288312411" MODIFIED="1393839720262" TEXT="Creaci&#xf3;n del Instituto Monetario Europeo (31 de diciembre de 1998)">
<node CREATED="1386410335603" ID="ID_260822866" MODIFIED="1386410372872" TEXT="Estabilidad de los precios"/>
</node>
</node>
<node CREATED="1386410385997" ID="ID_1001083220" MODIFIED="1392579924097" TEXT="Tercera">
<icon BUILTIN="full-3"/>
<node CREATED="1386410420208" ID="ID_642924773" MODIFIED="1389467367375" TEXT="Creaci&#xf3;n del BCE (1 de junio de 1998) y el SEBC (Sustituye al Instituto Monetario Europeo)"/>
<node CREATED="1386410393948" ID="ID_155149263" MODIFIED="1386410419625" TEXT="Implantaci&#xf3;n de la Moneda Unica (1 de enero de 2002)">
<node CREATED="1386410534433" ID="ID_569976660" MODIFIED="1386410548117" TEXT="Excepciones de Reino Unido, Dinamarca y Suecia"/>
<node CREATED="1392582369891" ID="ID_702716339" MODIFIED="1392582393403" TEXT="Actualmente 16 Estados tienen como moneda oficial el euro"/>
</node>
</node>
</node>
</node>
<node CREATED="1386408029239" FOLDED="true" ID="ID_721024413" MODIFIED="1394533883072" TEXT="Tratado de Amsterdam (2 de octubre de 1997)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Con el tratado de Amsterdam, se potencian los otros dos pilares que se hab&#237;an definido en el TUE, el PESC y el CAJI, pasando tener una pol&#237;tica exterior y de seguridad com&#250;n.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1392583065591" ID="ID_25153919" MODIFIED="1392583100810" TEXT="Instituciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1392582508209" ID="ID_874572312" MODIFIED="1392582558108" TEXT="Parlamento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386782272643" ID="ID_1215082863" MODIFIED="1392582523702" TEXT="Se potencia el procedimiento de codecisi&#xf3;n frente a la cooperaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1392582524300" ID="ID_1441946784" MODIFIED="1394437301266" TEXT="Aprobar&#xe1; al candidato a Presidente de la Comisi&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Una vez se ha elegido al Parlamento Europeo, el Consejo propone, tras consulta con los Grupos Parlamentarios, a un Presidente de la Comisi&#243;n, que se presentar&#225; para su investidura por el Parlamento
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1392583073711" ID="ID_609330823" MODIFIED="1392583106130" TEXT="Se refuerzan">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1392583078073" ID="ID_486222325" MODIFIED="1392583109000" TEXT="Tribunal de Justicia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1392583084132" ID="ID_936296912" MODIFIED="1392583107737" TEXT="Tribunal de Cuentas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1392582693919" ID="ID_963421741" MODIFIED="1392584252549" TEXT="Se crea el alto representante para la PESC (2&#xba; Pilar)">
<arrowlink DESTINATION="ID_1197541479" ENDARROW="Default" ENDINCLINATION="393;0;" ID="Arrow_ID_312331365" STARTARROW="None" STARTINCLINATION="393;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="male1"/>
<node CREATED="1392582711807" ID="ID_1542762657" MODIFIED="1392582742243" TEXT="Asistir&#xe1; al Consejo y su Presidencia en materia PESC">
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1392582850466" ID="ID_1988986831" MODIFIED="1392582888087" TEXT="CAJI (3&#xba; Pilar)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386411085963" ID="ID_1081553885" MODIFIED="1392582302431" TEXT="Traslado a la UE de Asilo, inmigraci&#xf3;n, control de fronteras exteriores y cooperaci&#xf3;n judicial   ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1394391052336" ID="ID_919714208" MODIFIED="1394391084630" TEXT="En 2004 se crear&#xe1; la agencia FRONTEX para el control de las fronteras exteriores"/>
<node CREATED="1394391136943" ID="ID_1962369354" MODIFIED="1394391151984" TEXT="En ese mismo a&#xf1;o se crea EUROPOL"/>
</node>
<node CREATED="1392582872594" ID="ID_1452633109" MODIFIED="1392583010962" TEXT="Tratado de Schengen">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1394391384165" ID="ID_573788668" MODIFIED="1394391408057" TEXT="Cruce de fronteras de los residentes de Estados Miembros"/>
</node>
<node CREATED="1392582969199" ID="ID_197565693" MODIFIED="1392583012319" TEXT="Se renombra como CPJP (Cooperaci&#xf3;n Policial y Judicial en materia Penal)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1392582641542" ID="ID_1178265377" MODIFIED="1392582668704" TEXT="Se introduce un t&#xed;tulo dedicado al empleo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#ff3333" CREATED="1392724274274" ID="ID_211305257" MODIFIED="1393082449227" TEXT="Pacto de Estabilidad y Crecimiento">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Su objetivo es prevenir la aparici&#243;n de un deficit excesivo en la UE tras la entrada en vigor del euro, viene a recoger el mantenimiento de los criterios de convergencia acordados en Maastricht.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1392724389746" ID="ID_1036677637" MODIFIED="1392724589120" TEXT="Supervisi&#xf3;n multilateral de situaciones presupuestarias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1392724405746" ID="ID_1877505849" MODIFIED="1392725506914" TEXT="Procedimiento de deficit excesivo (&gt; 3% PIB)">
<arrowlink DESTINATION="ID_932015978" ENDARROW="Default" ENDINCLINATION="530;0;" ID="Arrow_ID_30610703" STARTARROW="None" STARTINCLINATION="530;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1386408054153" FOLDED="true" ID="ID_669307032" MODIFIED="1394437329929" TEXT="Tratado de Niza (26 de febrero de 2001)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386411246300" FOLDED="true" ID="ID_1997116951" MODIFIED="1394396448964" TEXT="Modifica la estructura institucional (Adhesi&#xf3;n de 10 nuevos estados miembros)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386411322022" ID="ID_151549002" MODIFIED="1392583531987" TEXT="Nuevo reparto de esca&#xf1;os para 27 miembros en el Parlamento (732)"/>
<node CREATED="1386411330366" ID="ID_1412888255" MODIFIED="1392583924780" TEXT="Nuevo sistema de toma de decisiones por mayor&#xed;a cualificada (hasta 2014)">
<arrowlink DESTINATION="ID_1360745665" ENDARROW="Default" ENDINCLINATION="672;0;" ID="Arrow_ID_1513257088" STARTARROW="None" STARTINCLINATION="672;0;"/>
<node CREATED="1392629447703" ID="ID_1450387006" MODIFIED="1392629455964" TEXT="Triple mayor&#xed;a">
<node CREATED="1392629455965" ID="ID_996930466" MODIFIED="1392629459772" TEXT="De miembros"/>
<node CREATED="1392629460117" ID="ID_1936440965" MODIFIED="1392629462884" TEXT="De Estados"/>
<node CREATED="1392629463677" ID="ID_236476971" MODIFIED="1392629481004" TEXT="De poblaci&#xf3;n (cuando lo solicite alg&#xfa;n miembro)"/>
</node>
</node>
</node>
</node>
<node CREATED="1386782375188" FOLDED="true" ID="ID_89423422" MODIFIED="1394396624895" TEXT="Constituci&#xf3;n Europea (2004)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386782386492" HGAP="27" ID="ID_1961358266" MODIFIED="1392583226242" TEXT="Fue  rechazada" VSHIFT="-28">
<node CREATED="1392583216787" ID="ID_1580635603" MODIFIED="1392583224604" TEXT="Voto en contra de Francia"/>
<node CREATED="1394396453041" ID="ID_1049326859" MODIFIED="1394396468532" TEXT="Voto en contra de los Pa&#xed;ses Bajos"/>
</node>
</node>
<node CREATED="1386408067354" FOLDED="true" ID="ID_1877251025" MODIFIED="1394534095297" TEXT="Tratado de Lisboa (13 de diciembre de 2007)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entra en vigor en diciembre de 2009.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386411534191" ID="ID_1364241384" MODIFIED="1386411549372" TEXT="TCE">
<node CREATED="1386411549374" ID="ID_1254139995" MODIFIED="1386411552053" TEXT="TFUE"/>
</node>
<node CREATED="1386411571244" ID="ID_140091318" MODIFIED="1386411603126" TEXT="Desaparece la estructura de pilares"/>
<node COLOR="#ff0000" CREATED="1386413532431" ID="ID_326252468" MODIFIED="1393842457113" TEXT="Atribuye personalidad JURIDICA &#xfa;nica a la Uni&#xf3;n  ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386413881626" FOLDED="true" ID="ID_1667584233" MODIFIED="1394533925752" TEXT="Creaci&#xf3;n de figuras">
<node CREATED="1386413888028" ID="ID_180797818" MODIFIED="1386413919769" TEXT="Presidente del Consejo Europeo">
<node CREATED="1392670998970" ID="ID_1946965527" MODIFIED="1392671005651" TEXT="Nombrado por sus miembros"/>
<node CREATED="1392671022927" ID="ID_1455307549" MODIFIED="1392671026983" TEXT="Por 36 meses"/>
</node>
<node CREATED="1386413928204" ID="ID_1197541479" MODIFIED="1392584252549" TEXT="Alto Representante de la UE para Asuntos Exteriores y Pol&#xed;tica de Seguridad">
<node CREATED="1392584210413" ID="ID_1905113112" MODIFIED="1392584217874" TEXT="Vicepresidente de la Comisi&#xf3;n"/>
<node CREATED="1392584218638" ID="ID_542402320" MODIFIED="1392584237788" TEXT="Servicio de Acci&#xf3;n Exterior"/>
</node>
</node>
<node CREATED="1386413783119" FOLDED="true" ID="ID_1653842522" MODIFIED="1394396540110" TEXT="Aumenta el n&#xfa;mero de instituciones de 5 a 7">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Consejo Europeo (Formado por los jefes de Estado y de Gobierno y el Presidente de la Comisi&#243;n), Consejo, Comisi&#243;n,Parlamento Europeo, Tribunal de Justicia de la Uni&#243;n Europea (TillE), Tribunal de Cuentas y BCE
    </p>
  </body>
</html></richcontent>
<node CREATED="1389438605961" ID="ID_536804972" MODIFIED="1389438608730" TEXT="Antes">
<node CREATED="1389438608730" ID="ID_333129090" MODIFIED="1389438618948" TEXT="Consejo"/>
<node CREATED="1389438619313" ID="ID_255478436" MODIFIED="1389438623090" TEXT="Parlamento"/>
<node CREATED="1389438623470" ID="ID_1930696490" MODIFIED="1389438627154" TEXT="Comisi&#xf3;n"/>
<node CREATED="1389438641246" ID="ID_733795638" MODIFIED="1389438659719" TEXT="Tribunal de Cuentas"/>
<node CREATED="1389438698561" ID="ID_1345174870" MODIFIED="1389438712338" TEXT="Tribunal de Justicia"/>
</node>
<node CREATED="1389438662813" ID="ID_1237680269" MODIFIED="1389467711112" TEXT="Ahora adem&#xe1;s se reconocen como comunitarias">
<node CREATED="1389438666848" ID="ID_1096738517" MODIFIED="1392671042208" TEXT="Consejo Europeo (no recibe nuevas atribuciones)"/>
<node CREATED="1389438676651" ID="ID_43633090" MODIFIED="1389438691348" TEXT="BCE"/>
</node>
</node>
<node CREATED="1386413992184" ID="ID_1360745665" LINK="Tema%206.%20Las%20instituciones%20de%20la%20UE.mm" MODIFIED="1394533806135" TEXT="Se cambia la mayor&#xed;a cualificada en el Consejo">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      55 % de los Estados que representen el 65 % de la poblaci&#243;n
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1394531815224" ID="ID_851978621" MODIFIED="1394531865653" TEXT="Se nombra al procedimiento de codecisi&#xf3;n como procedimiento ordinario en el Parlamento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1394531852534" ID="ID_898811355" MODIFIED="1394531861005" TEXT="Para la adopci&#xf3;n de actos"/>
</node>
<node CREATED="1386784047363" ID="ID_928686837" MODIFIED="1394533780590" TEXT="Se cambia el n&#xfa;mero de Comisarios a 2/3 del n&#xfa;mero de los Estados miembros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1392670908481" ID="ID_432053049" MODIFIED="1394533782662" TEXT="Permite la ampliaci&#xf3;n m&#xe1;s all&#xe1; de 27 pa&#xed;ses">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1394451197603" ID="ID_1787677887" MODIFIED="1394451240849" TEXT="Vuleve a disminuir el n&#xfa;mero de decisiones que se toman por unanimidad en favor de las que se toman por mayor&#xed;a cualificada"/>
<node CREATED="1394451399883" ID="ID_567516740" MODIFIED="1394531770546" TEXT="Se refuerza el papel de los Parlamentos Nacionales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1394451469135" ID="ID_709986605" MODIFIED="1394451485490" TEXT="Ser&#xe1;n informados de los proyectos legislativos"/>
<node CREATED="1394451518075" ID="ID_1119086987" MODIFIED="1394451530466" TEXT="Participar&#xe1;n en el control de Europol y Eurojust"/>
</node>
<node CREATED="1394451546299" ID="ID_447529938" MODIFIED="1394451560559" TEXT="El Tribunal de Justicia amplia sus competencias ">
<node CREATED="1394451560560" ID="ID_1885513790" MODIFIED="1394451588058" TEXT="Conocer&#xe1; &#xe1;mbitos de cooperaci&#xf3;n penal y policial"/>
</node>
<node CREATED="1386414003843" ID="ID_423557528" MODIFIED="1394533912227" TEXT="La Carta de derechos fundamentales de la UE pasa a tener car&#xe1;cter vinculante">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1394447440355" ID="ID_1919810496" MODIFIED="1394447445785" TEXT="Dignidad"/>
<node CREATED="1394447376291" ID="ID_1770202366" MODIFIED="1394447378657" TEXT="Libertad"/>
<node CREATED="1394447379130" ID="ID_1937038060" MODIFIED="1394447383070" TEXT="Igualdad"/>
<node CREATED="1394447559771" ID="ID_18410959" MODIFIED="1394447564162" TEXT="Solidaridad"/>
<node CREATED="1394447471067" ID="ID_1131195306" MODIFIED="1394447475602" TEXT="Ciudadan&#xed;a"/>
<node CREATED="1394447405251" ID="ID_1671252687" MODIFIED="1394447408786" TEXT="Justicia"/>
</node>
</node>
<node COLOR="#ff3333" CREATED="1386784072331" FOLDED="true" ID="ID_78963356" LINK="../Bloque%20III/Tema%2012.%20El%20modelo%20econ&#xf3;mico%20espa&#xf1;ol.%20Evoluci&#xf3;n.mm" MODIFIED="1394396615989" TEXT="Tratado de Estabilidad, Coordinaci&#xf3;n y Gobernanza en la Uni&#xf3;n Econ&#xf3;mica y Monetaria (2012)   ">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tambi&#233;n llamado Pacto Fiscal. Firmado el 2 de marzo de 2012 por 25 miembros (excepto el Reino Unido y la Rep&#250;blica Checa), con el objeto de controlar las pol&#237;ticas fiscales nacionales mediante una revisi&#243;n del Pacto de Estabilidad y Crecimiento de 1997. Esto llev&#243; a la <b>reforma de la Constituci&#243;n</b>&#160;en su art&#237;culo 135, con la inclusi&#243;n del<b>&#160;Principio de Estabilidad Presupuestaria&#160; </b>en septiembre de 2011 y a la aprobaci&#243;n de la<b>&#160;Ley de Estabilidad Presupuestaria y Sostenibilidad Financiera</b>. Enlaza con el modelo econ&#243;mico de Espa&#241;a.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386784135409" ID="ID_788073065" MODIFIED="1393853549889">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Compromiso de<b>&#160;deficit estructural</b>&#160;&lt;0,5 % del PIB
    </p>
  </body>
</html></richcontent>
<node CREATED="1386784159956" ID="ID_1809801321" MODIFIED="1386784183935" TEXT="&lt; 1% del PIB en pa&#xed;ses con deuda inferior al 60 %"/>
</node>
<node CREATED="1392725572362" ID="ID_436527429" MODIFIED="1392741860355" TEXT="Bajar la deuda p&#xfa;blica por debajo del 60 % en 20 a&#xf1;os"/>
<node CREATED="1392725476026" ID="ID_932015978" MODIFIED="1393853533451" TEXT="Obligaci&#xf3;n de mantener el deficit P&#xda;BLICO &lt; 3% PIB">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1392725509946" ID="ID_533134512" MODIFIED="1392725770333" TEXT="De no hacerse dar&#xe1; lugar a sanciones semi-autom&#xe1;ticas"/>
</node>
<node CREATED="1386784185517" ID="ID_845310474" MODIFIED="1392725750757" TEXT="Llev&#xf3; a la reforma de la Constituci&#xf3;n (Art.135), incluso antes de la entrada en vigor de este Tratado."/>
</node>
</node>
</node>
<node CREATED="1386414137201" FOLDED="true" ID="ID_1823638626" MODIFIED="1394443105905" POSITION="right" TEXT="Derecho de la UE. Fuentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386414383993" ID="ID_645343508" MODIFIED="1394396957892" TEXT="Derecho originario">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386414399871" ID="ID_1645293851" MODIFIED="1386414405107" TEXT="Tratados constitutivos"/>
<node CREATED="1386414405594" ID="ID_1541489770" MODIFIED="1386414416950" TEXT="Tratados de adhesi&#xf3;n"/>
<node CREATED="1386414417587" ID="ID_655689399" MODIFIED="1386414435386" TEXT="Tratados o  convenios internacionales"/>
</node>
<node CREATED="1386414436559" FOLDED="true" ID="ID_845894407" MODIFIED="1394443104739" TEXT="Derecho derivado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386414460173" ID="ID_1226299977" MODIFIED="1386414466127" TEXT="Caracter&#xed;sticas">
<node CREATED="1386414466129" ID="ID_428953875" MODIFIED="1386414472440" TEXT="Procede de la propia UE"/>
<node CREATED="1386414474598" ID="ID_932666525" MODIFIED="1386414489621" TEXT="Responde a la necesidad de adoptar medidas oportunas"/>
<node CREATED="1386414490941" ID="ID_1033509212" MODIFIED="1386414514442" TEXT="Se encuentra sometido a los propios Tratados"/>
</node>
<node CREATED="1386414520476" ID="ID_1710466181" MODIFIED="1386414528294" TEXT="Fuentes">
<node CREATED="1386414528296" ID="ID_814917713" MODIFIED="1386414532133" TEXT="Reglamento">
<node CREATED="1386414559668" ID="ID_72630002" MODIFIED="1386414566506" TEXT="a) Alcance general">
<node CREATED="1386414571406" ID="ID_1365190733" MODIFIED="1386414587519" TEXT="Obligan a los Estados miembros y particulares"/>
<node CREATED="1386414587803" ID="ID_921065347" MODIFIED="1386414610452" TEXT="El &#xe1;mbito se define de manera ABSTRACTA"/>
</node>
<node CREATED="1386414611765" ID="ID_213452324" MODIFIED="1386414675576" TEXT="b) Obligaciones en todos sus elementos">
<node CREATED="1386414632128" ID="ID_1157230109" MODIFIED="1386414670244" TEXT="Resultado y procedimiento"/>
<node CREATED="1386414637708" ID="ID_579989142" MODIFIED="1386414656396" TEXT="Autosuficiente"/>
</node>
<node CREATED="1386414680357" ID="ID_1286955341" MODIFIED="1386414687737" TEXT="c) Directamente aplicable"/>
</node>
<node CREATED="1386414532466" ID="ID_1140792283" MODIFIED="1386414536750" TEXT="Directivas">
<node CREATED="1386414704069" ID="ID_867946273" MODIFIED="1386414714591" TEXT="a) Impone una obligaci&#xf3;n de RESULTADO"/>
<node CREATED="1386414719879" ID="ID_276061753" MODIFIED="1386414742753" TEXT="b) Tiene &#xfa;nicamente por destinatarios los Estados miembros"/>
<node CREATED="1386414743777" ID="ID_1068919235" MODIFIED="1386414765160" TEXT="c) Requiere la intervenci&#xf3;n normativa de los Estados miembros"/>
</node>
<node CREATED="1386414537138" ID="ID_977989478" MODIFIED="1386414539309" TEXT="Decisiones">
<node CREATED="1386414779310" ID="ID_123566483" MODIFIED="1386414791992" TEXT="Carece de car&#xe1;cter general">
<node CREATED="1386414830718" ID="ID_833051670" MODIFIED="1386414842341" TEXT="Sus destinatarios se tienen que concretar"/>
<node CREATED="1386414844626" ID="ID_182171095" MODIFIED="1386414850730" TEXT="Obliga en todos sus elementos"/>
</node>
<node CREATED="1386414793102" ID="ID_313228104" MODIFIED="1386414824958" TEXT="No requiere la intervenci&#xf3;n normativa de los Estados miembros"/>
</node>
<node CREATED="1386414539674" ID="ID_169232193" MODIFIED="1386414545678" TEXT="Recomendaciones o dict&#xe1;menes">
<node CREATED="1386414860391" ID="ID_102305369" MODIFIED="1386414881087" TEXT="a) No son vinculantes">
<node CREATED="1392587584936" ID="ID_1402775236" MODIFIED="1392587596482" TEXT="Carece de efectos jur&#xed;dicos directos"/>
</node>
<node CREATED="1386414883186" ID="ID_1892407238" MODIFIED="1386414900539" TEXT="b) Se manifiesta su criterio con respecto un asunto"/>
<node CREATED="1386414902022" ID="ID_51616372" MODIFIED="1386414914795" TEXT="c) Puede ser preceptiva o facultativa"/>
<node CREATED="1386414932338" ID="ID_812401507" MODIFIED="1386414949532" TEXT="d) El dictamen co&#xed;ncide con la recomendaci&#xf3;n">
<node CREATED="1386414949534" ID="ID_1855423957" MODIFIED="1386414955975" TEXT="Se emite previa solicitud"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1386415010680" ID="ID_23519332" MODIFIED="1394443106869" POSITION="right" TEXT="Relaciones entre el derecho de la UE y los Estados miembros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386415166465" FOLDED="true" ID="ID_100562979" MODIFIED="1393853782132" TEXT="Relaciones ordinarias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386415173935" ID="ID_1363337678" MODIFIED="1393853771385" TEXT="Principio de autonom&#xed;a">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      La <b>validez y eficacia</b>&#160;de las normas que lo integran resultan independientes de su aceptaci&#243;n por los Estados o del desarrollo de cualquier actividad por parte de &#233;stos, siendo por tanto independiente de &#233;stos.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386415232513" ID="ID_535515987" MODIFIED="1386415274441" TEXT="Principio de cooperaci&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      los Estados miembros han de adoptar todas las medidas apropiadas para asegurar el cumplimiento de las
    </p>
    <p>
      obligaciones derivadas de los Tratados o de los actos de las instituciones y, negativamente, han de abstenerse de adoptar cualquier medida que impida la consecuci&#243;n de los fines propios de los mismos
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1386415275935" FOLDED="true" ID="ID_1363452470" MODIFIED="1394446251665" TEXT="Relaciones extraordinarias (conflictos)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386415318609" FOLDED="true" ID="ID_312661788" MODIFIED="1394445437956" TEXT="Principio de efecto directo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386415382644" ID="ID_1058384086" MODIFIED="1393843858517">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Efecto directo</b>: En caso de conflicto normas<b>&#160;que no tienen atribuida aplicabilidad directa</b>&#160;por los Tratados
    </p>
  </body>
</html></richcontent>
<node CREATED="1386415394124" ID="ID_823483928" MODIFIED="1393843825382" TEXT="Pueden ser invocadas ante los Tribunales nacionales">
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1386415520661" ID="ID_1250074432" MODIFIED="1393843241146" TEXT="Fundamentos de este principio">
<node CREATED="1386415526968" ID="ID_813343373" MODIFIED="1393843743498" TEXT="a) Es preciso para los objetivos de los Tratados"/>
<node CREATED="1386415551116" ID="ID_421886233" MODIFIED="1393843748178" TEXT="b) Carece de sentido si precisa de un desarrollo posterior"/>
<node CREATED="1386415569636" ID="ID_653993542" MODIFIED="1393843916471" TEXT="c) Se basa en el principio de autonom&#xed;a del Derecho de la Uni&#xf3;n"/>
</node>
<node CREATED="1386415631067" ID="ID_478776134" MODIFIED="1393843877923" TEXT="Condiciones para su aplicaci&#xf3;n">
<node CREATED="1386415636139" ID="ID_1147837933" MODIFIED="1392627160380" TEXT="Disposici&#xf3;n sin AMBIGUEDADES"/>
<node CREATED="1386415644625" ID="ID_737738701" MODIFIED="1392627174171" TEXT="Precisa con objeto ESPECIFICO"/>
<node CREATED="1386415663191" ID="ID_1092458025" MODIFIED="1392627186152" TEXT="AUTOSUFICIENTE e incondicional"/>
</node>
<node CREATED="1386415599618" ID="ID_1634408004" MODIFIED="1386415605546" TEXT="Clases">
<node CREATED="1386415605548" ID="ID_525350304" MODIFIED="1393843700738" TEXT="Efecto directo vertical">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cuando una norma se invoca frente al Estado
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386415614336" ID="ID_1603688583" MODIFIED="1393843729162" TEXT="Efecto directo horizontal">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cuando la norma la invoca un particular frente a otro particular
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1386416033514" ID="ID_1671333999" MODIFIED="1394444967927" TEXT="Alcance">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No todas las normas cuentan con efecto directo
    </p>
  </body>
</html></richcontent>
<node CREATED="1386416041992" ID="ID_58631660" MODIFIED="1393844189550" TEXT="Disposiciones de los Tratados">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      S&#243;lo aquellas que contienen previsiones espec&#237;ficas para los particulares
    </p>
  </body>
</html></richcontent>
<node CREATED="1394445016058" ID="ID_757110164" MODIFIED="1394445066784" TEXT="Preceptos que no tienen  efecto directo">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Por ejemplo, los de pol&#237;tica social
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1394445043715" ID="ID_1959230990" MODIFIED="1394445155231" TEXT="Preceptos que tienen efectos directos verticales y horizontales">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      En el caso de preceptos que afecten a derechos b&#225;sicos, como es el de la libre circulaci&#243;n
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1394445156834" ID="ID_596127700" MODIFIED="1394445292034" TEXT="Preceptos con efecto directo s&#xf3;lo verticales">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Imponen obligaciones de hacer (obligaci&#243;n de adecuar los monopolios comerciales)&#160;&#160;o no hacer por parte de los Estados (prohibici&#243;n de establecer nuevos aranceles)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1386416096635" ID="ID_1041146173" MODIFIED="1389467888057" TEXT="Directivas">
<node CREATED="1394445313131" ID="ID_942395329" MODIFIED="1394445322002" TEXT="Si reune los requisitos anteriores">
<node CREATED="1386416131623" ID="ID_1910949714" MODIFIED="1394445349434" TEXT="y ha transcurrido el plazo para su trasposici&#xf3;n sin que se haya traspuesto"/>
</node>
<node CREATED="1386416152357" ID="ID_1829014337" MODIFIED="1394445365185" TEXT="puede tener efecto directo y ser&#xe1;n efectos exclusivamente veriticales"/>
</node>
<node CREATED="1386416176283" ID="ID_1585793860" MODIFIED="1392588098007" TEXT="Decisiones">
<node CREATED="1386416207426" ID="ID_1822825545" MODIFIED="1394445417394" TEXT="Una decisi&#xf3;n dirigida a un Estado miembro, puede ser igualmente invocada por un particular"/>
</node>
<node CREATED="1386416254249" ID="ID_1453235281" MODIFIED="1386416264012" TEXT="Acuerdos internacionales"/>
</node>
</node>
<node CREATED="1386415690077" FOLDED="true" ID="ID_1428135664" MODIFIED="1394445514586" TEXT="Principio de primac&#xed;a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386415712767" ID="ID_865442764" MODIFIED="1394445491845" TEXT="En caso de conflicto, el juez nacional debe aplicar la norma europea">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Y dejar inaplicada la nacional
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386415744020" ID="ID_208154261" MODIFIED="1386415750773" TEXT="Fundamento">
<node CREATED="1386415750775" ID="ID_1105375600" MODIFIED="1392627312812" TEXT=" Garantizar la APLICABILIDAD y el EFECTO directos"/>
<node CREATED="1386415768688" ID="ID_1280449588" MODIFIED="1392627296431" TEXT="Dar sentido a la atribuci&#xf3;n de COMPETENCIAS"/>
</node>
<node CREATED="1386415842017" ID="ID_792225377" MODIFIED="1386415845634" TEXT="Alcance">
<node CREATED="1386415845636" ID="ID_1098618086" MODIFIED="1386415857527" TEXT="Totalidad del Derecho, originario y derivado"/>
<node CREATED="1386415860663" ID="ID_781186254" MODIFIED="1394445445595" TEXT="Sobre toda norma interna">
<node CREATED="1386415872388" ID="ID_671555310" MODIFIED="1386415888503" TEXT="Ya sean normas anteriores o posteriores"/>
</node>
</node>
</node>
<node CREATED="1386415802355" FOLDED="true" ID="ID_1315141579" MODIFIED="1394446248426" TEXT="Principios complementarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386415810902" ID="ID_950878353" MODIFIED="1386415826276" TEXT="Seguridad jur&#xed;dica">
<node CREATED="1392588495128" ID="ID_404237518" MODIFIED="1392588519201" TEXT="Dejar de aplicar una norma interna para aplicar una comunitaria"/>
<node CREATED="1394445762979" ID="ID_1111742430" MODIFIED="1394445788433" TEXT="Los Estados miembros deben de eliminar esa norma de su ordenamiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1386415826754" ID="ID_386942901" MODIFIED="1392588528218" TEXT="Interpretaci&#xf3;n conforme al Derecho comunitario">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Incluso de directivas no transpuestas o transpuestas indirectamente
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386415898319" ID="ID_429547109" MODIFIED="1386415987025" TEXT="Responsabilidad patrimonial del Estado incumplidor">
<node CREATED="1392588564821" ID="ID_1737577168" MODIFIED="1392588593239" TEXT="Si se sufre un perjuicio por la no trasposici&#xf3;n">
<node CREATED="1392588593239" ID="ID_336145582" MODIFIED="1392588607653" TEXT="O trasposici&#xf3;n fuera de plazo"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1386416284748" FOLDED="true" ID="ID_40232636" MODIFIED="1392726295792" POSITION="right" TEXT="Participaci&#xf3;n de los Estados Miembros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386416320101" ID="ID_1977938604" MODIFIED="1386416334749" TEXT="En la elaboraci&#xf3;n de la propuesta de la Comisi&#xf3;n"/>
<node CREATED="1386416337725" ID="ID_152260" MODIFIED="1386416345565" TEXT="En el Parlamento"/>
<node CREATED="1386416346339" ID="ID_1912859082" MODIFIED="1386416357464" TEXT="En el seno del Consejo">
<node CREATED="1386416357466" ID="ID_260217296" MODIFIED="1386416365742" TEXT="Grupos de trabajo">
<node CREATED="1386416365744" ID="ID_745715443" MODIFIED="1386416374329" TEXT="Representantes nacionales"/>
</node>
<node CREATED="1386416377803" ID="ID_1673408029" MODIFIED="1386416383753" TEXT="COREPER"/>
<node CREATED="1386416385112" ID="ID_693959528" MODIFIED="1386416396423" TEXT="Consejo de Ministros">
<node CREATED="1386416397574" ID="ID_1562195110" MODIFIED="1386416414748" TEXT="adopci&#xf3;n mediante votos de Ministros"/>
</node>
</node>
<node CREATED="1386416840089" ID="ID_1384208836" MODIFIED="1386416848934" TEXT="Tratado de Amsterdam">
<node CREATED="1386416873250" ID="ID_1890505552" MODIFIED="1386416904451" TEXT="Las propuestas deber&#xe1;n enviarse a los Parlamentos nacionales"/>
</node>
</node>
<node CREATED="1386785326642" ID="ID_529186455" LINK="Tema%206.%20Las%20instituciones%20de%20la%20UE.mm" MODIFIED="1394446258463" POSITION="left" TEXT="Estrategia Europa 2020">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386785338711" ID="ID_1552565264" MODIFIED="1386785533151" TEXT="Objetivo: recuperar el liderazgo mundial gracias a una econom&#xed;a s&#xf3;lida y competitiva"/>
<node CREATED="1386785370690" ID="ID_553869707" MODIFIED="1386785375037" TEXT="Prioridades">
<node CREATED="1386785375037" ID="ID_638997981" MODIFIED="1389380518502" TEXT="Crecimiento inteligente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386785406820" ID="ID_1835133473" MODIFIED="1392628364603" TEXT="Uni&#xf3;n por la innovaci&#xf3;n">
<icon BUILTIN="broken-line"/>
<icon BUILTIN="group"/>
</node>
<node CREATED="1386785418808" ID="ID_143869776" MODIFIED="1392628339752" TEXT="Juventud en movimiento">
<icon BUILTIN="penguin"/>
</node>
<node CREATED="1386785425945" ID="ID_560117361" MODIFIED="1392628342513" TEXT="Agenda Digital para Europa">
<icon BUILTIN="edit"/>
</node>
</node>
<node CREATED="1386785385853" ID="ID_1747947619" MODIFIED="1389380520045" TEXT="Crecimiento sostenible">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386785440727" ID="ID_284511587" MODIFIED="1392628318832" TEXT="Europa que utilice eficazmente los recursos">
<icon BUILTIN="licq"/>
</node>
<node CREATED="1386785456412" ID="ID_41942148" MODIFIED="1392628326492" TEXT="Pol&#xed;tica industrial para la era de la mundializaci&#xf3;n">
<icon BUILTIN="launch"/>
</node>
</node>
<node CREATED="1386785394550" ID="ID_1400992158" MODIFIED="1389380521548" TEXT="Crecimiento integrador">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386785489009" ID="ID_1787157785" MODIFIED="1392628331328" TEXT="Agenda de nuevas cualificaciones y empleos">
<icon BUILTIN="calendar"/>
</node>
<node CREATED="1386785501153" ID="ID_1362808183" MODIFIED="1392628383900" TEXT="Plataforma europea contra la pobreza">
<icon BUILTIN="ksmiletris"/>
</node>
</node>
</node>
</node>
</node>
</map>
