<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1386669390813" ID="ID_829283526" MODIFIED="1386673276194" TEXT="Tema 6. Las instituciones de la UE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386669417992" FOLDED="true" ID="ID_410309772" MODIFIED="1393875057052" POSITION="right" TEXT="Consejo de la UE, Consejo de Ministros o Consejo  ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386669616038" FOLDED="true" ID="ID_87890493" MODIFIED="1393874794909" TEXT="Naturaleza">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386669678366" ID="ID_310988233" MODIFIED="1386669691469" TEXT="Representa a los Estados miembros"/>
<node CREATED="1386669695542" ID="ID_1736199487" MODIFIED="1386669718798" TEXT="M&#xe1;ximo poder de decisi&#xf3;n pol&#xed;tica y poder legislativo"/>
</node>
<node CREATED="1386669628212" FOLDED="true" ID="ID_1410705165" MODIFIED="1393874793645" TEXT="Composici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386669725400" ID="ID_230359475" MODIFIED="1386669743131" TEXT="Un representante de cada Estado miembro">
<node CREATED="1386669743132" ID="ID_1875953442" MODIFIED="1386669760536" TEXT="Con rango ministerial">
<node CREATED="1386669848190" ID="ID_1982811721" MODIFIED="1386669854835" TEXT="Dependiendo del asunto"/>
</node>
<node CREATED="1386669760970" ID="ID_972017358" MODIFIED="1386669792289" TEXT="Capacitado para comprometer al Gobierno de dicho Estado"/>
<node CREATED="1386669793267" ID="ID_1728364715" MODIFIED="1386669813282" TEXT="Con derecho de voto"/>
</node>
</node>
<node CREATED="1386669634748" ID="ID_290205083" MODIFIED="1393874796188" TEXT="Funcionamiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386669861526" ID="ID_1669223413" MODIFIED="1386669873504" TEXT="Organizaci&#xf3;n">
<node CREATED="1386669873505" ID="ID_1577354681" MODIFIED="1386669893693" TEXT="Se reune normalmente en Bruselas o Luxemburgo"/>
<node CREATED="1386669896247" ID="ID_1555392006" MODIFIED="1386669916174" TEXT="Presidencia por rotaci&#xf3;n de cada Estado miembro cada 6 meses"/>
</node>
<node CREATED="1386669922136" ID="ID_741672927" MODIFIED="1386671570512" TEXT="Procedimientos de votaci&#xf3;n">
<node CREATED="1386669930300" ID="ID_1862496769" MODIFIED="1393862071829" TEXT="A) Mayor&#xed;a simple"/>
<node CREATED="1386669958330" ID="ID_1445382936" MODIFIED="1393862079489" TEXT="B) Mayor&#xed;a cualificada">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      A partir del 1 de noviembre de 2014, hasta entonces se considera triple mayor&#237;a:
    </p>
    <p>
      - De votos.
    </p>
    <p>
      - De Estados.
    </p>
    <p>
      - De poblaci&#243;n.
    </p>
  </body>
</html></richcontent>
<node CREATED="1386669994427" ID="ID_1150854287" MODIFIED="1386670004602" TEXT="55% de los miembros del Consejo">
<node CREATED="1386670019109" ID="ID_1281909829" MODIFIED="1386670034019" TEXT="Incluya a la mayor&#xed;a de ellos"/>
<node CREATED="1386670036486" ID="ID_1498275404" MODIFIED="1386670060373" TEXT="Los Estados que reunan el 65% de la poblaci&#xf3;n de la UE"/>
</node>
<node CREATED="1386670071423" ID="ID_1971993625" MODIFIED="1386670086466" TEXT="Minor&#xed;a de bloqueo">
<node CREATED="1386670086467" ID="ID_832699301" MODIFIED="1386670089470" TEXT="4 miembros"/>
</node>
</node>
<node CREATED="1386700981998" ID="ID_980345286" MODIFIED="1393862085495" TEXT="C) Unanimidad"/>
</node>
</node>
<node CREATED="1386669663101" FOLDED="true" ID="ID_179622852" MODIFIED="1393862180702" TEXT="COREPER">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comite de Representantes Permanentes
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386670467728" ID="ID_183539704" MODIFIED="1386670477191" TEXT="Foro de di&#xe1;logo entre">
<node CREATED="1386670477192" ID="ID_1897477999" MODIFIED="1386670486370" TEXT="Representantes permanentes"/>
<node CREATED="1386670486852" ID="ID_1684535629" MODIFIED="1386670493178" TEXT="Autoridades de tutela"/>
<node CREATED="1386670518970" ID="ID_1362557057" MODIFIED="1386670537879" TEXT="Prepara las decisiones que va a tomar el Consejo"/>
</node>
<node CREATED="1386670496164" ID="ID_798243638" MODIFIED="1386670511461" TEXT="Organizaci&#xf3;n en dos niveles">
<node CREATED="1386670511462" ID="ID_901737165" MODIFIED="1386670545347" TEXT="COREPER I">
<node CREATED="1386670553240" ID="ID_1322416992" MODIFIED="1386670566405" TEXT="Representantes permanentes adjuntos"/>
<node CREATED="1386671224738" ID="ID_1669583068" MODIFIED="1386671240935" TEXT="Cuestiones de naturaleza t&#xe9;cnica"/>
</node>
<node CREATED="1386670545953" ID="ID_1519192288" MODIFIED="1386670550375" TEXT="COREPER II">
<node CREATED="1386671245355" ID="ID_921948343" MODIFIED="1389463918644" TEXT="Representantes permanentes (embajadores)"/>
<node CREATED="1386671257324" ID="ID_982790799" MODIFIED="1386671268131" TEXT="Cuestiones de naturaleza pol&#xed;tica"/>
</node>
</node>
<node CREATED="1386671300574" ID="ID_1310548439" MODIFIED="1386671315104" TEXT="Influencia">
<node CREATED="1386671315105" ID="ID_649914375" MODIFIED="1386671342225" TEXT="Cuestiones con preacuerdo en el COREPER">
<node CREATED="1386671342227" ID="ID_942641955" MODIFIED="1386671362711" TEXT="Punto &quot;A&quot; en el orden del d&#xed;a"/>
<node CREATED="1386671363168" ID="ID_1582784839" MODIFIED="1386671383423" TEXT="Aprobadas sin debate y publicadas"/>
</node>
<node CREATED="1386671385105" ID="ID_176295039" MODIFIED="1386671414988" TEXT="Si requieren deliberaci&#xf3;n">
<node CREATED="1386671414989" ID="ID_936051754" MODIFIED="1386671430450" TEXT="Punto &quot;B&quot;"/>
<node CREATED="1386671431083" ID="ID_541401245" MODIFIED="1386671450538" TEXT="Objeto de debate y votaci&#xf3;n"/>
</node>
</node>
</node>
<node CREATED="1386669666406" ID="ID_710439868" MODIFIED="1393875036132" TEXT="Competencias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386671479701" ID="ID_438781020" MODIFIED="1386671496829" TEXT="A) Legislativas y de ejecuci&#xf3;n">
<node CREATED="1386671593754" ID="ID_818746337" MODIFIED="1386671619802" TEXT="Emanan normas generales, adoptadas como reglamentos, directivas o decisiones"/>
<node CREATED="1386671626003" ID="ID_1813237183" MODIFIED="1386671796786" TEXT="La Comisi&#xf3;n le propone un acto y el Consejo decide"/>
<node CREATED="1386671901991" ID="ID_267072027" MODIFIED="1386671919687" TEXT="No puede modificar las propuestas de la Comisi&#xf3;n"/>
</node>
<node CREATED="1386671498102" ID="ID_1180422938" MODIFIED="1386671515605" TEXT="B) Coordinaci&#xf3;n">
<node CREATED="1386671883094" ID="ID_1145670242" MODIFIED="1386671896285" TEXT="Coordinaci&#xf3;n entre los Estados miembros y la UE"/>
</node>
</node>
</node>
<node CREATED="1386669498111" FOLDED="true" ID="ID_1900072220" MODIFIED="1394465929066" POSITION="right" TEXT="La Comisi&#xf3;n Europea">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386671961845" ID="ID_1752385520" MODIFIED="1394437530570" TEXT="Naturaleza">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386671968590" ID="ID_211243161" MODIFIED="1386671981425" TEXT="Independiente de los Estados miembros"/>
<node CREATED="1386671981707" ID="ID_844400467" MODIFIED="1386671997878" TEXT="Encarna la expresi&#xf3;n de los derechos de la UE"/>
</node>
<node CREATED="1386672007100" ID="ID_1491604773" MODIFIED="1393864915747" TEXT="Composici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node COLOR="#ff0000" CREATED="1386672040200" FOLDED="true" ID="ID_19790431" MODIFIED="1394463863662" TEXT="Presidente de la Comisi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386672328898" ID="ID_1655251812" MODIFIED="1386697928980" TEXT="a) Nombramiento">
<node CREATED="1386672264559" ID="ID_1417592729" MODIFIED="1394437550368" TEXT="Propuesto por el Consejo Europeo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386672283680" ID="ID_1705569005" MODIFIED="1394438596445" TEXT="Aprobaci&#xf3;n por el Parlamento por mayor&#xed;a abosulta">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386672304344" ID="ID_1192258912" MODIFIED="1386672316423" TEXT="Voto de investidura final colegiado"/>
</node>
<node CREATED="1386672347299" ID="ID_769003770" MODIFIED="1386697932396" TEXT="b) Funciones">
<node CREATED="1394438160266" ID="ID_225356161" MODIFIED="1394438276536" TEXT="Ostentar la m&#xe1;s alta representaci&#xf3;n institucional de la Comisi&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Y como tal, la de la Uni&#243;n en su conjunto, para las materias no incluidas en la pol&#237;tica exterior y de seguridad com&#250;n
    </p>
  </body>
</html></richcontent>
<node CREATED="1394438297171" ID="ID_595794615" MODIFIED="1394438308724" TEXT="Es tambi&#xe9;n miembro del Consejo Europeo"/>
</node>
<node CREATED="1386672355389" ID="ID_1050909887" MODIFIED="1394438000102">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Definir las <b>orientaciones generales </b>con las que la Comisi&#243;n desempe&#241;ar&#225; sus funciones
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386672371483" ID="ID_377540296" MODIFIED="1394438086078">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Determinar la <b>organizaci&#243;n interna</b>&#160;de la Comisi&#243;n
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386672381396" ID="ID_1289397233" MODIFIED="1394438147469">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Nombrar<b>&#160;Vicepresidentes</b>&#160;distintos del Alto Representante
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1394438094435" ID="ID_1996170570" MODIFIED="1394438123829" TEXT="M&#xe1;xima direcci&#xf3;n del personal funcionario y administrativo"/>
</node>
<node CREATED="1386784363771" ID="ID_1489695657" MODIFIED="1386784368953" TEXT="Mandato de 5 a&#xf1;os"/>
</node>
<node CREATED="1386672043565" FOLDED="true" ID="ID_1284813663" MODIFIED="1394463854037" TEXT="Comisarios">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1394442100283" ID="ID_1607900831" MODIFIED="1394442150754" TEXT="Su designaci&#xf3;n le corresponde al Consejo Europeo de com&#xfa;n acuerdo con el Presidente de la Comisi&#xf3;n"/>
<node CREATED="1386672406637" ID="ID_419887311" MODIFIED="1386672425579" TEXT="28 elegidos por su competencia general"/>
<node CREATED="1386672508192" ID="ID_1977440862" MODIFIED="1394442132614" TEXT="Personalidades con garant&#xed;as de independencia y compromiso con Europa"/>
<node CREATED="1386672532974" ID="ID_661638182" MODIFIED="1386672584145" TEXT="A partir de 1 de noviembre de 2014, ser&#xe1; de 2/3 del n&#xfa;mero de Estados miembros">
<node CREATED="1394442203850" ID="ID_1495038197" MODIFIED="1394442278516" TEXT="Seleccionados por un sistema de rotaci&#xf3;n que..."/>
<node CREATED="1394442225091" ID="ID_519678220" MODIFIED="1394442267802" TEXT="Tendr&#xe1; en cuenta la diversidad geogr&#xe1;fica y demogr&#xe1;fica del conjunto de los Estados"/>
</node>
</node>
<node CREATED="1386672048942" FOLDED="true" ID="ID_1919080931" MODIFIED="1394463862558" TEXT="Alto Representante para Asuntos Exteriores y Pol&#xed;tica de Seguridad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386672590747" ID="ID_1123957176" MODIFIED="1386672616544" TEXT="Preside los Consejos de Asuntos Exteriores"/>
<node CREATED="1386698224565" ID="ID_1229101408" MODIFIED="1394442326882" TEXT="Es tambi&#xe9;n Vicepresidente de la Comisi&#xf3;n y miembro del Consejo Europeo"/>
<node CREATED="1386672634856" ID="ID_71728929" MODIFIED="1386672658133" TEXT="Est&#xe1; al frente de la pol&#xed;tica exterior y de seguridad"/>
<node CREATED="1386672658631" ID="ID_1872847372" MODIFIED="1394442189010" TEXT="Contribuir a ELABORAR dicha pol&#xed;tica"/>
<node CREATED="1386672674238" ID="ID_179687141" MODIFIED="1386672688196" TEXT="EJECUTARLA como mandatario del Consejo"/>
<node CREATED="1386672748330" ID="ID_1883242750" MODIFIED="1386672795286" TEXT="INICIATIVA de la polit&#xed;ca comun de SEGURIDAD y DEFENSA"/>
<node CREATED="1386672796063" ID="ID_1439291188" MODIFIED="1386672812342" TEXT="Velar por la COHERENCIA de la acci&#xf3;n exterior"/>
</node>
</node>
<node CREATED="1386672098016" ID="ID_1796178758" MODIFIED="1393864919866" TEXT="Funcionamiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386672223341" FOLDED="true" ID="ID_1192405667" MODIFIED="1393875412906" TEXT="Autoorganizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386673483701" ID="ID_1685821648" MODIFIED="1393864922721" TEXT="Establecer&#xe1; su propia reglamento interno">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1386672229710" FOLDED="true" ID="ID_1717550654" MODIFIED="1393875416400" TEXT="Colegialidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386673516313" ID="ID_501367780" MODIFIED="1386673531525" TEXT="Las decisiones se extienden al &#xf3;rgano en su conjunto"/>
<node CREATED="1386673535287" ID="ID_599828604" MODIFIED="1386673547247" TEXT="Se asumen colectivamente por sus miembros"/>
<node CREATED="1386673565160" ID="ID_1640042940" MODIFIED="1386698392518" TEXT="Acuerdos por mayor&#xed;a de sus miembros (15 sobre 28)"/>
<node CREATED="1386673576762" ID="ID_1641365844" MODIFIED="1386673587072" TEXT="Igualdad de todos sus miembros"/>
</node>
</node>
<node CREATED="1386672117153" ID="ID_509730353" MODIFIED="1393875418397" TEXT="Competencias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386672123001" ID="ID_1518522144" MODIFIED="1392652943038" TEXT="Competencias de iniciativa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386673601322" ID="ID_105807559" MODIFIED="1386673615912" TEXT="Formaci&#xf3;n de actos (iniciativa legislativa)"/>
<node CREATED="1386673636347" ID="ID_1840950809" MODIFIED="1386673670091" TEXT="Los actos del Consejo y Parlamento necesitan la propuesta de la Comisi&#xf3;n"/>
<node CREATED="1386673675261" ID="ID_1808798796" MODIFIED="1394442179516" TEXT="El Consejo s&#xf3;lo puede modificar la propuesta por unanimidad"/>
</node>
<node CREATED="1386672138490" ID="ID_1987285922" MODIFIED="1392652954332" TEXT="Competencias de control">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Inicio de un procedimiento por incumplimiento o en constataci&#243;n de infracciones
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386672178916" ID="ID_465609155" MODIFIED="1386673903246" TEXT="a) Fase administrativa o precontenciosa">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Carta de emplazamiento al estado miembro:
    </p>
    <p>
      - Infracci&#243;n imputada
    </p>
    <p>
      - Plazo para que conteste.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386672193356" ID="ID_482036680" MODIFIED="1389451331715" TEXT="b) Fase contenciosa">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Interposici&#243;n de recurso de incumplimiento ante el TJUE
    </p>
  </body>
</html></richcontent>
<arrowlink DESTINATION="ID_34396656" ENDARROW="Default" ENDINCLINATION="361;0;" ID="Arrow_ID_1628771193" STARTARROW="None" STARTINCLINATION="361;0;"/>
<node CREATED="1386673945481" ID="ID_1895813078" MODIFIED="1386674188302" TEXT="Existencia de incumplimiento">
<node CREATED="1386674188303" ID="ID_1782921758" MODIFIED="1386674196689" TEXT="Ejecuci&#xf3;n de la sentencia"/>
<node CREATED="1386674197052" ID="ID_1771560701" MODIFIED="1386698675208" TEXT="Si no se hace efectiva, multa coercitiva"/>
</node>
</node>
</node>
<node CREATED="1386672150658" ID="ID_1852361130" MODIFIED="1386700780436" TEXT="Competencias en materia de relaciones exteriores">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1393864739444" ID="ID_336486114" MODIFIED="1393864752626" TEXT="Poder de negociaci&#xf3;n en los acuerdos internacionales"/>
</node>
</node>
</node>
<node COLOR="#ff0000" CREATED="1386669483726" FOLDED="true" ID="ID_1863444559" MODIFIED="1394469586300" POSITION="right" TEXT="El Parlamento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386674253206" ID="ID_1889052238" MODIFIED="1386674495582" TEXT="Naturaleza">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Representa el inter&#233;s de los pueblos de los Estados reunidos en la UE
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386674257103" FOLDED="true" ID="ID_121440232" MODIFIED="1394466694576" TEXT="Composici&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Diferencias con las &#250;ltimas elecciones:
    </p>
    <p>
      - Reparto de esca&#241;os diferente, por la incorporaci&#243;n de Croacia.
    </p>
    <p>
      - Lucha contra la corrupci&#243;n pol&#237;tica: los eurodiputado tendr&#225;n que presentar:
    </p>
    <ul>
      <li>
        Declaraci&#243;n detallada de inter&#233;s econ&#243;micos
      </li>
      <li>
        Gastos de viaje cuando sean abonados por terceros
      </li>
    </ul>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386674341941" ID="ID_1591466317" MODIFIED="1386674357321" TEXT="Cada Estado tiene un n&#xfa;mero de esca&#xf1;os"/>
<node CREATED="1386674358579" ID="ID_785832567" MODIFIED="1394437490293" TEXT="766 eurodiputados, actualmente.">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      En las pr&#243;ximas elecciones se eligir&#225;n 751 parlamentarios, Espa&#241;a mantiene a sus 54 representantes
    </p>
  </body>
</html></richcontent>
<node CREATED="1393864813238" ID="ID_1216840065" MODIFIED="1393864825689" TEXT="Elegidos por un periodo de 5 a&#xf1;os"/>
</node>
<node CREATED="1386674425861" ID="ID_1317658881" MODIFIED="1386674428236" TEXT="Pleno"/>
<node CREATED="1393864851606" ID="ID_673253975" MODIFIED="1393875755328" TEXT="El Pleno elige al">
<node CREATED="1386674435758" ID="ID_1916119057" MODIFIED="1386674441068" TEXT="Presidente"/>
<node CREATED="1386674428526" ID="ID_1318229946" MODIFIED="1386674435227" TEXT="Mesa">
<node CREATED="1393864840280" ID="ID_34308224" MODIFIED="1393864845321" TEXT="Vicepresidentes"/>
<node CREATED="1393864845670" ID="ID_845842600" MODIFIED="1393864848168" TEXT="Cuestores"/>
</node>
</node>
<node CREATED="1386674441510" ID="ID_446152262" MODIFIED="1386674444700" TEXT="Comisiones">
<node CREATED="1393864895637" ID="ID_1032700196" MODIFIED="1393864912409" TEXT="Cauce por donde discurre el trabajo de los Parlamentarios"/>
</node>
<node CREATED="1386674468864" ID="ID_890903091" MODIFIED="1386674474413" TEXT="Grupos Parlamentarios">
<node CREATED="1393864946595" ID="ID_1016328933" MODIFIED="1393864959123" TEXT="Parlamentarios pertenecientes a una misma corriente ideol&#xf3;gica"/>
</node>
</node>
<node CREATED="1386674499737" FOLDED="true" ID="ID_53015944" MODIFIED="1394466693317" TEXT="Funcionamiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386674515041" ID="ID_1230764855" MODIFIED="1386674520648" TEXT="Periodo de sesiones">
<node CREATED="1386674542754" ID="ID_1305787033" MODIFIED="1393865092316" TEXT="En todo caso se reune 2&#xba; martes de marzo"/>
<node CREATED="1386675415804" ID="ID_1127184121" MODIFIED="1393865082161" TEXT="Cada periodo anual tiene 12 periodos parciales de sesiones"/>
<node CREATED="1393865099186" ID="ID_1491737283" MODIFIED="1393865128064" TEXT="Puede reunirse en sesiones extraordinarias por petici&#xf3;n de la mayor&#xed;a de sus miembros"/>
</node>
<node CREATED="1386699732574" ID="ID_578063176" MODIFIED="1386699745756" TEXT="Sede en Estrasburgo"/>
<node CREATED="1386699772768" ID="ID_1393367979" MODIFIED="1386699790739" TEXT="Reuniones de las Comisiones en Bruselas"/>
<node CREATED="1386699793196" ID="ID_975765036" MODIFIED="1386699809155" TEXT="La Secretar&#xed;a General en Luxemburgo"/>
</node>
<node CREATED="1386675855798" ID="ID_1494395667" MODIFIED="1394466698222" TEXT="Competencias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386675864679" FOLDED="true" ID="ID_869113265" MODIFIED="1393865196064" TEXT="Competencias presupuestarias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386675930026" ID="ID_1732766649" MODIFIED="1386675940697" TEXT="Aprueba los Presupuestos de la UE"/>
<node CREATED="1386675974691" ID="ID_590849072" MODIFIED="1386675985458" TEXT="Corresponde la elaboraci&#xf3;n a la Comisi&#xf3;n"/>
<node CREATED="1386675942690" ID="ID_437919918" MODIFIED="1386675962417" TEXT="Corresponde conjuntamente al Parlamento y al Consejo"/>
<node CREATED="1386675991637" ID="ID_1500524353" MODIFIED="1386676000591" TEXT="Si hay desacuerdo">
<node CREATED="1386676000592" ID="ID_1550658710" MODIFIED="1386676010300" TEXT="Comit&#xe9; de Conciliaci&#xf3;n"/>
</node>
</node>
<node CREATED="1386675872183" ID="ID_906595915" MODIFIED="1394466846671" TEXT="Competencias legislativas">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Procedimiento de codecisi&#243;n adoptado en el tratado de Maastricht y como ordinario
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386676058584" ID="ID_1530972212" MODIFIED="1386676210289" TEXT="1) Propuesta de la Comisi&#xf3;n"/>
<node CREATED="1386676082888" ID="ID_1919147278" MODIFIED="1386676215196" TEXT="2) Primera lectura">
<node CREATED="1386676116854" ID="ID_1052379792" MODIFIED="1386676147130" TEXT="El Parlamento aprueba su posici&#xf3;n y la transmite al Consejo">
<node CREATED="1386676156454" ID="ID_1810208664" MODIFIED="1392653751353" TEXT="Si aprueba, se adoptar&#xe1; el acto">
<icon BUILTIN="forward"/>
</node>
<node CREATED="1386676170628" ID="ID_903323043" MODIFIED="1392653753865" TEXT="Si no lo aprueba, adoptar&#xe1; su posici&#xf3;n y la transmitir&#xe1; al Parlamento">
<icon BUILTIN="forward"/>
</node>
</node>
</node>
<node CREATED="1386676201486" ID="ID_460669759" MODIFIED="1386676271907" TEXT="3) Segunda lectura. A partir de la transmisi&#xf3;n, el Parlamento">
<node CREATED="1386676271908" ID="ID_350089961" MODIFIED="1392654089749" TEXT="a) Aprobar, por mayor&#xed;a, la posici&#xf3;n del consejo o no tomar decisi&#xf3;n">
<node CREATED="1386676295292" ID="ID_603611598" MODIFIED="1386676370987" TEXT="El acto adoptado seg&#xfa;n la formulaci&#xf3;n del Consejo">
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1386676321731" ID="ID_700290474" MODIFIED="1386676367240" TEXT="b) Rechazar, por mayor&#xed;a, la posicion del Consejo">
<node CREATED="1386676351159" ID="ID_938616026" MODIFIED="1386676372691" TEXT="Acto no aprobado">
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1386676374901" ID="ID_589912989" MODIFIED="1386676388472" TEXT="c) Proponer enmiendas">
<node CREATED="1386676388473" ID="ID_1286019001" MODIFIED="1392654080217" TEXT="Vuelve al Consejo y a la Comisi&#xf3;n que dictaminar&#xe1;n sobre las enmiendas">
<icon BUILTIN="forward"/>
</node>
<node CREATED="1386676423495" ID="ID_1128540967" MODIFIED="1386676445653" TEXT="Si se aprueban las enmiendas, se adopta el acto."/>
<node CREATED="1386676445960" ID="ID_845679014" MODIFIED="1386676472696" TEXT="Si no, se convoca al Comit&#xe9; de Conciliaci&#xf3;n"/>
</node>
</node>
<node CREATED="1386700031328" ID="ID_657882310" MODIFIED="1386700038535" TEXT="4) Conciliaci&#xf3;n">
<node CREATED="1386676482258" ID="ID_1682459775" MODIFIED="1386676500654" TEXT="Comit&#xe9; de Conciliaci&#xf3;n">
<node CREATED="1386676500655" ID="ID_893465517" MODIFIED="1386676505384" TEXT="Miembros del Consejo"/>
<node CREATED="1386676508371" ID="ID_222895950" MODIFIED="1386676522274" TEXT="Mismo n&#xfa;mero de miembros del Parlamento"/>
<node CREATED="1392654122438" ID="ID_198216333" MODIFIED="1392654133413" TEXT="Adoptar un texto conjunto">
<node CREATED="1392654133413" ID="ID_1280840469" MODIFIED="1392654145441" TEXT="Por mayor&#xed;a de los miembros del Parlamento"/>
<node CREATED="1392654145868" ID="ID_1800370754" MODIFIED="1392654162656" TEXT="Por mayor&#xed;a cualificada de los miembros del Consejo"/>
</node>
</node>
</node>
<node CREATED="1389442116331" ID="ID_200296454" MODIFIED="1389442175599" TEXT="5) Tercera lectura">
<node CREATED="1389442127634" ID="ID_1333316902" MODIFIED="1389442165886" TEXT="Adopci&#xf3;n del acto del Comit&#xe9; de Conciliaci&#xf3;n">
<node CREATED="1389442141004" ID="ID_1087617901" MODIFIED="1392654199113" TEXT="Parlamento, por mayor&#xed;a"/>
<node CREATED="1389442150997" ID="ID_1527131608" MODIFIED="1392654207116" TEXT="Consejo, por mayor&#xed;a cualificada"/>
</node>
</node>
<node CREATED="1386700068511" ID="ID_1012622218" MODIFIED="1386700078557" TEXT="Si no se aprueba, se considera no adoptado"/>
</node>
<node CREATED="1386675909657" FOLDED="true" ID="ID_1466650095" MODIFIED="1394465963394" TEXT="Competencias de control">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="forward"/>
<node CREATED="1386676530348" ID="ID_723924426" MODIFIED="1386676544851" TEXT="Voto de investidura">
<node CREATED="1386701144329" ID="ID_439967256" MODIFIED="1386701153689" TEXT="Presidente de la Comisi&#xf3;n"/>
<node CREATED="1386701154465" ID="ID_666512531" MODIFIED="1392654239720" TEXT="Alto Representante"/>
<node CREATED="1393879134082" ID="ID_1102515052" MODIFIED="1393879142132" TEXT="Los Comisarios"/>
<node CREATED="1393879143074" ID="ID_327731511" MODIFIED="1393879158707" TEXT="Posteriormente ser&#xe1;n nombrados por el Consejo, por mayor&#xed;a cualificada"/>
</node>
<node CREATED="1386676545164" ID="ID_1996220567" MODIFIED="1386676550219" TEXT="Voto de censura">
<node CREATED="1386701176329" ID="ID_413899293" MODIFIED="1393865871391" TEXT="Contra la Comis&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Por 2/3 de sus miembros, obliga a la renuncia colectiva de sus miembros
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1386676550877" ID="ID_873343999" MODIFIED="1386701283222" TEXT="Comisiones de investigaci&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Presuntas infracciones o mala administraci&#243;n
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386676559757" ID="ID_188522852" MODIFIED="1386676567244" TEXT="Derecho de petici&#xf3;n"/>
<node CREATED="1386676568678" ID="ID_1022419258" MODIFIED="1386701245516" TEXT="Nombramiento del Defensor del Pueblo"/>
<node CREATED="1386676593958" ID="ID_429899487" MODIFIED="1393865931983" TEXT="Pronunciarse sobre el Informe anual de Actividades de la Uni&#xf3;n Europea que elabora la Comisi&#xf3;n"/>
<node CREATED="1386676602447" ID="ID_1212438754" MODIFIED="1393878294261" TEXT="Comparecencias y preguntas parlamentarias, al Consejo Europeo, el Consejo y la Comisi&#xf3;n"/>
</node>
</node>
<node CREATED="1394462994296" ID="ID_1933355260" MODIFIED="1394463054077" TEXT="Elecciones 2014">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1394463946986" ID="ID_882690440" MODIFIED="1394463986090" TEXT="Ser&#xe1;n las primeras elecciones entre la entrada en vigor del tratado de L&#xed;sboa (31 de diciembre de 2009)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1394465935904" ID="ID_496425613" MODIFIED="1394465958012" TEXT="El nuevo Parlamento tiene el poder de codecisi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1394463994587" ID="ID_335731717" MODIFIED="1394464073120" TEXT="El n&#xfa;mero se Comisiarios se reducir&#xe1; a 2/3 del n&#xfa;mero de los Estados Miembros (1 de noviembre) "/>
<node CREATED="1394464017667" ID="ID_858435558" MODIFIED="1394464083003" TEXT="La mayor&#xed;a cualificada para la tma de decisiones cambiar&#xe1; a 55% de los Estados que supongan el 65% de la poblaci&#xf3;n (1 de noviembre)"/>
</node>
<node CREATED="1394466525790" ID="ID_1609192915" MODIFIED="1394466539008" TEXT="El n&#xfa;mero de eurodiputados disminuye a 751">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1394462949684" ID="ID_1471062582" MODIFIED="1394464527107" TEXT="En estas elecciones (2014), cada Partido Pol&#xed;tico debe decir previamente qui&#xe9;n es su candidato a Presidente de la Comision">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1394464489268" ID="ID_1208333200" MODIFIED="1394464531932" TEXT="Por lo que realmente los ciudadanos tambi&#xe9;n estar&#xed;an eligiendo al Presidente de la Comisi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1394463027579" ID="ID_1024987697" MODIFIED="1394463057476" TEXT="El Parlamento tiene poder sobre la totalidad del Presupuesto">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1386669514327" FOLDED="true" ID="ID_34396656" MODIFIED="1393879817336" POSITION="right" TEXT="El Tribunal de Justicia de la Uni&#xf3;n Europea">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386676682658" FOLDED="true" ID="ID_450032377" MODIFIED="1393867119426" TEXT="Composici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386676697494" ID="ID_1493594516" MODIFIED="1393866014008" TEXT="A) Jueces">
<node CREATED="1386676770227" ID="ID_1636097451" MODIFIED="1386676780867" TEXT="Uno de cada Estado miembro"/>
<node CREATED="1386676791233" ID="ID_1347705811" MODIFIED="1386676802309" TEXT="De com&#xfa;n acuerdo por los Gobiernos"/>
</node>
<node CREATED="1386676706051" ID="ID_244190228" MODIFIED="1393866019562" TEXT="B) Abogados Generales">
<node CREATED="1386676815017" ID="ID_509885535" MODIFIED="1386676826358" TEXT="9 Abogados Generales"/>
<node CREATED="1386676842748" ID="ID_1914851996" MODIFIED="1386676858738" TEXT="Primer Abogado General">
<node CREATED="1386676859011" ID="ID_16109816" MODIFIED="1386676874572" TEXT="Funci&#xf3;n">
<node CREATED="1392654844930" ID="ID_1571043475" MODIFIED="1392654860781" TEXT="Distribuci&#xf3;n de los asuntos entre los abogados generales"/>
<node CREATED="1386676874572" ID="ID_773547773" MODIFIED="1386676894463" TEXT="presentar publicamente conclusiones motivadas ">
<node CREATED="1392654885593" ID="ID_1525795620" MODIFIED="1392654905327" TEXT="sobre los asuntos que requieran su intervenci&#xf3;n"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1386676897684" FOLDED="true" ID="ID_217411251" MODIFIED="1393879534149" TEXT="Competencias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386676935437" ID="ID_1858364162" MODIFIED="1393867588786" TEXT="Pronunciarse sobre los recursos">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Interpuestos por&#160;un Estado miembro, una instituci&#243;n o personas f&#237;sicas o jur&#237;dicas.
    </p>
  </body>
</html></richcontent>
<node CREATED="1386680103010" FOLDED="true" ID="ID_1110270815" MODIFIED="1393879529360" TEXT="a) Recurso de incumplimiento o infracci&#xf3;n de un Estado miembro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="stop-sign"/>
<node CREATED="1386680137609" ID="ID_441049202" MODIFIED="1386680145181" TEXT="Interpuesto por la Comisi&#xf3;n"/>
<node CREATED="1386680145461" ID="ID_931711449" MODIFIED="1386701820307" TEXT="Interpuesto por cualquier Estado"/>
</node>
<node CREATED="1386680161036" FOLDED="true" ID="ID_1552491226" MODIFIED="1393879527082" TEXT="b) Recurso de anulaci&#xf3;n de los actos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_cancel"/>
<node CREATED="1393879281346" ID="ID_885215127" MODIFIED="1393879287736" TEXT="Actos de:">
<node CREATED="1393879287736" ID="ID_1560370243" MODIFIED="1393879296472" TEXT="Conjuntos Parlamento-Consejo"/>
<node CREATED="1393879296805" ID="ID_1085610614" MODIFIED="1393879300645" TEXT="Comisi&#xf3;n"/>
<node CREATED="1393879300900" ID="ID_1554315335" MODIFIED="1393879303679" TEXT="Consejo"/>
<node CREATED="1393879304512" ID="ID_1901713056" MODIFIED="1393879307290" TEXT="BCE"/>
<node CREATED="1393879308435" ID="ID_341191862" MODIFIED="1393879327176" TEXT="Que no sean dictamenes ni recomendaciones"/>
<node CREATED="1393879337025" ID="ID_802874636" MODIFIED="1393879348197" TEXT="O del Parlamento que afecten a particulares"/>
</node>
<node CREATED="1386680184351" ID="ID_1226106817" MODIFIED="1389461667537" TEXT="Legimitaci&#xf3;n activa">
<node CREATED="1386680212834" ID="ID_1924172330" MODIFIED="1386680219734" TEXT="Estados miembros"/>
<node CREATED="1386680220064" ID="ID_1255672101" MODIFIED="1386680222645" TEXT="Consejo"/>
<node CREATED="1386680222875" ID="ID_1506155995" MODIFIED="1386680226108" TEXT="Comisi&#xf3;n"/>
<node CREATED="1386680226328" ID="ID_290154890" MODIFIED="1386680233790" TEXT="Parlamento Europeo"/>
<node CREATED="1386680234170" ID="ID_343372346" MODIFIED="1386680254906" TEXT="Tribunal de Cuentas y BCE para salvaguardar sus prerogativas"/>
<node CREATED="1386680258377" ID="ID_1924436496" MODIFIED="1386680269781" TEXT="Particulares, si el acto les afecta"/>
</node>
<node CREATED="1386680201572" ID="ID_1744533517" MODIFIED="1393867290621" TEXT="Legitimaci&#xf3;n pasiva">
<node CREATED="1386680274766" ID="ID_260284688" MODIFIED="1386680288262" TEXT="Instituci&#xf3;n autora del acto recurrido"/>
</node>
<node CREATED="1386680291327" ID="ID_1024790405" MODIFIED="1386701519456" TEXT="Fundamento">
<node CREATED="1386680295469" ID="ID_1793519673" MODIFIED="1386680298355" TEXT="Incompetencia"/>
<node CREATED="1386680298753" ID="ID_666087270" MODIFIED="1386680306538" TEXT="Vicios sustanciales de forma"/>
<node CREATED="1386680307076" ID="ID_351613917" MODIFIED="1389461711519" TEXT="Violaci&#xf3;n del Derecho Comunitario"/>
<node CREATED="1386680318722" ID="ID_1779586700" MODIFIED="1386680326834" TEXT="Desviaci&#xf3;n de poder"/>
</node>
</node>
<node CREATED="1386680344314" FOLDED="true" ID="ID_1688752757" MODIFIED="1393879524352" TEXT="c) Recurso de inactividad o por omisi&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="closed"/>
<node CREATED="1386680370172" ID="ID_265536447" MODIFIED="1392655469550" TEXT="Si ante una violaci&#xf3;n del Tratado no se pronuncian">
<node CREATED="1386680389228" ID="ID_1225963402" MODIFIED="1386680394111" TEXT="Parlamento"/>
<node CREATED="1386680394275" ID="ID_1827689301" MODIFIED="1386680396490" TEXT="Consejo"/>
<node CREATED="1386680396716" ID="ID_282175138" MODIFIED="1386680399119" TEXT="Comisi&#xf3;n"/>
</node>
<node CREATED="1386680420047" ID="ID_579612925" MODIFIED="1386680436771" TEXT="Estados miembros y dem&#xe1;s instituciones">
<node CREATED="1392655497433" ID="ID_996410795" MODIFIED="1392655514767" TEXT="Podr&#xe1;n dirigirse al TJUE para que declare dicha violaci&#xf3;n"/>
</node>
<node CREATED="1386680437496" ID="ID_1856284044" MODIFIED="1386680446310" TEXT="Personas f&#xed;sicas o jur&#xed;dicas">
<node CREATED="1392655516863" ID="ID_1576466907" MODIFIED="1392655535616" TEXT="Si no se le ha dirigido un acto distinto de una recomendaci&#xf3;n o un dictamen"/>
</node>
</node>
<node CREATED="1386680449438" ID="ID_33671698" MODIFIED="1393879012379" TEXT="d) Recurso de personal">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="male2"/>
</node>
<node CREATED="1386680461864" FOLDED="true" ID="ID_1762984905" MODIFIED="1393879531809" TEXT="e) Cuesti&#xf3;n prejudicial">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="list"/>
<node CREATED="1386680505740" ID="ID_1259343090" MODIFIED="1386680520389" TEXT="Interpretaci&#xf3;n del Derecho de la Uni&#xf3;n Europea"/>
<node CREATED="1386677005491" ID="ID_386388937" MODIFIED="1386677024898" TEXT="A petici&#xf3;n de los &#xf3;rganos nacionales jurisdiccionales">
<node CREATED="1386701681146" ID="ID_363414871" MODIFIED="1386701699936" TEXT="La respuesta es una sentencia o auto motivado">
<node CREATED="1386701699936" ID="ID_454953288" MODIFIED="1386701715583" TEXT="El &#xf3;rgano jurisdiccional nacional se haya motivado"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1386676917910" ID="ID_800967755" MODIFIED="1393879544133" TEXT="Tribunal General">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Antiguo Tribunal de Primera Instancia
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386680555623" ID="ID_843875450" MODIFIED="1392655682974" TEXT="Todos los recursos que no est&#xe9;n reservados al TJUE o a las Salas Especializadas"/>
<node CREATED="1386680575007" ID="ID_443128029" MODIFIED="1386680589734" TEXT="Se compone exclusivamente de jueces">
<node CREATED="1392655694493" ID="ID_700363784" MODIFIED="1392655710578" TEXT="Actualmente un Juez por cada Estado miembro"/>
</node>
<node CREATED="1386680590007" ID="ID_1166690512" MODIFIED="1386680598852" TEXT="Desde el Tratado de Lisboa"/>
</node>
</node>
<node CREATED="1386669538664" FOLDED="true" ID="ID_1065034451" MODIFIED="1393879844668" POSITION="right" TEXT="El Tribunal de Cuentas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386680644148" ID="ID_1234976287" MODIFIED="1386702258325" TEXT="Composici&#xf3;n: un nacional de cada Estado miembro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386702191962" ID="ID_1683986801" MODIFIED="1386702201166" TEXT="Nombrados por el Consejo"/>
<node CREATED="1386680689888" ID="ID_50599758" MODIFIED="1386680706235" TEXT="Pertenecen a instituciones nacionales de control externo"/>
<node CREATED="1386680711359" ID="ID_1350695330" MODIFIED="1386680718596" TEXT="Especialmente calificadas"/>
<node CREATED="1386680722051" ID="ID_1168859800" MODIFIED="1386680728135" TEXT="Garant&#xed;as de independencia"/>
<node CREATED="1386680671691" ID="ID_91583146" MODIFIED="1386680681207" TEXT="Eligen entre ellos a su Presidente"/>
</node>
<node CREATED="1386680740901" ID="ID_1412726888" MODIFIED="1386702261273" TEXT="Funciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386680746088" ID="ID_1601543468" MODIFIED="1386680765850" TEXT="Controlar la obtenci&#xf3;n y utilizaci&#xf3;n de fondos"/>
<node CREATED="1386680769181" ID="ID_13665530" MODIFIED="1386680813042" TEXT="Totalidad de los ingresos y gastos de la UE"/>
<node CREATED="1386680830239" ID="ID_49152771" MODIFIED="1386680842516" TEXT="El control siempre externo">
<node CREATED="1386680865780" ID="ID_1386361810" MODIFIED="1386680876075" TEXT="Se verifican las cuentas"/>
<node CREATED="1389451998580" ID="ID_1808736118" MODIFIED="1389452014155" TEXT="Las instituciones son responsables del control interno"/>
</node>
<node CREATED="1386680877190" ID="ID_515211840" MODIFIED="1386680885519" TEXT="El control es a posteriori"/>
</node>
</node>
<node CREATED="1386669552753" FOLDED="true" ID="ID_1522506963" MODIFIED="1394469655862" POSITION="right" TEXT="El Consejo Europeo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386680906694" ID="ID_208395473" MODIFIED="1386702307278" TEXT="Composici&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386680913596" ID="ID_856404927" MODIFIED="1386680928788" TEXT="Jefes de Estado o de Gobierno"/>
<node CREATED="1386680935768" ID="ID_1462906963" MODIFIED="1386681256774" TEXT="Presidente, elegido por el Consejo"/>
<node CREATED="1386680938865" ID="ID_1666189637" MODIFIED="1386680947927" TEXT="Presidente de la Comisi&#xf3;n"/>
<node CREATED="1386680948169" ID="ID_244966945" MODIFIED="1386680995759" TEXT="Adicionalmente, Alto Representante de la UE para Asuntos Exteriores y pol&#xed;tica de seguridad"/>
</node>
<node CREATED="1386681034711" ID="ID_529338070" MODIFIED="1386702380459" TEXT="Funciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386702383197" ID="ID_365026771" MODIFIED="1386702395365" TEXT="No puede adoptar actos normativos"/>
<node CREATED="1386681063282" ID="ID_981421017" MODIFIED="1386681081908" TEXT="Formula declaraciones, conclusiones y comunicados">
<node CREATED="1386681082384" ID="ID_653556470" MODIFIED="1386681105471" TEXT="Env&#xed;a al Consejo para su plamaci&#xf3;n en acto normativo"/>
</node>
<node CREATED="1386681112140" ID="ID_854877586" MODIFIED="1386681139487" TEXT="Presentar&#xe1; al Parlamento Europeo un informe de cada una de sus reuniones"/>
<node CREATED="1386681141258" ID="ID_138858099" MODIFIED="1386681154424" TEXT="Se reunir&#xe1; dos veces por semestre">
<node CREATED="1386681188173" ID="ID_1894506608" MODIFIED="1386681194304" TEXT="Ordinarias: Bruselas"/>
<node CREATED="1386681194593" ID="ID_524316388" MODIFIED="1386681217181" TEXT="Extraordinarias: Pa&#xed;s que ostente la Presidencia"/>
</node>
</node>
</node>
<node CREATED="1386669556304" ID="ID_585748694" MODIFIED="1393880779376" POSITION="right" TEXT="El Banco Central Europeo">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Creado el 1 de junio de 1998 por el Tratado de Maastricht, a partir de la entrada en vigor del Tratado de Lisboa (1 de diciembre de2009) se le considera una instituci&#243;n europea.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386681276905" FOLDED="true" ID="ID_1641567513" MODIFIED="1394470256993" TEXT="Organizaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386681299878" ID="ID_1033393566" MODIFIED="1394469896511" TEXT="Consejo de Gobierno">
<node CREATED="1386681325642" ID="ID_512745202" MODIFIED="1386681335579" TEXT="Organo decisorio supremo"/>
<node CREATED="1386681342170" ID="ID_1952820266" MODIFIED="1394469903934" TEXT="Comit&#xe9; Ejecutivo, m&#xe1;s"/>
<node CREATED="1386681356748" ID="ID_651049839" MODIFIED="1393868145661" TEXT="Gobernadores de los Bancos Centrales de la zona euro"/>
</node>
<node CREATED="1386681307109" ID="ID_731181495" MODIFIED="1394469917334" TEXT="Comit&#xe9; Ejecutivo">
<node CREATED="1386681547593" ID="ID_188492458" MODIFIED="1386681575034" TEXT="Pone en pr&#xe1;ctica las orientaciones del Consejo de Gobierno"/>
<node CREATED="1386681411373" ID="ID_1407378120" MODIFIED="1386681425241" TEXT="Presidente y Vicepresidente del BCE"/>
<node CREATED="1386681519927" ID="ID_1386700134" MODIFIED="1393868189450" TEXT="4 miembros nombrados por los Jefes de Estado o de Gobierno de los pa&#xed;ses de la zona euro"/>
</node>
<node CREATED="1386681315853" ID="ID_900598943" MODIFIED="1394469924781" TEXT="Consejo General">
<node CREATED="1386681595775" ID="ID_239483080" MODIFIED="1386681609129" TEXT="Tercer &#xf3;rgano de decisi&#xf3;n"/>
<node CREATED="1386681609683" ID="ID_606552559" MODIFIED="1393868237577" TEXT="Presidente, Vicepresidente y Gobernadores de los Bancos Centrales de la UE"/>
</node>
<node CREATED="1386681670127" ID="ID_309216006" MODIFIED="1386681702328" TEXT="Presidente, Vicepresidente y miembros del Comit&#xe9; Ejecutivo">
<node CREATED="1386681705814" ID="ID_83298631" MODIFIED="1386681720697" TEXT="Nombrados por el Consejo Europeo"/>
<node CREATED="1386681727444" ID="ID_1233131163" MODIFIED="1386681746430" TEXT="Entre personas de reconocido pretigio"/>
</node>
</node>
<node CREATED="1386681281920" FOLDED="true" ID="ID_1458726405" MODIFIED="1394470254489" TEXT="Competencias">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386681771274" ID="ID_1220398018" MODIFIED="1386681781429" TEXT="Mantener la estabilidad de los precios"/>
<node CREATED="1386681781921" ID="ID_710200786" MODIFIED="1386681791063" TEXT="Definir la pol&#xed;tica monetaria"/>
<node CREATED="1386681791492" ID="ID_986109774" MODIFIED="1386681798839" TEXT="Salvaguardar el valor de la zona euro"/>
<node CREATED="1386681803559" ID="ID_309832843" MODIFIED="1386681807108" TEXT="Otras">
<node CREATED="1386681807108" ID="ID_1089022900" MODIFIED="1386681815969" TEXT="Emitir billetes y monedas"/>
<node CREATED="1386681816273" ID="ID_320118354" MODIFIED="1386681833028" TEXT="Cooperar en el &#xe1;mbito nacional y europeo"/>
<node CREATED="1393868304194" ID="ID_82298089" MODIFIED="1393868329500" TEXT="Estabilizar el sistema bancario"/>
</node>
<node CREATED="1386681854947" ID="ID_701290924" MODIFIED="1386681865072" TEXT="Definir la pol&#xed;tica monetaria de la zona euro"/>
</node>
<node CREATED="1386681285500" FOLDED="true" ID="ID_92760596" MODIFIED="1393880838628" TEXT="Funcionamiento">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386681899417" FOLDED="true" ID="ID_102302078" MODIFIED="1393868483847" TEXT="Independencia funcional">
<node CREATED="1386681993464" ID="ID_1378781887" MODIFIED="1386682023721" TEXT="Para la consecuci&#xf3;n de">
<node CREATED="1386682023721" ID="ID_1658867177" MODIFIED="1386682035312" TEXT="Objetivos de estabilidad en los precios"/>
<node CREATED="1386682035632" ID="ID_516868845" MODIFIED="1386682043619" TEXT="Apoyo a la pol&#xed;tica econ&#xf3;mica"/>
</node>
</node>
<node CREATED="1386681914386" FOLDED="true" ID="ID_760528406" MODIFIED="1393868482365" TEXT="Independencia institucional">
<node CREATED="1386681965703" ID="ID_560655213" MODIFIED="1393868472662" TEXT="Disfruta de personalidad jur&#xed;dica propia"/>
</node>
<node CREATED="1386681925407" FOLDED="true" ID="ID_519192356" MODIFIED="1393868481804" TEXT="Independencia personal">
<node CREATED="1386681954494" ID="ID_1247142815" MODIFIED="1386681961421" TEXT="Nombramiento de sus autoridades"/>
</node>
<node CREATED="1386681932654" FOLDED="true" ID="ID_710272989" MODIFIED="1393868481414" TEXT="Independencia financiera">
<node CREATED="1386681945282" ID="ID_1232669296" MODIFIED="1386681950883" TEXT="Autonom&#xed;a presupuestaria"/>
</node>
<node CREATED="1386702973660" ID="ID_1762802586" MODIFIED="1386703017838" TEXT="El Tribunal de Justicia ejerce sobre el BCE un control jurisdiccional"/>
</node>
</node>
<node CREATED="1392672332848" ID="ID_424045920" MODIFIED="1392672381494" POSITION="right" TEXT="Instituciones consultivas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1392672343734" ID="ID_1618518545" MODIFIED="1392673397183" TEXT="Comit&#xe9; de desarrollo econ&#xf3;mico y social">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sociedad civil: empresas, sindicatos, organizaciones no gubernamentales y ciudadanos individuales
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1392672371435" ID="ID_1065765741" MODIFIED="1393868698847" TEXT="Comit&#xe9; de regiones">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Creado en el tratado de Maastricht. Representantes de las autoridades regionales y locales
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1389477869257" ID="ID_1244998050" MODIFIED="1392657337417" POSITION="right" TEXT="TIC">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node COLOR="#ff0000" CREATED="1389478715840" ID="ID_1115194915" MODIFIED="1392670064234" TEXT="Horizon 2020">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#ff0000" CREATED="1389478141212" ID="ID_142455650" LINK="../Bloque%20IV/Planes%20de%20la%20Sociedad%20de%20la%20Informaci&#xf3;n.mm" MODIFIED="1394470281092" TEXT="Europa 2020 y el papel de las instituciones europeas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<icon BUILTIN="yes"/>
<icon BUILTIN="yes"/>
<node COLOR="#ff0000" CREATED="1389478134576" ID="ID_735541400" MODIFIED="1392670135076" TEXT="Agenda Digital Europea">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Estrategia dirigida a acercar a los ciudadanos europeos los beneficios de la sociedad de la informaci&#243;n.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1392665329949" ID="ID_1269856024" MODIFIED="1392670227510" TEXT="ISA">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Programa para facilitar la interoperabilidad de la informaci&#243;n intercambiada entre los pa&#237;ses miembros y la construcci&#243;n de servicios europeos transfronterizos
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1392665343614" ID="ID_1582056024" MODIFIED="1392670268995" TEXT="EIF">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Establece los elementos b&#225;sicos de la interoperabilidad: conceptos, principios,vocabulario
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1392665348739" ID="ID_397952442" MODIFIED="1392670309813" TEXT="EIS">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Fija las orientaciones, los objetivos y las medidas para conseguir la interoperabilidad europea
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</map>
