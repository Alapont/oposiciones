<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1370708769832" ID="ID_930873946" MODIFIED="1431243339279" TEXT="Tratamiento de Im&#xe1;genes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375006585730" ID="ID_1206514830" MODIFIED="1431243341008" POSITION="right" TEXT="Conceptos">
<node CREATED="1375006800379" ID="ID_824575978" MODIFIED="1431251707585" TEXT="S&#xf3;lo percibimos 7 millones de colores y 200 niveles de grises"/>
<node CREATED="1375006877443" ID="ID_1786921863" MODIFIED="1431251740539" TEXT="8 bits/pixel = 16 millones de colores y 256 ND">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="idea"/>
<node CREATED="1431251674328" ID="ID_1521487964" MODIFIED="1431251746955" TEXT="ND = niveles de grises"/>
</node>
<node CREATED="1431243648844" ID="ID_1478699194" MODIFIED="1431248106504" TEXT="Imagen de Raster o Mapa de Bits">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431243716282" ID="ID_594824703" MODIFIED="1431243792988" TEXT="Matrices de p&#xed;xeles que conforman una imagen digital"/>
</node>
<node CREATED="1431243794275" ID="ID_1740967842" MODIFIED="1431248106264" TEXT="Imagen vectorial">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431243800474" ID="ID_1561953464" MODIFIED="1431243872493" TEXT="Se componen de formas matem&#xe1;ticas (l&#xed;neas, c&#xed;rculos, curvas...) codificadas con f&#xf3;rmulas matem&#xe1;ticas"/>
<node CREATED="1431243849573" ID="ID_370732087" MODIFIED="1431243898220" TEXT="Tama&#xf1;os de archivo manejables y se redimensionan sin p&#xe9;rdida de calidad"/>
</node>
<node CREATED="1431248086565" ID="ID_1399383456" MODIFIED="1431250375847" TEXT="Thumbnails">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431248091775" ID="ID_167767015" MODIFIED="1431248103099" TEXT="Miniaturas en Internet de las im&#xe1;genes"/>
</node>
<node CREATED="1431248979662" ID="ID_520022441" MODIFIED="1431250378206" TEXT="Algoritmo de compresi&#xf3;n con p&#xe9;rdida">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431248989590" ID="ID_1989453560" MODIFIED="1431251789428" TEXT="Representa informaci&#xf3;n utilizando una menor cantidad de la misma, &#xa;siendo imposible una reconstrucci&#xf3;n exacta de los datos originales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1431250379942" ID="ID_1939445413" MODIFIED="1431250420504" TEXT="Algoritmo de compresi&#xf3;n sin p&#xe9;rdida">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431250389342" ID="ID_685572775" MODIFIED="1431251756858" TEXT="La compresi&#xf3;n es reversible y puede recuperarse una imagen id&#xe9;ntica a la original">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1375007213323" ID="ID_1323509330" MODIFIED="1431247810000" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tratamientos
    </p>
    <p>
      tras el escaneado
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431252875935" ID="ID_1728014220" MODIFIED="1431252895464">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Correcciones y
    </p>
    <p>
      realce de la imagen
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1375007231339" ID="ID_1369797750" MODIFIED="1518439326323">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Correcciones radiom&#233;tricas
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431253100959" ID="ID_1978139821" MODIFIED="1431253107870" TEXT="Afecta a todos los p&#xed;xeles por igual, independientemente de la posici&#xf3;n"/>
</node>
<node CREATED="1375007245022" ID="ID_108007816" MODIFIED="1431253538359">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Correcciones geom&#233;tricas
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431253166081" ID="ID_1485271276" MODIFIED="1431253172896" TEXT="No son iguales para todos los p&#xed;xeles, dependen de la posici&#xf3;n"/>
</node>
</node>
<node CREATED="1431247864459" ID="ID_612693537" MODIFIED="1431247954315" TEXT="Retoques de imagen">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431247867391" ID="ID_1576868830" MODIFIED="1431247951635" TEXT="Sharpening">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431247883239" ID="ID_518216095" MODIFIED="1431247886191" TEXT="Aumento de nitidez"/>
</node>
<node CREATED="1431247894221" ID="ID_803197974" MODIFIED="1431247951636" TEXT="Despeckling">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431247911075" ID="ID_758886577" MODIFIED="1431247914784" TEXT="Eliminaci&#xf3;n de puntos"/>
</node>
<node CREATED="1431247922849" ID="ID_1937028011" MODIFIED="1431247951635" TEXT="Descreening">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431247930692" ID="ID_1314694958" MODIFIED="1431249094816" TEXT="Eliminaci&#xf3;n de muar&#xe9; (patr&#xf3;n ondulado que aparece en algunas im&#xe1;genes, como interferencias)"/>
</node>
<node CREATED="1431248004332" ID="ID_249155906" MODIFIED="1431248015573" TEXT="Deskewing">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431248007077" ID="ID_1524336402" MODIFIED="1431248012088" TEXT="Eliminaci&#xf3;n de oblicuidad"/>
</node>
</node>
<node CREATED="1431252557637" ID="ID_627424754" MODIFIED="1431253339399" STYLE="bubble" TEXT="Escalado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431252571700" ID="ID_1101125920" MODIFIED="1431253342325" TEXT="Cambio del tama&#xf1;o de la imagen sin variar su resoluci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1370711635961" ID="ID_54601134" MODIFIED="1431243341030" POSITION="right" TEXT="Formatos de Ficheros">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370711850778" ID="ID_1130036160" MODIFIED="1431243341031" TEXT="PDF (Portable Document Format)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370711866476" ID="ID_1933550552" MODIFIED="1431249149358" TEXT="Multip&#xe1;gina">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1431246425204" ID="ID_524408831" MODIFIED="1431248365872" TEXT="Formato ENI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1370711785583" ID="ID_474168889" MODIFIED="1431243341033" TEXT="TIFF (Tagged Image File Format)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431250637588" ID="ID_1718353018" MODIFIED="1431250889494" TEXT="Comprimido o no comprimido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1377334091106" ID="ID_1410304699" MODIFIED="1431250861605" TEXT="Compresi&#xf3;n LZW o RLE sin p&#xe9;rdidas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1431248166970" ID="ID_826514191" MODIFIED="1431250729737" TEXT="Soporta millones de colores (24 bits - 16,7 millones de colores) ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1370711843129" ID="ID_1046019600" MODIFIED="1431249260806" TEXT="Multip&#xe1;gina (puede guardar m&#xe1;s de una imagen)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1431245546262" ID="ID_1665567010" MODIFIED="1431248250931" TEXT="No soporta animaciones ni fondos transparentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1370711825395" ID="ID_1776148684" MODIFIED="1431249140114" TEXT="Existen versiones comprimidas que s&#xf3;lo soportan B&amp;N">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1431246416798" ID="ID_1533247811" MODIFIED="1431248232868" TEXT="Formato ENI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1370711818049" ID="ID_1699971641" MODIFIED="1431248126751" TEXT="Muy usado en Gesti&#xf3;n documental">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370711757608" ID="ID_1635781033" MODIFIED="1431251323976">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      BMP (Windows Bitmap)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431250450288" ID="ID_1556391036" MODIFIED="1431250871030" TEXT="Propietario (Microsoft)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1370711769524" ID="ID_275988878" MODIFIED="1431249162707" TEXT="No comprimido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1370711777694" ID="ID_478834903" MODIFIED="1431249176572" TEXT="Sin p&#xe9;rdidas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1431249206342" ID="ID_542597036" MODIFIED="1431249226613" TEXT="Soporta millones de colores (24 bits - 16,7 millones de colores)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1370711806911" ID="ID_373371832" MODIFIED="1431249168845" TEXT="No multip&#xe1;gina">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1431245546262" ID="ID_1483162433" MODIFIED="1431249182276" TEXT="No animaciones ni fondos transparentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1370711879238" ID="ID_1386441" MODIFIED="1431243341038" TEXT="GIF (Graphics Interchange Format)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431246362513" ID="ID_619573298" MODIFIED="1431249448645" TEXT="Formato propietario">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
</node>
<node CREATED="1370793015237" ID="ID_1026872650" MODIFIED="1431250927383" TEXT="Compresi&#xf3;n LZW sin p&#xe9;rdidas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1370711907063" ID="ID_50527125" MODIFIED="1431249416772" TEXT="Soporta 256 colores (8 bits por pixel)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1370712056910" ID="ID_1010665103" MODIFIED="1431249423580" TEXT="Soporta animaciones y fondos transparentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1370711916670" ID="ID_847534570" MODIFIED="1431249439405" TEXT="Algoritmo de compresi&#xf3;n LZW patentado">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="idea"/>
</node>
</node>
<node CREATED="1370711927503" ID="ID_1346924190" MODIFIED="1431243341040" TEXT="PNG (Portable Network Graphics)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370711983180" ID="ID_1798620031" MODIFIED="1431249968543" TEXT="Compresi&#xf3;n sin p&#xe9;rdida mediante &quot;Algoritmo de deflaci&#xf3;n&quot;">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1370793004373" ID="ID_1470135022" MODIFIED="1431249599594" TEXT="No tiene p&#xe9;rdidas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1431248166970" ID="ID_203194154" MODIFIED="1431251385462" TEXT="Soporta trillones de colores (48 bits - 281 trillones de colores) ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1370793025689" ID="ID_1665353974" MODIFIED="1431249601466" TEXT="No soporta animaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_cancel"/>
<node CREATED="1431250256792" ID="ID_1777347791" MODIFIED="1431250260065" TEXT="Para ello">
<node CREATED="1431250260216" ID="ID_898397237" MODIFIED="1431250297748" TEXT="MNG (Multiple-image Network Graphics)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1431250289267" ID="ID_1443918420" MODIFIED="1431250297748" TEXT="APNG (Animated Portable Network Graphics)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431250291187" ID="ID_1269467437" MODIFIED="1431250294434" TEXT="Extensi&#xf3;n de PNG"/>
</node>
</node>
</node>
<node CREATED="1431246438626" ID="ID_113647724" MODIFIED="1431248341511" TEXT="Formato ENI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1431246551712" ID="ID_844370766" MODIFIED="1431249566002" TEXT="Est&#xe1;ndar recomendado por el W3C para publicaci&#xf3;n Web">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1370712015013" ID="ID_1004992933" MODIFIED="1431246704311">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      JPEG (Joint Photographic Expert Group)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370792992452" ID="ID_1941375176" MODIFIED="1431249946878" TEXT="Formato comprimido con p&#xe9;rdidas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_cancel"/>
<node CREATED="1370712068184" ID="ID_1732415074" MODIFIED="1431250100623">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Compresi&#243;n mediante DTC (Discrete Cosine Transform)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370712038506" ID="ID_139520741" MODIFIED="1431251077684" TEXT="Soporta millones de colores (24 bits - 16,7 millones de colores) ">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1370712047060" ID="ID_156053652" MODIFIED="1431249815969" TEXT="No soporta animaciones">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1431246707197" ID="ID_1328058927" MODIFIED="1431246715535" TEXT="Se utiliza indistintamente .jpg o .jpeg"/>
<node CREATED="1431246447174" ID="ID_1313588582" MODIFIED="1431250184350" TEXT="Formato ENI (.jpg, .jpeg)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1370712106909" ID="ID_357152300" MODIFIED="1431250189031" TEXT="Formato JPEG 2000">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<node CREATED="1431249858851" ID="ID_1326590662" MODIFIED="1431249897068" TEXT="Compresi&#xf3;n sin p&#xe9;rdida">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1431249872027" ID="ID_1604648690" MODIFIED="1431249892588" TEXT="Tiene gran n&#xfa;mero de patentes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1431249863772" ID="ID_1462658139" MODIFIED="1431250192904" TEXT=".jp2">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1431248723933" ID="ID_625670999" MODIFIED="1431248787103" TEXT="RAW">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431248727825" ID="ID_1640355593" MODIFIED="1431248755957" TEXT="Contiene la totalidad de datos tal y como han sido captados por el sensor digital"/>
<node CREATED="1431248756900" ID="ID_1043350142" MODIFIED="1431251518292" TEXT="Compresi&#xf3;n sin p&#xe9;rdida de informaci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1383051640420" ID="ID_413009984" MODIFIED="1431244112262" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tecnolog&#237;as de
    </p>
    <p>
      adquisici&#243;n
    </p>
    <p>
      de im&#225;genes
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383051773971" ID="ID_629121488" MODIFIED="1431245417800" STYLE="bubble" TEXT="CCD (Charged-Coupled Device)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="up"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1383051807295" ID="ID_1424837102" MODIFIED="1431245417800" TEXT="Circuito electr&#xf3;nico fotosensible">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1383051895746" ID="ID_1458141128" MODIFIED="1431245417801" TEXT="La tecnologia m&#xe1;s utilizada en la actualidad">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384601749017" ID="ID_970135578" MODIFIED="1431245417801" TEXT="Escaneres y C&#xe1;maras digitales de alta gama">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1383051822343" ID="ID_1742869807" MODIFIED="1431244342667" TEXT="CMOS (Complementary Metal Oxide Semiconductor)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383051876001" ID="ID_535994808" MODIFIED="1431244349012" TEXT="C&#xe1;maras digitales de baja gama">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1383051915811" ID="ID_1225454268" MODIFIED="1431245434757" STYLE="bubble" TEXT="CIS (Contact Image Sensor)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383051964933" ID="ID_1501929786" MODIFIED="1431245434758" TEXT="Extremo inferior del mercado de esc&#xe1;neres">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384601895308" ID="ID_1767164057" MODIFIED="1431245480670" TEXT="Basado en diodos LED">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384601806019" ID="ID_1788415134" MODIFIED="1431245434758" TEXT="Tecnolog&#xed;a reciente que est&#xe1; reemplazando a los CCDs">
<node CREATED="1384601877595" ID="ID_1589476953" MODIFIED="1431245434758" TEXT="Tiene menor calidad que CCD"/>
<node CREATED="1384601887019" ID="ID_275179547" MODIFIED="1431245434758" TEXT="Pero es m&#xe1;s ligero"/>
</node>
</node>
<node CREATED="1383051917652" ID="ID_622620763" MODIFIED="1431244342667" TEXT="PMT (Photomultiplier tube)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1383051998194" ID="ID_678285603" MODIFIED="1431244354967" TEXT="Extremo superior del mercado de esc&#xe1;neres">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1384601978858" ID="ID_1442192965" MODIFIED="1384601982988" TEXT="M&#xe1;s antiguos"/>
<node CREATED="1384601729551" ID="ID_1572502563" MODIFIED="1384601744887" TEXT="Lentos y caros comparados con CCD"/>
</node>
</node>
<node CREATED="1370712888566" ID="ID_644100955" MODIFIED="1431244727856" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Par&#225;metros de calidad
    </p>
    <p>
      de documento digitalizado
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431244432134" ID="ID_504094465" MODIFIED="1431252033467">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Resoluci&#243;n
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="idea"/>
<node CREATED="1431252013513" ID="ID_1211407105" MODIFIED="1431252030492">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Medida de la densidad o cantidad de informaci&#243;n digital&#160;
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1431252021722" ID="ID_1395109012" MODIFIED="1431252030494" TEXT="Cantidad total de informaci&#xf3;n de una imagen digital (p&#xed;xeles)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1431252054893" ID="ID_1834899420" MODIFIED="1431252083685" TEXT="Resoluci&#xf3;n interpolada">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="idea"/>
<node CREATED="1431252072085" ID="ID_360935519" MODIFIED="1431252076550" TEXT="Cantidad de informaci&#xf3;n que se puede obtener aplicando algoritmos matem&#xe1;ticos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1370712910155" ID="ID_906548634" MODIFIED="1431244794189">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      M&#233;todos para
    </p>
    <p>
      evaluar la resoluci&#243;n
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1370713147421" ID="ID_1641322349" MODIFIED="1431244736336" TEXT="Modelos de resoluci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1383052589025" ID="ID_20092148" MODIFIED="1431253262059" TEXT="Consiste en medir la resoluci&#xf3;n de los detalles, la nitidez, etc.">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370713157059" ID="ID_702146266" MODIFIED="1431244787985">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Funci&#243;n de la modulaci&#243;n
    </p>
    <p>
      de la transferencia (MTF):
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1383052238480" ID="ID_1415939997" MODIFIED="1431244779612" TEXT=" Medir la intensidad de la luz en el proceso de imagen (calibraci&#xf3;n)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370713205383" ID="ID_1378902114" MODIFIED="1431245051797">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Respuesta de la frecuencia espacial
    </p>
    <p>
      (Spacial Frecuency Response - SFR)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1431244810082" ID="ID_804455329" MODIFIED="1431244840790" TEXT="Capacidad del esc&#xe1;ner de transmitir informaci&#xf3;n de alta frecuencia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370713263816" ID="ID_1553836503" MODIFIED="1431245150234" STYLE="bubble" TEXT="ISO 12233: 2014 M&#xe9;todos de medida de la Resoluci&#xf3;n y SFR de c&#xe1;maras digitales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-4"/>
</node>
</node>
<node CREATED="1370713286583" ID="ID_807881028" MODIFIED="1431253557704">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reproducci&#243;n tonal
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1384453120952" ID="ID_726530957" MODIFIED="1384453352991" TEXT="Determina el grado de claridad u oscuridad de una imagen, as&#xed; como el contraste">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1370712964017" ID="ID_1508815141" MODIFIED="1431253572472" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Profundidad de
    </p>
    <p>
      bit o de color
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431252131799" ID="ID_225281488" MODIFIED="1431252133885" TEXT="N&#xfa;mero de bits utilizados para definir cada p&#xed;xel"/>
<node CREATED="1370712973236" ID="ID_1727059352" MODIFIED="1431252152631" TEXT="Blanco o negro - 1bpp (1 bit por pixel)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1370712988169" ID="ID_1616158083" MODIFIED="1431252163367" TEXT="Escala de grises - 8bpp (8 bits por pixel)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1370713008157" ID="ID_1752212206" MODIFIED="1431252189011">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Escaneado en color - 24bpp (24 bits por pixel: 8 por cada uno de los tres colores)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1370713386771" ID="ID_899209648" MODIFIED="1431245403135" TEXT="RGB">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431245285654" ID="ID_1080685850" MODIFIED="1431245403135" TEXT="Red, Green, Blue"/>
<node CREATED="1431253306499" ID="ID_538348189" MODIFIED="1431253310572" TEXT="Minotires y Escaners"/>
</node>
<node CREATED="1370713395816" ID="ID_1042385929" MODIFIED="1431245403136" TEXT="CMYK">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431245198650" ID="ID_154313727" MODIFIED="1431245403136" TEXT="Cyan, Magenta, Yellow y Key (negro)"/>
<node CREATED="1431253311770" ID="ID_1084615676" MODIFIED="1431253315396" TEXT="Impresi&#xf3;n y fotograf&#xed;a"/>
</node>
</node>
</node>
<node CREATED="1370713403601" ID="ID_47679341" MODIFIED="1383052549709" TEXT="Ruido">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1431252202400" ID="ID_1241942601" MODIFIED="1431252208761" TEXT="Procede del sensor y los componentes electr&#xf3;nicos asociados"/>
</node>
</node>
<node CREATED="1431252430985" ID="ID_3187987" MODIFIED="1431252431816" POSITION="right" TEXT="Reconocimiento de texto">
<node CREATED="1431252440916" ID="ID_1466473239" MODIFIED="1431252526484">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      OCR - Optical Character Recognition
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1431252443223" ID="ID_1413365590" MODIFIED="1431252498780" TEXT="Se convierte la imagen en texto (ya sea ASCII o UNICODE)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1431252458802" ID="ID_763522582" MODIFIED="1431252526483">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      ICR - Intelligent Character Recognition
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1431252460303" ID="ID_225680346" MODIFIED="1431252495637" TEXT="Permite aprender nuevos caracteres y formatos mediante el uso de redes neuronales">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</map>
