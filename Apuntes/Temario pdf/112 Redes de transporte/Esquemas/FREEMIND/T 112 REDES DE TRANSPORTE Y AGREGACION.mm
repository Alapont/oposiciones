<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1429694009565" ID="ID_1003496803" MODIFIED="1557131525896">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      T 112 REDES DE TRANSPORTE Y AGREGACION
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1480697397078" ID="ID_99707510" MODIFIED="1480697413578" POSITION="right" TEXT="REDES DE TRANSPORTE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1480697473500" ID="ID_160200656" MODIFIED="1480697559343" TEXT="JDS x WDM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1480766251734" ID="ID_1428883797" MODIFIED="1480766634812">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>WDM: </b>Multiplexaci&#243;n por divisi&#243;n en longitud de onda
    </p>
  </body>
</html></richcontent>
<node CREATED="1480766382953" ID="ID_843699377" MODIFIED="1480766385453" TEXT="Enviar varias se&#xf1;ales a diferent es longitudes de onda (diferentes  &#x3bb; ) por  una misma fibra  (luz de varios &#x2018;colores&#x2019;)">
<node CREATED="1480766423703" ID="ID_993707653" MODIFIED="1480766458734">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Densa (DWDM, &#8216;Dense&#8217; WDM): se utilizan 16 o m&#225;s&#160;&#160;&#955;
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1480766461093" ID="ID_991904718" MODIFIED="1480766463484" TEXT=" &#xa0;&#xa0;&#xa0;Ligera (CWDM &#x2018;Coarse&#x2019; WDM): se utilizan 2 &#xf3; 4&#xa0;&#xa0;&#x3bb;"/>
</node>
<node CREATED="1480767544406" ID="ID_101339661" MODIFIED="1480767584718">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ventanas de transmisi&#243;n&#160;&#243;ptica
    </p>
  </body>
</html></richcontent>
<node CREATED="1480767587593" ID="ID_129285114" MODIFIED="1480767622375">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1&#170; ventana: 850nm
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1480767595703" ID="ID_101139433" MODIFIED="1480767631562">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      2&#170; ventana: 1310 nm
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1480767604265" ID="ID_1656678880" MODIFIED="1480767650796">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      3&#170; ventana: 1550 nm
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1480766314109" ID="ID_845440662" MODIFIED="1480766702765">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>JDS (SDH)&#160;(Jerarqu&#237;a Digital S&#237;ncrona)</b>: Est&#225;ndar ITU para multiplexaci&#243;n en el tiempo
    </p>
  </body>
</html></richcontent>
<node CREATED="1480766566687" ID="ID_401056137" MODIFIED="1480767243578">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Evoluci&#243;n de JPD (Jerarqu&#237;a Digital Plesi&#243;crana) para incluir medios &#243;pticos de transmisi&#243;n y superar el m&#225;ximo agregado de 140 Mbps
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1480766705953" ID="ID_1405017180" MODIFIED="1480767389500">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>La sincronizaci&#243;n </b>ha de estar completamente garantizada en una red SDH, de lo contrario, se puede tener una considerable degradaci&#243;n en el funcionamiento de la red o incluso su fallo total, por ello todos los elementos de red deben estar sincronizados por una se&#241;al de reloj central muy precisa
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1480767920609" ID="ID_1210424998" MODIFIED="1480768097843">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Jerarqu&#237;as est&#225;ndar de multiplexaci&#243;n JDS
    </p>
  </body>
</html></richcontent>
<node CREATED="1480767977093" ID="ID_1551045487" MODIFIED="1480767985468" TEXT="STM-1 : 155 Mbps"/>
<node CREATED="1480767985937" ID="ID_759096344" MODIFIED="1480767994406" TEXT="STM-4 : 622 Mbps"/>
<node CREATED="1480767994750" ID="ID_741839560" MODIFIED="1480768019359">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      STM-16 :&#160;2488 Mbps
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1480768020453" ID="ID_486062878" MODIFIED="1480768036671">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      STM-64&#160;&#160;9953 Mbps
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1429694028733" ID="ID_1374066154" MODIFIED="1480697577156" TEXT="MPLS">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429697328228" ID="ID_196493707" MODIFIED="1429697352684" TEXT="MultiProtocol Label Switching ">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1429697166750" ID="ID_880044822" MODIFIED="1429697322663" TEXT="Tecnolog&#xed;a de conmutaci&#xf3;n (etiquetas), proporciona circuitos virtuales  ">
<icon BUILTIN="info"/>
<node CREATED="1429697391268" ID="ID_1445660262" MODIFIED="1429698003057" TEXT="Opera entre capa 2 (enlace datos) y 3 (red) de OSI">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1429701520762" ID="ID_1541841487" MODIFIED="1429701523272" TEXT="Apps">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429701523665" ID="ID_725932120" MODIFIED="1429701845844">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ing de tr&#225;fico (adapta flujos a recursos), diferentes <u>QoS</u>&#160;para apps (rutas prefijadas, Diffserv), VPNs
    </p>
  </body>
</html></richcontent>
<node CREATED="1429697250473" ID="ID_1381719011" MODIFIED="1429697265670" TEXT="Posible sustituto de ATM y FR"/>
</node>
</node>
<node CREATED="1480767823921" ID="ID_592221531" MODIFIED="1480768239468">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>GMPLS&#160;(General MPLS): </i><font size="3" face="SansSerif">versi&#243;n extendida de MPLS para abarcar la divisi&#243;n en el tiempo, (por ejemplo, SONET / SDH, PDH, G.709), longitudes de onda (lambdas) y conmutaci&#243;n espacial</font>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1429697829280" ID="ID_1184358375" MODIFIED="1429698158648" TEXT="Cabecera">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429697831262" ID="ID_1447503621" MODIFIED="1429697839267" TEXT="Solo si la tecnolog&#xed;a no soporta un campo para ello">
<node CREATED="1429697702876" ID="ID_638623224" MODIFIED="1429697724340" TEXT="En IPv6, la etiqueta va en el campo &apos;Etiqueta de Flujo&apos; de la cabecera">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1429698152208" ID="ID_682110918" MODIFIED="1429698224038">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>32 bits</u>&#160;(entre cabeceras de nivel 2 y 3)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1429697853785" ID="ID_768324237" MODIFIED="1429698149196" TEXT="TTL, Stack (jerarqu&#xed;a de etiquetas), EXP (QoS) y Etiqueta (20 bits)"/>
</node>
</node>
<node CREATED="1429697446637" ID="ID_1992415352" MODIFIED="1429697452135" TEXT="Elementos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429697452484" ID="ID_168786136" MODIFIED="1429697610110">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>LSR (Label Switch Router):</i>&#160;conmutan tr&#225;fico seg&#250;n etiquetas, estableciendo circuitos (LPS)
    </p>
  </body>
</html></richcontent>
<node CREATED="1429697741291" ID="ID_1740024160" MODIFIED="1429700767866">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>LIB (Library Info Base):&#160; </i><u>tabla</u>&#160;con info de enrut gracias a protocs de distrib de etiquetas
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429697500460" ID="ID_1940291044" MODIFIED="1429697531235">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>LER (Label Edge Router):</i>&#160;inicia/termina t&#250;nel (pone/quita cabeceras MPLS)
    </p>
  </body>
</html></richcontent>
<node CREATED="1429697532753" ID="ID_1962416408" MODIFIED="1429697547868" TEXT="Ingress Router (de entrada) o Egress (de salida)"/>
</node>
<node CREATED="1429697551473" ID="ID_1942113299" MODIFIED="1429697601994">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>LSP (Label Switched Path):</i>&#160;circuito virtual (t&#250;nel) de un&#160; <u>&#250;nico sentido</u>&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1429698793200" ID="ID_1719685192" MODIFIED="1429698855136" TEXT="Tipos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429698796761" ID="ID_1674017072" MODIFIED="1429698860551">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Salto a salto: </i>cada LSR selecciona indep el pr&#243;ximo salto para cada FEC
    </p>
  </body>
</html></richcontent>
<node CREATED="1429701342296" ID="ID_1149669528" MODIFIED="1429701426138" TEXT="LDP, RSVP-TE o CR-LDP"/>
</node>
<node CREATED="1429698817728" ID="ID_425463165" MODIFIED="1429701390997">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Expl&#237;cito</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1429701391585" ID="ID_845537607" MODIFIED="1429701398201">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Bajo demanda (desde LER Ingress)
    </p>
  </body>
</html></richcontent>
<node CREATED="1429701405647" ID="ID_637324160" MODIFIED="1429701416906" TEXT="CR-LDP o RSVP-TE"/>
</node>
<node CREATED="1429701382136" ID="ID_360403532" MODIFIED="1429701404403">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No solicitado (desd LER Egress)
    </p>
  </body>
</html></richcontent>
<node CREATED="1429701418654" ID="ID_1122700425" MODIFIED="1429701419513" TEXT="LDP"/>
</node>
</node>
</node>
</node>
<node CREATED="1429697616904" ID="ID_1201185223" MODIFIED="1429697652589">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>FEC (Forward Equiv Class):</i>&#160;conjunto de paquetes (tr&#225;fico) sobre el mismo LSP
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429698523182" ID="ID_1701558070" MODIFIED="1429698532439" TEXT="Proceso de conmutaci&#xf3;n">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429698532933" ID="ID_1605028227" MODIFIED="1429698650725">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>LER Ingress:</i>&#160;identifica FEC y le asigna etiqueta
    </p>
  </body>
</html></richcontent>
<node CREATED="1429698550700" ID="ID_391883002" MODIFIED="1429698633583">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Si lo recibe LSR:</i>&#160;busca en LIB pr&#243;ximo salto y le&#160; <u>cambia la etiqueta</u>&#160;por la correspondiente
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429698588731" ID="ID_999708644" MODIFIED="1429698641011">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Si lo recibe LER Egress:</i>&#160;elimina etiqueta y lo encamina por mecanismo habitual
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1429700821676" ID="ID_1395012431" MODIFIED="1429701124458" TEXT="Protocolos de distribuci&#xf3;n de etiquetas">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429700836803" ID="ID_1109031767" MODIFIED="1429700895831" TEXT="Proceds por los cuales enrutador LSR/LER informa a otro de relaci&#xf3;n etiqueta/FEC">
<icon BUILTIN="info"/>
</node>
<node CREATED="1429700969932" ID="ID_1686934339" MODIFIED="1429701146229" TEXT="Label Distribution (LDP)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429700976371" ID="ID_1624333469" MODIFIED="1429701003099" TEXT="Intercambio de info de mapeo de etiquetas entre dos LSR">
<node CREATED="1429701003100" ID="ID_1722508883" MODIFIED="1429701053901" TEXT="Bidir, por intercambio de PDUs (sobre conexiones TCP)"/>
</node>
</node>
<node CREATED="1429701164372" ID="ID_623469290" MODIFIED="1429701177069" TEXT="Resource Reserv - Traffic Engineer (RSVP-TE)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429701177867" ID="ID_1642143213" MODIFIED="1429701227198" TEXT="T&#xfa;neles bajo demanda (reserva de recursos desde origen)"/>
</node>
<node CREATED="1429701242994" ID="ID_241887571" MODIFIED="1429701256715" TEXT="Constraint-Based Routing LDP (CR-LDP)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429701257233" ID="ID_1643547570" MODIFIED="1429701311099">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      C&#225;lculo de trayectos con&#160; <u>restricciones</u>&#160;(BW, QoS, delay...)
    </p>
  </body>
</html></richcontent>
<node CREATED="1429701293082" ID="ID_126036034" MODIFIED="1429701301324" TEXT="Controla dimensionado de tr&#xe1;fico"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1480697414359" ID="ID_1277293777" MODIFIED="1480697432078" POSITION="right" TEXT="REDES DE AGREGACI&#xd3;N">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445328065655" ID="ID_1490412315" MODIFIED="1445328197746">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      ATM
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445331026103" ID="ID_162991668" MODIFIED="1445333391233">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Async Tx Mode) aprovecha sils en redes de opers, <u>conmut de paqs</u>&#160;(CVs). Async&#8594;compet por el canal
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1447836817452" ID="ID_299108319" MODIFIED="1447838458868">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Integr y aprovechar tecnolog&#237;as anteriores ( <u>JDP</u>/S, FR, Eth...)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1445331387164" ID="ID_1449089555" MODIFIED="1445332504231">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Celdas fijas de <u>53B</u>&#160;(48 payload y 5 cabec)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1447835898755" ID="ID_1793040757" MODIFIED="1447838635085">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>-retardos</u>&#160;y conmut +eficiente, datos+voz+audio, vels var (&lt; <u>2488Mbps</u>)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
<node CREATED="1445331510900" ID="ID_257542145" MODIFIED="1447841262834">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>RDSI-BA:</i>&#160;=mod ref q RDSI-BE, orient a <u>conex</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1447836871743" ID="ID_1277147014" MODIFIED="1447838596723">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No fiable: solo corrige 1 bit de cabec, y algunas capas AAL ofrecen FEC y recup (extremo a extr)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1445328095550" ID="ID_1599132649" MODIFIED="1445328097964" TEXT="Arq">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1447838159726" ID="ID_267754596" MODIFIED="1447838166358" TEXT="Planos funcionales">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1447838166361" ID="ID_574473886" MODIFIED="1447838369282">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Usuario (ctrl flujo y errores), Ctrl (conexiones) y Gesti&#243;n (adm red)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1445331795789" ID="ID_464139769" MODIFIED="1447837381603">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Capa AAL (Adapt)
    </p>
    <p>
      (nivel 4 OSI)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445332278207" ID="ID_898734022" MODIFIED="1447837349994">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>CS: </i>(Converg) adapta vel users, <u>detecta&#160;errores</u>, sync extrems, ctrl flujo
    </p>
  </body>
</html></richcontent>
<node CREATED="1445332413950" ID="ID_599697593" MODIFIED="1447840744032">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>SAR: </i>(Segm &amp; Ressemble) <u>segm</u>-48B, cabec de reensamblaje y relleno
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1447837381594" ID="ID_1947730442" MODIFIED="1447838102949">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tipos
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445332527428" ID="ID_1500794942" MODIFIED="1447837757974">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      AAL-1 (Tasa y Retardo cte, circs dedic), 2 (T var y R cte), 3/4 (T y R var) o 5 (+efic: no cab, best-eff)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1447837801117" ID="ID_534705361" MODIFIED="1447837809662" TEXT="=Clases A-B-C-D"/>
</node>
</node>
</node>
<node CREATED="1445331785433" ID="ID_850061072" MODIFIED="1447837195535">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Capa ATM
    </p>
    <p>
      (nivel 3-2)
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445332049924" ID="ID_1310351791" MODIFIED="1445332091051">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Construc/extrac cabecs, <u>enrut</u>, (de)mux celdas, ctrl cong
    </p>
  </body>
</html></richcontent>
<node CREATED="1445332092032" ID="ID_400927710" MODIFIED="1445332118909">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Solo nodos extremos tienen todos los niveles (intermedios solo retx)&#160;
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1445332130425" ID="ID_512128402" MODIFIED="1447840199021">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cabecera</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1447840199026" ID="ID_1406225708" MODIFIED="1447841385589">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      GFC (Gen Flow Ctrl),&#160; <u>VPI</u>&#160;(Id Path), <u>VCI</u>&#160;&#160;(Id Canal), PTI (Id tipo payl), CLP (prior,=DE/FR), HEC (err cab)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1445332183431" ID="ID_508588996" MODIFIED="1447837657131">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Un CV se id con tupla&#160; <u>VPI/VCI </u>, y van cambiando en cada salto
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1447840449391" ID="ID_928092565" MODIFIED="1447840581903">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Conex extr-extr: </i>&#160;VPC y VCC
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1445331811079" ID="ID_864418154" MODIFIED="1447837201180">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Capa f&#237;sica </i>
    </p>
    <p>
      <i>(nivel 2-1)</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1445331848943" ID="ID_1642595326" MODIFIED="1445333450609">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>TC:</i>&#160;(Converg de Tx) indep dl medio, ctrl errores de cabec (HEC), monta celdas, inserc OAM y relleno
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1445331935971" ID="ID_437593064" MODIFIED="1445333444612">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>PDM: </i>(Dep dl Medio F&#237;s)&#160;<u>tx/rx celdas</u>, reloj, codif l&#237;nea, interfs&#160; <u>UNI</u>&#160;(user-red) y <u>NNI</u>&#160;(red-red)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1445328106394" ID="ID_133891931" MODIFIED="1445328109996" TEXT="Servicios">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445333090751" ID="ID_367536823" MODIFIED="1445333096224" TEXT="Par&#xe1;ms de tr&#xe1;f">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445333108160" ID="ID_417289824" MODIFIED="1445333191938">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      PCR (tasa pico, celdas/sg), SCR (sostenib), MBS (m&#225;x long r&#225;faga), MCR (tasa m&#237;n)
    </p>
  </body>
</html></richcontent>
<node CREATED="1445333191943" ID="ID_740571108" MODIFIED="1445333201893">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Jitter (var de retardo), CDVT (var m&#225;x ret)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1445333096797" ID="ID_1781818305" MODIFIED="1447835983424">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>QoS</u>
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<node CREATED="1445333205101" ID="ID_1533696437" MODIFIED="1447841074526">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CLR (prob cels perdidas), CER (prob cels err&#243;n), CTD (ret m&#225;x tr&#225;f), <u>CDV</u>&#160; (var de ret pico a pico)
    </p>
  </body>
</html></richcontent>
<node CREATED="1445333260222" ID="ID_199702897" MODIFIED="1447841045719">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      SECBR (prob bloq cn errors), CMR (tasa cels mal insert)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1445333098822" ID="ID_1243721862" MODIFIED="1445333105576" TEXT="Tipos de conexs">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1445333302733" ID="ID_249255547" MODIFIED="1447837886234">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CBR (tasa cte), VBR-RT (T var sensib a ret), VBR-NRT (no sens), ABR (T dispo), UBR (Unspecif,best-eff)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1480703227343" ID="ID_550802723" MODIFIED="1480703246703" TEXT="VPLS (VIRTUAL PRIVATE LAN SERVICE)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1480703398734" ID="ID_1218155792" MODIFIED="1480765695218">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Es un est&#225;ndar del IETF para el establecimiento de redes privadas virtuales (VPN) multipunto-multipunto de nivel 2 basadas en Ethernet&#160; sobre infraestructura MPLS (RFC 4761 y 4762)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1480703543203" ID="ID_476688449" MODIFIED="1480703707140" TEXT="Permite la interconexi&#xf3;n de sites remotos a nivel 2, compartiendo un &#xfa;nico dominio de colisi&#xf3;n y extendi&#xe9;ndose la red LAN"/>
<node CREATED="1480703942312" ID="ID_1581309292" MODIFIED="1480703978625" TEXT="VPLS puede ser uno de los mecanismos para proveer alguno de los servicios definidos por Carrier Ethernet"/>
<node CREATED="1480703708468" ID="ID_1594283680" MODIFIED="1480765680531">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      El tr&#225;fico broadcast de VPLS consume mucho ancho de banda, por lo que VPLS es apropiado para interconectar un n&#250;mero peque&#241;o de sites. Para mejorar la escalabilidad de VPLS, se ha definido <b>H-VPLS (Hierarchical VPLS)</b>&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1480697455593" ID="ID_1087353619" MODIFIED="1480697467656" TEXT="CARRIER ETHERNET">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432286492924" ID="ID_1302997403" MODIFIED="1480701895968">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Origen: Metro Ethernet (Red Metropolitana)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432286685385" ID="ID_239884688" MODIFIED="1480697985046">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No es est&#225;ndar, sino alianza industrial (Metro Ethernet Forum)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1432286722079" ID="ID_1128581362" MODIFIED="1432286730497" TEXT="FO y disps de conmutaci&#xf3;n">
<node CREATED="1432286735350" ID="ID_1160213361" MODIFIED="1432292981585">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Jerarquizaci&#243;n de redes</i>: n&#250;cleo/interconexi&#243;n, distrib/agregaci&#243;n&#160;y acceso&#160;
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
</node>
<node CREATED="1432286939541" ID="ID_1522589001" MODIFIED="1432286961727">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      F&#225;cil integraci&#243;n con red cliente (LAN-Ethernet), - coste, eficiente y fiable&#160;
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
<node CREATED="1432286964499" ID="ID_1149330116" MODIFIED="1432286980274" TEXT="Topolog&#xed;a anillo, estrella o mallada"/>
</node>
<node CREATED="1432287017541" ID="ID_1262652883" MODIFIED="1432287019145" TEXT="Pura">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432287020013" ID="ID_204892129" MODIFIED="1432287062386">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Infr solo con disps de nivel 2, VLAN para aislar tr&#225;fico de clientes
    </p>
  </body>
</html></richcontent>
<node CREATED="1432287082684" ID="ID_1824525994" MODIFIED="1432287093399" TEXT="Poco escalable, fiable y estable">
<icon BUILTIN="button_cancel"/>
<node CREATED="1432287102922" ID="ID_1908118295" MODIFIED="1432287107374" TEXT="Experimental, peque&#xf1;a escala"/>
</node>
</node>
</node>
<node CREATED="1432287116762" ID="ID_934277164" MODIFIED="1432287122292" TEXT="Basada en SDH">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432287123034" ID="ID_408130666" MODIFIED="1432287153718" TEXT="Reutiliza infr SDH (alta fiabilidad)">
<node CREATED="1432287134384" ID="ID_1429568570" MODIFIED="1432287183628" TEXT="Alto coste y baja flexib de BW">
<icon BUILTIN="button_cancel"/>
<node CREATED="1432287175755" ID="ID_562898265" MODIFIED="1432287182097" TEXT="Paso intermedio de migraci&#xf3;n a MPLS"/>
</node>
</node>
</node>
<node CREATED="1432287189134" ID="ID_1853081321" MODIFIED="1432287193928" TEXT="Basada en MPLS">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1432287259801" ID="ID_778346528" MODIFIED="1432287428697">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Eth del cliente sobre MPLS sobre Eth del operador
    </p>
  </body>
</html></richcontent>
<node CREATED="1432287560227" ID="ID_308312400" MODIFIED="1432287793753">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Servs</i>: E-Line (VLL Virtual Leased Line encapsula Eth en MPLS) y VPLS (VPN multipunto)&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1432287454177" ID="ID_444811826" MODIFIED="1432287526554">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Escalabilidad (&gt;4096 VLAN), adaptabilidad (prot a fallos), multiprot, op y mant extremo-extremo
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1480697954187" ID="ID_1007601019" MODIFIED="1480765890578">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Carrier Ethernet</b>: Evoluci&#243;n de Metro Ethernet para estandarizar los servicios basados en Ethernet a nivel global y sus niveles de calidad de servicio, facilitando la certificaci&#243;n, el despliegue e integraci&#243;n de redes entre operadores.
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1480701906421" ID="ID_1496701673" MODIFIED="1480702213796" TEXT="En una red Carrier Ethernet, los datos se transportan a trav&#xe9;s de las conexiones virtuales Ethernet Punto-a-Punto y Multipunto-a-Multipunto (EVC), defini&#xe9;ndose los siguientes servicios:">
<node CREATED="1480702167906" ID="ID_712266179" MODIFIED="1480702298078">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      E-LINE: conexion Ethernet punto-a-punto
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1480702186593" ID="ID_1325892635" MODIFIED="1480702321156">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      E-LAN: conexi&#243;n Ethernet multipunto-a-multipunto
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1480702198468" ID="ID_1373924476" MODIFIED="1480702356390" TEXT="E-TREE: conexi&#xf3;n Ethernet punto-a-multipunto"/>
<node CREATED="1480702202093" ID="ID_360712389" MODIFIED="1480702418359">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      E-ACCESS: conexi&#243;n definida para la interconexi&#243;n de Proveedores de Servicio
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
</map>
