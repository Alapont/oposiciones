<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#338800" CREATED="1429260069182" ID="ID_994529049" MODIFIED="1516899857578" TEXT="T83 PLANIF ESTRAT">
<node CREATED="1429260512143" ID="ID_1203807879" MODIFIED="1429260519112" POSITION="right" TEXT="Introducci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429260519504" ID="ID_1621555709" MODIFIED="1429269131717" TEXT="SI">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429260522425" ID="ID_1867205749" MODIFIED="1429260580843" TEXT="Proceds (man-aut) para operar sobre datos y obtener info que apoye la toma de decisiones">
<node CREATED="1429260551263" ID="ID_98171904" MODIFIED="1429262673669" TEXT="Depende de objs de la org y est&#xe1; relacionado con resto de s&#xaa;">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1429260598142" ID="ID_1734988182" MODIFIED="1429269131195" TEXT="Planificaci&#xf3;n">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429260600774" ID="ID_101876776" MODIFIED="1429260614532" TEXT="Proceso de determinar objs y definir la mejor manera de alcanzarlos">
<node CREATED="1429260614533" ID="ID_1120342923" MODIFIED="1429261066811">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Criterios (Druker):</i>&#160;&#160;efiacia (metas correctas) y eficiencia (medios correctos)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429261103181" ID="ID_1405237003" MODIFIED="1429261104897" TEXT="Tipos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429261106286" ID="ID_1525365357" MODIFIED="1429261163589">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Estrat&#233;gica:</i>&#160;par&#225;metros b&#225;sicos para conseguir objs
    </p>
  </body>
</html></richcontent>
<node CREATED="1429261138199" ID="ID_339161994" MODIFIED="1429261283840">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Altos Directivos, largo plazo, muchas activs, complejo
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1429261224582" ID="ID_145694398" MODIFIED="1429261272954" TEXT="Requiere concocer: prop&#xf3;sitos socioeco, valores de Altos Dir, DAFO"/>
</node>
</node>
<node CREATED="1429261164964" ID="ID_10040862" MODIFIED="1429261213845">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Operativa:</i>&#160;recursos para alcanzar formulaciones del plan estrat&#233;gico
    </p>
  </body>
</html></richcontent>
<node CREATED="1429261184215" ID="ID_1230049128" MODIFIED="1429268976739" TEXT="Jefes de -rango, corto plazo, pocas activs, no tan complejo">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
<node CREATED="1429260659266" ID="ID_326793937" MODIFIED="1429269129900" TEXT="Estrategia">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429260662292" ID="ID_722032504" MODIFIED="1429260712762" TEXT="Reglas para tomar decisiones (Alta Direcci&#xf3;n)">
<node CREATED="1429260686748" ID="ID_388304830" MODIFIED="1429260714393" TEXT="Prespectivas">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429260689563" ID="ID_580991872" MODIFIED="1429260871910">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Tradicional: </i>(lo que pretende)<i>&#160;</i><u>pgm</u>&#160;para def y alcanzar objs a largo plazo (planif racional)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429260789041" ID="ID_1492936869" MODIFIED="1429260882619">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Moderna:</i>&#160;(lo que hace al final) patr&#243;n de <u>respuestas</u>&#160;de la org a su ambiente en el tiempo
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1429260894768" ID="ID_1397722024" MODIFIED="1429261040314" TEXT="Tipos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429260897938" ID="ID_1661850392" MODIFIED="1429269001469">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Integraci&#243;n:&#160; </i>control sobre distribuidores (hacia adelante), proveedores (atr&#225;s) o competencia (horiz)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429260938944" ID="ID_1652590189" MODIFIED="1429261054012">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Intensivas:</i>&#160;&#160;penetraci&#243;n en mercado, aumentar ventas
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429260951632" ID="ID_1793804984" MODIFIED="1429261037740">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Diversificaci&#243;n:</i>&#160;&#160;abarcar negocios diversos
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429260966344" ID="ID_335462632" MODIFIED="1429261022977" TEXT="Defensivas">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429260971169" ID="ID_1976012750" MODIFIED="1429269092032" TEXT="Riesgos compartidos (UTE), Encogimiento (-costes), Desinversi&#xf3;n (vender parte), Liquid (vender activos)"/>
</node>
</node>
</node>
</node>
<node CREATED="1429261931355" ID="ID_330811865" MODIFIED="1429261937220" POSITION="right" TEXT="Planificaci&#xf3;n estrat&#xe9;gica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429268011979" ID="ID_798310130" MODIFIED="1429269128813" TEXT="Etapas">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429261944499" ID="ID_1361646857" MODIFIED="1429262079926" TEXT="1. Formulaci&#xf3;n de estrategia">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429261953835" ID="ID_731314137" MODIFIED="1429266517183">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Misi&#243;n&#8594; Sit futura ( <u>Objs</u>)&#8594; Sit actual (DAFO)&#8594; Toma decisiones (alternativas estrat&#8594;eval&#8594; <u>selecci&#243;n</u>)&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1429262254936" ID="ID_68861837" MODIFIED="1429262261186" TEXT="Planif Estrat&#xe9;gica">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1429262168395" ID="ID_1353916121" MODIFIED="1429262172677" TEXT="2. Implementaci&#xf3;n">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429262173172" ID="ID_443089822" MODIFIED="1429269113116" TEXT="Estruct org&#x2192; S&#xaa; Control&#x2192; Adecuaci&#xf3;n estrat-estruct-control&#x2192; Manejo conflicto  ">
<node CREATED="1429262262111" ID="ID_824151275" MODIFIED="1429262488060">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Planif Operativa:</i>&#160;Objs operativos (<u>proys</u>) &#8594; L&#237;neas de acci&#243;n
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1429262278008" ID="ID_494902451" MODIFIED="1429262291451" TEXT="3. Evaluaci&#xf3;n">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429262291864" ID="ID_1082687854" MODIFIED="1429262375195" TEXT="Revisi&#xf3;n factores int-ext&#x2192; Medici&#xf3;n desempe&#xf1;o (monitoriz)&#x2192; Acciones correctivas ">
<node CREATED="1429262262111" ID="ID_1384494627" MODIFIED="1429262265722" TEXT="Planif Operativa">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1429268020923" ID="ID_195876555" MODIFIED="1429268034027" TEXT="T&#xe9;cnicas">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429268034483" ID="ID_538314596" MODIFIED="1429269174849">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tener en cuenta a la hora de&#160;<u>determinar objetivos</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1429268051647" ID="ID_1510118003" MODIFIED="1429269157100" TEXT="Factores int-ext de la org (DAFO) y factores cr&#xed;ticos para alcanzar objs (FCE)"/>
</node>
<node CREATED="1429268099080" ID="ID_1298840137" MODIFIED="1429268107441" TEXT="DAFO">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429268104696" ID="ID_736993961" MODIFIED="1429268695899">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Conocer <u>situaci&#243;n actual</u>&#160;&#160;de la org y del mercado
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1429268149786" ID="ID_1270544729" MODIFIED="1429268238107">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>An&#225;lisis Int:&#160; </i>seg&#250;n recursos y capacidades
    </p>
  </body>
</html></richcontent>
<node CREATED="1429268172397" ID="ID_1088467671" MODIFIED="1429268212633">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Debilidades:</i>&#160;limitan efectividad, deben ser controladas y superadas
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429268185949" ID="ID_1764258464" MODIFIED="1429268227998">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Fortalezas: </i>ventajas competitivas para explotar oportunidades
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429268242454" ID="ID_1058236599" MODIFIED="1429268256374">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>An&#225;lisis ext:</i>&#160;fuera de control
    </p>
  </body>
</html></richcontent>
<node CREATED="1429268257581" ID="ID_418436828" MODIFIED="1429268289723">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Amenazas:</i>&#160;puede impedir implantaci&#243;n, reducir efectiv, incrementar riesgos
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429268304025" ID="ID_1818251292" MODIFIED="1429268320191">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Oportunidades:</i>&#160;posibilidad para mejorar la rentabilidad
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429268326201" ID="ID_1442509189" MODIFIED="1429268380894">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ambientes relacionados:</i>&#160;inmediato (industria), nacional y macro (internac)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429268415388" ID="ID_1621611732" MODIFIED="1429268490646">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Situaciones de la org:</i>&#160;seg&#250;n los factores predominantes&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1429268432269" ID="ID_1505046169" MODIFIED="1429268485286">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      INT/EXT &#8594; MAXI-MINI/MAXI-MINI
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1429268507043" ID="ID_7471615" MODIFIED="1429268678226" TEXT="FCE">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429268510723" ID="ID_552998561" MODIFIED="1429268549232" TEXT="Factores Cr&#xed;ticos de &#xc9;xito&#x2192; mayor prioridad y especial atenci&#xf3;n ">
<node CREATED="1429268585635" ID="ID_859739001" MODIFIED="1429268891733">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lista objs&#8594; Id FEs (&gt;=1 por obj, <u>medios</u>&#160;para conseguirlos)&#8594; Id componentes&#8594; Seleccionar FCEs
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429268743238" ID="ID_167187452" MODIFIED="1429268917001" STYLE="fork" TEXT="Son esenciales para lograr objs y requieren recursos especiales">
<node CREATED="1429268813973" ID="ID_1559797186" MODIFIED="1429268906964" TEXT="Suelen haber entre 5 y 7">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1429262710036" ID="ID_10672500" MODIFIED="1429269741410" POSITION="right" TEXT="RD 806/2014">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429262858003" ID="ID_445197267" MODIFIED="1429262977404">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modelo com&#250;n de gobernanza TIC ( <u>estrategia global</u>&#160;) en&#160; <u>AGE y sus org p&#250;b</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1429262883747" ID="ID_636879190" MODIFIED="1429269212680">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Org P&#250;b:</i>&#160;OOAA, Agencias Estatales y Entidades P&#250;b Emp (AENA, RENFE Adif...)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429262717996" ID="ID_636424555" MODIFIED="1429268280801" TEXT="Deroga RD 589/2005: CSAE (estrategia TIC) y CMAEs (planes ministeriales)">
<icon BUILTIN="stop-sign"/>
<node CREATED="1429263589263" ID="ID_1676887674" MODIFIED="1429263609651" TEXT="eAdm&#x2192; Adm Dig"/>
</node>
</node>
<node CREATED="1434107551049" ID="ID_1322981514" MODIFIED="1434107558045" TEXT="Direcci&#xf3;n TIC (DTIC)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1434108265414" ID="ID_1857873399" MODIFIED="1434108740003">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Subsecretar&#237;a</u>&#160;(dep de MinHAP y MinPresi), &#243;rg de&#160;<b>racionalizaci&#243;n</b>&#160;TIC en la AGE
    </p>
  </body>
</html></richcontent>
<node CREATED="1434108315650" ID="ID_28376532" MODIFIED="1434108765318">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Coop entre AP y coord internac, innov, consolid, reutiliz, Cat&#225;logo de Servs Comunes, pol&#237;ticas
    </p>
    <p>
      de adquis, informes de PPT, seguim gasto, est&#225;ndares, pgms de inversi&#243;n, propuestas con CNI
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1429262940330" ID="ID_362079574" MODIFIED="1429264094790" TEXT="Comisi&#xf3;n Estrat (CETIC)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429263026320" ID="ID_1419236943" MODIFIED="1434116225533">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (dep de MinHAP) Def y supervisa <b>Estrategia TIC</b>: objs y acciones para transform dig de AGE
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1429264250980" ID="ID_380798297" MODIFIED="1434115730492" TEXT="Aprobada por Gobierno (Consejo Ministros) a propuesta de Mins Presi-Hac-Inetur">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1429263413333" ID="ID_1765942560" MODIFIED="1429263415739" TEXT="Funciones">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429263196748" ID="ID_581563940" MODIFIED="1434116040916">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      L&#237;neas estrat <u>seg&#250;n pol&#237;tica&#160;Gob</u>, elev a Mins, informar de normas, prioridads de inv
    </p>
  </body>
</html></richcontent>
<node CREATED="1429264592445" ID="ID_362527022" MODIFIED="1434116013229">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;Coop con CCAA-EELL (racionaliz y servs interadm integrados), UE e Iberoam
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1429264345407" ID="ID_886011422" MODIFIED="1429264370822" TEXT="Medios y servs compartidos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429264350695" ID="ID_1663976058" MODIFIED="1429264449001">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Por necesidades transversales de varias unidades adm,<u>obligatorio</u>&#160;y sustitutivo de particulares
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1429264460319" ID="ID_630676752" MODIFIED="1429269250569" TEXT="Declarados por CETIC (a propuesta de DTIC)">
<node CREATED="1429264477134" ID="ID_275245732" MODIFIED="1429264513108" TEXT="DTIC: elabora Cat&#xe1;logo de servs comunes y Registro de costes"/>
</node>
</node>
<node CREATED="1429264524300" ID="ID_1717204903" MODIFIED="1429264621501" TEXT="Proyectos de inter&#xe9;s prioritario">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429264622208" ID="ID_1312051880" MODIFIED="1429264722506" TEXT="Declarados por CETIC y trasladados a MinHacienda y Comisi&#xf3;n de Pol&#xed;ts Gasto para los PGE"/>
</node>
</node>
<node CREATED="1429263293975" ID="ID_1130841696" MODIFIED="1429263381988" TEXT="Pleno">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429263301024" ID="ID_1384012572" MODIFIED="1434116129947">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Presi (MHacienda), Secre (DTIC/CIO TIC), Secres de
    </p>
    <p>
      Estado (AAPP, TDI, SS) y Subsecres de Ministerios
    </p>
  </body>
</html></richcontent>
<node CREATED="1429263474559" ID="ID_1507994096" MODIFIED="1429263478328" TEXT="Comit&#xe9; Ejec">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429263478647" ID="ID_754250877" MODIFIED="1434116111879">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Presi (DTIC) y Titulares (DGRCC, DGPresus, DGTeleco, Inform SS, Inform
    </p>
    <p>
      &#160;Ag Trib, SGJusticia, DGFunc P&#250;b, Inspec Hac, IGAE, SubDG CNI/CCN)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1429263563496" ID="ID_697694727" MODIFIED="1429264095198" TEXT="Comisiones Minist de Adm Dig (CMADs) ">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429263623824" ID="ID_1193098575" MODIFIED="1429269419699">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Elaboran los <b>Planes de Acci&#243;n</b>&#160;de AD
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1429264824964" ID="ID_1024926812" MODIFIED="1429264832955">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Uno por Ministerio, seg&#250;n l&#237;neas estrat CETIC y directrices DTIC
    </p>
  </body>
</html></richcontent>
<node CREATED="1429264836463" ID="ID_1821620151" MODIFIED="1429269711002">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Servicios previstos, planific, rrh-t&#233;cn-fin y contratos&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429264902858" ID="ID_1449532512" MODIFIED="1434116069926">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CMAD&#8594; DTIC (valoraci&#243;n)&#8594; CETIC (informe del Comit&#233;)&#8594; Ministerio (aprobaci&#243;n por CMin)&#160;&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1429264861137" ID="ID_1694397205" MODIFIED="1429264870244" TEXT="Alcance &gt;=2 a&#xf1;os">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node CREATED="1429269419685" ID="ID_1265223094" MODIFIED="1429269425178" TEXT="Escalar">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429266286140" ID="ID_1582530629" MODIFIED="1429269391071" TEXT="1 Plan Acci&#xf3;n= n Planes Director (Secre Estado) / 1 Plan Director= n Planes SI (Dir General)"/>
</node>
</node>
<node CREATED="1429263702683" ID="ID_1400174874" MODIFIED="1429263705100" TEXT="Funciones">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429263705411" ID="ID_1731516943" MODIFIED="1429263755376" TEXT="Coord int sobre AD, enlace entre Minist y DTIC, cumplim directrices Estrategia, id oportunidades"/>
</node>
</node>
<node CREATED="1429263830223" ID="ID_1555021459" MODIFIED="1429264095630" TEXT="Unidades TIC">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429263911229" ID="ID_1130333367" MODIFIED="1429269477771">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Provision de <b>servicios</b>&#160;<b>TIC</b>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1429263936232" ID="ID_1615753630" MODIFIED="1429263975786" TEXT="Gesti&#xf3;n e implement, apps, consultoria, seguridad, att, innov, adquis bienes y servs TIC">
<node CREATED="1429269465664" ID="ID_158809770" MODIFIED="1429269471746" TEXT="Coordinadas por CMADs">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
<node CREATED="1429263822256" ID="ID_127857555" MODIFIED="1429264095966" TEXT="Comit&#xe9; de DTIC">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429263988836" ID="ID_1854965628" MODIFIED="1429264233638">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Asesor&#237;a y <b>apoyo&#160;a DTIC</b>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1429263997251" ID="ID_597340876" MODIFIED="1429269485587" TEXT="Coord y colab entre DTIC y AGE, contribuci&#xf3;n con buenas pr&#xe1;cticas comunes a todas las unidades TIC"/>
</node>
</node>
</node>
<node CREATED="1429265168486" ID="ID_468876093" MODIFIED="1429267995151" POSITION="right" TEXT="Plan de Sistemas (PSI)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429266617791" ID="ID_1925980035" MODIFIED="1429267234372" TEXT="Marco de ref para desarrollo de SI que responda a objs de la org">
<icon BUILTIN="info"/>
<node CREATED="1429266634103" ID="ID_910206793" MODIFIED="1429266804825" TEXT="Contenido">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429266680719" ID="ID_1264595280" MODIFIED="1429266686912" TEXT="Situaci&#xf3;n actual, modelos de arq info, proyectos a desarrollar, eval de recursos y plan de seguimiento"/>
</node>
</node>
<node CREATED="1429266694986" ID="ID_153282309" MODIFIED="1429269515438" TEXT="M&#xe9;trica v3 (activs y tareas)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1429266757969" ID="ID_607928998" MODIFIED="1429266872972">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>PSI1-Inicio del Plan SI:</i>&#160;determinar PSI, alcance y responsables (apoyo de Direcci&#243;n)
    </p>
  </body>
</html></richcontent>
<node CREATED="1429266853809" ID="ID_1237602887" MODIFIED="1429266868020" TEXT="Descrip general: objs estrat, &#xe1;mbito, FCEs y responsables"/>
</node>
<node CREATED="1429266875600" ID="ID_343932735" MODIFIED="1429266961906">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>PSI2-Def y org del PSI: </i>detallar &#225;mbito y alcance, org (equipo, funcs) y Plan Trabajo (calendar)
    </p>
  </body>
</html></richcontent>
<node CREATED="1429266918616" ID="ID_1577607526" MODIFIED="1429266955283">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>PSI3-Estudio de info relevante:</i>&#160;an&#225;lisis de antecedentes que puedan afectar
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429266956406" ID="ID_1051265880" MODIFIED="1429267018730">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>PSI4-Id Reqs:</i>&#160;procesos, necesidades de info (modelado) y Plan Trabajo (reqs=objs)
    </p>
  </body>
</html></richcontent>
<node CREATED="1429266995813" ID="ID_1783697313" MODIFIED="1429267010917" TEXT="Cat&#xe1;logo de reqs (objs) y modelado de info"/>
</node>
<node CREATED="1429267012097" ID="ID_951547338" MODIFIED="1429267047895">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>PSI5-Estudio de SI actuales:</i>&#160;alcance y objs de estudio, an&#225;lisis de SI actuales
    </p>
  </body>
</html></richcontent>
<node CREATED="1429267049241" ID="ID_538679266" MODIFIED="1429267078706">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>PSI6-Dise&#241;o del modelo SI:</i>&#160;situaci&#243;n&#160;actual, def modelo SI (diagrama)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429267092248" ID="ID_980275881" MODIFIED="1429267123653">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>PSI7-Def de arq tecn:</i>&#160;id necesidades de infraestr tecn y selecci&#243;n de arq tecn
    </p>
  </body>
</html></richcontent>
<node CREATED="1429267124312" ID="ID_1551413261" MODIFIED="1429267158619">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>PSI8-Def Plan de Acci&#243;n:</i>&#160;elab de Plan de Proyectos y Plan de Mantenimiento
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1429267159341" ID="ID_141814418" MODIFIED="1429267207982">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>PSI9-Revisi&#243;n y aprobaci&#243;n del PSI: </i>convocatoria de presentaci&#243;n y eval de propuesta, y aprob
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</map>
