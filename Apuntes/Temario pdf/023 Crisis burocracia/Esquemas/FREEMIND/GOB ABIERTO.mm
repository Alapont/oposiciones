<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1461688588831" ID="ID_1637547758" MODIFIED="1462449629603">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T17B GOB
    </p>
    <p style="text-align: center">
      ABIERTO
    </p>
    <p style="text-align: center">
      Y LAS TIC
    </p>
  </body>
</html></richcontent>
<node CREATED="1461755768613" ID="ID_133562848" MODIFIED="1461916935380" POSITION="right" TEXT="Intro">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461746318524" ID="ID_1291264717" MODIFIED="1461917030102">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Apertura</u>&#160;de asuntos p&#250;b a&#160;<u>ciuds</u>&#8594; transpar (rendic cuentas), particip (mejora pol&#237;ts) y colab (+cal)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1461741220756" ID="ID_1148117674" MODIFIED="1461917049198">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Deriva de gobernanza para mejorar servs y<u>recup confianza</u>&#160;(tras crisis)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1461753247950" ID="ID_682994017" MODIFIED="1461916902115">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>TIC:</i>&#160;elem <u>habilitador</u>&#8594; insufic por s&#237; mismas: req cambios cult y org
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1462519533097" ID="ID_1609416130" MODIFIED="1462519555563">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Permiten al ciud ser <u>espectador y actor</u>&#160;de los fen&#243;menos soc
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1461916935366" ID="ID_1273037312" MODIFIED="1461916936819" TEXT="Evol">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461743735964" ID="ID_429290296" MODIFIED="1461916959974">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2009:</i>&#160;Memorando GA de EEUU (Obama)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1461743991042" ID="ID_1691489272" MODIFIED="1461917088180">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      2011: Alianza de Gob Ab
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
<node CREATED="1461743996765" ID="ID_1369986067" MODIFIED="1462520374038">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (hoy 64 pa&#237;ses) plataf internac de <u>soporte</u>&#160;a pa&#237;ses comprometidos con la transpar
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1461744175175" ID="ID_817191732" MODIFIED="1462520645948">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Plan Acci&#243;n ESP 2012-16 (CORA)
    </p>
  </body>
</html>
</richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461744686212" ID="ID_826000886" MODIFIED="1462520685439">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Portales</u>&#160;(Transpar, Archivos Esp-PARES, Adm Just), RISP (<u>18/2015</u>), Datos S&#170;Nac Salud, ctrl subvs
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1461748111991" ID="ID_737390997" MODIFIED="1461755809912" TEXT="Engloba">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461747726065" ID="ID_1784065753" MODIFIED="1461917143310">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>eAdm:</i>&#160;antesala, TIC en procesos adm para +<u>productiv y efici</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1461748098489" ID="ID_1483560005" MODIFIED="1462520417381">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Open Data: </i>datos p&#250;b accesibles y <u>reut</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1461747801928" ID="ID_1584898272" MODIFIED="1461917158725">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>eDemocracia:</i>&#160;procs elect, <u>votaci&#243;n</u>&#160;y particip
    </p>
  </body>
</html></richcontent>
<node CREATED="1461747750781" ID="ID_1539518022" MODIFIED="1461917175365">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Pol&#237;tica 2.0: </i>web 2.0 y RRSS como <u>bar&#243;metro</u>&#160;soc y <u>marketing</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1461746384366" ID="ID_1336288172" MODIFIED="1461755194576" POSITION="right" TEXT="Pilares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1461748840437" ID="ID_71268582" MODIFIED="1461755472044">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Transpar
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461748860320" ID="ID_1987555568" MODIFIED="1461917238127">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Cal democr</u>&#160;req una ciud bien informada&#8594; gesti&#243;n&#160;fondos, prior y ejec proys, crits actuac
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1461755472032" ID="ID_1906494806" MODIFIED="1461917247133">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Normas
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461741551432" ID="ID_173419629" MODIFIED="1462520603302">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>19/2013: Portal de Transp</i>&#8594; info instit, jur y eco disponible
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1461741457701" ID="ID_23308538" MODIFIED="1461757774569">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>37/2007 y 18/2015-RISP:</i>&#160;datos.gob.es
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1462520735593" ID="ID_1339821000" MODIFIED="1462520759973">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ley&#160;3/2015:</i>&#160;transpar de altos cargos AGE
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1462520713629" ID="ID_1495751948" MODIFIED="1462520735017">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ley 40/2015:</i>&#160;ppio transpar
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1453110237914" ID="ID_364330446" MODIFIED="1461917279670">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Estrat TIC:</i>&#160;ppio transp y obj gesti&#243;n corp intelig de info
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1453110248413" ID="ID_1412553029" MODIFIED="1453113215766">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Info almac, proteg y&#160; <u>dispo</u>&#160;para ciud/empr (bajo Ley Transp y LOPD)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1461753462972" ID="ID_1013254008" MODIFIED="1461755864241">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      ETL (ICR, DW)&#8594; almac masivo (Big Data)&#8594; convers en conoc (BI-DM, web sem-RDF)&#8594; accesib (multich)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="kaddressbook"/>
<node CREATED="1461754822572" ID="ID_1883319863" MODIFIED="1461917336102">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      pe Canal GA (MinHAP)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1461750790057" ID="ID_172981353" MODIFIED="1461750792793" TEXT="Particip">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461750847722" ID="ID_999714382" MODIFIED="1461917386665">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Bidir</u>&#8594;&#160;Ciuds particip en <u>decisiones</u>; AP <u>contrasta</u>&#160;decis&#160;con ciuds
    </p>
  </body>
</html></richcontent>
<node CREATED="1461741956062" ID="ID_1965061926" MODIFIED="1461742105277">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Crisis Bienestar, establec Democracia y CCAA&#8594; req&#160; <u>nuevas v&#237;as</u>&#160;de particip soc&#8594; TIC
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1461741686110" ID="ID_622641209" MODIFIED="1461917392892" TEXT="Normas">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461741689285" ID="ID_829689429" MODIFIED="1461755335308">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Constit
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461741697688" ID="ID_417195004" MODIFIED="1461741916599">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>art 1. </i>Estado Soc y <u>Democr&#225;tico</u>&#8594; soberan&#237;a en el pueblo
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1461741737932" ID="ID_1981026841" MODIFIED="1461741764103">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>art 9. </i>Poderes p&#250;b deben fomentar la<u>particip</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1461741784652" ID="ID_198139556" MODIFIED="1461917581938">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Directa: </i>dcho a petic, inic <u>leg</u>, <u>refer</u>, audiencia, <u>jurado</u>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1461741858630" ID="ID_1208206255" MODIFIED="1461917429687">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Indir:</i>&#160;dchos de particip y pol&#237;ts, <u>CG</u>, Gob, partidos sindics y otras <u>asoc</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1461742203865" ID="ID_99746350" MODIFIED="1461917446043">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      LRJPAC (medios-e), LOFAGE (descentr=particip ciud), 59/2003 (firma), 951/2005 (q&amp;s),
    </p>
    <p>
      LAECSP (sedes, 060), 56/2007 y Avanza (+partic-e), 18/2011 (TIC, particip en just)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1461750967051" ID="ID_1347186382" MODIFIED="1462520558766">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Escucha activa y en RT (RRSS, blogs, <u>portales</u>)&#8594; <u>intelig colectiva </u>; CRM (CAU), id-e
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="kaddressbook"/>
</node>
</node>
<node CREATED="1461751065994" ID="ID_1290130765" MODIFIED="1461751068382" TEXT="Colab">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461751091675" ID="ID_1546741011" MODIFIED="1461753749969">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Coop ciuds-AP, entre AP o con sector priv para&#160; <u>ejec</u>&#160;de planes&#8594; <u>corresponsab</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1461751308368" ID="ID_577699555" MODIFIED="1461755577700">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Indispensable involucr&#160; <u>Dir</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1461753709607" ID="ID_581889529" MODIFIED="1461917546003">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Streaming de eventos, voto-e (Scytl), crowdsource (extern ab), crowdfund (financ)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="kaddressbook"/>
<node CREATED="1461755577697" ID="ID_309130147" MODIFIED="1461917565775">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      pe dondevanmisimpuestos.es,&#160;DataUSA.io (MIT, 2016)&#8594; correspons ciuds
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1461746385016" ID="ID_8960343" MODIFIED="1461917834929" POSITION="right" TEXT="Retos">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="prepare"/>
<node CREATED="1461751728691" ID="ID_1826471862" MODIFIED="1461917616090">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Pol&#237;ticos: </i><u>liderazgo</u>&#160;q apueste por GA como <u>palanca</u>&#160;para cambiar pol&#237;tica
    </p>
  </body>
</html></richcontent>
<node CREATED="1461917618413" ID="ID_127877169" MODIFIED="1461917683928">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Enc Glob Corrupc-E&amp;Y'15:</i>&#160;ESP <u>22&#186;</u>&#160;(peor pese a det 79M&#8364; de fraude)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1461752212057" ID="ID_460748541" MODIFIED="1461917757498">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Adm:</i>&#160;rev proced&#160;para implatar Ley Transp sin sobrecarga trabajo&#8594;<u>interop</u>&#160;y coord; sil pos; +v&#237;as solic
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1461752449016" ID="ID_220245231" MODIFIED="1461917799917">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Soc: -</i>desconfi TIC y brecha dig&#8594; 48% uso eAdm (AEVAL'15)
    </p>
  </body>
</html></richcontent>
<node CREATED="1461917799921" ID="ID_1733039812" MODIFIED="1461917822589">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Solo&#160;<u>22%</u>&#160;conoce Portal Transp&#8594; req evol y +prox al ciud
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1461752676595" ID="ID_847346332" MODIFIED="1461752785139">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Empr:</i>&#160;apoyar innov e <u>infomediarias</u>&#8594; Ley Emprendedores (14/2013)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1461751904260" ID="ID_1687071880" MODIFIED="1461917906122">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Normat: </i>dcho acceso a info=<u>fund</u>, -l&#237;mites, +info, cumplim sancs, enriq il&#237;cito de funcs en c&#243;d&#160;&#160; <u>penal</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</map>
