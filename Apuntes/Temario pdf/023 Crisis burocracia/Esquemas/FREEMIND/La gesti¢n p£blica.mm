<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1386926917876" ID="ID_823245153" MODIFIED="1393526941838" TEXT="Tema 19. La gesti&#xf3;n p&#xfa;blica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386926993720" ID="ID_189111979" MODIFIED="1518434212760" POSITION="right" TEXT="La crisis de la burocracia">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386950614033" ID="ID_889476222" MODIFIED="1393435991413" TEXT="Origen de la burocracia (Max Weber)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386950756909" ID="ID_1481540267" MODIFIED="1386952286550" TEXT="Desarrollo de las econom&#xed;as monetarias">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Especialmente el Estado y los partidos de masas
    </p>
    <p>
      
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386950796370" ID="ID_725801019" MODIFIED="1386950812455" TEXT="El crecimiento y la expansi&#xf3;n de las tareas administrativas"/>
<node CREATED="1386950862959" ID="ID_239632652" MODIFIED="1386952318536" TEXT="Superioridad t&#xe9;cnica del tipo burocr&#xe1;tico de administraci&#xf3;n"/>
</node>
<node CREATED="1386950881296" ID="ID_1525928672" MODIFIED="1393435999137" TEXT="Modelo de organizaci&#xf3;n burocr&#xe1;tica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386950918510" ID="ID_437558617" MODIFIED="1386950923816" TEXT="Principios b&#xe1;sicos">
<node CREATED="1386951158978" ID="ID_486837164" MODIFIED="1386951251129" TEXT="Estructuras jerarquizadas">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Enlaza con el principio de jerarqu&#xed;a de nuestra administraci&#xf3;n
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386951176910" ID="ID_749828811" MODIFIED="1386951292516" TEXT="Control pol&#xed;tico">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se separa el gobierno de los &#xf3;rganos administrativos, lo que tambi&#xe9;n se refleja en nuestra Administraci&#xf3;n.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1386951205700" ID="ID_416531956" MODIFIED="1386951974636" TEXT="Caracter&#xed;sticas (Weber)">
<node CREATED="1386951304037" ID="ID_562412049" MODIFIED="1386951594111" TEXT="a) Pleno sometimiento al ordenamiento jur&#xed;dico (103 CE)">
<icon BUILTIN="list"/>
</node>
<node CREATED="1386951385430" ID="ID_1197119924" MODIFIED="1386951407288" TEXT="b) Formalizaci&#xf3;n de las comunicaciones"/>
<node CREATED="1386951407933" ID="ID_942667801" MODIFIED="1386951418528" TEXT="c) Racionalidad de la divisi&#xf3;n del trabajo"/>
<node CREATED="1386951420671" ID="ID_588548548" MODIFIED="1386951434713" TEXT="d) La impersonalidad de las relaciones"/>
<node CREATED="1386951458728" ID="ID_1237222823" MODIFIED="1386951599805" TEXT="e) Jerarqu&#xed;a de autoridad">
<icon BUILTIN="list"/>
</node>
<node CREATED="1386952087418" ID="ID_483622035" MODIFIED="1390414497573" TEXT="f) Rutinas y procedimientos de trabajo estandarizados"/>
<node CREATED="1386951473166" ID="ID_129014112" MODIFIED="1386951603049" TEXT="g) Competencia t&#xe9;cnica y meritocracia">
<icon BUILTIN="list"/>
</node>
<node CREATED="1386951489289" ID="ID_384891188" MODIFIED="1386951517091" TEXT="f) Especializaci&#xf3;n de la Administraci&#xf3;n"/>
<node CREATED="1386951520186" ID="ID_1031978546" MODIFIED="1386951606513" TEXT="i) Profesionalizaci&#xf3;n de los funcionarios">
<icon BUILTIN="list"/>
<node CREATED="1386951543765" ID="ID_1195676298" MODIFIED="1386951654569" TEXT="Sirve exclusivamente a los intereses generales">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Es independiente frente a presiones pol&#xed;ticas
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386951616362" ID="ID_376607677" MODIFIED="1390414522320" TEXT="El trabajo debe ser su principal fuente de ingresos"/>
</node>
<node CREATED="1386951656150" ID="ID_322886853" MODIFIED="1386951679053" TEXT="j) Funcionamiento previsible"/>
</node>
</node>
<node CREATED="1386952114273" ID="ID_610258613" MODIFIED="1393436003542" TEXT="Crisis del modelo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386952995172" ID="ID_1268088720" MODIFIED="1386953005257" TEXT="Factores que influyeron">
<node CREATED="1386952349920" ID="ID_183512621" MODIFIED="1386952371470" TEXT="Crisis fiscal del Estado de los a&#xf1;os 70">
<node CREATED="1386952374281" ID="ID_793616207" MODIFIED="1386952381866" TEXT="Crisis del petr&#xf3;leo"/>
<node CREATED="1386952383554" ID="ID_996318031" MODIFIED="1386952395857" TEXT="Dificultades econ&#xf3;micas mundiales"/>
</node>
<node CREATED="1386952398876" ID="ID_967513547" MODIFIED="1386952416293" TEXT="La crisis democr&#xe1;tica de la teor&#xed;a de la delegaci&#xf3;n">
<node CREATED="1386952449506" ID="ID_1890938249" MODIFIED="1386952471589" TEXT="Incremento de las atribuciones del poder ejecutivo"/>
<node CREATED="1386952471835" ID="ID_315391766" MODIFIED="1386952867953" TEXT="Incremento de las potestades de la Administraci&#xf3;n"/>
</node>
<node CREATED="1386952875426" ID="ID_240519648" MODIFIED="1386952906040" TEXT="La creciente percepci&#xf3;n c&#xed;vica de la irresponsabilidad de las burocracias"/>
<node CREATED="1386952919997" ID="ID_1986658574" MODIFIED="1386952950610" TEXT="La irrupci&#xf3;n de las nuevas tecnolog&#xed;as y la globalizaci&#xf3;n">
<node CREATED="1386952955556" ID="ID_163980769" MODIFIED="1386952982761" TEXT="Incremento de la complejidad, diversidad y dinamismo de las sociedades"/>
</node>
</node>
<node CREATED="1386952990348" ID="ID_758952951" MODIFIED="1386953046973" TEXT="Paso de la sociedad industrial a la sociedad de la informaci&#xf3;n y el conocimiento"/>
<node CREATED="1386953087293" ID="ID_716815348" MODIFIED="1386953094133" TEXT="Cr&#xed;ticas">
<node CREATED="1386953094135" ID="ID_1099745206" MODIFIED="1386953123440" TEXT="la burocracia limita las libertades, al limitar las posibilidades de elecci&#xf3;n"/>
<node CREATED="1386953123751" ID="ID_620884492" MODIFIED="1386953142060" TEXT="No ofrec&#xed;a los incentivos para mejorar el rendimiento">
<node CREATED="1386953142061" ID="ID_894549527" MODIFIED="1386953150907" TEXT="Eran menos eficientes"/>
</node>
</node>
</node>
</node>
<node CREATED="1386953171019" ID="ID_849816748" MODIFIED="1394743359204" POSITION="right" TEXT="Nueva gesti&#xf3;n p&#xfa;blica (NGP)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386953263245" ID="ID_715567454" MODIFIED="1394552352402" TEXT="Principios">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      "Nueva gesti&#xf3;n del gobierno", de Osborne y Gaebler
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386957046502" ID="ID_558131969" MODIFIED="1386957064993" TEXT="Se basa en la Econom&#xed;a, Eficiencia y Eficacia"/>
<node CREATED="1386953327408" ID="ID_1669972201" MODIFIED="1386953442490" TEXT="Reestructurar">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Eliminar todo lo que no contribuya a aportar un valor al servicio p&#xfa;blico. Supone un enorme adelgazamiento en favor del sector privado.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386953334401" ID="ID_423330527" MODIFIED="1386953465626" TEXT="Reingenier&#xed;a">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reconstruir los nuevos modelos de gesti&#xf3;n administrativa
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386953353809" ID="ID_1196793902" MODIFIED="1394521864242" TEXT="Reinventar">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Crear una nueva cultura organizacional, que ya se ha visto en el tema de funci&#xf3;n gerencial como una de los principios en que debe basarse la funci&#xf3;n p&#xfa;blica
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386953339167" ID="ID_822965848" MODIFIED="1394521935674" TEXT="Realineaci&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Evaluar el nuevo desempe&#xf1;o de los empleados p&#xfa;blicos, motivar, incentivar y legitimizar la actividad de los empleados p&#xfa;blicos
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386953347482" ID="ID_1623422905" MODIFIED="1394522067838" TEXT="Reconceptualizaci&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Promover organizaciones con capacidad de adaptaci&#xf3;n y generar conocimiento. Esto podr&#xed;a enlazar con el tema de gesti&#xf3;n del conocimiento.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1386953604217" ID="ID_1427438780" MODIFIED="1394703932708" TEXT="Introducci&#xf3;n de t&#xe9;cnicas de management privado en el sector p&#xfa;blico">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Es muy parecido a un ep&#xed;grafe del tema 17. Actuaciones concretas de la funci&#xf3;n gerencial en las AAPP.
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386955169778" ID="ID_1908301171" LINK="Tema%2017.%20Funci&#xf3;n%20Gerencial.mm" MODIFIED="1518434221953" TEXT="La nueva gesti&#xf3;n directiva">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Enlaza con el tema 17
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386921111982" ID="ID_1484263947" MODIFIED="1387265604912" TEXT="Direcci&#xf3;n General">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386921119180" ID="ID_1027062726" MODIFIED="1386921127137" TEXT="Planificaci&#xf3;n estrat&#xe9;gica"/>
<node CREATED="1386921127475" ID="ID_736579168" MODIFIED="1386921146863" TEXT="Gesti&#xf3;n de los procesos de cambio organizativo"/>
<node CREATED="1386921148386" ID="ID_459463702" MODIFIED="1386921156256" TEXT="Direcci&#xf3;n por OBJETIVOS"/>
<node CREATED="1386921156650" ID="ID_1388238387" MODIFIED="1386921169495" TEXT="Direcci&#xf3;n por PROYECTOS"/>
</node>
<node CREATED="1386921182464" ID="ID_1604885851" MODIFIED="1387265606472" TEXT="Espec&#xed;ficas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386921220502" ID="ID_69572382" MODIFIED="1386921227959" TEXT="El marketing"/>
<node CREATED="1386921229142" ID="ID_1486512140" MODIFIED="1386921237875" TEXT="La direcci&#xf3;n de operaciones"/>
<node CREATED="1386921238317" ID="ID_1977273439" MODIFIED="1386921250235" TEXT="El dise&#xf1;o organizativo"/>
<node CREATED="1386921250620" ID="ID_692923210" MODIFIED="1386921260634" TEXT="La direcci&#xf3;n eficiente de los RRHH"/>
<node CREATED="1386921261084" ID="ID_226800846" MODIFIED="1386921278617" TEXT="La gesti&#xf3;n financiera"/>
</node>
<node CREATED="1386955249080" ID="ID_947767318" MODIFIED="1390418018638" TEXT="T&#xe9;cnicas de desarrollo de habilidades directivas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386955288006" ID="ID_517115721" MODIFIED="1386955294133" TEXT="Toma de decisiones"/>
<node CREATED="1386955295018" ID="ID_944910587" MODIFIED="1386955310749" TEXT="Negociaci&#xf3;n y gesti&#xf3;n del conflicto"/>
<node CREATED="1386955311345" ID="ID_183624611" MODIFIED="1386955321904" TEXT="Liderazgo y trabajo en equipo"/>
<node CREATED="1386955322188" ID="ID_1853564017" MODIFIED="1386955327903" TEXT="Creatividad"/>
<node CREATED="1386955328611" ID="ID_1788287336" MODIFIED="1386955351355" TEXT="Capacidad de asumir la complejidad, la ambiguedad y la incertidumbre"/>
</node>
</node>
<node CREATED="1386955180187" ID="ID_583080001" MODIFIED="1518434223577" TEXT="El empleo de las TIC en las siguientes &#xe1;reas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386955356934" ID="ID_309150250" MODIFIED="1386955370895" TEXT="Mejora de la informaci&#xf3;n dirigida a los ciudadanos"/>
<node CREATED="1386955384481" ID="ID_1739111296" MODIFIED="1386955401689" TEXT="Ejecuci&#xf3;n de la gesti&#xf3;n interna">
<node CREATED="1386955401691" ID="ID_746484467" MODIFIED="1386955411853" TEXT="Cumplimiento de sus objetivos"/>
<node CREATED="1386955412424" ID="ID_749529793" MODIFIED="1386955417614" TEXT="Mejora de la eficiencia"/>
</node>
<node CREATED="1386955420704" ID="ID_1952414049" MODIFIED="1394523371652" TEXT="Mejora de la gobernabilidad">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entendida como el fomento de la participaci&#xf3;n ciudadana
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1386955508555" ID="ID_646144180" MODIFIED="1518434224666" TEXT="Orientaci&#xf3;n al cliente">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1390415017035" ID="ID_295090844" MODIFIED="1390415049969" TEXT="Se trata de convertir al ciudadano en cliente del servicio p&#xfa;blico"/>
<node CREATED="1394524929278" ID="ID_141458840" MODIFIED="1394524954317" TEXT="El objetivo &#xfa;nico no es el cumpliiento de la norma, sino la satisfacci&#xf3;n del ciudadano"/>
</node>
<node CREATED="1386955526697" ID="ID_1110236970" MODIFIED="1518434225573" TEXT="Dise&#xf1;o organizativo">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386955678928" ID="ID_584186160" MODIFIED="1386955688029" TEXT="Se dividen las funciones en ">
<node CREATED="1386955688031" ID="ID_1803202490" MODIFIED="1386955696312" TEXT="La formulaci&#xf3;n de pol&#xed;ticas"/>
<node CREATED="1386955696790" ID="ID_1974378781" MODIFIED="1386955706518" TEXT="La ejecuci&#xf3;n de las pol&#xed;ticas"/>
</node>
<node CREATED="1386956941756" ID="ID_1928522903" MODIFIED="1386957027728" TEXT="Ejemplo: Agencias Estatales"/>
</node>
<node CREATED="1386955536075" ID="ID_725751337" MODIFIED="1518434226875" TEXT="B&#xfa;squeda de la calidad">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Enlaza con el tema 18
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1390415082164" ID="ID_493257576" MODIFIED="1393501037453" TEXT="SE trata de una calidad definida por los ciudadanos">
<node CREATED="1390415104924" ID="ID_1555511841" MODIFIED="1390415142256" TEXT="Repercute directamente en una mejora de la satisfacci&#xf3;n del usuario con el servicio"/>
</node>
<node CREATED="1386955737040" ID="ID_1273138438" MODIFIED="1386955750236" TEXT="Implantaci&#xf3;n de las cartas de servicio">
<node CREATED="1386958689403" ID="ID_760262751" MODIFIED="1386958696964" TEXT="Establecimiento de est&#xe1;ndares"/>
</node>
<node CREATED="1386956934502" ID="ID_851729928" MODIFIED="1386956939997" TEXT="Mejora continua"/>
</node>
<node CREATED="1386955793461" ID="ID_409222256" MODIFIED="1518434227817" TEXT="Nuevas herramientas de gesti&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386955823361" ID="ID_1917352298" MODIFIED="1386955837476" TEXT="Introducci&#xf3;n del copago">
<node CREATED="1386955837478" ID="ID_441125801" MODIFIED="1386955846158" TEXT="Inducen a una conciencia de coste"/>
</node>
<node CREATED="1386955849566" ID="ID_503318202" MODIFIED="1386955858094" TEXT="Vales, bonos o cheques"/>
<node CREATED="1386955864656" ID="ID_741081225" MODIFIED="1386955881511" TEXT="Evaluaci&#xf3;n del desempe&#xf1;o de los funcionarios"/>
<node CREATED="1386955888039" ID="ID_1547861832" MODIFIED="1386955912714" TEXT="Contrataci&#xf3;n externa como alternativa a la producci&#xf3;n directa"/>
<node CREATED="1386955915920" ID="ID_180226703" MODIFIED="1386955935806" TEXT="Creaci&#xf3;n de mercados internos de competencia p&#xfa;blica"/>
</node>
</node>
<node CREATED="1386956183755" ID="ID_1102076386" MODIFIED="1518434234389" TEXT="Replanteamiento de la Nueva Gesti&#xf3;n P&#xfa;blica">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1390415230029" ID="ID_765751265" MODIFIED="1393504429965" TEXT="Consideraciones previas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1390415434238" ID="ID_1154116696" MODIFIED="1393504368095" TEXT="Para evitar la desviaci&#xf3;n burocr&#xe1;tica ">
<node CREATED="1390415466428" ID="ID_1674140112" MODIFIED="1390415473021" TEXT="SE separa ">
<node CREATED="1390415473022" ID="ID_1752492613" MODIFIED="1390415479517" TEXT="La formulaci&#xf3;n de pol&#xed;ticas"/>
<node CREATED="1390415479892" ID="ID_916343442" MODIFIED="1390415490155" TEXT="La ejecuci&#xf3;n de las pol&#xed;ticas"/>
</node>
</node>
<node CREATED="1390415245734" ID="ID_1861189098" MODIFIED="1393504483488" TEXT="Se establece una RELACION CONTRACTUAL prinicipal-agente"/>
<node CREATED="1393504498721" ID="ID_213256012" MODIFIED="1393504508347" TEXT="Se basa en que ">
<node CREATED="1390415271680" ID="ID_1835701702" MODIFIED="1393504373009" TEXT="Todo el desempe&#xf1;o puede ser medido ">
<node CREATED="1390415318519" ID="ID_834865831" MODIFIED="1390415318520" TEXT="y el principal posee el suficiente conocimiento para fijar los objetivos del agente"/>
</node>
<node CREATED="1390415326316" ID="ID_1527953724" MODIFIED="1393504382088" TEXT="Al ciudadano s&#xf3;lo le interesa el cumplimiento del contrato"/>
</node>
</node>
<node CREATED="1390415368426" ID="ID_1048361879" MODIFIED="1393504432102" TEXT="Problemas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386956436266" ID="ID_34952779" MODIFIED="1393504383383" TEXT="La separaci&#xf3;n de la concepci&#xf3;n y formulaci&#xf3;n de pol&#xed;ticas de su ejecuci&#xf3;n NO ES POSIBLE">
<node CREATED="1386956528780" ID="ID_427520722" MODIFIED="1386956547550" TEXT="Las Agencias pasan a ejercer el control pol&#xed;tico"/>
<node CREATED="1386956548059" ID="ID_1592059431" MODIFIED="1386956565836" TEXT="Los Ministerios se inmiscuyen en el trabajo de las Agencias"/>
</node>
<node CREATED="1386956476769" ID="ID_1172831572" MODIFIED="1393504384881" TEXT="La concentraci&#xf3;n en &#xe1;reas o sectores espec&#xed;ficos">
<node CREATED="1386956573172" ID="ID_880505566" MODIFIED="1386957012086" TEXT="Dificulta la coordinaci&#xf3;n y la cooperacion entre los diferentes agentes"/>
<node CREATED="1386956620923" ID="ID_1658161404" MODIFIED="1386956639648" TEXT="La mayor&#xed;a de los bienes p&#xfa;blicos proviene de m&#xfa;ltiples entidades"/>
</node>
</node>
</node>
</node>
<node CREATED="1386957129857" ID="ID_430512175" MODIFIED="1518434236974" POSITION="right" TEXT="Gobernanza">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1393516535207" ID="ID_242242448" MODIFIED="1394553695784" TEXT="Aparici&#xf3;n:">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1393516648168" ID="ID_343248972" MODIFIED="1393516817041" TEXT=" J.R. Hollingsworth y L.N. Lindberg, art&#xed;culo &quot;The Governance of the American Enonomy: The Role of Markets, Clans, Hierarchies and Associative Behaviour&quot;">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1386957436642" ID="ID_304387598" MODIFIED="1394742260932" TEXT="Origen">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1393516833505" ID="ID_878991592" MODIFIED="1393516880447" TEXT="Koiman: &quot;Estamos asistiendo m&#xe1;s a un cambio por reequilibrio que a una alteraci&#xf3;n por abandono de las funciones estatales tradicionales&quot;">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1386957443411" ID="ID_310400448" MODIFIED="1393506858463" TEXT="Los gobiernos no son los &#xfa;nicos que se enfrentan a las grandes cuestiones sociales">
<node CREATED="1386957474827" ID="ID_219767647" MODIFIED="1386957484231" TEXT="Sociedad civil"/>
<node CREATED="1386957484546" ID="ID_944547225" MODIFIED="1386957487308" TEXT="Empresas"/>
</node>
<node CREATED="1386957525442" ID="ID_785104170" MODIFIED="1386957577198" TEXT="Las cuestiones sociales son el resultado de la interacci&#xf3;n de varios factores"/>
<node CREATED="1393516931200" ID="ID_1967701375" MODIFIED="1393516938947" TEXT="Globalizaci&#xf3;n">
<node CREATED="1393516938947" ID="ID_1460908018" MODIFIED="1393516982172" TEXT="Se transfieren competencias a instituciones internacionales"/>
<node CREATED="1393516982672" ID="ID_1981516835" MODIFIED="1393517008914" TEXT="Desregulaci&#xf3;n de los mercados internacionales"/>
<node CREATED="1393517024364" ID="ID_312086483" MODIFIED="1393517048757" TEXT="Algunas pol&#xed;ticas actuales obligan al tratamiento internacional">
<node CREATED="1393517048757" ID="ID_301225752" MODIFIED="1393517057181" TEXT="Medioambiente"/>
<node CREATED="1393517057608" ID="ID_320485597" MODIFIED="1393517060808" TEXT="Igualdad"/>
</node>
</node>
<node CREATED="1393517074183" ID="ID_1474175402" MODIFIED="1394530289868">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Incremento de la<b>&#xa0;fragmentaci&#xf3;n y complejidad</b>&#xa0;de las estructuras pol&#xed;ticas y administrativas
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1386957231902" ID="ID_857181292" MODIFIED="1394742332327" TEXT="Caracter&#xed;sticas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386957243636" ID="ID_455179598" MODIFIED="1386957278172" TEXT="Mayor grado de cooperaci&#xf3;n y de interacci&#xf3;n entre el Estado y actores no estatales"/>
<node CREATED="1386957305518" ID="ID_1262559264" MODIFIED="1386957326044" TEXT="Se difuminan los l&#xed;mites entre lo p&#xfa;blico y lo privado"/>
<node CREATED="1386957298027" ID="ID_814620784" MODIFIED="1386957349231" TEXT="No se elimina lo anterior: se convive con &#xe9;l."/>
<node CREATED="1393516102752" ID="ID_400012241" MODIFIED="1394530472042">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Modelo relacional o en<b>&#xa0;REDES</b>&#xa0;de interacci&#xf3;n p&#xfa;blico-privado
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1393516538165" ID="ID_1893997693" MODIFIED="1393516986981" TEXT="Rhodes: &quot; Un cambio de sentido del gobierno, un nuevo m&#xe9;todo conforme al cual se gobierna la sociedad&quot;">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1386957644745" ID="ID_1684150694" MODIFIED="1394742323079" TEXT="La gobernanza como gesti&#xf3;n de redes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1393517176051" ID="ID_491121972" MODIFIED="1393517258008" TEXT="Se pasa del ejercicio JER&#xc1;RQUICO a la GESTION DE REDES de actores individuales o colectivos"/>
<node CREATED="1393517220846" ID="ID_837246687" MODIFIED="1393517332435" TEXT="Se adapta mejor a los escenarios sociales din&#xe1;micos y complejos, cuya gesti&#xf3;n ser&#xed;a imposible desde un punto de vista monopol&#xed;stico (Marsh)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1393517333299" ID="ID_611397" MODIFIED="1393517364611" TEXT="Las redes presentan una forma organizativa plana y horizontal"/>
<node CREATED="1393517382900" ID="ID_1396790816" MODIFIED="1393517474014" TEXT="Mecanismo de coordinaci&#xf3;n">
<node CREATED="1393517474014" ID="ID_497124049" MODIFIED="1393517552357" TEXT="La confianza y/o lealtad "/>
<node CREATED="1393517553127" ID="ID_466272536" MODIFIED="1393517591256" TEXT="Frente a la jerarqu&#xed;a de la burocracia y competencia de los mercados"/>
</node>
<node CREATED="1393517141832" ID="ID_516671056" MODIFIED="1393517150633" TEXT="Ventajas">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386957654822" ID="ID_1429105188" MODIFIED="1394531048682">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      La<b>&#xa0;formulaci&#xf3;n de pol&#xed;ticas</b>&#xa0;se enriquece con la informaci&#xf3;n aportada por varios actores
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386957680071" ID="ID_1533393145" MODIFIED="1394531063798">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se puede alcanzar una <b>mayor aceptaci&#xf3;n y legitimaci&#xf3;n</b>&#xa0; social
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386957714715" ID="ID_239031189" MODIFIED="1394531285058">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Participaci&#xf3;n<b>&#xa0;interactiva y sim&#xe9;trica</b>: se tienen en cuenta una amplia<b>&#xa0;variedad de intereses y valores</b>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1386957736201" ID="ID_802222211" MODIFIED="1394531093688">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se reducen los <b>costes de transacci&#xf3;n</b>&#xa0;en la toma de decisiones
    </p>
  </body>
</html></richcontent>
<node CREATED="1386957796217" ID="ID_385265" MODIFIED="1394531136869" TEXT="A trav&#xe9;s de base de conocimiento com&#xfa;n">
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1386957813155" ID="ID_578499026" MODIFIED="1394531107806">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se pueden equilibrar las <b>asimetr&#xed;as</b>&#xa0;de poder al existir canales adicionales
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1386957838159" ID="ID_955796997" MODIFIED="1393517646074" TEXT="Cr&#xed;tica (Klickert)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386957866663" ID="ID_1409448518" MODIFIED="1386957885632" TEXT="Se puede desatender el inter&#xe9;s general por la negociaci&#xf3;n"/>
<node CREATED="1386957886241" ID="ID_327576014" MODIFIED="1386957925345" TEXT="Se pueden obstaculizar las innovaciones al dar un peso excesivo a intereses involucrados"/>
<node CREATED="1386957928175" ID="ID_1019663429" MODIFIED="1386957942592" TEXT="Los procesos decisionales pueden no ser transparentes"/>
<node CREATED="1386957948558" ID="ID_151703770" MODIFIED="1386957989144" TEXT="Puede producirse un deficit democr&#xe1;tico">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Escaso margen para la intervenci&#xf3;n parlamentaria
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1393517658248" FOLDED="true" ID="ID_784531239" MODIFIED="1394532983408" TEXT="Ambitos local y supranacional">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1393517671486" ID="ID_1401289059" MODIFIED="1393517732787" TEXT="Participaci&#xf3;n en el &#xe1;mbito local">
<node CREATED="1393517732787" ID="ID_1522450567" MODIFIED="1393539021569" TEXT="Caracter&#xed;sticas">
<node CREATED="1393517739581" ID="ID_1058417998" MODIFIED="1393517768893" TEXT="a) Visibilidad, cercan&#xed;a y accesibilidad de la organizaci&#xf3;n municipal"/>
<node CREATED="1393517770381" ID="ID_1231294006" MODIFIED="1393517848734" TEXT="b) La administraci&#xf3;n local es m&#xe1;s VERSATIL y flexible"/>
<node CREATED="1393517792073" ID="ID_1993903821" MODIFIED="1393517837549" TEXT="c) Mayores posibilidades para la INCLUSION de individuos interesados en los temas p&#xfa;blicos"/>
</node>
<node CREATED="1393517857460" ID="ID_1580866881" MODIFIED="1393517904668" TEXT="Enfoque que revaloriza el papel de la participaci&#xf3;n ciudadana ">
<node CREATED="1393517915500" ID="ID_412344247" MODIFIED="1393517940791" TEXT="Frente a la Nueva Gesti&#xf3;n P&#xfa;blica, que presta mayor atenci&#xf3;n a los resultados e impacto"/>
</node>
</node>
<node CREATED="1393517943480" ID="ID_744039179" MODIFIED="1394525582461" TEXT="&#xc1;mbito supranacional (UE)">
<node CREATED="1393517973128" FOLDED="true" ID="ID_1391044489" MODIFIED="1394532359216" TEXT="Distribuci&#xf3;n de responsabilidades y pautas de interacci&#xf3;n entre sus instituciones">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Desde el tratado de Lisboa, tambi&#xe9;n los Parlamentos nacionales son informados de las iniciativas legislativas
    </p>
  </body>
</html></richcontent>
<node CREATED="1394532145545" ID="ID_1934686643" MODIFIED="1394532165423" TEXT="Codecisi&#xf3;n entre el Parlamento y el Consejo en la funci&#xf3;n legislativa"/>
<node CREATED="1394532166169" ID="ID_1276968984" MODIFIED="1394532176899" TEXT="Control del Parlamento sobre la Comisi&#xf3;n">
<node CREATED="1394532176901" ID="ID_1876169617" MODIFIED="1394532188494" TEXT="Investidura del Presidente"/>
<node CREATED="1394532188800" ID="ID_1347147992" MODIFIED="1394532196279" TEXT="Investidura del Alto Representante"/>
<node CREATED="1394532196593" ID="ID_1128093496" MODIFIED="1394532210383" TEXT="Moci&#xf3;n de censura"/>
<node CREATED="1394532210913" ID="ID_1209882622" MODIFIED="1394532218407" TEXT="Comisiones de Investigaci&#xf3;n"/>
<node CREATED="1394532219305" ID="ID_127490832" MODIFIED="1394532230887" TEXT="Preguntas a la Comisi&#xf3;n y al Consejo"/>
</node>
<node CREATED="1394532237065" ID="ID_960880798" MODIFIED="1394532259010" TEXT="Intervenci&#xf3;n del Consejo Europeo">
<node CREATED="1394532259011" ID="ID_992851786" MODIFIED="1394532274482" TEXT="En la propuesta de la Composici&#xf3;n de la Comisi&#xf3;n">
<node CREATED="1394532274483" ID="ID_1683900017" MODIFIED="1394532277143" TEXT="Presidente"/>
<node CREATED="1394532277408" ID="ID_119938993" MODIFIED="1394532280336" TEXT="Comisarios"/>
<node CREATED="1394532280609" ID="ID_410235818" MODIFIED="1394532288887" TEXT="Alto Representante"/>
</node>
<node CREATED="1394532289985" ID="ID_833914201" MODIFIED="1394532298175" TEXT="En la propuesta de actos al Consejo"/>
</node>
<node CREATED="1394532320377" ID="ID_1657615754" MODIFIED="1394532347663" TEXT="Solicitudes de interveci&#xf3;n de la Comisi&#xf3;n Europea al Tribunal de Justicia Europeo"/>
</node>
<node CREATED="1393518015053" ID="ID_401252360" MODIFIED="1393518047370" TEXT="Libro Blanco sobre la gobernanza en Europa (2001): estrategias">
<node CREATED="1393518047370" ID="ID_29668708" MODIFIED="1394531400857" TEXT="La promoci&#xf3;n de una red de organizaciones para la elaboraci&#xf3;n e implantaci&#xf3;n de pol&#xed;ticas">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entre ellas el Comit&#xe9; de Regiones y el Comit&#xe9; Econ&#xf3;mico y Social
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1393518085956" ID="ID_1117866005" MODIFIED="1393518115840" TEXT="Desarrollo de capacidades de coordinaci&#xf3;n, que vinculan las administraciones ">
<node CREATED="1393518115840" ID="ID_823548074" MODIFIED="1393539163286" TEXT="sin recurrir a estructuras jer&#xe1;rquicas"/>
</node>
</node>
<node CREATED="1394531521171" ID="ID_1871256918" MODIFIED="1394531964168" TEXT="Comit&#xe9; de Regiones">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Organo consultivo creado por el Tratado de Maastricht, configura la participaci&#xf3;n de las entidades locales y regionales en la pol&#xed;tica de la Uni&#xf3;n Europea
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1394531526576" ID="ID_452789305" MODIFIED="1394532106131" TEXT="Comit&#xe9; Econ&#xf3;mico y Social ">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se cre&#xf3; por el Tratado Constritutivo de la Comunidad Econ&#xf3;mica Europea en 1957 (Tratado de Roma). Se trata de un &#xf3;rgano auxiliar.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1386960808933" ID="ID_1270186176" MODIFIED="1518434235559" POSITION="right" TEXT="El Gobierno abierto (Open Government)">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386960906328" ID="ID_932710442" MODIFIED="1393507878075" TEXT="Pilares">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386960909132" ID="ID_1638180601" MODIFIED="1386960978230" TEXT="Transparencia">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Toda la informaci&#xf3;n a disposici&#xf3;n de la sociedad
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386962779658" ID="ID_1261920595" MODIFIED="1393507716880" TEXT="Informaci&#xf3;n sobre la ACTIVIDAD de la instituci&#xf3;n"/>
<node CREATED="1386962794559" ID="ID_359587884" MODIFIED="1393507739313" TEXT="Informaci&#xf3;n ECONOMICA y de GESTION de la instituci&#xf3;n"/>
<node CREATED="1386962812389" ID="ID_1197361904" MODIFIED="1386962831254" TEXT="Otra informaci&#xf3;n p&#xfa;blica de inter&#xe9;s"/>
</node>
<node CREATED="1386960924827" ID="ID_1704165018" MODIFIED="1386961019319" TEXT="Participaci&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Implicar y dar voz a los ciudadanos en los procesos de toma de decisiones
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386962842138" ID="ID_1051452308" MODIFIED="1393507776426" TEXT="Consulta p&#xfa;blica">
<icon BUILTIN="group"/>
</node>
<node CREATED="1386962848897" ID="ID_915050826" MODIFIED="1393507781652" TEXT="Aportaci&#xf3;n ciudadana">
<icon BUILTIN="male1"/>
</node>
<node CREATED="1386962866187" ID="ID_80902644" MODIFIED="1393507803819" TEXT="Dise&#xf1;o de pol&#xed;ticas p&#xfa;blicas">
<icon BUILTIN="pencil"/>
</node>
</node>
<node CREATED="1386960933303" ID="ID_222095338" MODIFIED="1386961091182" TEXT="Colaboraci&#xf3;n">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Entre las administraciones p&#xfa;blicas, gobiernos, ciudadanos, empresas, administraciones
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1386962896998" ID="ID_1596836017" MODIFIED="1386962908714" TEXT="Co-prestaci&#xf3;n de servicios p&#xfa;blicos"/>
<node CREATED="1386962909372" ID="ID_1609868327" MODIFIED="1386962965504" TEXT="Servicios basados en la Reutilizaci&#xf3;n de la Informaci&#xf3;n en el Sector P&#xfa;blico (RISP)"/>
</node>
</node>
</node>
</node>
</map>
