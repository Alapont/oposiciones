<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1461660654839" ID="ID_1921807359" MODIFIED="1461915086456">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T17
    </p>
    <p style="text-align: center">
      GESTI&#211;N P&#218;B
    </p>
    <p style="text-align: center">
      (MODELOS)
    </p>
  </body>
</html></richcontent>
<node CREATED="1461663006743" ID="ID_1803957677" MODIFIED="1461663859755" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>1. Burocracia</b>
    </p>
    <p>
      (&lt;70s)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1461663998530" ID="ID_1532555286" MODIFIED="1461666447821">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      S&#170; <u>conc&#233;ntrico</u>&#160;&#160;con modelo de <u>jerarq</u>&#160;funcionarial
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1461664170496" ID="ID_883169707" MODIFIED="1461686635254">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Racionaliz&#160;<u>legal</u>&#160;(somet a ley) por encima de gerencial (efica/ci)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
<node CREATED="1461664235731" ID="ID_1862751047" MODIFIED="1461686659529">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cumplim&#160;del <u>proc adm</u>&#160;se antepone a objs
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1461915510186" ID="ID_469507834" MODIFIED="1461915514016" TEXT="Origen">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461915512207" ID="ID_147253670" MODIFIED="1461915525082" TEXT="Desarr Estado Moderno&#x2192; expansi&#xf3;n de tareas adm">
<node CREATED="1461664333250" ID="ID_160354522" MODIFIED="1461915110894">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ppios:</i>&#160;estruct jer&#225;rq <u>r&#237;gidas</u>&#160;y <u>ctrl pol&#237;tico</u>&#160;&#160;sobre la AP (distingue pol&#237;t de ejec&#8594; <u>neutra</u>)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1461663018788" ID="ID_1755213070" MODIFIED="1461664454369" TEXT="Caracts">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461664499240" ID="ID_322855433" MODIFIED="1461742818587">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pleno somet a ley&#160;(<u>previsible</u>), <u>formaliz</u>&#160;de coms (escrita), div rac e impers del trabajo (funcs bien def),
    </p>
    <p>
      <u>jerarq</u>&#160;de autoridad (ctrl int), meritocracia, especializ (no intereses priv), <u>profesionaliz</u>, delegaci&#243;n
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1461663028819" ID="ID_354030704" MODIFIED="1461916194208" TEXT="Crisis">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_cancel"/>
<node CREATED="1461664963813" ID="ID_1899672473" MODIFIED="1462519670637">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Crisis Bienestar ( <u>petrol</u>), complej materias, alej de realidad soc,<u>inadapt</u>&#160;a nuevas tecn (<u>Soc Info </u>)
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1461665254294" ID="ID_613737505" MODIFIED="1461670242073">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Sol:&#160;</i>adopc <u>t&#233;cns gerenciales</u>&#160;y mecs del merc en el SP&#8594; Nueva GP
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1461663040504" ID="ID_1076240165" MODIFIED="1461743207484" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>2. Nueva </b>
    </p>
    <p>
      <b>Gesti&#243;n P&#250;b</b>
    </p>
    <p>
      (70-00s)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1461658537536" ID="ID_1718334689" MODIFIED="1461668885493">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      T&#233;cns de gesti&#243;n <u>priv app a AP</u>&#160;ante entorno cambiante bajo <u>efici, eficacia, respons y cult org</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1461665462971" ID="ID_1936965456" MODIFIED="1462519844996">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Orient a optimiz m&#225;x de <u>results</u>, seg&#250;n <u>satisf de ciuds</u>=clientes
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1461663048079" ID="ID_775111788" MODIFIED="1461663050071" TEXT="Origen">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461665579044" ID="ID_116875981" MODIFIED="1461743539609">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Public Management:</i>&#160;<u>EEUU</u>, UK y NZ&#8594; influyen en pa&#237;ses OCDE
    </p>
  </body>
</html></richcontent>
<node CREATED="1461743374049" ID="ID_886947971" MODIFIED="1461915158153">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>ESP:</i>&#160;a partir de los 90s con el proceso de<i>&#160;'Moderniz Adm'</i>&#8594; <u>controversia</u>&#160;al ppio (dif opins)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1461666116895" ID="ID_286974538" MODIFIED="1461667661268" TEXT="Caracts">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461663077552" ID="ID_457064581" MODIFIED="1461915702704">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Dise&#241;o&#160;org:</i>&#160;&#160;separa pol&#237;tica (central) de <u>ejec</u>&#160;(descentraliz)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1461656724731" ID="ID_365390596" MODIFIED="1461915981442">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lib de merc (-intervenc), <u>privatiz</u>&#160;SP (Estado regula), nuevos <u>orgs</u>&#160;priv (flex)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1461666641254" ID="ID_748719514" MODIFIED="1461915991737">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Uso TIC-eAdm:</i>&#160;acceso de ciuds a info, gesti&#243;n int (efici) y gobernab (particip ciud)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1461663080466" ID="ID_1481490292" MODIFIED="1461667013752">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cal:</i>&#160;def por ciuds&#8594; cartas de servs e indicadores para <u>mejora cont</u>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1461663084602" ID="ID_1277591889" MODIFIED="1461915382594">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Herrams gesti&#243;n:</i>&#160;<u>ctrl gasto</u>, superv, tasas, <u>contrat ext</u>, mercs int, competitividad
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1461663073626" ID="ID_1132403208" MODIFIED="1462519779579">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Orient al cliente: </i>ciud como cliente, recoger su opini&#243;n
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1461663051887" ID="ID_384016220" MODIFIED="1461915780378">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Gesti&#243;n Directiva: </i>&#160;gerente (ni func ni pol&#237;t)&#8594;&#160; <u>t&#233;cns</u>&#160;priv
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1461658188499" ID="ID_161226404" MODIFIED="1461915604897">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Dir Gen: </i><u>planif</u>&#160;estrat, dir por <u>objs</u>&#8594; anticip y <u>adaptac</u>&#160;a cambios
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1461658287018" ID="ID_533056589" MODIFIED="1461915422818">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Espec&#237;ficas: </i>(&#225;reas) <u>marketing</u>, dir ops, dise&#241;o org, dir rrhh, gesti&#243;n financ y gesti&#243;n de SI&#160;
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1461658335535" ID="ID_1004682104" MODIFIED="1461915431219">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Habilids dir: </i><u>formaci&#243;n</u>&#160;para mejorar productiv y eficiencia&#8594; importancia creciente
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1461656223674" ID="ID_449386542" MODIFIED="1461915477722">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Particularids AP</b>
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461656232785" ID="ID_739246482" MODIFIED="1461915473605">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Req <u>adapt</u>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1461661811470" ID="ID_370776876" MODIFIED="1462520234777">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Pol&#237;tica en lugar de merc:</i>&#160;asign de recs <u>equit</u>&#160;(no excluye al no solvente, como si fuera cliente)
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1461661981404" ID="ID_162258920" MODIFIED="1461916051086">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Poderes p&#250;b</i>: <u>limit</u>&#160;por leyes,&#160;Estado <u>instrumental</u>&#160; para paliar necesidades p&#250;b
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1461662059904" ID="ID_1628699083" MODIFIED="1461662359853">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Creaci&#243;n de valor:</i>&#160;no solo por prods/servs&#8594; indir&#160;( <u>dif&#237;cil medir</u>): bienestar, seg, ig...
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1461656219042" ID="ID_297322045" MODIFIED="1461742925407">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reflejo
    </p>
    <p>
      gerencial
    </p>
    <p>
      en normas
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461659464438" ID="ID_1777668553" MODIFIED="1461742911215">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Funcionam
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461659470926" ID="ID_1098550302" MODIFIED="1461659536830">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>art 103-Const: </i>AP <u>sirve</u>&#160;intereses generales (ciud cmo cliente)
    </p>
  </body>
</html></richcontent>
<node CREATED="1461659537415" ID="ID_1460800230" MODIFIED="1461659633173">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>art 3 y 4-LOFAGE: </i>ppios de eco, prox a ciuds, efici, efica y objs; serv al ciud
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1461659636514" ID="ID_1518052261" MODIFIED="1461915907188">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>art 3-LRJPAC:</i>&#160;ppios de <u>coop</u>&#160;y colab, efici y serv
    </p>
  </body>
</html></richcontent>
<node CREATED="1461659670266" ID="ID_1149619862" MODIFIED="1461916709080">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>LGP:</i>&#160;<u>planif</u>&#8594; escenarios plurianual, pgms minist y centros gestores; gasto por objs, eval
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1461659769410" ID="ID_1574940363" MODIFIED="1461659892941">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>RD 951/2005: </i>pgms para mejora de<u>cal</u>&#160;de servs AGE
    </p>
  </body>
</html></richcontent>
<node CREATED="1461659822429" ID="ID_42192067" MODIFIED="1461743159661">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>RD 3/2011:</i>&#160;contratos de <u>colab</u>&#160;p&#250;b-priv (gesti&#243;n priv)&#8594; <u>externaliz</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1461660747101" ID="ID_1550830818" MODIFIED="1461916010758">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>CORA:</i>&#160;reco t&#233;cns gerenc para <u>medir productiv y efici</u>&#160;(indics)
    </p>
  </body>
</html></richcontent>
<node CREATED="1461660771892" ID="ID_1107390618" MODIFIED="1461915955887">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      -demora resol, evitar cap no usadas y objs seg&#250;n recs dispo
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1461916682645" ID="ID_1084845417" MODIFIED="1461916712964">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ley 40/2015:</i>&#160;ppios&#8594; transpar, planif y dir por <u>objs</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1461659902996" ID="ID_292320051" MODIFIED="1461742911691">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Org
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461659919174" ID="ID_1514351337" MODIFIED="1461662793948">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>LOFAGE:&#160;</i>Adm Instit (OOAA, EPE y Ags)&#8594; orgs p&#250;b con&#160; <u>pers jur</u>&#160;prop&#8594; incorp dcho priv (flex)
    </p>
  </body>
</html></richcontent>
<node CREATED="1461915811976" ID="ID_1830990961" MODIFIED="1461915824539">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>2001:</i>&#160;creaci&#243;n <u>AEAT</u>&#160;(hito)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1461660237440" ID="ID_413682541" MODIFIED="1461916126511">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ley 28/2006:</i>&#160;<u>Ags</u>&#8594; dir por objs y planif (cal), pe AEVAL
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1461660325040" ID="ID_1652785614" MODIFIED="1461742912058">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Personal
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461659075830" ID="ID_1443898871" MODIFIED="1461915872294">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>EBEP: </i>(RD 5/2015) refuerza funci&#243;n <u>dir</u>&#8594; m&#233;ritos y cap, eval seg&#250;n results
    </p>
  </body>
</html></richcontent>
<node CREATED="1461660712690" ID="ID_732569695" MODIFIED="1461667981700">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Eval de <u>desempe&#241;o</u>&#160;(conducta y rendim)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
<node CREATED="1461668052576" ID="ID_170725057" MODIFIED="1461916205402" TEXT="Crisis">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_cancel"/>
<node CREATED="1461668057876" ID="ID_978298130" MODIFIED="1461916215518">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Exceso de <u>externaliz</u>&#160;y recortes
    </p>
  </body>
</html></richcontent>
<node CREATED="1461668114937" ID="ID_1800276212" MODIFIED="1461670259385">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Dif&#237;cil separar</u>&#160;pol&#237;t-ejec&#8594; planif objs no puede hacerse sin conocim q da la ejec
    </p>
  </body>
</html></richcontent>
<node CREATED="1461668205100" ID="ID_12412710" MODIFIED="1461670268014">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Falta coord</u>/colab AP y priv para eval results
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1461663095024" ID="ID_872765665" MODIFIED="1461743687390" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b>3. Gobernanza</b>
    </p>
    <p>
      (&gt;00s)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1461668580229" ID="ID_1338850613" MODIFIED="1461916312001">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Interacci&#243;n</u>&#160;&#160;y coop entre el Estado y <u>actores ext</u>&#160;(soc)&#8594; decis mixtas y adapt a escenarios din
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1461669215476" ID="ID_1658580703" MODIFIED="1461740850215">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Paso de jerarqu&#237;a a<u>redes</u>&#160;de actores (planas y horiz)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1461668847358" ID="ID_1762868625" MODIFIED="1461668850923" TEXT="Origen">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461668850927" ID="ID_969053498" MODIFIED="1462520012651">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Globaliz</u>&#160;(probls afectan a todos), nuevos actores,&#160; <u>p&#233;rd sober</u>&#160;nac&#8594; Estado <u>facilitador</u>, -dif pub-priv
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1461668827966" ID="ID_381185472" MODIFIED="1461744361100">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No anula modelos anteriores, los modula&#8594;&#160; <u>conviven</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1461755130087" ID="ID_454489038" MODIFIED="1461755139722" TEXT="Hoy&#x2192; Gobierno Abierto (T19B)"/>
</node>
</node>
<node CREATED="1461669247855" ID="ID_1744124905" MODIFIED="1461669252115" TEXT="Gesti&#xf3;n de redes">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461669341934" ID="ID_236898742" MODIFIED="1462520159680">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pol&#237;ts <u>enriq</u>&#160;con info y colab, +acept soc,&#160; <u>particip</u>&#160;(democr), orient a soc,&#160;conocim com&#250;n,&#160; <u>capital</u>&#160;soc&#160;
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1461669465164" ID="ID_1079693836" MODIFIED="1462520270230">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Muchos actores <u>obstac</u>&#160;cambios, dif&#237;cil id&#160; <u>respons</u>&#160;(no transpar), parlam pierde protag
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="button_cancel"/>
<node CREATED="1461744472535" ID="ID_905598278" MODIFIED="1462520187485">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Todav&#237;a req&#160;reformas profundas y +implic soc
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1461669610979" ID="ID_1885835657" MODIFIED="1461669618098" TEXT="&#xc1;mbitos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1461669663197" ID="ID_1973154078" MODIFIED="1461669889380">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Local&#8594; partic ciud:</i>&#160;id intereses concretos, adm vers&#225;til ante demandas, +inclusi&#243;n&#160;de interesados
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1461669780667" ID="ID_1002628194" MODIFIED="1462520115730">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Supranac&#8594; <u>Libro Blanco</u>&#160;de Gob Eu (2001):</i>acercar EU a ciuds&#8594; apertura del proc de<u>elab de pol&#237;ts</u>
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1461740930084" ID="ID_1043048670" MODIFIED="1461741051503">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      RD 802-6/2014
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1461741036123" ID="ID_648924176" MODIFIED="1461741157775">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Nuevo modelo gobernanza AGE basado en TIC
    </p>
  </body>
</html></richcontent>
<node CREATED="1461916513447" ID="ID_1006004294" MODIFIED="1461916529849" TEXT="Articula &#xfa;nica pol&#xed;tica dentro de la multiplic de actores implicados en TIC"/>
</node>
</node>
</node>
</node>
</map>
