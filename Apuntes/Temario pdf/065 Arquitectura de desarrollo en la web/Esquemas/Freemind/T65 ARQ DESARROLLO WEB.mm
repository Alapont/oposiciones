<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1436976896955" ID="ID_807814835" MODIFIED="1517500191755">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T62 ARQ
    </p>
    <p style="text-align: center">
      DESARROLLO WEB
    </p>
    <p style="text-align: center">
      <b>Contenido Din&#225;mico</b>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1436976986261" ID="ID_144036606" MODIFIED="1436976989888" POSITION="right" TEXT="Arq Internet">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436977427015" ID="ID_806697343" MODIFIED="1436977429784" TEXT="Sitio web">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436977429784" ID="ID_980813032" MODIFIED="1436987993508">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Info almacenada en ficheros (formato HTML) solicitados por cliente a serv web v&#237;a&#160; <u>HTTP</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1436977448101" ID="ID_299621983" MODIFIED="1436987647385">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Din&#225;mico:</i>&#160;incluyen&#160; <u>scripts</u>&#160;interpret por serv y cliente
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1436976989889" ID="ID_339637581" MODIFIED="1436977255807" TEXT="2 niveles (C/S)">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436976997483" ID="ID_342371274" MODIFIED="1436977075658">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Presentaci&#243;n uniforme, en contenido din&#225;mico&#160;&#160;<u>cliente no puede</u>&#160;soportar todo el proceso
    </p>
  </body>
</html></richcontent>
<node CREATED="1436977089410" ID="ID_1589914056" MODIFIED="1446630202920">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Sol: </i>3er nivel que <u>aisle</u>&#160;acceso a datos&#8594; capa App
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1436977080096" ID="ID_1745881524" MODIFIED="1436977256175" TEXT="3 niveles">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436977145139" ID="ID_1443174320" MODIFIED="1436977237372">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Presentaci&#243;n (p&#225;gs web e interac user)&#8594; App (Objetos de Neg, l&#243;gica y com user-BDR)&#8594; Datos (BDR)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1436977256637" ID="ID_1136239312" MODIFIED="1436977266791">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Componentes sw
    </p>
    <p>
      de app web
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436977549149" ID="ID_1298873309" MODIFIED="1436977625096" TEXT="Cliente">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436987044665" ID="ID_891559194" MODIFIED="1436987914350">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Naveg, int&#233;rprete script, JVM (ejec applets), plugins, TCP/IP
    </p>
  </body>
</html></richcontent>
<node CREATED="1436988274829" ID="ID_1372141503" MODIFIED="1436988381290">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Debe soportar protcs de app, seg, contenido, multim y script
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1436977537026" ID="ID_530086976" MODIFIED="1436977624016" TEXT="Serv Web/App">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436987093768" ID="ID_806757381" MODIFIED="1436988197696">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Serv HTTP/SSL, p&#225;gs HTML, Objetos de Neg, p&#225;gs din&#225;micas, int&#233;rprete
    </p>
    <p>
      script, JVM, <u>drivers</u>&#160;JDBC/ODBC/ADO, archivos multimedia,&#160; <u>TCP/IP</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1436987408322" ID="ID_1136075053" MODIFIED="1436987599757">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Gestor Transaccs (opc): funcs adicionales
    </p>
    <p>
      (mux conexs a BD, monit del s&#170;)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1436977545748" ID="ID_1651151873" MODIFIED="1436977624448" TEXT="Serv Datos">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436977587708" ID="ID_1309398945" MODIFIED="1437664128702">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      SGBDR, <u>drivers</u>&#160;JDBC/ODBC/ADO y TCP/IP
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1436977612739" ID="ID_1688498742" MODIFIED="1436988025138">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tecns de desarrollo
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1436977618038" ID="ID_1448546636" MODIFIED="1436977635274" TEXT="Java">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436977645450" ID="ID_1462573603" MODIFIED="1446630933723">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Java Server Pages (.JSP) y Servlets, <u>indep</u>&#160;de SO/naveg, JDBC, Apache/IIS, <u>JavaScript</u>,&#160;EJB(s) y applets(c)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1436977672762" ID="ID_857662005" MODIFIED="1436977677924" TEXT="Microsoft">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436977677924" ID="ID_1760272766" MODIFIED="1446630942683">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Active (.ASP), Windows y naveg IE, ODBC/ADO.NET, IIS,&#160;&#160;<u>JScript o VBScript</u>, COM(s) y ActiveX(c)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1436988802516" ID="ID_1683740947" MODIFIED="1436988805798" TEXT="OpenSource">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436977645450" ID="ID_111086245" MODIFIED="1448436897160">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      .PHP o Perl, <u>indep</u>&#160;de SO/naveg/serv web, DBI (Perl) o ADOdb (PHP), <u>JavaScript</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1436977269000" ID="ID_1871247020" MODIFIED="1436985910755" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Lengs de scripts
    </p>
    <p>
      de cliente
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448437107855" ID="ID_371999461" MODIFIED="1448438164953">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>DHTML:</i>&#160;cont din&#225;m en el naveg&#8594; <u>HTML+leng interpretado</u>&#160;en cliente (interacc y&#160; <u>comprobs</u>&#160;de info)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
</node>
<node CREATED="1436987740222" ID="ID_508315627" MODIFIED="1448438451415">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cliente 'standalone'&#8594; No req conex al serv (solo en solicitud de p&#225;g)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1448437327114" ID="ID_957889191" MODIFIED="1448437963995">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Scripts:</i>&#160;leng de <u>eventos</u>&#160;e <u>interpretado</u>/traducido en t de ejec&#8594; parsea l&#237;nea a l&#237;nea
    </p>
  </body>
</html></richcontent>
<node CREATED="1448437418433" ID="ID_1170992699" MODIFIED="1448437956470">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      -rendim, no permite reutiliz (al no compilar), limita dise&#241;o (usab)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1436977281652" ID="ID_1531505057" MODIFIED="1436977285950" TEXT="JavaScript">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436977792440" ID="ID_1943840062" MODIFIED="1448437612695">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Basado en <u>objetos</u>&#160;(no permite herencia m&#250;ltiple) e&#160; <u>indep</u>&#160;de plataf
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1436977863810" ID="ID_1687906915" MODIFIED="1448438213705">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Case sensitive, similar a Java (<u>no igual)</u>, est&#225;ndar ECMA-262
    </p>
  </body>
</html></richcontent>
<node CREATED="1436978028733" ID="ID_1750430960" MODIFIED="1436988592243">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Integr con naveg, usa&#160; <u>cookies</u>&#160;(conserva estado&#160;cliente)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1436978086155" ID="ID_1554597594" MODIFIED="1436978109855" TEXT="Difiere entre navegs">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node CREATED="1436977293631" ID="ID_858659355" MODIFIED="1448438029152">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      JScript
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1448438029138" ID="ID_726768542" MODIFIED="1448438039216" TEXT="Competencia MS de JavaScript, similar">
<node CREATED="1436978204212" ID="ID_773888491" MODIFIED="1448438011263">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Muy eficiente, sirve para cliente y serv (IIS)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1436978250050" ID="ID_534986023" MODIFIED="1448438252166">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Incompat con ECMA y JavaScript, solo funciona en&#160;&#160; <u>IE</u>&#160;o Windows Script Host
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node CREATED="1436977286396" ID="ID_345243544" MODIFIED="1436977289528" TEXT="VBScript">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436978437259" ID="ID_1763705229" MODIFIED="1448438225962">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Visual Basic, MS) ejec seqs de cliente en IE y de servs en IIS
    </p>
  </body>
</html></richcontent>
<node CREATED="1436978823632" ID="ID_1809852628" MODIFIED="1436978852797" TEXT="Integr total con Windows (apps muy sofisticadas, +funcs que JavaScript)">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1436978856159" ID="ID_915379999" MODIFIED="1436978867877" TEXT="Exclusivo para IE y IIS">
<icon BUILTIN="button_cancel"/>
</node>
</node>
</node>
<node CREATED="1436988682080" ID="ID_1870383419" MODIFIED="1436988685266" TEXT="PerlScript">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436988689947" ID="ID_206090709" MODIFIED="1448438280067">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Permite uso de <u>Perl</u>&#160;en scripts;&#160;orientado a entorno&#160;MS (ASP);&#160;se ejec en IIS, IE&#160;o WSH; tb en serv
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1436979547030" ID="ID_655202526" MODIFIED="1448437257714" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comps de cliente
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436979070174" ID="ID_879827109" MODIFIED="1448437891653">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Applet no permite acceso a recursos, ActiveX s&#237; (al descargarse). Solo req conex en descarga
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1436979125499" ID="ID_1124867561" MODIFIED="1448437897687">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Son <u>+pesados</u>&#160;que los Scripts e <u>inseguros</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1436977314318" ID="ID_250858961" MODIFIED="1436986255843" TEXT="Applets">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436979109153" ID="ID_464660670" MODIFIED="1448437829079">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pgm de <u>Java</u>&#160;remoto (ref en p&#225;g HTML), se ejec en <u>cliente</u>&#8594; en zona '<u>sandbox</u>' (protege datos y recs)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1436986007612" ID="ID_1693570440" MODIFIED="1448442458681">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      JRE no le permite R/W archivos en cliente, acceder a apps, ni cargar libs
    </p>
  </body>
</html></richcontent>
<node CREATED="1436986086504" ID="ID_717222241" MODIFIED="1448442471859">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Solo&#160; <u>imprimir y guardar</u>&#160;docs (con permiso cliente y applet firmado)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1448442387730" ID="ID_1204725224" MODIFIED="1448442427840">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Puede abrir conexs con el serv
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_cancel"/>
<node CREATED="1448442001180" ID="ID_187865646" MODIFIED="1448442008239" TEXT="Usar solo en redes Intranet">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1436986222715" ID="ID_186826121" MODIFIED="1446630336544">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Etiqueta creaci&#243;n&#160;&lt;APPLET&gt;
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436986261668" ID="ID_1300977914" MODIFIED="1446630450746">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      CODE (pe MiApplet.class)/OBJECT (applet serializ), WIDTH/HEIGHT (zona naveg), CODEBASE (opc, URL)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1436977318254" ID="ID_1511461862" MODIFIED="1436977321529" TEXT="Controles ActiveX">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436986310885" ID="ID_1026141739" MODIFIED="1448383619939">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comps que se descargan dsd p&#225;g web para caracts sofisticadas y<u>animaciones</u>. Se integran con&#160; <u>XML</u>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1436986359088" ID="ID_1772572256" MODIFIED="1436986407269">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Arq abierta: </i>comps pueden interactuar <u>indep del leng</u>&#160;de pgmaci&#243;n y son <u>reutilizables</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1436986416824" ID="ID_633747486" MODIFIED="1436986435007">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tecn <u>COM</u>&#160;(Component Object Model)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1436986480285" ID="ID_1009741661" MODIFIED="1436986507115">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Etiqueta de creaci&#243;n&#160;&lt;OBJECT&gt;
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436986488073" ID="ID_631070078" MODIFIED="1436986531700" TEXT="CLASSID (id &#xfa;nico de control) y CODEBASE (URL dsd dnd se descarga)"/>
</node>
</node>
</node>
<node CREATED="1436977327008" ID="ID_1880390881" MODIFIED="1436977336362" POSITION="right" TEXT="Integraci&#xf3;n">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436977336363" ID="ID_987490045" MODIFIED="1436977340126" TEXT="Contenido">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436986564778" ID="ID_1880005600" MODIFIED="1446631069004">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      SGD, portales, buscadores, herrams de public web y de gesti&#243;n departamental, blog, comunids
    </p>
  </body>
</html></richcontent>
<node CREATED="1436986900438" ID="ID_845039011" MODIFIED="1436986908172" TEXT="Plugins (formatos no soportados, pe PDF)"/>
</node>
</node>
<node CREATED="1436977340731" ID="ID_1546760049" MODIFIED="1436977342711" TEXT="Sonido">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436986614068" ID="ID_1259639254" MODIFIED="1446631213377">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>MP3: </i>(MPEG Layer 3, Inst Fraunhofer 1992) patentado, con p&#233;rdidas, comp <u>1:10</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1446631149495" ID="ID_853063533" MODIFIED="1446631172268">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ogg: </i>muy similar a MP3 pero <u>libre</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1446631142641" ID="ID_849098450" MODIFIED="1446631269218">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Otros:</i>&#160;WMA (MS, con DRM), RealAudio, WAV (MS e IBM, sin compr ni p&#233;rdidas), Streaming
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1436977343268" ID="ID_1266569790" MODIFIED="1436977345727" TEXT="Im&#xe1;genes">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436986693473" ID="ID_1174566086" MODIFIED="1446631693199">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>PNG:</i>&#160;(ISOC) portable, liviano y sin p&#233;rdidas
    </p>
  </body>
</html></richcontent>
<node CREATED="1446631684818" ID="ID_1322716598" MODIFIED="1446631684821">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Reemplaza GIF (alg LZW patentado) y JPEG (alg con p&#233;rdidas 1:10, ISO e ITU-T)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1436977346253" ID="ID_331985862" MODIFIED="1436977350065" TEXT="Animaci&#xf3;n">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1436986768236" ID="ID_1959966787" MODIFIED="1436986843213">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Interactiva:</i>&#160;Shockwave, Flash (+ligero), VRML (Virtual Reality Model Lang, mundos 3D)
    </p>
  </body>
</html></richcontent>
<node CREATED="1436986811871" ID="ID_1515338815" MODIFIED="1436988968402">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>No interactiva:</i>&#160;(video) QuickTime, RealVideo, MPEG4 (divx)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
</map>
