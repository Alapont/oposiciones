<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1437662376300" ID="ID_1368853956" MODIFIED="1517500209007">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      T62 JAVA EE
    </p>
    <p style="text-align: center">
      <b>Apps Web</b>
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1437822588988" ID="ID_718625977" MODIFIED="1449825802286" POSITION="right" TEXT="Patr&#xf3;n MCV">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437823537949" ID="ID_8253263" MODIFIED="1449825776051">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Divide funcionalidad de <u>app web</u>&#160;en elementos sw repartidos por tareas <u>indep</u>&#8594; comps <u>especializados</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1437823260215" ID="ID_991317044" MODIFIED="1449825761472">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cliente ligero y arq&#160; <u>multicapa</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1449825798265" ID="ID_1435384456" MODIFIED="1449825801236" TEXT="Modelo">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437823585551" ID="ID_553968391" MODIFIED="1449825843595">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>L&#243;g de neg</u>&#8594; acceso a datos y reglas. Lengs de prop general (Java) y lengs de SGBD
    </p>
  </body>
</html></richcontent>
<node CREATED="1449822424500" ID="ID_1806223224" MODIFIED="1449822477426" TEXT="EJB">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1449825802268" ID="ID_184473695" MODIFIED="1449825805004" TEXT="Vista">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437823655200" ID="ID_1134712926" MODIFIED="1449825836769">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Presentaci&#243;n</u>&#160;de UI (result de transaccs), con controlador asociado. Usa tecns de dise&#241;o
    </p>
  </body>
</html></richcontent>
<node CREATED="1449822427176" ID="ID_120108686" MODIFIED="1449822478493" TEXT="JSP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1449825805426" ID="ID_1071249258" MODIFIED="1449825808060" TEXT="Controlador">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437823747655" ID="ID_439729564" MODIFIED="1449825823037">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Control de flujo </u>&#8594; rx peticiones y las traduce a llamadas al modelo y vistas
    </p>
  </body>
</html></richcontent>
<node CREATED="1449822435378" ID="ID_586629845" MODIFIED="1449822479205" TEXT="Servlets">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1437822614315" ID="ID_1539865420" MODIFIED="1449826218661" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Java EE
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437823835159" ID="ID_580152820" MODIFIED="1449822830508">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Plataf de <u>apps multicapa</u>&#160;distrib&#8594;
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1449822693138" ID="ID_414753201" MODIFIED="1449822911591">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Comps reut, seg unific (=criterios), ctrl transacc (evita cuello),&#160; <u>abierto</u>&#160;e intercamb datos XML con WS
    </p>
  </body>
</html></richcontent>
<node CREATED="1437823975333" ID="ID_848344530" MODIFIED="1449822971290">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      -costes y neutr tecn
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1437822710927" ID="ID_777791680" MODIFIED="1449826088150">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      SOA
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="xmag"/>
<node CREATED="1437827912955" ID="ID_1328805817" MODIFIED="1449826103681">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pgms que ofrezcan <u>servicios-WS a otros</u>&#160;&#160;pgms&#160;(<u>indep</u>&#160;de su plataforma) mediante HTTP
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1437824142972" ID="ID_404257830" MODIFIED="1437824146739" TEXT="Ecosistema">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437824146739" ID="ID_1210812861" MODIFIED="1437836459198">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Especs</u>&#160;b&#225;sicas, libs/APIs, buenas pr&#225;ctics (Blueprints), <u>test de compat</u>&#160;(CTSuite), doc ref (ejempls, tut)
    </p>
  </body>
</html></richcontent>
<node CREATED="1449822632021" ID="ID_67479753" MODIFIED="1449822668890">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se incluyen las APIs de&#160; <u>Java SE</u>&#160;(fuente origen)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1449823154895" ID="ID_1468431972" MODIFIED="1449823168474">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#218;ltima versi&#243;n estable: Java EE <b>5</b>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1437837795355" ID="ID_1756394819" MODIFIED="1449826227424">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Compat con JEE <b>7</b>:</i>&#160;Glassfish, TMAX JEUS, WildFly, Cosminexus, Weblogic y Websphere
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1448444265292" ID="ID_60392856" MODIFIED="1449826231786">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>JBOSS</u>&#160;(libre y el +usado) compat con JEE <b>6</b>
    </p>
  </body>
</html></richcontent>
<node CREATED="1448444247840" ID="ID_760627472" MODIFIED="1449825943284">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Apache Tomcat:</i>&#160;contiene servlets y JSP (serv web) pero no sirve como sol Java (no tiene EJBs!)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1437822633607" ID="ID_346927496" MODIFIED="1448444652416">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Arq/
    </p>
    <p>
      capas
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437824813843" ID="ID_119301035" MODIFIED="1437824925775">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Cliente
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437824818483" ID="ID_97883126" MODIFIED="1449823629575">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cliente web: </i>interfaz HTML en &#160;<u>naveg</u>&#8594; ligero, p&#225;gs&#160; <u>JSP</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1449822228099" ID="ID_263580848" MODIFIED="1449823613740">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>G2C:</i>&#160;(Gov to Citizen)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1437824870022" ID="ID_1386461562" MODIFIED="1449823714426">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Contenedor&#160;Cliente:</i>&#160; <u>app</u>&#160;escritorio&#8594; pesado, UI +elaborada (Swing, AWT) y cierta l&#243;g
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1449822249008" ID="ID_1558561169" MODIFIED="1449823674395">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>G2G: </i>despliegue WebStart (+seg) y <u>WS</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1437824983735" ID="ID_521694508" MODIFIED="1449823495130">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Contenedor de Applets:</i>&#160;req JVM (obsoleto)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1437825038943" ID="ID_758957252" MODIFIED="1448444663903">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Serv
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1437825045876" ID="ID_1753009613" MODIFIED="1437838972810">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cont Web</i>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1437825053884" ID="ID_1191913089" MODIFIED="1437826984722">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sw que gestiona ejec de <u>componentes Web</u>. Capa intermedia entre serv web y Servlet
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="info"/>
</node>
<node CREATED="1437822664884" ID="ID_1118237635" MODIFIED="1449823864230" TEXT="Servlets">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437825662962" ID="ID_1509740643" MODIFIED="1437825757845">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Generan <u>contenido din&#225;mico </u>. Invocados por URL y devueltos por http
    </p>
  </body>
</html></richcontent>
<node CREATED="1437825737899" ID="ID_769607956" MODIFIED="1437826142868" TEXT="Multi-thread (comparten recs)">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1449823864213" ID="ID_1968554698" MODIFIED="1449823873740">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Controladores
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1449823777912" ID="ID_1486149854" MODIFIED="1449823910422">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Recogen info del user (pe datos de formulario) y&#160; <u>reparten&#160;juego</u>&#160;sobre los EJB (priorizando)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1449823825634" ID="ID_811578153" MODIFIED="1449823918858">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Recogen resultados de EJBs, los empaquetan y se lo devuelven al JSP para q lo presente
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1437825774369" ID="ID_51531566" MODIFIED="1437825843016">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>HttpServlet: </i>clase abstracta de la q heredan m&#233;todos doGet, doPost, doPut
    </p>
  </body>
</html></richcontent>
<node CREATED="1449823747019" ID="ID_1076346112" MODIFIED="1449823767908">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      paqs java.servlet / java.servlet.http
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1437825889878" ID="ID_546931051" MODIFIED="1449823963185">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Serv web rx e id petici&#243;n cliente y la retx al Contenedor&#8594; Crea obj-Servlet y ejec doGet/Post&#8594; Analiza <i>HttpServletRequest</i>&#160;y <u>llama a comps EJBs</u>&#8594; Genera <i>HttpServletResponse:</i>&#160;p&#225;g HTML/XML para cliente
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="idea"/>
</node>
<node CREATED="1437825850672" ID="ID_1778674692" MODIFIED="1437825883522" TEXT="Generar la interfaz de respuesta HTML req mucha codificaci&#xf3;n&#x2192; Sol: JSP">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1437822668883" ID="ID_443545027" MODIFIED="1437826945761">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      JSP
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437826185812" ID="ID_1268504194" MODIFIED="1437826270691">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Java Server Page) doc txt con <u>HTML y bloques</u>&#160;de c&#243;d embebido a ejec en serv (contenido din&#225;m)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1437826647590" ID="ID_1499317716" MODIFIED="1449824894773">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Tipo especial de Servlet especializado en construir la interfaz de <u>presentaci&#243;n</u>&#8594; <b>Vista</b>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
<node CREATED="1449824004230" ID="ID_1108448227" MODIFIED="1449824063145">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      paq javax.servlet.jsp
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1437826318851" ID="ID_721733463" MODIFIED="1437826675765">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contenedor busca Servlet en memo a partir de JSP y si no existe lo crea
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="idea"/>
</node>
<node CREATED="1437826700392" ID="ID_1272857176" MODIFIED="1449824134650">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>C&#243;digo JSP</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1449824136979" ID="ID_485574208" MODIFIED="1449824345157">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Varios lengs&#8594; HTML, XHTML, scripts (pe JavaScript), CSS... y&#160; <u>Java</u>&#160;(scriptlet&#160;embebido como &lt;%...%&gt;)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1437822696666" ID="ID_1010163616" MODIFIED="1449824919613" TEXT="JSF">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437827325870" ID="ID_1470624374" MODIFIED="1449824103811">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Java Serves Faces) variante de JSP para el desarrollo de<u>UI</u>&#160;basadas en<u>comps</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1437825061302" ID="ID_33391799" MODIFIED="1437838978307">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Cont EJB </i>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1437825053884" ID="ID_382094306" MODIFIED="1437827887794">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sw que gestiona ejec de <u>componentes EJB</u>&#160;(Enteprise Java Beans, puede haber varios)
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="info"/>
<node CREATED="1437827887796" ID="ID_776884801" MODIFIED="1449822311915">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Permiten escalabilidad
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1437822700106" ID="ID_274787665" MODIFIED="1449824950713" TEXT="EJB">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437827376167" ID="ID_150220749" MODIFIED="1437827420902">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Encapsulan la <u>l&#243;g de neg</u>&#160;de la app y se ejec de forma<u>distribuida</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1437825601315" ID="ID_1611185565" MODIFIED="1437827434130">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Resuelven peticiones e interact con SGBDR
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1437827475532" ID="ID_221676342" MODIFIED="1449826023658">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Session Beans</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1437827554955" ID="ID_371492227" MODIFIED="1449824912419">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Representan una <u>transac de cliente</u>&#160;en el lado del serv&#8594; &quot;fachada del <b>Modelo</b>&quot;
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1449824448484" ID="ID_822642970" MODIFIED="1449824477155">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      No persistentes y <u>s&#237;ncronos</u>&#8594; importa el t de resp (G2C, no puedes hacer esperar al ciudadano)
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1437827490170" ID="ID_396475194" MODIFIED="1449824999482">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Stateless (no diferencia sesiones, no se usa),&#160; <u>Stateful</u>&#160;(guardan estado user, si cae se puede retomar)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1437827613776" ID="ID_521512907" MODIFIED="1449826029581">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Message driven Beans</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1449824596486" ID="ID_1485320656" MODIFIED="1449824987753">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      =Session <u>as&#237;ncronos.</u>&#160;Ops costosas o t resp indet (G2G)&#8594; pe tx 1 millon de DNIs para verif
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1437827748697" ID="ID_1278569571" MODIFIED="1449826035182">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Entity Beans</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1449824639332" ID="ID_166543637" MODIFIED="1449825028446">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mapean de BD a memo del serv (hacen de<u>cach&#233;</u>) y aplican <u>algoritmo</u>&#160;o reglas de neg
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1449824737496" ID="ID_225420841" MODIFIED="1449824768005">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Persistencia</u>&#8594; gestionada por el propio bean (BMP) o por el contenedor (CMP, +usados)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1437825179895" ID="ID_891640511" MODIFIED="1437825182582" TEXT="Datos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437825182582" ID="ID_1476712948" MODIFIED="1449822413326">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      EIS (Enterprise Info Sys) = SGBDR + legacy (ERP, DW...)
    </p>
  </body>
</html></richcontent>
<node CREATED="1449822330434" ID="ID_665004995" MODIFIED="1449824829341">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Java solo aborda la capa/tier de presentaci&#243;n y app, no la de datos
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
<node CREATED="1437822713759" ID="ID_1673193690" MODIFIED="1437835883992" TEXT="Servicios">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437828174739" ID="ID_1635729448" MODIFIED="1437828206156">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>APIs</u>&#160;que implementan los Contenedores <u>Web y EJB</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1437835845544" ID="ID_1745655629" MODIFIED="1437835907398">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>JNDI: </i>(Naming &amp; Directory Interf) acceso a servs directorio (LDAP) y de nombres (DNS)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1437835883983" ID="ID_1714458451" MODIFIED="1437835928720">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Otros relevantes
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1437828232592" ID="ID_869303454" MODIFIED="1449823300213">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      HTTP/S, JTA (transaccs), JMT (mail), JAF (Activ FW, id tipo datos), JCA (Conector entre difs SI), JMS (msjs entre comps), JMX (gesti&#243;n disps, apps), JAAS (ctrl acceso), JACC (contrato serv-prov de autoriz), JAXR (intercamb de regs&#8594; WSDL/UDDI), JAX-RPC (SOAP-HTTP), SAAJ (SOAP con adjuntos&#8594; recopila valores)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1437822759931" ID="ID_1347421494" MODIFIED="1449826214204" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Frameworks desarrollo
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1449825124915" ID="ID_627977294" MODIFIED="1449825136915" TEXT="Bloques de construcc sw para no empezar dsd 0">
<node CREATED="1449823349774" ID="ID_1305948011" MODIFIED="1449823421313">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Apache Struts (c&#243;d ab), Apache Tapestry, Spring (ab), Hibernate ORM (persist de datos), Maverick
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1449825253303" ID="ID_1669399596" MODIFIED="1449825511464" POSITION="right" TEXT="Comparativa">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1449825257575" ID="ID_1298773168" MODIFIED="1449825260334" TEXT=".NET">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1449825260338" ID="ID_1177963144" MODIFIED="1449825547507">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Producto, MS, .NET SDK, <u>CLR-IL </u>, ASP.NET, .NET comps, ADO.NET, Win/Web Forms, Visual Studio, ADSI
    </p>
  </body>
</html></richcontent>
<node CREATED="1449825448914" ID="ID_1986222804" MODIFIED="1449825581843">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      C#, VB, C++ y otros (no Java)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1449825344121" ID="ID_1366743902" MODIFIED="1449825351792">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Java EE
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1449825347421" ID="ID_1541309966" MODIFIED="1449825542495">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Est&#225;ndar, &gt;30 empresas, Java API,<u>JRE-Bytecode</u>, Servlets-JSP, EJB, JDBC, Swing, varias herrams, JNDI
    </p>
  </body>
</html></richcontent>
<node CREATED="1449825456426" ID="ID_112550016" MODIFIED="1449825457596" TEXT="Java"/>
</node>
</node>
</node>
</node>
</map>
