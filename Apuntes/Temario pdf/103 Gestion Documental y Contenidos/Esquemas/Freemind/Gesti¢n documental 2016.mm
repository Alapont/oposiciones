<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1444895392230" ID="ID_849053916" MODIFIED="1480709365448" TEXT="SGD">
<node CREATED="1444895415086" ID="ID_1465122124" MODIFIED="1444898169730" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Gesti&#243;n
    </p>
    <p>
      docum
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444895898426" ID="ID_248097301" MODIFIED="1444897758833">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#193;rea de gesti&#243;n de ctrl eficaz y sistem&#225;tico de creaci&#243;n, rx, mant, uso y dispo de docs
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1444895787183" ID="ID_1416708891" MODIFIED="1444895998983">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      M&#225;x <u>eficacia y rentab</u>&#160;en el tratam de docs
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
</node>
<node CREATED="1444897653690" ID="ID_424602832" MODIFIED="1444897735438">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Activs ordenadas, servs coherentes, pol&#237;ticas y decis docum, productiv,
    </p>
    <p>
      continuidad, reqs leg, apoyo en litigios, gesti&#243;n riesgos, memo corp
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1444896031991" ID="ID_323177070" MODIFIED="1448011072712">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>SGD</i>
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444897830743" ID="ID_646290603" MODIFIED="1444897830746">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Elems y sus rels para normalizar, ctrl y coord procs y activs de todo el CV de docs de una org
    </p>
  </body>
</html></richcontent>
<node CREATED="1444896178926" ID="ID_1532119728" MODIFIED="1444896250588">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Conserv</u>&#160;de atribs, origin, autent, integr y&#160; <u>org</u>&#160;de docs&#160;
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
</node>
</node>
<node CREATED="1444896815341" ID="ID_392284032" MODIFIED="1444899323806" TEXT="Funciones en la AP">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444896822762" ID="ID_1474251799" MODIFIED="1444896858732">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Creaci&#243;n, captura, ingreso, almacenam, conserv/archivo, eliminaci&#243;n/expurgo, difusi&#243;n, consulta&#160;&#160;&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1444896452227" ID="ID_1816556719" MODIFIED="1444897801895" TEXT="SW de GD">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444896255649" ID="ID_1987390692" MODIFIED="1444898218212">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Pgms de gesti&#243;n con biblios de&#160; <u>docs</u>&#160;estruct y comprimidos + <u>&#237;ndices</u>&#160;en BD para consulta r&#225;pida&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1444896385460" ID="ID_356757702" MODIFIED="1444896421063">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Almac seg, indiz y recup de contenidos
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="xmag"/>
</node>
<node CREATED="1444897929091" ID="ID_1029296846" MODIFIED="1444898227841">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Relaciona</u>&#160;docs y les da sem&#225;ntica com&#250;n, permite <u>b&#250;squeda</u>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444898001049" ID="ID_1374783483" MODIFIED="1444898073668" TEXT="Arq t&#xe9;cnica">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444898005518" ID="ID_1929290385" MODIFIED="1444898070273">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Herrams&#8594; escaneado, ctrl versiones (modifs), almac, indizaci&#243;n, acceso, WF, cumplim normas, b&#250;sq
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1444897963070" ID="ID_807890505" MODIFIED="1444898513326">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Herrams
    </p>
    <p>
      de b&#250;sq
    </p>
  </body>
</html></richcontent>
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444898096623" ID="ID_1340327100" MODIFIED="1444899558485">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Silencio doc: </i>docs en BD no recup (b&#250;sq espec&#237;fic/no adecuada)
    </p>
  </body>
</html></richcontent>
<node CREATED="1444898130747" ID="ID_744903271" MODIFIED="1444899568567">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Ruido doc: </i>docs recup no relevantes (b&#250;sq gen&#233;rica)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444898518410" ID="ID_401574935" MODIFIED="1447951721810">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>&#205;ndices:</i>&#160;t&#233;rminos <u>normalizados</u>&#160;q representan contenido (de materias, alfab)
    </p>
  </body>
</html></richcontent>
<node CREATED="1444898552135" ID="ID_1446697459" MODIFIED="1447951706878">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Keywords: </i>en <u>leng natural</u>&#160;(KWIC/OC-In/Out Context)&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444898574481" ID="ID_1018464248" MODIFIED="1444898647234">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Tesauros:</i>&#160;t&#233;rminos <u>ordenados</u>&#160;jer&#225;rq sobre un <u>&#225;mbito</u>&#160;con rels sem&#225;nticas
    </p>
  </body>
</html></richcontent>
<node CREATED="1444899230120" ID="ID_982434350" MODIFIED="1447951753204">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      pe&#160;PC&#8594; pgrmaci&#243;n&#8594; leng
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1444896441808" ID="ID_322014462" MODIFIED="1444896445889" TEXT="Est&#xe1;ndares">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444896632680" ID="ID_1142528691" MODIFIED="1448012309920">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i><u>MoReq</u>:</i>&#160;(UE) Modelo de Reqs para la org de edocs (sem&#225;ntica y metadatos)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1444896538794" ID="ID_977252514" MODIFIED="1444896562170">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>NTIs del ENI:</i>&#160;edoc, digitaliz, exped-e, firmaE, intermed, copiado y conversi&#243;n
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444896562977" ID="ID_1157879055" MODIFIED="1444997100923">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>ISO&#160;30300: </i>(antigua 15489) <u>SGD</u>: fundams y vocab com&#250;n (30301: reqs)
    </p>
  </body>
</html></richcontent>
<node CREATED="1444896677515" ID="ID_477668981" MODIFIED="1444996811909">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>ISO 14721:</i>&#160;<u>conserv</u>&#160;de edocs a largo plazo y s&#170; ab de info de archivo
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1444896904334" ID="ID_740794033" MODIFIED="1444896984301" TEXT="CV de docs">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444897361489" ID="ID_1529336917" MODIFIED="1444897618491">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      El CV debe ir <u>integrado</u>&#160;en la produc adm de la org&#8594;
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1444897472541" ID="ID_1309453891" MODIFIED="1448011232105">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Docs gestionados en los s&#170; q&#160; <u>automatizan</u>&#160;los proceds adm: Reg E/S, Tramitaci&#243;n y Archivo&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444897154252" ID="ID_935433474" MODIFIED="1444899703012">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Docum Activa:</i>&#160;(arch de oficina) docs se tramitan para resolver asunto. Valor <u>admin</u>, consulta muy freq
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1444897197979" ID="ID_55619838" MODIFIED="1444899648615">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Semiactiva:</i>&#160;(arch general) tr&#225;mite terminado. Consulta menor
    </p>
  </body>
</html></richcontent>
<node CREATED="1444897221138" ID="ID_869715505" MODIFIED="1444899684529">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Inactiva:</i>&#160;(arch hist&#243;rico) doc pierde su valor adm&#8594;&#160;<u>hist&#243;rico </u>. Consulta puntual (investig)
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1444896911246" ID="ID_1544783234" MODIFIED="1444901124983" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      S&#170; de Gesti&#243;n
    </p>
    <p>
      de Contenidos
    </p>
    <p>
      (CMS/SGC)
    </p>
  </body>
</html></richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444899378901" ID="ID_229705368" MODIFIED="1444899390406">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      ECM
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444899390995" ID="ID_369359790" MODIFIED="1448010344159">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (Empresarial) evol <u>SGC+SGD</u>&#160;(sol integral)&#8594; gesti&#243;n doc&#160; <u>desestructurada</u>&#160;en una org (txt, xls, img, xml)
    </p>
  </body>
</html></richcontent>
<node CREATED="1444899466957" ID="ID_181724401" MODIFIED="1448012370374">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Automatizan</u>&#160;&#160;CV del cont, cumple pol&#237;tica dl neg (permisos),
    </p>
    <p>
      tx docs por mail/fax/link, public en dif disps (m&#243;vil)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1444899728900" ID="ID_1414215723" MODIFIED="1448010469288">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>WCM (Web)</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1444899856037" ID="ID_1906650681" MODIFIED="1448010396557">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Separa&#160;contenido de <u>presentaci&#243;n</u>&#8594; descentraliza la public, m&#243;ds de gesti&#243;n y personaliz
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1444899931413" ID="ID_881818428" MODIFIED="1448010484115">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Contenidos&#160;en BD o estruct ficheros&#8594;&#160;WCM responde a solicitudes de p&#225;gs componiendo &#160; <u>plantillas</u>
    </p>
  </body>
</html></richcontent>
<node CREATED="1444899907255" ID="ID_1568095919" MODIFIED="1448010408031">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <u>Categoriza</u>&#160;con metadatos
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444900053249" ID="ID_28201292" MODIFIED="1448010454088">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Autores, publicadores (revisar y autorizan public), admins (proponen plantillas) y lectores (naveg)
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="group"/>
</node>
</node>
<node CREATED="1444901007155" ID="ID_297374656" MODIFIED="1448010738159">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Gestor de Portales</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1448010505654" ID="ID_1120970007" MODIFIED="1448010729435">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Plataf sw para construir sitios web sin c&#243;digo (con widgets/&#160; <u>portlets</u>)
    </p>
  </body>
</html></richcontent>
<node CREATED="1448010671688" ID="ID_983879145" MODIFIED="1448011135645">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Gestor Portales&#8594; present / CMS&#8594; l&#243;g neg / SGD&#8594; persist
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node CREATED="1448010509234" ID="ID_1728211384" MODIFIED="1448010529073">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Homogeneiza la imagen de todo el website corporativo
    </p>
  </body>
</html></richcontent>
<node CREATED="1444901058413" ID="ID_1290414433" MODIFIED="1448010591947">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Personaliz, autentic, <u>idiomas y disps</u>, CMS, integr cn otros websites y RRSS
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1444900164310" ID="ID_728444909" MODIFIED="1444900200265">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>DAM:</i>&#160;(Dig Asset Manag) CMS especializado en <u>multimedia</u>&#8594; metadatos y herrams espec&#237;ficos
    </p>
  </body>
</html></richcontent>
<node CREATED="1444900201628" ID="ID_110462040" MODIFIED="1448010643420">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      pe productoras de TV, YouTube
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444900219928" ID="ID_408625046" MODIFIED="1444900256155">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>RM: </i>(Records Manag) <u>trazado</u>&#160;sistem&#225;tico del CV de la docum dejando constancia (registros)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444900315261" ID="ID_1043443137" MODIFIED="1444900318321" TEXT="Est&#xe1;ndares">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444900318325" ID="ID_1350873168" MODIFIED="1447951836378">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>CMIS: </i>(Cont Manag&#160; <u>Interop</u>&#160;Serv, OASIS) permite acceder a SGCs indep de su implem con WS/REST
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1444900822334" ID="ID_731232714" MODIFIED="1444900854185">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>JCR-170:</i>&#160;(Cont Repository API Java) similar a CMIS
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444900454792" ID="ID_1531239776" MODIFIED="1444900768713">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>CIFS:</i>&#160;(Common Inet File Sys, MS) prot de capa app de acceso <u>compartido</u>&#160;a archivos
    </p>
  </body>
</html></richcontent>
<node CREATED="1444900889512" ID="ID_1866657320" MODIFIED="1444900919640">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>JSR-286: </i>(Java Portlet Spec) gestor de <u>portales</u>&#160;con WSRP
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444900769327" ID="ID_613894603" MODIFIED="1448010655249">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>WebDAV:</i>&#160;(Web Distrib Auth &amp; Vers, IETF) ext HTTP para <u>tratar</u>&#160;docs de un serv <u>remoto</u>
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
<node CREATED="1444900866614" ID="ID_1578284346" MODIFIED="1444900886330">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>iECM:&#160;</i>(interop, AIIM) SOA para ECM
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1444901174268" ID="ID_262636736" MODIFIED="1444901176733" TEXT="Procesos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444901259876" ID="ID_925175757" MODIFIED="1448011908644">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Captaci&#243;n (transf a formato &#250;nico: XML)&#8594; Categ (metadatos)&#8594; Redacc (conts propios)&#8594;
    </p>
  </body>
</html></richcontent>
<node CREATED="1448011908649" ID="ID_1929549086" MODIFIED="1448011920317">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Edici&#243;n (img, traduc)&#8594; Distrib (sindic a suscriptores en&#160; <u>XML/ICE</u>&#160;)&#8594;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1448011892583" ID="ID_1251120112" MODIFIED="1448011940674">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Composic (preview)&#8594; Public (plantillas/XSL, MCMC:&#160; <u>multimodo</u>&#160;html, vxml...)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444901177270" ID="ID_1328354332" MODIFIED="1444902676061">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Funcs
    </p>
  </body>
</html></richcontent>
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444902173106" ID="ID_1291656751" MODIFIED="1448010916262">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Catalog:</i>&#160;<u>CIS</u>&#160;(Cont Intellig Serv, de Documentum) analiza text y clasifica autom (genera metadatos)&#160;
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1444902367487" ID="ID_1863318050" MODIFIED="1448010959201">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Presentaci&#243;n: </i>formatea cont de la BD mediante metadatos, atribs, reglas y plantillas XSL
    </p>
  </body>
</html></richcontent>
<node CREATED="1448010961249" ID="ID_971305335" MODIFIED="1448012452427">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Personalizaci&#243;n:</i>&#160;<u>localiz</u>&#8594; moneda e idioma seg&#250;n IP
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444902508074" ID="ID_793141638" MODIFIED="1448010996298">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Sindicaci&#243;n</i>
    </p>
  </body>
</html></richcontent>
<node CREATED="1448010996303" ID="ID_1816030588" MODIFIED="1448010996306">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Provisi&#243;n de cont para ser reut/integr en formato est&#225;ndar (SOAP,XML,RSS)&#160;
    </p>
  </body>
</html></richcontent>
<node CREATED="1444902715185" ID="ID_1792459698" MODIFIED="1448011043666">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Agreg:</i>&#160;web redistrib conts de otros proveeds adaptados a clientes
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444902597832" ID="ID_718776652" MODIFIED="1448011465050">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>RSS: </i>(Rich Site Summay 1.0 / Really Simple Sindic 2.0) archivo .rss o .xml&#8594; txt con&#160;&#160; <u>refs</u>&#160;a conts web
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1444901188564" ID="ID_1665906625" MODIFIED="1444901195983" TEXT="Arq l&#xf3;g">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444902818987" ID="ID_591290584" MODIFIED="1444902868340">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>SGC:</i>&#160;framework ppal (n&#250;cleo)
    </p>
  </body>
</html></richcontent>
<node CREATED="1444902872969" ID="ID_161685832" MODIFIED="1444902913226">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Captador de conts, gesti&#243;n usuarios, gestor &#225;rbol naveg, WF, editor, preview y gestor distrib
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444902939027" ID="ID_1623418904" MODIFIED="1444902963978">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Serv de Contenidos:</i>&#160;<u>obtiene conts</u>&#160;req seg&#250;n pol&#237;ticas de cach&#233; y personaliz y los pasa a Presentaci&#243;n
    </p>
  </body>
</html></richcontent>
<node CREATED="1444903075543" ID="ID_843475626" MODIFIED="1444903148566">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Buscador de Contenidos:</i>&#160;API de <u>acceso</u>&#160;a la BD de conts
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444902969563" ID="ID_237647188" MODIFIED="1444903156645">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Gestor cach&#233;:</i>&#160;<u>libera</u>&#160;al Serv de Conts de obtener contenidos actualizados
    </p>
  </body>
</html></richcontent>
<node CREATED="1444903097886" ID="ID_524042895" MODIFIED="1444903119041">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Serv de Presentaci&#243;n:</i>&#160;<u>ajusta</u>&#160;conts a cada disp
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1444903159381" ID="ID_1568399152" MODIFIED="1447951862575">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Personaliz:</i>&#160;m&#243;d <u>indep</u>, ofrece al usuario lo q quiere ver&#8594;
    </p>
  </body>
</html></richcontent>
<node CREATED="1444903187693" ID="ID_718255337" MODIFIED="1444903256368">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Serv de pers, serv de observ, segment, reglas, motor de recos, informes, datos de pers
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1444901203311" ID="ID_1393265023" MODIFIED="1444901206239" TEXT="Productos">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444903275628" ID="ID_324158778" MODIFIED="1444903279548" TEXT="Opensource">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444903283549" ID="ID_1939322883" MODIFIED="1447952251822">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Drupal, WordPress, Joomla, <u>Alfresco</u>, OpenCMS, Zope, Plone, Mambo, Diferior, Symphony
    </p>
  </body>
</html></richcontent>
<node CREATED="1444987596267" ID="ID_1634362336" MODIFIED="1447952253728" TEXT="LIFERAY (portales)"/>
</node>
</node>
<node CREATED="1444903280149" ID="ID_1751921505" MODIFIED="1444903282883" TEXT="Comerciales">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1444903347057" ID="ID_916657922" MODIFIED="1448010934463">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      IBM FileNet, EMC <u>Documentum </u>, Vignette, OpenText, Sharepoint
    </p>
  </body>
</html></richcontent>
<node CREATED="1447952311469" ID="ID_1842350381" MODIFIED="1447952324233" TEXT="SAP Netw Portal, Oracle Portal, Red Hat JBoss (portales)"/>
</node>
</node>
</node>
</node>
</node>
</map>
