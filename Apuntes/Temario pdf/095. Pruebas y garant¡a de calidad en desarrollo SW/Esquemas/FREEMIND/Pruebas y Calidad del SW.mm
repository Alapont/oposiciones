<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1481290314409" ID="ID_782657610" MODIFIED="1481290381116" TEXT="Pruebas y Calidad del SW">
<node CREATED="1481290330825" ID="ID_1932499942" MODIFIED="1481290339128" POSITION="right" TEXT="Niveles de pruebas">
<node CREATED="1481290556320" ID="ID_1184291718" MODIFIED="1481290931608" TEXT="Pruebas unitarias &#xa;Pruebas de integraci&#xf3;n &#xa;Pruebas de sistema &#xa;Prueba de aceptaci&#xf3;n "/>
</node>
<node CREATED="1481290342124" ID="ID_1689439583" MODIFIED="1481290463388" POSITION="left" TEXT="Herramientas">
<node CREATED="1481290471130" ID="ID_351452269" MODIFIED="1557416773841" TEXT="Selenium&#xa;Firebug &#xa;Notepad++ &#xa;SoapUI &#xa;Wireshark &#xa;JMeter / Badboy Software&#xa;Cobertura / Emma&#xa;SonarQube &#xa;Beyond Compare &#xa;Cucumber &#xa;Fitnesse"/>
</node>
<node CREATED="1481290350099" ID="ID_412558702" MODIFIED="1481290354528" POSITION="right" TEXT="Estrategias">
<node CREATED="1481290662850" ID="ID_568958979" MODIFIED="1481290894136" TEXT="Estrategias Bottom-up y Top-down &#xa;Pruebas de interfaces &#xa;Pruebas de caja negra y caja blanca &#xa;Desarrollo de pilotos y empleo de metodolog&#xed;as &#xe1;giles y evolutivas.  "/>
</node>
<node CREATED="1481290383259" ID="ID_131047381" MODIFIED="1481290397041" POSITION="left" TEXT="T&#xe9;cnicas">
<node CREATED="1481290597491" ID="ID_1024993298" MODIFIED="1481290716769" TEXT="TDD &#xa;Metodolog&#xed;a orientada a comportamiento &#xa;Integraci&#xf3;n continua &#xa;Entrega continua &#xa;Definici&#xf3;n de casos l&#xed;mite "/>
</node>
<node CREATED="1481290433122" ID="ID_416861802" MODIFIED="1481290440417" POSITION="right" TEXT="Criterios de aceptaci&#xf3;n">
<node CREATED="1481290619661" ID="ID_1568398830" MODIFIED="1481290858929" TEXT="El criterio de aceptaci&#xf3;n es el criterio por el que se definen las caracter&#xed;sticas que por las que se eval&#xfa;an que los requisitos del software son satisfechos. En el caso de metodolog&#xed;as &#xe1;giles se define una historia de usuario. Dentro de la metodolog&#xed;a &#xe1;gil los criterios de finalizaci&#xf3;n de una tarea se definen como DoD.  "/>
<node CREATED="1481290625354" ID="ID_889767497" MODIFIED="1481290843601" TEXT="SMART: Specific (Especifico), Measurable (Medible), Achievable (Alcanzable), Relevant (Relevante), Time-bound (Temporalmente limitado)."/>
<node CREATED="1481290639905" ID="ID_1963745951" MODIFIED="1481290827569" TEXT="Existen dos t&#xe9;cnicas para escribir criterios de aceptaci&#xf3;n:   &#xa;Comportamiento: Consiste en escribir condiciones evento, condici&#xf3;n, acci&#xf3;n como forma de describir las pruebas del c&#xf3;digo.  &#xa;Escenarios: Descripci&#xf3;n desde el punto de vista de usuario de un caso de &#xe9;xito concreto, y de los caminos alternativos. Es posible que involucre m&#xe1;s de un usuario, y que describa no solamente requerimientos funcionales, sino no funcionales y de rendimiento.  "/>
</node>
<node CREATED="1481290679714" ID="ID_852964384" MODIFIED="1481290682923" POSITION="left" TEXT="Planificacion">
<node CREATED="1481290686122" ID="ID_1449262426" MODIFIED="1481290774544" TEXT="Definici&#xf3;n de los criterios de aceptaci&#xf3;n y del dimensionamiento del equipo e infraestructura de pruebas Pruebas sobre criterios funcionales &#xa;Pruebas sobre criterios no funcionales &#xa;Pruebas est&#xe1;ticas y din&#xe1;micas &#xa;Proceso de Peer Review (revisi&#xf3;n entre pares)&#xa;Pruebas de regresi&#xf3;n. Gesti&#xf3;n de configuraci&#xf3;n y control en la introducci&#xf3;n de defectos.  &#x9;&#xa;Definici&#xf3;n de m&#xe9;tricas y herramientas para la realizaci&#xf3;n de pruebas "/>
</node>
</node>
</map>
