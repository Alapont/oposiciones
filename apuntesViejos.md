# Oposiciones

Material para estudiar oposiciones del [Cuerpo Superior de Sistemas y Tecnologías de la Información de la Administración del Estado](https://www.boe.es/boe/dias/2020/07/31/pdfs/BOE-A-2020-8872.pdf) Basado en la convocatoria de 2020

# Indice
- [Oposiciones](#oposiciones)
- [Indice](#indice)
  - [Fuentes de apuntes](#fuentes-de-apuntes)
- [Temario](#temario)
  - [A. Temas generales](#a-temas-generales)
    - [I. Marco constitucional español y Unión Europea](#i-marco-constitucional-español-y-unión-europea)
    - [II. Actuación administrativa y gestión financiera](#ii-actuación-administrativa-y-gestión-financiera)
    - [III. Estructura social y económica de España](#iii-estructura-social-y-económica-de-españa)
    - [IV. Dirección pública](#iv-dirección-pública)
  - [B. Temas específicos](#b-temas-específicos)
      - [I. Organización y gestión de los sistemas de información](#i-organización-y-gestión-de-los-sistemas-de-información)
      - [II. Tecnología básica](#ii-tecnología-básica)
      - [III. Ingeniería de los sistemas de información](#iii-ingeniería-de-los-sistemas-de-información)
      - [IV. Redes, comunicaciones e Internet](#iv-redes-comunicaciones-e-internet)
- [Apuntes](#apuntes)
  - [A. Temas generales](#a-temas-generales-1)
    - [I. Marco constitucional español y Unión Europea](#i-marco-constitucional-español-y-unión-europea-1)
      - [1. La constitución Española de 1978 (I). Los principios constitucionales. Los derechos fundamentales y sus garantías. La Corona. Cortes Generales. Congreso de los diputados y Senado. El gobierno. Los órganos constitucionales de control del Gobierno: Tribunal de Cuentas, Defensor del Pueblo. La función consultiva: el Consejo de Estado](#1-la-constitución-española-de-1978-i-los-principios-constitucionales-los-derechos-fundamentales-y-sus-garantías-la-corona-cortes-generales-congreso-de-los-diputados-y-senado-el-gobierno-los-órganos-constitucionales-de-control-del-gobierno-tribunal-de-cuentas-defensor-del-pueblo-la-función-consultiva-el-consejo-de-estado)
        - [Principios constitucionales](#principios-constitucionales)
        - [Derechos y deberes fundamentales](#derechos-y-deberes-fundamentales)
        - [Instituciones del estado](#instituciones-del-estado)
          - [Corona](#corona)
          - [Cortes Generales](#cortes-generales)
          - [Gobierno](#gobierno)
          - [Tribunal de cuentas](#tribunal-de-cuentas)
          - [Defensor del pueblo](#defensor-del-pueblo)
          - [Consejo de Estado](#consejo-de-estado)
        - [Tipos de leyes](#tipos-de-leyes)
        - [Elaboración de las leyes](#elaboración-de-las-leyes)
          - [Fases](#fases)
      - [La Constitución Española de 1978 (II). El Poder Judicial. La justicia en la Constitución. El Consejo General del Poder Judicial. El Ministerio Fiscal. El Tribunal Constitucional. Naturaleza, organización y atribuciones.](#la-constitución-española-de-1978-ii-el-poder-judicial-la-justicia-en-la-constitución-el-consejo-general-del-poder-judicial-el-ministerio-fiscal-el-tribunal-constitucional-naturaleza-organización-y-atribuciones)
        - [El poder judicial](#el-poder-judicial)
          - [Independencia](#independencia)
          - [Unidad jurisdiccional](#unidad-jurisdiccional)
          - [Exclusividad jurisdiccional](#exclusividad-jurisdiccional)
          - [Participación popular: el jurado](#participación-popular-el-jurado)
          - [Otros principios](#otros-principios)
          - [Organización territorial](#organización-territorial)
        - [El Consejo General del Poder Judicial](#el-consejo-general-del-poder-judicial)
          - [Organización](#organización)
        - [El ministerio Fiscal](#el-ministerio-fiscal)
        - [Tribunal Constitucional](#tribunal-constitucional)
          - [Composición](#composición)
  - [B. Temas específicos](#b-temas-específicos-1)
    - [I. Organización y gestión de los sistemas de la información](#i-organización-y-gestión-de-los-sistemas-de-la-información)
      - [44. Accesibilidad y usabilidad W3C. Diseño universal. Diseño web adaptativo](#44-accesibilidad-y-usabilidad-w3c-diseño-universal-diseño-web-adaptativo)
        - [Accesibilidad](#accesibilidad)
        - [Usabilidad](#usabilidad)
        - [W3C](#w3c)
        - [WAI](#wai)
        - [Web Content ACcessibility Guidelines](#web-content-accessibility-guidelines)
        - [Conformidad](#conformidad)
        - [Diseño](#diseño)
        - [Normativa](#normativa)
- [Glosario](#glosario)
- [Siglas](#siglas)
- [Leyes y normas](#leyes-y-normas)


## Fuentes de apuntes
    [astic](https://astic.es/)
    [preparatic](http://www.preparatic.org/)

# Temario
## A. Temas generales
### I. Marco constitucional español y Unión Europea
1. La Constitución Española de 1978 (I). Los principios constitucionales. Los derechos fundamentales y sus garantías. La Corona. Cortes Generales. Congreso de los Diputados y Senado. El Gobierno. Los órganos constitucionales de control del Gobierno: Tribunal de Cuentas, Defensor del Pueblo. La función consultiva: el Consejo de Estado.
2. La Constitución Española de 1978 (II). El Poder Judicial. La justicia en la Constitución. El Consejo General del Poder Judicial. El Ministerio Fiscal. El Tribunal Constitucional. Naturaleza, organización y atribuciones. 
3. La Constitución Española de 1978 (III). Las Comunidades Autónomas: organización política y administrativa. La Administración local: regulación constitucional y entidades que la integran.
4. La Constitución Española de 1978 (IV). La Administración pública: Principios constitucionales. La Administración General del Estado. Sus órganos centrales. 
5. La Unión Europea: antecedentes, evolución y objetivos. Los tratados originarios y modificativos. El derecho de la Unión Europea. Relación entre el derecho de la Unión Europea y el ordenamiento jurídico de los Estados miembros. Las instituciones: el Consejo Europeo, el Consejo, la Comisión Europea, el Parlamento Europeo el Tribunal de Justicia de la Unión Europea, el Tribunal de Cuentas y el Banco Central Europeo.
6. La organización territorial de la Administración General del Estado. Delegados y subdelegados del Gobierno. Organización de los servicios periféricos. El sector público institucional. Organización, funcionamiento y principios generales de actuación. Los organismos y entidades públicas estatales. 
### II. Actuación administrativa y gestión financiera
7. Las fuentes del derecho administrativo. La jerarquía de las fuentes. La ley. Las disposiciones del Gobierno con fuerza de ley: decreto-ley y decreto legislativo. El reglamento: concepto, clases y límites. Otras fuentes del derecho administrativo.
8. El régimen jurídico de las Administraciones públicas. El procedimiento administrativo y la relación de los ciudadanos con las Administraciones públicas.
9.  La revisión de los actos en vía administrativa. Los recursos administrativos. El recurso contencioso-administrativo.
10. La responsabilidad patrimonial de las Administraciones públicas. Procedimiento de responsabilidad patrimonial. La acción de responsabilidad.
11. Los contratos de las Administraciones públicas. Principios comunes. La Ley de Contratos del Sector Público. Requisitos necesarios para la celebración de los contratos. Procedimientos de contratación y formas de adjudicación. Tipos de contratos y características generales. La facturación en el sector público.
12. Los presupuestos generales del Estado. Estructura del presupuesto: clasificación de los gastos. Las modificaciones presupuestarias. La ejecución del gasto y su control interno: la Intervención General de la Administración del Estado. El control externo: el Tribunal de Cuentas y las Cortes Generales.
13. Los convenios. Las encomiendas de gestión o encargos a medios propios personificados. Límites en su utilización. La colaboración público-privada en la prestación de los servicios públicos. El régimen jurídico de las subvenciones públicas. Procedimiento. 
### III. Estructura social y económica de España
14. El modelo económico español en el marco de la economía mundial. El crecimiento sostenible y las políticas ambientales. La Agenda 2030 y los Objetivos de Desarrollo Sostenible. Líneas generales de la política económica actual. La renta nacional. Evolución y distribución. La internacionalización  de la economía española. Comercio exterior y balanza de pagos.
15. El sector primario. El sector industrial. El sector servicios. Los subsectores de comercio, transportes y comunicaciones. El turismo.
16. La estructura social. Tendencias demográficas. Políticas de igualdad de género. Discapacidad y dependencia. Políticas contra la violencia de género.
17. El mercado de trabajo: evolución y características actuales. Principales magnitudes. Políticas públicas de empleo.
18. El impacto de las tecnologías de la información y las comunicaciones en la sociedad. La brecha digital. Nuevos hábitos de relación y de consumo de la información.
19. El impacto de las tecnologías de la información y las comunicaciones en la economía y en el mercado de trabajo. Principales magnitudes.
### IV. Dirección pública
20. La función gerencial en las Administraciones públicas. Particularidades de la gestión pública.
21. Liderazgo. Gestión de competencias y personas. Gestión de conflictos e inteligencia emocional en el puesto de trabajo. Técnicas de negociación y gestión comercial.
22. La calidad en los servicios públicos y el papel de las TIC en su modernización. La legislación en materia de sociedad de la información y administración electrónica en España y Europa.
23. La crisis de la burocracia como sistema de gestión. La adecuación de técnicas del sector privado. El «management» público y la gobernanza. Hacia la excelencia en las instituciones públicas.
24. Empleo Público en España. Tipología del empleo público. Estatuto Básico del Empleado Público. Planificación de recursos humanos en las Administraciones Públicas: la oferta de empleo público. Ley de Incompatibilidades del personal al servicio de las Administraciones Públicas.
25. La gobernanza pública y el gobierno abierto. Concepto y principios informadores del gobierno abierto: colaboración, participación, transparencia y rendición de cuentas. Datos abiertos y reutilización. El marco jurídico y los planes de gobierno abierto en España. La Ley 19/2013, de 9 de diciembre, de transparencia, acceso a la información pública y buen gobierno. El Consejo de Transparencia y Buen Gobierno: Real Decreto 919/2014, de 31 de octubre, por el que se aprueba su estatuto. Funciones. La Oficina de Transparencia y Acceso a la Información (OTAI): Funciones. El Portal de Transparencia. Las Unidades de Información y Transparencia (UITS): Funciones. La transparencia y el acceso a la información en las comunidades autónomas y entidades locales.
26. La dinamización y el apoyo a la actividad económica y al emprendimiento. La garantía de la unidad de mercado.
27. La política de protección de datos de carácter personal. Régimen jurídico. El Reglamento UE 2016/679, de 27 de abril, relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales y a la libre circulación de estos datos. Principios y derechos. Obligaciones. El Delegado de Protección de Datos en las Administraciones Públicas. La Agencia Española de Protección de Datos.
## B. Temas específicos
#### I. Organización y gestión de los sistemas de información
28. Definición, estructura, y dimensionamiento eficiente de los sistemas de información.
29. La información en las organizaciones. Las organizaciones basadas en la información. La Administración como caso específico de este tipo de organización.
30. Modelos de gobernanza TIC. Organización e instrumentos operativos de las tecnologías de la información y las comunicaciones en la Administración General del Estado y sus organismos públicos. La transformación digital de la Administración General del Estado.
31. Reutilización de la información en el sector público en Europa y España. Papel de las TIC en la implantación de políticas de datos abiertos y transparencia.
32. Estrategia, objetivos y funciones del directivo de sistemas y tecnologías de la información en la Administración.
33. Herramientas de planificación y control de gestión de la función del directivo de sistemas y tecnologías de la información en la Administración. El cuadro de mando.
34. Organización y funcionamiento de un centro de sistemas de información. Funciones de desarrollo, mantenimiento, sistemas, bases de datos, comunicaciones, seguridad, calidad, microinformática y atención a usuarios.
35. Dirección y gestión de proyectos de tecnologías de la información. Planificación estratégica, gestión de recursos, seguimiento de proyectos, toma de decisiones.
36. Metodologías predictivas para la gestión de proyectos: GANTT, PERT.
37. Metodologías ágiles para la gestión de proyectos. Metodologías lean.
37. Auditoría informática. Concepto y contenidos. Administración, planeamiento, organización, infraestructura técnica y prácticas operativas.
39. La gestión de la compra pública de tecnologías de la información.
40. Adquisición de sistemas: estudio de alternativas, evaluación de la viabilidad y toma de decisión.
41. Alternativas básicas de decisión en el campo del equipamiento hardware y software.
42. La rentabilidad de las inversiones en los proyectos de tecnologías de la información.
43. La protección jurídica de los programas de ordenador. Los medios de comprobación de la legalidad y control del software.
44. Accesibilidad y usabilidad. W3C. Diseño universal. Diseño web adaptativo. 
45. Interoperabilidad de sistemas (1). El Esquema Nacional de Interoperabilidad. Dimensiones de la interoperabilidad.
46. Interoperabilidad de sistemas (2). Las Normas Técnicas de Interoperabilidad. Interoperabilidad de los documentos y expedientes electrónicos y normas para el intercambio de datos entre Administraciones Públicas.
47. Seguridad de sistemas (1). Análisis y gestión de riesgos. Herramientas.
48. Seguridad de sistemas (2). El Esquema Nacional de Seguridad. Adecuación al Esquema Nacional de Seguridad. Estrategia Nacional de Seguridad. CCN-STIC.
49. Infraestructuras, servicios comunes y compartidos para la interoperabilidad entre Administraciones públicas. Cl@ve, la carpeta ciudadana, el Sistema de Interconexión de Registros, la Plataforma de Intermediación de Datos, y otros servicios.
50. Organizaciones internacionales y nacionales de normalización. Pruebas de conformidad y certificación. El establecimiento de servicios de pruebas de conformidad.
51. Planes y Actuaciones de la Agenda Digital para España. Descripción, estructura y objetivos de los planes. El Mercado Único Digital.
#### II. Tecnología básica
52. Sistemas de altas prestaciones. Grid Computing. Mainframe.
53. Equipos departamentales. Servidores. Medidas de seguridad para equipos departamentales y servidores. Centros de proceso de datos: diseño, implantación y gestión.
54. Dispositivos personales de PC y dispositivos móviles. La conectividad de los dispositivos personales. Medidas de seguridad y gestión para equipos personales y dispositivos móviles.
55. Cloud Computing. IaaS, PaaS, SaaS. Nubes privadas, públicas e híbridas.
56. Sistemas de almacenamiento para sistemas grandes y departamentales. Dispositivos para tratamiento de información multimedia. Virtualización del almacenamiento. Copias de seguridad.
57. Tipos de sistemas de información multiusuario. Sistemas grandes, medios y pequeños. Servidores de datos y de aplicaciones. Virtualización de servidores.
58. El procesamiento cooperativo y la arquitectura cliente-servidor. Arquitectura SOA.
59. Conceptos y fundamentos de sistemas operativos. Evolución y tendencias.
60. Sistemas operativos UNIX-LINUX. Fundamentos, administración, instalación, gestión.
61. Sistemas operativos Microsoft. Fundamentos, administración, instalación, gestión.
62. Conceptos básicos de otros sistemas operativos: OS X, iOS, Android, z/OS. Sistemas operativos para dispositivos móviles.
63. Los sistemas de gestión de bases de datos SGBD. El modelo de referencia de ANSI.
64. El modelo relacional. El lenguaje SQL. Normas y estándares para la interoperabilidad entre gestores de bases de datos relacionales.
65. Arquitectura de desarrollo en la web. Desarrollo web front-end. Scripts de cliente. Frameworks. UX. Desarrollo web en servidor, conexión a bases de datos e interconexión con sistemas y servicios.
66. Entorno de desarrollo Microsoft.NET.
67. Entorno de desarrollo JAVA.
68. Entorno de desarrollo PHP.
69. Software de código abierto. Software libre. Conceptos base. Aplicaciones en entorno ofimático y servidores web.
70. Inteligencia artificial: la orientación heurística, inteligencia artificial distribuida, agentes inteligentes.
71. Gestión del conocimiento: representación del conocimiento. Sistemas expertos. Herramientas.
72. Sistemas CRM (Customer Relationship Management) y ERP (Enterprise Resource Planning). Generación de informes a la dirección.
73. E-learning: conceptos, herramientas, sistemas de implantación y normalización. 
74. Los sistemas de información geográfica. Conceptos y funcionalidad básicos.
75. Gestión de los datos corporativos. Almacén de datos (Data-Warehouse). Arquitectura OLAP. Minería de datos.
76. Big Data. Captura, análisis, transformación, almacenamiento y explotación de conjuntos masivos de datos. Entornos Hadoop o similares. Bases de datos NoSQL.
77. Lenguajes y herramientas para la utilización de redes globales. HTML, CSS y XML. Navegadores web y compatibilidad con estándares.
78. Comercio electrónico. Mecanismos de pago. Gestión del negocio. Factura electrónica. Pasarelas de pago.
79. El cifrado. Algoritmos de cifrado simétricos y asimétricos. La función hash. El notariado.
80. Identificación y firma electrónica (1) Marco europeo y nacional. Certificados digitales. Claves privadas, públicas y concertadas. Formatos de firma electrónica. Protocolos de directorio basados en LDAP y X.500. Otros servicios.
81. Identificación y firma electrónica (2) Prestación de servicios públicos y privados. Infraestructura de clave pública (PKI). Mecanismos de identificación y firma: «Smart Cards», DNI electrónico, mecanismos biométricos.
82. Adaptación de aplicaciones y entornos a los requisitos de la normativa de protección de datos según los niveles de seguridad. Herramientas de cifrado y auditoría.
83. El tratamiento de imágenes. Tecnologías de digitalización y de impresión. Impresión 3D.
84. Reconocimiento óptico de caracteres (OCR, ICR). Reconocimiento biométrico.
#### III. Ingeniería de los sistemas de información
85. El ciclo de vida de los sistemas de información. Modelos del ciclo de vida. 86. Planificación estratégica de sistemas de información y de comunicaciones. El plan de sistemas de información.
87. Análisis funcional de sistemas, casos de uso e historias de usuario. Metodologías de desarrollo de sistemas. Metodologías ágiles: Scrum y Kanban.
88. Análisis del dominio de los sistemas: modelado de dominio, modelo entidad relación y modelos de clases.
89. Análisis dinámico de sistemas: modelado de procesos, modelado dinámico y BPMN (Business Process Model and Notation).
90. Análisis de aspectos no funcionales: rendimiento, seguridad, privacidad.
91. Diseño arquitectónico de sistemas. Diagramas de despliegue.
92. Técnicas de diseño de software. Diseño por capas y patrones de diseño.
93. La elaboración de prototipos en el desarrollo de sistemas. Diseño de interfaces de aplicaciones.
94. La metodología de planificación y desarrollo de sistemas de información Métrica.
95. Procesos de pruebas y garantía de calidad en el desarrollo de software. Planificación, estrategia de pruebas y estándares. Niveles, técnicas y herramientas de pruebas de software. Criterios de aceptación de software.
96. Modelos de integración continua. Herramientas y sus aplicaciones.
97. Métricas y evaluación de la calidad del software. La implantación de la función de calidad.
98. La estimación de recursos y esfuerzo en el desarrollo de sistemas de información.
99. La migración de aplicaciones en el marco de procesos de ajuste dimensional y por obsolescencia técnica. Gestión de la configuración y de versiones. Gestión de entornos.
100. Mantenimiento de sistemas. Mantenimiento predictivo, adaptativo y correctivo. Planificación y gestión del mantenimiento.
101. Gestión de cambios en proyectos de desarrollo de software. Gestión de la configuración y de versiones. Gestión de entornos.
102. La calidad en los servicios de información. El Modelo EFQM y la Guía para los servicios ISO 9004.
103. Gestión documental. Gestión de contenidos. Tecnologías CMS y DMS de alta implantación.
104. Sistemas de recuperación de la información. Políticas, procedimientos y métodos para la conservación de la información.
105. Planificación y control de las TIC: gestión de servicios e infraestructuras TIC, gestión del valor de las TIC. Acuerdos de nivel de servicio. Gestión de incidencias. Bases conceptuales de ITIL (IT Infrastructure Library), y CoBIT (Control Objetives for Information and Related Technology), objetivos de control y métricas.
106. Las tecnologías emergentes. Concepto. Clasificación, aspectos jurídicos y aplicaciones.
#### IV. Redes, comunicaciones e Internet
107. Redes de telecomunicaciones. Conceptos. Medios de transmisión. Conmutación de circuitos y paquetes. Protocolos de encaminamiento. Infraestructuras de acceso. Interconexión de redes. Calidad de servicio.
108. La red Internet y los servicios básicos.
109. Sistemas de cableado y equipos de interconexión de redes.
110. El modelo de referencia de interconexión de sistemas abiertos (OSI) de ISO: arquitectura, capas, interfaces, protocolos, direccionamiento y encaminamiento.
111. Tecnologías de acceso: fibra (GPON, FTTH), móviles (LTE), inalámbrica. 
112. Redes de transporte: JDSxWDM, MPLS. Redes de agregación: ATM, CarrierEthernet-VPLS (H-VPLS).
113. Redes inalámbricas: el estándar IEEE 802.11.Características funcionales y técnicas. Sistemas de expansión del espectro. Sistemas de acceso. Autenticación. Modos de operación. Bluetooth. Seguridad, normativa reguladora.
114. Redes IP: arquitectura de redes, encaminamiento y calidad de servicio. Transición y convivencia IPv4 – IPv6. Funcionalidades específicas de IPv6.
115. Redes de nueva generación y servicios convergentes (NGN/IMS). VoIP, ToIP y comunicaciones unificadas.
116. La transformación digital e industria 4.0: ciudades inteligentes. Internet de las Cosas (IoT).
117. Redes de área local. Arquitectura. Tipología. Medios de transmisión. Métodos de acceso. Dispositivos de interconexión. Gestión de dispositivos. Administración de redes LAN. Gestión de usuarios en redes locales. Monitorización y control de tráfico. Gestión SNMP. Configuración y gestión de redes virtuales (VLAN). Redes de área extensa. 
118. Arquitectura de las redes Intranet y Extranet. Concepto, estructura y características. Su implantación en las organizaciones. Modelo de capas: servidores de aplicaciones, servidores de datos, granjas de servidores. 
119. Las redes públicas de transmisión de datos. La red SARA. La red sTESTA. Planificación y gestión de redes.
120. Telecomunicaciones por cable (CATV). Estructura de la red de cable. Operadores del mercado. Servicios de red.
121. El correo electrónico. Servicios de mensajería. Servicios de directorio.
122. Las comunicaciones móviles. Generaciones de tecnologías de telefonía móvil.
123. Aplicaciones móviles. Características, tecnologías, distribución y tendencias.
124. La seguridad en redes. Tipos de ataques y herramientas para su prevención: cortafuegos, control de accesos e intrusiones, técnicas criptográficas, etc. Medidas específicas para las comunicaciones móviles.
125. La seguridad en el nivel de aplicación. Tipos de ataques y protección de servicios web, bases de datos e interfaces de usuario.
126. Ciberseguridad. La estrategia nacional de ciberseguridad.
127. La gestión de la continuidad del negocio. Planes de Continuidad y Contingencia del Negocio.
128. Normas reguladoras de las telecomunicaciones. La Comisión Nacional de los Mercados y la Competencia  CNMC): organización, funciones y competencia en el ámbito de las telecomunicaciones.
129. Sistemas de videoconferencia. Herramientas de trabajo en grupo. Dimensionamiento y calidad de servicio en las comunicaciones y acondicionamiento de salas y equipos. Streaming de video.
130. Acceso remoto a sistemas corporativos: gestión de identidades, single sign-on y teletrabajo.
131. Virtualización de sistemas y de centros de datos. Virtualización de puestos de trabajo. Maquetas de terminales Windows y de servidores Linux.
132. Herramientas de trabajo colaborativo y redes sociales. La Guía de comunicación digital de la Administración del Estado.


# Apuntes
## A. Temas generales
### I. Marco constitucional español y Unión Europea
#### 1. La constitución Española de 1978 (I). Los principios constitucionales. Los derechos fundamentales y sus garantías. La Corona. Cortes Generales. Congreso de los diputados y Senado. El gobierno. Los órganos constitucionales de control del Gobierno: Tribunal de Cuentas, Defensor del Pueblo. La función consultiva: el Consejo de Estado

La [Constitución de 1978](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229): La constitución es la norma fundamental del estado, que plasma las reglas básicas de convivencia de una determinada comunidad política. Es por tanto la norma suprema de la que derivan todas las demás. Se compone de
- Preámbulo
- Título preliminar
- Titulo I de los derechos y deberes fundamentales
- Titulo II de la corona
- Titulo III de las cortes generales
- Titulo IV del gobierno y la administración
- Titulo V de las relaciones entre el Gobierno y las cortes generales
- Titulo VI del poder judicial
- Titulo VII Economía y hacienda
- Titulo VIII Organización territorial del estado
- Titulo IX Tribunal constitucional
- Titulo X De la reforma constitucional
- Disposiciones adicionales y transitorias: acerca de la cuestión territorial, disposición derogatoria, que deroga las leyes fundamentales del régimen anterior y disposición final.

> Se distingue entre la parte **dogmática** (Preámbulo, título preliminar y I) y la parte **orgánica**

- Es **rígida** porque el procedimiento de modificación es largo y complejo. Esto la dota de estabilidad. Ha habido dos cambios fundamentales:
  - 1992: Articulo 13.2 para cumplir el tratado de Maastricht
  - 2011: Articulo 135 por el principio de estabilidad presupuestaria
- Es integradora y consensuada
  - El consenso favoreció estabilidad política
  - Fue a costa de ambigüedad y poca concreción en temas controvertidos
- Constitucionaliza los derechos fundamentales y asegura su ejercicio
- Es **democrática** y **monárquica**
- Es **económica** y **social**
- Es una constitución derivada ya que sigue el constitucionalismo histórico español y el europeo, buscando la neutralidad
- Es norma jurídica, es decir, se integra en el sistema legal, siendo ley suprema
- Es fuente de derecho

El Título primero se compone de los capítulos
- De los españoles y los extranjeros
- Derechos y libertades
- De los principios rectores de la política social y económica
- De las garantías de las libertades y derechos fundamentales
- De la suspensión de derechos y libertades

##### Principios constitucionales
- **Art1** España es un estado social y democrático de Derecho, que propugna como valores superiores de su ordenamiento jurídico la libertad, la justicia, la igualdad y el pluralismo político. La soberanía nacional reside en el pueblo español, del que emanan los poderes del estado. La forma política del Estado español es la Monarquía parlamentaria.
- **Art2** La constitución se fundamenta en la indisoluble unidad de la Nación española, patria común e indivisible de todos los españoles y reconoce y garantiza el derecho a la autonomía de las nacionalidades y regiones que la integran y la solidaridad entre todas ellas
- **Art9**
  - **Art9.1** Los ciudadanos y los poderes públicos están sujetos a la Constitución y al resto del ordenamiento jurídico.
  - **Art9.2** (igualdad material) Corresponde a los poderes públicos promover las condiciones para que la libertad y la igualdad del individuo y de los grupos en que se integra sean reales y efectivas; remover los obstáculos que impidan o dificulten su plenitud y facilitar la participación de todos los ciudadanos en la vida política, cultural y social
  > enlaza con temas de participación ciudadana.

- **Art9.3** La Constitución garantiza el principio de legalidad, la jerarquía normática, la publicidad de las normas, la irretroactividad de las disposiciones sancionadoras no favorables o restrictivas de derechos individuales, la seguridad jurídica, la responsabilidad, la interdicción de la arbitrariedad de los poderes públicos.
> Enlaza con el tema de responsabilidad patrimonial

##### Derechos y deberes fundamentales
Están recogidos en el título I
Tipos de garantías
  - Normativas
    - Leyes á para artículos 14-29 con procedimiento especial de reforma constitucional
    - leyes ordinarias para el resto del capítulo
  - Jurisdiccionales
    - Recurso de amparo (al constitucional) por violación de derechos y libertades fundamentales o el art 30.2
Artículos importantes
- **Art14** (igualdad formal) los españoles son iguales ante la ley, sin que pueda prevalecer discriminación alguna por razón de nacimiento, raza, sexo, religión, opinión o cualquier otra condición o circunstancia personal o social
> Enlaza con el tema de igualdad

- **Art18** Derecho al honor, intimidad personal y familiar y a la propia imagen. Garantía del secreto de las comunicaciones, salvo resolución judicial. La ley limitará el uso de la informática para garantizar el honor y la intimidad personal.
  > Referencia a la informática en la constitución

- **Art23** Derecho a participar en los asuntos públicos y acceso en condiciones de igualdad a las funciones y cargos públicos
  > Enlaza con participación ciudadana
  > Enlaza con igualdad
- **Art4** Derecho a la tutela judicial efectiva
  > Enlaza con poder judicial
  - **Art27** Derecho a la educación
  > Enlaza con el estado de bienestar
  - **Art28.1** Todos tienen derecho a sindicarse libremente
  > Enlaza con el tema del mercado laboral
  - **Art29** Derecho de petición (individual y colectiva)
  > Enlaza con los temas de Procedimiento administrativo y participación ciudadana
  - **Art30.2** La ley fijara las obligaciones militares de los españoles y regulara, con las debidas garantías, la objeción de conciencia, así como las demás causas de exención del servicio militar obligatorio, pudiendo imponer, en su caso, una prestación social sustitutoria.
  - **Art31** deber de contribuir al sostenimiento de los gastos públicos
  > Enlaza con economía
  - **Art35** Deber de trabajar y derecho al trabajo
  > Enlaza con el tema del mercado laboral

 _Principios rectores de la política social y económica_
 - **Art40** Los poderes públicos promoverán las condiciones favorables para el progreso social y económico. De manera especial, realizaran una política orientada al pleno empleo.
  > Enlaza con el estado de bienestar y con el mercado laboral
  - **Art41** Los poderes públicos mantendrán un régimen público de Seguridad Social para todos los ciudadanos que garantice la asistencia y prestaciones suficientes ante las situaciones de necesidad, especialmente en caso de desempleo. 
    - Enlaza con Estado de Bienestar y  envejecimiento demográfico, reforma de pensiones y atención a la dependencia
    - Enlaza con mercado laboral
  - **Art43** Derecho a la protección de la salud
  > Enlaza con estado del Bienestar
##### Instituciones del estado
###### Corona
Se regula en el título II
Simboliza la unidad y permanencia del Estado ante la división de poderes. El rey es el jefe del estado, arbitra y modera el funcionamiento regular de las instituciones y asume la mas alta representación del Estado en las relaciones internacionales.
Tiene la competencia de sancionar y promulgar las leyes, disolver las Cortes Generales y convocar elecciones, el mando supremo de las Fuerzas Armadas, etc. La persona del rey es inviolable y no esta sujeta a responsabilidad.
Sus actos deben ser refrendados por el Presidente del Gobierno y, en su caso, los ministro competentes. De los actos del rey serán responsables las personas que los refrenden. Por tanto, la Corona es una institución con un carácter mas simbólico que efectivo.

###### Cortes Generales
Se regula en el título II
Las cortes representan al pueblo español y están formadas por Congreso y Senado. Ejercen la potestad legislativa, aprueban los presupuestos, controlan la acción de gobierno y tienen las demás competencias que le atribuya la constitución.
El pueblo español elige a sus diputados y senadores mediante sufragio universal, libre, igual, directo y secreto.
La Ley Orgánica de Régimen Electoral General (LOREG) 5/1985 establece el mecanismo de representación proporcional corregido dado por la fórmula D'Hondt y recoge el número definitivo de diputados: 350
Tienen una estructura bicameral, formada por **Congreso** y **Senado**. El Congreso ostenta la representación por excelencia del pueblo español, mientras que el Senado es la cámara de representación territorial.

###### Gobierno
Ostenta el poder ejecutivo
Según el Art97 a; gobierno le corresponde la dirección política interior y exterior, la administración civil y militar y la defensa del Estado. Además tiene la función ejecutiva y la potestad reglamentaria.
La Ley de Gobierno 50/1997 establece los siguientes principios
- principio de dirección presidencial
- principio de colegialidad
- principio departamental
El gobierno se compone del Presidente, el Vicepresidente o Vicepresidentes y los Ministros.
Los ministros desarrollan la acción del Gobierno en el ámbito de su departamento y ejercen la potestad reglamentaria dentro de las materias de su competencia.
El órgano mas destacado del gobierno es el Consejo de ministros, expresión colegiada del mismo.
Los secretarios de estado, son el órgano de colaboración del Gobierno mas importante, aunque no forman parte del mismo. Son órganos superiores de la AGE. Son directamente responsables de la ejecución de la acción del Gobierno, actuando bajo la dirección del departamento al que pertenecen
El gobierno realiza funciones políticas, legislativas y ejecutivas.
La actividad del gobierno se somete a un triple control
- Su actuación esta sujeta a la constitución y al resto del ordenamiento jurídico
- Sus actos son impugnables por la jurisdicción contencioso-administrativa
- Las cortes Generales ejercen el control político de sus actos

###### Tribunal de cuentas
Órgano fiscalizador de las cuentas y de la gestión económica del estado y el sector público
Depende de las Cortes Generales y ejerce sus funciones por control de las mismas.
Realiza dos funciones
- Fiscalización externa permanente y consultiva de la actividad económica y financiera del sector público.
- Jurisdicción y enjuiciamiento. Exigencia de responsabilidad en que pueden incurrir quienes tengan a su cargo el manejo de caudales y fondos públicos.

###### Defensor del pueblo
Su labor es fiscalizar en nombre del parlamento la actividad de la administración para defender los derechos fundamentales recogidos en el Título I
Puede interponer recursos de amparo y de inconstitucionalidad.

###### Consejo de Estado
Es el supremo órgano consultivo del Gobierno (art107)
Emite dictámenes a petición de la autoridad consultante. También elabora estudios, informes y memorias a petición del Presidente del Gobierno o de un Ministro.

##### Tipos de leyes
1. **Ley orgánica** Desarrollo de derechos fundamentales y libertades públicas, estatutos de autonomía, régimen electoral y demás previstas en la Constitución. Exigen mayoría absoluta del congreso. no es de mayor o menor rango que una ley ordinaria, símplemente regula determinadas materias
2. **Ley ordinaria** Emana de las cortes Generales y se aprueban por mayoría simple.
3. **Ley de pleno o de Comisiones** Se transfiere la competencia legislativa a la comisión. No pueden delegarse materias relativas a
   1. Reforma constitucional
   2. Asuntos internacionalesLey Orgánica o de Bases
   3. Presupuestos Generales del Estado
4. **Ley de bases** Ley ordinaria aprobada por las Cortes que contiene una autorización al Gobierno para que desarrolle esas bases en un texto articulado y lo apruebe por medio de un decreto legislativo
6. **Ley Marco** Son leyes ordinarias dictadas por las cortes generales en materias de competencia estatal. Por medio de estas leyes se atribuyen a todas o algunas CCAA la facultad de dictar, para si mismas, normal legislativas en el marco de los principios, bases y directrices fijados por ellas.
7. **Ley de armonización** Se trata de una ley estatal que es también ley ordinaria aunque debe adoptarse por las Cortes por mayoría absoluta en cada una de las cámaras, que deberán apreciar que concurre una exigencia del interés general. La finalidad es igualar normativas autonómicas que están tratando un mismo asunto de forma muy diferente. El contenido de la ley son una serie de principios que deberán seguir las Comunidades Autónomas para igualar sus legislaciones. Es importante resaltar que este tipo de leyes pueden dictarse incluso sobre materias que sean competencia de las CCAA.
8. **Especial** Los estatutos de autonomía se aprueban como Ley Orgánica
9.  **Decreto-ley** Emana del gobierno, no del parlamento. Se aprueba en casos de extraordinaria y urgente necesidad y posteriormente se someten al control parlamentario. No pueden afectar a determinadas materias:
    1.  ordenamiento de las instituciones básicas del estado
    2.  derechos, deberes y libertades fundamentales
    3.  regimen de las CCAA
    4.  derecho electoral
10. **Decreto legislativo** Emana del gobierno por delegación de las Cortes, para dictar textos articulados o refundidos. No pueden regular materias propias de una Ley Orgánica. La autorización se concede por medio de una ley ordinaria o de bases. Si es una ley ordinaria el Gobierno elaborará un texto refundido y lo aprobará por decreto legislativo. Si se hace por ley de Bases el resultado sera un texto articulado aprobado mediante decreto legislativo
11. **Ley autonómica** Emana de los Parlamentos Autonómicos. Solo puede ejercerse en los ámbitos de las competencias atribuidas a la CA. Salvo que desarrollen una ley de bases, ley marco o ley de armonización, no se encuentran subordinadas a las estatales.
13. **Tratados internacionales**
14. **Real decreto** Reglamento emitido por el consejo de ministros o presidente del gobierno. No tiene rango de ley
15. **Orden ministerial** Reglamento emitido por el Consejo de Ministro o Presidente del Gobierno. No tiene rango de ley

##### Elaboración de las leyes
El gobierno elabora **Proyectos de ley** y se aprueban por el consejo de ministros
Las **proposiciones de Ley** se pueden proponer por
- Congreso
  - 15 diputados
  - Grupo parlamentario
- Senado
  - 25 senadores
  - Grupo parlamentario
- Asambleas legislativas
  - Solicitando al gobierno la adopción de un proyecto de ley
  - remitiendo a la mesa del Congreso una proposición de ley delegando hasta 3 miembros para su defensa
  - El pueblo con 500.000 firmas
###### Fases
  1. **Congreso** La mesa ordena la publicación del proyecto, se abre el plazo de enmiendas (15 días) y se envía a la comisión correspondiente. Si las enmiendas a la totalidad son aprobadas, se devuelve a quien lo propuso, si son solo algunos artículos, se debaten y votan. Por último se delibera y se aprueba por el Pleno.
  2. **Senado** Se tramita en 2 meses o 20 días naturales si el proyecto es declarado urgente, o bien por el congreso o bien por el Gobierno. Si no se presentan enmiendas, finaliza la tramitación. Si se aprueba enmiendas se envía otra vez al congreso. La enmienda debe obtener mayoría simple en el Congreso para una ley Ordinaria o mayoría absoluta para una Ley Orgánica., para que se incorpore al Proyecto. Si se veta la ley, por mayoría absoluta en el Senado, se envía al Congreso.si el Congreso aprueba la ley por mayoría absoluta, finaliza la tramitación. También puede esperar 2 meses para aprobar con mayoría simple.
  3. **Sanción y Promulgación** Por el rey en 15 díás
  4. **Publicación en el BOE** También puede ser publicada en las lenguas de las CCAA
  5. **Entrada en vigor** en el plazo que indica la ley o 20 días si no se establece.




#### La Constitución Española de 1978 (II). El Poder Judicial. La justicia en la Constitución. El Consejo General del Poder Judicial. El Ministerio Fiscal. El Tribunal Constitucional. Naturaleza, organización y atribuciones.

##### El poder judicial
Se encarga de hacer cumplir las normas dictadas por el legislativo. Así como el legislativo y Ejecutivo están muy relacionados, el Judicial está separado. La justicia emana del pueblo

El título VI de la Constitución regula "Del poder judicial"
- La justicia emana del pueblo. _principio democrático_ Los jueces y magistrados la administran en nombre del rey, de forma independiente, inamovible, responsable y sometida únicamente a imperio de la ley.
- Los jueces y magistrados no podrán ser separados, suspendidos, trasladados o jubilados, si no por las causas y con las garantías provistas en la ley.
- La potestad jurisdiccional la ejercen exclusivamente juzgados y tribunales determinados por las leyes
- Los juzgados y tribunales ejercen la función anterior y la que les atribuyan las leyes
- Principio de unidad jurisdiccional, excepto jurisdicción militar y estado de sitio, según la constitución
- Se prohíben los tribunales de excepción.

###### Independencia
**Independencia**: total de los demás poderes, incluidos otros jueces. Solo se puede corregir mediante el mecanismo del recurso. No hay circulares internas ni instrucciones generales. También tienen disponibilidad de un cuerpo de policía dependiente de los jueces y magistrados, evitando que tengan que acudir al legislativo.
**Inamovilidad**: Los jueces no podrán ser separados, suspendidos, trasladados o jubilados, si no es por lo dispuesto en la ley. De esta manera se asegura que los jueces actúen libremente, sin riesgo de coacciones por parte del Ejecutivo / legislativo.
**Incompatibilidades y prohibiciones** Se prohíbe que los jueces y fiscales en activo desempeñen cargos públicos, pertenecer a partidos políticos o sindicatos. Su asociación  se regulara por ley. Se les declara incompatibles para ocupar escaños de diputados o senadores
- se busca conseguir que el juez tenga dedicación plena, por el interés público
  - jurisdicciones ajenas al Poder Judicial
  - cargos de elección popular o designación política
  - empleos o cargos retribuidos por la Administración del estado, cortes, Casa Real, CCAA, provincias, municipios, entidades dependientes
  - Empleo en los tribunales / juzgados de cualquier orden jurisdiccional
  - otros empleos /cargos/ profesiones, excepto docencia e investigación jurídica, producción artística o literaria
  - Ejercicio de la abogacía o Proceduría
  - Asesoramiento jurídico
  - actividades mercantiles y puestos de gerencia, dirección, administración, consejero, socio, o cualquier otro puesto que implique intervención en sociedades mercantiles.
- las prohibiciones son debido a los inconvenientes que tendría el juez en el ejercicio de derechos reconocidos a los ciudadanos.
  - Pertenencia o empleo por partidos políticos o sindicatos
  - envío de felicitaciones o censuras por sus actos a autoridades, cargos públicos, funcionarios, etc
  - asistir como miembros del poder judicial a actos públicos sin carácter judicial (excepto convocados por el Rey, el CGPJ)
  - solo pueden votar en elecciones
Se **autogobierna** desde el **Consejo General del Poder Judicial**.
La interpretación y aplicación de la ley se debe jacer según los preceptos constitucionales.

###### Unidad jurisdiccional
La unidad jurisdiccional tiene tres acepciones:
1. Monopolio en la aplicación del derecho: solo los órganos judiciales pueden aplicar las normas jurídicas a los litigios concretos. No pueden cumplir otra función
2. Resolución plena del asunto confiado a su competencia. Sin que puedan separarse elementos de la litis (incidentes o cuestiones previas o prejuiciales)
3. Inexistencia de especies de delito o personas cualificadas sustraibles a su jurisdicción

Se prohíben los tribunales de honor en la administración civil y organizaciones profesionales. También se prohíbe que las sanciones administrativas puedan consistir en privación de libertad.
Aunque hay especializaciones (civil, penal, laboral, contencioso-administrativa...) los jueces y magistrados constituyen un cuerpo único.

###### Exclusividad jurisdiccional
El ejercicio de la potestad jurisdiccional corresponde exclusivamente a los jueces. Cualquier otra investigación no es vinculante.

###### Participación popular: el jurado
El tribunal del jurado tiene competencia para enjuiciamiento de los siguientes delitos:
- Contra las personas
- Cometidos por funcionarios en ejercicio de sus cargos
- Contra el honor
- Contra la libertad y seguridad
- Incendios
- Homicidio
- Amenazas
- Omisión del deber de socorro
- Allanamiento de morada
- Incendios forestales
- Infidelidad en la custodia de documentos
- Cohecho
- Tráfico de influencias
- Malversación de caudales públicos
- Fraudes y exacciones ilegales
- Negociaciones prohibidas a funcionarios
- Infidelidad en la custodia de presos

###### Otros principios
- Gratuidad
- Publicidad
- Oralidad
- Obligatoriedad en el cumplimiento de sentencias y resoluciones

###### Organización territorial

| Territorio         | Juzgado |
|:-------------------|:--------|
| Nacional           | Tribunal supremo (civil, Penal, Contencioso-admin, Social, Militar)<br>Audiencia nacional (Apelación, Penal, Contencioso-admin, Social)<br>Juzgados centrales (Instrucción, Penal, Contencioso-admin, de Menores|
| Comunidad autónoma | Tribunal superior de justicia (Civil, penal, Contencioso-admin, Social)|
| Provincia          | Audiencia Provincial<br> Juzgados de lo Mercantil<br>Juzgados de lo Penal<br>Juzgados de lo Contencioso-administrativo<br>Juzgados de lo Social<br>Juzgados de Vigilancia Penitenciaria<br>Juzgados de Menores|
| Partido Judicial   | Juzgados de primera instancia e instrucción<br>Juzgados de Menores<br>Juzgados de Violencia sobre la mujer|
| Municipio          | Juzgado de paz, si no hay Juzgado de primera instancia e instrucción|

##### El Consejo General del Poder Judicial

Es el máximo órgano de gobierno del Poder Judicial. Le corresponden el ingreso, formación, promociones, disciplina de Jueces y Magistrados y la inspección de Jueces y Tribunales. No ejerce función jurisdiccional. Se forma por el presidente y 20 miembros, nombrados por el Rey para un periodo de 5 años

###### Organización
Se organiza según el libro VII de la Ley Orgánica 6/1985 e introducido por la LO 4/2013 del Poder Judicial

**Presidente**: Solo puede reelegirse una vez. Propuesto por el CGPJ por 3/5 y refrendado por el Presidente del gobierno en RD
Funciones:
- Representación del CGPJ
- Convocar y presidir el Pleno y la Comisión Permanente. Tiene voto de calidad en caso de empate
- Fija el orden del día de las reuniones del Pleno y Comisión Permanente.
- Propuestas en materia de competencias del Pleno y Comisión Permanente
- Someter al Pleno las propuestas de nombramiento de magistrados del Tribunal Supremo
- Propone nombramiento de ponencias para preparación de despachos
- Firma acuerdos del Pleno y Comisión Permanente
- Dirige los órganos técnicos del Consejo

Puede cesarse por
- Fin de mandato
- Renuncia
- A propuesta del pleno, por notoria incapacidad o incumplimiento grave de los deberes del cargo (por 3/5)

**Vocales**
Se eligen por mayoría cualificada de 3/5 de las cámaras según lo descrito en la LO 4/2013 Arts 566 y 557. De los 12 jueces en activo, 3 vienen del Tribunal Supremo, 3 con mas de 25 años de carrera judicial y 6 sin sujeción a antigüedad:

|                 | Entre jueces y magistrados | Entre juristas | Total |
|----------------:|:--------------------------:|:--------------:|:------|
| Por el congreso | 6                          | 4              | 10    |
| Por el senado   | 6                          | 4              | 10    |
| Total           | 12                         | 8              | 20    |

**Vicepresidente**: Propuesto por el pleno entre los Vocales y nombrado por el Rey. Sustituye al Presidente en caso de vacante, enfermedad, etc.

**Pleno**: Se reúne convocado por el Presidente según lo indique el Reglamento o de manera extraordinaria cuando lo soliciten 5 miembros, hay Quorum con 13 miembros + {presidente, vicepresidente}

Competencias:
- Nombramientos (mayoría 3/5)
  - Presidente del TS
  - Miembros del TC
  - Presidentes de Sala y Magistrados del TS
  - Presidente de los Tribunales Superiores de Justicia de las CCAA
  - Magistrados de la Sala Segunda (lo Penal) y tercera (lo contencioso-administrativo) del TS
- conocer de la autorización de las actividades del CNI en referencia a los derechos de los artículos 18.2 y 18.3 de la Constitución (honor e inviolabilidad del domicilio y comunicaciones)
- Propuesta de nombramiento de los demás cargos de designación discrecional
- Art 124.4 sobre el nombramiento del Fiscal general del estado
- Recursos de alzada contra acuerdos de la comisión permanente, disciplinaria y las Salas de Gobierno de los Tribunales de Justicia y los órganos de gobierno de Tribunales y Juzgados
- Resolver expediente de rehabilitación de la Comisión Disciplinaria
- Informes de ejercicio de la potestad atribuida al CGPJ
- Acordar la separación y jubilación de Jueces
- Elegir y nombrar vocales de de las Comisiones y Delegaciones
- Aprobar la memoria Anual sobre el estado de la administración de justicia
- Elaborar los presupuestos del CGPJ a integrar con los PGE y dirigirlos

**Comisión permanente**: Formada por 4 vocales de carrera, 3 juristas y el presidente del CGPJ. Se encarga de
- Preparar sesiones del pleno
- Velar por la ejecución de los acuerdos
- Resolver concesión de licencias
- Autorizar escalafón de la carrera judicial
- Competencias delegadas por el pleno

Existen otras comisiones:
- **Disciplinaria** Instrucción de expedientes e imposiciones de sanciones a Jueces y Magistrados (4jueces, 3 no-jueces)
- **Asuntos económicos** (3 vocales)
- **Igualdad** Asesora al pleno sobre medidas necesarias para integrar el principio de igualdad de género en el ejercicio del CGPJ (3 vocales)
- **Órganos técnicos**
  - Secretaría general
  - Servicio de inspección
  - Gabinete técnico
  - Escuela judicial
  - Centro de documentación
  - Oficina de comunicación.

Competencias
- Nombramientos
  - Proponer nombramientos del presidente del TS y del CGPJ
  - Proponer nombramiento de los miembros del TS
  - Proponer nombramiento de 2 miembros del TC
- Imposición de recursos
- Gobierno del Poder Judicial
  - Selección de jueces y magistrados
  - Inspección de juzgados y tribunales
  - Estructura y funcionamiento de la Escuela Judicial
  - Concurso Oposición del cuerpo de letrados del CGPJ
- Normativa
- Otras

##### El ministerio Fiscal
No forma parte del Poder Judicial, colabora con este como representante del estado.

Funciones
- Velar por que la función jurisdiccional se ejerza de acuerdo a leyes y plazos
- Defensa de la integridad de Jueces y Tribunales
- Velar por el respeto a las instituciones constitucionales y derechos fundamentales y libertades públicas
- Ejercitar las acciones penales y civiles que surjan de los delitos y faltas
- Intervenir en el proceso penal, instando de autoridad judicial la adopción de medidas cautelares y la práctica de diligencias
- Tomar parte en defensa de la legalidad y el interés público en los procesos relativos al estado civil cuando esté comprometido el interés social o afectan a menores, incapaces, inválidos...
- Mantener la integridad de la jurisdicción y competencia de los jueces y tribunales
- Velar por el cumplimiento de las resoluciones judiciales que afectan al interés público y social
- Velar por la protección procesal de las victimas, promoviendo mecanismos para que reciban ayuda y asistencia
- Recurso de amparo ante el TC
- Intervenir en los procesos de amparo
- Actuar en beneficio del menor
- Intervenir ante el tribunal de cuentas
- Defender la legalidad en los procesos 
  - contencioso-administrativo
  - laboral
- Promover y prestar auxilio judicial internacional

Se organiza en una estructura piramidal: Del Fiscal General del Estado, asistido por el Consejo Fiscal, la junta de Fiscales de Sala, la inspección Fiscal, la secretaría técnica y los fiscales de sala

- Fiscal General
  - Propone al Gobierno el nombramiento de los distintos cargos, previo informe del CGPJ
  - Propone al Gobierno los ascensos conforme a informes del CGPJ
  - Conceder licencias de su competencia
  - Por debajo se encuentran diferentes fiscalías
    - TC, TS y AN
    - Prevención y Represión de Tráfico de Drogas
    - Anticorrupción
    - Tribunales Superiores de Justicia
    - Audiencias provinciales
    - Se componen de
      - Fiscal jefe
      - Teniente fiscal (vice-fiscal jefe)
      - fiscales de sala con categorías
        - Fiscal de sala -> Magistrado en el TS
        - Fiscal -> Magistrado
        - Abogado Fiscal -> Juez

Tienen un estatuto con requisitos, oposición, retribución e incompatibilidades que los jueces.

##### Tribunal Constitucional
Se regula por la LO 2/1979 y modificada por

- **LO 7/1999** De los conflictos en defensa de la autonomía local
- **LO 1/2000** Ampliación de plazos de interposición del recurso de inconstitucionalidad
- **LO 6/2007** Nueva configuración del recurso al amparo
- **LO 1/2010** Atribuye conocimiento de los recursos interpuestos contra las normas forales fiscales del País Vasco, de acuerdo a la Disposición Adicional I de la Constitución y el art. 41.2 del Estatuto Vasco
- **LO 8/2010** Reglas especiales para la renovación del TC en caso de vacantes
- **LO 12/2015** Establecimiento recurso previo de inconstitucionalidad
- **LO 15/2015** Ejecución de resoluciones del TC como garantía del estado de derecho

Es un órgano constitucional, intérprete supremo de la Constitución e independiente del resto de órganos.
> Es un órgano en si mismo, es parte fundamental de la estructura constitucional. Hay paridad de rango y relaciones con otras instituciones estatales.
> Sigue la controversia de si es un ente jurídico (tribunal) por la voluntad de limitar su función a la interpretación de la constitución, o político (intérprete de la constitución) por incidir directamente en la dirección política del estado.

Como Tribunal, actúa por estímulo externo (no lo hace de oficio). El procedimiento es similar a otro tribunal. Resuelve en sentencias de Derecho, no política.
Por otro lado, su actuación es un acto político. Se elije de forma diferente. Sus sentencias se asimilan como acto de legislación, no como una jurisprudencia. Tampoco está dentro del Poder Judicial.

###### Composición 

Se compone de 12 miembros nombrados por el Rey, participando todos los poderes del estado.
 \# | Propuesto por | con
  --|:--------------|:---
  4 | Congreso      | mayoría 3/5
  4 | Senado        | mayoría 3/5
  2 | Gobierno
  2 | CGPJ

Deben ser Españoles, mayores de edad {magistrados, Fiscales, Profesores universitarios, Funcionarios Públicos, Abogados} y Juristas con 15 años de experiencia.

No pueden ser
- Representativos públicos
  - Diputados
  - Senadores
  - Diputados autonómicos
  - Concejales
  - Alcaldes
  - ...
- Cargos políticos
- Empleados de partidos políticos o sindicatos
- Fiscal / Juez
- Incompatibilidad del poder judicial

Funcionan en pleno, sala o sección. 

## B. Temas específicos
### I. Organización y gestión de los sistemas de la información
#### 44. Accesibilidad y usabilidad W3C. Diseño universal. Diseño web adaptativo

##### Accesibilidad
> Accesibilidad es el grado en el que un producto puede ser usado por una persona con algún grado de discapacidad (física, cognitiva o tecnológica) respecto a cómo lo usaría una persona sin discapacidad.

La **accesibilidad web** se refiere al acceso universal a la web independientemente de HW, SW, infraestructura de red, idioma, cultura, localización y capacidades de los usuarios. _La accesibilidad es necesaria para una persona con discapacidad y para cualquier persona que se encuentre bajo circunstancias externas que le dificulten el acceso_

El [W3C](https://www.w3c.es/) a través de su iniciativa [WAI](https://www.w3.org/WAI/)

##### Usabilidad
La usabilidad es la medida en la que un producto puede ser usado por determinados usuarios para conseguir objetivos específicos con efectividad, eficiencia y satisfacción en un contexto de uso específico.

También es la capacidad que tiene un producto SW para ser atractivo, entendido/comprendido, aprendido y usado por el usuario cuando es utilizado bajo unas condiciones de uso

##### W3C
Es una comunidad internacional que desarrolla estándares que aseguran el crecimiento de la web a largo plazo. Su objetivo es guiar la web hacia su máximo potencial, a través del desarrollo de protocolos y pautas que aseguren su futuro.

Sus principios son:
- **Web para todo el mundo** El valor social que aporta la web es que hace posible la comunicación humana, el conocimiento y las oportunidades para compartir conocimiento. Uno de los objetivos principales es hacer que estos beneficios estén disponibles para todo el mundo, con independencia del hardware, software, estructura de red, idioma, cultura, localización y habilidad física o mental
- **Web desde cualquier dispositivo** La cantidad de dispositivos diferentes para acceder a la web ha crecido exponencialmente.

##### WAI
El WAI (W3C Accesibility Initiative) es un comité con el objetivo de promocionar criterios de accesibilidad para la web. Se forma entre otros por estos grupos:
- **Accesibility Guidelines Working Group** [AG-WG] Desarrolla pautas para hacer contenido web accesibles y materiales de apoyo para las Pautas de Accesibilidad de Contenido web
- **Accessible Platform Architectures** [APA] revisa especificaciones, desarrolla materiales de apoyo técnico, colabora con otros grupos de trabajo en tecnología de la accesibilidad y coordina estrategias de accesibilidad armonizada en el W3C
- **Accessible Rich Internet Applications** [ARIA] Desarrolla tecnologías que mejoran la accesibilidad de los contenidos web, incluyendo las propias
- **WAI Interest Group**  es un grupo público con lista de correo para discusión general sobre accesibilidad web

Sus pautas mas destacadas son:
- **Authoring Tool Accessibility Guidelines** [ATAG] muestra como hacer que las herramientas de autor produzcan páginas web e interfaces con contenido accesibles.
- **User Agent Accessibility Guidelines** [UAAG] Muestra como diseñar navegadores accesibles para gente con discapacidad
- **Web Content Accessibility Guidelines** [WCAG] Pautas para hacer que el contenido web sea accesible. Esta destinado a desarrolladores web.

##### Web Content ACcessibility Guidelines
Las [WCAG](https://www.w3.org/WAI/standards-guidelines/wcag/) Guías para mejorar la accesibilidad principalmente de:
- Discapacitados cognitivos o del aprendizaje
- Baja visión
- Personas con discapacidad desde dispositivos móviles

La versión anterior (ver2.0 de 2008) No cubra las necesidades de estos grupos de personas. También han cambiado las tecnologías y las formas de acceder a la web de formas no contempladas. La versión actual (ver2.1 de 2018) pretende mejorar la accesibilidad en entornos de escritorio, portátiles, tablets y móviles.
Añaden: 
- Criterios de conformidad
- Glosario extendido
- Pauta de modalidades de entrada a los principios de operabilidad
- Se añade un punto adicional en componentes opcionales de la declaración de conformidad.

Existen 4 principios generales que constituyen el primer nivel de la estructura y la base filosófica de las pautas:
- **Perceptible** El contenido debe poder percibirse por todos los usuarios (visual, sonora o táctil)
  - **Alternativas** textuales para el contenido no textual
  - Subtítulos y **alternativas** al contenido multimedia
  - Crear contenido que pueda presentarse de --diferentes formas** (incluyendo tecnología asistencial) **sin perder información**
  - Facilitar a los usuarios a ver y oir el contenido
- **Operable** El contenido debe ser manejable por los distintos tipos de entrada
  - Proporcionar acceso completo mediante el **teclado**
  - Proporcionar el **tiempo suficiente** para leer y usar el contenido
  - No usar contenido que pueda provocar **ataques**
  - Ayudar a los usuarios a navegar y **encontrar contenido**
- **Comprensible** Los usuarios han de ser capaces de entender el contenido, organización y manejo
  - Hacer que los contenidos textuales resulten legibles y comprensibles
  - Hacer que los contenidos aparezcan y operen de manera predecible
  - Ayudar a los usuarios a evitar y corregir errores
- **Robusto** El contenido debe estar correctamente estructurado para poder ser utilizado.
  - Maximizar la compatibilidad con aplicaciones de usuario actuales y futuras.

Estas pautas contienen objetivos básicos que se han de seguir para crear contenido accesible. Recogen para ello **criterios de conformidad**. Los criterios tienen varios niveles de conformidad y pueden variar para poder ajustarse a diferentes niveles de conformidad.

Las **Técnicas de suficiencia** permiten lograr cumplir con el criterio para el elemento que se trate. Las **Técnicas complementarias** ayudan a mejorar la accesibilidad, pero no garantizan el completo cumplimiento. Los **Fallos comunes** son técnicas que, de aplicarse, generarán barreras de accesibilidad conocidas y deben evitarse.

##### Conformidad
Para considerar que un contenido es conforme al WCAG hay que cumplir con estos 5 requisitos
1. **Nivel de conformidad** Hay que satisfacer todos los criterios de conformidad de ese nivel y los niveles de menos exigencia, o proporcionar una alternativa conforme al nivel objetivo.
2. **Páginas completas** la conformidad se aplica solo a páginas completas con todo su contenido. Salvo que existan alternativas accesibles directamente desde la página, que se pueden considerar como parte de la misma página.
3. **Procesos completos** La conformidad de cada página sera la misma que la del proceso en su totalidad (si abarca varias páginas)
4. **Uso de tecnologías compatibles con la accesibilidad** si el contenido depende de una o varias tecnologías hay que usarlas en modo que sea compatible con la accesibilidad.
5. **Sin interferencia**Si un contenido usa una tecnología no compatible, no tiene que impedir el acceso al resto del contenido.

De cumplir con todos los requisitos, se puede incluir una declaración de conformidad. Debe incluir:
- Fecha de la declaración
- Título de las pautas, versión y Uri
- Nivel de conformidad
- Descripción de las páginas web
- Lista de tecnologías de las que depende

##### Diseño

El **Diseño universal** (o para todos) implica facilitar el uso para el mayor número de personas posible, sin la necesidad de adaptarlas o rediseñarlas de una forma especial.
Se rige por los siguientes principios:
1. **Uso equitativo** El diseño es útil a personas con diversas capacidades.
2. **Uso flexible** El diseño se acomoda a un amplio rango de preferencias y habilidades individuales
3. **Simple e intuitivo** El uso es fácil de entender
4. **Información perceptible** El diseño comunica de manera eficaz la información necesaria
5. **Con tolerancia al error** El diseño minimiza los riesgos y las consecuencias adversas
6. **Que exige poco esfuerzo físico** El diseño puede ser usado eficaz y confortablemente con mínima fatiga
7. **Tamaño y espacio para el acceso y el uso** Un espacio apropiado para el acceso, alcance, manipulación y uso

Existen diferentes típos de diseño web
- **Diseño fluido** Los elementos se localizan por porcentajes o medidas proporcionales, así se adaptan según los elementos de la pantalla. Da problemas en pantallas muy grandes o muy pequeñas.
- **Diseño adaptable** Usa pantallas estáticas según el punto de ruptura donde cambia de un diseño a otro. Es mas barato de desarrollar, ya que no hay que adaptar la misma pantalla a condiciones mas extremas. En teléfonos suele mostrar una versión con menos contenido
- **Responsive** El diseño y contenido se adapta a cada pantalla. Ordenando los contenidos en bloques que se organizan según el espacio. Permitiendo tener todo el contenido independientemente del contenido

##### Normativa

Sustituye y mejora las condiciones que ya se exigían a los portales de las administraciones publicas. Cubre todos los sitios web y aplicaciones móviles del sector público. Garantiza la igualdad y no discriminación de acceso a toda la ciudadanía:
- Facilitara y garantizara el derecho al acceso a las tecnologías, productos y servicios relacionados con la sociedad de la información y medios de comunicación.
- Facilitará poder hacer un mejor uso de sitios web

Los cambios mas significativos son:
- Equiparación de requisitos de accesibilidad españóles al estándar armonizado europeo
- inclusión de aplicaciones móviles
- inclusión de intranets/extranets nuevas o que se renueven sustancialmente
- Adaptación de webs ya creadas y mecanismos de quejas y solicitudes de información accesible
- Previsiones de seguimiento periódico de implantación del RD mediante gestión descentralizada
- Cada portal deberá tener una declaración de accesibilidad
- Mecanismo de Reclamación a disposición de todas las personas interesadas
- Reportes públicos a la Comisión Europea cada 3 años del estado de la aplicación de la directiva
- Mecanismo de comunicación de incumplimiento

# Glosario
- **Constitución**: Norma fundamental del estado
- **Derogar**: Dejar sin efecto una norma vigente.
- **Interdecir** Vedar, prohibir
- **Interponer** Dar inicio a un recurso legal
- **ley**:  Precepto dictado por la autoridad competente, en que se manda o prohíbe algo en consonancia con la justicia y para el bien de los gobernados.
- **Retroactividad** Extensión de la aplicación de una norma a hechos y situaciones anteriores a su entrada en vigor o a actos y negocios jurídicos.
- **Texto articulado** Texto que tiene artículos con la finalidad de crear una nueva norma sobre una determinada materia
- **Texto refundido** Texto que recoge y unifica varias leyes que regulan la misma materia

# Siglas
- **AN** Audiencia Nacional
- **AGE** Administración General del estado
- **BOE** Boletín Oficial del Estado
- **CCAA** Comunidades Autónomas
- **CGPJ** Consejo General del Poder Judicial
- **PGE** Presupuestos generales del estado
- **TC** Tribunal Constitucional
- **TS** Tribunal Supremo
# Leyes y normas
- **LOREG** Ley Orgánica de Régimen Electoral General
- **ISO-9241** Ergonomic requirements for office work with visual display terminals.
- **ISO9126** Software engineering - Product Quality
- **RD 1112/2018** Real Decreto sobre accesibilidad de los sitios web y aplicaciones para dispositivos móviles del sector público.